package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.OrderItemsResponse
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class FindChecksOrderItemsListAdapter(context: Context, list: List<OrderItemsResponse>) :
    RecyclerView.Adapter<FindChecksOrderItemsListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: List<OrderItemsResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_find_checks_order_items, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): OrderItemsResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_item_name.text = mList!![position].itemName.toString()
        if (mList!![position].modifiersList!!.size > 0) {
            holder.tv_modifier_qty.text = mList!![position].modifiersList!!.size.toString()
        } else {
            holder.tv_modifier_qty.text = "0"
        }
        holder.tv_item_price.text = "%.2f".format(mList!![position].unitPrice)
        holder.tv_item_qty.text = mList!![position].quantity.toString()
        holder.tv_discount_amout.text = "%.2f".format(mList!![position].discountAmount)
        holder.tv_item_net_amount.text =
            "%.2f".format((mList!![position].unitPrice!! * mList!![position].quantity!!.toDouble()))
        holder.tv_item_tax.text = "%.2f".format(mList!![position].itemTaxAmount)
        holder.tv_item_total.text = "%.2f".format(mList!![position].totalPrice)
        holder.tv_voided.text = capitalizeFirstChar(mList!![position].voidStatus.toString())
        if (mList!![position].voidReason.toString() == "null") {
            holder.tv_reason.text = "--"
        } else {
            holder.tv_reason.text = mList!![position].voidReason.toString()
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
        val tv_modifier_qty = view.findViewById(R.id.tv_modifier_qty) as TextView
        val tv_item_price = view.findViewById(R.id.tv_item_price) as TextView
        val tv_item_qty = view.findViewById(R.id.tv_item_qty) as TextView
        val tv_discount_amout = view.findViewById(R.id.tv_discount_amout) as TextView
        val tv_item_net_amount = view.findViewById(R.id.tv_item_net_amount) as TextView
        val tv_item_tax = view.findViewById(R.id.tv_item_tax) as TextView
        val tv_item_total = view.findViewById(R.id.tv_item_total) as TextView
        val tv_voided = view.findViewById(R.id.tv_voided) as TextView
        val tv_reason = view.findViewById(R.id.tv_reason) as TextView
    }

    private fun capitalizeFirstChar(str: String): String {
        return str[0].toUpperCase() + str.slice(1 until str.length)
    }
}