package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.restaurant.zing.apiInterface.StatesResponse
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class StatesListAdapter(
    context: Context,
    serversList: ArrayList<StatesResponse>
) :
    BaseAdapter() {

    private var mContext: Context? = null
    private var mList: ArrayList<StatesResponse>? = null

    init {
        this.mContext = context
        this.mList = serversList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater =
            mContext!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View
        val holder: ItemHolder
        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.spinner_list_country_item, parent, false)
            holder = ItemHolder(view)
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ItemHolder
        }
        holder.tv_name.text = mList!![position].name.toString()

        return view
    }

    override fun getItem(position: Int): Any {
        return mList!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mList!!.size
    }

    fun getSelectedItem(position: Int): StatesResponse {
        return mList!![position]
    }

    inner class ItemHolder(view: View) {
        val tv_name = view.findViewById(R.id.tv_name) as TextView
    }
}