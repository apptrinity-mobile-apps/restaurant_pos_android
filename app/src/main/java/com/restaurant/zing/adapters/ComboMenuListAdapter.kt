package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.getSizeList
import com.restaurant.zing.helpers.ComboBogoCustomPojo
import com.restaurant.zing.helpers.ComboBogoItemType
import com.restaurant.zing.helpers.ComboMenuPositionAndItem
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class ComboMenuListAdapter(
    context: Context,
    list: ArrayList<ComboBogoItemType>,
    quantity: Int
) :
    RecyclerView.Adapter<ComboMenuListAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var quantity = 0
    private var mList: ArrayList<ComboBogoItemType>
    private var comboCustomList: ArrayList<ComboMenuPositionAndItem> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
        this.quantity = quantity
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.combo_bogo_main_menu_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        val layoutManager = LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
        holder.rv_combo_bogo_menu_items.layoutManager = layoutManager
        holder.rv_combo_bogo_menu_items.hasFixedSize()
        if (mList[position].menu_type == "Menu Group") {
            holder.tv_item_type.text = mList[position].name + " (Max. 1 item)"
        } else {
            holder.tv_item_type.text = mList[position].name
        }
        val adapter =
            ComboMenuItemsListAdapter(
                mContext!!,
                position,
                mList[position].menu_type,
                mList[position].items
            )
        holder.rv_combo_bogo_menu_items.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    fun getSelected(): ArrayList<ComboMenuPositionAndItem> {
        return comboCustomList
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rv_combo_bogo_menu_items =
            view.findViewById(R.id.rv_combo_bogo_menu_items) as RecyclerView
        val tv_item_type = view.findViewById(R.id.tv_item_type) as TextView
    }

    inner class ComboMenuItemsListAdapter(
        context: Context,
        menu_position: Int,
        menu_type: String,
        list: ArrayList<ComboBogoCustomPojo>
    ) :
        RecyclerView.Adapter<ComboMenuItemsListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var menuType: String? = null
        private var menuPosition: Int? = null
        private var mList: ArrayList<ComboBogoCustomPojo>
        private var sizeName = ""
        private var basePrice = 0.00

        init {
            this.mContext = context
            this.menuType = menu_type
            this.menuPosition = menu_position
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.combos_menu_item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            var adapter = ComboMenuSizeListAdapter(
                mContext!!,
                menuPosition!!,
                ArrayList()
            )
            val layoutManager = LinearLayoutManager(mContext!!, RecyclerView.HORIZONTAL, false)
            holder.rv_combo_bogo_sizes.layoutManager = layoutManager
            holder.rv_combo_bogo_sizes.hasFixedSize()
            if (mList[position].sizeList.toString() != "null") {
                if (mList[position].sizeList.size > 0) {
                    holder.rv_combo_bogo_sizes.visibility = View.VISIBLE
                    adapter =
                        ComboMenuSizeListAdapter(
                            mContext!!,
                            menuPosition!!,
                            mList[position].sizeList
                        )
                    holder.rv_combo_bogo_sizes.adapter = adapter
                    adapter.notifyDataSetChanged()
                    adapter.setSelectedPosition(0)
                    sizeName = adapter.getSelected().sizeName!!
                    basePrice = adapter.getSelected().price!!.toDouble()
                    Log.d("sizeName", sizeName)
                } else {
                    holder.rv_combo_bogo_sizes.visibility = View.GONE
                    sizeName = ""
                    basePrice = 0.00
                }
            } else {
                holder.rv_combo_bogo_sizes.visibility = View.GONE
                sizeName = ""
                basePrice = 0.00
            }
            if (menuType == "Menu Item") {
                if (mList[position].sizeList.toString() != "null") {
                    if (mList[position].sizeList.size > 0) {
                        holder.ll_item_header.background =
                            ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                    } else {
                        holder.ll_item_header.background =
                            ContextCompat.getDrawable(mContext!!, R.color.transparent)
                    }
                } else {
                    holder.ll_item_header.background =
                        ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                }
                val comboMenuPositionAndItem = ComboMenuPositionAndItem(
                    menuPosition!!,
                    mList[position].itemName,
                    sizeName,
                    menuType!!,
                    mList[position]
                )
                comboCustomList.add(comboMenuPositionAndItem)
            }
            holder.tv_item_name.text = mList[position].itemName
            holder.ll_item_header.setOnClickListener {
                if (menuType == "Menu Group") {
                    if (comboCustomList.size > 0) {
                        var isRemoved = false
                        for (i in 0 until comboCustomList.size) {
                            if (comboCustomList[i].menu_position == menuPosition) {
                                if (comboCustomList[i].menu_type == "Menu Group") {
                                    if (comboCustomList[i].item.itemId == mList[position].itemId) {
                                        comboCustomList.removeAt(i)
                                    }
                                    holder.ll_item_header.background =
                                        ContextCompat.getDrawable(
                                            mContext!!,
                                            R.color.transparent
                                        )
                                    isRemoved = true
                                    break
                                }
                            }
                        }
                        if (!isRemoved) {
                            if (mList[position].sizeList.toString() != "null") {
                                if (mList[position].sizeList.size > 0) {
                                    sizeName = adapter.getSelected().sizeName!!
                                    basePrice = adapter.getSelected().price!!.toDouble()
                                }
                            }
                            val comboMenuPositionAndItem = ComboMenuPositionAndItem(
                                menuPosition!!,
                                mList[position].itemName,
                                sizeName,
                                menuType!!,
                                mList[position]
                            )
                            comboCustomList.add(comboMenuPositionAndItem)
                            holder.ll_item_header.background =
                                ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                        }
                    } else {
                        if (mList[position].sizeList.toString() != "null") {
                            if (mList[position].sizeList.size > 0) {
                                sizeName = adapter.getSelected().sizeName!!
                                basePrice = adapter.getSelected().price!!.toDouble()
                            }
                        }
                        val comboMenuPositionAndItem = ComboMenuPositionAndItem(
                            menuPosition!!,
                            mList[position].itemName,
                            sizeName,
                            menuType!!,
                            mList[position]
                        )
                        comboCustomList.add(comboMenuPositionAndItem)
                        holder.ll_item_header.background =
                            ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                    }
                    Log.d("spos", comboCustomList.size.toString())
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rv_combo_bogo_sizes =
                view.findViewById(R.id.rv_combo_bogo_sizes) as RecyclerView
            val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            val ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
        }
    }

    inner class ComboMenuSizeListAdapter(
        context: Context,
        main_menu_position: Int,
        list: ArrayList<getSizeList>
    ) :
        RecyclerView.Adapter<ComboMenuSizeListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var menuPosition: Int? = null
        private var mList: ArrayList<getSizeList>
        private var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = list
            this.menuPosition = main_menu_position
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.combo_bogo_menu_size_item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_size_name.text = mList[position].sizeName
            holder.cv_size_header.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                for (i in 0 until comboCustomList.size) {
                    if (comboCustomList[i].menu_position == menuPosition) {
                        val comboMenuPositionAndItem = ComboMenuPositionAndItem(
                            menuPosition!!,
                            comboCustomList[i].name,
                            mList[position].sizeName!!,
                            comboCustomList[i].menu_type,
                            comboCustomList[i].item
                        )
                        comboCustomList[i] = comboMenuPositionAndItem
                    }
                }
            }
            if (selectedPosition == position) {
                holder.cv_size_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.button_blue
                    )
                )
                holder.tv_size_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_size_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_size_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black_80
                    )
                )
            }
        }

        fun setSelectedPosition(position: Int) {
            selectedPosition = position
            notifyDataSetChanged()
        }

        fun getSelected(): getSizeList {
            return mList[selectedPosition]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_size_name = view.findViewById(R.id.tv_size_name) as TextView
            val cv_size_header = view.findViewById(R.id.cv_size_header) as CardView
        }
    }
}