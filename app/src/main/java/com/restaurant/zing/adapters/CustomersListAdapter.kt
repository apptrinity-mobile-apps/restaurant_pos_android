package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.activities.CustomerRecordActivity
import com.restaurant.zing.apiInterface.CustomerDataResponse
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class CustomersListAdapter(context: Context, list: ArrayList<CustomerDataResponse>) :
    RecyclerView.Adapter<CustomersListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<CustomerDataResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.customer_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): CustomerDataResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_name.text =
            "${mList!![position].firstName.toString()} ${mList!![position].lastName.toString()}"
        holder.tv_email.text = mList!![position].email.toString()
        holder.tv_number.text = mList!![position].phoneNumber.toString()
        holder.cv_item_header.setOnClickListener {
            val intent = Intent(mContext!!, CustomerRecordActivity::class.java)
            intent.putExtra("customerId", mList!![position].id.toString())
            intent.putExtra("firstName", mList!![position].firstName.toString())
            intent.putExtra("lastName", mList!![position].lastName.toString())
            intent.putExtra("email", mList!![position].email.toString())
            intent.putExtra("phoneNumber", mList!![position].phoneNumber.toString())
            if (mList!![position].credits.toString() == "null") {
                intent.putExtra("credits", mContext!!.getString(R.string.default_value_0))
            } else {
                intent.putExtra("credits", mList!![position].credits.toString())
            }
            mContext!!.startActivity(intent)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name: TextView = view.findViewById(R.id.tv_name)
        var tv_email: TextView = view.findViewById(R.id.tv_email)
        var tv_number: TextView = view.findViewById(R.id.tv_number)
        var cv_item_header: CardView = view.findViewById(R.id.cv_item_header)
    }

}