package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.activities.EmployeeTimeClockActivity
import com.restaurant.zing.apiInterface.EmployeeClockInOutDataResponse
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class TimeCardListAdapter(context: Context, list: ArrayList<EmployeeClockInOutDataResponse>) :
    RecyclerView.Adapter<TimeCardListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<EmployeeClockInOutDataResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.time_card_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): EmployeeClockInOutDataResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_employee_name.text =
            mList!![position].firstName.toString() + " " + mList!![position].lastName.toString()
        if (mList!![position].loggedIn!!) {
            holder.tv_employee_status.text = mContext!!.getString(R.string.value_in)
        } else {
            holder.tv_employee_status.text = mContext!!.getString(R.string.value_out)
        }
        holder.ll_item_header.setOnClickListener {
            val intent = Intent(mContext!!, EmployeeTimeClockActivity::class.java)
            intent.putExtra("empId", mList!![position].id.toString())
            mContext!!.startActivity(intent)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_employee_name: TextView = view.findViewById(R.id.tv_employee_name)
        var tv_employee_status: TextView = view.findViewById(R.id.tv_employee_status)
        var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
    }

}