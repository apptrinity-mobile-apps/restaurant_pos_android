package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.activities.FindChecksDetailedViewActivity
import com.restaurant.zing.apiInterface.MatchedOrders
import com.restaurant.zing.apiInterface.Transactions
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class FindChecksListAdapter(context: Context, list: List<MatchedOrders>) :
    RecyclerView.Adapter<FindChecksListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: List<MatchedOrders>? = null
    private var sessionManager: SessionManager
    private var currencyType = ""

    init {
        this.mContext = context
        this.mList = list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.findcheck_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): MatchedOrders {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        currencyType = sessionManager.getCurrencyType
        holder.tv_currency_total_amount.text = currencyType
        holder.tv_order_number.text =
            "#" + mList!![position].orderNumber.toString() + "/" + mList!![position].checkNumber.toString()
        holder.tv_check_date_time.text = mList!![position].createdOn
        holder.tv_total_amount.text = "%.2f".format(mList!![position].subTotal)
        if (mList!![position].transactionsList.isEmpty()) {
            holder.tv_nopayments.visibility = View.VISIBLE
        } else {
            holder.tv_nopayments.visibility = View.GONE
        }
        val itemsAdapter =
            FindChecksSubListAdapter(mContext!!, mList!![position].transactionsList)
        holder.rv_findcheck_sublist.adapter = itemsAdapter
        itemsAdapter.notifyDataSetChanged()
        holder.cv_header.setOnClickListener {
            val intent = Intent(mContext!!, FindChecksDetailedViewActivity::class.java)
            intent.putExtra("mOrderId", mList!![position].orderId)
            mContext!!.startActivity(intent)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_order_number: TextView = view.findViewById(R.id.tv_order_number)
        var tv_check_date_time: TextView = view.findViewById(R.id.tv_check_date_time)
        var tv_total_amount: TextView = view.findViewById(R.id.tv_total_amount)
        var tv_nopayments: TextView = view.findViewById(R.id.tv_nopayments)
        var tv_currency_total_amount: TextView = view.findViewById(R.id.tv_currency_total_amount)
        val rv_findcheck_sublist = view.findViewById(R.id.rv_findcheck_sublist) as RecyclerView
        val cv_header = view.findViewById(R.id.cv_header) as CardView
    }

    inner class FindChecksSubListAdapter(context: Context, transactions: List<Transactions>) :
        RecyclerView.Adapter<FindChecksSubListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: List<Transactions>? = null

        init {
            this.context = context
            this.mList = transactions
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): FindChecksSubListAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.find_check_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: FindChecksSubListAdapter.ViewHolder, position: Int
        ) {
            holder.tv_fc_paymenttype.text = mList!![position].paymentType
            holder.tv_fc_info.text = mList!![position].cashDrawerName
            holder.tv_fc_amount.text = "%.2f".format(mList!![position].totalAmount)
            holder.tv_fc_tips.text = "%.2f".format(mList!![position].tipAmount)
        }

        fun getItem(position: Int): Transactions {
            return mList!![position]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_fc_paymenttype = view.findViewById(R.id.tv_fc_paymenttype) as TextView
            val tv_fc_info = view.findViewById(R.id.tv_fc_info) as TextView
            val tv_fc_amount = view.findViewById(R.id.tv_fc_amount) as TextView
            val tv_fc_tips = view.findViewById(R.id.tv_fc_tips) as TextView
        }
    }
}