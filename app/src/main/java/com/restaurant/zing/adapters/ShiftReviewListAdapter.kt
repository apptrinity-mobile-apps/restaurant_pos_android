package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.activities.ShiftReviewDetailedViewActivity
import com.restaurant.zing.apiInterface.EmployeeShiftReviewDataResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R

class ShiftReviewListAdapter(context: Context, list: ArrayList<EmployeeShiftReviewDataResponse>) :
    RecyclerView.Adapter<ShiftReviewListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<EmployeeShiftReviewDataResponse>? = null
    private var sessionManager: SessionManager
    private var currencyType = ""

    init {
        this.mContext = context
        this.mList = list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.shift_review_employee_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): EmployeeShiftReviewDataResponse {
        return mList!![position]
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        currencyType = sessionManager.getCurrencyType
        holder.tv_employee_name.text = mList!![position].employeeName.toString()
        holder.tv_currency_employee_total_payment.text = currencyType
        holder.tv_currency_employee_hand_cash.text = currencyType
        holder.tv_currency_employee_non_cash_tip.text = currencyType
        when {
            mList!![position].clockInStatus.equals("clockedIn", true) -> {
                holder.tv_employee_status.text = mContext!!.getString(R.string.clocked_in)
            }
            mList!![position].clockInStatus.equals("clockedOut", true) -> {
                holder.tv_employee_status.text = mContext!!.getString(R.string.clocked_out)
            }
            else -> {
                holder.tv_employee_status.text = ""
            }
        }
        holder.tv_employee_total_payment.text = "%.2f".format(mList!![position].totalAmount)
        if (mList!![position].cashOnHand.toString() == "null") {
            holder.tv_employee_hand_cash.text = "0.00"
        } else {
            holder.tv_employee_hand_cash.text = "%.2f".format(mList!![position].cashOnHand)
        }
        if (mList!![position].nonCashTips.toString() == "null") {
            holder.tv_employee_non_cash_tip.text = "0.00"
        } else {
            holder.tv_employee_non_cash_tip.text = "%.2f".format(mList!![position].nonCashTips)
        }
        holder.ll_item_header.setOnClickListener {
            val intent = Intent(mContext!!, ShiftReviewDetailedViewActivity::class.java)
            intent.putExtra("empId", mList!![position].employeeId.toString())
            mContext!!.startActivity(intent)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_employee_name: TextView = view.findViewById(R.id.tv_employee_name)
        var tv_employee_status: TextView = view.findViewById(R.id.tv_employee_status)
        var tv_employee_total_payment: TextView = view.findViewById(R.id.tv_employee_total_payment)
        var tv_employee_hand_cash: TextView = view.findViewById(R.id.tv_employee_hand_cash)
        var tv_employee_non_cash_tip: TextView = view.findViewById(R.id.tv_employee_non_cash_tip)
        var tv_currency_employee_total_payment: TextView =
            view.findViewById(R.id.tv_currency_employee_total_payment)
        var tv_currency_employee_hand_cash: TextView =
            view.findViewById(R.id.tv_currency_employee_hand_cash)
        var tv_currency_employee_non_cash_tip: TextView =
            view.findViewById(R.id.tv_currency_employee_non_cash_tip)
        var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
    }

}