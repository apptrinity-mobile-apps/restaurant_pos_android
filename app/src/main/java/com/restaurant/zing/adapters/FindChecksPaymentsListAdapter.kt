package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.TransactionsReceiptResponse
import com.restaurant.zing.R
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SetTextI18n")
class FindChecksPaymentsListAdapter(context: Context, list: List<TransactionsReceiptResponse>) :
    RecyclerView.Adapter<FindChecksPaymentsListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: List<TransactionsReceiptResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_find_checks_payments, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): TransactionsReceiptResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /*
        * status
        * 1 -> success -> captured
        * 0 -> */
        holder.tv_payment_type.text = capitalizeFirstChar(mList!![position].paymentMode.toString())
        try {
            val full_date = mList!![position].createdOn.toString()
            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.parse(full_date)
            val sdf1 = SimpleDateFormat("MM-dd-yyyy hh:mm:ss", Locale.getDefault())
            val date_str = sdf1.format(date!!)
            holder.tv_payment_date.text = date_str
        } catch (e: Exception) {
            holder.tv_payment_date.text = ""
            e.printStackTrace()
        }
        holder.tv_tendered_amount.text = "%.2f".format(mList!![position].amount)
        holder.tv_refunded_amount.text = "%.2f".format(mList!![position].changeAmount)
        if (mList!![position].status == 1) {
            holder.tv_payment_status.text = mContext!!.getString(R.string.captured)
        } else {
            holder.tv_payment_status.text = ""
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_payment_type = view.findViewById(R.id.tv_payment_type) as TextView
        val tv_payment_date = view.findViewById(R.id.tv_payment_date) as TextView
        val tv_tendered_amount = view.findViewById(R.id.tv_tendered_amount) as TextView
        val tv_refunded_amount = view.findViewById(R.id.tv_refunded_amount) as TextView
        val tv_payment_status = view.findViewById(R.id.tv_payment_status) as TextView
    }

    private fun capitalizeFirstChar(str: String): String {
        return str[0].toUpperCase() + str.slice(1 until str.length)
    }
}