package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.CreditCardBreakdownDataResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R

class CardBreakdownListAdapter(context: Context, list: ArrayList<CreditCardBreakdownDataResponse>) :
    RecyclerView.Adapter<CardBreakdownListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<CreditCardBreakdownDataResponse>? = null
    private var sessionManager: SessionManager
    private var currencyType = ""

    init {
        this.mContext = context
        this.mList = list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_breakdown_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): CreditCardBreakdownDataResponse {
        return mList!![position]
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        currencyType = sessionManager.getCurrencyType
        holder.tv_item_name.text = mList!![position].cardType
        holder.tv_item_amount.text = currencyType + "%.2f".format(mList!![position].cardAmount)
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_name: TextView = view.findViewById(R.id.tv_item_name)
        var tv_item_amount: TextView = view.findViewById(R.id.tv_item_amount)
    }

}