package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.helpers.VoidItemsDataModel
import com.restaurant.zing.helpers.VoidItemsDataModelNew
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class VoidItemsPaymentListAdapter(context: Context, val list: ArrayList<VoidItemsDataModelNew>) :
    RecyclerView.Adapter<VoidItemsPaymentListAdapter.ViewHolder>() {
    private var mContext: Context? = null
private var tempList: ArrayList<VoidItemsDataModelNew> = ArrayList()

    init {
        this.mContext = context
        this.tempList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.void_item_payment, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var qty = list[position].itemQty
        val voidStatus = list[position].voidStatus
        holder.checkBoxItemName.text = list[position].itemName
        holder.itemQuantity.text = list[position].itemQty.toString()
        holder.checkBoxItemName.isChecked = list[position].isSelected
        holder.checkBoxItemName.tag = list[position]
        if (voidStatus && qty == 0) {
            holder.checkBoxItemName.isEnabled = false
            holder.checkBoxItemName.text =
                list[position].itemName + " " + mContext!!.getString(R.string.voided)
            holder.checkBoxItemName.paintFlags =
                holder.checkBoxItemName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
        // combo/bogo grouping
        val type: String
        when {
            list[position].isBogo -> {
                type = "BoGo"
            }
            list[position].isCombo -> {
                type = "Combo"
            }
            else -> {
                type = ""
                holder.tv_item_type.visibility = View.GONE
                holder.ll_item.setPadding(0, 0, 0, 0)
            }
        }
        holder.tv_item_type.text = type
        if (list[position].isCombo) {
            holder.tv_item_type.visibility = View.VISIBLE
            holder.ll_item.setPadding(20, 0, 0, 0)
        }
        if (list[position].isBogo) {
            holder.tv_item_type.visibility = View.VISIBLE
            holder.ll_item.setPadding(20, 0, 0, 0)
        }
        if (position - 1 != -1) {
            if ((list[position].isCombo && list[position - 1].isCombo) && (list[position].comboUniqueId == list[position - 1].comboUniqueId)) {
                holder.tv_item_type.visibility = View.GONE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if ((list[position].isBogo && list[position - 1].isBogo) && (list[position].bogoUniqueId == list[position - 1].bogoUniqueId)) {
                holder.tv_item_type.visibility = View.GONE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
        }
        holder.checkBoxItemName.setOnClickListener { buttonView ->
            val cb = buttonView as CheckBox
            val dataModel = cb.tag as VoidItemsDataModelNew
            dataModel.isSelected = cb.isChecked
            list[position].isSelected = cb.isChecked
            for (i in 0 until list.size) {
                if (list[position].isCombo) {
                    if ((list[position].comboId == list[i].comboId) && (list[position].comboUniqueId == list[i].comboUniqueId)) {
                        list[i].isSelected = cb.isChecked
                    }
                }
                if (list[position].isBogo) {
                    if ((list[position].bogoId == list[i].bogoId) && (list[position].bogoUniqueId == list[i].bogoUniqueId)) {
                        list[i].isSelected = cb.isChecked
                    }
                }
            }
            notifyDataSetChanged()
        }
        holder.plus.setOnClickListener {
//            if (qty == tempList[position].itemQty) {
//                // do not add
//            } else {
                qty++
                holder.itemQuantity.text = qty.toString()
                for (i in 0 until list.size) {
                    if (list[position].isCombo) {
                        if ((list[position].comboId == list[i].comboId) && (list[position].comboUniqueId == list[i].comboUniqueId)) {
                            list[i].itemQty = qty
                        }
                    } else if (list[position].isBogo) {
                        if ((list[position].bogoId == list[i].bogoId) && (list[position].bogoUniqueId == list[i].bogoUniqueId)) {
                            list[i].itemQty = qty
                        }
                    } else {
                        list[position].itemQty = qty
                    }
                }
//            }
            notifyDataSetChanged()
        }
        holder.minus.setOnClickListener {
            if (qty == 0) {
                // do not remove quantity
            } else {
                qty--
                holder.itemQuantity.text = qty.toString()
                for (i in 0 until list.size) {
                    if (list[position].isCombo) {
                        if ((list[position].comboId == list[i].comboId) && (list[position].comboUniqueId == list[i].comboUniqueId)) {
                            list[i].itemQty = qty
                        }
                    } else if (list[position].isBogo) {
                        if ((list[position].bogoId == list[i].bogoId) && (list[position].bogoUniqueId == list[i].bogoUniqueId)) {
                            list[i].itemQty = qty
                        }
                    } else {
                        list[position].itemQty = qty
                    }
                }
            }
            notifyDataSetChanged()
        }
    }

    fun getVoidedItems(): ArrayList<VoidItemsDataModel> {
        val mSelectedListNew = ArrayList<VoidItemsDataModel>()
        for (i in 0 until list.size) {
            if (list[i].isSelected) {
                mSelectedListNew.add(VoidItemsDataModel(list[i].itemId, list[i].itemQty))
            }
        }
        return mSelectedListNew
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var minus = view.findViewById(R.id.minus) as CardView
        var plus = view.findViewById(R.id.plus) as CardView
        var itemQuantity = view.findViewById(R.id.itemQuantity) as TextView
        var tv_item_type = view.findViewById(R.id.tv_item_type) as TextView
        var ll_item = view.findViewById(R.id.ll_item) as LinearLayout
        var checkBoxItemName = view.findViewById(R.id.checkBoxItemName) as CheckBox
    }

}