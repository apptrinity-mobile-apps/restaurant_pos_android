package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.TotalDiscountsDataResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R

class DiscountsListAdapter(context: Context, list: ArrayList<TotalDiscountsDataResponse>) :
    RecyclerView.Adapter<DiscountsListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<TotalDiscountsDataResponse>? = null
    private var sessionManager: SessionManager
    private var currencyType = ""

    init {
        this.mContext = context
        this.mList = list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.total_discount_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): TotalDiscountsDataResponse {
        return mList!![position]
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        currencyType = sessionManager.getCurrencyType
        holder.tv_item_name.text = mList!![position].discountName.toString()
        holder.tv_item_quantity.text = "(" + mList!![position].count + ")"
        holder.tv_item_amount.text = currencyType + "%.2f".format(mList!![position].amount)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_name: TextView = view.findViewById(R.id.tv_item_name)
        var tv_item_quantity: TextView = view.findViewById(R.id.tv_item_quantity)
        var tv_item_amount: TextView = view.findViewById(R.id.tv_item_amount)
    }

}