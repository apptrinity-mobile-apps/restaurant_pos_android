package com.restaurant.zing.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.helpers.HomeListDataModel
import com.restaurant.zing.R
import com.squareup.picasso.Picasso

class HomeListAdapter(context: Context, list: ArrayList<HomeListDataModel>) :
    RecyclerView.Adapter<HomeListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<HomeListDataModel>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.home_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): HomeListDataModel {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_display_name.text = mList!![position].name
        Picasso.get().load(mList!![position].icon)
            .into(holder.iv_icon)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_display_name: TextView = view.findViewById(R.id.tv_display_name)
        var iv_icon: ImageView = view.findViewById(R.id.iv_icon)
    }

}