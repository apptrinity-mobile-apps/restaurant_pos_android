package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.ModifiersDataResponse
import com.restaurant.zing.apiInterface.OrderItemsResponse
import com.restaurant.zing.apiInterface.SpecialRequestDataResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class ChecksBreakdownPaymentListAdapter(
    context: Context,
    items_list: ArrayList<OrderItemsResponse>
) :
    RecyclerView.Adapter<ChecksBreakdownPaymentListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mItemsList: ArrayList<OrderItemsResponse>? = null
    private val DISCOUNT_LEVEL_ITEM = "item"
    private val DISCOUNT_LEVEL_CHECK = "check"
    private var currencyType = ""
    private var sessionManager: SessionManager

    init {
        this.mContext = context
        this.mItemsList = items_list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.payment_screen_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mItemsList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val type: String
        currencyType = sessionManager.getCurrencyType
        val modifiersLayoutManager = LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
        holder.rv_modifiers.layoutManager = modifiersLayoutManager
        holder.rv_modifiers.hasFixedSize()
        val splRequestLayoutManager = LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
        holder.rv_spl_requests.layoutManager = splRequestLayoutManager
        holder.rv_spl_requests.hasFixedSize()
        if (mItemsList!![position].modifiersList!!.isEmpty()) {
            holder.rv_modifiers.visibility = View.GONE
        } else {
            holder.rv_modifiers.visibility = View.VISIBLE
        }
        if (mItemsList!![position].specialRequestList!!.isEmpty()) {
            holder.rv_spl_requests.visibility = View.GONE
        } else {
            holder.rv_spl_requests.visibility = View.VISIBLE
        }
        val modifiersAdapter =
            ModifiersListAdapter(mContext!!, mItemsList!![position].modifiersList!!)
        holder.rv_modifiers.adapter = modifiersAdapter
        modifiersAdapter.notifyDataSetChanged()
        val requestsAdapter =
            SplRequestsAdapter(mContext!!, mItemsList!![position].specialRequestList!!)
        holder.rv_spl_requests.adapter = requestsAdapter
        requestsAdapter.notifyDataSetChanged()
        holder.tv_item_name.text = mItemsList!![position].itemName.toString()
        holder.tv_item_quantity.text = mItemsList!![position].quantity.toString()
        holder.tv_item_amount.text = currencyType + "%.2f".format(mItemsList!![position].totalPrice)

        when {
            mItemsList!![position].isBogo!! -> {
                type = "BoGo"
            }
            mItemsList!![position].isCombo!! -> {
                type = "Combo"
            }
            else -> {
                type = ""
                holder.tv_item_type.visibility = View.GONE
            }
        }
        holder.tv_item_type.text = type
        if (mItemsList!![position].isCombo!!) {
            holder.tv_item_type.visibility = View.VISIBLE
            holder.tv_item_name.setPadding(25, 0, 0, 0)
        }
        if (mItemsList!![position].isBogo!!) {
            holder.tv_item_type.visibility = View.VISIBLE
            holder.tv_item_name.setPadding(25, 0, 0, 0)
        }
        if (position - 1 != -1) {
            if ((mItemsList!![position].isCombo!! && mItemsList!![position - 1].isCombo!!) && (mItemsList!![position].comboUniqueId == mItemsList!![position - 1].comboUniqueId)) {
                holder.tv_item_type.visibility = View.GONE
                holder.tv_item_name.setPadding(25, 0, 0, 0)
            }
            if ((mItemsList!![position].isBogo!! && mItemsList!![position - 1].isBogo!!) && (mItemsList!![position].bogoUniqueId == mItemsList!![position - 1].bogoUniqueId)) {
                holder.tv_item_type.visibility = View.GONE
                holder.tv_item_name.setPadding(25, 0, 0, 0)
            }
        }
        if (mItemsList!![position].voidStatus!! && mItemsList!![position].quantity == 0) {
            holder.tv_item_name.paintFlags =
                holder.tv_item_name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
        when (mItemsList!![position].discountLevel) {
            DISCOUNT_LEVEL_ITEM -> {
                if (!mItemsList!![position].isCombo!! /*&& !mItemsList!![position].isBogo!!*/) {
                    if (mItemsList!![position].isBogo!!) {
                        holder.tv_item_discount_name.setPadding(0, 0, 0, 0)
                    }
                    holder.ll_item_discount.visibility = View.VISIBLE
                    holder.tv_item_discount_name.text = mItemsList!![position].discountName
                    holder.tv_item_discount_amount.text =
                        currencyType + "%.2f".format(mItemsList!![position].discountAmount)
                }
            }
            DISCOUNT_LEVEL_CHECK -> {
                if (!mItemsList!![position].isCombo!! /*&& !mItemsList!![position].isBogo!!*/) {
                    if (mItemsList!![position].isBogo!!) {
                        holder.tv_item_discount_name.setPadding(0, 0, 0, 0)
                    }
                    holder.ll_item_discount.visibility = View.VISIBLE
                    holder.tv_item_discount_name.text = mItemsList!![position].discountName
                    holder.tv_item_discount_amount.text =
                        currencyType + "%.2f".format(mItemsList!![position].discountAmount)
                }
            }
            else -> {
                holder.ll_item_discount.visibility = View.GONE
            }
        }

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
        var ll_item_discount: LinearLayout = view.findViewById(R.id.ll_item_discount)
        var tv_item_discount_name: TextView = view.findViewById(R.id.tv_item_discount_name)
        var tv_item_discount_amount: TextView = view.findViewById(R.id.tv_item_discount_amount)
        var tv_item_name: TextView = view.findViewById(R.id.tv_item_name)
        var tv_item_quantity: TextView = view.findViewById(R.id.tv_item_quantity)
        var tv_item_amount: TextView = view.findViewById(R.id.tv_item_amount)
        var tv_item_type: TextView = view.findViewById(R.id.tv_item_type)
        var rv_spl_requests: RecyclerView = view.findViewById(R.id.rv_spl_requests)
        var rv_modifiers: RecyclerView = view.findViewById(R.id.rv_modifiers)
    }

    inner class ModifiersListAdapter(
        context: Context,
        modifiers: ArrayList<ModifiersDataResponse>
    ) :
        RecyclerView.Adapter<ModifiersListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<ModifiersDataResponse>? = null

        init {
            this.context = context
            this.mList = modifiers
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): ModifiersListAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.payment_terminal_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ModifiersListAdapter.ViewHolder, position: Int) {
            currencyType = sessionManager.getCurrencyType
            holder.tv_sub_item_name.text = mList!![position].modifierName.toString()
            holder.tv_sub_item_price.text =
                currencyType + "%.2f".format(mList!![position].modifierTotalPrice)
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
            var tv_sub_item_price: TextView = view.findViewById(R.id.tv_sub_item_price)
        }
    }

    inner class SplRequestsAdapter(
        context: Context,
        splRequests: ArrayList<SpecialRequestDataResponse>
    ) : RecyclerView.Adapter<SplRequestsAdapter.ViewHolder>() {
        private var context: Context? = null
        private var mList: ArrayList<SpecialRequestDataResponse>? = null

        init {
            this.context = context
            this.mList = splRequests
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.payment_terminal_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            currencyType = sessionManager.getCurrencyType
            holder.tv_sub_item_name.text = mList!![position].name.toString()
            holder.tv_sub_item_price.text =
                currencyType + "%.2f".format(mList!![position].requestPrice)
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
            var tv_sub_item_price: TextView = view.findViewById(R.id.tv_sub_item_price)
        }
    }

}