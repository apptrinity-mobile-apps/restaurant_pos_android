package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.Combo
import com.restaurant.zing.R

class ComboListAdapter(
    context: Context,
    val sub_listArray: ArrayList<Combo>
) :
    RecyclerView.Adapter<ComboListAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.combo_menu_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sub_listArray.size
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.tv_title_id.text = sub_listArray[position].name
        Log.e("combo_type", sub_listArray[position].type)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
    }

}