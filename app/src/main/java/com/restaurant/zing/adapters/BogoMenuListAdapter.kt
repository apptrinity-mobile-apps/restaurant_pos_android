package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.getSizeList
import com.restaurant.zing.helpers.*
import com.restaurant.zing.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class BogoMenuListAdapter(
    context: Context,
    list: ArrayList<ComboBogoItemType>,
    quantity: Int
) :
    RecyclerView.Adapter<BogoMenuListAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var quantity = 0
    private var mList: ArrayList<ComboBogoItemType>
    private var bogoCustomList: ArrayList<BogoMenuPositionAndItem> = ArrayList()
    private var dayOfTheWeek = ""
    private var currentTime = ""
    private var currencyType = ""
    private var sessionManager: SessionManager

    init {
        this.mContext = context
        this.mList = list
        this.quantity = quantity
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.combo_bogo_main_menu_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        val layoutManager = LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
        holder.rv_combo_bogo_menu_items.layoutManager = layoutManager
        holder.rv_combo_bogo_menu_items.hasFixedSize()
        if (mList[position].menu_type == "Menu Group") {
            holder.tv_item_type.text = mList[position].name + " (Max. 1 item)"
        } else {
            holder.tv_item_type.text = mList[position].name
        }
        val adapter =
            BogoMenuItemsListAdapter(
                mContext!!,
                position,
                mList[position].menu_type,
                mList[position].items
            )
        holder.rv_combo_bogo_menu_items.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    fun getSelected(): ArrayList<BogoMenuPositionAndItem> {
        return bogoCustomList
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rv_combo_bogo_menu_items =
            view.findViewById(R.id.rv_combo_bogo_menu_items) as RecyclerView
        val tv_item_type = view.findViewById(R.id.tv_item_type) as TextView
    }

    inner class BogoMenuItemsListAdapter(
        context: Context,
        menu_position: Int,
        menu_type: String,
        list: ArrayList<ComboBogoCustomPojo>
    ) :
        RecyclerView.Adapter<BogoMenuItemsListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var menuType: String? = null
        private var menuPosition: Int? = null
        private var mList: ArrayList<ComboBogoCustomPojo>
        private var sizeName = ""
        private var basePrice = 0.00

        init {
            this.mContext = context
            this.menuType = menu_type
            this.menuPosition = menu_position
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.bogos_menu_item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            /*PriceStrategy 1 = Base price
                                2 = Size list price
                                3 = Base price
                                4 = Time Based
                                5 = Open Price */
            holder.ll_amount.visibility = View.GONE
            holder.rv_combo_bogo_sizes.visibility = View.GONE
            currencyType = sessionManager.getCurrencyType
            // setting default value to 0.00
            holder.et_custom_amount.setText("0.00")
            holder.tv_currency_amount.text = currencyType
            var adapter = BogoMenuSizeListAdapter(mContext!!, menuPosition!!, ArrayList())
            val layoutManager = LinearLayoutManager(mContext!!, RecyclerView.HORIZONTAL, false)
            holder.rv_combo_bogo_sizes.layoutManager = layoutManager
            holder.rv_combo_bogo_sizes.hasFixedSize()
            when (mList[position].pricingStrategy) {
                1 -> {
                    basePrice = mList[position].basePrice
                }
                2 -> {
                    if (mList[position].sizeList.toString() != "null") {
                        if (mList[position].sizeList.size > 0) {
                            holder.rv_combo_bogo_sizes.visibility = View.VISIBLE
                            adapter =
                                BogoMenuSizeListAdapter(
                                    mContext!!,
                                    menuPosition!!,
                                    mList[position].sizeList
                                )
                            holder.rv_combo_bogo_sizes.adapter = adapter
                            adapter.notifyDataSetChanged()
                            adapter.setSelectedPosition(0)
                            sizeName = adapter.getSelected().sizeName!!
                            basePrice = adapter.getSelected().price!!.toDouble()
                            Log.d("sizeName", sizeName)
                        } else {
                            holder.rv_combo_bogo_sizes.visibility = View.GONE
                            sizeName = ""
                            basePrice = 0.00
                        }
                    } else {
                        holder.rv_combo_bogo_sizes.visibility = View.GONE
                        sizeName = ""
                        basePrice = 0.00
                    }
                }
                3 -> {
                    basePrice = mList[position].menuPriceList[0].price!!.toDouble()
                }
                4 -> {
                    var timeBased = false
                    try {
                        val sdf = SimpleDateFormat("EEEE", Locale.getDefault())
                        val d = Date()
                        dayOfTheWeek = sdf.format(d)
                        val df: DateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
                        currentTime = df.format(Calendar.getInstance().time)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    for (k in 0 until mList[position].timePriceList.size) {
                        for (element in mList[position].timePriceList[k].days) {
                            if (dayOfTheWeek == element) {
                                try {
                                    val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
                                    val currentDate: Date = dateFormat.parse(currentTime)!!
                                    val timeFrom: Date =
                                        dateFormat.parse(mList[position].timePriceList[k].timeFrom)!!
                                    val timeTo: Date =
                                        dateFormat.parse(mList[position].timePriceList[k].timeTo)!!
                                    if (currentDate.after(timeFrom) && currentDate.before(timeTo)) {
                                        timeBased = true
                                        basePrice =
                                            mList[position].timePriceList[k].price.toDouble()
                                        break
                                    }
                                } catch (e: java.lang.Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    if (!timeBased) {
                        basePrice = mList[position].basePrice
                    }
                }
                5 -> {
                    holder.ll_amount.visibility = View.VISIBLE
                    basePrice = holder.et_custom_amount.text.toString().toDouble()
                }
            }
            if (menuType == "Menu Item") {
                if (mList[position].sizeList.toString() != "null") {
                    if (mList[position].sizeList.size > 0) {
                        holder.ll_item_header.background =
                            ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                    } else {
                        holder.ll_item_header.background =
                            ContextCompat.getDrawable(mContext!!, R.color.transparent)
                    }
                } else {
                    holder.ll_item_header.background =
                        ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                }
                val bogoMenuPositionAndItem = BogoMenuPositionAndItem(
                    menuPosition!!,
                    mList[position].itemName,
                    sizeName,
                    basePrice,
                    menuType!!,
                    mList[position]
                )
                bogoCustomList.add(bogoMenuPositionAndItem)
            }
            holder.tv_item_name.text = mList[position].itemName
            holder.ll_item_header.setOnClickListener {
                if (menuType == "Menu Group") {
                    when (mList[position].pricingStrategy) {
                        1 -> {
                            basePrice = mList[position].basePrice
                        }
                        2 -> {
                            if (mList[position].sizeList.toString() != "null") {
                                if (mList[position].sizeList.size > 0) {
                                    holder.rv_combo_bogo_sizes.visibility = View.VISIBLE
                                    sizeName = adapter.getSelected().sizeName!!
                                    basePrice = adapter.getSelected().price!!.toDouble()
                                } else {
                                    holder.rv_combo_bogo_sizes.visibility = View.GONE
                                    sizeName = ""
                                    basePrice = 0.00
                                }
                            } else {
                                holder.rv_combo_bogo_sizes.visibility = View.GONE
                                sizeName = ""
                                basePrice = 0.00
                            }
                        }
                        3 -> {
                            basePrice = mList[position].menuPriceList[0].price!!.toDouble()
                        }
                        4 -> {
                            var timeBased = false
                            try {
                                val sdf = SimpleDateFormat("EEEE", Locale.getDefault())
                                val d = Date()
                                dayOfTheWeek = sdf.format(d)
                                val df: DateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
                                currentTime = df.format(Calendar.getInstance().time)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            for (k in 0 until mList[position].timePriceList.size) {
                                for (element in mList[position].timePriceList[k].days) {
                                    if (dayOfTheWeek == element) {
                                        try {
                                            val dateFormat =
                                                SimpleDateFormat("HH:mm", Locale.getDefault())
                                            val currentDate: Date = dateFormat.parse(currentTime)!!
                                            val timeFrom: Date =
                                                dateFormat.parse(mList[position].timePriceList[k].timeFrom)!!
                                            val timeTo: Date =
                                                dateFormat.parse(mList[position].timePriceList[k].timeTo)!!
                                            if (currentDate.after(timeFrom) && currentDate.before(
                                                    timeTo
                                                )
                                            ) {
                                                timeBased = true
                                                basePrice =
                                                    mList[position].timePriceList[k].price.toDouble()
                                                break
                                            }
                                        } catch (e: java.lang.Exception) {
                                            e.printStackTrace()
                                        }
                                    }
                                }
                            }
                            if (!timeBased) {
                                basePrice = mList[position].basePrice
                            }
                        }
                        5 -> {
                            basePrice = holder.et_custom_amount.text.toString().toDouble()
                        }
                    }
                    if (bogoCustomList.size > 0) {
                        var isRemoved = false
                        for (i in 0 until bogoCustomList.size) {
                            if (bogoCustomList[i].menu_position == menuPosition) {
                                if (bogoCustomList[i].menu_type == "Menu Group") {
                                    if (bogoCustomList[i].item.itemId == mList[position].itemId) {
                                        bogoCustomList.removeAt(i)
                                    }
                                    holder.ll_item_header.background =
                                        ContextCompat.getDrawable(
                                            mContext!!,
                                            R.color.transparent
                                        )
                                    isRemoved = true
                                    break
                                }
                            }
                        }
                        if (!isRemoved) {
                            val bogoMenuPositionAndItem = BogoMenuPositionAndItem(
                                menuPosition!!,
                                mList[position].itemName,
                                sizeName,
                                basePrice,
                                menuType!!,
                                mList[position]
                            )
                            bogoCustomList.add(bogoMenuPositionAndItem)
                            holder.ll_item_header.background =
                                ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                        }
                    } else {
                        val bogoMenuPositionAndItem = BogoMenuPositionAndItem(
                            menuPosition!!,
                            mList[position].itemName,
                            sizeName,
                            basePrice,
                            menuType!!,
                            mList[position]
                        )
                        bogoCustomList.add(bogoMenuPositionAndItem)
                        holder.ll_item_header.background =
                            ContextCompat.getDrawable(mContext!!, R.color.blue_send_25)
                    }
                }
            }
            holder.et_custom_amount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s!!.isNotEmpty()) {
                        basePrice = s.toString().toDouble()
                        for (i in 0 until bogoCustomList.size) {
                            if (bogoCustomList[i].menu_position == menuPosition) {
                                val bogoMenuPositionAndItem = BogoMenuPositionAndItem(
                                    menuPosition!!,
                                    bogoCustomList[i].name,
                                    bogoCustomList[i].sizeName,
                                    basePrice,
                                    bogoCustomList[i].menu_type,
                                    bogoCustomList[i].item
                                )
                                bogoCustomList[i] = bogoMenuPositionAndItem
                            }
                        }
                    }
                }
            })
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rv_combo_bogo_sizes =
                view.findViewById(R.id.rv_combo_bogo_sizes) as RecyclerView
            val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            val tv_currency_amount = view.findViewById(R.id.tv_currency_amount) as TextView
            val ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            val ll_amount = view.findViewById(R.id.ll_amount) as LinearLayout
            val et_custom_amount = view.findViewById(R.id.et_custom_amount) as EditText
        }
    }

    inner class BogoMenuSizeListAdapter(
        context: Context,
        main_menu_position: Int,
        list: ArrayList<getSizeList>
    ) :
        RecyclerView.Adapter<BogoMenuSizeListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var menuPosition: Int? = null
        private var mList: ArrayList<getSizeList>
        private var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = list
            this.menuPosition = main_menu_position
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.combo_bogo_menu_size_item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_size_name.text = mList[position].sizeName
            holder.cv_size_header.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                for (i in 0 until bogoCustomList.size) {
                    if (bogoCustomList[i].menu_position == menuPosition) {
                        val bogoMenuPositionAndItem = BogoMenuPositionAndItem(
                            menuPosition!!,
                            bogoCustomList[i].name,
                            mList[position].sizeName!!,
                            mList[position].price!!.toDouble(),
                            bogoCustomList[i].menu_type,
                            bogoCustomList[i].item
                        )
                        bogoCustomList[i] = bogoMenuPositionAndItem
                    }
                }
            }
            if (selectedPosition == position) {
                holder.cv_size_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.button_blue
                    )
                )
                holder.tv_size_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_size_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_size_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black_80
                    )
                )
            }
        }

        fun setSelectedPosition(position: Int) {
            selectedPosition = position
            notifyDataSetChanged()
        }

        fun getSelected(): getSizeList {
            return mList[selectedPosition]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_size_name = view.findViewById(R.id.tv_size_name) as TextView
            val cv_size_header = view.findViewById(R.id.cv_size_header) as CardView
        }
    }
}