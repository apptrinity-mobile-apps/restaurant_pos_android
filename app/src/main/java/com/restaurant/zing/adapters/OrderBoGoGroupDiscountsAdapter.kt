package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.BogoBuyItem
import com.restaurant.zing.R

class OrderBoGoGroupDiscountsAdapter(
    context: Context,
    val bogo_group_listArray: List<BogoBuyItem>
) :
    RecyclerView.Adapter<OrderBoGoGroupDiscountsAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.combo_group_menu_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bogo_group_listArray.size
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.tv_title_id.text = bogo_group_listArray[position].name
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
        val cv_bogo_id = view.findViewById<CardView>(R.id.cv_bogo_id)
    }

}