package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.CustomerCreditsHistoryResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class CustomerCreditsHistoryListAdapter(
    context: Context,
    list: ArrayList<CustomerCreditsHistoryResponse>
) :
    RecyclerView.Adapter<CustomerCreditsHistoryListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<CustomerCreditsHistoryResponse>? = null
    private val TYPE_CREDIT = "credit"
    private val TYPE_DEBIT = "debit"
    private var sessionManager: SessionManager
    private var currencyType = ""

    init {
        this.mContext = context
        this.mList = list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.customer_credit_history_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): CustomerCreditsHistoryResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        currencyType = sessionManager!!.getCurrencyType
        if (mList!![position].credits.toString() == "null") {
            holder.tv_credits.text = mContext!!.getString(R.string.default_value_0)
        } else {
            holder.tv_credits.text = "%.2f".format(mList!![position].credits)
        }
        holder.tv_credits_type.text =
            capitalizeFirstChar(mList!![position].transactionType.toString())
        holder.tv_restaurant_name.text = mList!![position].restaurantName.toString()
        if (mList!![position].creditsReason.toString() == "null") {
            holder.tv_reason.text = ""
        } else {
            holder.tv_reason.text = mList!![position].creditsReason.toString()
        }
        try {
            val full_date = mList!![position].createdOn.toString()
            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.parse(full_date)
            val sdf1 = SimpleDateFormat("MM/dd/yyyy hh:mm aa", Locale.getDefault())
            val date_str = sdf1.format(date!!)
            holder.tv_credit_date.text = date_str.toUpperCase(Locale.ROOT)
        } catch (e: Exception) {
            holder.tv_credit_date.text = ""
            e.printStackTrace()
        }
        holder.tv_currency_type.text = currencyType
        if (mList!![position].transactionType.toString() == TYPE_CREDIT) {
            holder.tv_credits.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.button_black)
            )
            holder.tv_currency_type.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.button_black)
            )
        } else if (mList!![position].transactionType.toString() == TYPE_DEBIT) {
            holder.tv_credits.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.current_status_open)
            )
            holder.tv_currency_type.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.current_status_open)
            )
        }
        holder.cv_item_header.setOnClickListener { }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_credits = view.findViewById(R.id.tv_credits) as TextView
        val tv_credit_date = view.findViewById(R.id.tv_credit_date) as TextView
        val tv_currency_type = view.findViewById(R.id.tv_currency_type) as TextView
        val tv_credits_type = view.findViewById(R.id.tv_credits_type) as TextView
        val tv_restaurant_name = view.findViewById(R.id.tv_restaurant_name) as TextView
        val tv_reason = view.findViewById(R.id.tv_reason) as TextView
        val cv_item_header = view.findViewById(R.id.cv_item_header) as CardView
    }

    private fun capitalizeFirstChar(str: String): String {
        return str[0].toUpperCase() + str.slice(1 until str.length)
    }
}