package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.DeliveryDataResponse
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class DeliveryCloseOutAdapter(context: Context, list: ArrayList<DeliveryDataResponse>) :
    RecyclerView.Adapter<DeliveryCloseOutAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<DeliveryDataResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.delivery_close_out_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): DeliveryDataResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_delivery_name.text = mList!![position].type.toString()
        holder.tv_delivery_quantity.text = "(" + mList!![position].qua + ")"
        holder.tv_delivery_amount.text = "%.2f".format(mList!![position].amount)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_delivery_amount: TextView = view.findViewById(R.id.tv_delivery_amount)
        var tv_delivery_quantity: TextView = view.findViewById(R.id.tv_delivery_quantity)
        var tv_delivery_name: TextView = view.findViewById(R.id.tv_delivery_name)
    }

}