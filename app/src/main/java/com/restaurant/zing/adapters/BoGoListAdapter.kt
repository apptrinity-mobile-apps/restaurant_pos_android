package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.BogoBuyItem
import com.restaurant.zing.R

class BoGoListAdapter(
    context: Context,
    val sub_listArray: ArrayList<BogoBuyItem>
) :
    RecyclerView.Adapter<BoGoListAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.combo_menu_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sub_listArray.size
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.tv_title_id.text = sub_listArray[position].name
        Log.e("bogo_type", sub_listArray[position].type)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
        val cv_bogo_id = view.findViewById<CardView>(R.id.cv_bogo_id)
    }
}