package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.ModifiersDataResponse
import com.restaurant.zing.apiInterface.OrderItemsResponse
import com.restaurant.zing.apiInterface.SpecialRequestDataResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R

@SuppressLint("SetTextI18n")
class DeliveryItemListAdapter(
    context: Context,
    items_list: ArrayList<OrderItemsResponse>
) :
    RecyclerView.Adapter<DeliveryItemListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mItemsList: ArrayList<OrderItemsResponse>? = null
    private var sessionManager: SessionManager
    private var currencyType = ""

    init {
        this.mContext = context
        this.mItemsList = items_list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.delivery_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mItemsList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        currencyType = sessionManager.getCurrencyType
        val modifiersLayoutManager =
            LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
        holder.rv_modifiers.layoutManager = modifiersLayoutManager
        holder.rv_modifiers.hasFixedSize()
        val splRequestLayoutManager =
            LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
        holder.rv_spl_requests.layoutManager = splRequestLayoutManager
        holder.rv_spl_requests.hasFixedSize()
        if (mItemsList!![position].modifiersList!!.isEmpty()) {
            holder.rv_modifiers.visibility = View.GONE
        } else {
            holder.rv_modifiers.visibility = View.VISIBLE
        }
        if (mItemsList!![position].specialRequestList!!.isEmpty()) {
            holder.rv_spl_requests.visibility = View.GONE
        } else {
            holder.rv_spl_requests.visibility = View.VISIBLE
        }
        val modifiersAdapter =
            ModifiersListAdapter(mContext!!, mItemsList!![position].modifiersList!!)
        holder.rv_modifiers.adapter = modifiersAdapter
        modifiersAdapter.notifyDataSetChanged()
        val requestsAdapter =
            SplRequestsAdapter(mContext!!, mItemsList!![position].specialRequestList!!)
        holder.rv_spl_requests.adapter = requestsAdapter
        requestsAdapter.notifyDataSetChanged()
        holder.tv_currency_total.text = currencyType
        holder.tv_item_name.text = mItemsList!![position].itemName.toString()
        holder.tv_item_quantity.text = mItemsList!![position].quantity.toString()
        holder.tv_item_amount.text = "%.2f".format(mItemsList!![position].totalPrice)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
        var tv_item_name: TextView = view.findViewById(R.id.tv_item_name)
        var tv_item_quantity: TextView = view.findViewById(R.id.tv_item_quantity)
        var tv_item_amount: TextView = view.findViewById(R.id.tv_item_amount)
        var tv_currency_total: TextView = view.findViewById(R.id.tv_currency_total)
        var rv_spl_requests: RecyclerView = view.findViewById(R.id.rv_spl_requests)
        var rv_modifiers: RecyclerView = view.findViewById(R.id.rv_modifiers)
    }

    inner class ModifiersListAdapter(
        context: Context,
        modifiers: ArrayList<ModifiersDataResponse>
    ) :
        RecyclerView.Adapter<ModifiersListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<ModifiersDataResponse>? = null

        init {
            this.context = context
            this.mList = modifiers
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): ModifiersListAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ModifiersListAdapter.ViewHolder, position: Int
        ) {
            currencyType = sessionManager.getCurrencyType
            holder.tv_currency_total.text = currencyType
            holder.tv_sub_item_name.text = mList!![position].modifierName.toString()
            holder.tv_sub_item_price.text = "%.2f".format(mList!![position].modifierTotalPrice)
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
            var tv_sub_item_price: TextView = view.findViewById(R.id.tv_sub_item_price)
            var tv_currency_total: TextView = view.findViewById(R.id.tv_currency_total)
        }
    }

    inner class SplRequestsAdapter(
        context: Context,
        splRequests: ArrayList<SpecialRequestDataResponse>
    ) : RecyclerView.Adapter<SplRequestsAdapter.ViewHolder>() {
        private var context: Context? = null
        private var mList: ArrayList<SpecialRequestDataResponse>? = null

        init {
            this.context = context
            this.mList = splRequests
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            currencyType = sessionManager.getCurrencyType
            holder.tv_currency_total.text = currencyType
            holder.tv_sub_item_name.text = mList!![position].name.toString()
            holder.tv_sub_item_price.text = "%.2f".format(mList!![position].requestPrice)
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
            var tv_sub_item_price: TextView = view.findViewById(R.id.tv_sub_item_price)
            var tv_currency_total: TextView = view.findViewById(R.id.tv_currency_total)
        }
    }

}
