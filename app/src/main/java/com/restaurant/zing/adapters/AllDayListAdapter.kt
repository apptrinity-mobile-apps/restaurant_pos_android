package com.restaurant.zing.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.OrderedItemsResponse
import com.restaurant.zing.R

class AllDayListAdapter (
    context: Context,
    modifiers: ArrayList<OrderedItemsResponse>
) :
    RecyclerView.Adapter<AllDayListAdapter.ViewHolder>() {

    private var context: Context? = null
    private var mList: ArrayList<OrderedItemsResponse>? = null

    init {
        this.context = context
        this.mList = modifiers
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): AllDayListAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.all_day_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(
        holder: AllDayListAdapter.ViewHolder, position: Int
    ) {
        holder.tv_item_name.text = mList!![position].name.toString()
        holder.tv_item_quantity.text = mList!![position].quntity.toString()
        holder.tv_item_name.isSelected = true
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
        val tv_item_quantity = view.findViewById(R.id.tv_item_quantity) as TextView
    }
}