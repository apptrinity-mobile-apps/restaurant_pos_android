package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.*
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.EmployeeInOutHoursDataResponse
import com.restaurant.zing.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class EmployeeTimeCardDetailsListAdapter(
    context: Context, list: ArrayList<EmployeeInOutHoursDataResponse>
) :
    RecyclerView.Adapter<EmployeeTimeCardDetailsListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<EmployeeInOutHoursDataResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.employee_clock_in_out_list_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): EmployeeInOutHoursDataResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val str_intime = mList!![position].checkInTime.toString()
            val str_outtime = mList!![position].checkOutTime.toString()
            val intime_str = sdf.parse(str_intime)
            val date_str = sdf.parse(str_intime)
            val time_sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
            val date_sdf = SimpleDateFormat("MM/dd/yy", Locale.getDefault())
            val inTime = time_sdf.format(intime_str!!)
            var outTime = ""
            if (str_outtime == "null") {
                outTime = "---"
            } else {
                val outtime_str = sdf.parse(str_outtime)
                outTime = time_sdf.format(outtime_str!!)
            }
            val date = date_sdf.format(date_str!!)
            holder.tv_item_date.text = date
            holder.tv_item_job_title.text = mList!![position].jobTitle.toString()
            holder.tv_item_time_in.text = inTime
            holder.tv_item_time_out.text = outTime
            holder.tv_item_hours.text = mList!![position].hours.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_date: TextView = view.findViewById(R.id.tv_item_date)
        var tv_item_job_title: TextView = view.findViewById(R.id.tv_item_job_title)
        var tv_item_time_in: TextView = view.findViewById(R.id.tv_item_time_in)
        var tv_item_time_out: TextView = view.findViewById(R.id.tv_item_time_out)
        var tv_item_hours: TextView = view.findViewById(R.id.tv_item_hours)
    }
}