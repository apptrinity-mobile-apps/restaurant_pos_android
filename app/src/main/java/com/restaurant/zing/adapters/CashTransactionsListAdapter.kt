package com.restaurant.zing.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.zing.apiInterface.CashTransactionsListResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class CashTransactionsListAdapter(context: Context, list: ArrayList<CashTransactionsListResponse>) :
    RecyclerView.Adapter<CashTransactionsListAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var mList: ArrayList<CashTransactionsListResponse>? = null
    private var currencyType = ""
    private var sessionManager: SessionManager

    init {
        this.mContext = context
        this.mList = list
        sessionManager = SessionManager(mContext!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cash_drawer_sub_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItem(position: Int): CashTransactionsListResponse {
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        currencyType = sessionManager.getCurrencyType
        try {
            val full_date = mList!![position].createdOn.toString()
            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.parse(full_date)
            val sdf1 = SimpleDateFormat("MM/dd, hh:mm aa", Locale.getDefault())
            val date_str = sdf1.format(date!!)
            holder.tv_item_date.text = date_str
            holder.tv_item_action_type.text = mList!![position].action.toString()
            if (mList!![position].amount.toString() == "null") {
                holder.tv_item_amount.text =
                    currencyType + mContext!!.getString(R.string.default_value_0)
            } else {
                holder.tv_item_amount.text = currencyType + "%.2f".format(mList!![position].amount)
            }
            holder.tv_item_user.text = mList!![position].User.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        holder.ll_item_header.setOnClickListener {
            showCashTransactionDialog(mList!![position])
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_date: TextView = view.findViewById(R.id.tv_item_date)
        var tv_item_action_type: TextView = view.findViewById(R.id.tv_item_action_type)
        var tv_item_amount: TextView = view.findViewById(R.id.tv_item_amount)
        var tv_item_user: TextView = view.findViewById(R.id.tv_item_user)
        var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
    }

    private fun showCashTransactionDialog(listItem: CashTransactionsListResponse) {
        val dialog = Dialog(mContext!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(mContext!!).inflate(R.layout.cash_transaction_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog.setCanceledOnTouchOutside(true)
        val tv_date = view.findViewById(R.id.tv_date) as TextView
        val tv_action_type = view.findViewById(R.id.tv_action_type) as TextView
        val tv_amount = view.findViewById(R.id.tv_amount) as TextView
        val tv_user = view.findViewById(R.id.tv_user) as TextView
        val tv_comment = view.findViewById(R.id.tv_comment) as TextView
        val full_date = listItem.createdOn.toString()
        val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
        val sdf = SimpleDateFormat(pattern, Locale.getDefault())
        val date = sdf.parse(full_date)
        val sdf1 = SimpleDateFormat("MM/dd, hh:mm aa", Locale.getDefault())
        val date_str = sdf1.format(date!!)
        tv_date.text = date_str
        tv_action_type.text = listItem.action.toString()
        if (listItem.amount.toString() == "null") {
            tv_amount.text = currencyType + mContext!!.getString(R.string.default_value_0)
        } else {
            tv_amount.text = currencyType + "%.2f".format(listItem.amount)
        }
        tv_user.text = listItem.User.toString()
        if (listItem.comment.toString() == "null") {
            tv_comment.text = ""
        } else {
            tv_comment.text = listItem.comment.toString()
        }
        dialog.show()
    }

}