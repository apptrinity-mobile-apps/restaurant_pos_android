package com.restaurant.zing.apiInterface

class VoidReasonsResponse {
    val description: String? = null
    val id: String? = null
    val isActive: Boolean? = null
    val name: String? = null
    val orderValue: String? = null
}
