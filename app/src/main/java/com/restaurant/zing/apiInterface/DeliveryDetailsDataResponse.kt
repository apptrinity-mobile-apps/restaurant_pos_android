package com.restaurant.zing.apiInterface

import java.io.Serializable

class DeliveryDetailsDataResponse : Serializable {
    val address1: String? = null
    val address2: String? = null
    val cityName: String? = null
    val countryCode: String? = null
    val latitude: String? = null
    val longitude: String? = null
    val notes: String? = null
    val stateName: String? = null
    val zipCode: String? = null
    val fullAddress: String? = null
}
