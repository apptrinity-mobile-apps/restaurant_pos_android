package com.restaurant.zing.apiInterface

class CashTransactionsOpenDataResponse {
    val result: String? = null
    val responseStatus: Int? = null
    val startingBalance: ArrayList<StartingBalanceResponse>? = null
}
