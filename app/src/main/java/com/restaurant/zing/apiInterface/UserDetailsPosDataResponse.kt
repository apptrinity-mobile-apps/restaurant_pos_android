package com.restaurant.zing.apiInterface

class UserDetailsPosDataResponse {
    val addOrUpdateServiceCharges: Boolean? = null
    val applyCashPayments: Boolean? = null
    val cashDrawerAccess: Boolean? = null
    val changeServer: Boolean? = null
    val changeTable: Boolean? = null
    val editOtherEmployeesOrders: Boolean? = null
    val keyInCreditCards: Boolean? = null
    val kitchenDisplayScreenMode: Boolean? = null
    val myReports: Boolean? = null
    val noSale: Boolean? = null
    val offlineOrBackgroundCreditCardProcessing: Boolean? = null
    val pandingOrdersMode: Boolean? = null
    val paymentTerminalMode: Boolean? = null
    val quickOrderMode: Boolean? = null
    val shiftReviewSalesData: Boolean? = null
    val tableServiceMode: Boolean? = null
    val viewOtherEmployeesOrders: Boolean? = null
}
