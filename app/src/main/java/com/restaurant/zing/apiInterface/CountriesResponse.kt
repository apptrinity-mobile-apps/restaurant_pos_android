package com.restaurant.zing.apiInterface

import java.io.Serializable

class CountriesResponse : Serializable {
    var _id: String? = null
    var capital: String? = null
    var countryCode: String? = null
    var countryId: Int? = null
    var currency: String? = null
    var iso3: String? = null
    var name: String? = null
    var phoneCode: String? = null
    var status: Int? = null
}
