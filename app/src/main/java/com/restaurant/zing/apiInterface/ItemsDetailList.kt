package com.restaurant.zing.apiInterface

class ItemsDetailList {

    /*"itemsDetails": [
               {
                   "discountAmount": 0.0,
                   "discountType": "",
                   "discountValue": 0.0,
                   "id": "5f6ddc6427a1ddc07a0da2e4",
                   "itemId": "5f6c61f4231345a28de22b3f",
                   "itemName": "item3-1",
                   "itemStatus": 0,
                   "itemType": "normal",
                   "modifiersList": [
                       {
                           "modifierId": "5ece1917bb45a0c20e14941f",
                           "modifierName": "Test",
                           "modifierQty": 2,
                           "modifierTotalPrice": 4.0,
                           "modifierUnitPrice": 2.0
                       }
                   ],
                   "quantity": 1,
                   "specialRequestList": [],
                   "totalPrice": 10.0,
                   "unitPrice": 10.0,
                   "voidQuantity": 0,
                   "voidStatus": false
               }
           ],*/

    /*   "discountAmount": 0.0,
                   "discountType": "",
                   "discountValue": 0.0,
                   "id": "5f6ddc6427a1ddc07a0da2e4",
                   "itemId": "5f6c61f4231345a28de22b3f",
                   "itemName": "item3-1",
                   "itemStatus": 0,
                   "itemType": "normal",
                    "quantity": 1,
                   "specialRequestList": [],
                   "totalPrice": 10.0,
                   "unitPrice": 10.0,
                   "voidQuantity": 0,
                   "voidStatus": false*/

    val discountAmount: Double? = null
    val discountType: String? = null
    val discountValue: Double? = null
    val id: String? = null
    val itemId: String? = null
    val itemName: String? = null
    val itemStatus: Int? = null
    val itemType: String? = null
    val quantity: Int? = null
    val totalPrice: Double? = null
    val unitPrice: Double? = null
    val voidQuantity: Int? = null
    val voidStatus: Boolean? = null
    val specialRequestList: ArrayList<String>? = null
    val modifiersList: ArrayList<ExistingModifiersList>? = null


}
