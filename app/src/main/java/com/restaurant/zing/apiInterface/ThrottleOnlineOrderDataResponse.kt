package com.restaurant.zing.apiInterface

class ThrottleOnlineOrderDataResponse {
    val responseStatus: Int? = null
    val deliveryDelay: Int? = null
    val takeoutDelay: Int? = null
    val result: String? = null
    val acceptOnlineOrders: String? = null
    val snoozeTime: String? = null
}
