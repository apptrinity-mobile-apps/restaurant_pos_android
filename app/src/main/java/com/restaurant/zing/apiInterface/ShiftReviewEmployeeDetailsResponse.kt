package com.restaurant.zing.apiInterface

class ShiftReviewEmployeeDetailsResponse {
    val date: String? = null
    val employeeName: String? = null
}
