package com.restaurant.zing.apiInterface

class StayOrderItemsResponse {
    var id: String? = null
    var modifiersList: ArrayList<ModifiersDataResponse>? = null
    var name: String? = null
    var quantity: Int? = null
    var removeQuantity: Int? = null
    var removeStatus: Boolean? = null
    var specialRequestList: ArrayList<SpecialRequestDataResponse>? = null
    var status: Int? = null
    var voidQuantity: Int? = null
    var voidStatus: Boolean? = null
}
