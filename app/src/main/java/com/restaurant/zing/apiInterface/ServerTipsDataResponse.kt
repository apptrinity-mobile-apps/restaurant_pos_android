package com.restaurant.zing.apiInterface

class ServerTipsDataResponse {
    val cashBeforeTips: Double? = null
    val cashGratuity: Double? = null
    val creditNonCashGratuity: Double? = null
    val creditNonCashTips: Double? = null
    val totalCash: Double? = null
    val totalTipsAndFeesTippedOut: Double? = null
}
