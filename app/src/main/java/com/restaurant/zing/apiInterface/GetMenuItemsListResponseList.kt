package com.restaurant.zing.apiInterface

class GetMenuItemsListResponseList{

    val basePrice: String? = null
    val id: String? = null
    val name: String? = null
    val discountApplicable: Boolean = false
    val priceProvider: String? = null
    val pricingStrategy: String? = null
    val restaurantId: String? = null
    val sizeList: ArrayList<getSizeList>? = null
    val menuPriceList: ArrayList<getSizeList>? = null
    val timePriceList: ArrayList<getTimePriceList>? = null
    val menuGroupId: String? = null
    val menuId: String? = null

    // Extra QTY
    val qty: Int = 1

    //Extra Status
    val itemStatus: Int = 0
    var allmodifierArray = ArrayList<GetAllModifierListResponseList>()

    // taxes
    val aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>? = null
    val diningOptionTaxException: Boolean? = null
    val diningTaxOption: Boolean? = null
    val taxIncludeOption: Boolean? = null
/*"timePriceList": [
                {
                    "days": [
                        "Friday",
                        "Wednesday",
                        "Tuesday"
                    ],
                    "price": "8",
                    "timeFrom": "14:33",
                    "timeTo": "15:33"
                },
                {
                    "days": [
                        "Sunday"
                    ],
                    "price": "8",
                    "timeFrom": "19:33",
                    "timeTo": "21:33"
                }
            ]
        }’
                  ],*/
}
