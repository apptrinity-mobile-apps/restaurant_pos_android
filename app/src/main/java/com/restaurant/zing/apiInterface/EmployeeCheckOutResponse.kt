package com.restaurant.zing.apiInterface

class EmployeeCheckOutResponse {
    var checkInDurationInSeconds: Int? = null
    var checkOutId: String? = null
    var responseStatus: Int? = null
    var result: String? = null
}
