package com.restaurant.zing.apiInterface

class GetModifierGroupListResponse {
/*{
    "responseStatus": 1,
    "result": [
        {
            "existingMenuGroup": true,
            "id": "5ecff1a24f58bc6962082235",
            "isRequired": 0,
            "maxSelections": 20,
            "minSelections": 1,
            "multipleSelect": true,
            "name": "test",
            "optionsList": [
                {
                    "id": "5ee856936e5c8f2923b16aa3",
                    "optionName": "MenuGRoupItem",
                    "price": 10.0
                }
            ],
            "posName": "fd",
            "pricing": 2
        },
        {
            "existingMenuGroup": false,
            "id": "5ed3e28f4c6b12c6f58f945d",
            "isRequired": 1,
            "maxSelections": 14,
            "minSelections": 1,
            "multipleSelect": true,
            "name": "modifiers",
            "optionsList": [],
            "posName": "pos ",
            "pricing": 2
        }
    ]
}
*/

    val responseStatus:Int?=null
    val result:ArrayList<GetModifierGroupListResponseList>?=null
    val resultMessage:String?=null
}
