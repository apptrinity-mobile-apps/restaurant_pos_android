package com.restaurant.zing.apiInterface

import java.io.Serializable

class TaxTableResponse : Serializable {
    var from: String? = null
    var priceDifference: Double? = null
    var repeat: Boolean? = null
    var taxApplied: String? = null
    var to: String? = null
}
