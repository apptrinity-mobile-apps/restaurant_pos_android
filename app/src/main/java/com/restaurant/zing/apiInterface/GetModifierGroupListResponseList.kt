package com.restaurant.zing.apiInterface

class GetModifierGroupListResponseList (
     existingMenuGroup:String?=null,
     id:String?=null,
     isRequired:Int?=null,
     maxSelections:Int?=null,
     minSelections:Int?=null,
     multipleSelect:Boolean?=null,
     name:String?=null,
     optionsList:ArrayList<OptionsListModel>?=null,
     posName:String?=null,
     pricing:Int?=null
){
    /*{
           "existingMenuGroup": true,
            "id": "5ecff1a24f58bc6962082235",
            "isRequired": 0,
            "maxSelections": 20,
            "minSelections": 1,
            "multipleSelect": true,
            "name": "test",
            "optionsList": [
                {
                    "id": "5ee856936e5c8f2923b16aa3",
                    "optionName": "MenuGRoupItem",
                    "price": 10.0
                }
            ],
            "posName": "fd",
            "pricing": 2
       },*/

    val existingMenuGroup:String?=null
    val id:String?=null
    val isRequired:Int?=null
    val maxSelections:Int?=null
    val minSelections:Int?=null
    val multipleSelect:Boolean?=null
    val name:String?=null
    val optionsList:ArrayList<OptionsListModel>?=null
    val posName:String?=null
    val pricing:Int?=null

}
