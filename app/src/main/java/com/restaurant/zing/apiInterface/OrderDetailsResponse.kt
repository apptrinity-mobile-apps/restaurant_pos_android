package com.restaurant.zing.apiInterface

import java.io.Serializable

class OrderDetailsResponse : Serializable {
    val checkNumber: Int? = null
    val checkUniqueId: String? = null
    val closedOn: String? = null
    val createdBy: String? = null
    val createdByName: String? = null
    val createdOn: String? = null
    val creditsUsed: Double? = null
    val dineInBehaviour: String? = null
    val dueAmount: Double? = null
    val dineInOption: String? = null
    val dineInOptionName: String? = null
    val discountAmount: Double? = null
    val haveCustomer: Boolean? = null
    val id: String? = null
    val restaurantId: String? = null
    val revenueCenterId: String? = null
    val revenueCenterName: String? = null
    val discountLevel: String? = null
    val discountId: String? = null
    val discountName: String? = null
    val discountType: String? = null
    val discountValue: Double? = null
    val serviceAreaId: String? = null
    val orderNumber: Int? = null
    val status: Int? = null
    val subTotal: Double? = null
    val tableNumber: Int? = null
    val taxAmount: Double? = null
    val tipAmount: Double? = null
    val totalAmount: Double? = null
    val receiptNumber: String? = null
    val preparationTime: String? = null
    val toBeDeliveredBy: String? = null
    val sentToKitchenOn: String? = null
    val orderItems: ArrayList<OrderItemsResponse>? = null
    val itemsDetails: ArrayList<OrderItemsResponse>? = null
    val customerDetails: CustomerDataResponse? = null
    val deliveryDetails: DeliveryDetailsDataResponse? = null
    val discountApplicable: Boolean? = null
    val transactionsList:ArrayList<TransactionsReceiptResponse>? = null
    val guestCount:Int?=null
    val noOfChecks:Int?=null
    val originDevice:String?=null
    val recentDevice:String?=null
    val source:String?=null
}