package com.restaurant.zing.apiInterface

import java.io.Serializable

class ServiceAreasListResponse : Serializable {
    var id: String? = null
    var primary: Boolean? = null
    var revenueCenter: String? = null
    var revenueCenterName: String? = null
    var serviceChargeId: String? = null
    var serviceName: String? = null
}