package com.restaurant.zing.apiInterface

class StartingBalanceResponse {
    val id: String? = null
    val startingBalance: Double? = null
}
