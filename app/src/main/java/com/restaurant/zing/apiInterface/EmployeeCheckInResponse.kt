package com.restaurant.zing.apiInterface

class EmployeeCheckInResponse {
    var checkInId: String? = null
    var responseStatus: Int? = null
    var result: String? = null
}
