package com.restaurant.zing.apiInterface

class GiftCardBalanceResponse {

    val giftCard: GiftCardBalance? = null
    val responseStatus: Int? = null
    val result: String? = null
    val mainOrderId: String? = null
}
