package com.restaurant.zing.apiInterface

import java.io.Serializable

class StatesResponse : Serializable {
    var _id: String? = null
    var countryCode: String? = null
    var countryId: Int? = null
    var name: String? = null
    var stateCode: String? = null
    var stateId: Int? = null
    var status: Int? = null
}
