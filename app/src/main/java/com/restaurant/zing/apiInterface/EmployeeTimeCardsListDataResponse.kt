package com.restaurant.zing.apiInterface

class EmployeeTimeCardsListDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val employeeName: String? = null
    val jobTitle: String? = null
    val jobId: String? = null
    val restaurantName: String? = null
    val checkInStatus: Boolean? = null
    val employeeData: ArrayList<EmployeeInOutHoursResponse>? = null
}
