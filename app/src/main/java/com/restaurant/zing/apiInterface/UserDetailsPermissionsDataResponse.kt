package com.restaurant.zing.apiInterface

class UserDetailsPermissionsDataResponse {
    val accountAdminAccess: UserDetailsAccountAdminDataResponse? = null
    val deliveryAccess: UserDetailsDeliveryDataResponse? = null
    val deviceSetupAccess: UserDetailsDeviceSetupDataResponse? = null
    val managerAccess: UserDetailsManagerDataResponse? = null
    val posAccess: UserDetailsPosDataResponse? = null
    val quickEditAccess: UserDetailsQuickEditDataResponse? = null
    val restaurantAdminAccess: UserDetailsRestaurantAdminDataResponse? = null
    val webSetupAccess: UserDetailsWebSetupDataResponse? = null
}
