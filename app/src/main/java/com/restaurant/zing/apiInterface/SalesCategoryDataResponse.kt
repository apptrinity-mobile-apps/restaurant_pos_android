package com.restaurant.zing.apiInterface

class SalesCategoryDataResponse {
    val categoryId: String? = null
    val categoryName: String? = null
    val netSales: Double? = null
    val quantity: Int? = null
}
