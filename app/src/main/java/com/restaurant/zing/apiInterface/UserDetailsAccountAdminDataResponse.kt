package com.restaurant.zing.apiInterface

class UserDetailsAccountAdminDataResponse {
    val dataExportConfig: Boolean? = null
    val financialAccounts: Boolean? = null
    val manageIntegrations: Boolean? = null
    val userPermissions: Boolean? = null
}
