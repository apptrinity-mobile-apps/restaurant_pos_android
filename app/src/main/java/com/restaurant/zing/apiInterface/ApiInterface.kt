package com.restaurant.zing.apiInterface

import com.google.gson.JsonObject
import com.restaurant.zing.helpers.ConstantURLs
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface ApiInterface {

    @Headers("Content-type: application/json")
    @POST("add_customers")
    fun addCustomerApi(@Body body: JsonObject): Call<AddNewCustomerResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_menus")
    fun getRightMenuApi(@Body body: JsonObject): Call<GetRightMenuResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_menu_groups")
    fun getMenuGroupApi(@Body body: JsonObject): Call<GetMenuGroupResponse>

    @Headers("Content-type: application/json")
    @POST("menu_items")
    fun getMenuItemsListApi(@Body body: JsonObject): Call<GetMenuItemsListResponse>

    @Headers("Content-type: application/json")
    @POST("add_order")
    fun addOrderApi(@Body body: JsonObject): Call<AddOrderResponse>

    @Headers("Content-type: application/json")
    @POST("order_guest_notification")
    fun orderGuestNotificationApi(@Body body: JsonObject): Call<AddOrderResponse>

    @Headers("Content-type: application/json")
    @POST("tip_guest_notification")
    fun tipGuestNotificationApi(@Body body: JsonObject): Call<AddOrderResponse>

    @Headers("Content-type: application/json")
    @POST("payment_success_notification")
    fun paymentNotificationApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("update_order")
    fun updateOrderApi(@Body body: JsonObject): Call<AddOrderResponse>

    @Headers("Content-type: application/json")
    @POST("pay_order")
    fun payOrderApi(@Body body: JsonObject): Call<AddOrderResponse>

    /*@Headers("Content-type: application/json")
    @POST("prepaid_order")
    fun PrepaidOrderApi(@Body body: JsonObject): Call<AddOrderResponse>*/

    @Headers("Content-type: application/json")
    @POST("order_no")
    fun getOrderNumberApi(@Body body: JsonObject): Call<AddOrderResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_disounts")
    fun getDiscountsApi(@Body body: JsonObject): Call<AddDiscountResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_disounts_item")
    fun getDiscountsItemApi(@Body body: JsonObject): Call<AddDiscountResponse>

    @Headers("Content-type: application/json")
    @POST("discount_detail_view")
    fun getComboBOGODiscountsApi(@Body body: JsonObject): Call<AddComboBogoDiscountResponse>

    @Headers("Content-type: application/json")
    @POST("discount_detail_view")
    fun getBOGODiscountsApi(@Body body: JsonObject): Call<BoGoResponse>

    @Headers("Content-type: application/json")
    @POST("get_menu_items_on_menu_group")
    fun getDiscountMenuGroupApi(@Body body: JsonObject): Call<DiscountMenuItemsModel>

    @Headers("Content-type: application/json")
    @POST("get_all_dinning_options")
    fun getDinningOptionsListApi(@Body body: JsonObject): Call<GetDinningOptionsListResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_modifier_groups_by_menu_item")
    fun getModifierMenuItemListApi(@Body body: JsonObject): Call<GetModifierGroupListResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_modifiers")
    fun getAllModifierListApi(@Body body: JsonObject): Call<GetAllModifierListResponse>

    @Headers("Content-type: application/json")
    @POST("get_table_order")
    fun getTableOrdersListApi(@Body body: JsonObject): Call<GetOrderDEtailsResponse>

    @Headers("Content-type: application/json")
    @POST("sell_gift_card")
    fun sellGiftCardApi(@Body body: JsonObject): Call<SellGiftCardResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_open_items")
    fun allOpenItemsApi(@Body body: JsonObject): Call<GetAllOpenItemResponse>

    @Headers("Content-type: application/json")
    @POST("gift_card_add_value")
    fun addValueGiftCardApi(@Body body: JsonObject): Call<SellGiftCardResponse>

    @Headers("Content-type: application/json")
    @POST("balance_enquiry")
    fun giftCardBalanceEnqApi(@Body body: JsonObject): Call<GiftCardBalanceResponse>

    @Headers("Content-type: application/json")
    @POST("split_order")
    fun splitOrderSetUpApi(@Body body: JsonObject): Call<GiftCardBalanceResponse>

    @Headers("Content-type: application/json")
    @POST("login")
    fun loginWithPasswordApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("login_passcode")
    fun loginWithPasscodeApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("view_customer")
    fun viewCustomerApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("view_customer_phone")
    fun searchCustomerPhoneApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("customer_name_email")
    fun searchCustomerNameAndEmailApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("add_customer_credits")
    fun addCreditsToCustomer(@Body body: JsonObject): Call<AllDataResponse>

    /*@Headers("Content-type: application/json")
    @POST("view_employee_shift_review")
    fun viewEmployeeShiftReviewApi(@Body body: JsonObject): Call<AllDataResponse>*/

    @Headers("Content-type: application/json")
    @POST("view_detail_employee_shift_review")
    fun viewEmployeeShiftReviewDetailedApi(@Body body: JsonObject): Call<EmployeeShiftReviewDetailedResponse>

    @Headers("Content-type: application/json")
    @POST("view_detail_manager_level_shift_review")
    fun viewEmployeeShiftReviewManagerApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_cash_deposits")
    fun getAllCashDepositsApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("open_checks")
    fun openChecksApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("closed_checks")
    fun closedChecksApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("paid_checks")
    fun paidChecksApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("close_out_day_summary")
    fun closeOutDaySummaryApi(@Body body: JsonObject): Call<CloseOutDayDataResponse>

    @Headers("Content-type: application/json")
    @POST("clock_in_out_employees")
    fun clockInOutEmployeeApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("view_order")
    fun viewOrderApi(@Body body: JsonObject): Call<ViewOrderDataResponse>

    @Headers("Content-type: application/json")
    @POST("view_order_for_payment")
    fun viewOrderPaymentApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("order_details_sent_email")
    fun orderDetailsSentToEmailApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_cash_drawers_lists")
    fun getAllCashDrawersListApi(@Body body: JsonObject): Call<CashDrawersListDataResponse>

    @Headers("Content-type: application/json")
    @POST("add_payout")
    fun addPayOutApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("adjust_starting_balance")
    fun adjustStartingBalanceApi(@Body body: JsonObject): Call<AdjustStartingBalanceDataResponse>

    @Headers("Content-type: application/json")
    @POST("view_cash_transactions")
    fun viewCashTransactionsApi(@Body body: JsonObject): Call<CashTransactionsDataResponse>

    @Headers("Content-type: application/json")
    @POST("add_cash_transactions")
    fun addCashTransactionsApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("close_cash_drawers")
    fun closeCashDrawerApi(@Body body: JsonObject): Call<CashTransactionsCloseDataResponse>

    @Headers("Content-type: application/json")
    @POST("open_cash_drawers")
    fun openCashDrawerApi(@Body body: JsonObject): Call<CashTransactionsOpenDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_throttle_online_orders")
    fun getThrottleOnlineOrdersApi(@Body body: JsonObject): Call<ThrottleOnlineOrderDataResponse>

    @Headers("Content-type: application/json")
    @POST("update_throttle_online_orders")
    fun updateThrottleOnlineOrdersApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_servers")
    fun getAllServersApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("employee_time_card_reports")
    fun getEmployeeTimeCardsApi(@Body body: JsonObject): Call<EmployeeTimeCardsListDataResponse>

    @Headers("Content-type: application/json")
    @POST("show_orders")
    fun getReceivedOrdersApi(@Body body: JsonObject): Call<ReceivedOrderDataResponse>

    @Headers("Content-type: application/json")
    @POST("show_all_day_view")
    fun getAllDayOrdersApi(@Body body: JsonObject): Call<AllDayOrderDataResponse>

    @Headers("Content-type: application/json")
    @POST("show_recently_fullfilled")
    fun getRecentFulFilledOrdersApi(@Body body: JsonObject): Call<ReceivedOrderDataResponse>

    @Headers("Content-type: application/json")
    @POST("fullfill_orders")
    fun fulFilledOrderApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("unfullfill_orders")
    fun unFulFilledOrderApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_unassigned_online_orders")
    fun getDeliveryOnlineOrder(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("tax_rates")
    fun getTaxRates(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("void_item")
    fun voidItemApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("void_mutliple_items")
    fun voidMultipleItemsApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_void_reasons")
    fun voidItemReasonsApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_enroute_delivery_orders")
    fun getEnrouteDelivery(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_delivered_orders")
    fun getDeliveredOrders(@Body body: JsonObject): Call<AllDataResponse>

    /*@Headers("Content-type: application/json")
    @POST("view_order")
    fun getViewOrder(@Body body: JsonObject): Call<ViewOrderDataResponse>*/

    /*@Headers("Content-type: application/json")
    @POST("view_online_order")
    fun getViewOnlineOrder(@Body body: JsonObject): Call<ViewOrderDataResponse>*/

    @Headers("Content-type: application/json")
    @POST("get_all_drivers_list")
    fun getAllDriversList(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("dispatch_delivery_to_driver")
    fun dispatchDeliveryToDriver(@Body body: JsonObject): Call<AllDataResponse>

    /*@Headers("Content-type: application/json")
    @POST("pending_online_order")
    fun pendingOnlineOrders(@Body body: JsonObject): Call<AllDataResponse>*/

    @Headers("Content-type: application/json")
    @POST("approve_pending_online_order")
    fun approvePendingOnlineOrders(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("add_cash_deposits")
    fun addCashDeposit(@Body body: JsonObject): Call<AllDataResponse>

    /*@Headers("Content-type: application/json")
    @POST("future_online_order")
    fun futureOnlineOrders(@Body body: JsonObject): Call<AllDataResponse>*/

    @Headers("Content-type: application/json")
    @POST("get_all_receipt_data")
    fun getAllReceiptData(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_contries")
    fun getAllCountriesApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_states")
    fun getAllStatesApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_cities")
    fun getAllCitiesApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("employee_checkin")
    fun employeeCheckInApi(@Body body: JsonObject): Call<EmployeeCheckInResponse>

    @Headers("Content-type: application/json")
    @POST("employee_checkout")
    fun employeeCheckOutApi(@Body body: JsonObject): Call<EmployeeCheckOutResponse>

    @Headers("Content-type: application/json")
    @POST("logout")
    fun logOutApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("generate_restaurant_otp")
    fun generateRestaurantOtpApi(@Body body: JsonObject): Call<RestaurantOtpResponse>

    @Headers("Content-type: application/json")
    @POST("get_service_areas_pos")
    fun getServiceAreasApi(@Body body: JsonObject): Call<ServiceAreasResponse>

    @Headers("Content-type: application/json")
    @POST("update_recall_order_new")
    fun updateRecallOrderNewApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("find_by_check_number")
    fun findByCheckNumberApi(@Body body: JsonObject): Call<FindChecksDataResponse>

    @Headers("Content-type: application/json")
    @POST("find_by_id")
    fun findByCheckIdApi(@Body body: JsonObject): Call<FindChecksDataResponse>

    @Headers("Content-type: application/json")
    @POST("find_by_customer_info")
    fun findByCustomerApi(@Body body: JsonObject): Call<FindChecksDataResponse>

    @Headers("Content-type: application/json")
    @POST("find_by_card_last4")
    fun findByCardApi(@Body body: JsonObject): Call<FindChecksDataResponse>

    @Headers("Content-type: application/json")
    @POST("advanced_search")
    fun findByAdvancedSearchApi(@Body body: JsonObject): Call<FindChecksDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_stay_orders")
    fun getStayOrdersApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("cancel_stay_orders")
    fun cancelStayOrdersApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("table_service_guest")
    fun updateTableServiceGuestsApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("update_customer_order_info")
    fun updateCustomerInfoForCheckApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_pending_approval_online_orders")
    fun getUnApprovalOrdersApi(@Body body: JsonObject): Call<NeedApprovalOrdersResponse>

    @Headers("Content-type: application/json")
    @POST("get_pending_scheduled_orders")
    fun getScheduledOrdersApi(@Body body: JsonObject): Call<NeedApprovalOrdersResponse>

    @Headers("Content-type: application/json")
    @POST("get_approved_online_orders")
    fun getApprovedOrdersApi(@Body body: JsonObject): Call<NeedApprovalOrdersResponse>

    @Headers("Content-type: application/json")
    @POST("confirm_scheduled_order")
    fun confirmScheduledOrderApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("cancel_scheduled_order")
    fun cancelScheduledOrderApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_assigned_online_orders")
    fun getDeliveryAssignedOrdersApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("complete_delivery")
    fun completeDeliveryApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("pickup_order")
    fun pickupApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("cancel_dispatch_order")
    fun cancelDispatchApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("giftcard_balance_transfer")
    fun giftCardBalanceTransferApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_customers_address")
    fun getCustomerAddressApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_emp_ids")
    fun getAllEmployeesIdApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("giftcard_otp")
    fun sendGiftCardOtpApi(@Body body: JsonObject): Call<RestaurantOtpResponse>

    @Headers("Content-type: application/json")
    @POST("customer_credits_history")
    fun getCustomerCreditsHistoryApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("restaurant_mode")
    fun getRestaurantModeApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("enable_test_mode")
    fun enableTestModeApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("add_active_cash_drawers")
    fun addActiveCashDrawerApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("get_prep_stations_list")
    fun getAllPrepStationsApi(@Body body: JsonObject): Call<AllDataResponse>

    @Headers("Content-type: application/json")
    @POST("fetch_customer_bonus_points")
    fun customerBonusPointsApi(@Body body: JsonObject): Call<CustomerBonusResponse>

    @Headers("Content-type: application/json")
    @POST("fetch_customer_bonus_transactions_list")
    fun customerBonusPointsTransactionsApi(@Body body: JsonObject): Call<CustomerBonusResponse>

    @Headers("Content-type: application/json")
    @POST("check_existing_device_name")
    fun checkDeviceNameApi(@Body body: JsonObject): Call<AllDataResponse>

    companion object Factory {
        /* staging */
//        private val BASE_URL = "http://3.14.236.148/api/pos/"
//        private val BASE_URL = "http://3.138.149.52/api/pos/"
        private val BASE_URL = "${ConstantURLs.BASE_URL}/api/pos/"

        /* live */
//        private val BASE_URL = ""
        private val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

}