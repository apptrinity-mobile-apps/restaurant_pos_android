package com.restaurant.zing.apiInterface

import java.io.Serializable

class ServiceAreasResponse : Serializable {
    var responseStatus: Int? = null
    var result: String? = null
    var service_areasList: ArrayList<ServiceAreasListResponse>? = null
}