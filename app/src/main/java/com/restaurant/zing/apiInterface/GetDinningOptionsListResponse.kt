package com.restaurant.zing.apiInterface

class GetDinningOptionsListResponse {
/*{
   "dinningOptionsList": [
        {
            "behavior": "curbside",
            "id": "5ec75477df75cbd8514590c2",
            "optionName": "TakeOut1"
        },
        {
            "behavior": "curbside",
            "id": "5ee9eddeef8788744983a225",
            "optionName": "OnlineOrderin"
        },
        {
            "behavior": "delivery",
            "id": "5ee9ee87ef8788744983a226",
            "optionName": "Delivery"
        },
        {
            "behavior": "dine_in",
            "id": "5f69b297d7c855443fc6fb3d",
            "optionName": "take_out"
        },
        {
            "behavior": "take_out",
            "id": "5f742842cdf5948e938be418",
            "optionName": "takeout"
        },
        {
            "behavior": "curbside",
            "id": "5f9940ca9867a757f03afe39",
            "optionName": "take_out"
        },
        {
            "behavior": "curbside",
            "id": "5f9fd002313f61ef8013e91c",
            "optionName": "Delivery"
        },
        {
            "behavior": "take_out",
            "id": "5f9fd002313f61ef8013e91d",
            "optionName": "Delivery"
        }
    ],
    "responseStatus": 1,
    "result": "Dinning Options data fetched Successfully!!"
}

*/

    val dinningOptionsList:ArrayList<GetDinningOptionsListResponseList>?=null
    val responseStatus:Int?=null
    val result:String?=null
}
