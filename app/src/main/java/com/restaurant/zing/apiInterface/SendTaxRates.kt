package com.restaurant.zing.apiInterface

import java.io.Serializable

class SendTaxRates(
    var default: Boolean,
    var importId: String,
    var status: Int,
    var taxName: String,
    var taxRate: Double,
    var taxType: String,
    var taxTypeId: Int,
    var taxTable: ArrayList<TaxTableResponse>,
    var roundingOptions: String,
    var roundingOptionId: Int,
    var taxid: String,
    var uniqueNumber: Long,
    var taxAmount: Double
) : Serializable