package com.restaurant.zing.apiInterface

class ShiftReviewCurrentStatusDataResponse {
    //val cashTipsNotDeclared: Boolean? = null
    val closedChecks: Int? = null
    val unclosedChecks: Int? = null
    val paidChecks: Int? = null
    val unpaidChecks: Int? = null
}
