package com.restaurant.zing.apiInterface

class UserDetailsDataResponse {
    val currencyId: String? = null
    val currencySymbol: String? = null
    val empId: String? = null
    val firstName: String? = null
    val id: String? = null
    val jobId: String? = null
    val lastName: String? = null
    val profilePic: String? = null
    val restaurantId: String? = null
    val permissionList: UserDetailsPermissionsDataResponse? = null
}
