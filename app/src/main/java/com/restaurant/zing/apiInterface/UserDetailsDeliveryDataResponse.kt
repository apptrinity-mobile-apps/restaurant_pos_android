package com.restaurant.zing.apiInterface

class UserDetailsDeliveryDataResponse {
    val cancelDispatch: Boolean? = null
    val completeDelivery: Boolean? = null
    val deliveryMode: Boolean? = null
    val dispatchDriver: Boolean? = null
    val updateAllDeliveryOrders: Boolean? = null
    val updateDriver: Boolean? = null
}
