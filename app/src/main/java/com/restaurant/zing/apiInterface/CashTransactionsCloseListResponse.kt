package com.restaurant.zing.apiInterface

class CashTransactionsCloseListResponse {
    var user: String? = null
    var id: String? = null
    var date: String? = null
    var amount: String? = null
    var action: String? = null
}
