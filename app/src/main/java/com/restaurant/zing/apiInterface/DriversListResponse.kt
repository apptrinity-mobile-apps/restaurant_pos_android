package com.restaurant.zing.apiInterface

class DriversListResponse {


    /*"driverAvailable": true,
    "empEmail": "suri9",
    "empId": "99",
    "firstName": null,
    "id": "5f5ee7973d87073b2254d3f0",
    "lastName": null,
    "phoneNumber": null*/

    val driverAvailable: Boolean? = null
    val empEmail: String? = null
    val empId: String? = null
    val firstName: String? = null
    val id: String? = null
    val lastName: String? = null
    val phoneNumber: String? = null
}
