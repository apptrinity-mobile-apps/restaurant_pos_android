package com.restaurant.zing.apiInterface

class GetAllPreModifierListResponseList {
    val POSName: String? = null
    val color: String? = null
    val displayMode: String? = null
    val fixedPrice: Double? = null
    val id: String? = null
    val kitchenName: String? = null
    val multiplicationFactor: Double? = null
    val multiplyPrice: Boolean? = null
    val name: String? = null
}
