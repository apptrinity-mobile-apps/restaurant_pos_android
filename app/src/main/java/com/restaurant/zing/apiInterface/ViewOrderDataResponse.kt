package com.restaurant.zing.apiInterface

class ViewOrderDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val orderDetails: OrderDetailsResponse? = null
}
