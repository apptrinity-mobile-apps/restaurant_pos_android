package com.restaurant.zing.apiInterface

class EmployeeInOutHoursDataResponse {
    var checkInStatus: Boolean? = null
    var checkInTime: String? = null
    var checkOutTime: String? = null
    var employeeId: String? = null
    var employeeName: String? = null
    var hours: String? = null
    var jobId: String? = null
    var jobTitle: String? = null
}
