package com.restaurant.zing.apiInterface

class TotalRemovalsDataResponse {
    val removalAmount: Double? = null
    val removedItemCount: Int? = null
}
