package com.restaurant.zing.apiInterface

class CustomerBonusPointsDataResponse {
    val restarant_available_points: Double? = null
    val restaurant_credit_points: Double? = null
    val restaurant_debit_points: Double? = null
    val total_available_points: Double? = null
    val total_credit_points: Double? = null
    val total_debit_points: Double? = null
}