package com.restaurant.zing.apiInterface

class CashTransactionsCloseDataResponse {
    val result: String? = null
    val responseStatus: Int? = null
    val cashDrawersList: ArrayList<CashTransactionsCloseListResponse>? = null
    val close: Boolean? = null
}
