package com.restaurant.zing.apiInterface

class DeliveryDataResponse {
    val amount: Double? = null
    val qua: Int? = null
    val type: String? = null
}
