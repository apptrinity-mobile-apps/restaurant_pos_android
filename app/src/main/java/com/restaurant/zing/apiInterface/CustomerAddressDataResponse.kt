package com.restaurant.zing.apiInterface

class CustomerAddressDataResponse {
    val fullAddress: String? = null
    val createdOn: String? = null
    val customerId: String? = null
    val id: String? = null
    val latitude: String? = null
    val longitude: String? = null
    val restaurantId: String? = null
    val status: Int? = null
}
