package com.restaurant.zing.apiInterface

class ShiftReviewSalesAndTaxesSummaryDataResponse {
    val cashOnHand: Double? = null
    val gratuity: Double? = null
    val janeOwesHouse: Double? = null
    val nonCashTips: Double? = null
}
