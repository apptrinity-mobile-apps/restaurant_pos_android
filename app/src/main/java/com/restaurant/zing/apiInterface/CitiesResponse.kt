package com.restaurant.zing.apiInterface

import java.io.Serializable

class CitiesResponse : Serializable {
    var _id: String? = null
    var cityId: Int? = null
    var countryCode: String? = null
    var countryId: Int? = null
    var latitude: String? = null
    var longitude: String? = null
    var name: String? = null
    var stateCode: String? = null
    var stateId: Int? = null
    var status: Int? = null
}
