package com.restaurant.zing.apiInterface

class EmployeeShiftReviewDetailedResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val employeeDetails: ShiftReviewEmployeeDetailsResponse? = null
    val currentStatus: ShiftReviewCurrentStatusDataResponse? = null
    val salesAndTaxesSummary: ShiftReviewSalesAndTaxesSummaryDataResponse? = null
    val tipSummary: ShiftReviewTipsSummaryDataResponse? = null
}
