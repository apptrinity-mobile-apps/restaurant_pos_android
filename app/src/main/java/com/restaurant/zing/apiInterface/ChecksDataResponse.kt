package com.restaurant.zing.apiInterface

class ChecksDataResponse {
    val amount: Double? = null
    val checkNumber: String? = null
    val closed: String? = null
    val createdOn: String? = null
//    val dineInOption: String? = null
    val dineInOptionId: String? = null
    val dineInOptionName: String? = null
    val orderId: String? = null
    val orderNumber: Int? = null
    val status: Int? = null
    val tableNumber: Int? = null
    val recallRef: String? = null
    val isRecallOrder: Boolean? = null
    val itemNames: ArrayList<ItemNameDiscountDataResponse>? = null
}
