package com.restaurant.zing.apiInterface

import java.io.Serializable

class GetAllModifierListResponseList(
    val id: String,
    val name: String,
    var price: Double,
    var total: Double,
    val modifierGroupId: ArrayList<modifierIdList>
) :Serializable
