package com.restaurant.zing.apiInterface

class TotalVoidsDataResponse {
    val voidAmount: Double? = null
    val voidItemCount: Int? = null
    val voidOrderCount: Int? = null
    val voidPercent: Double? = null
}
