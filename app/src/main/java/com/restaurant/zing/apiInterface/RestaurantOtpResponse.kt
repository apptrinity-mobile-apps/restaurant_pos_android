package com.restaurant.zing.apiInterface

import java.io.Serializable

class RestaurantOtpResponse : Serializable {
    var OTP: String? = null
    var otp: String? = null
    var responseStatus: Int? = null
    var result: String? = null
    var restaurantId: String? = null
    var restaurantName: String? = null
}
