package com.restaurant.zing.apiInterface

class AdjustStartingBalanceDataResponse {
    val result: String? = null
    val responseStatus: Int? = null
    val Balance: Double? = null
    val closeOutBalance: Double? = null
    val startingBalance: Double? = null
}
