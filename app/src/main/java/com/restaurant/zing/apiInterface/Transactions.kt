package com.restaurant.zing.apiInterface

data class Transactions(
    val amount: Double,
    val cardType: String,
    val cashDrawerId: String,
    val cashDrawerName: String,
    val date: String,
    val giftCardMessage: Any,
    val giftCardNumber: Long,
    val gratuityAmount: Double,
    val paymentType: String,
    val refundAmount: Double,
    val staus: String,
    val tipAmount: Double,
    val totalAmount: Double
)