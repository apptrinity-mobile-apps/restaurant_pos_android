package com.restaurant.zing.apiInterface

import java.io.Serializable

class DeliveryOnlineDataResponse : Serializable {
    val checkNumber: String? = null
    val deliveryStatus: String? = null
    val dineInOption: String? = null
    val dineInBehaviour: String? = null
    val dineInOptionName: String? = null
    val discountAmount: String? = null
    val discountValue: Double? = null
    val id: String? = null
    val itemId: String? = null
    val driverId: String? = null
    val orderDate: String? = null
    val orderTime: String? = null
    val itemName: String? = null
    val orderNumber: String? = null
    val posOrderId: String? = null
    val quantity: String? = null
    val status: Int? = null
    val subTotal: String? = null
    val tableNumber: String? = null
    val taxAmount: Double? = null
    val totalAmount: Double? = null
    val tipAmount: Double? = null
    val totalPrice: String? = null
    val unitPrice: String? = null
    val items_list: ArrayList<UnAssignedItemListResponse>? = null
    val paymentStatus: Int? = null
}
