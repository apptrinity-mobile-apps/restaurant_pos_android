package com.restaurant.zing.apiInterface

import java.io.Serializable

class GetAllDiscountsListResponse(
    val allowOtherDiscounts: Boolean,
    val appliesTo: Int,
    val discountValueType: String,
    val exceptDiscountItems: ArrayList<DiscountItems>,
    val id: String,
    val includeDiscountItems: ArrayList<DiscountItems>,
    val maxDiscountAmount: Double,
    val minDiscountAmount: Double,
    val name: String,
    val orderValue: String,
    val status: Int,
    val type: String,
    val uniqueNumber: String,
    val value: Double
) : Serializable
