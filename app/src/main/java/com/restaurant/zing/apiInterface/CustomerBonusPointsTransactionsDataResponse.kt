package com.restaurant.zing.apiInterface

class CustomerBonusPointsTransactionsDataResponse {
    val createdOn : String? = null
    val credits : Int? = null
    val creditsReason : String? = null
    val customerId : String? = null
    val id : String? = null
    val preCardNo : Long? = null
    val restaurantId : String? = null
    val restaurantName : String? = null
    val transactionType : String? = null
}