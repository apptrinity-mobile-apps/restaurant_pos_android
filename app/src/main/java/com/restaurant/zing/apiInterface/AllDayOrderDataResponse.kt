package com.restaurant.zing.apiInterface

class AllDayOrderDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val orders: ArrayList<OrderedItemsResponse>? = null
}
