package com.restaurant.zing.apiInterface

class GetAllOpenItemList {
/* {
            "id": "5f9f9340f6b86fa98056175e",
            "name": "naive"
        }*/

    var id: String? = null
    var name: String? = null

    // taxes
    var aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>? = null
    var diningOptionTaxException: Boolean? = null
    var diningTaxOption: Boolean? = null
    var taxIncludeOption: Boolean? = null
}
