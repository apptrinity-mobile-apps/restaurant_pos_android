package com.restaurant.zing.apiInterface

class PostOrderData (
    val itemId:String,
    val menuGroupId:String,
    val itemName:String,
    val quantity:Int,
    val unitPrice:Double,
    val totalPrice:Double,
    val specialRequestList:ArrayList<GetSpecialReq>,
    val modifiersList:ArrayList<GetModifierListReq>,
    val itemType:String,
    val discountId:String,
    val discountName:String,
    val discountType:String,
    val discountValue:String,
    val discountAmount:Double
)