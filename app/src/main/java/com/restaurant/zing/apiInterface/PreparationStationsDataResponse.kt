package com.restaurant.zing.apiInterface

class PreparationStationsDataResponse {
    var id: String? = null
    var stationName: String? = null
    var uniqueNumber: String? = null
}
