package com.restaurant.zing.apiInterface

class AddDiscountResponse {

    val responseStatus:Int?=null
    val result:String?=null
//    val discounts:ArrayList<GetAllDiscountsListResponse>?=null
    val discounts:ArrayList<DiscountsListResponse>?=null
}
