package com.restaurant.zing.apiInterface

class EmployeeInOutHoursResponse {
    val checkInStatus: Boolean? = null
    val checkInTime: String? = null
    val checkOutTime: String? = null
    val date: String? = null
    val day_for_sorting: String? = null
    val hours: String? = null
    val jobId: String? = null
    val jobTitle: String? = null
    val emp_checkinout_data: ArrayList<EmployeeInOutHoursDataResponse>? = null
}
