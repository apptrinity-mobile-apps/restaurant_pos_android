package com.restaurant.zing.apiInterface

class SalesAndTaxesSummaryDataResponse {
    val tax: Double? = null
    val totalNetSales: Double? = null
    val totalSales: Double? = null
}
