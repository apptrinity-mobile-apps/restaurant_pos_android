package com.restaurant.zing.apiInterface

class ShiftReviewTipsSummaryDataResponse {
    val cashBeforeTips: Double? = null
    val creditNonCashGratuity: Double? = null
    val cashGratuity: Double? = null
    val creditNonCashTips: Double? = null
    val totalCash: Double? = null
    val totalTipsAndFeesTippedOut: Double? = null
}
