package com.restaurant.zing.apiInterface

import java.io.Serializable

class CustomerDataResponse : Serializable {
    val createdBy: String? = null
    val createdOn: String? = null
    val credits: Double? = null
    val email: String? = null
    val firstName: String? = null
    val id: String? = null
    val lastName: String? = null
    val phoneNumber: String? = null
    val preCardNo: Long? = null
    val restaurantId: String? = null
}
