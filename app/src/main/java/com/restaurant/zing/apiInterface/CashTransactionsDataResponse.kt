package com.restaurant.zing.apiInterface

class CashTransactionsDataResponse {
    val closeOutBalance: Double? = null
    val startingBalance: Double? = null
    val Balance: Double? = null
    val result: String? = null
    val responseStatus: Int? = null
    val cashdrawers: ArrayList<CashTransactionsListResponse>? = null
}
