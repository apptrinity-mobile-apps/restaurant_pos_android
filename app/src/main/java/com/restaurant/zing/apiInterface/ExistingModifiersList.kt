package com.restaurant.zing.apiInterface

class ExistingModifiersList {
/* "modifierId": "5ece1917bb45a0c20e14941f",
        "modifierName": "Test",
        "modifierQty": 2,
        "modifierTotalPrice": 4.0,
        "modifierUnitPrice": 2.0*/

    val modifierId:String?=null
    val modifierName:String?=null
    val modifierQty:Int?=null
    val modifierTotalPrice:Double?=null
    val modifierUnitPrice:Double?=null
}
