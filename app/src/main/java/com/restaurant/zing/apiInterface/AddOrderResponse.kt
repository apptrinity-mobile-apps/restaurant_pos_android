package com.restaurant.zing.apiInterface

class AddOrderResponse {
/*{
   "responseStatus": 1,
   "result": "Order Items saved successfully!!!"
    "orderId": "5fd861ba325307cc394801df"
}
*/

    val responseStatus:Int?=null
    val result:String?=null
    val orderId:String?=null
    val orderNo:Int?=null


}
