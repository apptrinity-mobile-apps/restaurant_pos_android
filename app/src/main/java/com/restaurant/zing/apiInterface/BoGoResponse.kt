package com.restaurant.zing.apiInterface

data class BoGoResponse(
    val bogoBuyItems: List<List<BogoBuyItem>>,
    val bogoGetItems: List<BogoGetItem>,
    val buyDiscountValue: Int,
    val discountApplyType: Int,
    val discountValue: Any,
    val discountValueType: String,
    val getDiscountValue: Int,
    val responseStatus: Int,
    val result: String
)

/* bogodiscouuntApplyType:1
(of first eligible item)
bogodiscouuntApplyType:2
(of least expensive eligible item)
bogodiscouuntApplyType:3
(of most expensive eligible item)*/

data class BogoBuyItem(
    val bogoId: String,
    val basePrice: Double,
    val discountAmount: Int,
    val discountId: String,
    val discountName: String,
    val discountType: String,
    val discountValue: Int,
    val id: String,
    val isBogo: Boolean,
    val isCombo: Boolean,
    val discountApplicable: Boolean,
    val itemId: String,
    val itemName: String,
    val itemType: String,
    val menuGroupId: String,
    val menuId: String,
    val modifiersList: ArrayList<GetAllModifierListResponseList>,
    val name: String,
    val pricingStrategy: Int,
    val quantity: Int,
    val specialRequestList: ArrayList<SpecialRequestDataResponse>? = null,
    val timePriceList: ArrayList<TimePrice>? = null,
    val sizeList: ArrayList<getSizeList>? = null,
    val menuPriceList: ArrayList<getSizeList>? = null,
    val totalPrice: Int,
    val type: String,
    val uniqueNumber: String,
    val unitPrice: Int,
// taxes
    val taxesList: ArrayList<TaxRatesDetailsResponse>? = null,
    val diningOptionTaxException: Boolean,
    val diningTaxOption: Boolean,
    val taxIncludeOption: Boolean
)

data class BogoGetItem(
    val bogoId: String,
    val basePrice: Double,
    val discountAmount: Int,
    val discountId: String,
    val discountName: String,
    val discountType: String,
    val discountValue: Int,
    val id: String,
    val isBogo: Boolean,
    val isCombo: Boolean,
    val discountApplicable: Boolean,
    val itemId: String,
    val itemName: String,
    val itemType: String,
    val menuGroupId: String,
    val menuId: String,
    val modifiersList: ArrayList<GetAllModifierListResponseList>,
    val name: String,
    val pricingStrategy: Int,
    val quantity: Int,
    val specialRequestList: ArrayList<SpecialRequestDataResponse>? = null,
    val timePriceList: ArrayList<TimePrice>? = null,
    val sizeList: ArrayList<getSizeList>? = null,
    val menuPriceList: ArrayList<getSizeList>? = null,
    val totalPrice: Int,
    val type: String,
    val uniqueNumber: String,
    val unitPrice: Int,
// taxes
    val taxesList: ArrayList<TaxRatesDetailsResponse>? = null,
    val diningOptionTaxException: Boolean,
    val diningTaxOption: Boolean,
    val taxIncludeOption: Boolean
)