package com.restaurant.zing.apiInterface

class CashTransactionsListResponse {
    val User: String? = null
    val action: String? = null
    val amount: Double? = null
    val createdOn: String? = null
    val comment: String? = null
    val id: String? = null
    val serverId: String? = null
    val transactionType: String? = null
}
