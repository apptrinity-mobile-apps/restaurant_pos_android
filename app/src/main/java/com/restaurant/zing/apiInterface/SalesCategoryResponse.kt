package com.restaurant.zing.apiInterface

class SalesCategoryResponse {
    val salesCategories: ArrayList<SalesCategoryDataResponse>? = null
    val salesCategoriesTotalNetSales: Double? = null
}
