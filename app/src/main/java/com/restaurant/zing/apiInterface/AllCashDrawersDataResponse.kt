package com.restaurant.zing.apiInterface

class AllCashDrawersDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val cashdrawers: ArrayList<AllCashDrawersListResponse>? = null
}
