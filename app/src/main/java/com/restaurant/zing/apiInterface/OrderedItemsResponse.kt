package com.restaurant.zing.apiInterface

class OrderedItemsResponse {
    var id: String? = null
    var name: String? = null
    var quantity: Int? = null
    var quntity: Int? = null
    var status: Int? = null
    var preparationTime: Long? = null
    var removeQuantity: Int? = null
    var removeStatus: Boolean? = null
    var voidStatus: Boolean? = null
    var voidQuantity: Int? = null
    var specialInstructions: String? = null
    var modifiersList: ArrayList<ModifiersDataResponse>? = null
    var specialRequestList: ArrayList<SpecialRequestDataResponse>? = null
}
