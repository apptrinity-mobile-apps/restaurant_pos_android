package com.restaurant.zing.apiInterface

class CurrentStatusDataResponse {
    val actualDeposits: Int? = null
    val clockedInEmployee: Int? = null
    val paymentsCapture: Int? = null
    val unclosedCashDrawers: Int? = null
    val unclosedChecks: Int? = null
    val unpaidChecks: Int? = null
    val paidChecks: Int? = null
}
