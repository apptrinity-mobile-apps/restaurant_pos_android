package com.restaurant.zing.apiInterface

class ReceiptSetUpDataResponse {


    val closedcheckRecipet: String? = null
    val charityTipMsg: String? = null
    val combineItems: String? = null
    val createdBy: String? = null
    val createdOn: String? = null
    val creditCardRecipets: String? = null
    val displayCharityTip: String? = null
    val displayDiscounts: String? = null
    val displayItemQTY: String? = null
    val displayTipLine: String? = null
    val displayTipPercent: String? = null
    val displayTipPercentList: ArrayList<DisplayTipPercentList>? = null
    val freeItems: String? = null
    val freeModifiers: String? = null
    val guestCount: String? = null
    val guestDetailsDelivery: String? = null
    val id: String? = null
    val receiptLogo: String? = null
    val itemModifiers: String? = null
    val hideModifierPrices: String? = null
    val itemizeQuickOrder: String? = null
    val itemizeTableService: String? = null
    val largeOrderNum: String? = null
    val printPayments: String? = null
    val promoArea: String? = null
    val receiptFooter: String? = null
    val receiptHeader: String? = null
    val restaurantId: String? = null
    val serviceChargesFont: String? = null
    val showHouseAcccounts: String? = null
    val specialRequests: String? = null
    val status: String? = null
    val surveyCode: String? = null
    val tabName: String? = null
    val tipWithGrauities: String? = null
    val uniqueNumber: String? = null




}
