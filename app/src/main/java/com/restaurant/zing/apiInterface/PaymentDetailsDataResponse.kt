package com.restaurant.zing.apiInterface

class PaymentDetailsDataResponse {
    val cash: Double? = null
    val credits: Double? = null
    val totalPayments: Double? = null
    val totalSales: Double? = null
}
