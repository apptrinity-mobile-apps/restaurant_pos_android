package com.restaurant.zing.apiInterface

class OrderchecksDetails {

    /* "checkNumber": 2,
           "closedOn": null,
           "createdBy": "5ec629ad0ae0b6043743eab9",
           "createdByName": "Poorna Chand",
           "createdOn": "Fri, 16 Oct 2020 12:59:45 GMT",
           "dineInBehaviour": "dine_in",
           "dineInOption": "5f69b297d7c855443fc6fb3d",
           "dineInOptionName": "test123",
           "discountAmount": 0.0,
           "haveCustomer": false,
           "id": "5f8999411b673a6054b96a66",
           "itemsDetails": [
               {
                   "discountAmount": 0.0,
                   "discountType": "",
                   "discountValue": 0.0,
                   "id": "5f6ddc6427a1ddc07a0da2e4",
                   "itemId": "5f6c61f4231345a28de22b3f",
                   "itemName": "item3-1",
                   "itemStatus": 0,
                   "itemType": "normal",
                   "modifiersList": [
                       {
                           "modifierId": "5ece1917bb45a0c20e14941f",
                           "modifierName": "Test",
                           "modifierQty": 2,
                           "modifierTotalPrice": 4.0,
                           "modifierUnitPrice": 2.0
                       }
                   ],
                   "quantity": 1,
                   "specialRequestList": [],
                   "totalPrice": 10.0,
                   "unitPrice": 10.0,
                   "voidQuantity": 0,
                   "voidStatus": false
               }
           ],
           "orderNumber": 1,
           "status": 1,
           "subTotal": 14.0,
           "tableNumber": 3,
           "taxAmount": 0.6,
           "tipAmount": 1.5,
           "totalAmount": 14.6
       }*/
    val checkNumber:Int?=null
    val closedOn:String?=null
    val createdBy:String?=null
    val createdByName:String?=null
    val createdOn:String?=null
    val dineInBehaviour:String?=null
    val dineInOption:String?=null
    val dineInOptionName:String?=null
    val discountAmount:String?=null
    val haveCustomer:String?=null
    val id:String?=null
    val orderNumber:Int?=null
    val status:Int?=null
    val subTotal:Double?=null
    val tableNumber:Int?=null
    val taxAmount:Double?=null
    val tipAmount:Double?=null
    val totalAmount:String?=null
    val itemsDetails:ArrayList<ItemsDetailList>?=null

}
