package com.restaurant.zing.apiInterface

import java.io.Serializable

class OrdersCustomData(
    var basePrice: String,
    var discountApplicable: Boolean,
    val id: String,
    val name: String,
    val itemType: String,
    var qty: String,
    val total: String,
    val discount: String,
    val restaurantId: String,
    val allmodifierArray: ArrayList<GetAllModifierListResponseList>,
    val itemId: String,
    val menuGroupId: String,
    val menuId: String,
    val discountApplied: Boolean = false,
    var discountAppliedType: String = "",
    val discountsArray: ArrayList<GetAllDiscountsListResponse>,
    val itemStatus: Int,
    var isCombo: Boolean = false,
    var isBogo: Boolean = false,
    var comboOrBogoOfferId: String = "", //combo or bogo id only if combo or bogo is selected
    var comboOrBogoUniqueId: String = "", // unique id for each combo or bogo group
    var voidStatus: Boolean,
    var voidQuantity: Int,
    // taxes
    val aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>,
    val taxAppliedList: ArrayList<SendTaxRates>,
    val diningOptionTaxException: Boolean,
    val diningTaxOption: Boolean,
    val taxIncludeOption: Boolean
) : Serializable