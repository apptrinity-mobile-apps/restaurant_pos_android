package com.restaurant.zing.apiInterface

data class MatchedOrders(
    val checkNumber: Int,
    val checkUniqueId: String,
    val createdOn: String,
    val orderId: String,
    val orderNumber: Int,
    val orderUniqueId: String,
    val paymentStatus: Int,
    val subTotal: Double,
    val tipAmount: Double,
    val transactionsList: List<Transactions>
)