package com.restaurant.zing.apiInterface

data class DiscountMenuItemsModel(
    val menuItems: ArrayList<MenuItem>,
    val responseStatus: Int,
    val result: String
)

data class MenuItem(
    val basePrice: Double,
    val discountAmount: Int,
    val discountId: String,
    val discountName: String,
    val discountType: String,
    val discountValue: Int,
    val isBogo: Boolean,
    val isCombo: Boolean,
    val discountApplicable: Boolean,
    val itemId: String? = null,
    val itemName: String,
    val itemType: String,
    val menuGroupId: String,
    val menuId: String,
    val modifiersList: ArrayList<GetAllModifierListResponseList>? = null,
    val pricingStrategy: Int,
    val quantity: Int,
    val sizeList: ArrayList<getSizeList>? = null,
    val specialRequestList: ArrayList<SpecialRequestDataResponse>? = null,
    val timePriceList: ArrayList<TimePrice>? = null,
    val menuPriceList: ArrayList<getSizeList>? = null,
    val totalPrice: Int,
    val type: String,
    val unitPrice: Int,
// taxes
    val taxesList: ArrayList<TaxRatesDetailsResponse>? = null,
    val diningOptionTaxException: Boolean,
    val diningTaxOption: Boolean,
    val taxIncludeOption: Boolean
)
