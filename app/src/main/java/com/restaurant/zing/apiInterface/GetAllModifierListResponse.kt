package com.restaurant.zing.apiInterface

class GetAllModifierListResponse {

    val responseStatus: Int? = null
    val result: String? = null
    val modifierDetails: ArrayList<GetAllModifierListResponseList>? = null
    val preModifiers: ArrayList<GetAllPreModifierListResponseList>? = null
}
