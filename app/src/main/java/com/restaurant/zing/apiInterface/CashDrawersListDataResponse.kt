package com.restaurant.zing.apiInterface

class CashDrawersListDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val cashdrawersOpenList: ArrayList<CashDrawersDataResponse>? = null
    val cashdrawersClosedList: ArrayList<CashDrawersDataResponse>? = null
    val cashdrawersActiveList: ArrayList<CashDrawersDataResponse>? = null
}
