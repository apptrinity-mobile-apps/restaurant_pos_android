package com.restaurant.zing.apiInterface

import java.io.Serializable

class NeedApprovalOrdersResponse : Serializable {
    var result: String? = null
    var responseStatus: Int? = null
    var orders: ArrayList<NeedApprovalOrdersItemsResponse>? = null
}
