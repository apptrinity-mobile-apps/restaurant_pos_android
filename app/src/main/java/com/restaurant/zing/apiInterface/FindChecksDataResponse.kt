package com.restaurant.zing.apiInterface

data class FindChecksDataResponse(
    val matchedOrdersList: List<MatchedOrders>,
    val responseStatus: Int,
    val result: String
)