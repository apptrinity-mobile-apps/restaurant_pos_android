package com.restaurant.zing.apiInterface

class EmployeeClockInOutDataResponse {
    val firstName: String? = null
    val id: String? = null
    val lastName: String? = null
    val loggedIn: Boolean? = null
}
