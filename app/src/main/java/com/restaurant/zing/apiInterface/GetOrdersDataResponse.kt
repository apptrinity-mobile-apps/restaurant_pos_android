package com.restaurant.zing.apiInterface

class GetOrdersDataResponse {
    var checkNumber: Int? = null
    var created: String? = null
    var fullfilledON: String? = null
    var name: String? = null
    var orderId: String? = null
    var orderNumber: Int? = null
    var status: Int? = null
    var tableNumber: Int? = null
    var recallRef: String? = null
    var placedFrom: String? = null
    var deviceName: String? = null
    var deviceId: String? = null
    var isRecallOrder: Boolean? = null
    var items: ArrayList<OrderedItemsResponse>? = null
}
