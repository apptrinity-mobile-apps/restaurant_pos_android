package com.restaurant.zing.apiInterface

import java.io.Serializable

class NeedApprovalOrdersItemsResponse : Serializable {
    var checkNumber: Int? = null
    var createdOn: String? = null
    var dineInBehaviour: String? = null
    var dineInOptionId: String? = null
    var dineInOptionName: String? = null
    var isRecallOrder: Boolean? = null
    var name: String? = null
    var orderId: String? = null
    var orderNumber: Int? = null
    var processStartTime: String? = null
    var recallRef: String? = null
    var restaurantId: String? = null
    var scheduledEndTime: String? = null
    var scheduledStartTime: String? = null
    var status: Int? = null
    var tableNumber: Int? = null
    var totalAmount: Double? = null
    var items: ArrayList<OrderItemsResponse>? = null
    val paymentStatus: Int? = null
}
