package com.restaurant.zing.apiInterface

class CustomerCreditsHistoryResponse {
    var createdOn: String? = null
    var credits: Double? = null
    var creditsReason: String? = null
    var customerId: String? = null
    var employeeId: String? = null
    var employeeName: String? = null
    var id: String? = null
    var preCardNo: Long? = null
    var restaurantId: String? = null
    var restaurantName: String? = null
    var status: Int? = null
    var transactionType: String? = null
}