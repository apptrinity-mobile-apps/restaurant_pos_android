package com.restaurant.zing.apiInterface

class UserDetailsQuickEditDataResponse {
    val addExistingItemsOrMods: Boolean? = null
    val addNewItemsMods: Boolean? = null
    val buttonColor: Boolean? = null
    val fullQuickEdit: Boolean? = null
    val inventoryAndQuantity: Boolean? = null
    val name: Boolean? = null
    val posName: Boolean? = null
    val price: Boolean? = null
    val rearrangingItemsMods: Boolean? = null
    val removeItemsOrMods: Boolean? = null
    val addOrUpdateServiceCharges: Boolean? = null
    val sku: Boolean? = null
}
