package com.restaurant.zing.apiInterface

class StayOrderResponse {
    var checkNumber: String? = null
    var createdOn: String? = null
    var dineInBehaviour: String? = null
    var dineInOptionId: String? = null
    var dineInOptionName: String? = null
    var totalAmount: String? = null
    var isRecallOrder: Boolean? = null
    var items: ArrayList<StayOrderItemsResponse>? = null
    var name: String? = null
    var orderId: String? = null
    var orderNumber: Int? = null
    var recallRef: String? = null
    var status: Int? = null
    var tableNumber: Int? = null
}
