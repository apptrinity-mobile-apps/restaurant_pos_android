package com.restaurant.zing.apiInterface

class GetAllOpenItemResponse {

    /*{
    "openItemsList":[
        {
            "id": "5f7ff4d71af0a5b6cfd03223",
            "name": "srisai"
        },
        {
            "id": "5f7ff5361af0a5b6cfd03224",
            "name": "test1"
        },
        {
            "id": "5f7ff546b8c0fcff9416fb4b",
            "name": "test1d"
        },
        {
            "id": "5f997fb560b7ca64037fe227",
            "name": "open item 1"
        },
        {
            "id": "5f9a4b005a2e29465ed6c57f",
            "name": "test2910"
        },
        {
            "id": "5f9f9340f6b86fa98056175e",
            "name": "naive"
        }
    ],
    "responseStatus": 1,
    "result": "Open Items data fetched successfully!!"
}
*/

    val responseStatus:Int?=null
    val result:String?=null

    val openItemsList:ArrayList<GetAllOpenItemList>?=null

}
