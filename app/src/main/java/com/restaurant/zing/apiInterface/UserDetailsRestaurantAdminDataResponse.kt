package com.restaurant.zing.apiInterface

class UserDetailsRestaurantAdminDataResponse {
    val customersCreditsAndReports: Boolean? = null
    val editFullMenu: Boolean? = null
    val editHistoricalData: Boolean? = null
    val employeeInfo: Boolean? = null
    val employeeJobsAndWages: Boolean? = null
    val giftOrRewardsCardReport: Boolean? = null
    val houseAccounts: Boolean? = null
    val laborReports: Boolean? = null
    val localMenuEdit: Boolean? = null
    val marketingInfo: Boolean? = null
    val menuReports: Boolean? = null
    val salesReports: Boolean? = null
    val tables: Boolean? = null
}
