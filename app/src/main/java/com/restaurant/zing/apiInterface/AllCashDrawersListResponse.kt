package com.restaurant.zing.apiInterface

class AllCashDrawersListResponse {
    var cashDrawerName: String? = null
    var id: String? = null
    var preAssigned: Boolean? = null
    var cashDrawer: Int? = null
    var cashDrawerBalnce: Double? = null
}
