package com.restaurant.zing.apiInterface

class FutureOnlineOrderDataResponse {

    /*"checkNumber": null,
    "deliveryStatus": 0,
    "dineInBehaviour": "delivery",
    "discountAmount": 0.4,
    "items_list": [
    {
        "itemId": "5fab7fde7de40f5c0fabf70c",
        "itemName": "a_poori"
    },
    {
        "itemId": "5fab7fde7de40f5c0fabf70d",
        "itemName": "a_onion dosa"
    },
    {
        "itemId": "5fab7fde7de40f5c0fabf70e",
        "itemName": "a_upma"
    }
    ],
    "orderDate": "11-11-2020",
    "orderNumber": 1,
    "orderTime": "06:08:30",
    "posOrderId": "5fab7fde7de40f5c0fabf70b",
    "scheduledDate": "11-10-2020",
    "scheduledEndTime": "04:30:00:AM",
    "scheduledStartTime": "02:30:00:AM",
    "status": 0,
    "subTotal": 25.0,
    "tableNumber": null,
    "taxAmount": 1.0,
    "tipAmount": 1.5,
    "totalAmount": 25.6*/

    val checkNumber: String? = null
    val deliveryStatus: String? = null
    val items_list: ArrayList<UnAssignedItemListResponse>? = null
    val dineInBehaviour: String? = null
    val discountAmount: String? = null
    val orderDate: String? = null
    val orderNumber: String? = null
    val orderTime: String? = null
    val posOrderId: String? = null
    val scheduledDate: String? = null
    val scheduledEndTime: String? = null
    val scheduledStartTime: String? = null
    val status: String? = null
    val subTotal: String? = null
    val tableNumber: String? = null
    val taxAmount: String? = null
    val tipAmount: String? = null
    val totalAmount: String? = null


}
