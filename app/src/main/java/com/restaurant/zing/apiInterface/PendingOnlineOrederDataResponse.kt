package com.restaurant.zing.apiInterface

class PendingOnlineOrederDataResponse {

    val checkNumber: String? = null
    val deliveryStatus: String? = null
    val dineInOption: String? = null
    val dineInBehaviour: String? = null
    val discountAmount: String? = null
    val discountValue: String? = null
    val id: String? = null
    val itemId: String? = null
    val itemName: String? = null
    val orderTime: String? = null
    val orderDate: String? = null
    val orderNumber: String? = null
    val posOrderId: String? = null
    val quantity: String? = null
    val status: String? = null
    val subTotal: String? = null
    val tableNumber: String? = null
    val taxAmount: String? = null
    val tipAmount: String? = null
    val totalPrice: String? = null
    val unitPrice: String? = null
    val items_list: ArrayList<UnAssignedItemListResponse>? = null
    val scheduledDate: String? = null
    val scheduledEndTime: String? = null
    val scheduledStartTime: String? = null

}
