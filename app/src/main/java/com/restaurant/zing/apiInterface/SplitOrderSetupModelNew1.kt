package com.restaurant.zing.apiInterface

import java.io.Serializable

class SplitOrderSetupModelNew1(

    val checkNumber: Int,
    val closedOn: String,
    val createdBy: String,
    val createdByName: String,
    val createdOn: String,
    val creditsUsed: Double,
    val dineInBehaviour: String,
    val dueAmount: Double,
    val dineInOption: String,
    val dineInOptionName: String,
    val discountAmount: Double,
    val haveCustomer: Boolean,
    val id: String,
    val status: Int,
    val restaurantId: String,
    val revenueCenterId: String,
    val discountLevel: String,
    val discountId: String,
    val discountName: String,
    val discountType: String,
    val discountValue: Double,
    val serviceAreaId: String,
    val orderNumber: Int,
    val subTotal: Double,
    val tableNumber: Int,
    val taxAmount: Double,
    val tipAmount: Double,
    val totalAmount: Double,
    val customerDetails: CustomerDataResponse,
    val orderDetails: ArrayList<OrderItemsModel>

) : Serializable