package com.restaurant.zing.apiInterface

class DepositsDataResponse {
    val actualDeposit: Double? = null
    val expectedDeposit: Double? = null
    val overage_shortage:Double? = null
}
