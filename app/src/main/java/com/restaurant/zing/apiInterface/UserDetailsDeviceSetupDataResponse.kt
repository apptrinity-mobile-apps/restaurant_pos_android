package com.restaurant.zing.apiInterface

class UserDetailsDeviceSetupDataResponse {
    val advancedTerminalSetup: Boolean? = null
    val kdsAndOrderScreenSetup: Boolean? = null
    val terminalSetup: Boolean? = null
}
