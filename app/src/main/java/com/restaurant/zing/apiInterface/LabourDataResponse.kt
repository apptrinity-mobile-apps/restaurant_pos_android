package com.restaurant.zing.apiInterface

class LabourDataResponse {
    val costs_sales: Int? = null
    val totalHours: String? = null
    val totalLabourCost: Int? = null
    val totalNetSales: Double? = null
}
