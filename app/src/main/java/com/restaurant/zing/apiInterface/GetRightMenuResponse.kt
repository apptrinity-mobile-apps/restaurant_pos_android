package com.restaurant.zing.apiInterface

class GetRightMenuResponse {
/*{
   "menuList": [
       {
           "id": "5ed618031f1ef910b6136b0b",
           "menuName": "testednsvdhfghgfbv"
       },
       {
           "id": "5f5f2484b74fa1949be8a03b",
           "menuName": "new samplesadsad12"
       },
       {
           "id": "5f64840c7ffe0f3e51167614",
           "menuName": "sample"
       },
       {
           "id": "5f6ad339cbdf7324e44ba360",
           "menuName": "test239"
       }
   ],
   "responseStatus": 1,
   "result": "Menu's data fetched Successfully!!"
}
*/

    val responseStatus:Int?=null
    val result:String?=null
    val menuList:ArrayList<GetRightMenuResponseList>?=null
}
