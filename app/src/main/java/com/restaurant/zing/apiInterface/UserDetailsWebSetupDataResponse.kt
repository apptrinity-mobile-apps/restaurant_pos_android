package com.restaurant.zing.apiInterface

class UserDetailsWebSetupDataResponse {
    val dataExportConfig: Boolean? = null
    val discountsSetup: Boolean? = null
    val financialAccounts: Boolean? = null
    val kitchenOrDiningRoomSetup: Boolean? = null
    val manageInstructions: Boolean? = null
    val paymentsSetup: Boolean? = null
    val publishing: Boolean? = null
    val restaurantGroupsSetup: Boolean? = null
    val restaurantOperationsSetup: Boolean? = null
    val taxRatesSetup: Boolean? = null
    val userPermissions: Boolean? = null
}
