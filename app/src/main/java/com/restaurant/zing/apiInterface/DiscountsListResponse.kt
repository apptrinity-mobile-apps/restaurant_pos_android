package com.restaurant.zing.apiInterface

import java.io.Serializable

class DiscountsListResponse : Serializable {

    val allowOtherDiscounts: Boolean? = null
    val appliesTo: Int? = null
    val discountValueType: String? = null
    val exceptDiscountItems: ArrayList<DiscountItems>? = null
    val id: String? = null
    val includeDiscountItems: ArrayList<DiscountItems>? = null
    val maxDiscountAmount: Double? = null
    val minDiscountAmount: Double? = null
    val name: String? = null
    val orderValue: String? = null
    val status: Int? = null
    val type: String? = null
    val uniqueNumber: String? = null
    val value: Double? = null

}
