package com.restaurant.zing.apiInterface

class CashDrawersDataResponse {
    var cashDrawer: Int? = null
    var cashDrawerName: String? = null
    var cashDrawerOneBalnce: Double? = null
    var id: String? = null
    var preAssigned: Boolean? = null
    var preAssignedName: String? = null
    var preAssignedTo: String? = null
    var restaurantId: String? = null
}
