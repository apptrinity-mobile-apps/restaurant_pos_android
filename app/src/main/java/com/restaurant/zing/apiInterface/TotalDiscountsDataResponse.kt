package com.restaurant.zing.apiInterface

class TotalDiscountsDataResponse {
    val amount: Double? = null
    val count: Int? = null
    val discountName: String? = null
}
