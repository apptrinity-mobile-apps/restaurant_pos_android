package com.restaurant.zing.apiInterface

import java.io.Serializable

class OrderItemsResponse : Serializable {
    val discountAmount: Double? = null
    val discountType: String? = null
    val discountId: String? = null
    val discountLevel: String? = null
    val discountName: String? = null
    val discountValue: Double? = null
    val discountValueType: String? = null
    val maxDiscountAmount: Double? = null
    val minDiscountAmount: Double? = null
    val exceptDiscountItems: ArrayList<DiscountItems>? = null
    val includeDiscountItems: ArrayList<DiscountItems>? = null
    val allowOtherDiscounts: Boolean? = null
    val itemId: String? = null
    val id: String? = null
    val menuGroupId: String? = null
    val menuId: String? = null
    val itemName: String? = null
    val name: String? = null
    val itemType: String? = null
    val itemStatus: Int? = null
    val quantity: Int? = null
    val totalPrice: Double? = null
    val unitPrice: Double? = null
    val isBogo: Boolean? = null
    val discountApplicable: Boolean = false
    val isCombo: Boolean? = null
    val removeStatus: Boolean? = null
    val removeQuantity: Int? = null
    val voidStatus: Boolean? = null
    val voidReason: String? = null
    val voidQuantity: Int? = null
    val status: Int? = null
    val comboId: String? = null
    val bogoId: String? = null
    val comboUniqueId: String? = null
    val bogoUniqueId: String? = null
    val diningOptionTaxException: Boolean? = null
    val diningTaxOption: Boolean? = null
    val taxIncludeOption: Boolean? = null
    val itemTaxAmount: Double? = null
    val taxesList: ArrayList<TaxRatesDetailsResponse>? = null
    val specialRequestList: ArrayList<SpecialRequestDataResponse>? = null
    val modifiersList: ArrayList<ModifiersDataResponse>? = null
    val deliveryDetails: ArrayList<DeliveryDetailsDataResponse>? = null
    val discountApplies: String? = null
}
