package com.restaurant.zing.apiInterface

import java.io.Serializable

class TaxRatesDetailsResponse : Serializable {
    val taxid: String? = null
    val createdBy: String? = null
    val default: Boolean = false
    val enableTakeOutRate: Boolean = false
    val importId: String? = null
    val orderValue: String? = null
    val restaurantId: String? = null
    val roundingOptions: Int? = null
    val status: Int? = null
    val takeOutTaxRate: Int? = null
    val taxName: String? = null
    val taxRate: Double? = null
    val taxTable: ArrayList<TaxTableResponse>? = null
    val taxType: Int? = null
    val uniqueNumber: Long? = null
    //val updatedBy: String? = null
}

