package com.restaurant.zing.apiInterface

class GiftCardBalance {
    val amount: Double? = null
    val cardType: String? = null
    val createdOn: String? = null
    val email: String? = null
    val name: String? = null
}
