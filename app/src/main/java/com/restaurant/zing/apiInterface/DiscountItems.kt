package com.restaurant.zing.apiInterface

import java.io.Serializable

class DiscountItems(
    val id: String,
    val itemName: String,
    val quantity: String,
    val type: String,
    val uniqueNumber: String
) : Serializable
