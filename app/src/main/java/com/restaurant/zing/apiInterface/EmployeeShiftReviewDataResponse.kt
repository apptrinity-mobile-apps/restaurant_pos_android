package com.restaurant.zing.apiInterface

class EmployeeShiftReviewDataResponse {
    val amount: Double? = null
    val cashOnHand: Double? = null
    val nonCashTips: Double? = null
    val employeeId: String? = null
    val employeeName: String? = null
    val id: String? = null
    val clockInStatus: String? = null
    val createdOn: String? = null
    val tipAmountType: String? = null
    val totalAmount: Double? = null
    val actionType: String? = null
    val cashInTotalAmount: String? = null
}
