package com.restaurant.zing.apiInterface

import java.io.Serializable

class SpecialRequestDataResponse :Serializable{
    val name: String? = null
    val requestPrice: Double? = null
}
