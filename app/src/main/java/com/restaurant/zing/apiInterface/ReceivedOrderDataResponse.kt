package com.restaurant.zing.apiInterface

class ReceivedOrderDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    var displayPrepType: Boolean? = null
    val orders: ArrayList<GetOrdersDataResponse>? = null
}
