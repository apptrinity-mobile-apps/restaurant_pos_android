package com.restaurant.zing.apiInterface

class AllDataResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val deviceId: String? = null
    val testModeStatus: Boolean? = null
    val userDetails: UserDetailsDataResponse? = null
    val customer: ArrayList<CustomerDataResponse>? = null
    val managerLevelShiftReviewList: ArrayList<EmployeeShiftReviewDataResponse>? = null
    val cashDepositsList: ArrayList<CashDepositsDataResponse>? = null
    val openChecks: ArrayList<ChecksDataResponse>? = null
    val closedChecks: ArrayList<ChecksDataResponse>? = null
    val paidChecks: ArrayList<ChecksDataResponse>? = null
    val customers_addressList: ArrayList<CustomerAddressDataResponse>? = null
    val clockedIn: ArrayList<EmployeeClockInOutDataResponse>? = null
    val clockedOut: ArrayList<EmployeeClockInOutDataResponse>? = null
    val ordersData: ArrayList<DeliveryOnlineDataResponse>? = null
    val deliveryList: ArrayList<DeliveryOnlineDataResponse>? = null
    val serverList: ArrayList<ServersDataResponse>? = null
    val pendingOnlineOrderList: ArrayList<PendingOnlineOrederDataResponse>? = null
    val futureOnlineOrderList: ArrayList<FutureOnlineOrderDataResponse>? = null
    val checksDetails: ArrayList<OrderDetailsResponse>? = null
    val orderDetails: ArrayList<OrderDetailsResponse>? = null
    val taxRates: ArrayList<TaxRatesDetailsResponse>? = null
    val ReceiptSetUp: ReceiptSetUpDataResponse? = null
    val void_reasonsList: ArrayList<VoidReasonsResponse>? = null
    val contries: ArrayList<CountriesResponse>? = null
    val states: ArrayList<StatesResponse>? = null
    val cities: ArrayList<CitiesResponse>? = null
    val orders: ArrayList<StayOrderResponse>? = null
    val diversList: ArrayList<DriversListResponse>? = null
    val employees: ArrayList<EmployeeIdsResponse>? = null
    val customerCreditsList: ArrayList<CustomerCreditsHistoryResponse>? = null
    val stationsList: ArrayList<PreparationStationsDataResponse>? = null
}
