package com.restaurant.zing.apiInterface

class CloseOutDayDataResponse {
    val creditCardBreakdown: ArrayList<CreditCardBreakdownDataResponse>? = null
    val currentStatus: CurrentStatusDataResponse? = null
    val delivery: ArrayList<DeliveryDataResponse>? = null
    val deposits: DepositsDataResponse? = null
    val labour: LabourDataResponse? = null
    val paymentDetails: PaymentDetailsDataResponse ? = null
    val responseStatus: Int? = null
    val result: String? = null
    val dateFormat: String? = null
    val salesAndTaxesSummary: SalesAndTaxesSummaryDataResponse? = null
    val salesCategories: SalesCategoryResponse? = null
    val serverTipouts: ServerTipsDataResponse? = null
    val totalDiscounts: ArrayList<TotalDiscountsDataResponse>? = null
    val totalRemovals: TotalRemovalsDataResponse? = null
    val totalVoids: TotalVoidsDataResponse? = null
}
