package com.restaurant.zing.apiInterface

import java.io.Serializable

class TransactionsReceiptResponse : Serializable {
    var amount: Double? = null
    var amountTendered: Double? = null
    var changeAmount: Double? = null
    var giftCardId: String? = null
    var cashDrawerId: String? = null
    var createdOn: String? = null
    var paymentMode: String? = null
    var transactionId: String? = null
    var sourceName: String? = null
    var sourceLast4: String? = null
    var sourceHolderName: String? = null
    var paymentType: Int? = null
    var status: Int? = null
}
