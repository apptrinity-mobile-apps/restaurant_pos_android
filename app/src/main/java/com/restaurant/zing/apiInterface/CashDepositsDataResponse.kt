package com.restaurant.zing.apiInterface

class CashDepositsDataResponse {
    val action: String? = null
    val actionType: String? = null
    val amount: Double? = null
    val cashDrawersId: String? = null
    val transactionType: String? = null
    val comment: String? = null
    val createdBy: String? = null
    val createdOn: String? = null
    val id: String? = null
    val restaurantId: String? = null
    val status: Int? = null
    val employeeName: String? = null
}
