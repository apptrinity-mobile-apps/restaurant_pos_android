package com.restaurant.zing.apiInterface

class CreditCardBreakdownDataResponse {
    val cardAmount: Double? = null
    val cardType: String? = null
}
