package com.restaurant.zing.apiInterface

class GetMenuItemsListResponse {
    val responseStatus:Int?=null
    val result:String?=null
    val menuItem:ArrayList<GetMenuItemsListResponseList>?=null
    /*{
    "menuItem": [
 {
            "basePrice": 600.0,
            "id": "5f6c4bba1ce310eaec629282",
            "name": "Item1",
            "priceProvider": 2,
            "pricingStrategy": 1,
            "restaurantId": "McDonalds1"
        },{
            "id": "5f6c61f4231345a28de22b3f",
            "name": "item3-1",
            "priceProvider": 3,
            "pricingStrategy": 2,
            "restaurantId": "McDonalds1",
            "sizeList": [
                {
                    "price": "20.00",
                    "sizeName": "small"
                },
                {
                    "price": "35.00",
                    "sizeName": "medium"
                },
                {
                    "price": "45.00",
                    "sizeName": "large"
                },
                {
                    "price": "95.00",
                    "sizeName": "very_large"
                }
            ]
        },
       {
            "basePrice": "20",
            "id": "5f71983eaae65719ce69e759",
            "name": "menu3",
            "priceProvider": 3,
            "pricingStrategy": 1,
            "restaurantId": "5ec628eb0ae0b6043743eab6"
        },
        {
            "id": "5f6c646016aab56c66f91be9",
            "name": "item5",
            "priceProvider": 3,
            "pricingStrategy": 5,
             "restaurantId": "5ec628eb0ae0b6043743eab6"

        },
        {
            "id": "5f6c664de4dc407bc5edf900",
            "name": "item6",
		 "basePrice": "20",
            "priceProvider": 3,
            "pricingStrategy": 4,
            "restaurantId": "5ec628eb0ae0b6043743eab6",
            "timePriceList": [
                {
                    "days": [
                        "sunday",
                        "monday"
                    ],
                    "price": "20.00",
                    "timeFrom": "8.00 AM",
                    "timeTo": "9.00 PM"
                },
                {
                    "days": [
                        "sunday",
                        "monday"
                    ],
                    "price": "50.00",
                    "timeFrom": "8.00 AM",
                    "timeTo": "9.00 PM"
                }
            ]
        },
        {
            "basePrice": 0.0,
            "id": "5f6c6730e096b201714d4a00",
            "name": "item7",
            "priceProvider": 3,
            "pricingStrategy": 1,
             "restaurantId": "5ec628eb0ae0b6043743eab6"
        },
		{
            "id": "5f6e022fad4dec38f0c79921",
            "name": "testItem",
            "priceProvider": 1,
            "pricingStrategy": 2,
            "restaurantId": "5ec628eb0ae0b6043743eab6",
            "sizeList": [
                {
                    "price": "25",
                    "sizeName": "Sizw 1"
                }
            ]
        },
   {
            "basePrice": "20",
            "id": "5f71983eaae65719ce69e759",
            "name": "menu3",
            "priceProvider": 3,
            "pricingStrategy": 1,
            "restaurantId": "5ec628eb0ae0b6043743eab6"
        },

{
            "id": "5f6e022fad4dec38f0c79921",
            "name": "testItem",
		 "basePrice": "20",

            "priceProvider": 1,
            "pricingStrategy": 4,
            "restaurantId": "5ec628eb0ae0b6043743eab6",
            "timePriceList": [
                {
                    "days": [
                        "Friday",
                        "Wednesday",
                        "Tuesday"
                    ],
                    "price": "8",
                    "timeFrom": "14:33",
                    "timeTo": "15:33"
                },
                {
                    "days": [
                        "Sunday"
                    ],
                    "price": "8",
                    "timeFrom": "19:33",
                    "timeTo": "21:33"
                }
            ]
        }’
                  ],
    "responseStatus": 1,
    "result": "Menu Item data fetched successfully"
}
*/

}



