package com.restaurant.zing.apiInterface

class ItemNameDiscountDataResponse {

    val discountAmount: String? = null
    val discountId: String? = null
    val discountName: String? = null
    val discountType: String? = null
    val discountValue: String? = null
    val itemName: String? = null

}
