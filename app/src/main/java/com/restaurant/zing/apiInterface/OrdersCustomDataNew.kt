package com.restaurant.zing.apiInterface

import java.io.Serializable

class OrdersCustomDataNew(
    var basePrice: Double,
    var discountApplicable: Boolean,
    val name: String,
    val id: String,
    val itemId: String,
    val menuGroupId: String,
    val menuId: String,
    val itemType: String,
    val itemStatus: Int,
    var qty: Int,
    val total: Double,
    val discount: String,
    val restaurantId: String,
    val removeQuantity: Int,
    val removeStatus: Boolean,
    val voidQuantity: Int,
    val voidStatus: Boolean,
    val allmodifierArray: ArrayList<GetAllModifierListResponseList>,
    val discountApplied: Boolean,
    var discountAppliedType: String,
    val discountsArray: ArrayList<GetAllDiscountsListResponse>,
    var isCombo: Boolean,
    var isBogo: Boolean,
    var comboOrBogoOfferId: String, // combo or bogo id only if combo or bogo is selected
    var comboOrBogoUniqueId: String // unique id for each combo or bogo group
) : Serializable