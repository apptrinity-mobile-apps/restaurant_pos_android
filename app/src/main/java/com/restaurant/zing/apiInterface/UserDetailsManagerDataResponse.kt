package com.restaurant.zing.apiInterface

class UserDetailsManagerDataResponse {
    val adjustCashDrawerStartBalance: Boolean? = null
    val bulkClosePaidChecks: Boolean? = null
    val bulkTransferCheck: Boolean? = null
    val bulkVoidOpenChecks: Boolean? = null
    val cashDrawerLockdown: Boolean? = null
    val cashDrawersBlind: Boolean? = null
    val cashDrawersFull: Boolean? = null
    val closeOutDay: Boolean? = null
    val discounts: Boolean? = null
    val editSentItems: Boolean? = null
    val editTimeEntries: Boolean? = null
    val endBreaksEarly: Boolean? = null
    val findChecks: Boolean? = null
    val giftCardAdjustment: Boolean? = null
    val largeCashOverOrUnder: Boolean? = null
    val logBook: Boolean? = null
    val negativeDeclaredTips: Boolean? = null
    val openItems: Boolean? = null
    val otherPaymentTypes: Boolean? = null
    val payout: Boolean? = null
    val registerSwipeCards: Boolean? = null
    val sendNotifications: Boolean? = null
    val shiftReview: Boolean? = null
    val taxExempt: Boolean? = null
    val throttleOnlineOrders: Boolean? = null
    val transferOrRewardsAdjustment: Boolean? = null
    val unlinkedRefunds: Boolean? = null
    val voidItemOrOrders: Boolean? = null
    val voidOrRefundPayments: Boolean? = null
}
