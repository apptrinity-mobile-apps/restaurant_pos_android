package com.restaurant.zing.apiInterface

class CustomerBonusResponse {
    val responseStatus: Int? = null
    val result: String? = null
    val creditsData : CustomerBonusPointsDataResponse? = null
    val transactions_list : List<CustomerBonusPointsTransactionsDataResponse>? = null
}