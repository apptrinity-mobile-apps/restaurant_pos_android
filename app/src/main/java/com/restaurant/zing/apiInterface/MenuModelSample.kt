package com.restaurant.zing.apiInterface

import java.util.*

class MenuModelSample(
    var basePrice: String,
    var id: String,
    var name: String,
    var priceProvider: String,
    allmodifierArray: ArrayList<GetAllModifierListResponseList?>
) {
    var allmodifierArray: ArrayList<GetAllModifierListResponseList?> = ArrayList()

    init {
        this.allmodifierArray = allmodifierArray
    }
}