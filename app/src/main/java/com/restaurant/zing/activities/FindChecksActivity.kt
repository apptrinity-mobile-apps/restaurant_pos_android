package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.AllEmployeeIdsListAdapter
import com.restaurant.zing.adapters.FindChecksListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class FindChecksActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var tabLayout: TabLayout
    private lateinit var findby_checknoTab: TextView
    private lateinit var findby_idTab: TextView
    private lateinit var findby_customerTab: TextView
    private lateinit var findby_creditcardTab: TextView
    private lateinit var findby_advsearchTab: TextView

    private lateinit var findby_checknoTabHeader: LinearLayout
    private lateinit var findby_idTabHeader: LinearLayout
    private lateinit var findby_customerTabHeader: LinearLayout
    private lateinit var findby_creditcardTabHeader: LinearLayout
    private lateinit var findby_advsearchTabHeader: LinearLayout

    private lateinit var layout_findckno: LinearLayout
    private lateinit var layout_findbyid: LinearLayout
    private lateinit var layout_findbycustomer: LinearLayout
    private lateinit var layout_findbycredit: LinearLayout
    private lateinit var layout_advancesearch: LinearLayout
    private lateinit var rv_find_checks: RecyclerView
    private lateinit var tv_find_checks_empty: TextView

    lateinit var et_check_date: EditText
    lateinit var et_check_number: EditText
    lateinit var et_check_id: EditText
    lateinit var et_check_guid: EditText
    lateinit var et_order_guid: EditText
    lateinit var et_order_id: EditText
    lateinit var et_phonenumber: EditText
    lateinit var et_check_firstname: EditText
    lateinit var et_lastname: EditText
    lateinit var et_creditcard: EditText
    lateinit var et_from: EditText
    lateinit var et_to: EditText
    lateinit var et_advsstart_date: EditText
    lateinit var et_advsend_date: EditText
    lateinit var sp_employees: Spinner
    lateinit var tv_findchecks_search: TextView
    lateinit var allEmployeeIdsListAdapter: AllEmployeeIdsListAdapter

    var cal = Calendar.getInstance()
    lateinit var employeesList: ArrayList<EmployeeIdsResponse>

    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mPosUserId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var mSelectedIndex = -1
    private var selected_tab = ""
    private var selectedEmployeeId = ""
    private var currenryType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_checks)

        try {
            selected_tab = intent.getStringExtra("selected_tab")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()
        try {
            val jsonObj = JSONObject()
            jsonObj.put("employeeId", mPosUserId)
            jsonObj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            getAllEmployeeIds(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("restaurantId", restaurantId)
            jObj.put("userId", mPosUserId)
            jObj.put("jobId", mEmpJobId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            employeeLogout(finalObject)
        }
        et_check_date.setOnClickListener {
            hideKeyboard()
            showDatePicker(et_check_date)
        }
        et_advsstart_date.setOnClickListener {
            hideKeyboard()
            showDatePicker(et_advsstart_date)
        }
        et_advsend_date.setOnClickListener {
            hideKeyboard()
            showDatePicker(et_advsend_date)
        }
        tv_findchecks_search.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("employeeId", mPosUserId)
            jObj.put("restaurantId", restaurantId)
            when (selected_tab) {
                "check_no" -> {
                    jObj.put("checkNumber", et_check_number.text.toString())
                    jObj.put("checkDate", et_check_date.text.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    when {
                        et_check_number.text.toString().trim() == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please enter check number!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        et_check_date.text.toString().trim() == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please select check date!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            findByCheckNumber(finalObject)
                        }
                    }
                }
                "check_id" -> {
                    jObj.put("checkNumber", et_check_id.text.toString())
                    jObj.put("orderNumber", et_order_id.text.toString())
                    jObj.put("checkUniqueId", et_check_guid.text.toString())
                    jObj.put("orderUniqueId", et_order_guid.text.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    if (et_check_id.text.toString().trim() == "" &&
                        et_order_id.text.toString().trim() == "" &&
                        et_check_guid.text.toString().trim() == "" &&
                        et_order_guid.text.toString().trim() == ""
                    ) {
                        Toast.makeText(
                            this@FindChecksActivity,
                            "Please enter any ID!", Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        findByCheckId(finalObject)
                    }
                }
                "customer" -> {
                    jObj.put("firstName", et_check_firstname.text.toString())
                    jObj.put("lastName", et_lastname.text.toString())
                    jObj.put("phoneNumber", et_phonenumber.text.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    if (et_check_firstname.text.toString().trim() == "" &&
                        et_lastname.text.toString().trim() == "" &&
                        et_phonenumber.text.toString().trim() == ""
                    ) {
                        Toast.makeText(
                            this@FindChecksActivity,
                            "Please enter any customer detail!", Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        findByCustomer(finalObject)
                    }
                }
                "credit_card" -> {
                    jObj.put("cardLast4Digit", et_creditcard.text.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    if (et_creditcard.text.toString().trim() == "") {
                        Toast.makeText(
                            this@FindChecksActivity,
                            "Please enter any customer detail!", Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        findByCard(finalObject)
                    }
                }
                "advance_search" -> {
                    jObj.put("startDate", et_advsstart_date.text.toString())
                    jObj.put("endDate", et_advsend_date.text.toString())
                    jObj.put("amountFrom", et_from.text.toString())
                    jObj.put("amountTo", et_to.text.toString())
                    jObj.put("selectedEmployeeId", selectedEmployeeId)
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    when {
                        et_advsstart_date.text.toString().trim() == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please select start date!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        et_advsend_date.text.toString().trim() == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please select end date!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        et_from.text.toString().trim() == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please enter amount start range!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        et_to.text.toString().trim() == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please enter amount end range!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        selectedEmployeeId == "" -> {
                            Toast.makeText(
                                this@FindChecksActivity,
                                "Please select employee!", Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            findByAdvancedSearch(finalObject)
                        }
                    }
                }
            }
        }
        sp_employees.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                val selectedItem = allEmployeeIdsListAdapter.getSelectedItem(position)
                selectedEmployeeId = selectedItem.id.toString()
            }
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                hideKeyboard()
                mSelectedIndex = -1
                when (tab!!.position) {
                    0 -> {
                        selected_tab = "check_no"
                        layout_findckno.visibility = View.VISIBLE
                        layout_findbyid.visibility = View.GONE
                        layout_findbycustomer.visibility = View.GONE
                        layout_findbycredit.visibility = View.GONE
                        layout_advancesearch.visibility = View.GONE
                        findby_checknoTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.white)
                        )
                        findby_idTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_customerTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_creditcardTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_advsearchTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_checknoTabHeader.background = ContextCompat.getDrawable(
                            this@FindChecksActivity, R.drawable.gradient_bg_1
                        )
                        findby_idTabHeader.background = null
                        findby_customerTabHeader.background = null
                        findby_creditcardTabHeader.background = null
                        findby_advsearchTabHeader.background = null
                    }
                    1 -> {
                        selected_tab = "check_id"
                        layout_findckno.visibility = View.GONE
                        layout_findbyid.visibility = View.VISIBLE
                        layout_findbycustomer.visibility = View.GONE
                        layout_findbycredit.visibility = View.GONE
                        layout_advancesearch.visibility = View.GONE
                        findby_checknoTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_idTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.white)
                        )
                        findby_customerTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_creditcardTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_advsearchTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_checknoTabHeader.background = null
                        findby_idTabHeader.background = ContextCompat.getDrawable(
                            this@FindChecksActivity, R.drawable.gradient_bg_1
                        )

                        findby_customerTabHeader.background = null
                        findby_creditcardTabHeader.background = null
                        findby_advsearchTabHeader.background = null
                    }
                    2 -> {
                        selected_tab = "customer"
                        layout_findckno.visibility = View.GONE
                        layout_findbyid.visibility = View.GONE
                        layout_findbycustomer.visibility = View.VISIBLE
                        layout_findbycredit.visibility = View.GONE
                        layout_advancesearch.visibility = View.GONE
                        findby_checknoTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_idTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_customerTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.white)
                        )
                        findby_creditcardTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_advsearchTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_checknoTabHeader.background = null
                        findby_idTabHeader.background = null
                        findby_customerTabHeader.background = ContextCompat.getDrawable(
                            this@FindChecksActivity, R.drawable.gradient_bg_1
                        )
                        findby_creditcardTabHeader.background = null
                        findby_advsearchTabHeader.background = null
                    }
                    3 -> {
                        selected_tab = "credit_card"
                        layout_findckno.visibility = View.GONE
                        layout_findbyid.visibility = View.GONE
                        layout_findbycustomer.visibility = View.GONE
                        layout_findbycredit.visibility = View.VISIBLE
                        layout_advancesearch.visibility = View.GONE
                        findby_checknoTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_idTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_customerTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_creditcardTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.white)
                        )
                        findby_advsearchTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_checknoTabHeader.background = null
                        findby_idTabHeader.background = null
                        findby_customerTabHeader.background = null
                        findby_creditcardTabHeader.background = ContextCompat.getDrawable(
                            this@FindChecksActivity, R.drawable.gradient_bg_1
                        )
                        findby_advsearchTabHeader.background = null
                    }
                    4 -> {
                        selected_tab = "advance_search"
                        layout_findckno.visibility = View.GONE
                        layout_findbyid.visibility = View.GONE
                        layout_findbycustomer.visibility = View.GONE
                        layout_findbycredit.visibility = View.GONE
                        layout_advancesearch.visibility = View.VISIBLE
                        findby_checknoTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_idTab.setTextColor(
                            ContextCompat.getColor(
                                this@FindChecksActivity, R.color.edit_text_hint
                            )
                        )
                        findby_customerTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_creditcardTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
                        )
                        findby_advsearchTab.setTextColor(
                            ContextCompat.getColor(this@FindChecksActivity, R.color.white)
                        )
                        findby_checknoTabHeader.background = null
                        findby_idTabHeader.background = null
                        findby_customerTabHeader.background = null
                        findby_creditcardTabHeader.background = null
                        findby_advsearchTabHeader.background = ContextCompat.getDrawable(
                            this@FindChecksActivity, R.drawable.gradient_bg_1
                        )
                    }
                }
            }
        })
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mPosUserId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currenryType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tabLayout = findViewById(R.id.tabLayout)
        layout_findckno = findViewById(R.id.layout_findckno)
        layout_findckno.visibility = View.VISIBLE
        layout_findbyid = findViewById(R.id.layout_findbyid)
        layout_findbycustomer = findViewById(R.id.layout_findbycustomer)
        layout_findbycredit = findViewById(R.id.layout_findbycredit)
        layout_advancesearch = findViewById(R.id.layout_advancesearch)
        rv_find_checks = findViewById(R.id.rv_find_checks)
        tv_find_checks_empty = findViewById(R.id.tv_find_checks_empty)
        //find by checkno...
        et_check_date = findViewById(R.id.et_check_date)
        et_check_number = findViewById(R.id.et_check_number)
        tv_findchecks_search = findViewById(R.id.tv_findchecks_search)
        //find by check id
        et_check_id = findViewById(R.id.et_check_id)
        et_check_guid = findViewById(R.id.et_check_guid)
        et_order_guid = findViewById(R.id.et_order_guid)
        et_order_id = findViewById(R.id.et_order_id)
        //find by customer details
        et_phonenumber = findViewById(R.id.et_phonenumber)
        et_check_firstname = findViewById(R.id.et_check_firstname)
        et_lastname = findViewById(R.id.et_check_firstname)
        //find by card
        et_creditcard = findViewById(R.id.et_creditcard)
        //find by advanced search
        et_from = findViewById(R.id.et_from)
        et_to = findViewById(R.id.et_to)
        et_advsstart_date = findViewById(R.id.et_advsstart_date)
        et_advsend_date = findViewById(R.id.et_advsend_date)
        sp_employees = findViewById(R.id.sp_employees)
        setUpTabIcons()
        val openLinearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_find_checks.layoutManager = openLinearLayoutManager
        rv_find_checks.hasFixedSize()
        employeesList = ArrayList()
    }

    private fun showDatePicker(editText: EditText) {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "MM-dd-yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                Log.d("testdate", sdf.format(cal.time))
                editText.setText(sdf.format(cal.time))
            }
        DatePickerDialog(
            this@FindChecksActivity,
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun setUpTabIcons() {
        val findby_ChecknoView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val findby_idView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val findby_customerView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val findby_creditcardView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val findby_advsearchView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)

        findby_checknoTabHeader = findby_ChecknoView.findViewById(R.id.ll_tab_header)
        findby_idTabHeader = findby_idView.findViewById(R.id.ll_tab_header)
        findby_customerTabHeader = findby_customerView.findViewById(R.id.ll_tab_header)
        findby_creditcardTabHeader = findby_creditcardView.findViewById(R.id.ll_tab_header)
        findby_advsearchTabHeader = findby_advsearchView.findViewById(R.id.ll_tab_header)

        findby_checknoTab = findby_ChecknoView.findViewById(R.id.tv_tab_name)
        findby_idTab = findby_idView.findViewById(R.id.tv_tab_name)
        findby_customerTab = findby_customerView.findViewById(R.id.tv_tab_name)
        findby_creditcardTab = findby_creditcardView.findViewById(R.id.tv_tab_name)
        findby_advsearchTab = findby_advsearchView.findViewById(R.id.tv_tab_name)

        findby_checknoTab.text = getString(R.string.findby_checkno)
        findby_idTab.text = getString(R.string.findby_id)
        findby_customerTab.text = getString(R.string.findby_customer)
        findby_creditcardTab.text = getString(R.string.findby_creditcard)
        findby_advsearchTab.text = getString(R.string.findby_advsearch)

        val openLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val paidLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val closedLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val creditLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val advanceLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        findby_ChecknoView.layoutParams = openLp
        findby_idView.layoutParams = paidLp
        findby_customerView.layoutParams = closedLp
        findby_creditcardView.layoutParams = creditLp
        findby_advsearchView.layoutParams = advanceLp

        if (selected_tab == "check_no") {
            mSelectedIndex = -1
            selected_tab = "check_no"
            layout_findckno.visibility = View.VISIBLE
            layout_findbyid.visibility = View.GONE
            layout_findbycustomer.visibility = View.GONE
            layout_findbycredit.visibility = View.GONE
            layout_advancesearch.visibility = View.GONE
            findby_checknoTab.setTextColor(
                ContextCompat.getColor(this@FindChecksActivity, R.color.white)
            )
            findby_idTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_customerTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_creditcardTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_advsearchTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_checknoTabHeader.background = ContextCompat.getDrawable(
                this@FindChecksActivity, R.drawable.gradient_bg_1
            )
            findby_idTabHeader.background = null
            findby_customerTabHeader.background = null
            findby_creditcardTabHeader.background = null
            findby_advsearchTabHeader.background = null
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_ChecknoView), true)
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_idView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_customerView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_creditcardView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_advsearchView))
        } else if (selected_tab == "check_id") {
            mSelectedIndex = -1
            selected_tab = "check_id"
            layout_findckno.visibility = View.GONE
            layout_findbyid.visibility = View.VISIBLE
            layout_findbycustomer.visibility = View.GONE
            layout_findbycredit.visibility = View.GONE
            layout_advancesearch.visibility = View.GONE
            findby_checknoTab.setTextColor(
                ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
            )
            findby_idTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.white
                )
            )
            findby_customerTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_creditcardTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_advsearchTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )

            findby_checknoTabHeader.background = null
            findby_idTabHeader.background = ContextCompat.getDrawable(
                this@FindChecksActivity, R.drawable.gradient_bg_1
            )
            findby_customerTabHeader.background = null
            findby_creditcardTabHeader.background = null
            findby_advsearchTabHeader.background = null
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_ChecknoView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_idView), true)
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_customerView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_creditcardView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_advsearchView))
        } else if (selected_tab == "customer") {
            mSelectedIndex = -1
            selected_tab = "customer"
            layout_findckno.visibility = View.GONE
            layout_findbyid.visibility = View.GONE
            layout_findbycustomer.visibility = View.VISIBLE
            layout_findbycredit.visibility = View.GONE
            layout_advancesearch.visibility = View.GONE
            findby_checknoTab.setTextColor(
                ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
            )
            findby_idTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_customerTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.white
                )
            )
            findby_creditcardTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_advsearchTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_checknoTabHeader.background = null
            findby_idTabHeader.background = null
            findby_customerTabHeader.background = ContextCompat.getDrawable(
                this@FindChecksActivity, R.drawable.gradient_bg_1
            )
            findby_creditcardTabHeader.background = null
            findby_advsearchTabHeader.background = null
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_ChecknoView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_idView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_customerView), true)
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_creditcardView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_advsearchView))
        } else if (selected_tab == "credit_card") {
            mSelectedIndex = -1
            selected_tab = "credit_card"
            layout_findckno.visibility = View.GONE
            layout_findbyid.visibility = View.GONE
            layout_findbycustomer.visibility = View.GONE
            layout_findbycredit.visibility = View.VISIBLE
            layout_advancesearch.visibility = View.GONE
            findby_checknoTab.setTextColor(
                ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
            )
            findby_idTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_customerTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_creditcardTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.white
                )
            )
            findby_advsearchTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_checknoTabHeader.background = null
            findby_idTabHeader.background = null
            findby_customerTabHeader.background = null
            findby_creditcardTabHeader.background = ContextCompat.getDrawable(
                this@FindChecksActivity, R.drawable.gradient_bg_1
            )
            findby_advsearchTabHeader.background = null
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_ChecknoView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_idView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_customerView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_creditcardView), true)
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_advsearchView))
        } else if (selected_tab == "advance_search") {
            mSelectedIndex = -1
            selected_tab = "advance_search"
            layout_findckno.visibility = View.GONE
            layout_findbyid.visibility = View.GONE
            layout_findbycustomer.visibility = View.GONE
            layout_findbycredit.visibility = View.GONE
            layout_advancesearch.visibility = View.VISIBLE
            findby_checknoTab.setTextColor(
                ContextCompat.getColor(this@FindChecksActivity, R.color.edit_text_hint)
            )
            findby_idTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_customerTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_creditcardTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.edit_text_hint
                )
            )
            findby_advsearchTab.setTextColor(
                ContextCompat.getColor(
                    this@FindChecksActivity, R.color.white
                )
            )
            findby_checknoTabHeader.background = null
            findby_idTabHeader.background = null
            findby_customerTabHeader.background = null
            findby_creditcardTabHeader.background = null
            findby_advsearchTabHeader.background = ContextCompat.getDrawable(
                this@FindChecksActivity, R.drawable.gradient_bg_1
            )
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_ChecknoView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_idView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_customerView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_creditcardView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(findby_advsearchView), true)
        }
    }

    private fun findByCheckNumber(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.findByCheckNumberApi(finalObject)
        call.enqueue(object : Callback<FindChecksDataResponse> {
            override fun onFailure(call: Call<FindChecksDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<FindChecksDataResponse>,
                response: Response<FindChecksDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val checksList = respBody.matchedOrdersList
                        if (checksList.isNotEmpty()) {
                            val adapter =
                                FindChecksListAdapter(
                                    this@FindChecksActivity,
                                    checksList
                                )
                            rv_find_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_find_checks.visibility = View.VISIBLE
                            tv_find_checks_empty.visibility = View.GONE
                        } else {
                            rv_find_checks.visibility = View.GONE
                            tv_find_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        rv_find_checks.visibility = View.GONE
                        tv_find_checks_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun findByCheckId(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.findByCheckIdApi(finalObject)
        call.enqueue(object : Callback<FindChecksDataResponse> {
            override fun onFailure(call: Call<FindChecksDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<FindChecksDataResponse>,
                response: Response<FindChecksDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val checksList = respBody.matchedOrdersList
                        if (checksList.isNotEmpty()) {
                            val adapter =
                                FindChecksListAdapter(
                                    this@FindChecksActivity,
                                    checksList
                                )
                            rv_find_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_find_checks.visibility = View.VISIBLE
                            tv_find_checks_empty.visibility = View.GONE
                        } else {
                            rv_find_checks.visibility = View.GONE
                            tv_find_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        rv_find_checks.visibility = View.GONE
                        tv_find_checks_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun findByCustomer(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.findByCustomerApi(finalObject)
        call.enqueue(object : Callback<FindChecksDataResponse> {
            override fun onFailure(call: Call<FindChecksDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<FindChecksDataResponse>,
                response: Response<FindChecksDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val checksList = respBody.matchedOrdersList
                        if (checksList.isNotEmpty()) {
                            val adapter =
                                FindChecksListAdapter(
                                    this@FindChecksActivity,
                                    checksList
                                )
                            rv_find_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_find_checks.visibility = View.VISIBLE
                            tv_find_checks_empty.visibility = View.GONE
                        } else {
                            rv_find_checks.visibility = View.GONE
                            tv_find_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        rv_find_checks.visibility = View.GONE
                        tv_find_checks_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun findByCard(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.findByCardApi(finalObject)
        call.enqueue(object : Callback<FindChecksDataResponse> {
            override fun onFailure(call: Call<FindChecksDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<FindChecksDataResponse>,
                response: Response<FindChecksDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val checksList = respBody.matchedOrdersList
                        if (checksList.isNotEmpty()) {
                            val adapter =
                                FindChecksListAdapter(
                                    this@FindChecksActivity,
                                    checksList
                                )
                            rv_find_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_find_checks.visibility = View.VISIBLE
                            tv_find_checks_empty.visibility = View.GONE
                        } else {
                            rv_find_checks.visibility = View.GONE
                            tv_find_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        rv_find_checks.visibility = View.GONE
                        tv_find_checks_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun findByAdvancedSearch(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.findByAdvancedSearchApi(finalObject)
        call.enqueue(object : Callback<FindChecksDataResponse> {
            override fun onFailure(call: Call<FindChecksDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<FindChecksDataResponse>,
                response: Response<FindChecksDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val checksList = respBody.matchedOrdersList
                        if (checksList.isNotEmpty()) {
                            val adapter =
                                FindChecksListAdapter(
                                    this@FindChecksActivity,
                                    checksList
                                )
                            rv_find_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_find_checks.visibility = View.VISIBLE
                            tv_find_checks_empty.visibility = View.GONE
                        } else {
                            rv_find_checks.visibility = View.GONE
                            tv_find_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        rv_find_checks.visibility = View.GONE
                        tv_find_checks_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllEmployeeIds(finalObject: JsonObject) {
        employeesList.clear()
        val api = ApiInterface.create()
        val call = api.getAllEmployeesIdApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        if (respBody.employees!!.size > 0) {
                            employeesList.addAll(respBody.employees)
                        }
                        allEmployeeIdsListAdapter =
                            AllEmployeeIdsListAdapter(this@FindChecksActivity, employeesList)
                        sp_employees.adapter = allEmployeeIdsListAdapter
                        allEmployeeIdsListAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@FindChecksActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@FindChecksActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@FindChecksActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = this.currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}