package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bolt.consumersdk.CCConsumer
import com.bolt.consumersdk.CCConsumerTokenCallback
import com.bolt.consumersdk.domain.CCConsumerAccount
import com.bolt.consumersdk.domain.CCConsumerCardInfo
import com.bolt.consumersdk.domain.CCConsumerError
import com.bolt.consumersdk.enums.CCConsumerMaskFormat
import com.bolt.consumersdk.views.CCConsumerCreditCardNumberEditText
import com.bolt.consumersdk.views.CCConsumerCvvEditText
import com.bolt.consumersdk.views.CCConsumerExpirationDateEditText
import com.bruce.pickerview.popwindow.TimePickerPopWin
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.restaurant.zing.helpers.RecyclerItemClickListener
import com.restaurant.zing.adapters.*
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.apiInterface.MenuItem
import com.restaurant.zing.helpers.*
import com.restaurant.zing.R
import kotlinx.android.synthetic.main.activity_order_details.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n", "SimpleDateFormat")
class OrderDetailsActivity : AppCompatActivity() {

    private lateinit var loading_dialog: Dialog
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var rv_user_search: RecyclerView
    private lateinit var ll_user_empty: LinearLayout
    private lateinit var et_first_name: EditText
    private lateinit var et_last_name: EditText
    private lateinit var et_phone_number: EditText
    private lateinit var et_email: EditText
    private lateinit var tv_ok: TextView
    private lateinit var comboArrayList: ArrayList<Combo>
    private lateinit var maincomboArrayList: ArrayList<List<Combo>>
    private lateinit var bogoArrayList: ArrayList<BogoBuyItem>
    private lateinit var bogoGetArrayList: ArrayList<BogoGetItem>
    private lateinit var mainbogoArrayList: ArrayList<List<BogoBuyItem>>
    private var specialRequests = GetAllModifierListResponseList("", "", 0.00, 0.00, ArrayList())
    private lateinit var taxRatesList: ArrayList<TaxRatesDetailsResponse>
    private var preModifier = GetAllPreModifierListResponseList()
    private var custom_array1: ArrayList<OrdersCustomData> = ArrayList()
    private var finalCustom_array: List<OrdersCustomData> = ArrayList()
    private lateinit var orderDetails: OrderDetailsResponse
    private lateinit var void_reason_dialog: Dialog
    private lateinit var void_reasonsList: ArrayList<VoidReasonsResponse>
    private var checkLevelDiscountArray = ArrayList<GetAllDiscountsListResponse>()
    private lateinit var add_customer_dialog: Dialog
    private lateinit var gift_card_payment_dialog: Dialog
    private lateinit var gift_card_with_bank_payment_dialog: Dialog
    private lateinit var successDialog: Dialog
    private lateinit var countriesList: ArrayList<CountriesResponse>
    private lateinit var statesList: ArrayList<StatesResponse>
    private lateinit var citiesList: ArrayList<CitiesResponse>
    private lateinit var customerAddresesList: ArrayList<CustomerAddressDataResponse>
    private lateinit var countriesListAdapter: CountriesListAdapter
    private lateinit var statesListAdapter: StatesListAdapter
    private lateinit var citiesListAdapter: CitiesListAdapter
    private lateinit var sp_country: Spinner
    private lateinit var sp_state: Spinner
    private lateinit var sp_city: Spinner
    private lateinit var sp_delivery_country: Spinner
    private lateinit var sp_delivery_state: Spinner
    private lateinit var sp_delivery_city: Spinner
    private lateinit var dummyCountry: CountriesResponse
    private lateinit var dummyState: StatesResponse
    private lateinit var dummyCity: CitiesResponse
    private lateinit var tv_cancel: TextView
    private lateinit var tv_edit_table: TextView
    private lateinit var tv_edit_guests: TextView
    private lateinit var tv_checks: TextView
    private lateinit var dialog_gift_card: Dialog
    private lateinit var stay_orders_dialog: Dialog
    private lateinit var stay_orders_comment_dialog: Dialog
    private lateinit var table_orders_dialog: Dialog
    private lateinit var ll_show_stay_orders: LinearLayout
    private lateinit var tv_stay_order_count: TextView
    private lateinit var stayOrders: ArrayList<StayOrderResponse>
    private lateinit var tableChecks: ArrayList<OrderDetailsResponse>
    private lateinit var tv_stay_orders_empty: TextView
    private lateinit var ll_cancel: LinearLayout
    private lateinit var tv_select_all: TextView
    private lateinit var rv_stay_orders: RecyclerView
    private lateinit var stayOrdersAdapter: StayOrdersAdapter
    private lateinit var tv_test_mode: TextView
    private lateinit var rv_combo_items: RecyclerView
    private lateinit var rv_bogo_buy_items: RecyclerView
    private lateinit var rv_bogo_get_items: RecyclerView
    private lateinit var comboMenuListAdapter: ComboMenuListAdapter
    private lateinit var bogoBuyMenuListAdapter: BogoMenuListAdapter
    private lateinit var bogoGetMenuListAdapter: BogoMenuListAdapter

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var getDiningOptions: HashMap<String, String>
    private lateinit var getServiceAreaOptions: HashMap<String, Any>
    private lateinit var tablesessionserviceData: HashMap<String, String>
    private lateinit var getCashDrawer: HashMap<String, String>
    private lateinit var comboItemTypeList: ArrayList<ComboBogoItemType>
    private lateinit var bogoBuyItemTypeList: ArrayList<ComboBogoItemType>
    private lateinit var bogoGetItemTypeList: ArrayList<ComboBogoItemType>
    private lateinit var getDeviceDetails: HashMap<String, String>

    private var diningOptionSelectedPosition = -1
    private var final_tip_amount = 0.00
    private var update_order = ""
    private var mSelectedIndex = -1
    private var customerId = ""
    private var customerFirstName = ""
    private var customerLastName = ""
    private var customerEmail = ""
    private var customerPhone = ""
    private var mOrderId = ""
    private var mItemId = ""
    private var noOfGuests = "0"
    private var tableNumber = "0"
    private var from_screen = ""
    private var discountApplicable: Boolean = false
    private var discountApplicableItem: Boolean = false
    private var taxAmount = 0.00
    private var isModifierSelected = false
    private var modifier_item_index = -1
    private var isPreModifierSelected = false
    private var deviceToken = RestaurantId.DEVICE_TOKEN
    private var isHavingCustomer = false
    private var isDeliverySelected = false
    private var isDineInSelected = false
    private var isCurbSideSelected = false
    private var isHavingCurbSideInfo = false
    private var isHavingDeliveryInfo = false
    private var isHavingDeliveryDetails = false
    private var isTakeOutSelected = false
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var voidQty = 0
    private var voidReasonId = ""
    private var voidReason = ""
    private var comboBogoId = ""
    private var comboBogoMinimumQty = ""
    private var comboBogoUniqueId = ""
    private var comboPrice = 0.00
    private var isBogoBuySelected = false
    private var isScheduledOrder = false
    private val DISCOUNT_LEVEL_ITEM = "item"
    private val DISCOUNT_LEVEL_CHECK = "check"
    private var checkNumber = 1
    private var splitListPosition = 0
    private var checkLevelApplied = false
    private var discountModeType = ""
    private var discountModeId = ""
    private var bogoGetDiscountValue = 0
    private var bogoGetDiscountValueType = ""
    private var discountApplyType = 0
    private var total_final_tax = 0.00
    private var maxDiscountAmount = 0.00
    private var minDiscountAmount = 0.00
    private var giftCardAmount = 0.00
    private var giftCardType = ""
    private var isOrderSent = false
    private var isStayOrderSelected = false
    private var isStaySelectedAll = false
    private var mTableNumber = 0
    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private var countryId = ""
    private var stateId = ""
    private var cityId = ""
    private var serviceAreaId = ""
    private var revenueCenterId = ""
    private var dataType = ""
    private var notes = ""
    private var transportColor = ""
    private var transportDescription = ""
    private var customer_delivery_address_id = "0" //default
    private var customer_delivery_address_line_1 = ""
    private var customer_delivery_address_line_2 = ""
    private var customer_delivery_address_zip_code = ""
    private var customer_delivery_address_notes = ""
    private var customer_delivery_address_lat = ""
    private var customer_delivery_address_long = ""
    private var customer_delivery_address = ""
    private var mSelectedDeliveryAddressIndex = -1
    private var countryCode = ""
    private var stateName = ""
    private var cityName = ""
    private var cashDrawerId = ""
    private var menuGroupsCount = 0
    private var isTestModeEnabled = false
    private var currencyType = ""
    private var deviceName = ""
    private var deviceNameId = ""

    /* global items */
    var category_array: ArrayList<GetRightMenuResponseList>? = null
    lateinit var categories_LayoutManager: LinearLayoutManager
    lateinit var special_req_dialog: Dialog
    lateinit var gift_sell_card_dialog: Dialog
    lateinit var gift_sell_next_dialog: Dialog
    lateinit var tip_dialog: Dialog
    lateinit var discount_menu_group_dialog: Dialog
    lateinit var add_open_item_dialog: Dialog
    lateinit var open_price_dialog: Dialog
    lateinit var size_based_dialog: Dialog
    lateinit var tv_gift_next_button: TextView
    lateinit var tv_gift_cancel_button: TextView
    lateinit var tv_table_no: TextView
    lateinit var tv_timedatepicker: EditText
    lateinit var tv_sub_total_id: TextView
    lateinit var tv_total_discount_id: TextView
    lateinit var tv_tax_amount_id: TextView
    lateinit var tv_balance_due_id: TextView
    lateinit var tv_final_sum_price: TextView
    lateinit var rv_order_item_id: RecyclerView
    lateinit var img_back_id: ImageView
    lateinit var cv_main_order_id: CardView
    lateinit var cv_dining_opt_id: CardView
    lateinit var cv_sell_gift_card_id: CardView
    lateinit var cv_add_value_id: CardView
    lateinit var tv_main_discount_id: TextView
    lateinit var tv_discount_cancel_btn_id: TextView
    lateinit var tv_discount_remove_btn_id: TextView
    lateinit var ll_discounts_id: LinearLayout
    lateinit var ll_gift_card_id: LinearLayout
    lateinit var ll_gift_content_id: LinearLayout
    lateinit var ll_main_course_id: LinearLayout
    lateinit var ll_menu_item_edit_id: LinearLayout
    lateinit var tv_extra_done_id: TextView
    lateinit var tv_remove_item_id: TextView
    lateinit var ll_edit_item_qty_id: LinearLayout
    lateinit var rv_discounts_list_id: RecyclerView
    lateinit var cv_discount_id: CardView
    lateinit var ll_all_modifiers_id: LinearLayout
    lateinit var ll_modifier_list_id: LinearLayout
    lateinit var rv_modifier_group_id: RecyclerView
    lateinit var rv_modifier_list_id: RecyclerView
    lateinit var rv_pre_modifier_list_id: RecyclerView
    lateinit var rv_discounts_item_id: RecyclerView
    lateinit var ll_bogo_id: LinearLayout
    lateinit var ll_combo_id: LinearLayout
    lateinit var tv_pre_modifiers: TextView
    lateinit var tv_modifiers: TextView
    lateinit var ll_schedule_order: LinearLayout
    var showDatePikerDialog = CustomDatePickerDialog()
    private lateinit var dialog_datepicker: Dialog
    var date = ""
    var selected_date = ""
    var mDay = 0
    var mMonth = 0
    var mYear = 0
    lateinit var mDate: Calendar
    var timeDatePicker = ""
    var dayOfTheWeek = ""
    var currentTime = ""
    var item_qty = 1
    var item_index: Int? = null
    var global_category_menu_group_id = ""
    var global_right_menu_id = ""
    var global_item_id = ""
    var final_sub_total = 0.00
    var discount_amount = 0.00
    var order_number = 0
    var dinningOptionBehaviour = ""
    var dinningName = ""
    var dinningId = ""
    var dinningIdSession = ""
    var dinningOptionBehaviourSession = ""
    var custom_array = ArrayList<OrdersCustomData>()
    var specialReq_array = ArrayList<GetSpecialReq>()
    var discountssArray: ArrayList<GetAllDiscountsListResponse>? = null
    var getdiscountsArray = ArrayList<GetAllDiscountsListResponse>()
    lateinit var orderAdapter: OrderAdapter
    var emptymodifierArray = ArrayList<GetAllModifierListResponseList>()
    var discountID_stg = ""
    var discountname_stg = ""
    var discountValueType_stg = ""
    var discountType_stg = ""
    var discount_int = 0.00
    var discountAmount_int = 0.00
    var total_discountAmount = 0.00
    private var price_total = 0.00
    private var pay_type = "prepaid"
    val subCategoryArray = ArrayList<GetMenuItemsListResponseList>()
    var mDuscountMenuGropArray = ArrayList<MenuItem>()

    /* global items */
    private lateinit var tv_currency_total: TextView
    private lateinit var tv_currency_sub_total: TextView
    private lateinit var tv_currency_tax: TextView
    private lateinit var tv_currency_discount: TextView
    private lateinit var tv_currency_price: TextView
    private lateinit var tv_currency_item_total: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        initialize()
        tv_customer_name_id.isSelected = true
        tv_check_table_dinein_option.isSelected = true
        tv_modifier_item_id.isSelected = true
        tv_currency_total.text = currencyType
        tv_currency_sub_total.text = currencyType
        tv_currency_tax.text = currencyType
        tv_currency_discount.text = currencyType
        tv_currency_price.text = "($currencyType)"
        tv_currency_item_total.text = "($currencyType)"
        if (isTestModeEnabled) {
            tv_test_mode.visibility = View.VISIBLE
        } else {
            tv_test_mode.visibility = View.GONE
        }
        if (dinningId != "") {
            when (dinningOptionBehaviourSession) {
                "delivery" -> {
                    isDeliverySelected = true
                }
                "dine_in" -> {
                    isDineInSelected = true
                }
                "take_out" -> {
                    isTakeOutSelected = true
                }
                "curbside" -> {
                    isCurbSideSelected = true
                }
                else -> {
                }
            }
            tv_dine_in_option_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.graditent_bg
            )
        }
        from_screen = intent.getStringExtra("from_screen")!!
        if (from_screen != "table_service") {
            try {
                val templist = sessionManager.getTempOrderList
                val orderDetails = templist[SessionManager.TEMP_ORDER_LIST]!!
                if (orderDetails != "") {
                    val gson = Gson()
                    val orderDetailsArrayList: ArrayList<OrdersCustomData> = gson.fromJson(
                        orderDetails,
                        object : TypeToken<List<OrdersCustomData?>?>() {}.type
                    )
                    custom_array.addAll(orderDetailsArrayList)
                    fullOrderSetUp()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        try {
            if (intent.getStringExtra("noOfGuests") != null || intent.getStringExtra("noOfGuests")!! != "") {
                noOfGuests = intent.getStringExtra("noOfGuests")!!
            }
            if (intent.getStringExtra("tableNumber") != null || intent.getStringExtra("tableNumber")!! != "") {
                tableNumber = intent.getStringExtra("tableNumber")!!
            }
            if (intent.getStringExtra("serviceAreaId") != "null" || intent.getStringExtra("serviceAreaId") != "") {
                serviceAreaId = intent.getStringExtra("serviceAreaId")!!
            }
            if (intent.getStringExtra("revenueCenterId") != "null" || intent.getStringExtra("revenueCenterId") != "") {
                revenueCenterId = intent.getStringExtra("revenueCenterId")!!
            }
            if (intent.getStringExtra("dataType") != "null" || intent.getStringExtra("dataType") != "") {
                dataType = intent.getStringExtra("dataType")!!
            }
            if (dataType == "table_service") {
                val obj = JSONObject()
                obj.put("tableNumber", tableNumber)
                obj.put("noOfGuest", noOfGuests)
                obj.put("createdBy", mEmployeeId)
                obj.put("restaurantId", restaurantId)
                obj.put("serviceAreaId", serviceAreaId)
                obj.put("status", 1)
                val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.d("TableServiceGuests", finalObject.toString())
                updateTableServiceGuests(finalObject)
            } else if (dataType == "table_service_occupied") {
                val obj = JSONObject()
                obj.put("userId", mEmployeeId)
                obj.put("restaurantId", restaurantId)
                obj.put("tableNumber", tableNumber)
                val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.d("ExistingOrder", finalObject.toString())
                ExistingOrderAPI(finalObject)
            }
            sessionManager.tableServiceData(noOfGuests, tableNumber)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (from_screen == "home") {
            sessionManager.tableServiceData("0", "0")
        }
        tablesessionserviceData = sessionManager.getTableServiceData
        if (tablesessionserviceData[SessionManager.TABLE_NUMBER] == "") {
            tv_table_no.text = "Table No: $tableNumber"
            tv_check_table_dinein_option.text = "#$checkNumber, Table $tableNumber, $dinningName"
        } else {
            tableNumber = tablesessionserviceData[SessionManager.TABLE_NUMBER]!!
            tv_table_no.text = "Table No: " + tablesessionserviceData[SessionManager.TABLE_NUMBER]
            tv_check_table_dinein_option.text =
                "#$checkNumber, Table ${tablesessionserviceData[SessionManager.TABLE_NUMBER]}, $dinningName"
        }
        if (noOfGuests == "" || noOfGuests == "null") {
            tv_edit_guests.text = getString(R.string.guests)
        } else {
            tv_edit_guests.text = noOfGuests + " " + getString(R.string.guests)
        }
        Log.d("table_guests", "$tableNumber=====$noOfGuests")
        if (from_screen == "payment_terminal") {
            tv_remove_item_id.text = getString(R.string.void_stg)
        } else {
            tv_remove_item_id.text = getString(R.string.remove)
        }
        try {
            val obj = JSONObject()
            obj.put("userId", mEmployeeId)
            obj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
            getVoidItemsReasons(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            val obj = JSONObject()
            obj.put("employeeId", mEmployeeId)
            obj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
            checkStayOrdersCount(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            orderDetails = intent.getSerializableExtra("orderDetails")!! as OrderDetailsResponse
            customizeOrderDetails(orderDetails, "")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            val sdf = SimpleDateFormat("EEEE", Locale.getDefault())
            val d = Date()
            dayOfTheWeek = sdf.format(d)
            val df: DateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
            currentTime = df.format(Calendar.getInstance().time)
            Log.e("dayOfTheWeek", dayOfTheWeek + "---" + currentTime)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        ll_switch_user.setOnClickListener {
            val obj = JSONObject()
            obj.put("employeeId", mEmployeeId)
            obj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
            checkStayOrders(finalObject)
            checkStayOrdersCount(finalObject)
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        ll_schedule_order.setOnClickListener {
            val timePickerPopWin =
                TimePickerPopWin.Builder(this,
                    TimePickerPopWin.OnTimePickListener { _, _, _, time ->
                        showDatePikerDialog.showDatePicker(
                            this,
                            tv_timedatepicker.text.toString(),
                            tv_timedatepicker,
                            time
                        )
                        showDatePikerDialog.datePickerDialog.setButton(
                            DialogInterface.BUTTON_NEGATIVE,
                            getString(R.string.cancel)
                        ) { _, which ->
                            if (which == DialogInterface.BUTTON_NEGATIVE) {
                                isScheduledOrder = false
                            }
                        }
                        isScheduledOrder = true
                    }).textConfirm("CONFIRM")
                    .textCancel(getString(R.string.cancel))
                    .btnTextSize(16)
                    .viewTextSize(25)
                    .colorCancel(Color.parseColor("#999999"))
                    .colorConfirm(Color.parseColor("#009900"))
                    .build()
            timePickerPopWin.showPopWin(this)
        }
        img_back_id.setOnClickListener {
            onBackPressed()
        }
        val obj = JSONObject()
        obj.put("userId", mEmployeeId)
        obj.put("restaurantId", restaurantId)
        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
        GetRightMenuResponse(jObject)
        getTaxRates(jObject)
        cv_dining_opt_id.setOnClickListener {
            ll_modifier_list_id.visibility = View.VISIBLE
            ll_all_modifiers_id.visibility = View.VISIBLE
            rv_modifier_group_id.visibility = View.VISIBLE
            rv_modifier_list_id.visibility = View.GONE
            tv_modifiers.visibility = View.GONE
            rv_pre_modifier_list_id.visibility = View.GONE
            tv_pre_modifiers.visibility = View.GONE
            rv_discounts_item_id.visibility = View.GONE
            ll_bogo_id.visibility = View.GONE
            ll_combo_id.visibility = View.GONE
            cv_dining_opt_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.graditent_bg
            )
            cv_discount_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.white_bg_corner_10
            )
            val jObj = JSONObject()
            jObj.put("restaurantId", restaurantId)
            val jsonObject = JsonParser.parseString(jObj.toString()).asJsonObject
            GetDinningOptionsAPI(jsonObject)
        }
        tv_main_discount_id.setOnClickListener {
            Log.e("dinning_data", jObject.toString())
            GetDiscountsAPI(jObject)
        }
        tv_discount_cancel_btn_id.setOnClickListener {
            ll_discounts_id.visibility = View.GONE
        }
        tv_discount_remove_btn_id.visibility = View.GONE
        tv_discount_remove_btn_id.setOnClickListener {
            checkLevelApplied = false
            checkLevelDiscountArray.clear()
            for (i in 0 until custom_array.size) {
                if (custom_array[i].isCombo || custom_array[i].isBogo) {
                    // don't add
                } else {
                    val customData = OrdersCustomData(
                        custom_array[i].basePrice,
                        custom_array[i].discountApplicable,
                        custom_array[i].id,
                        custom_array[i].name,
                        custom_array[i].itemType,
                        custom_array[i].qty,
                        custom_array[i].total,
                        custom_array[i].discount,
                        custom_array[i].restaurantId,
                        custom_array[i].allmodifierArray,
                        custom_array[i].itemId,
                        custom_array[i].menuGroupId,
                        custom_array[i].menuId,
                        false,
                        "",
                        ArrayList(),
                        custom_array[i].itemStatus,
                        custom_array[i].isCombo,
                        custom_array[i].isBogo,
                        custom_array[i].comboOrBogoOfferId,
                        custom_array[i].comboOrBogoUniqueId,
                        custom_array[i].voidStatus,
                        custom_array[i].voidQuantity,
                        custom_array[i].aplicableTaxRates,
                        custom_array[i].taxAppliedList,
                        custom_array[i].diningOptionTaxException,
                        custom_array[i].diningTaxOption,
                        custom_array[i].taxIncludeOption
                    )
                    custom_array[i] = customData
                }
            }
            fullOrderSetUp()
        }
        tv_discount_done_btn_id.setOnClickListener {
            ll_discounts_id.visibility = View.GONE
            rv_discounts_list_id.visibility = View.GONE
        }
        cv_discount_id.setOnClickListener {
            rv_discounts_item_id.visibility = View.VISIBLE
            rv_modifier_list_id.visibility = View.GONE
            tv_modifiers.visibility = View.GONE
            rv_pre_modifier_list_id.visibility = View.GONE
            tv_pre_modifiers.visibility = View.GONE
            ll_modifier_list_id.visibility = View.GONE
            cv_dining_opt_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.white_bg_corner_10
            )
            val jObj = JSONObject()
            jObj.put("userId", mEmployeeId)
            jObj.put("itemId", mItemId)
            jObj.put("restaurantId", restaurantId)
            val jsonObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.e("DISCOUNTITEM", "" + jsonObject)
            GetDiscountsItemAPI(jsonObject)
        }
        ll_gift_card_id.background = ContextCompat.getDrawable(
            this@OrderDetailsActivity,
            R.drawable.light_gray_background
        )
        ll_gift_content_id.visibility = View.GONE
        ll_main_course_id.visibility = View.VISIBLE
        ll_discounts_id.visibility = View.GONE
        rv_discounts_list_id.visibility = View.GONE
        ll_bogo_id.visibility = View.GONE
        ll_combo_id.visibility = View.GONE
        ll_gift_card_id.setOnClickListener {
            ll_gift_card_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.graditent_bg
            )
            ll_main_course_id.visibility = View.GONE
            ll_edit_item_qty_id.visibility = View.GONE
            rv_modifier_list_id.visibility = View.GONE
            tv_modifiers.visibility = View.GONE
            rv_pre_modifier_list_id.visibility = View.GONE
            tv_pre_modifiers.visibility = View.GONE
            ll_all_modifiers_id.visibility = View.GONE
            ll_modifier_list_id.visibility = View.GONE
            ll_gift_content_id.visibility = View.VISIBLE
            ll_menu_item_edit_id.visibility = View.VISIBLE
            ll_bogo_id.visibility = View.GONE
            ll_combo_id.visibility = View.GONE
        }
        tv_extra_done_id.setOnClickListener {
            item_index = -1
            ll_gift_card_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.light_gray_background
            )
            ll_main_course_id.visibility = View.VISIBLE
            ll_edit_item_qty_id.visibility = View.VISIBLE
            ll_menu_item_edit_id.visibility = View.GONE
            ll_bogo_id.visibility = View.GONE
            ll_combo_id.visibility = View.GONE
        }
        cv_special_req_id.setOnClickListener {
            specialRequestDialog()
        }
        cv_balance_enq_id.setOnClickListener {
            giftCardDialog("check_balance")
        }
        cv_sell_gift_card_id.setOnClickListener {
            sellValueDailog()
        }
        cv_add_value_id.setOnClickListener {
            giftCardDialog("add_value")
        }
        tv_remove_item_id.setOnClickListener {
            if (from_screen == "payment_terminal") {
                if (custom_array[item_index!!].itemId == custom_array[item_index!!].id) {
                    custom_array.removeAt(item_index!!)
                    handleDiscounts()
                    GetRightMenuResponse(jObject)
                    fullOrderSetUp()
                    ll_gift_card_id.background = ContextCompat.getDrawable(
                        this,
                        R.drawable.light_gray_background
                    )
                    ll_main_course_id.visibility = View.VISIBLE
                    ll_edit_item_qty_id.visibility = View.VISIBLE
                    ll_menu_item_edit_id.visibility = View.GONE
                    ll_bogo_id.visibility = View.GONE
                    ll_combo_id.visibility = View.GONE
                } else {
                    showVoidReasonDialog()
                }
            } else {
                custom_array.removeAt(item_index!!)
                handleDiscounts()
                GetRightMenuResponse(jObject)
                fullOrderSetUp()
                ll_gift_card_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.light_gray_background
                )
                ll_main_course_id.visibility = View.VISIBLE
                ll_edit_item_qty_id.visibility = View.VISIBLE
                ll_menu_item_edit_id.visibility = View.GONE
                ll_bogo_id.visibility = View.GONE
                ll_combo_id.visibility = View.GONE
            }
        }
        cv_plus_id.setOnClickListener {
            item_qty++
            tv_set_qty_id.text = item_qty.toString()
            val taxAppliedList: ArrayList<SendTaxRates>
            if (custom_array[item_index!!].taxIncludeOption) {
                taxAppliedList = taxCalculationNew(
                    item_qty.toDouble(),
                    custom_array[item_index!!].basePrice.toDouble(),
                    custom_array[item_index!!].aplicableTaxRates
                )
            } else {
                taxAppliedList = ArrayList()
            }
            val modifiersArray = custom_array[item_index!!].allmodifierArray
            for (i in 0 until modifiersArray.size) {
                val modifierItem = modifiersArray[i]
                //if (modifierItem.id != "") {
                modifierItem.total = item_qty * modifierItem.price
                //}
                modifiersArray[i] = modifierItem
            }
            val customOrderData = OrdersCustomData(
                custom_array[item_index!!].basePrice,
                custom_array[item_index!!].discountApplicable,
                custom_array[item_index!!].id,
                custom_array[item_index!!].name,
                custom_array[item_index!!].itemType,
                item_qty.toString(),
                (item_qty * custom_array[item_index!!].basePrice.toDouble()).toString(),
                custom_array[item_index!!].discount,
                custom_array[item_index!!].restaurantId,
                modifiersArray,
                custom_array[item_index!!].itemId,
                custom_array[item_index!!].menuGroupId,
                custom_array[item_index!!].menuId,
                custom_array[item_index!!].discountApplied,
                custom_array[item_index!!].discountAppliedType,
                custom_array[item_index!!].discountsArray,
                custom_array[item_index!!].itemStatus,
                custom_array[item_index!!].isCombo,
                custom_array[item_index!!].isBogo,
                custom_array[item_index!!].comboOrBogoOfferId,
                custom_array[item_index!!].comboOrBogoUniqueId,
                custom_array[item_index!!].voidStatus,
                custom_array[item_index!!].voidQuantity,
                custom_array[item_index!!].aplicableTaxRates,
                taxAppliedList,
                custom_array[item_index!!].diningOptionTaxException,
                custom_array[item_index!!].diningTaxOption,
                custom_array[item_index!!].taxIncludeOption
            )
            custom_array[item_index!!] = customOrderData
            if (custom_array[item_index!!].discountAppliedType == DISCOUNT_LEVEL_ITEM) {
                if (custom_array[item_index!!].discountApplicable) {
                    if ((item_qty.toDouble() * custom_array[item_index!!].basePrice.toDouble()) >= custom_array[item_index!!].discountsArray[0].minDiscountAmount) {
                        val orderCustomData = OrdersCustomData(
                            custom_array[item_index!!].basePrice,
                            custom_array[item_index!!].discountApplicable,
                            custom_array[item_index!!].id,
                            custom_array[item_index!!].name,
                            custom_array[item_index!!].itemType,
                            custom_array[item_index!!].qty,
                            custom_array[item_index!!].total,
                            custom_array[item_index!!].discount,
                            custom_array[item_index!!].restaurantId,
                            custom_array[item_index!!].allmodifierArray,
                            custom_array[item_index!!].itemId,
                            custom_array[item_index!!].menuGroupId,
                            custom_array[item_index!!].menuId,
                            custom_array[item_index!!].discountApplied,
                            custom_array[item_index!!].discountAppliedType,
                            custom_array[item_index!!].discountsArray,
                            custom_array[item_index!!].itemStatus,
                            custom_array[item_index!!].isCombo,
                            custom_array[item_index!!].isBogo,
                            custom_array[item_index!!].comboOrBogoOfferId,
                            custom_array[item_index!!].comboOrBogoUniqueId,
                            custom_array[item_index!!].voidStatus,
                            custom_array[item_index!!].voidQuantity,
                            custom_array[item_index!!].aplicableTaxRates,
                            custom_array[item_index!!].taxAppliedList,
                            custom_array[item_index!!].diningOptionTaxException,
                            custom_array[item_index!!].diningTaxOption,
                            custom_array[item_index!!].taxIncludeOption
                        )
                        custom_array[item_index!!] = orderCustomData
                    } else {
                        val orderCustomData = OrdersCustomData(
                            custom_array[item_index!!].basePrice,
                            custom_array[item_index!!].discountApplicable,
                            custom_array[item_index!!].id,
                            custom_array[item_index!!].name,
                            custom_array[item_index!!].itemType,
                            custom_array[item_index!!].qty,
                            custom_array[item_index!!].total,
                            custom_array[item_index!!].discount,
                            custom_array[item_index!!].restaurantId,
                            custom_array[item_index!!].allmodifierArray,
                            custom_array[item_index!!].itemId,
                            custom_array[item_index!!].menuGroupId,
                            custom_array[item_index!!].menuId,
                            false,
                            "",
                            ArrayList(),
                            custom_array[item_index!!].itemStatus,
                            custom_array[item_index!!].isCombo,
                            custom_array[item_index!!].isBogo,
                            custom_array[item_index!!].comboOrBogoOfferId,
                            custom_array[item_index!!].comboOrBogoUniqueId,
                            custom_array[item_index!!].voidStatus,
                            custom_array[item_index!!].voidQuantity,
                            custom_array[item_index!!].aplicableTaxRates,
                            custom_array[item_index!!].taxAppliedList,
                            custom_array[item_index!!].diningOptionTaxException,
                            custom_array[item_index!!].diningTaxOption,
                            custom_array[item_index!!].taxIncludeOption
                        )
                        custom_array[item_index!!] = orderCustomData
                    }
                } else {
                    val orderCustomData = OrdersCustomData(
                        custom_array[item_index!!].basePrice,
                        custom_array[item_index!!].discountApplicable,
                        custom_array[item_index!!].id,
                        custom_array[item_index!!].name,
                        custom_array[item_index!!].itemType,
                        custom_array[item_index!!].qty,
                        custom_array[item_index!!].total,
                        custom_array[item_index!!].discount,
                        custom_array[item_index!!].restaurantId,
                        custom_array[item_index!!].allmodifierArray,
                        custom_array[item_index!!].itemId,
                        custom_array[item_index!!].menuGroupId,
                        custom_array[item_index!!].menuId,
                        false,
                        "",
                        ArrayList(),
                        custom_array[item_index!!].itemStatus,
                        custom_array[item_index!!].isCombo,
                        custom_array[item_index!!].isBogo,
                        custom_array[item_index!!].comboOrBogoOfferId,
                        custom_array[item_index!!].comboOrBogoUniqueId,
                        custom_array[item_index!!].voidStatus,
                        custom_array[item_index!!].voidQuantity,
                        custom_array[item_index!!].aplicableTaxRates,
                        custom_array[item_index!!].taxAppliedList,
                        custom_array[item_index!!].diningOptionTaxException,
                        custom_array[item_index!!].diningTaxOption,
                        custom_array[item_index!!].taxIncludeOption
                    )
                    custom_array[item_index!!] = orderCustomData
                }
            } else {
                handleDiscounts()
            }
            fullOrderSetUp()
        }
        cv_minus_id.setOnClickListener {
            if (item_qty == 1) {
                // do nothing
            } else {
                item_qty--
                tv_set_qty_id.text = item_qty.toString()
                val taxAppliedList: ArrayList<SendTaxRates>
                if (custom_array[item_index!!].taxIncludeOption) {
                    taxAppliedList = taxCalculationNew(
                        item_qty.toDouble(),
                        custom_array[item_index!!].basePrice.toDouble(),
                        custom_array[item_index!!].aplicableTaxRates
                    )
                } else {
                    taxAppliedList = ArrayList()
                }
                val modifiersArray: ArrayList<GetAllModifierListResponseList> =
                    custom_array[item_index!!].allmodifierArray
                for (i in 0 until modifiersArray.size) {
                    val modifierItem = modifiersArray[i]
                    // if (modifierItem.id != "") {
                    modifierItem.total = item_qty * modifierItem.price
                    // }
                    modifiersArray[i] = modifierItem
                }
                val customOrderData = OrdersCustomData(
                    custom_array[item_index!!].basePrice,
                    custom_array[item_index!!].discountApplicable,
                    custom_array[item_index!!].id,
                    custom_array[item_index!!].name,
                    custom_array[item_index!!].itemType,
                    item_qty.toString(),
                    (item_qty * custom_array[item_index!!].basePrice.toDouble()).toString(),
                    custom_array[item_index!!].discount,
                    custom_array[item_index!!].restaurantId,
                    modifiersArray,
                    custom_array[item_index!!].itemId,
                    custom_array[item_index!!].menuGroupId,
                    custom_array[item_index!!].menuId,
                    custom_array[item_index!!].discountApplied,
                    custom_array[item_index!!].discountAppliedType,
                    custom_array[item_index!!].discountsArray,
                    custom_array[item_index!!].itemStatus,
                    custom_array[item_index!!].isCombo,
                    custom_array[item_index!!].isBogo,
                    custom_array[item_index!!].comboOrBogoOfferId,
                    custom_array[item_index!!].comboOrBogoUniqueId,
                    custom_array[item_index!!].voidStatus,
                    custom_array[item_index!!].voidQuantity,
                    custom_array[item_index!!].aplicableTaxRates,
                    taxAppliedList,
                    custom_array[item_index!!].diningOptionTaxException,
                    custom_array[item_index!!].diningTaxOption,
                    custom_array[item_index!!].taxIncludeOption
                )
                custom_array[item_index!!] = customOrderData
                if (custom_array[item_index!!].discountAppliedType == DISCOUNT_LEVEL_ITEM) {
                    if (custom_array[item_index!!].discountApplicable) {
                        if ((item_qty.toDouble() * custom_array[item_index!!].basePrice.toDouble()) >= custom_array[item_index!!].discountsArray[0].minDiscountAmount) {
                            val orderCustomData = OrdersCustomData(
                                custom_array[item_index!!].basePrice,
                                custom_array[item_index!!].discountApplicable,
                                custom_array[item_index!!].id,
                                custom_array[item_index!!].name,
                                custom_array[item_index!!].itemType,
                                custom_array[item_index!!].qty,
                                custom_array[item_index!!].total,
                                custom_array[item_index!!].discount,
                                custom_array[item_index!!].restaurantId,
                                custom_array[item_index!!].allmodifierArray,
                                custom_array[item_index!!].itemId,
                                custom_array[item_index!!].menuGroupId,
                                custom_array[item_index!!].menuId,
                                custom_array[item_index!!].discountApplied,
                                custom_array[item_index!!].discountAppliedType,
                                custom_array[item_index!!].discountsArray,
                                custom_array[item_index!!].itemStatus,
                                custom_array[item_index!!].isCombo,
                                custom_array[item_index!!].isBogo,
                                custom_array[item_index!!].comboOrBogoOfferId,
                                custom_array[item_index!!].comboOrBogoUniqueId,
                                custom_array[item_index!!].voidStatus,
                                custom_array[item_index!!].voidQuantity,
                                custom_array[item_index!!].aplicableTaxRates,
                                custom_array[item_index!!].taxAppliedList,
                                custom_array[item_index!!].diningOptionTaxException,
                                custom_array[item_index!!].diningTaxOption,
                                custom_array[item_index!!].taxIncludeOption
                            )
                            custom_array[item_index!!] = orderCustomData
                        } else {
                            val orderCustomData = OrdersCustomData(
                                custom_array[item_index!!].basePrice,
                                custom_array[item_index!!].discountApplicable,
                                custom_array[item_index!!].id,
                                custom_array[item_index!!].name,
                                custom_array[item_index!!].itemType,
                                custom_array[item_index!!].qty,
                                custom_array[item_index!!].total,
                                custom_array[item_index!!].discount,
                                custom_array[item_index!!].restaurantId,
                                custom_array[item_index!!].allmodifierArray,
                                custom_array[item_index!!].itemId,
                                custom_array[item_index!!].menuGroupId,
                                custom_array[item_index!!].menuId,
                                false,
                                "",
                                ArrayList(),
                                custom_array[item_index!!].itemStatus,
                                custom_array[item_index!!].isCombo,
                                custom_array[item_index!!].isBogo,
                                custom_array[item_index!!].comboOrBogoOfferId,
                                custom_array[item_index!!].comboOrBogoUniqueId,
                                custom_array[item_index!!].voidStatus,
                                custom_array[item_index!!].voidQuantity,
                                custom_array[item_index!!].aplicableTaxRates,
                                custom_array[item_index!!].taxAppliedList,
                                custom_array[item_index!!].diningOptionTaxException,
                                custom_array[item_index!!].diningTaxOption,
                                custom_array[item_index!!].taxIncludeOption
                            )
                            custom_array[item_index!!] = orderCustomData
                        }
                    } else {
                        val orderCustomData = OrdersCustomData(
                            custom_array[item_index!!].basePrice,
                            custom_array[item_index!!].discountApplicable,
                            custom_array[item_index!!].id,
                            custom_array[item_index!!].name,
                            custom_array[item_index!!].itemType,
                            custom_array[item_index!!].qty,
                            custom_array[item_index!!].total,
                            custom_array[item_index!!].discount,
                            custom_array[item_index!!].restaurantId,
                            custom_array[item_index!!].allmodifierArray,
                            custom_array[item_index!!].itemId,
                            custom_array[item_index!!].menuGroupId,
                            custom_array[item_index!!].menuId,
                            false,
                            "",
                            ArrayList(),
                            custom_array[item_index!!].itemStatus,
                            custom_array[item_index!!].isCombo,
                            custom_array[item_index!!].isBogo,
                            custom_array[item_index!!].comboOrBogoOfferId,
                            custom_array[item_index!!].comboOrBogoUniqueId,
                            custom_array[item_index!!].voidStatus,
                            custom_array[item_index!!].voidQuantity,
                            custom_array[item_index!!].aplicableTaxRates,
                            custom_array[item_index!!].taxAppliedList,
                            custom_array[item_index!!].diningOptionTaxException,
                            custom_array[item_index!!].diningTaxOption,
                            custom_array[item_index!!].taxIncludeOption
                        )
                        custom_array[item_index!!] = orderCustomData
                    }
                } else {
                    handleDiscounts()
                }
                fullOrderSetUp()
            }
        }
        tv_cancel.setOnClickListener {
            if (custom_array.size > 0) {
                if (isStayOrderSelected) {
                    sessionManager.saveTempOrderList("")
                    finish()
                    startActivity(intent)
                } else {
                    custom_array.clear()
                    GetRightMenuResponse(jObject)
                    fullOrderSetUp()
                    checkLevelDiscountArray.clear()
                    checkLevelApplied = false
                    isScheduledOrder = false
                    ll_main_course_id.visibility = View.VISIBLE
                    ll_edit_item_qty_id.visibility = View.VISIBLE
                    ll_menu_item_edit_id.visibility = View.GONE
                    ll_bogo_id.visibility = View.GONE
                    ll_combo_id.visibility = View.GONE
                }
            }
        }
        tv_stay_order_id.setOnClickListener {
            if (custom_array.size > 0) {
                pay_type = "stay"
                CreateAddOrder(0, "stay")
            }
        }
        tv_send_order_btn_id.setOnClickListener {
            if (dinningId == "") {
                CenterToast().showToast(this@OrderDetailsActivity,"Please choose any dinning option!")
            } else if (custom_array.size > 0) {
                pay_type = "send"
                if (!isHavingCustomer) {
                    searchUser()
                } else {
                    CreateAddOrder(1, "send")
                }
            }
        }
        tv_pay_bill_order_id.setOnClickListener {
            if (dinningId == "") {
                CenterToast().showToast(this@OrderDetailsActivity,"Please choose any dinning option!")
            } else if (custom_array.size > 0) {
                pay_type = "send_pay"
                if (!isHavingCustomer) {
                    searchUser()
                } else {
                    TipAmountDailog()
                }
            }
        }
        tv_edit_table.setOnClickListener {
            editTable()
        }
        tv_edit_guests.setOnClickListener {
            editGuests()
        }
        ll_show_stay_orders.setOnClickListener {
            showStayOrders()
        }
        if (from_screen == "payment_terminal") {
            tv_order_no_id.text = "Order #$order_number"
        } else {
            if (dataType == "table_service_occupied") {
                // do nothing
            } else {
                val mOrderNoobj = JSONObject()
                mOrderNoobj.put("restaurantId", restaurantId)
                val mOrderjObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.e("order_no_get", mOrderjObject.toString())
                GetOrderNoAPI(mOrderjObject)
            }
        }
        tv_dine_in_option_id.setOnClickListener {
            tv_dine_in_option_id.background = ContextCompat.getDrawable(
                this@OrderDetailsActivity,
                R.drawable.graditent_bg
            )
            ll_dinein_option_id.visibility = View.VISIBLE
            val jObj = JSONObject()
            jObj.put("restaurantId", restaurantId)
            val jsonObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.e("dinning_data", jsonObject.toString())
            GetDinningOptionsAPI(jsonObject)
        }
        tv_done_dine_in_btn_id.setOnClickListener {
            ll_dinein_option_id.visibility = View.GONE
            if (dinningId == "") {
                tv_dine_in_option_id.background =
                    ContextCompat.getDrawable(
                        this@OrderDetailsActivity, R.drawable.gray_background
                    )
            } else {
                tv_dine_in_option_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.graditent_bg
                )
            }
        }
        tv_cancel_dine_in_btn_id.setOnClickListener {
            ll_dinein_option_id.visibility = View.GONE
            if (dinningId == "") {
                tv_dine_in_option_id.background =
                    ContextCompat.getDrawable(
                        this@OrderDetailsActivity, R.drawable.gray_background
                    )
            } else {
                tv_dine_in_option_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.graditent_bg
                )
            }
        }
        tv_add_open_item_id.setOnClickListener {
            GetAllOpenItemsAPI(jObject)
        }
        cv_home_id.setOnClickListener {
            val intent = Intent(this@OrderDetailsActivity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
        tv_checks.setOnClickListener {
            showTableOrders(tableChecks)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        getDiningOptions = sessionManager.getDiningOptions
        getServiceAreaOptions = sessionManager.getServiceAreaOptions
        getCashDrawer = sessionManager.getCashDrawer
        cashDrawerId = getCashDrawer[SessionManager.CASH_DRAWER_ID_KEY]!!
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        dinningOptionBehaviour = getDiningOptions[SessionManager.DINING_OPTION_BEHAVIOUR_KEY]!!
        dinningOptionBehaviourSession =
            getDiningOptions[SessionManager.DINING_OPTION_BEHAVIOUR_KEY]!!
        dinningId = getDiningOptions[SessionManager.DINING_OPTION_ID_KEY]!!
        dinningName = getDiningOptions[SessionManager.DINING_OPTION_NAME_KEY]!!
        serviceAreaId = getServiceAreaOptions[SessionManager.SERVICE_AREA_ID_KEY].toString()
        revenueCenterId = getServiceAreaOptions[SessionManager.REVENUE_CENTER_ID_KEY].toString()
        getDeviceDetails = sessionManager.getDeviceDetails
        deviceName = getDeviceDetails[SessionManager.DEVICE_NAME]!!
        deviceNameId = getDeviceDetails[SessionManager.DEVICE_NAME_ID]!!
        isTestModeEnabled = sessionManager.getRestaurantMode
        currencyType = sessionManager.getCurrencyType
        tv_sub_total_id = findViewById(R.id.tv_sub_total_id)
        tv_total_discount_id = findViewById(R.id.tv_total_discount_id)
        tv_tax_amount_id = findViewById(R.id.tv_tax_amount_id)
        tv_balance_due_id = findViewById(R.id.tv_balance_due_id)
        tv_final_sum_price = findViewById(R.id.tv_final_sum_price)
        rv_order_item_id = findViewById(R.id.rv_order_item_id)
        img_back_id = findViewById(R.id.img_back_id)
        cv_main_order_id = findViewById(R.id.cv_main_order_id)
        cv_dining_opt_id = findViewById(R.id.cv_dining_opt_id)
        cv_sell_gift_card_id = findViewById(R.id.cv_sell_gift_card_id)
        cv_add_value_id = findViewById(R.id.cv_add_value_id)
        tv_main_discount_id = findViewById(R.id.tv_main_discount_id)
        tv_discount_cancel_btn_id = findViewById(R.id.tv_discount_cancel_btn_id)
        tv_discount_remove_btn_id = findViewById(R.id.tv_discount_remove_btn_id)
        ll_discounts_id = findViewById(R.id.ll_discounts_id)
        ll_gift_card_id = findViewById(R.id.ll_gift_card_id)
        ll_gift_content_id = findViewById(R.id.ll_gift_content_id)
        ll_main_course_id = findViewById(R.id.ll_main_course_id)
        ll_edit_item_qty_id = findViewById(R.id.ll_edit_item_qty_id)
        ll_menu_item_edit_id = findViewById(R.id.ll_menu_item_edit_id)
        tv_extra_done_id = findViewById(R.id.tv_extra_done_id)
        tv_remove_item_id = findViewById(R.id.tv_remove_item_id)
        rv_discounts_list_id = findViewById(R.id.rv_discounts_list_id)
        cv_discount_id = findViewById(R.id.cv_discount_id)
        rv_modifier_list_id = findViewById(R.id.rv_modifier_list_id)
        ll_modifier_list_id = findViewById(R.id.ll_modifier_list_id)
        ll_all_modifiers_id = findViewById(R.id.ll_all_modifiers_id)
        rv_modifier_group_id = findViewById(R.id.rv_modifier_group_id)
        rv_pre_modifier_list_id = findViewById(R.id.rv_pre_modifier_list_id)
        rv_discounts_item_id = findViewById(R.id.rv_discounts_item_id)
        ll_bogo_id = findViewById(R.id.ll_bogo_id)
        ll_combo_id = findViewById(R.id.ll_combo_id)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tv_pre_modifiers = findViewById(R.id.tv_pre_modifiers)
        tv_modifiers = findViewById(R.id.tv_modifiers)
        tv_table_no = findViewById(R.id.tv_table_no)
        tv_timedatepicker = findViewById(R.id.tv_timedatepicker)
        ll_schedule_order = findViewById(R.id.ll_schedule_order)
        tv_cancel = findViewById(R.id.tv_cancel)
        tv_edit_table = findViewById(R.id.tv_edit_table)
        tv_edit_guests = findViewById(R.id.tv_edit_guests)
        ll_show_stay_orders = findViewById(R.id.ll_show_stay_orders)
        tv_stay_order_count = findViewById(R.id.tv_stay_order_count)
        tv_checks = findViewById(R.id.tv_checks)
        tv_test_mode = findViewById(R.id.tv_test_mode)
        tv_currency_total = findViewById(R.id.tv_currency_total)
        tv_currency_sub_total = findViewById(R.id.tv_currency_sub_total)
        tv_currency_tax = findViewById(R.id.tv_currency_tax)
        tv_currency_discount = findViewById(R.id.tv_currency_discount)
        tv_currency_price = findViewById(R.id.tv_currency_price)
        tv_currency_item_total = findViewById(R.id.tv_currency_item_total)

        checkLevelDiscountArray = ArrayList()
        custom_array.clear()
        comboItemTypeList = ArrayList()
        bogoBuyItemTypeList = ArrayList()
        bogoGetItemTypeList = ArrayList()
        customerAddresesList = ArrayList()
        countriesList = ArrayList()
        statesList = ArrayList()
        citiesList = ArrayList()
        dummyCountry = CountriesResponse()
        dummyCountry._id = "dummy"
        dummyCountry.capital = ""
        dummyCountry.countryCode = ""
        dummyCountry.countryId = 0
        dummyCountry.currency = ""
        dummyCountry.iso3 = ""
        dummyCountry.name = "Please select country."
        dummyCountry.phoneCode = ""
        dummyCountry.status = 0

        dummyState = StatesResponse()
        dummyState._id = "dummy"
        dummyState.countryCode = ""
        dummyState.countryId = 0
        dummyState.name = "Please select state."
        dummyState.stateCode = ""
        dummyState.stateId = 0
        dummyState.status = 0

        dummyCity = CitiesResponse()
        dummyCity._id = "dummy"
        dummyCity.cityId = 0
        dummyCity.countryCode = ""
        dummyCity.countryId = 0
        dummyCity.latitude = ""
        dummyCity.longitude = ""
        dummyCity.name = "Please select city."
        dummyCity.stateCode = ""
        dummyCity.stateId = 0
        dummyCity.status = 0
    }

    inner class CategoryListAdapter(
        context: Context,
        val mCategoryArray: ArrayList<GetRightMenuResponseList>
    ) :
        RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = 0

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.category_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mCategoryArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = mCategoryArray[position].name
            holder.ll_title_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                val obj = JSONObject()
                obj.put("menuGroupId", mCategoryArray[position].id)
                obj.put("menuId", global_right_menu_id)
                obj.put("restaurantId", restaurantId)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.e("sub_group_data", jObject.toString())
                GetSubCategotyResponse(jObject, true)
            }
            if (selectedPosition == position) {
                holder.ll_title_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.graditent_bg
                )
                global_category_menu_group_id = mCategoryArray[position].id!!.toString()
            } else {
                holder.ll_title_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.gray_background
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val ll_title_id = view.findViewById<LinearLayout>(R.id.ll_title_id)
        }

    }

    inner class SubListAdapter(
        context: Context,
        val sub_listArray: ArrayList<GetMenuItemsListResponseList>
    ) :
        RecyclerView.Adapter<SubListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var price_provider = ""

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.sub_category_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return sub_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = sub_listArray[position].name
            holder.cv_sub_list_id.setOnClickListener {
                val aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>
                if (sub_listArray[position].taxIncludeOption!!) {
                    aplicableTaxRates = sub_listArray[position].aplicableTaxRates!!
                } else {
                    aplicableTaxRates = ArrayList()
                }
                discountApplicable = sub_listArray[position].discountApplicable
                Log.e("discountApplicable", "" + discountApplicable)
                var base_price_final = ""
                /*PriceStrategy 1 = Base price
                                2 = Size list price
                                3 = Base price
                                4 = Time Based
                                5 = Open Price */
                Log.e("pricingStrategy", sub_listArray[position].pricingStrategy!!)
                if (sub_listArray[position].pricingStrategy.equals("1")) {
                    price_provider = "1"
                    base_price_final = sub_listArray[position].basePrice!!.toString()
                } else if (sub_listArray[position].pricingStrategy.equals("2")) {
                    price_provider = "2"
                    SizedBasedPriceDailog(
                        sub_listArray[position].sizeList!!,
                        "",
                        sub_listArray[position].id!!,
                        sub_listArray[position].name!!,
                        sub_listArray[position].qty.toString(),
                        "",
                        "",
                        sub_listArray[position].restaurantId!!,
                        ArrayList(),
                        global_category_menu_group_id,
                        global_right_menu_id,
                        sub_listArray[position].itemStatus,
                        "",
                        aplicableTaxRates,
                        sub_listArray[position].diningOptionTaxException!!,
                        sub_listArray[position].diningTaxOption!!,
                        sub_listArray[position].taxIncludeOption!!
                    )
                } else if (sub_listArray[position].pricingStrategy.equals("3")) {
                    price_provider = "1"
                    base_price_final =
                        sub_listArray[position].menuPriceList!![0].price!!.toString()
                } else if (sub_listArray[position].pricingStrategy.equals("4")) {
                    var timeBased = false
                    for (k in 0 until sub_listArray[position].timePriceList!!.size) {
                        for (m in 0 until sub_listArray[position].timePriceList!![k].days!!.size) {
                            if (dayOfTheWeek == sub_listArray[position].timePriceList!![k].days!![m]) {
                                try {
                                    val dateFormat = SimpleDateFormat("HH:mm")
                                    val currentDate: Date = dateFormat.parse(currentTime)!!
                                    val timeFrom: Date =
                                        dateFormat.parse(sub_listArray[position].timePriceList!![k].timeFrom!!)!!
                                    val timeTo: Date =
                                        dateFormat.parse(sub_listArray[position].timePriceList!![k].timeTo!!)!!
                                    if (currentDate.after(timeFrom) && currentDate.before(timeTo)) {
                                        println("datetrue")
                                        Log.d("date----", "$timeFrom=====$timeTo====$currentDate")
                                        timeBased = true
                                        base_price_final =
                                            sub_listArray[position].timePriceList!![k].price!!
                                        break
                                    }
                                } catch (e: java.lang.Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    if (!timeBased) {
                        base_price_final = sub_listArray[position].basePrice!!
                    }
                    price_provider = "1"
                } else if (sub_listArray[position].pricingStrategy.equals("5")) {
                    price_provider = "0"
                    OpenValueSetDailog(
                        base_price_final,
                        sub_listArray[position].id!!,
                        sub_listArray[position].name!!,
                        sub_listArray[position].qty.toString(),
                        "",
                        "",
                        sub_listArray[position].restaurantId!!,
                        ArrayList(),
                        global_category_menu_group_id,
                        global_right_menu_id,
                        sub_listArray[position].itemStatus,
                        "",
                        aplicableTaxRates,
                        sub_listArray[position].diningOptionTaxException!!,
                        sub_listArray[position].diningTaxOption!!,
                        sub_listArray[position].taxIncludeOption!!
                    )
                    return@setOnClickListener
                }
                global_item_id = sub_listArray[position].id!!
                if (price_provider == "1") {
                    val taxAppliedList: ArrayList<SendTaxRates>
                    if (sub_listArray[position].taxIncludeOption!!) {
                        taxAppliedList = taxCalculationNew(
                            sub_listArray[position].qty.toDouble(),
                            base_price_final.toDouble(),
                            sub_listArray[position].aplicableTaxRates!!
                        )
                    } else {
                        taxAppliedList = ArrayList()
                    }
                    val customOrderData = OrdersCustomData(
                        base_price_final,
                        sub_listArray[position].discountApplicable,
                        sub_listArray[position].id!!,
                        sub_listArray[position].name!!,
                        "normal",
                        sub_listArray[position].qty.toString(),
                        (sub_listArray[position].qty * base_price_final.toDouble()).toString(),
                        "",
                        sub_listArray[position].restaurantId!!,
                        ArrayList(),
                        sub_listArray[position].id!!,
                        global_category_menu_group_id,
                        global_right_menu_id,
                        false,
                        "",
                        ArrayList(),
                        sub_listArray[position].itemStatus,
                        isCombo = false,
                        isBogo = false,
                        comboOrBogoOfferId = "",
                        comboOrBogoUniqueId = "",
                        voidStatus = false,
                        voidQuantity = 0,
                        aplicableTaxRates = aplicableTaxRates,
                        taxAppliedList = taxAppliedList,
                        diningOptionTaxException = sub_listArray[position].diningOptionTaxException!!,
                        diningTaxOption = sub_listArray[position].diningTaxOption!!,
                        taxIncludeOption = sub_listArray[position].taxIncludeOption!!
                    )
                    // adding item to order list
                    custom_array.add(customOrderData)
                    handleDiscounts()
                    fullOrderSetUp()
                }
                holder.cv_sub_list_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.graditent_bg
                )
                holder.tv_title_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            }
            if (custom_array.size > 0) {
                for (i in 0 until custom_array.size) {
                    if (!custom_array[i].isBogo && !custom_array[i].isCombo) {
                        if (custom_array[i].itemId == sub_listArray[position].id) {
                            holder.cv_sub_list_id.background = ContextCompat.getDrawable(
                                mContext!!,
                                R.drawable.graditent_bg
                            )
                            holder.tv_title_id.setTextColor(
                                ContextCompat.getColor(
                                    mContext!!,
                                    R.color.white
                                )
                            )
                        }
                    }
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_sub_list_id = view.findViewById<CardView>(R.id.cv_sub_list_id)
        }

    }

    inner class ModifirGroupAdapter(
        context: Context,
        val modifier_group_listArray: ArrayList<GetModifierGroupListResponseList>
    ) :
        RecyclerView.Adapter<ModifirGroupAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var max_Selection = 0
        var min_Selection = 0

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.modifier_group_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return modifier_group_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            if (custom_array[item_index!!].allmodifierArray.size > 0) {
                for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                    if (custom_array[item_index!!].allmodifierArray[i].modifierGroupId.size > 0) {
                        for (k in 0 until custom_array[item_index!!].allmodifierArray[i].modifierGroupId.size) {
                            if (custom_array[item_index!!].allmodifierArray[i].modifierGroupId[k].id == modifier_group_listArray[position].id) {
                                holder.cv_modifier_group_list_id.background =
                                    ContextCompat.getDrawable(mContext!!, R.drawable.graditent_bg)
                                holder.tv_title_id.setTextColor(
                                    ContextCompat.getColor(mContext!!, R.color.white)
                                )
                            }
                        }
                    }
                }
            }
            holder.tv_title_id.text = modifier_group_listArray[position].name
            holder.cv_modifier_group_list_id.setOnClickListener {
                cv_discount_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.white_bg_corner_10
                )
                cv_dining_opt_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.white_bg_corner_10
                )
                rv_discounts_item_id.visibility = View.GONE
                ll_modifier_list_id.visibility = View.GONE
                if (/*modifier_group_listArray[position].isRequired == 0 && */!modifier_group_listArray[position].multipleSelect!!) {
                    max_Selection = 1
                    min_Selection = 1
                } else {
                    max_Selection = if (modifier_group_listArray[position].maxSelections != null) {
                        modifier_group_listArray[position].maxSelections!!
                    } else {
                        0
                    }
                    min_Selection = if (modifier_group_listArray[position].minSelections != null) {
                        modifier_group_listArray[position].minSelections!!
                    } else {
                        0
                    }
                }
                holder.cv_modifier_group_list_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.graditent_bg
                )
                holder.tv_title_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                val obj = JSONObject()
                obj.put("mod_grp", modifier_group_listArray[position].id)
                obj.put("createdBy", mEmployeeId)
                obj.put("restaurantId", restaurantId)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.e("modifiers_list_data", jObject.toString() + "===" + max_Selection)
                GetModifierListAPI(
                    jObject,
                    max_Selection,
                    min_Selection,
                    modifier_group_listArray[position].id.toString(),
                    modifier_group_listArray[position].name.toString()
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_modifier_group_list_id =
                view.findViewById<CardView>(R.id.cv_modifier_group_list_id)
        }

    }

    inner class OrderAdapter(
        context: Context,
        val ourCustomesArray: ArrayList<OrdersCustomData>
    ) :
        RecyclerView.Adapter<OrderAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.order_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourCustomesArray.size
        }

        @SuppressLint("RecyclerView")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val type: String
            holder.tv_add_item_id.text = ourCustomesArray[position].name
            holder.tv_qty_id.text = ourCustomesArray[position].qty
            if (ourCustomesArray[position].itemStatus == 2) {
                holder.iv_fulfilled.visibility = View.VISIBLE
                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.17f
                )
                holder.tv_add_item_id.layoutParams = layoutParams
            } else {
                holder.iv_fulfilled.visibility = View.GONE
            }
            if (from_screen == "payment_terminal") {
                if (ourCustomesArray[position].discountApplied /*&& !ourCustomesArray[position].isBogo*/ && !ourCustomesArray[position].isCombo) {
                    var discountAmount = 0.00
                    for (i in 0 until ourCustomesArray[position].discountsArray.size) {
                        if (ourCustomesArray[position].discountsArray[i].discountValueType == "currency"
                        ) {
                            discountAmount =
                                ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].discountsArray[i].value
                        } else if (ourCustomesArray[position].discountsArray[i].discountValueType == "percentage"
                        ) {
                            discountAmount =
                                (ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble()) * (ourCustomesArray[position].discountsArray[i].value / 100)
                        }
                        holder.tv_discount_name_id.text =
                            ourCustomesArray[position].discountsArray[i].name
                    }
                    holder.ll_discount.visibility = View.VISIBLE
                    holder.tv_after_discount_amount_id.text =
                        "%.2f".format(ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble() - discountAmount)
                    holder.tv_discount_amount_id.text = "%.2f".format(discountAmount)
                    when (ourCustomesArray[position].discountAppliedType) {
                        DISCOUNT_LEVEL_ITEM -> {
                            holder.ll_discount.visibility = View.VISIBLE
                        }
                        DISCOUNT_LEVEL_CHECK -> {
                            holder.ll_discount.visibility = View.VISIBLE
                        }
                        else -> {
                            holder.ll_discount.visibility = View.GONE
                        }
                    }
                } else {
                    holder.ll_discount.visibility = View.GONE
                }
            } else {
                if (ourCustomesArray[position].discountApplied /*&& !ourCustomesArray[position].isBogo*/ && !ourCustomesArray[position].isCombo) {
                    var discountAmount = 0.00
                    for (i in 0 until ourCustomesArray[position].discountsArray.size) {
                        if (ourCustomesArray[position].discountsArray[i].discountValueType == "currency"
                        ) {
                            discountAmount =
                                ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].discountsArray[i].value
                        } else if (ourCustomesArray[position].discountsArray[i].discountValueType == "percentage"
                        ) {
                            if (ourCustomesArray[position].isBogo) {
                                discountAmount =
                                    (ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble()) * (ourCustomesArray[position].discountsArray[i].value / 100)
                            } else {
                                if (ourCustomesArray[position].discountsArray[i].maxDiscountAmount != 0.00) {
                                    if ((ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble()) >= ourCustomesArray[position].discountsArray[i].maxDiscountAmount) {
                                        discountAmount =
                                            (ourCustomesArray[position].discountsArray[i].maxDiscountAmount) * (ourCustomesArray[position].discountsArray[i].value / 100)
                                    } else {
                                        discountAmount =
                                            (ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble()) * (ourCustomesArray[position].discountsArray[i].value / 100)
                                    }
                                } else {
                                    discountAmount =
                                        (ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble()) * (ourCustomesArray[position].discountsArray[i].value / 100)
                                }
                            }
                        }
                    }
                    holder.tv_discount_amount_id.text = "%.2f".format(discountAmount)
                    if (ourCustomesArray[position].isBogo) {
                        holder.tv_discount_name_id.setPadding(25, 0, 0, 0)
                    }
                    holder.tv_after_discount_amount_id.text =
                        "%.2f".format(ourCustomesArray[position].qty.toDouble() * ourCustomesArray[position].basePrice.toDouble() - discountAmount)
                    holder.tv_discount_name_id.text =
                        ourCustomesArray[position].discountsArray[0].name
                    when (ourCustomesArray[position].discountAppliedType) {
                        DISCOUNT_LEVEL_ITEM -> {
                            holder.ll_discount.visibility = View.VISIBLE
                        }
                        DISCOUNT_LEVEL_CHECK -> {
                            holder.ll_discount.visibility = View.VISIBLE
                        }
                        else -> {
                            holder.ll_discount.visibility = View.GONE
                        }
                    }
                } else {
                    holder.ll_discount.visibility = View.GONE
                }
            }
            holder.tv_price_id.text =
                "%.2f".format(ourCustomesArray[position].basePrice.toDouble())
            holder.tv_price_id.visibility = View.VISIBLE
            if (ourCustomesArray[position].isCombo /*|| ourCustomesArray[position].isBogo*/) {
                if (ourCustomesArray[position].discountApplied) {
                    holder.tv_total_proce_id.text = "0.00"
                } else {
                    holder.tv_total_proce_id.text =
                        "%.2f".format((ourCustomesArray[position].qty.toInt() * ourCustomesArray[position].basePrice.toDouble()))
                }
            } else {
                holder.tv_total_proce_id.text =
                    "%.2f".format((ourCustomesArray[position].qty.toInt() * ourCustomesArray[position].basePrice.toDouble()))
            }
            Log.e("status_ll", ourCustomesArray[position].itemStatus.toString())
            Log.e("allModifirers", ourCustomesArray[position].allmodifierArray.size.toString())
            if (ourCustomesArray[position].allmodifierArray.size != 0) {
                holder.rv_order_modifiers_id.visibility = View.VISIBLE
                Log.e(
                    "allModifirers",
                    ourCustomesArray[position].allmodifierArray.size.toString()
                )
                val order_LayoutManager =
                    LinearLayoutManager(this@OrderDetailsActivity, RecyclerView.VERTICAL, false)
                holder.rv_order_modifiers_id.layoutManager = order_LayoutManager
                val orderModifierAdapter = OrderModifierAdapter(
                    this.mContext!!,
                    ourCustomesArray[position].allmodifierArray,
                    isModifierSelected
                )
                holder.rv_order_modifiers_id.adapter = orderModifierAdapter
                orderModifierAdapter.notifyDataSetChanged()
            } else {
                holder.rv_order_modifiers_id.visibility = View.GONE
            }
            when {
                ourCustomesArray[position].isBogo -> {
                    type = "BoGo"
                }
                ourCustomesArray[position].isCombo -> {
                    type = "Combo"
                }
                else -> {
                    type = ""
                    holder.tv_item_type.visibility = View.GONE
                }
            }
            holder.tv_item_type.text = type
            if (ourCustomesArray[position].isCombo) {
                holder.tv_item_type.visibility = View.VISIBLE
                if (ourCustomesArray[position].itemStatus == 2) {
                    holder.tv_add_item_id.setPadding(0, 0, 0, 0)
                } else {
                    holder.tv_add_item_id.setPadding(25, 0, 0, 0)
                }
            }
            if (ourCustomesArray[position].isBogo) {
                holder.tv_item_type.visibility = View.VISIBLE
                if (ourCustomesArray[position].itemStatus == 2) {
                    holder.tv_add_item_id.setPadding(0, 0, 0, 0)
                } else {
                    holder.tv_add_item_id.setPadding(25, 0, 0, 0)
                }
            }
            if (position - 1 != -1) {
                if ((ourCustomesArray[position].isCombo && ourCustomesArray[position - 1].isCombo) && (ourCustomesArray[position].comboOrBogoUniqueId == ourCustomesArray[position - 1].comboOrBogoUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.tv_add_item_id.setPadding(25, 0, 0, 0)
                }
                if ((ourCustomesArray[position].isBogo && ourCustomesArray[position - 1].isBogo) && (ourCustomesArray[position].comboOrBogoUniqueId == ourCustomesArray[position - 1].comboOrBogoUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.tv_add_item_id.setPadding(25, 0, 0, 0)
                }
                if ((!ourCustomesArray[position].isBogo && !ourCustomesArray[position - 1].isBogo) && (!ourCustomesArray[position].isCombo && !ourCustomesArray[position - 1].isCombo)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.tv_add_item_id.setPadding(0, 0, 0, 0)
                }
            }
            if (ourCustomesArray[position].voidStatus && ourCustomesArray[position].qty == "0") {
                holder.tv_add_item_id.paintFlags =
                    holder.tv_add_item_id.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
            holder.ll_order_items_id.setOnClickListener {
                item_index = -1
                mItemId = ourCustomesArray[position].itemId
                discountApplicable = ourCustomesArray[position].discountApplicable
                item_qty = ourCustomesArray[position].qty.toInt()
                tv_set_qty_id.text = holder.tv_qty_id.text.toString()
                tv_modifier_item_id.text = ourCustomesArray[position].name
                if (discountApplicable) {
                    cv_discount_id.visibility = View.VISIBLE
                } else {
                    cv_discount_id.visibility = View.GONE
                }
                if (ourCustomesArray[position].isCombo || ourCustomesArray[position].isBogo) {
                    rv_discounts_item_id.visibility = View.GONE
                    ll_modifier_list_id.visibility = View.GONE
                    rv_discounts_item_id.visibility = View.GONE
                    ll_menu_item_edit_id.visibility = View.GONE
                    ll_edit_item_qty_id.visibility = View.GONE
                    ll_seat_no_id.visibility = View.GONE
                    ll_modifier_list_id.visibility = View.GONE
                    ll_all_modifiers_id.visibility = View.GONE
                    ll_main_course_id.visibility = View.GONE
                    ll_gift_content_id.visibility = View.GONE
                    rv_modifier_list_id.visibility = View.GONE
                    tv_modifiers.visibility = View.GONE
                    rv_pre_modifier_list_id.visibility = View.GONE
                    tv_pre_modifiers.visibility = View.GONE
                    rv_modifier_group_id.visibility = View.GONE
                    ll_bogo_id.visibility = View.GONE
                    ll_combo_id.visibility = View.GONE
                    showEditComboDialog(ourCustomesArray[position])
                } else {
                    rv_discounts_item_id.visibility = View.GONE
                    ll_modifier_list_id.visibility = View.GONE
                    rv_discounts_item_id.visibility = View.GONE
                    ll_menu_item_edit_id.visibility = View.VISIBLE
                    ll_edit_item_qty_id.visibility = View.VISIBLE
                    ll_seat_no_id.visibility = View.VISIBLE
                    ll_modifier_list_id.visibility = View.GONE
                    ll_all_modifiers_id.visibility = View.VISIBLE
                    ll_main_course_id.visibility = View.GONE
                    ll_gift_content_id.visibility = View.GONE
                    rv_modifier_list_id.visibility = View.GONE
                    tv_modifiers.visibility = View.GONE
                    rv_pre_modifier_list_id.visibility = View.GONE
                    tv_pre_modifiers.visibility = View.GONE
                    rv_modifier_group_id.visibility = View.VISIBLE
                    ll_bogo_id.visibility = View.GONE
                    ll_combo_id.visibility = View.GONE
                    val obj_modif = JSONObject()
                    obj_modif.put("menu_item", ourCustomesArray[position].itemId)
                    obj_modif.put("createdBy", mEmployeeId)
                    obj_modif.put("restaurantId", restaurantId)
                    val jObject_modif = JsonParser.parseString(obj_modif.toString()).asJsonObject
                    Log.e("modifier_group_data", jObject_modif.toString() + item_index)
                    getModifierGroupAPI(jObject_modif)
                }
                item_index = position
                selectPosition = position
                isModifierSelected = true
                modifier_item_index = position
                val orderModifierAdapter = OrderModifierAdapter(
                    this.mContext!!,
                    ourCustomesArray[position].allmodifierArray,
                    isModifierSelected
                )
                holder.rv_order_modifiers_id.adapter = orderModifierAdapter
                orderModifierAdapter.notifyDataSetChanged()
                notifyDataSetChanged()
                if (ourCustomesArray[item_index!!].itemId == ourCustomesArray[item_index!!].id) {
                    tv_remove_item_id.text = getString(R.string.remove)
                } else {
                    if (isStayOrderSelected) {
                        tv_remove_item_id.text = getString(R.string.remove)
                    } else {
                        tv_remove_item_id.text = getString(R.string.void_stg)
                    }
                }
                cv_discount_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.white_bg_corner_10
                )
                cv_dining_opt_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.white_bg_corner_10
                )
                holder.ll_order_items_id.setBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.bg_order_modifier
                    )
                )
                holder.tv_item_type.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_add_item_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_price_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_qty_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_total_proce_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                Log.d("main_item", "" + position)
            }
            if (selectPosition == position) {
                holder.ll_order_items_id.setBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.bg_order_modifier
                    )
                )
                holder.tv_item_type.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_add_item_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_price_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_qty_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_total_proce_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_discount_name_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_discount_amount_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_after_discount_amount_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                isModifierSelected = true
                val orderModifierAdapter = OrderModifierAdapter(
                    this.mContext!!,
                    ourCustomesArray[position].allmodifierArray,
                    isModifierSelected
                )
                holder.rv_order_modifiers_id.adapter = orderModifierAdapter
                orderModifierAdapter.notifyDataSetChanged()
            } else {
                isModifierSelected = false
                val orderModifierAdapter = OrderModifierAdapter(
                    this.mContext!!,
                    ourCustomesArray[position].allmodifierArray,
                    isModifierSelected
                )
                holder.rv_order_modifiers_id.adapter = orderModifierAdapter
                orderModifierAdapter.notifyDataSetChanged()
                holder.ll_order_items_id.setBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_type.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.payment_time_stamp
                    )
                )
                holder.tv_add_item_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black
                    )
                )
                holder.tv_price_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black
                    )
                )
                holder.tv_qty_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black
                    )
                )
                holder.tv_total_proce_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black
                    )
                )
                holder.tv_discount_name_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.login_text_default
                    )
                )
                holder.tv_discount_amount_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.login_text_default
                    )
                )
                holder.tv_after_discount_amount_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.login_text_default
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_add_item_id = view.findViewById<TextView>(R.id.tv_add_item_id)
            val tv_qty_id = view.findViewById<TextView>(R.id.tv_qty_id)
            val tv_price_id = view.findViewById<TextView>(R.id.tv_price_id)
            val tv_total_proce_id = view.findViewById<TextView>(R.id.tv_total_proce_id)
            val ll_order_items_id = view.findViewById<LinearLayout>(R.id.ll_order_items_id)
            val rv_order_modifiers_id =
                view.findViewById<RecyclerView>(R.id.rv_order_modifiers_id)
            val ll_discount = view.findViewById<LinearLayout>(R.id.ll_discount)
            val tv_discount_name_id = view.findViewById<TextView>(R.id.tv_discount_name_id)
            val tv_discount_amount_id = view.findViewById<TextView>(R.id.tv_discount_amount_id)
            val tv_after_discount_amount_id =
                view.findViewById<TextView>(R.id.tv_after_discount_amount_id)
            val tv_item_type = view.findViewById<TextView>(R.id.tv_item_type)
            val iv_fulfilled = view.findViewById<ImageView>(R.id.iv_fulfilled)
        }
    }

    inner class OrderModifierAdapter(
        context: Context,
        val modifier_listArray: ArrayList<GetAllModifierListResponseList>,
        modifierSelected: Boolean
    ) :
        RecyclerView.Adapter<OrderModifierAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var isModifierSelected = false

        init {
            this.mContext = context
            this.isModifierSelected = modifierSelected
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.order_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return modifier_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            if (isModifierSelected) {
                holder.tv_add_sub_item_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_sub_item_price.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_sub_item_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.iv_delete.setImageResource(R.drawable.ic_remove_white)
            } else {
                holder.tv_add_sub_item_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black_80
                    )
                )
                holder.tv_sub_item_price.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black_80
                    )
                )
                holder.tv_sub_item_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black_80
                    )
                )
                holder.iv_delete.setImageResource(R.drawable.ic_remove_red)
            }
            holder.tv_add_sub_item_id.visibility = View.VISIBLE
            holder.tv_sub_item_price.visibility = View.VISIBLE
            Log.e("allModifirers", modifier_listArray.size.toString())
            holder.tv_add_sub_item_id.text = modifier_listArray[position].name
            holder.tv_sub_item_price.text =
                "%.2f".format(modifier_listArray[position].price.toString().toDouble())
            holder.tv_sub_item_total.text =
                "%.2f".format(modifier_listArray[position].total.toString().toDouble())
            if (modifier_listArray[position].id == "") {
                holder.iv_delete.visibility = View.VISIBLE
            } else {
                holder.iv_delete.visibility = View.INVISIBLE
            }
            holder.iv_delete.setOnClickListener {
                Log.d("item_index", item_index.toString())
                if (item_index == null) {
                    // do nothing
                } else {
                    if (item_index!! != -1) {
                        if (custom_array[item_index!!].allmodifierArray.size > 0) {
                            for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                                if (custom_array[item_index!!].allmodifierArray[i].name == modifier_listArray[position].name) {
                                    custom_array[item_index!!].allmodifierArray.removeAt(i)
                                    break
                                }
                            }
                        }
                        val orderCustomData = OrdersCustomData(
                            custom_array[item_index!!].basePrice,
                            custom_array[item_index!!].discountApplicable,
                            custom_array[item_index!!].id,
                            custom_array[item_index!!].name,
                            custom_array[item_index!!].itemType,
                            custom_array[item_index!!].qty,
                            custom_array[item_index!!].total,
                            custom_array[item_index!!].discount,
                            custom_array[item_index!!].restaurantId,
                            custom_array[item_index!!].allmodifierArray,
                            custom_array[item_index!!].itemId,
                            custom_array[item_index!!].menuGroupId,
                            custom_array[item_index!!].menuId,
                            custom_array[item_index!!].discountApplied,
                            custom_array[item_index!!].discountAppliedType,
                            custom_array[item_index!!].discountsArray,
                            custom_array[item_index!!].itemStatus,
                            custom_array[item_index!!].isCombo,
                            custom_array[item_index!!].isBogo,
                            custom_array[item_index!!].comboOrBogoOfferId,
                            custom_array[item_index!!].comboOrBogoUniqueId,
                            custom_array[item_index!!].voidStatus,
                            custom_array[item_index!!].voidQuantity,
                            custom_array[item_index!!].aplicableTaxRates,
                            custom_array[item_index!!].taxAppliedList,
                            custom_array[item_index!!].diningOptionTaxException,
                            custom_array[item_index!!].diningTaxOption,
                            custom_array[item_index!!].taxIncludeOption
                        )
                        custom_array[item_index!!] = orderCustomData
                        fullOrderSetUp()
                    }
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_add_sub_item_id = view.findViewById<TextView>(R.id.tv_add_sub_item_id)
            val tv_sub_item_price = view.findViewById<TextView>(R.id.tv_sub_item_price)
            val tv_sub_item_total = view.findViewById(R.id.tv_sub_item_total) as TextView
            val iv_delete = view.findViewById<ImageView>(R.id.iv_delete)
        }
    }

    private fun specialRequestDialog() {
        special_req_dialog = Dialog(this)
        special_req_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        special_req_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        special_req_dialog.setCancelable(false)
        special_req_dialog.setContentView(R.layout.special_req_dailog_one)
        val et_request_name_id =
            special_req_dialog.findViewById(R.id.et_request_name_id) as EditText
        val et_request_price_id =
            special_req_dialog.findViewById(R.id.et_request_price_id) as EditText
        val tv_sr_next_button =
            special_req_dialog.findViewById(R.id.tv_sr_next_button) as TextView
        val tv_sr_cancel_button =
            special_req_dialog.findViewById(R.id.tv_sr_cancel_button) as TextView
        val tv_currency_price =
            special_req_dialog.findViewById(R.id.tv_currency_price) as TextView
        tv_currency_price.text = currencyType
        tv_sr_cancel_button.setOnClickListener {
            special_req_dialog.dismiss()
        }
        tv_sr_next_button.setOnClickListener {
            if (et_request_name_id.text.toString().trim() == "") {
                Toast.makeText(this, "Enter Special Request", Toast.LENGTH_SHORT).show()
                et_request_name_id.requestFocus()
            } else if (et_request_price_id.text.toString().trim() == "") {
                Toast.makeText(this, "Enter Valid Amount", Toast.LENGTH_SHORT).show()
                et_request_price_id.requestFocus()
            } else {
                specialRequests = GetAllModifierListResponseList(
                    id = "",
                    name = et_request_name_id.text.toString(),
                    price = et_request_price_id.text.toString().toDouble(),
                    total = (custom_array[item_index!!].qty.toDouble() * et_request_price_id.text.toString()
                        .toDouble()),
                    modifierGroupId = ArrayList()
                )
                val specialRequestsArray = ArrayList<GetAllModifierListResponseList>()
                if (custom_array[item_index!!].allmodifierArray.size > 0) {
                    specialRequestsArray.addAll(custom_array[item_index!!].allmodifierArray)
                    specialRequestsArray.add(specialRequests)
                } else {
                    specialRequestsArray.add(specialRequests)
                }
                val orderCustomData = OrdersCustomData(
                    custom_array[item_index!!].basePrice,
                    custom_array[item_index!!].discountApplicable,
                    custom_array[item_index!!].id,
                    custom_array[item_index!!].name,
                    custom_array[item_index!!].itemType,
                    custom_array[item_index!!].qty,
                    custom_array[item_index!!].total,
                    custom_array[item_index!!].discount,
                    custom_array[item_index!!].restaurantId,
                    specialRequestsArray,
                    custom_array[item_index!!].itemId,
                    custom_array[item_index!!].menuGroupId,
                    custom_array[item_index!!].menuId,
                    custom_array[item_index!!].discountApplied,
                    custom_array[item_index!!].discountAppliedType,
                    custom_array[item_index!!].discountsArray,
                    custom_array[item_index!!].itemStatus,
                    custom_array[item_index!!].isCombo,
                    custom_array[item_index!!].isBogo,
                    custom_array[item_index!!].comboOrBogoOfferId,
                    custom_array[item_index!!].comboOrBogoUniqueId,
                    custom_array[item_index!!].voidStatus,
                    custom_array[item_index!!].voidQuantity,
                    custom_array[item_index!!].aplicableTaxRates,
                    custom_array[item_index!!].taxAppliedList,
                    custom_array[item_index!!].diningOptionTaxException,
                    custom_array[item_index!!].diningTaxOption,
                    custom_array[item_index!!].taxIncludeOption
                )
                custom_array[item_index!!] = orderCustomData
                Log.d("custom_array", custom_array.size.toString())
                fullOrderSetUp()
                special_req_dialog.dismiss()
            }
        }
        special_req_dialog.show()
    }

    private fun sellValueDailog() {
        giftCardAmount = 0.00
        gift_sell_card_dialog = Dialog(this)
        gift_sell_card_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        gift_sell_card_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        gift_sell_card_dialog.setCancelable(false)
        gift_sell_card_dialog.setContentView(R.layout.gift_card_dailog_one)
        val et_add_gift_amount_id =
            gift_sell_card_dialog.findViewById(R.id.et_add_gift_amount_id) as EditText
        tv_gift_next_button = gift_sell_card_dialog.findViewById(R.id.tv_gift_next_button)
        tv_gift_cancel_button = gift_sell_card_dialog.findViewById(R.id.tv_gift_cancel_button)
        val rg_cards = gift_sell_card_dialog.findViewById(R.id.rg_cards) as RadioGroup
        val tv_currency_amount =
            gift_sell_card_dialog.findViewById(R.id.tv_currency_amount) as TextView
        tv_currency_amount.text = currencyType
        tv_gift_cancel_button.setOnClickListener {
            gift_sell_card_dialog.dismiss()
        }
        rg_cards.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_classic -> {
                    giftCardType = "classic"
                }
                R.id.rb_electronic -> {
                    giftCardType = "e-card"
                }
            }
        }
        tv_gift_next_button.setOnClickListener {
            when {
                et_add_gift_amount_id.text.toString().trim() == "" -> {
                    Toast.makeText(this, "Enter Valid Gift Amount!", Toast.LENGTH_SHORT).show()
                    et_add_gift_amount_id.requestFocus()
                }
                et_add_gift_amount_id.text.toString().toDouble() < 5.00 -> {
                    Toast.makeText(
                        this,
                        "Gift card amount should be more than ${currencyType}5 !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_add_gift_amount_id.text.toString().toDouble() > 500.00 -> {
                    Toast.makeText(
                        this,
                        "Gift card amount should be less than ${currencyType}500 !",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                giftCardType == "" -> {
                    Toast.makeText(this, "Please select gift card type!", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    giftCardAmount = et_add_gift_amount_id.text.toString().toDouble()
                    addCustomer("sell")
                }
            }
        }
        gift_sell_card_dialog.show()
    }

    private fun giftCardDialog(type: String) {
        var cardNumber = ""
        dialog_gift_card = Dialog(this)
        dialog_gift_card.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.gift_card_payment_dailog, null)
        dialog_gift_card.setContentView(view)
        dialog_gift_card.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_gift_card.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_gift_card.setCanceledOnTouchOutside(false)
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val cv_one = view.findViewById(R.id.cv_one) as CardView
        val cv_two = view.findViewById(R.id.cv_two) as CardView
        val cv_three = view.findViewById(R.id.cv_three) as CardView
        val cv_four = view.findViewById(R.id.cv_four) as CardView
        val cv_five = view.findViewById(R.id.cv_five) as CardView
        val cv_six = view.findViewById(R.id.cv_six) as CardView
        val cv_seven = view.findViewById(R.id.cv_seven) as CardView
        val cv_eight = view.findViewById(R.id.cv_eight) as CardView
        val cv_nine = view.findViewById(R.id.cv_nine) as CardView
        val cv_zero = view.findViewById(R.id.cv_zero) as CardView
        val cv_look_up = view.findViewById(R.id.cv_look_up) as CardView
        val cv_scan = view.findViewById(R.id.cv_scan) as CardView
        val cv_delete = view.findViewById(R.id.cv_delete) as CardView
        val ll_done = view.findViewById(R.id.ll_gift_card_done) as LinearLayout
        val ll_card_verify = view.findViewById(R.id.ll_card_verify) as LinearLayout
        val tv_cash_drop_amount = view.findViewById(R.id.tv_cash_drop_amount) as TextView
        val tv_currency_amount = view.findViewById(R.id.tv_currency_amount) as TextView
        tv_currency_amount.text = currencyType
        ll_card_verify.visibility = View.GONE
        iv_close.setOnClickListener {
            dialog_gift_card.dismiss()
        }
        cv_delete.setOnClickListener {
            if (cardNumber != "") {
                cardNumber = cardNumber.dropLast(1)
                if (cardNumber == "") {
                    cardNumber = str_zero
                }
            } else {
                cardNumber = str_zero
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_one.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_one
            } else {
                cardNumber += str_one
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_two.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_two
            } else {
                cardNumber += str_two
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_three.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_three
            } else {
                cardNumber += str_three
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_four.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_four
            } else {
                cardNumber += str_four
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_five.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_five
            } else {
                cardNumber += str_five
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_six.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_six
            } else {
                cardNumber += str_six
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_seven.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_seven
            } else {
                cardNumber += str_seven
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_eight.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_eight
            } else {
                cardNumber += str_eight
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_nine.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_nine
            } else {
                cardNumber += str_nine
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_zero.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_zero
            } else {
                cardNumber += str_zero
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_look_up.setOnClickListener {
            Toast.makeText(this, "Looking for $cardNumber", Toast.LENGTH_SHORT).show()
        }
        cv_scan.setOnClickListener {
            Toast.makeText(this, "Scan gift card", Toast.LENGTH_SHORT).show()
        }
        ll_done.setOnClickListener {
            if (type == "add_value") {
                SellvalueAfterNextDailog(cardNumber)
            } else if (type == "check_balance") {
                if (cardNumber == "") {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Please enter card number!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val obj_gift_bance = JSONObject()
                    obj_gift_bance.put("giftCardNo", cardNumber)
                    obj_gift_bance.put("restaurantId", restaurantId)
                    val jObjectj_gift_bance =JsonParser.parseString(obj_gift_bance.toString()).asJsonObject
                    GetGiftCardBalance(jObjectj_gift_bance)
                }
            }
        }
        dialog_gift_card.show()
    }

    private fun AddGiftCard(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.sellGiftCardApi(json_obj)
        call.enqueue(object : Callback<SellGiftCardResponse> {
            override fun onFailure(call: Call<SellGiftCardResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<SellGiftCardResponse>,
                response: Response<SellGiftCardResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        gift_sell_card_dialog.dismiss()
                        add_customer_dialog.dismiss()
                        gift_card_payment_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun AddValueGiftCard(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.addValueGiftCardApi(json_obj)
        call.enqueue(object : Callback<SellGiftCardResponse> {
            override fun onFailure(call: Call<SellGiftCardResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<SellGiftCardResponse>,
                response: Response<SellGiftCardResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        dialog_gift_card.dismiss()
                        gift_sell_next_dialog.dismiss()
                        gift_card_with_bank_payment_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun SellvalueAfterNextDailog(cardNumber: String) {
        gift_sell_next_dialog = Dialog(this)
        gift_sell_next_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        gift_sell_next_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        gift_sell_next_dialog.setCancelable(false)
        gift_sell_next_dialog.setContentView(R.layout.add_value_dialog)
        val tv_value_next_id =
            gift_sell_next_dialog.findViewById(R.id.tv_value_next_id) as TextView
        val tv_value_cancel_id =
            gift_sell_next_dialog.findViewById(R.id.tv_value_cancel_id) as TextView
        val tv_gift_card_no_id =
            gift_sell_next_dialog.findViewById(R.id.tv_gift_card_no_id) as TextView
        val tv_currency_amount =
            gift_sell_next_dialog.findViewById(R.id.tv_currency_amount) as TextView
        val et_add_value_amount_id =
            gift_sell_next_dialog.findViewById(R.id.et_add_value_amount_id) as EditText
        tv_gift_card_no_id.text = cardNumber
        tv_currency_amount.text = currencyType
        tv_value_cancel_id.setOnClickListener {
            gift_sell_next_dialog.dismiss()
        }
        gift_sell_next_dialog.show()
        tv_value_next_id.setOnClickListener {
            if (et_add_value_amount_id.text.toString().trim() == "") {
                Toast.makeText(this, "Enter Valid Gift Amount", Toast.LENGTH_SHORT).show()
                et_add_value_amount_id.requestFocus()
            } else {
                giftCardWithBankPaymentDialog(
                    cardNumber,
                    et_add_value_amount_id.text.toString().trim().toDouble()
                )
            }
        }
    }

    private fun GetGiftCardBalance(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.giftCardBalanceEnqApi(json_obj)
        call.enqueue(object : Callback<GiftCardBalanceResponse> {
            override fun onFailure(call: Call<GiftCardBalanceResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GiftCardBalanceResponse>,
                response: Response<GiftCardBalanceResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        GiftCardBalanceDialog(
                            response.body()!!.giftCard!!.amount!!.toString(),
                            response.body()!!.giftCard!!.cardType!!.toString()
                        )
                        dialog_gift_card.dismiss()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun GiftCardBalanceDialog(amount: String, cardType: String) {
        val gift_bance_dialog = Dialog(this)
        gift_bance_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        gift_bance_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        gift_bance_dialog.setCancelable(false)
        gift_bance_dialog.setContentView(R.layout.gift_balance_dialog)
        val tv_gift_amount_id =
            gift_bance_dialog.findViewById(R.id.tv_gift_amount_id) as TextView
        val tv_gift_card_type_id =
            gift_bance_dialog.findViewById(R.id.tv_gift_card_type_id) as TextView
        val tv_currency_amount =
            gift_bance_dialog.findViewById(R.id.tv_currency_amount) as TextView
        tv_currency_amount.text = currencyType
        tv_gift_amount_id.text = "%.2f".format(amount.toDouble())
        tv_gift_card_type_id.text = cardType
        val tv_ok_id = gift_bance_dialog.findViewById(R.id.tv_ok_id) as TextView
        tv_ok_id.setOnClickListener {
            gift_bance_dialog.dismiss()
        }
        gift_bance_dialog.show()
    }

    private fun SizedBasedPriceDailog(
        size_list: ArrayList<getSizeList>,
        basePrice: String,
        id: String,
        name: String,
        qty: String,
        total: String,
        discount: String,
        restaurantId: String,
        allmodifiers: ArrayList<GetAllModifierListResponseList>,
        categoryID: String,
        rightmenuID: String,
        itemStatus: Int,
        comboOrbogo: String,
        aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>,
        diningOptionTaxException: Boolean,
        diningTaxOption: Boolean,
        taxIncludeOption: Boolean
    ) {
        val size_list_array = ArrayList<getSizeList>()
        var price_retun = ""
        var size_type_name = ""
        val size_based_dialog = Dialog(this)
        size_based_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        size_based_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        size_based_dialog.setCancelable(false)
        size_based_dialog.setContentView(R.layout.size_based_list_dialog)
        val tv_item_name_id = size_based_dialog.findViewById(R.id.tv_item_name_id) as TextView
        val tv_value_cancel_id =
            size_based_dialog.findViewById(R.id.tv_value_cancel_id) as TextView
        val tv_value_next_id = size_based_dialog.findViewById(R.id.tv_value_next_id) as TextView
        val rv_size_based_id =
            size_based_dialog.findViewById(R.id.rv_size_based_id) as RecyclerView
        for (k in 0 until size_list.size) {
            size_list_array.add(size_list[k])
        }
        tv_item_name_id.text = name
        if (size_list_array.size > 0) {
            price_retun = size_list_array[0].price!!
            size_type_name = size_list_array[0].sizeName!!
        }
        rv_size_based_id.addOnItemTouchListener(
            RecyclerItemClickListener(this@OrderDetailsActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d("size_based", "mode position clicked - $position")
                        price_retun = size_list_array[position].price!!
                        size_type_name = size_list_array[position].sizeName!!
                    }
                })
        )
        val order_LayoutManager =
            LinearLayoutManager(this@OrderDetailsActivity, RecyclerView.HORIZONTAL, false)
        rv_size_based_id.layoutManager = order_LayoutManager
        val adapter = SizeBasedAdapter(this@OrderDetailsActivity, size_list_array)
        rv_size_based_id.adapter = adapter
        adapter.notifyDataSetChanged()
        tv_value_cancel_id.setOnClickListener {
            size_based_dialog.dismiss()
        }
        tv_value_next_id.setOnClickListener {
            val taxAppliedList: ArrayList<SendTaxRates>
            if (taxIncludeOption) {
                taxAppliedList = taxCalculationNew(
                    qty.toDouble(),
                    price_retun.toDouble(),
                    aplicableTaxRates
                )
            } else {
                taxAppliedList = ArrayList()
            }
            val itemName = "$name $size_type_name"
            //Add custom item
            val customOrderData = OrdersCustomData(
                price_retun,
                discountApplicable,
                id,
                itemName,
                "normal",
                qty,
                (qty.toDouble() * price_retun.toDouble()).toString(),
                "",
                restaurantId,
                allmodifiers,
                id,
                categoryID,
                rightmenuID,
                false,
                "",
                ArrayList(),
                itemStatus,
                isCombo = false,
                isBogo = false,
                comboOrBogoOfferId = "",
                comboOrBogoUniqueId = "",
                voidStatus = false,
                voidQuantity = 0,
                aplicableTaxRates = aplicableTaxRates,
                taxAppliedList = taxAppliedList,
                diningOptionTaxException = diningOptionTaxException,
                diningTaxOption = diningTaxOption,
                taxIncludeOption = taxIncludeOption
            )
            // adding item to order list
            custom_array.add(customOrderData)
            handleDiscounts()
            fullOrderSetUp()
            size_based_dialog.dismiss()
        }
        size_based_dialog.show()
    }

    private fun getModifierGroupAPI(json_obj: JsonObject) {
        val ModifierGroupArray = ArrayList<GetModifierGroupListResponseList>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getModifierMenuItemListApi(json_obj)
        call.enqueue(object : Callback<GetModifierGroupListResponse> {
            override fun onFailure(call: Call<GetModifierGroupListResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetModifierGroupListResponse>,
                response: Response<GetModifierGroupListResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val result = response.body()!!.result!!
                        for (i in 0 until result.size) {
                            ModifierGroupArray.add(result[i])
                        }
                        val order_LayoutManager =
                            LinearLayoutManager(
                                this@OrderDetailsActivity,
                                RecyclerView.HORIZONTAL,
                                false
                            )
                        rv_modifier_group_id.layoutManager = order_LayoutManager
                        val adapter =
                            ModifirGroupAdapter(this@OrderDetailsActivity, ModifierGroupArray)
                        rv_modifier_group_id.adapter = adapter
                        adapter.notifyDataSetChanged()
                    } else if (resp.responseStatus == 2) {
                        val order_LayoutManager =
                            LinearLayoutManager(
                                this@OrderDetailsActivity,
                                RecyclerView.HORIZONTAL,
                                false
                            )
                        rv_modifier_group_id.layoutManager = order_LayoutManager
                        val adapter =
                            ModifirGroupAdapter(this@OrderDetailsActivity, ModifierGroupArray)
                        rv_modifier_group_id.adapter = adapter
                        adapter.notifyDataSetChanged()
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.resultMessage.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun GetModifierListAPI(
        json_obj: JsonObject,
        max: Int,
        min: Int,
        mod_grp: String,
        mod_name: String
    ) {
        val modifierListArray = ArrayList<GetAllModifierListResponseList>()
        val preModifierListArray = ArrayList<GetAllPreModifierListResponseList>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllModifierListApi(json_obj)
        call.enqueue(object : Callback<GetAllModifierListResponse> {
            override fun onFailure(call: Call<GetAllModifierListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<GetAllModifierListResponse>,
                response: Response<GetAllModifierListResponse>?
            ) {
                try {
                    if (response!!.body()!!.responseStatus == 1) {
                        for (i in 0 until response.body()!!.modifierDetails!!.size) {
                            modifierListArray.add(response.body()!!.modifierDetails!![i])
                        }
                        for (i in 0 until response.body()!!.preModifiers!!.size) {
                            preModifierListArray.add(response.body()!!.preModifiers!![i])
                        }
                        rv_modifier_list_id.visibility = View.VISIBLE
                        tv_modifiers.visibility = View.VISIBLE
                        if (max == 0) {
                            tv_modifiers.text = getString(R.string.modifiers)
                        } else {
                            tv_modifiers.text =
                                "${getString(R.string.modifiers)}: Maximum modifiers can be added: $max"
                        }
                    } else {
                        rv_modifier_list_id.visibility = View.GONE
                        tv_modifiers.visibility = View.GONE
                    }
                    val mNoOfColumns_drinks =
                        Utility.calculateNoOfColumns(this@OrderDetailsActivity, 210F)
                    val modif_list_LayoutManager =
                        GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
                    val pre_modif_list_LayoutManager =
                        GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
                    rv_modifier_list_id.layoutManager = modif_list_LayoutManager
                    rv_pre_modifier_list_id.layoutManager = pre_modif_list_LayoutManager
                    val adapter =
                        ModifierListAdapter(
                            this@OrderDetailsActivity,
                            modifierListArray,
                            max,
                            min,
                            mod_grp,
                            mod_name
                        )
                    rv_modifier_list_id.adapter = adapter
                    adapter.notifyDataSetChanged()
                    if (preModifierListArray.size > 0) {
                        rv_pre_modifier_list_id.visibility = View.VISIBLE
                        tv_pre_modifiers.visibility = View.VISIBLE
                    } else {
                        rv_pre_modifier_list_id.visibility = View.GONE
                        tv_pre_modifiers.visibility = View.GONE
                    }
                    isPreModifierSelected = false
                    val pre_modifier_adapter =
                        PreModifierListAdapter(this@OrderDetailsActivity, preModifierListArray)
                    rv_pre_modifier_list_id.adapter = pre_modifier_adapter
                    pre_modifier_adapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class ModifierListAdapter(
        context: Context,
        val modifier_List_listArray: ArrayList<GetAllModifierListResponseList>,
        var maxValue: Int, var minValue: Int, mod_grp: String, mod_name: String
    ) :
        RecyclerView.Adapter<ModifierListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var addSelectPosition = 0
        private var mod_grp_id = ""
        private var mod_grp_name = ""
        val modifierGroupIdListarray = ArrayList<modifierIdList>()

        init {
            this.mContext = context
            this.mod_grp_id = mod_grp
            this.mod_grp_name = mod_name
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.modifier_list_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return modifier_List_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = modifier_List_listArray[position].name
            if (custom_array[item_index!!].allmodifierArray.size > 0) {
                for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                    if (!custom_array[item_index!!].isBogo && !custom_array[item_index!!].isCombo) {
                        if (custom_array[item_index!!].allmodifierArray[i].id == "") {
                            // do nothing
                        } else if (custom_array[item_index!!].allmodifierArray[i].id == modifier_List_listArray[position].id) {
                            holder.cv_modifier_list_id.background =
                                ContextCompat.getDrawable(mContext!!, R.drawable.graditent_bg)
                            holder.tv_title_id.setTextColor(
                                ContextCompat.getColor(mContext!!, R.color.white)
                            )
                            addSelectPosition++
                        }
                    }
                }
                Log.d("select_position", "check" + addSelectPosition)
            }
            holder.cv_modifier_list_id.setOnClickListener {
                if (addSelectPosition != maxValue) {
                    holder.cv_modifier_list_id.background =
                        ContextCompat.getDrawable(mContext!!, R.drawable.graditent_bg)
                    holder.tv_title_id.setTextColor(
                        ContextCompat.getColor(mContext!!, R.color.white)
                    )
                    val modListIds = modifierIdList()
                    modListIds.id = mod_grp_id
                    modListIds.name = mod_grp_name
                    modifierGroupIdListarray.add(modListIds)
                    val addmodifierlist: GetAllModifierListResponseList
                    if (isPreModifierSelected) {
                        val price: Double
                        var name = ""
                        if (preModifier.multiplyPrice!!) {
                            price =
                                modifier_List_listArray[position].price * preModifier.multiplicationFactor!!
                        } else {
                            price =
                                modifier_List_listArray[position].price + preModifier.fixedPrice!!
                        }
                        if (preModifier.displayMode!! == getString(R.string.pre_modifier_sufix)) {
                            name =
                                modifier_List_listArray[position].name + " " + preModifier.name
                        } else if (preModifier.displayMode!! == getString(R.string.pre_modifier_prefix)) {
                            name =
                                preModifier.name + " " + modifier_List_listArray[position].name
                        }
                        addmodifierlist = GetAllModifierListResponseList(
                            modifier_List_listArray[position].id,
                            name,
                            price,
                            (custom_array[item_index!!].qty.toDouble() * price),
                            modifierGroupIdListarray
                        )
                    } else {
                        addmodifierlist = GetAllModifierListResponseList(
                            modifier_List_listArray[position].id,
                            modifier_List_listArray[position].name,
                            modifier_List_listArray[position].price,
                            (custom_array[item_index!!].qty.toDouble() * modifier_List_listArray[position].price),
                            modifierGroupIdListarray
                        )
                    }
                    if (custom_array[item_index!!].allmodifierArray.size > 0) {
                        var isRemoved = false
                        for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                            if (custom_array[item_index!!].allmodifierArray[i].id == modifier_List_listArray[position].id) {
                                custom_array[item_index!!].allmodifierArray.removeAt(i)
                                addSelectPosition--
                                holder.cv_modifier_list_id.background =
                                    ContextCompat.getDrawable(
                                        mContext!!,
                                        R.drawable.white_bg_corner_10
                                    )
                                holder.tv_title_id.setTextColor(
                                    ContextCompat.getColor(mContext!!, R.color.black_80)
                                )
                                isRemoved = true
                                break
                            }
                        }
                        if (!isRemoved) {
                            addSelectPosition++
                            custom_array[item_index!!].allmodifierArray.add(addmodifierlist)
                        }
                    } else {
                        addSelectPosition++
                        custom_array[item_index!!].allmodifierArray.add(addmodifierlist)
                    }
                    val orderCustomData = OrdersCustomData(
                        custom_array[item_index!!].basePrice,
                        custom_array[item_index!!].discountApplicable,
                        custom_array[item_index!!].id,
                        custom_array[item_index!!].name,
                        custom_array[item_index!!].itemType,
                        custom_array[item_index!!].qty,
                        custom_array[item_index!!].total,
                        custom_array[item_index!!].discount,
                        custom_array[item_index!!].restaurantId,
                        custom_array[item_index!!].allmodifierArray,
                        custom_array[item_index!!].itemId,
                        custom_array[item_index!!].menuGroupId,
                        custom_array[item_index!!].menuId,
                        custom_array[item_index!!].discountApplied,
                        custom_array[item_index!!].discountAppliedType,
                        custom_array[item_index!!].discountsArray,
                        custom_array[item_index!!].itemStatus,
                        custom_array[item_index!!].isCombo,
                        custom_array[item_index!!].isBogo,
                        custom_array[item_index!!].comboOrBogoOfferId,
                        custom_array[item_index!!].comboOrBogoUniqueId,
                        custom_array[item_index!!].voidStatus,
                        custom_array[item_index!!].voidQuantity,
                        custom_array[item_index!!].aplicableTaxRates,
                        custom_array[item_index!!].taxAppliedList,
                        custom_array[item_index!!].diningOptionTaxException,
                        custom_array[item_index!!].diningTaxOption,
                        custom_array[item_index!!].taxIncludeOption
                    )
                    custom_array[item_index!!] = orderCustomData
                    fullOrderSetUp()
                } else if (maxValue == 0) {
                    holder.cv_modifier_list_id.background =
                        ContextCompat.getDrawable(mContext!!, R.drawable.graditent_bg)
                    holder.tv_title_id.setTextColor(
                        ContextCompat.getColor(mContext!!, R.color.white)
                    )
                    val modListIds = modifierIdList()
                    modListIds.id = mod_grp_id
                    modListIds.name = mod_grp_name
                    modifierGroupIdListarray.add(modListIds)
                    val addmodifierlist: GetAllModifierListResponseList
                    if (isPreModifierSelected) {
                        val price: Double
                        var name = ""
                        if (preModifier.multiplyPrice!!) {
                            price =
                                modifier_List_listArray[position].price * preModifier.multiplicationFactor!!
                        } else {
                            price =
                                modifier_List_listArray[position].price + preModifier.fixedPrice!!
                        }
                        if (preModifier.displayMode!! == getString(R.string.pre_modifier_sufix)) {
                            name =
                                modifier_List_listArray[position].name + " " + preModifier.name
                        } else if (preModifier.displayMode!! == getString(R.string.pre_modifier_prefix)) {
                            name =
                                preModifier.name + " " + modifier_List_listArray[position].name
                        }
                        addmodifierlist = GetAllModifierListResponseList(
                            modifier_List_listArray[position].id,
                            name,
                            price,
                            (custom_array[item_index!!].qty.toDouble() * price),
                            modifierGroupIdListarray
                        )
                    } else {
                        addmodifierlist = GetAllModifierListResponseList(
                            modifier_List_listArray[position].id,
                            modifier_List_listArray[position].name,
                            modifier_List_listArray[position].price,
                            (custom_array[item_index!!].qty.toDouble() * modifier_List_listArray[position].price),
                            modifierGroupIdListarray
                        )
                    }
                    if (custom_array[item_index!!].allmodifierArray.size > 0) {
                        var isRemoved = false
                        for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                            if (custom_array[item_index!!].allmodifierArray[i].id == modifier_List_listArray[position].id) {
                                custom_array[item_index!!].allmodifierArray.removeAt(i)
                                holder.cv_modifier_list_id.background =
                                    ContextCompat.getDrawable(
                                        mContext!!,
                                        R.drawable.white_bg_corner_10
                                    )
                                holder.tv_title_id.setTextColor(
                                    ContextCompat.getColor(mContext!!, R.color.black_80)
                                )
                                isRemoved = true
                                break
                            }
                        }
                        if (!isRemoved) {
                            custom_array[item_index!!].allmodifierArray.add(addmodifierlist)
                        }
                    } else {
                        custom_array[item_index!!].allmodifierArray.add(addmodifierlist)
                    }
                    val orderCustomData = OrdersCustomData(
                        custom_array[item_index!!].basePrice,
                        custom_array[item_index!!].discountApplicable,
                        custom_array[item_index!!].id,
                        custom_array[item_index!!].name,
                        custom_array[item_index!!].itemType,
                        custom_array[item_index!!].qty,
                        custom_array[item_index!!].total,
                        custom_array[item_index!!].discount,
                        custom_array[item_index!!].restaurantId,
                        custom_array[item_index!!].allmodifierArray,
                        custom_array[item_index!!].itemId,
                        custom_array[item_index!!].menuGroupId,
                        custom_array[item_index!!].menuId,
                        custom_array[item_index!!].discountApplied,
                        custom_array[item_index!!].discountAppliedType,
                        custom_array[item_index!!].discountsArray,
                        custom_array[item_index!!].itemStatus,
                        custom_array[item_index!!].isCombo,
                        custom_array[item_index!!].isBogo,
                        custom_array[item_index!!].comboOrBogoOfferId,
                        custom_array[item_index!!].comboOrBogoUniqueId,
                        custom_array[item_index!!].voidStatus,
                        custom_array[item_index!!].voidQuantity,
                        custom_array[item_index!!].aplicableTaxRates,
                        custom_array[item_index!!].taxAppliedList,
                        custom_array[item_index!!].diningOptionTaxException,
                        custom_array[item_index!!].diningTaxOption,
                        custom_array[item_index!!].taxIncludeOption
                    )
                    custom_array[item_index!!] = orderCustomData
                    fullOrderSetUp()
                } else {
                    Log.e("set_position", position.toString() + "---" + addSelectPosition)
                    if (custom_array[item_index!!].allmodifierArray.size > 0) {
                        for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                            if (custom_array[item_index!!].allmodifierArray[i].id == modifier_List_listArray[position].id) {
                                custom_array[item_index!!].allmodifierArray.removeAt(i)
                                addSelectPosition--
                                holder.cv_modifier_list_id.background =
                                    ContextCompat.getDrawable(
                                        mContext!!,
                                        R.drawable.white_bg_corner_10
                                    )
                                holder.tv_title_id.setTextColor(
                                    ContextCompat.getColor(mContext!!, R.color.black_80)
                                )
                                break
                            }
                        }
                    }
                    val orderCustomData = OrdersCustomData(
                        custom_array[item_index!!].basePrice,
                        custom_array[item_index!!].discountApplicable,
                        custom_array[item_index!!].id,
                        custom_array[item_index!!].name,
                        custom_array[item_index!!].itemType,
                        custom_array[item_index!!].qty,
                        custom_array[item_index!!].total,
                        custom_array[item_index!!].discount,
                        custom_array[item_index!!].restaurantId,
                        custom_array[item_index!!].allmodifierArray,
                        custom_array[item_index!!].itemId,
                        custom_array[item_index!!].menuGroupId,
                        custom_array[item_index!!].menuId,
                        custom_array[item_index!!].discountApplied,
                        custom_array[item_index!!].discountAppliedType,
                        custom_array[item_index!!].discountsArray,
                        custom_array[item_index!!].itemStatus,
                        custom_array[item_index!!].isCombo,
                        custom_array[item_index!!].isBogo,
                        custom_array[item_index!!].comboOrBogoOfferId,
                        custom_array[item_index!!].comboOrBogoUniqueId,
                        custom_array[item_index!!].voidStatus,
                        custom_array[item_index!!].voidQuantity,
                        custom_array[item_index!!].aplicableTaxRates,
                        custom_array[item_index!!].taxAppliedList,
                        custom_array[item_index!!].diningOptionTaxException,
                        custom_array[item_index!!].diningTaxOption,
                        custom_array[item_index!!].taxIncludeOption
                    )
                    custom_array[item_index!!] = orderCustomData
                    fullOrderSetUp()
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_modifier_list_id = view.findViewById<CardView>(R.id.cv_modifier_list_id)
        }

    }

    inner class PreModifierListAdapter(
        context: Context,
        val pre_modifiers: ArrayList<GetAllPreModifierListResponseList>
    ) :
        RecyclerView.Adapter<PreModifierListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var preModifierSelectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): PreModifierListAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.modifier_list_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return pre_modifiers.size
        }

        override fun onBindViewHolder(
            holder: PreModifierListAdapter.ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = pre_modifiers[position].name.toString()
            val hexColorCode = if (pre_modifiers[position].color.toString().contains("#")) {
                pre_modifiers[position].color.toString()
            } else if (pre_modifiers[position].color.toString() == "") {
                // if empty set it to white
                "#FFFFFF"
            } else {
                "#FFFFFF"
            }
            val colorCode = Color.parseColor(hexColorCode)
            holder.cv_modifier_list_id.setOnClickListener {
                if (preModifierSelectedPosition == -1 || preModifierSelectedPosition != position) {
                    preModifierSelectedPosition = position
                    isPreModifierSelected = true
                } else if (preModifierSelectedPosition == position) {
                    preModifierSelectedPosition = -1
                    isPreModifierSelected = false
                }
                preModifier = pre_modifiers[position]
                notifyDataSetChanged()
            }
            if (preModifierSelectedPosition == position) {
                holder.cv_modifier_list_id.setCardBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.cyan)
                )
                holder.tv_title_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_modifier_list_id.setCardBackgroundColor(
                    colorCode
                )
                holder.tv_title_id.setTextColor(
                    ContextCompat.getColor(mContext!!, R.color.black_80)
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_modifier_list_id = view.findViewById<CardView>(R.id.cv_modifier_list_id)
        }
    }

    private fun OpenValueSetDailog(
        basePrice: String,
        id: String,
        name: String,
        qty: String,
        total: String,
        discount: String,
        restaurantId: String,
        allmodifiers: ArrayList<GetAllModifierListResponseList>,
        categoryID: String,
        rightmenuID: String,
        itemStatus: Int,
        comboOrbogo: String,
        aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>,
        diningOptionTaxException: Boolean,
        diningTaxOption: Boolean,
        taxIncludeOption: Boolean
    ) {
        open_price_dialog = Dialog(this)
        open_price_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        open_price_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        open_price_dialog.setCancelable(false)
        open_price_dialog.setContentView(R.layout.open_price_dialog)
        val tv_food_item_id = open_price_dialog.findViewById(R.id.tv_food_item_id) as TextView
        val et_open_price_id = open_price_dialog.findViewById(R.id.et_open_price_id) as EditText
        val tv_value_next_id = open_price_dialog.findViewById(R.id.tv_value_next_id) as TextView
        val tv_currency_amount = open_price_dialog.findViewById(R.id.tv_currency_amount) as TextView
        val tv_value_cancel_id =
            open_price_dialog.findViewById(R.id.tv_value_cancel_id) as TextView
        tv_food_item_id.text = name
        tv_currency_amount.text = currencyType
        tv_value_cancel_id.setOnClickListener {
            open_price_dialog.dismiss()
        }
        tv_value_next_id.setOnClickListener {
            if (et_open_price_id.text.toString().trim().equals("")) {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Enter Amount to Proceed",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val taxAppliedList: ArrayList<SendTaxRates>
                if (taxIncludeOption) {
                    taxAppliedList = taxCalculationNew(
                        qty.toDouble(),
                        et_open_price_id.text.toString().toDouble(),
                        aplicableTaxRates
                    )
                } else {
                    taxAppliedList = ArrayList()
                }
                val customOrderData = OrdersCustomData(
                    et_open_price_id.text.toString(),
                    discountApplicable,
                    id,
                    name,
                    "normal",
                    qty,
                    (qty.toDouble() * et_open_price_id.text.toString()
                        .toDouble()).toString(),
                    "",
                    restaurantId,
                    allmodifiers,
                    id,
                    categoryID,
                    rightmenuID,
                    false,
                    "",
                    ArrayList(),
                    itemStatus,
                    isCombo = false,
                    isBogo = false,
                    comboOrBogoOfferId = "",
                    comboOrBogoUniqueId = "",
                    voidStatus = false,
                    voidQuantity = 0,
                    aplicableTaxRates = aplicableTaxRates,
                    taxAppliedList = taxAppliedList,
                    diningOptionTaxException = diningOptionTaxException,
                    diningTaxOption = diningTaxOption,
                    taxIncludeOption = taxIncludeOption
                )
                // adding item to order list
                custom_array.add(customOrderData)
                handleDiscounts()
                fullOrderSetUp()
                open_price_dialog.dismiss()
            }
        }
        open_price_dialog.show()
    }

    private fun GetRightMenuResponse(json_obj: JsonObject) {
        val RightMenuArray = ArrayList<GetRightMenuResponseList>()
        if (from_screen == "table_service") {
            // don't show loader
        } else {
            loading_dialog.show()
        }
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getRightMenuApi(json_obj)
        call.enqueue(object : Callback<GetRightMenuResponse> {
            override fun onFailure(call: Call<GetRightMenuResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetRightMenuResponse>,
                response: Response<GetRightMenuResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (response.body()!!.responseStatus == 1) {
                        for (i in 0 until response.body()!!.menuList!!.size) {
                            RightMenuArray.add(response.body()!!.menuList!![i])
                        }
                        if (RightMenuArray.size > 0) {
                            global_right_menu_id = response.body()!!.menuList!![0].id!!.toString()
                            val obj = JSONObject()
                            obj.put("userId", mEmployeeId)
                            obj.put("restaurantId", restaurantId)
                            obj.put("menuId", response.body()!!.menuList!![0].id)
                            val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                            Log.e("groups_menu_data", jObject.toString())
                            GetCategotyGroupResponse(jObject)
                        }
                    }
                    val order_LayoutManager =
                        LinearLayoutManager(
                            this@OrderDetailsActivity,
                            RecyclerView.VERTICAL,
                            false
                        )
                    rv_right_menu_id.layoutManager = order_LayoutManager
                    val adapter = RightMenuAdapter(this@OrderDetailsActivity, RightMenuArray)
                    rv_right_menu_id.adapter = adapter
                    adapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class RightMenuAdapter(
        context: Context,
        val ourRecommendationsArray: ArrayList<GetRightMenuResponseList>
    ) :
        RecyclerView.Adapter<RightMenuAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = 0

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.right_menu_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourRecommendationsArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_Rmenu_Name_id.text = ourRecommendationsArray[position].menuName
            holder.ll_right_memu_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                ll_gift_content_id.visibility = View.GONE
                ll_main_course_id.visibility = View.VISIBLE
                ll_edit_item_qty_id.visibility = View.GONE
                ll_menu_item_edit_id.visibility = View.GONE
                ll_bogo_id.visibility = View.GONE
                ll_combo_id.visibility = View.GONE
                val obj = JSONObject()
                obj.put("userId", mEmployeeId)
                obj.put("restaurantId", restaurantId)
                obj.put("menuId", ourRecommendationsArray[position].id)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.e("groups_menu_data", jObject.toString())
                GetCategotyGroupResponse(jObject)
            }
            if (selectedPosition == position) {
                holder.ll_right_memu_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.graditent_bg
                )
                global_right_menu_id = ourRecommendationsArray[position].id!!.toString()
            } else {
                holder.ll_right_memu_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.right_menu_uncheck_bg
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_Rmenu_Name_id = view.findViewById<TextView>(R.id.tv_Rmenu_Name_id)
            val ll_right_memu_id = view.findViewById<LinearLayout>(R.id.ll_right_memu_id)
        }

    }

    inner class SizeBasedAdapter(
        context: Context,
        val ourRecommendationsArray: ArrayList<getSizeList>
    ) :
        RecyclerView.Adapter<SizeBasedAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = 0

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.size_list_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourRecommendationsArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = ourRecommendationsArray[position].sizeName
            holder.cv_modifier_list_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
            }
            if (selectedPosition == position) {
                holder.cv_modifier_list_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.graditent_bg
                )
                holder.tv_title_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_modifier_list_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.gray_background
                )
                holder.tv_title_id.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.black_80
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_modifier_list_id = view.findViewById<CardView>(R.id.cv_modifier_list_id)
        }

    }

    inner class DinningOptionAdapter(
        context: Context,
        val ourDinningOptionArray: ArrayList<GetDinningOptionsListResponseList>
    ) :
        RecyclerView.Adapter<DinningOptionAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.dine_in_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourDinningOptionArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = ourDinningOptionArray[position].optionName
            holder.cv_sub_list_id.setOnClickListener {
                selectedPosition = position
                diningOptionSelectedPosition = position
                notifyDataSetChanged()
                dinningOptionBehaviour = ourDinningOptionArray[position].behavior!!
                dinningName = ourDinningOptionArray[position].optionName!!
                dinningId = ourDinningOptionArray[position].id!!
                when (ourDinningOptionArray[position].behavior!!) {
                    "delivery" -> {
                        isDeliverySelected = true
                        isCurbSideSelected = false
                        isDineInSelected = false
                        isTakeOutSelected = false
                        tv_check_table_dinein_option.text =
                            "#$checkNumber, Table $tableNumber, $dinningName"
                        if (custom_array.size > 0) {
                            for (i in 0 until custom_array.size) {
                                if (custom_array[i].diningTaxOption) {
                                    val orderCustomData = OrdersCustomData(
                                        custom_array[i].basePrice,
                                        custom_array[i].discountApplicable,
                                        custom_array[i].id,
                                        custom_array[i].name,
                                        custom_array[i].itemType,
                                        custom_array[i].qty,
                                        custom_array[i].total,
                                        custom_array[i].discount,
                                        custom_array[i].restaurantId,
                                        custom_array[i].allmodifierArray,
                                        custom_array[i].itemId,
                                        custom_array[i].menuGroupId,
                                        custom_array[i].menuId,
                                        custom_array[i].discountApplied,
                                        custom_array[i].discountAppliedType,
                                        custom_array[i].discountsArray,
                                        custom_array[i].itemStatus,
                                        custom_array[i].isCombo,
                                        custom_array[i].isBogo,
                                        custom_array[i].comboOrBogoOfferId,
                                        custom_array[i].comboOrBogoUniqueId,
                                        custom_array[i].voidStatus,
                                        custom_array[i].voidQuantity,
                                        custom_array[i].aplicableTaxRates,
                                        ArrayList(),
                                        custom_array[i].diningOptionTaxException,
                                        custom_array[i].diningTaxOption,
                                        custom_array[i].taxIncludeOption
                                    )
                                    custom_array[i] = orderCustomData
                                }
                            }
                            fullOrderSetUp()
                        }
                    }
                    "curbside" -> {
                        isDeliverySelected = false
                        isCurbSideSelected = true
                        isDineInSelected = false
                        isTakeOutSelected = false
                        tv_check_table_dinein_option.text =
                            "#$checkNumber, Table $tableNumber, $dinningName"
                    }
                    "dine_in" -> {
                        isDeliverySelected = false
                        isCurbSideSelected = false
                        isDineInSelected = true
                        isTakeOutSelected = false
                        tv_check_table_dinein_option.text =
                            "#$checkNumber, Table $tableNumber, $dinningName"
                        if (custom_array.size > 0) {
                            for (i in 0 until custom_array.size) {
                                if (custom_array[i].taxIncludeOption) {
                                    val taxAppliedList: ArrayList<SendTaxRates>
                                    if (custom_array[i].taxIncludeOption) {
                                        taxAppliedList = taxCalculationNew(
                                            custom_array[i].qty.toDouble(),
                                            custom_array[i].basePrice.toDouble(),
                                            custom_array[i].aplicableTaxRates
                                        )
                                    } else {
                                        taxAppliedList = ArrayList()
                                    }
                                    val orderCustomData = OrdersCustomData(
                                        custom_array[i].basePrice,
                                        custom_array[i].discountApplicable,
                                        custom_array[i].id,
                                        custom_array[i].name,
                                        custom_array[i].itemType,
                                        custom_array[i].qty,
                                        custom_array[i].total,
                                        custom_array[i].discount,
                                        custom_array[i].restaurantId,
                                        custom_array[i].allmodifierArray,
                                        custom_array[i].itemId,
                                        custom_array[i].menuGroupId,
                                        custom_array[i].menuId,
                                        custom_array[i].discountApplied,
                                        custom_array[i].discountAppliedType,
                                        custom_array[i].discountsArray,
                                        custom_array[i].itemStatus,
                                        custom_array[i].isCombo,
                                        custom_array[i].isBogo,
                                        custom_array[i].comboOrBogoOfferId,
                                        custom_array[i].comboOrBogoUniqueId,
                                        custom_array[i].voidStatus,
                                        custom_array[i].voidQuantity,
                                        custom_array[i].aplicableTaxRates,
                                        taxAppliedList,
                                        custom_array[i].diningOptionTaxException,
                                        custom_array[i].diningTaxOption,
                                        custom_array[i].taxIncludeOption
                                    )
                                    custom_array[i] = orderCustomData
                                }
                            }
                            fullOrderSetUp()
                        }
                    }
                    "take_out" -> {
                        isDeliverySelected = false
                        isCurbSideSelected = false
                        isDineInSelected = false
                        isTakeOutSelected = true
                        tv_check_table_dinein_option.text =
                            "#$checkNumber, Table $tableNumber, $dinningName"
                        if (custom_array.size > 0) {
                            for (i in 0 until custom_array.size) {
                                if (custom_array[i].diningTaxOption) {
                                    val orderCustomData = OrdersCustomData(
                                        custom_array[i].basePrice,
                                        custom_array[i].discountApplicable,
                                        custom_array[i].id,
                                        custom_array[i].name,
                                        custom_array[i].itemType,
                                        custom_array[i].qty,
                                        custom_array[i].total,
                                        custom_array[i].discount,
                                        custom_array[i].restaurantId,
                                        custom_array[i].allmodifierArray,
                                        custom_array[i].itemId,
                                        custom_array[i].menuGroupId,
                                        custom_array[i].menuId,
                                        custom_array[i].discountApplied,
                                        custom_array[i].discountAppliedType,
                                        custom_array[i].discountsArray,
                                        custom_array[i].itemStatus,
                                        custom_array[i].isCombo,
                                        custom_array[i].isBogo,
                                        custom_array[i].comboOrBogoOfferId,
                                        custom_array[i].comboOrBogoUniqueId,
                                        custom_array[i].voidStatus,
                                        custom_array[i].voidQuantity,
                                        custom_array[i].aplicableTaxRates,
                                        ArrayList(),
                                        custom_array[i].diningOptionTaxException,
                                        custom_array[i].diningTaxOption,
                                        custom_array[i].taxIncludeOption
                                    )
                                    custom_array[i] = orderCustomData
                                }
                            }
                            fullOrderSetUp()
                        }
                    }
                    else -> {
                        tv_check_table_dinein_option.text =
                            "#$checkNumber, Table $tableNumber, $dinningName"
                    }
                }
            }
            selectedPosition = diningOptionSelectedPosition
            holder.tv_title_id.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
            if (selectedPosition == position) {
                holder.cv_sub_list_id.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.graditent_bg)
            } else {
                holder.cv_sub_list_id.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.gray_background)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_sub_list_id = view.findViewById<CardView>(R.id.cv_sub_list_id)
        }

    }

    private fun GetCategotyGroupResponse(json_obj: JsonObject) {
        category_array = ArrayList()
        category_array!!.clear()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getMenuGroupApi(json_obj)
        call.enqueue(object : Callback<GetMenuGroupResponse> {
            override fun onFailure(call: Call<GetMenuGroupResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetMenuGroupResponse>,
                response: Response<GetMenuGroupResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        for (i in 0 until response.body()!!.menugroupsList!!.size) {
                            category_array!!.add(response.body()!!.menugroupsList!![i])
                        }
                        if (category_array!!.size > 0) {
                            val obj = JSONObject()
                            obj.put("menuGroupId", response.body()!!.menugroupsList!![0].id)
                            obj.put("menuId", global_right_menu_id)
                            obj.put("restaurantId", restaurantId)
                            val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                            Log.e("sub_group_data", jObject.toString())
                            GetSubCategotyResponse(jObject, true)
                        } else {
                            GetSubCategotyResponse(JsonObject(), false)
                        }
                    } else {
                        GetSubCategotyResponse(JsonObject(), false)
                    }
                    categories_LayoutManager =
                        LinearLayoutManager(
                            this@OrderDetailsActivity,
                            RecyclerView.HORIZONTAL,
                            false
                        )
                    rv_category_id.layoutManager =
                        categories_LayoutManager
                    val adapter =
                        CategoryListAdapter(this@OrderDetailsActivity, category_array!!)
                    rv_category_id.adapter = adapter
                    adapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun GetSubCategotyResponse(json_obj: JsonObject, hasData: Boolean) {
        subCategoryArray.clear()
        loading_dialog.show()
        if (hasData) {
            val apiInterface = ApiInterface.create()
            val call = apiInterface.getMenuItemsListApi(json_obj)
            call.enqueue(object : Callback<GetMenuItemsListResponse> {
                override fun onFailure(call: Call<GetMenuItemsListResponse>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)
                }

                override fun onResponse(
                    call: Call<GetMenuItemsListResponse>,
                    response: Response<GetMenuItemsListResponse>?
                ) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {
                        Log.e("update", response!!.body()!!.responseStatus!!.toString())
                        if (response.body()!!.responseStatus == 1) {
                            if (loading_dialog.isShowing)
                                loading_dialog.dismiss()
                            for (i in 0 until response.body()!!.menuItem!!.size) {
                                subCategoryArray.add(response.body()!!.menuItem!![i])
                            }
                        }
                        val mNoOfColumns_drinks =
                            Utility.calculateNoOfColumns(this@OrderDetailsActivity, 250F)
                        val drinks_LayoutManager =
                            GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
                        rv_sub_category_id.layoutManager = drinks_LayoutManager
                        val adapter_drinks =
                            SubListAdapter(this@OrderDetailsActivity, subCategoryArray)
                        rv_sub_category_id.adapter = adapter_drinks
                        adapter_drinks.notifyDataSetChanged()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
        } else {
            if (loading_dialog.isShowing)
                loading_dialog.dismiss()
            val mNoOfColumns_drinks =
                Utility.calculateNoOfColumns(this@OrderDetailsActivity, 250F)
            val drinks_LayoutManager =
                GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
            rv_sub_category_id.layoutManager = drinks_LayoutManager
            val adapter_drinks =
                SubListAdapter(this@OrderDetailsActivity, subCategoryArray)
            rv_sub_category_id.adapter = adapter_drinks
            adapter_drinks.notifyDataSetChanged()
        }

    }

    private fun GetDinningOptionsAPI(json_obj: JsonObject) {
        val dinningOptionsArray = ArrayList<GetDinningOptionsListResponseList>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDinningOptionsListApi(json_obj)
        call.enqueue(object : Callback<GetDinningOptionsListResponse> {
            override fun onFailure(call: Call<GetDinningOptionsListResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetDinningOptionsListResponse>,
                response: Response<GetDinningOptionsListResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        for (i in 0 until response.body()!!.dinningOptionsList!!.size) {
                            dinningOptionsArray.add(response.body()!!.dinningOptionsList!![i])
                        }
                        for (j in 0 until dinningOptionsArray.size) {
                            if (dinningId == dinningOptionsArray[j].id!!) {
                                diningOptionSelectedPosition = j
                            }
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    rv_dinein_opt_id.visibility = View.VISIBLE
                    val mNoOfColumns_drinks =
                        Utility.calculateNoOfColumns(this@OrderDetailsActivity, 250F)
                    val drinks_LayoutManager =
                        GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
                    rv_dinein_opt_id.layoutManager = drinks_LayoutManager
                    val adapter_drinks =
                        DinningOptionAdapter(this@OrderDetailsActivity, dinningOptionsArray)
                    rv_dinein_opt_id.adapter = adapter_drinks
                    adapter_drinks.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showVoidReasonDialog() {
        voidQty = 0
        voidReasonId = ""
        voidReason = ""
        void_reason_dialog = Dialog(this)
        void_reason_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.void_item_reason_dialog, null)
        void_reason_dialog.setContentView(view)
        void_reason_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        void_reason_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        void_reason_dialog.setCanceledOnTouchOutside(false)
        val cv_minus = view.findViewById(R.id.cv_minus) as CardView
        val cv_plus = view.findViewById(R.id.cv_plus) as CardView
        val item_name = view.findViewById(R.id.item_name) as TextView
        val tv_quantity = view.findViewById(R.id.tv_quantity) as TextView
        val sp_void_reason = view.findViewById(R.id.sp_void_reason) as Spinner
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        item_name.text = custom_array[item_index!!].name
        voidQty = custom_array[item_index!!].qty.toInt()
        tv_quantity.text = voidQty.toString()
        cv_minus.setOnClickListener {
            if (voidQty == 0) {
                // don't remove
            } else {
                voidQty--
            }
            tv_quantity.text = voidQty.toString()
        }
        cv_plus.setOnClickListener {
            if (custom_array[item_index!!].qty == voidQty.toString()) {
                // don't add
            } else {
                voidQty++
            }
            tv_quantity.text = voidQty.toString()
        }
        val adapter = VoidReasonsListAdapter(this, void_reasonsList)
        sp_void_reason.adapter = adapter
        sp_void_reason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = adapter.getSelectedItem(position)
                voidReasonId = selectedItem.id.toString()
                voidReason = selectedItem.name.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_cancel.setOnClickListener {
            void_reason_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("id", custom_array[item_index!!].id)
            jObj.put("voidId", voidReasonId)
            jObj.put("voidReason", voidReason)
            jObj.put("voidQuantity", voidQty)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            voidItem(finalObject)
        }
        void_reason_dialog.show()
    }

    private fun CreateAddOrder(SaveType: Int, actionType: String) {
        if (dinningId == "") {
            CenterToast().showToast(this@OrderDetailsActivity,"Please choose any dinning option!")
        } else {
            if (custom_array.size != 0) {
                val mMainjsonObj = JSONObject()
                val mJsonArray = JSONArray()
                custom_array1.clear()
                custom_array1.addAll(custom_array)
                for (m in 0 until custom_array.size) {
                    var qty = 0
                    Log.d("itemId", custom_array[m].itemId)
                    println("ArrayList with orj: " + custom_array.size + "--" + custom_array1.size)
                    for (i in 0 until custom_array1.size) {
                        Log.d("positions", "$m$i")
                        Log.d("qty", "$m$i" + "qty $qty")
                        if ((custom_array[m].itemId == custom_array1[i].itemId) && (!custom_array[m].isCombo && !custom_array1[i].isCombo) && (!custom_array[m].isBogo && !custom_array1[i].isBogo)) {
                            if ((custom_array1[i].allmodifierArray.size == 0 && custom_array[m].allmodifierArray.size == 0) && (custom_array1[i].basePrice == custom_array[m].basePrice)) {
                                qty += custom_array1[i].qty.toInt()
                                val taxAppliedList: ArrayList<SendTaxRates>
                                if (custom_array1[i].taxIncludeOption) {
                                    taxAppliedList = taxCalculationNew(
                                        qty.toDouble(),
                                        custom_array1[i].basePrice.toDouble(),
                                        custom_array1[i].aplicableTaxRates
                                    )
                                } else {
                                    taxAppliedList = ArrayList()
                                }
                                val orderCustomData = OrdersCustomData(
                                    custom_array1[i].basePrice,
                                    custom_array1[i].discountApplicable,
                                    custom_array1[i].id,
                                    custom_array1[i].name,
                                    custom_array1[i].itemType,
                                    qty.toString(),
                                    (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                    custom_array1[i].discount,
                                    custom_array1[i].restaurantId,
                                    custom_array1[i].allmodifierArray,
                                    custom_array1[i].itemId,
                                    custom_array1[i].menuGroupId,
                                    custom_array1[i].menuId,
                                    custom_array1[i].discountApplied,
                                    custom_array1[i].discountAppliedType,
                                    custom_array1[i].discountsArray,
                                    custom_array1[i].itemStatus,
                                    custom_array1[i].isCombo,
                                    custom_array1[i].isBogo,
                                    custom_array1[i].comboOrBogoOfferId,
                                    custom_array1[i].comboOrBogoUniqueId,
                                    custom_array1[i].voidStatus,
                                    custom_array1[i].voidQuantity,
                                    custom_array1[i].aplicableTaxRates,
                                    taxAppliedList,
                                    custom_array1[i].diningOptionTaxException,
                                    custom_array1[i].diningTaxOption,
                                    custom_array1[i].taxIncludeOption
                                )
                                Log.d("qty if", "$m$i" + "qty $qty")
                                custom_array1[m] = orderCustomData
                            }
                        } else if ((custom_array[m].itemId == custom_array1[i].itemId) && (custom_array[m].isCombo && custom_array1[i].isCombo) && (!custom_array[m].isBogo && !custom_array1[i].isBogo)) {
                            // combo
                            if (custom_array[m].comboOrBogoOfferId == custom_array1[i].comboOrBogoOfferId) {
                                if (custom_array[m].basePrice == custom_array1[i].basePrice) {
                                    qty += custom_array1[i].qty.toInt()
                                    val taxAppliedList: ArrayList<SendTaxRates>
                                    if (custom_array1[i].taxIncludeOption) {
                                        taxAppliedList = taxCalculationNew(
                                            qty.toDouble(),
                                            custom_array1[i].basePrice.toDouble(),
                                            custom_array1[i].aplicableTaxRates
                                        )
                                    } else {
                                        taxAppliedList = ArrayList()
                                    }
                                    val orderCustomData = OrdersCustomData(
                                        custom_array1[i].basePrice,
                                        custom_array1[i].discountApplicable,
                                        custom_array1[i].id,
                                        custom_array1[i].name,
                                        custom_array1[i].itemType,
                                        qty.toString(),
                                        (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                        custom_array1[i].discount,
                                        custom_array1[i].restaurantId,
                                        custom_array1[i].allmodifierArray,
                                        custom_array1[i].itemId,
                                        custom_array1[i].menuGroupId,
                                        custom_array1[i].menuId,
                                        custom_array1[i].discountApplied,
                                        custom_array1[i].discountAppliedType,
                                        custom_array1[i].discountsArray,
                                        custom_array1[i].itemStatus,
                                        custom_array1[i].isCombo,
                                        custom_array1[i].isBogo,
                                        custom_array1[i].comboOrBogoOfferId,
                                        custom_array1[i].comboOrBogoUniqueId,
                                        custom_array1[i].voidStatus,
                                        custom_array1[i].voidQuantity,
                                        custom_array1[i].aplicableTaxRates,
                                        taxAppliedList,
                                        custom_array1[i].diningOptionTaxException,
                                        custom_array1[i].diningTaxOption,
                                        custom_array1[i].taxIncludeOption
                                    )
                                    Log.d("qty else if", "$m$i" + "qty $qty")
                                    custom_array1[m] = orderCustomData
                                }
                            }
                        } else if ((custom_array[m].itemId == custom_array1[i].itemId) && (!custom_array[m].isCombo && !custom_array1[i].isCombo) && (custom_array[m].isBogo && custom_array1[i].isBogo)) {
                            // bogo
                            if (custom_array[m].comboOrBogoOfferId == custom_array1[i].comboOrBogoOfferId) {
                                if (custom_array[m].comboOrBogoUniqueId == custom_array1[i].comboOrBogoUniqueId) {
                                    if (custom_array[m].discountApplied == custom_array1[i].discountApplied) {
                                        qty += custom_array1[i].qty.toInt()
                                        val taxAppliedList: ArrayList<SendTaxRates>
                                        if (custom_array1[i].taxIncludeOption) {
                                            taxAppliedList = taxCalculationNew(
                                                qty.toDouble(),
                                                custom_array1[i].basePrice.toDouble(),
                                                custom_array1[i].aplicableTaxRates
                                            )
                                        } else {
                                            taxAppliedList = ArrayList()
                                        }
                                        val orderCustomData = OrdersCustomData(
                                            custom_array1[i].basePrice,
                                            custom_array1[i].discountApplicable,
                                            custom_array1[i].id,
                                            custom_array1[i].name,
                                            custom_array1[i].itemType,
                                            qty.toString(),
                                            (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                            custom_array1[i].discount,
                                            custom_array1[i].restaurantId,
                                            custom_array1[i].allmodifierArray,
                                            custom_array1[i].itemId,
                                            custom_array1[i].menuGroupId,
                                            custom_array1[i].menuId,
                                            custom_array1[i].discountApplied,
                                            custom_array1[i].discountAppliedType,
                                            custom_array1[i].discountsArray,
                                            custom_array1[i].itemStatus,
                                            custom_array1[i].isCombo,
                                            custom_array1[i].isBogo,
                                            custom_array1[i].comboOrBogoOfferId,
                                            custom_array1[i].comboOrBogoUniqueId,
                                            custom_array1[i].voidStatus,
                                            custom_array1[i].voidQuantity,
                                            custom_array1[i].aplicableTaxRates,
                                            taxAppliedList,
                                            custom_array1[i].diningOptionTaxException,
                                            custom_array1[i].diningTaxOption,
                                            custom_array1[i].taxIncludeOption
                                        )
                                        Log.d("qty else if else", "$m$i" + "qty $qty")
                                        custom_array1[m] = orderCustomData
                                    }
                                }
                            }
                        } else {
                            Log.d("qty else", "$m$i" + "qty $qty")
                        }
                        Log.d("qty", "final qty $qty")
                    }
                    println("ArrayList with orj: " + custom_array1)
                    finalCustom_array = custom_array1.distinctBy {
                        listOf(
                            it.itemId,
                            it.basePrice,
                            it.allmodifierArray,
                            it.comboOrBogoUniqueId,
                            it.discountApplied
                        )
                    }
                }
                var total_discount = 0.00
                var totalDiscountAmount = 0.00
                var price_total = 0.00
                var modifier_price_total = 0.00
                var sub_total: Double
                var totalTax = 0.00
                for (newOrder in finalCustom_array.indices) {
                    if (finalCustom_array[newOrder].isCombo /*|| finalCustom_array[newOrder].isBogo*/) {
                        if (!finalCustom_array[newOrder].discountApplied) {
                            price_total += (finalCustom_array[newOrder].qty.toInt() * finalCustom_array[newOrder].basePrice.toDouble())
                            for (m in 0 until finalCustom_array[newOrder].allmodifierArray.size) {
                                modifier_price_total += (finalCustom_array[newOrder].allmodifierArray[m].total)
                            }
                        }
                    } else {
                        price_total += (finalCustom_array[newOrder].qty.toInt() * finalCustom_array[newOrder].basePrice.toDouble())
                        for (m in 0 until finalCustom_array[newOrder].allmodifierArray.size) {
                            modifier_price_total += (finalCustom_array[newOrder].allmodifierArray[m].total)
                        }
                    }
                }
                sub_total = price_total + modifier_price_total
                for (m in finalCustom_array.indices) {
                    println("ArrayList finalCustom_array: " + finalCustom_array)
                    if (finalCustom_array[m].discountApplied && (!finalCustom_array[m].isCombo /*&& !finalCustom_array[m].isBogo*/)) {
                        if (finalCustom_array[m].discountsArray.size > 0) {
                            for (j in 0 until finalCustom_array[m].discountsArray.size) {
                                var discount = 0.00
                                if (finalCustom_array[m].discountsArray[j].discountValueType == "currency"
                                ) {
                                    discount =
                                        finalCustom_array[m].qty.toDouble() * finalCustom_array[m].discountsArray[j].value
                                    Log.d("discount if", "" + discount)
                                } else if (finalCustom_array[m].discountsArray[j].discountValueType == "percentage"
                                ) {
                                    discount =
                                        (finalCustom_array[m].qty.toDouble() * finalCustom_array[m].basePrice.toDouble()) * (finalCustom_array[m].discountsArray[j].value / 100)
                                    Log.d("discount else", "" + discount)
                                }
                                total_discount += discount
                                Log.d("discount for", "$discount-----$total_discount")
                            }
                            totalDiscountAmount = total_discount
                        }
                    } else {
                        totalDiscountAmount = total_discount
                    }
                    val mjsonObj = JSONObject()
                    val mSpecialJson = JSONArray()
                    val mModifierJson = JSONArray()
                    val mTaxesJson = JSONArray()
                    if (finalCustom_array[m].allmodifierArray.size != 0) {
                        for (k in 0 until finalCustom_array[m].allmodifierArray.size) {
                            Log.e(
                                "modifiers_ll",
                                finalCustom_array[m].allmodifierArray[k].name + "===" + finalCustom_array[m].allmodifierArray[k].id
                            )
                            if (finalCustom_array[m].allmodifierArray[k].id == "") {
                                val mSplRequestsObj = JSONObject()
                                mSplRequestsObj.put(
                                    "name",
                                    finalCustom_array[m].allmodifierArray[k].name
                                )
                                mSplRequestsObj.put(
                                    "requestPrice",
                                    finalCustom_array[m].allmodifierArray[k].total
                                )
                                mSpecialJson.put(mSplRequestsObj)
                            } else {
                                val mModifierObj = JSONObject()
                                mModifierObj.put(
                                    "modifierId",
                                    finalCustom_array[m].allmodifierArray[k].id
                                )
                                mModifierObj.put(
                                    "modifierName",
                                    finalCustom_array[m].allmodifierArray[k].name
                                )
                                mModifierObj.put("modifierQty", 1)
                                mModifierObj.put(
                                    "modifierUnitPrice",
                                    finalCustom_array[m].allmodifierArray[k].price
                                )
                                mModifierObj.put(
                                    "modifierTotalPrice",
                                    finalCustom_array[m].allmodifierArray[k].total
                                )
                                for (i in 0 until finalCustom_array[m].allmodifierArray[k].modifierGroupId.size) {
                                    mModifierObj.put(
                                        "modifierGroupId",
                                        finalCustom_array[m].allmodifierArray[k].modifierGroupId[i].id
                                    )
                                }
                                mModifierJson.put(mModifierObj)
                            }
                        }
                    }
                    try {
                        var itemTax = 0.00
                        mjsonObj.put("itemId", finalCustom_array[m].itemId)
                        mjsonObj.put("menuGroupId", finalCustom_array[m].menuGroupId)
                        mjsonObj.put("menuId", finalCustom_array[m].menuId)
                        mjsonObj.put("itemName", finalCustom_array[m].name)
                        mjsonObj.put("quantity", finalCustom_array[m].qty.toInt())
                        if (finalCustom_array[m].taxIncludeOption) {
                            if (finalCustom_array[m].taxAppliedList.size > 0) {
                                for (i in 0 until finalCustom_array[m].taxAppliedList.size) {
                                    val mTax = JSONObject()
                                    mTax.put(
                                        "default",
                                        finalCustom_array[m].taxAppliedList[i].default
                                    )
                                    mTax.put(
                                        "importId",
                                        finalCustom_array[m].taxAppliedList[i].importId
                                    )
                                    mTax.put(
                                        "status",
                                        finalCustom_array[m].taxAppliedList[i].status
                                    )
                                    mTax.put(
                                        "taxName",
                                        finalCustom_array[m].taxAppliedList[i].taxName
                                    )
                                    if (finalCustom_array[m].taxAppliedList[i].taxType == "taxTable") {
                                        val mTaxTableArray = JSONArray()
                                        if (finalCustom_array[m].taxAppliedList[i].taxTable.size > 0) {
                                            for (j in 0 until finalCustom_array[m].taxAppliedList[i].taxTable.size) {
                                                val mTaxTableObject = JSONObject()
                                                mTaxTableObject.put(
                                                    "from",
                                                    finalCustom_array[m].taxAppliedList[i].taxTable[j].from
                                                )
                                                mTaxTableObject.put(
                                                    "priceDifference",
                                                    finalCustom_array[m].taxAppliedList[i].taxTable[j].priceDifference
                                                )
                                                mTaxTableObject.put(
                                                    "repeat",
                                                    finalCustom_array[m].taxAppliedList[i].taxTable[j].repeat
                                                )
                                                mTaxTableObject.put(
                                                    "taxApplied",
                                                    finalCustom_array[m].taxAppliedList[i].taxTable[j].taxApplied
                                                )
                                                mTaxTableObject.put(
                                                    "to",
                                                    finalCustom_array[m].taxAppliedList[i].taxTable[j].to
                                                )
                                                mTaxTableArray.put(mTaxTableObject)
                                            }
                                        }
                                        mTax.put("taxTable", mTaxTableArray)
                                    } else {
                                        mTax.put(
                                            "taxRate",
                                            finalCustom_array[m].taxAppliedList[i].taxRate
                                        )
                                    }
                                    mTax.put(
                                        "taxType",
                                        finalCustom_array[m].taxAppliedList[i].taxTypeId
                                    )
                                    mTax.put("taxid", finalCustom_array[m].taxAppliedList[i].taxid)
                                    mTax.put(
                                        "uniqueNumber",
                                        finalCustom_array[m].taxAppliedList[i].uniqueNumber
                                    )
                                    mTax.put(
                                        "taxAmount",
                                        finalCustom_array[m].taxAppliedList[i].taxAmount
                                    )
                                    itemTax += finalCustom_array[m].taxAppliedList[i].taxAmount
                                    mTaxesJson.put(mTax)
                                }
                            } else {
                                itemTax += 0.00
                            }
                            mjsonObj.put("taxesList", mTaxesJson)
                        } else {
                            mjsonObj.put("taxesList", mTaxesJson)
                        }
                        mjsonObj.put("taxIncludeOption", finalCustom_array[m].taxIncludeOption)
                        mjsonObj.put("itemTaxAmount", itemTax)
                        totalTax += itemTax
                        if (finalCustom_array[m].isCombo /*|| finalCustom_array[m].isBogo*/) {
                            if (finalCustom_array[m].discountApplied) {
                                mjsonObj.put("unitPrice", 0.00)
                                mjsonObj.put("totalPrice", 0.00)
                            } else {
                                mjsonObj.put("unitPrice", finalCustom_array[m].basePrice.toDouble())
                                mjsonObj.put(
                                    "totalPrice",
                                    (finalCustom_array[m].qty.toInt() * finalCustom_array[m].basePrice.toDouble())
                                )
                            }
                        } else {
                            mjsonObj.put("unitPrice", finalCustom_array[m].basePrice.toDouble())
                            mjsonObj.put(
                                "totalPrice",
                                (finalCustom_array[m].qty.toInt() * finalCustom_array[m].basePrice.toDouble())
                            )
                        }
                        mjsonObj.put("specialRequestList", mSpecialJson)
                        mjsonObj.put("modifiersList", mModifierJson)
                        mjsonObj.put("itemType", finalCustom_array[m].itemType)
                        if (finalCustom_array[m].discountApplied) {
                            if (finalCustom_array[m].discountsArray.size > 0) {
                                for (j in 0 until finalCustom_array[m].discountsArray.size) {
                                    mjsonObj.put(
                                        "discountId",
                                        finalCustom_array[m].discountsArray[j].id
                                    )
                                    mjsonObj.put(
                                        "discountLevel",
                                        finalCustom_array[m].discountAppliedType
                                    )
                                    mjsonObj.put(
                                        "discountName",
                                        finalCustom_array[m].discountsArray[j].name
                                    )
                                    mjsonObj.put(
                                        "discountType",
                                        finalCustom_array[m].discountsArray[j].discountValueType
                                    )
                                    mjsonObj.put(
                                        "discountValue",
                                        finalCustom_array[m].discountsArray[j].value
                                    )
                                    var discountAmount = 0.00
                                    if (finalCustom_array[m].discountsArray[j].discountValueType == "currency"
                                    ) {
                                        discountAmount =
                                            finalCustom_array[m].discountsArray[j].value
                                    } else if (finalCustom_array[m].discountsArray[j].discountValueType == "percentage"
                                    ) {
                                        discountAmount =
                                            (finalCustom_array[m].basePrice.toDouble() * finalCustom_array[m].qty.toDouble()) * (finalCustom_array[m].discountsArray[j].value / 100)
                                    }
                                    mjsonObj.put("discountAmount", discountAmount)
                                }
                            }
                        } else {
                            mjsonObj.put("discountId", "")
                            mjsonObj.put("discountName", "")
                            mjsonObj.put("discountLevel", "")
                            mjsonObj.put("discountType", "")
                            mjsonObj.put("discountValue", 0)
                            mjsonObj.put("discountAmount", 0)
                        }
                        if (finalCustom_array[m].isCombo) {
                            mjsonObj.put("comboId", finalCustom_array[m].comboOrBogoOfferId)
                            mjsonObj.put("comboUniqueId", finalCustom_array[m].comboOrBogoUniqueId)
                        }
                        if (finalCustom_array[m].isBogo) {
                            mjsonObj.put("bogoId", finalCustom_array[m].comboOrBogoOfferId)
                            mjsonObj.put("bogoUniqueId", finalCustom_array[m].comboOrBogoUniqueId)
                        }
                        mjsonObj.put("isCombo", finalCustom_array[m].isCombo)
                        mjsonObj.put("isBogo", finalCustom_array[m].isBogo)
                        mjsonObj.put("discountApplicable", finalCustom_array[m].discountApplicable)
                        mjsonObj.put("discountApplied", finalCustom_array[m].discountApplied)
                        mJsonArray.put(mjsonObj)
                    } catch (e: Exception) {
                        throw e
                    }
                }
                val customerDetailsOBJ = JSONObject()
                val deliveryInfoOBJ = JSONObject()
                val curbsideInfoOBJ = JSONObject()
                customerDetailsOBJ.put("id", customerId)
                if (isHavingDeliveryDetails) {
                    deliveryInfoOBJ.put("id", customer_delivery_address_id)
                    deliveryInfoOBJ.put("notes", customer_delivery_address_notes)
                } else {
                    deliveryInfoOBJ.put("id", customer_delivery_address_id)
                    deliveryInfoOBJ.put("fullAddress", customer_delivery_address)
                    deliveryInfoOBJ.put("latitude", customer_delivery_address_lat)
                    deliveryInfoOBJ.put("longitude", customer_delivery_address_long)
                    deliveryInfoOBJ.put("notes", customer_delivery_address_notes)
                }
                if (isDeliverySelected) {
                    mMainjsonObj.put("deliveryInfo", deliveryInfoOBJ)
                }
                if (isCurbSideSelected) {
                    curbsideInfoOBJ.put("notes", notes)
                    curbsideInfoOBJ.put("transportColor", transportColor)
                    curbsideInfoOBJ.put("transportDescription", transportDescription)
                    mMainjsonObj.put("curbsideInfo", curbsideInfoOBJ)
                }
                mMainjsonObj.put("orderItems", mJsonArray)
                mMainjsonObj.put("orderNumber", order_number)
                if (isScheduledOrder) {
                    var time = tv_timedatepicker.text.toString()
                    var utcTime = ""
                    if (time.contains("Scheduled time:")) {
                        time = time.replaceFirst("Scheduled time: ", "")
                    }
                    try {
                        val sdf = SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.getDefault())
                        sdf.timeZone = TimeZone.getDefault()
                        val date = sdf.parse(time)
                        val formatterUTC =
                            SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.getDefault())
                        formatterUTC.timeZone = TimeZone.getTimeZone("GMT")
                        utcTime = formatterUTC.format(date!!)
                        Log.d("date---", "$time-----$utcTime===${TimeZone.getDefault()}")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    mMainjsonObj.put("processStartTime", utcTime)
                    mMainjsonObj.put("isFutureOrder", isScheduledOrder)
                }
                mMainjsonObj.put("checkNumber", checkNumber)
                if (tableNumber == "null") {
                    mMainjsonObj.put("tableNumber", 0)
                } else {
                    mMainjsonObj.put("tableNumber", tableNumber.toInt())
                }
                if (noOfGuests == "null") {
                    mMainjsonObj.put("guestCount", 0)
                } else {
                    mMainjsonObj.put("guestCount", noOfGuests.toInt())
                }
                mMainjsonObj.put("tipAmount", final_tip_amount)
                mMainjsonObj.put("discountAmount", totalDiscountAmount)
                mMainjsonObj.put("subTotal", sub_total)
                mMainjsonObj.put("taxAmount", totalTax)
                mMainjsonObj.put(
                    "totalAmount",
                    sub_total + totalTax - totalDiscountAmount
                )
                mMainjsonObj.put("dineInOptionId", dinningId)
                mMainjsonObj.put("dineInOptionName", dinningName)
                mMainjsonObj.put("dineInBehaviour", dinningOptionBehaviour)
                if (isDineInSelected) {
                    mMainjsonObj.put("serviceAreaId", serviceAreaId)
                    mMainjsonObj.put("revenueCenterId", revenueCenterId)
                }
                mMainjsonObj.put("createdBy", mEmployeeId)
                mMainjsonObj.put("restaurantId", restaurantId)
                mMainjsonObj.put("creditsUsed", 0)
                mMainjsonObj.put("haveCustomer", isHavingCustomer)
                mMainjsonObj.put("customerDetails", customerDetailsOBJ)
                mMainjsonObj.put("saveType", SaveType)
                mMainjsonObj.put("deviceId", deviceNameId)
                if (actionType == "prepaid") {
                    mMainjsonObj.put("cashDrawerId", cashDrawerId)
                    mMainjsonObj.put(
                        "amount",
                        sub_total + totalTax - total_discountAmount
                    )
                    mMainjsonObj.put("paymentType", 0)
                }
                val jObject = JsonParser.parseString(mMainjsonObj.toString()).asJsonObject
                Log.e("mParentjsonObj_data", jObject.toString())
                if (actionType == "send" || actionType == "stay" || actionType == "send_pay") {
                    if (update_order == "update") {
                        UpdatedOrder(SaveType)
                    } else {
                        PostOrderAPI(jObject)
                    }
                } else if (actionType == "prepaid") {
                    //PrepaidOrderAPI(jObject)
                } else if (actionType == "pay") {
                    //NewPayOrderAPI()
                }
            }
        }
    }

    private fun UpdatedOrder(saveType :Int) {
        /* save type =
        * 0 = stay
        * 1 = send */
        if (custom_array.size != 0) {
            val morderDetailsObject = JSONObject()
            val mMainjsonObj = JSONObject()
            val mJsonArray = JSONArray()
            custom_array1.clear()
            custom_array1.addAll(custom_array)
            for (m in 0 until custom_array.size) {
                var qty = 0
                Log.d("itemId", custom_array[m].itemId)
                println("ArrayList with orj: " + custom_array.size + "--" + custom_array1.size)
                for (i in 0 until custom_array1.size) {
                    Log.d("positions", "$m$i")
                    Log.d("qty", "$m$i" + "qty $qty")
                    if ((custom_array[m].itemId == custom_array1[i].itemId) && (!custom_array[m].isCombo && !custom_array1[i].isCombo) && (!custom_array[m].isBogo && !custom_array1[i].isBogo)) {
                        if ((custom_array1[i].allmodifierArray.size == 0 && custom_array[m].allmodifierArray.size == 0) && (custom_array1[i].basePrice == custom_array[m].basePrice)) {
                            qty += custom_array1[i].qty.toInt()
                            val taxAppliedList: ArrayList<SendTaxRates>
                            if (custom_array1[i].taxIncludeOption) {
                                taxAppliedList = taxCalculationNew(
                                    qty.toDouble(),
                                    custom_array1[i].basePrice.toDouble(),
                                    custom_array1[i].aplicableTaxRates
                                )
                            } else {
                                taxAppliedList = ArrayList()
                            }
                            val orderCustomData = OrdersCustomData(
                                custom_array1[i].basePrice,
                                custom_array1[i].discountApplicable,
                                custom_array1[i].id,
                                custom_array1[i].name,
                                custom_array1[i].itemType,
                                qty.toString(),
                                (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                custom_array1[i].discount,
                                custom_array1[i].restaurantId,
                                custom_array1[i].allmodifierArray,
                                custom_array1[i].itemId,
                                custom_array1[i].menuGroupId,
                                custom_array1[i].menuId,
                                custom_array1[i].discountApplied,
                                custom_array1[i].discountAppliedType,
                                custom_array1[i].discountsArray,
                                custom_array1[i].itemStatus,
                                custom_array1[i].isCombo,
                                custom_array1[i].isBogo,
                                custom_array1[i].comboOrBogoOfferId,
                                custom_array1[i].comboOrBogoUniqueId,
                                custom_array1[i].voidStatus,
                                custom_array1[i].voidQuantity,
                                custom_array1[i].aplicableTaxRates,
                                taxAppliedList,
                                custom_array1[i].diningOptionTaxException,
                                custom_array1[i].diningTaxOption,
                                custom_array1[i].taxIncludeOption
                            )
                            Log.d("qty if", "$m$i" + "qty $qty")
                            custom_array1[m] = orderCustomData
                        }
                    } else if ((custom_array[m].itemId == custom_array1[i].itemId) && (custom_array[m].isCombo && custom_array1[i].isCombo) && (!custom_array[m].isBogo && !custom_array1[i].isBogo)) {
                        // combo
                        if (custom_array[m].comboOrBogoOfferId == custom_array1[i].comboOrBogoOfferId) {
                            if (custom_array[m].basePrice == custom_array1[i].basePrice) {
                                qty += custom_array1[i].qty.toInt()
                                val taxAppliedList: ArrayList<SendTaxRates>
                                if (custom_array1[i].taxIncludeOption) {
                                    taxAppliedList = taxCalculationNew(
                                        qty.toDouble(),
                                        custom_array1[i].basePrice.toDouble(),
                                        custom_array1[i].aplicableTaxRates
                                    )
                                } else {
                                    taxAppliedList = ArrayList()
                                }
                                val orderCustomData = OrdersCustomData(
                                    custom_array1[i].basePrice,
                                    custom_array1[i].discountApplicable,
                                    custom_array1[i].id,
                                    custom_array1[i].name,
                                    custom_array1[i].itemType,
                                    qty.toString(),
                                    (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                    custom_array1[i].discount,
                                    custom_array1[i].restaurantId,
                                    custom_array1[i].allmodifierArray,
                                    custom_array1[i].itemId,
                                    custom_array1[i].menuGroupId,
                                    custom_array1[i].menuId,
                                    custom_array1[i].discountApplied,
                                    custom_array1[i].discountAppliedType,
                                    custom_array1[i].discountsArray,
                                    custom_array1[i].itemStatus,
                                    custom_array1[i].isCombo,
                                    custom_array1[i].isBogo,
                                    custom_array1[i].comboOrBogoOfferId,
                                    custom_array1[i].comboOrBogoUniqueId,
                                    custom_array1[i].voidStatus,
                                    custom_array1[i].voidQuantity,
                                    custom_array1[i].aplicableTaxRates,
                                    taxAppliedList,
                                    custom_array1[i].diningOptionTaxException,
                                    custom_array1[i].diningTaxOption,
                                    custom_array1[i].taxIncludeOption
                                )
                                Log.d("qty else if", "$m$i" + "qty $qty")
                                custom_array1[m] = orderCustomData
                            }
                        }
                    } else if ((custom_array[m].itemId == custom_array1[i].itemId) && (!custom_array[m].isCombo && !custom_array1[i].isCombo) && (custom_array[m].isBogo && custom_array1[i].isBogo)) {
                        // bogo
                        if (custom_array[m].comboOrBogoOfferId == custom_array1[i].comboOrBogoOfferId) {
                            if (custom_array[m].comboOrBogoUniqueId == custom_array1[i].comboOrBogoUniqueId) {
                                if (custom_array[m].discountApplied == custom_array1[i].discountApplied) {
                                    qty += custom_array1[i].qty.toInt()
                                    val taxAppliedList: ArrayList<SendTaxRates>
                                    if (custom_array1[i].taxIncludeOption) {
                                        taxAppliedList = taxCalculationNew(
                                            qty.toDouble(),
                                            custom_array1[i].basePrice.toDouble(),
                                            custom_array1[i].aplicableTaxRates
                                        )
                                    } else {
                                        taxAppliedList = ArrayList()
                                    }
                                    val orderCustomData = OrdersCustomData(
                                        custom_array1[i].basePrice,
                                        custom_array1[i].discountApplicable,
                                        custom_array1[i].id,
                                        custom_array1[i].name,
                                        custom_array1[i].itemType,
                                        qty.toString(),
                                        (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                        custom_array1[i].discount,
                                        custom_array1[i].restaurantId,
                                        custom_array1[i].allmodifierArray,
                                        custom_array1[i].itemId,
                                        custom_array1[i].menuGroupId,
                                        custom_array1[i].menuId,
                                        custom_array1[i].discountApplied,
                                        custom_array1[i].discountAppliedType,
                                        custom_array1[i].discountsArray,
                                        custom_array1[i].itemStatus,
                                        custom_array1[i].isCombo,
                                        custom_array1[i].isBogo,
                                        custom_array1[i].comboOrBogoOfferId,
                                        custom_array1[i].comboOrBogoUniqueId,
                                        custom_array1[i].voidStatus,
                                        custom_array1[i].voidQuantity,
                                        custom_array1[i].aplicableTaxRates,
                                        taxAppliedList,
                                        custom_array1[i].diningOptionTaxException,
                                        custom_array1[i].diningTaxOption,
                                        custom_array1[i].taxIncludeOption
                                    )
                                    Log.d("qty else if else", "$m$i" + "qty $qty")
                                    custom_array1[m] = orderCustomData
                                }
                            }
                        }
                    } else {
                        Log.d("qty else", "$m$i" + "qty $qty")
                    }
                    Log.d("qty", "final qty $qty")
                }
                println("ArrayList with orj: " + custom_array1)
                finalCustom_array = custom_array1.distinctBy {
                    listOf(it.itemId, it.basePrice, it.allmodifierArray, it.comboOrBogoUniqueId)
                }
            }
            var total_discount = 0.00
            var totalDiscountAmount = 0.00
            var totalTax = 0.00
            total_discountAmount = orderDetails.discountAmount!!
            for (m in finalCustom_array.indices) {
                if (finalCustom_array[m].discountApplied && (!finalCustom_array[m].isCombo /*&& !finalCustom_array[m].isBogo*/)) {
                    if (finalCustom_array[m].discountsArray.size > 0) {
                        for (j in 0 until finalCustom_array[m].discountsArray.size) {
                            var discount = 0.00
                            if (finalCustom_array[m].discountsArray[j].discountValueType == "currency"
                            ) {
                                discount =
                                    finalCustom_array[m].qty.toDouble() * finalCustom_array[m].discountsArray[j].value
                                Log.d("discount if", "" + discount)
                            } else if (finalCustom_array[m].discountsArray[j].discountValueType == "percentage"
                            ) {
                                discount =
                                    (finalCustom_array[m].qty.toDouble() * finalCustom_array[m].basePrice.toDouble()) * (finalCustom_array[m].discountsArray[j].value / 100)
                                Log.d("discount else", "" + discount)
                            }
                            total_discount += discount
                            Log.d("discount for", "$discount-----$total_discount")
                        }
                        totalDiscountAmount = total_discount
                    }
                } else {
                    totalDiscountAmount = total_discount
                }
                val mjsonObj = JSONObject()
                val mSpecialJson = JSONArray()
                val mModifierJson = JSONArray()
                val mTaxesJson = JSONArray()
                if (finalCustom_array[m].allmodifierArray.size != 0) {
                    for (k in 0 until finalCustom_array[m].allmodifierArray.size) {
                        Log.e(
                            "modifiers_ll_update",
                            finalCustom_array[m].allmodifierArray[k].name + "===" + finalCustom_array[m].allmodifierArray[k].id
                        )

                        if (finalCustom_array[m].allmodifierArray[k].id == "") {
                            val mSplRequestsObj = JSONObject()
                            mSplRequestsObj.put(
                                "name",
                                finalCustom_array[m].allmodifierArray[k].name
                            )
                            mSplRequestsObj.put(
                                "requestPrice",
                                finalCustom_array[m].allmodifierArray[k].total
                            )
                            mSpecialJson.put(mSplRequestsObj)
                        } else {
                            val mModifierObj = JSONObject()
                            mModifierObj.put(
                                "modifierId",
                                finalCustom_array[m].allmodifierArray[k].id
                            )
                            mModifierObj.put(
                                "modifierName",
                                finalCustom_array[m].allmodifierArray[k].name
                            )
                            mModifierObj.put("modifierQty", 1)
                            mModifierObj.put(
                                "modifierUnitPrice",
                                finalCustom_array[m].allmodifierArray[k].price
                            )
                            mModifierObj.put(
                                "modifierTotalPrice",
                                finalCustom_array[m].allmodifierArray[k].total
                            )
                            for (i in 0 until finalCustom_array[m].allmodifierArray[k].modifierGroupId.size) {
                                mModifierObj.put(
                                    "modifierGroupId",
                                    finalCustom_array[m].allmodifierArray[k].modifierGroupId[i].id
                                )
                            }
                            mModifierJson.put(mModifierObj)
                        }
                    }
                }
                try {
                    var itemTax = 0.00
                    if (finalCustom_array[m].id != finalCustom_array[m].itemId) {
                        // if old item is either updated or not
                        mjsonObj.put("id", finalCustom_array[m].id)
                    } else {
                        // if new item added
                        mjsonObj.put("menuGroupId", finalCustom_array[m].menuGroupId)
                        mjsonObj.put("menuId", finalCustom_array[m].menuId)
                    }
                    mjsonObj.put("itemId", finalCustom_array[m].itemId)
                    mjsonObj.put("itemStatus", 0)
                    mjsonObj.put("itemName", finalCustom_array[m].name)
                    mjsonObj.put("quantity", finalCustom_array[m].qty.toInt())
                    mjsonObj.put("removeQuantity", finalCustom_array[m].voidQuantity)
                    mjsonObj.put("removeStatus", finalCustom_array[m].voidStatus)
                    if (finalCustom_array[m].taxIncludeOption) {
                        if (finalCustom_array[m].taxAppliedList.size > 0) {
                            for (i in 0 until finalCustom_array[m].taxAppliedList.size) {
                                val mTax = JSONObject()
                                mTax.put("default", finalCustom_array[m].taxAppliedList[i].default)
                                mTax.put(
                                    "importId",
                                    finalCustom_array[m].taxAppliedList[i].importId
                                )
                                mTax.put("status", finalCustom_array[m].taxAppliedList[i].status)
                                mTax.put("taxName", finalCustom_array[m].taxAppliedList[i].taxName)
                                if (finalCustom_array[m].taxAppliedList[i].taxType == "taxTable") {
                                    val mTaxTableArray = JSONArray()
                                    if (finalCustom_array[m].taxAppliedList[i].taxTable.size > 0) {
                                        for (j in 0 until finalCustom_array[m].taxAppliedList[i].taxTable.size) {
                                            val mTaxTableObject = JSONObject()
                                            mTaxTableObject.put(
                                                "from",
                                                finalCustom_array[m].taxAppliedList[i].taxTable[j].from
                                            )
                                            mTaxTableObject.put(
                                                "priceDifference",
                                                finalCustom_array[m].taxAppliedList[i].taxTable[j].priceDifference
                                            )
                                            mTaxTableObject.put(
                                                "repeat",
                                                finalCustom_array[m].taxAppliedList[i].taxTable[j].repeat
                                            )
                                            mTaxTableObject.put(
                                                "taxApplied",
                                                finalCustom_array[m].taxAppliedList[i].taxTable[j].taxApplied
                                            )
                                            mTaxTableObject.put(
                                                "to",
                                                finalCustom_array[m].taxAppliedList[i].taxTable[j].to
                                            )
                                            mTaxTableArray.put(mTaxTableObject)
                                        }
                                    }
                                    mTax.put("taxTable", mTaxTableArray)
                                } else {
                                    mTax.put(
                                        "taxRate",
                                        finalCustom_array[m].taxAppliedList[i].taxRate
                                    )
                                }
                                mTax.put(
                                    "taxType",
                                    finalCustom_array[m].taxAppliedList[i].taxTypeId
                                )
                                mTax.put("taxid", finalCustom_array[m].taxAppliedList[i].taxid)
                                mTax.put(
                                    "uniqueNumber",
                                    finalCustom_array[m].taxAppliedList[i].uniqueNumber
                                )
                                mTax.put(
                                    "taxAmount",
                                    finalCustom_array[m].taxAppliedList[i].taxAmount
                                )
                                itemTax += finalCustom_array[m].taxAppliedList[i].taxAmount
                                mTaxesJson.put(mTax)
                            }
                        } else {
                            itemTax += 0.00
                        }
                        mjsonObj.put("taxesList", mTaxesJson)
                    } else {
                        mjsonObj.put("taxesList", mTaxesJson)
                    }
                    mjsonObj.put("taxIncludeOption", finalCustom_array[m].taxIncludeOption)
                    mjsonObj.put("itemTaxAmount", itemTax)
                    totalTax += itemTax
                    if (finalCustom_array[m].isCombo /*|| finalCustom_array[m].isBogo*/) {
                        if (finalCustom_array[m].discountApplied) {
                            mjsonObj.put("unitPrice", 0.00)
                            mjsonObj.put("totalPrice", 0.00)
                        } else {
                            mjsonObj.put("unitPrice", finalCustom_array[m].basePrice.toDouble())
                            mjsonObj.put(
                                "totalPrice",
                                (finalCustom_array[m].qty.toInt() * finalCustom_array[m].basePrice.toDouble())
                            )
                        }
                    } else {
                        mjsonObj.put("unitPrice", finalCustom_array[m].basePrice.toDouble())
                        mjsonObj.put(
                            "totalPrice",
                            (finalCustom_array[m].qty.toInt() * finalCustom_array[m].basePrice.toDouble())
                        )
                    }
                    mjsonObj.put("specialRequestList", mSpecialJson)
                    mjsonObj.put("modifiersList", mModifierJson)
                    mjsonObj.put("modifiersList", mModifierJson)
                    mjsonObj.put("itemType", finalCustom_array[m].itemType)
                    if (finalCustom_array[m].discountApplied) {
                        if (finalCustom_array[m].discountsArray.size > 0) {
                            for (j in 0 until finalCustom_array[m].discountsArray.size) {
                                mjsonObj.put(
                                    "discountId",
                                    finalCustom_array[m].discountsArray[j].id
                                )
                                mjsonObj.put(
                                    "discountLevel",
                                    finalCustom_array[m].discountAppliedType
                                )
                                mjsonObj.put(
                                    "discountName",
                                    finalCustom_array[m].discountsArray[j].name
                                )
                                mjsonObj.put(
                                    "discountType",
                                    finalCustom_array[m].discountsArray[j].discountValueType
                                )
                                mjsonObj.put(
                                    "discountValue",
                                    finalCustom_array[m].discountsArray[j].value
                                )
                                var discountAmount = 0.00
                                if (finalCustom_array[m].discountsArray[j].discountValueType == "currency"
                                ) {
                                    discountAmount =
                                        finalCustom_array[m].discountsArray[j].value
                                } else if (finalCustom_array[m].discountsArray[j].discountValueType == "percentage"
                                ) {
                                    discountAmount =
                                        (finalCustom_array[m].basePrice.toDouble() * finalCustom_array[m].qty.toDouble()) * (finalCustom_array[m].discountsArray[j].value / 100)
                                }
                                mjsonObj.put("discountAmount", discountAmount)
                            }
                        }
                    } else {
                        mjsonObj.put("discountId", "")
                        mjsonObj.put("discountLevel", "")
                        mjsonObj.put("discountName", "")
                        mjsonObj.put("discountType", "")
                        mjsonObj.put("discountValue", 0)
                        mjsonObj.put("discountAmount", 0)
                    }
                    if (finalCustom_array[m].isCombo) {
                        mjsonObj.put("comboId", finalCustom_array[m].comboOrBogoOfferId)
                        mjsonObj.put("comboUniqueId", finalCustom_array[m].comboOrBogoUniqueId)
                    }
                    if (finalCustom_array[m].isBogo) {
                        mjsonObj.put("bogoId", finalCustom_array[m].comboOrBogoOfferId)
                        mjsonObj.put("bogoUniqueId", finalCustom_array[m].comboOrBogoUniqueId)
                    }
                    mjsonObj.put("isCombo", finalCustom_array[m].isCombo)
                    mjsonObj.put("isBogo", finalCustom_array[m].isBogo)
                    mjsonObj.put("voidQuantity", 0)
                    mjsonObj.put("voidStatus", false)
                    mjsonObj.put("discountApplicable", finalCustom_array[m].discountApplicable)
                    mjsonObj.put("discountApplied", finalCustom_array[m].discountApplied)
                    mJsonArray.put(mjsonObj)
                } catch (e: Exception) {
                    throw e
                }
            }
            val customerDetailsOBJ = JSONObject()
            if (orderDetails.haveCustomer!!) {
                customerDetailsOBJ.put("email", customerEmail)
                customerDetailsOBJ.put("firstName", customerFirstName)
                customerDetailsOBJ.put("lastName", customerLastName)
                customerDetailsOBJ.put("id", customerId)
                customerDetailsOBJ.put("phoneNumber", customerPhone)
            } else {
                customerDetailsOBJ.put("email", "")
                customerDetailsOBJ.put("firstName", "")
                customerDetailsOBJ.put("lastName", "")
                customerDetailsOBJ.put("id", "")
                customerDetailsOBJ.put("phoneNumber", "")
            }
            mMainjsonObj.put("orderItems", mJsonArray)
            mMainjsonObj.put("closedOn", orderDetails.closedOn)
            mMainjsonObj.put("checkNumber", orderDetails.checkNumber)
            if (isScheduledOrder) {
                var time = tv_timedatepicker.text.toString()
                if (time.contains("Scheduled time:")) {
                    time = time.replaceFirst("Scheduled time: ", "")
                }
                mMainjsonObj.put("processStartTime", time)
                mMainjsonObj.put("isFutureOrder", isScheduledOrder)
            }
            mMainjsonObj.put("closedOn", null)
            mMainjsonObj.put("createdBy", orderDetails.createdBy)
            mMainjsonObj.put("createdByName", orderDetails.createdByName)
            mMainjsonObj.put("createdOn", orderDetails.createdOn)
            mMainjsonObj.put("creditsUsed", orderDetails.creditsUsed)
            mMainjsonObj.put("dineInOptionName", dinningName)
            mMainjsonObj.put("dineInBehaviour", dinningOptionBehaviour)
            mMainjsonObj.put("dineInOption", dinningId)
            mMainjsonObj.put("discountAmount", totalDiscountAmount)
            mMainjsonObj.put("haveCustomer", orderDetails.haveCustomer!!)
            mMainjsonObj.put("id", orderDetails.id)
            mMainjsonObj.put("orderNumber", orderDetails.orderNumber)
            mMainjsonObj.put("restaurantId", restaurantId)
            mMainjsonObj.put("revenueCenterId", revenueCenterId)
            mMainjsonObj.put("serviceAreaId", serviceAreaId)
            mMainjsonObj.put("status", orderDetails.status)
            mMainjsonObj.put("subTotal", final_sub_total)
            //mMainjsonObj.put("tableNumber", orderDetails.tableNumber)
            if (tableNumber == "null") {
                mMainjsonObj.put("tableNumber", 0)
            } else {
                mMainjsonObj.put("tableNumber", tableNumber.toInt())
            }
            if (noOfGuests == "null") {
                mMainjsonObj.put("guestCount", 0)
            } else {
                mMainjsonObj.put("guestCount", noOfGuests.toInt())
            }
            mMainjsonObj.put("taxAmount", totalTax)
            mMainjsonObj.put("tipAmount", final_tip_amount)
            mMainjsonObj.put(
                "totalAmount",
                final_sub_total + totalTax - totalDiscountAmount
            )
            mMainjsonObj.put("customerDetails", customerDetailsOBJ)
            morderDetailsObject.put("orderDetails", mMainjsonObj)
            if (isStayOrderSelected) {
                if (saveType == 0) {
                    mMainjsonObj.put("saveType", 0)
                } else {
                    mMainjsonObj.put("saveType", 1)
                }
            } else {
                mMainjsonObj.put("saveType", 1)
            }
            val finalJsonObj = JSONObject()
            finalJsonObj.put("orderDetails", mMainjsonObj)
            val jObject = JsonParser.parseString(finalJsonObj.toString()).asJsonObject
            Log.e("mUpdatejsonObj_data", jObject.toString())
            UpdateOrderAPI(jObject)
        }
    }

    private fun GuestDisplayNotification() {
        val mMainjsonObj = JSONObject()
        val mJsonArray = JSONArray()
        custom_array1.clear()
        custom_array1.addAll(custom_array)
        for (m in 0 until custom_array.size) {
            var qty = 0
            Log.d("itemId", custom_array[m].itemId)
            println("ArrayList with orj: " + custom_array.size + "--" + custom_array1.size)
            for (i in 0 until custom_array1.size) {
                Log.d("positions", "$m$i")
                Log.d("qty", "$m$i" + "qty $qty")
                if ((custom_array[m].itemId == custom_array1[i].itemId) && (!custom_array[m].isCombo && !custom_array1[i].isCombo) && (!custom_array[m].isBogo && !custom_array1[i].isBogo)) {
                    if ((custom_array1[i].allmodifierArray.size == 0 && custom_array[m].allmodifierArray.size == 0) && (custom_array1[i].basePrice == custom_array[m].basePrice)) {
                        qty += custom_array1[i].qty.toInt()
                        val taxAppliedList: ArrayList<SendTaxRates>
                        if (custom_array1[i].taxIncludeOption) {
                            taxAppliedList = taxCalculationNew(
                                qty.toDouble(),
                                custom_array1[i].basePrice.toDouble(),
                                custom_array1[i].aplicableTaxRates
                            )
                        } else {
                            taxAppliedList = ArrayList()
                        }
                        val orderCustomData = OrdersCustomData(
                            custom_array1[i].basePrice,
                            custom_array1[i].discountApplicable,
                            custom_array1[i].id,
                            custom_array1[i].name,
                            custom_array1[i].itemType,
                            qty.toString(),
                            (qty * custom_array1[i].basePrice.toDouble()).toString(),
                            custom_array1[i].discount,
                            custom_array1[i].restaurantId,
                            custom_array1[i].allmodifierArray,
                            custom_array1[i].itemId,
                            custom_array1[i].menuGroupId,
                            custom_array1[i].menuId,
                            custom_array1[i].discountApplied,
                            custom_array1[i].discountAppliedType,
                            custom_array1[i].discountsArray,
                            custom_array1[i].itemStatus,
                            custom_array1[i].isCombo,
                            custom_array1[i].isBogo,
                            custom_array1[i].comboOrBogoOfferId,
                            custom_array1[i].comboOrBogoUniqueId,
                            custom_array1[i].voidStatus,
                            custom_array1[i].voidQuantity,
                            custom_array1[i].aplicableTaxRates,
                            taxAppliedList,
                            custom_array1[i].diningOptionTaxException,
                            custom_array1[i].diningTaxOption,
                            custom_array1[i].taxIncludeOption
                        )
                        Log.d("qty if", "$m$i" + "qty $qty")
                        custom_array1[m] = orderCustomData
                    }
                } else if ((custom_array[m].itemId == custom_array1[i].itemId) && (custom_array[m].isCombo && custom_array1[i].isCombo) && (!custom_array[m].isBogo && !custom_array1[i].isBogo)) {
                    // combo
                    if (custom_array[m].comboOrBogoOfferId == custom_array1[i].comboOrBogoOfferId) {
                        if (custom_array[m].basePrice == custom_array1[i].basePrice) {
                            qty += custom_array1[i].qty.toInt()
                            val taxAppliedList: ArrayList<SendTaxRates>
                            if (custom_array1[i].taxIncludeOption) {
                                taxAppliedList = taxCalculationNew(
                                    qty.toDouble(),
                                    custom_array1[i].basePrice.toDouble(),
                                    custom_array1[i].aplicableTaxRates
                                )
                            } else {
                                taxAppliedList = ArrayList()
                            }
                            val orderCustomData = OrdersCustomData(
                                custom_array1[i].basePrice,
                                custom_array1[i].discountApplicable,
                                custom_array1[i].id,
                                custom_array1[i].name,
                                custom_array1[i].itemType,
                                qty.toString(),
                                (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                custom_array1[i].discount,
                                custom_array1[i].restaurantId,
                                custom_array1[i].allmodifierArray,
                                custom_array1[i].itemId,
                                custom_array1[i].menuGroupId,
                                custom_array1[i].menuId,
                                custom_array1[i].discountApplied,
                                custom_array1[i].discountAppliedType,
                                custom_array1[i].discountsArray,
                                custom_array1[i].itemStatus,
                                custom_array1[i].isCombo,
                                custom_array1[i].isBogo,
                                custom_array1[i].comboOrBogoOfferId,
                                custom_array1[i].comboOrBogoUniqueId,
                                custom_array1[i].voidStatus,
                                custom_array1[i].voidQuantity,
                                custom_array1[i].aplicableTaxRates,
                                taxAppliedList,
                                custom_array1[i].diningOptionTaxException,
                                custom_array1[i].diningTaxOption,
                                custom_array1[i].taxIncludeOption
                            )
                            Log.d("qty else if", "$m$i" + "qty $qty")
                            custom_array1[m] = orderCustomData
                        }
                    }
                } else if ((custom_array[m].itemId == custom_array1[i].itemId) && (!custom_array[m].isCombo && !custom_array1[i].isCombo) && (custom_array[m].isBogo && custom_array1[i].isBogo)) {
                    // bogo
                    if (custom_array[m].comboOrBogoOfferId == custom_array1[i].comboOrBogoOfferId) {
                        if (custom_array[m].comboOrBogoUniqueId == custom_array1[i].comboOrBogoUniqueId) {
                            if (custom_array[m].discountApplied == custom_array1[i].discountApplied) {
                                qty += custom_array1[i].qty.toInt()
                                val taxAppliedList: ArrayList<SendTaxRates>
                                if (custom_array1[i].taxIncludeOption) {
                                    taxAppliedList = taxCalculationNew(
                                        qty.toDouble(),
                                        custom_array1[i].basePrice.toDouble(),
                                        custom_array1[i].aplicableTaxRates
                                    )
                                } else {
                                    taxAppliedList = ArrayList()
                                }
                                val orderCustomData = OrdersCustomData(
                                    custom_array1[i].basePrice,
                                    custom_array1[i].discountApplicable,
                                    custom_array1[i].id,
                                    custom_array1[i].name,
                                    custom_array1[i].itemType,
                                    qty.toString(),
                                    (qty * custom_array1[i].basePrice.toDouble()).toString(),
                                    custom_array1[i].discount,
                                    custom_array1[i].restaurantId,
                                    custom_array1[i].allmodifierArray,
                                    custom_array1[i].itemId,
                                    custom_array1[i].menuGroupId,
                                    custom_array1[i].menuId,
                                    custom_array1[i].discountApplied,
                                    custom_array1[i].discountAppliedType,
                                    custom_array1[i].discountsArray,
                                    custom_array1[i].itemStatus,
                                    custom_array1[i].isCombo,
                                    custom_array1[i].isBogo,
                                    custom_array1[i].comboOrBogoOfferId,
                                    custom_array1[i].comboOrBogoUniqueId,
                                    custom_array1[i].voidStatus,
                                    custom_array1[i].voidQuantity,
                                    custom_array1[i].aplicableTaxRates,
                                    taxAppliedList,
                                    custom_array1[i].diningOptionTaxException,
                                    custom_array1[i].diningTaxOption,
                                    custom_array1[i].taxIncludeOption
                                )
                                Log.d("qty else if else", "$m$i" + "qty $qty")
                                custom_array1[m] = orderCustomData
                            }
                        }
                    }
                } else {
                    Log.d("qty else", "$m$i" + "qty $qty")
                }
                Log.d("qty", "final qty $qty")
            }
            println("ArrayList with orj: " + custom_array1)
            finalCustom_array = custom_array1.distinctBy {
                listOf(it.itemId, it.basePrice, it.allmodifierArray, it.comboOrBogoUniqueId)
            }
        }
        var total_discount = 0.00
        var totalDiscountAmount = 0.00
        var totalTax = 0.00
        for (m in finalCustom_array.indices) {
            if (finalCustom_array[m].discountApplied && (!finalCustom_array[m].isCombo && !finalCustom_array[m].isBogo)) {
                if (finalCustom_array[m].discountsArray.size > 0) {
                    for (j in 0 until finalCustom_array[m].discountsArray.size) {
                        var discount = 0.00
                        if (finalCustom_array[m].discountsArray[j].discountValueType == "currency"
                        ) {
                            discount =
                                finalCustom_array[m].qty.toDouble() * finalCustom_array[m].discountsArray[j].value
                            Log.d("discount if", "" + discount)
                        } else if (finalCustom_array[m].discountsArray[j].discountValueType == "percentage"
                        ) {
                            discount =
                                (finalCustom_array[m].qty.toDouble() * finalCustom_array[m].basePrice.toDouble()) * (finalCustom_array[m].discountsArray[j].value / 100)
                            Log.d("discount else", "" + discount)
                        }
                        total_discount += discount
                        Log.d("discount for", "$discount-----$total_discount")
                    }
                    totalDiscountAmount = total_discount
                }
            } else {
                totalDiscountAmount = total_discount
            }
            val mjsonObj = JSONObject()
            val mSpecialJson = JSONArray()
            val mModifierJson = JSONArray()
            val mTaxesJson = JSONArray()
            if (finalCustom_array[m].allmodifierArray.size != 0) {
                for (k in 0 until finalCustom_array[m].allmodifierArray.size) {
                    if (finalCustom_array[m].allmodifierArray[k].id == "") {
                        val mSplRequestsObj = JSONObject()
                        mSplRequestsObj.put(
                            "name",
                            finalCustom_array[m].allmodifierArray[k].name
                        )
                        mSplRequestsObj.put(
                            "requestPrice",
                            finalCustom_array[m].allmodifierArray[k].total
                        )
                        mSpecialJson.put(mSplRequestsObj)
                    } else {
                        val mModifierObj = JSONObject()
                        mModifierObj.put(
                            "modifierId",
                            finalCustom_array[m].allmodifierArray[k].id
                        )
                        mModifierObj.put(
                            "modifierName",
                            finalCustom_array[m].allmodifierArray[k].name
                        )
                        mModifierObj.put("modifierQty", 1)
                        mModifierObj.put(
                            "modifierUnitPrice",
                            finalCustom_array[m].allmodifierArray[k].price
                        )
                        mModifierObj.put(
                            "modifierTotalPrice",
                            finalCustom_array[m].allmodifierArray[k].total
                        )
                        for (i in 0 until finalCustom_array[m].allmodifierArray[k].modifierGroupId.size) {
                            mModifierObj.put(
                                "modifierGroupId",
                                finalCustom_array[m].allmodifierArray[k].modifierGroupId[i].id
                            )
                        }
                        mModifierJson.put(mModifierObj)
                    }
                }
            }
            try {
                var itemTax = 0.00
                mjsonObj.put("itemId", finalCustom_array[m].itemId)
                mjsonObj.put("menuGroupId", finalCustom_array[m].menuGroupId)
                mjsonObj.put("menuId", finalCustom_array[m].menuId)
                mjsonObj.put("itemName", finalCustom_array[m].name)
                mjsonObj.put("quantity", finalCustom_array[m].qty.toInt())
                if (finalCustom_array[m].taxIncludeOption) {
                    if (finalCustom_array[m].taxAppliedList.size > 0) {
                        for (i in 0 until finalCustom_array[m].taxAppliedList.size) {
                            val mTax = JSONObject()
                            mTax.put("default", finalCustom_array[m].taxAppliedList[i].default)
                            mTax.put("importId", finalCustom_array[m].taxAppliedList[i].importId)
                            mTax.put("status", finalCustom_array[m].taxAppliedList[i].status)
                            mTax.put("taxName", finalCustom_array[m].taxAppliedList[i].taxName)
                            if (finalCustom_array[m].taxAppliedList[i].taxType == "taxTable") {
                                val mTaxTableArray = JSONArray()
                                if (finalCustom_array[m].taxAppliedList[i].taxTable.size > 0) {
                                    for (j in 0 until finalCustom_array[m].taxAppliedList[i].taxTable.size) {
                                        val mTaxTableObject = JSONObject()
                                        mTaxTableObject.put(
                                            "from",
                                            finalCustom_array[m].taxAppliedList[i].taxTable[j].from
                                        )
                                        mTaxTableObject.put(
                                            "priceDifference",
                                            finalCustom_array[m].taxAppliedList[i].taxTable[j].priceDifference
                                        )
                                        mTaxTableObject.put(
                                            "repeat",
                                            finalCustom_array[m].taxAppliedList[i].taxTable[j].repeat
                                        )
                                        mTaxTableObject.put(
                                            "taxApplied",
                                            finalCustom_array[m].taxAppliedList[i].taxTable[j].taxApplied
                                        )
                                        mTaxTableObject.put(
                                            "to",
                                            finalCustom_array[m].taxAppliedList[i].taxTable[j].to
                                        )
                                        mTaxTableArray.put(mTaxTableObject)
                                    }
                                }
                                mTax.put("taxTable", mTaxTableArray)
                            } else {
                                mTax.put("taxRate", finalCustom_array[m].taxAppliedList[i].taxRate)
                            }
                            mTax.put("taxType", finalCustom_array[m].taxAppliedList[i].taxTypeId)
                            mTax.put("taxid", finalCustom_array[m].taxAppliedList[i].taxid)
                            mTax.put(
                                "uniqueNumber",
                                finalCustom_array[m].taxAppliedList[i].uniqueNumber
                            )
                            mTax.put("taxAmount", finalCustom_array[m].taxAppliedList[i].taxAmount)
                            itemTax += finalCustom_array[m].taxAppliedList[i].taxAmount
                            mTaxesJson.put(mTax)
                        }
                    } else {
                        itemTax += 0.00
                    }
                    mjsonObj.put("taxesList", mTaxesJson)
                } else {
                    mjsonObj.put("taxesList", mTaxesJson)
                }
                mjsonObj.put("taxIncludeOption", finalCustom_array[m].taxIncludeOption)
                mjsonObj.put("itemTaxAmount", itemTax)
                totalTax += itemTax
                if (finalCustom_array[m].isCombo || finalCustom_array[m].isBogo) {
                    if (finalCustom_array[m].discountApplied) {
                        mjsonObj.put("unitPrice", 0.00)
                        mjsonObj.put("totalPrice", 0.00)
                    } else {
                        mjsonObj.put("unitPrice", finalCustom_array[m].basePrice.toDouble())
                        mjsonObj.put(
                            "totalPrice",
                            (finalCustom_array[m].qty.toInt() * finalCustom_array[m].basePrice.toDouble())
                        )
                    }
                } else {
                    mjsonObj.put("unitPrice", finalCustom_array[m].basePrice.toDouble())
                    mjsonObj.put(
                        "totalPrice",
                        (finalCustom_array[m].qty.toInt() * finalCustom_array[m].basePrice.toDouble())
                    )
                }
                mjsonObj.put("specialRequestList", mSpecialJson)
                mjsonObj.put("modifiersList", mModifierJson)
                mjsonObj.put("itemType", finalCustom_array[m].itemType)
                if (finalCustom_array[m].discountApplied) {
                    if (finalCustom_array[m].discountsArray.size > 0) {
                        for (j in 0 until finalCustom_array[m].discountsArray.size) {
                            mjsonObj.put(
                                "discountId",
                                finalCustom_array[m].discountsArray[j].id
                            )
                            mjsonObj.put(
                                "discountLevel",
                                finalCustom_array[m].discountAppliedType
                            )
                            mjsonObj.put(
                                "discountName",
                                finalCustom_array[m].discountsArray[j].name
                            )
                            mjsonObj.put(
                                "discountType",
                                finalCustom_array[m].discountsArray[j].discountValueType
                            )
                            mjsonObj.put(
                                "discountValue",
                                finalCustom_array[m].discountsArray[j].value
                            )
                            var discountAmount = 0.00
                            if (finalCustom_array[m].discountsArray[j].discountValueType == "currency"
                            ) {
                                discountAmount =
                                    finalCustom_array[m].qty.toDouble() * finalCustom_array[m].discountsArray[j].value
                            } else if (finalCustom_array[m].discountsArray[j].discountValueType == "percentage"
                            ) {
                                discountAmount =
                                    (finalCustom_array[m].qty.toDouble() * finalCustom_array[m].basePrice.toDouble()) * (finalCustom_array[m].discountsArray[j].value / 100)
                            }
                            mjsonObj.put("discountAmount", discountAmount)
                        }
                    }
                } else {
                    mjsonObj.put("discountId", "")
                    mjsonObj.put("discountLevel", "")
                    mjsonObj.put("discountName", "")
                    mjsonObj.put("discountType", "")
                    mjsonObj.put("discountValue", 0)
                    mjsonObj.put("discountAmount", 0)
                }
                if (finalCustom_array[m].isCombo) {
                    mjsonObj.put("comboId", finalCustom_array[m].comboOrBogoOfferId)
                    mjsonObj.put("comboUniqueId", finalCustom_array[m].comboOrBogoUniqueId)
                }
                if (finalCustom_array[m].isBogo) {
                    mjsonObj.put("bogoId", finalCustom_array[m].comboOrBogoOfferId)
                    mjsonObj.put("bogoUniqueId", finalCustom_array[m].comboOrBogoUniqueId)
                }
                mjsonObj.put("isCombo", finalCustom_array[m].isCombo)
                mjsonObj.put("isBogo", finalCustom_array[m].isBogo)
                mjsonObj.put("discountApplicable", finalCustom_array[m].discountApplicable)
                mjsonObj.put("discountApplied", finalCustom_array[m].discountApplied)
                mJsonArray.put(mjsonObj)
            } catch (e: Exception) {
                throw e
            }
        }
        mMainjsonObj.put("orderItems", mJsonArray)
        mMainjsonObj.put("orderNumber", order_number)
        if (isScheduledOrder) {
            var time = tv_timedatepicker.text.toString()
            if (time.contains("Scheduled time:")) {
                time = time.replaceFirst("Scheduled time: ", "")
            }
            mMainjsonObj.put("processStartTime", time)
            mMainjsonObj.put("isFutureOrder", isScheduledOrder)
        }
        mMainjsonObj.put("checkNumber", checkNumber)
        mMainjsonObj.put("discountAmount", totalDiscountAmount)
        mMainjsonObj.put("taxAmount", /*taxAmount*/totalTax)
        mMainjsonObj.put("deviceToken", deviceToken)
        val jObject = JsonParser.parseString(mMainjsonObj.toString()).asJsonObject
        Log.e("mGuestItems_post", jObject.toString())
        OrderGuestNotifications(jObject)
    }

    private fun PostOrderAPI(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.addOrderApi(json_obj)
        call.enqueue(object : Callback<AddOrderResponse> {
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    Log.e("result", response.body()!!.result!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        mOrderId = response.body()!!.orderId.toString()
                        Log.e("mOrderId", mOrderId)
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                        // clearing order list saved in session
                        sessionManager.saveTempOrderList("")
                        if (pay_type != "send_pay") {
                            isOrderSent = true
                            successDialog(response.body()!!.result.toString())
                        } else {
                            val intent =
                                Intent(
                                    this@OrderDetailsActivity,
                                    PaymentScreenActivity::class.java
                                )
                            intent.putExtra("from_screen", "quick_order")
                            intent.putExtra("mOrderId", mOrderId)
                            startActivity(intent)
                            finish()
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun UpdateOrderAPI(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.updateOrderApi(json_obj)
        call.enqueue(object : Callback<AddOrderResponse> {
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    Log.e("result", response.body()!!.result!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                        // clearing order list saved in session
                        sessionManager.saveTempOrderList("")
                        if (pay_type != "send_pay") {
                            if (update_order == "update" && pay_type == "pay") {
                                val intent =
                                    Intent(
                                        this@OrderDetailsActivity,
                                        PaymentScreenActivity::class.java
                                    )
                                intent.putExtra("from_screen", "quick_order")
                                intent.putExtra("mOrderId", mOrderId)
                                startActivity(intent)
                                finish()
                            } else {
                                successDialog(response.body()!!.result.toString())
                            }
                        } else {
                            val intent =
                                Intent(
                                    this@OrderDetailsActivity,
                                    PaymentScreenActivity::class.java
                                )
                            intent.putExtra("from_screen", "quick_order")
                            intent.putExtra("mOrderId", mOrderId)
                            startActivity(intent)
                            finish()
                        }
                        update_order = ""
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun GetOrderNoAPI(json_obj: JsonObject) {
        if (from_screen == "table_service") {
            // don't show loader
        } else {
            loading_dialog.show()
        }
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getOrderNumberApi(json_obj)
        call.enqueue(object : Callback<AddOrderResponse> {
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    order_number = response!!.body()!!.orderNo!!
                    tv_order_no_id.text = "Order #$order_number"
                    tv_check_table_dinein_option.text =
                        "#$checkNumber, Table $tableNumber, $dinningName"
                } catch (e: Exception) {
                    order_number = 1
                    tv_order_no_id.text = "Order #$order_number"
                    e.printStackTrace()
                }
            }
        })
    }

    private fun ExistingOrderAPI(json_obj: JsonObject) {
        loading_dialog.show()
        orderDetails = OrderDetailsResponse()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getTableOrdersListApi(json_obj)
        call.enqueue(object : Callback<GetOrderDEtailsResponse> {
            override fun onFailure(call: Call<GetOrderDEtailsResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetOrderDEtailsResponse>,
                response: Response<GetOrderDEtailsResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus!! == 1) {
                        tableChecks = resp.checksDetails!!
                        if (tableChecks.size > 0) {
                            showTableOrders(tableChecks)
                            //if (tableChecks.size == 1) {
                            //    tv_checks.visibility = View.GONE
                            //} else {
                            tv_checks.visibility = View.VISIBLE
                            // }
                        } else {
                            tv_checks.visibility = View.GONE
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun GetDiscountsAPI(json_obj: JsonObject) {
        discountssArray = ArrayList()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDiscountsApi(json_obj)
        call.enqueue(object : Callback<AddDiscountResponse> {
            override fun onFailure(call: Call<AddDiscountResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddDiscountResponse>,
                response: Response<AddDiscountResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        for (i in 0 until resp.discounts!!.size) {
                            val exceptDiscountItems: ArrayList<DiscountItems>
                            val includeDiscountItems: ArrayList<DiscountItems>
                            val value: Double
                            val appliesTo: Int
                            val discountValueType: String
                            val maxDiscountAmount: Double
                            val minDiscountAmount: Double
                            if (resp.discounts[i].maxDiscountAmount.toString() == "null") {
                                maxDiscountAmount = 0.00
                            } else {
                                maxDiscountAmount = resp.discounts[i].maxDiscountAmount!!
                            }
                            if (resp.discounts[i].minDiscountAmount.toString() == "null") {
                                minDiscountAmount = 0.00
                            } else {
                                minDiscountAmount = resp.discounts[i].minDiscountAmount!!
                            }
                            if (resp.discounts[i].type!!.toString() == "bogo" ||
                                resp.discounts[i].type!!.toString() == "BOGO" ||
                                resp.discounts[i].type!!.toString() == "combo" ||
                                resp.discounts[i].type!!.toString() == "COMBO" ||
                                resp.discounts[i].type!!.toString() == "fixedDiscount" ||
                                resp.discounts[i].type!!.toString() == "openDiscount"
                            ) {
                                exceptDiscountItems = ArrayList()
                                includeDiscountItems = ArrayList()
                            } else {
                                exceptDiscountItems =
                                    resp.discounts[i].exceptDiscountItems!!
                                includeDiscountItems =
                                    resp.discounts[i].includeDiscountItems!!
                            }
                            if (resp.discounts[i].type!!.toString() == "COMBO" || resp.discounts[i].type!!.toString() == "combo") {
                                appliesTo = 0
                                discountValueType = "currency"
                            } else if (resp.discounts[i].type!!.toString() == "BOGO" || resp.discounts[i].type!!.toString() == "bogo") {
                                if (resp.discounts[i].appliesTo.toString() == "null") {
                                    appliesTo = 0
                                } else {
                                    appliesTo = resp.discounts[i].appliesTo!!
                                }
                                if (resp.discounts[i].discountValueType.toString() == "null") {
                                    discountValueType = "currency"
                                } else {
                                    discountValueType = resp.discounts[i].discountValueType!!
                                }
                            } else {
                                if (resp.discounts[i].appliesTo.toString() == "null") {
                                    appliesTo = 0
                                } else {
                                    appliesTo = resp.discounts[i].appliesTo!!
                                }
                                discountValueType = resp.discounts[i].discountValueType!!
                            }
                            if (resp.discounts[i].type!! == "open" || resp.discounts[i].type!! == "openDiscount") {
                                value = 0.00
                            } else if (resp.discounts[i].type!!.toString() == "BOGO" || resp.discounts[i].type!!.toString() == "bogo") {
                                value = 0.00
                            } else {
                                value = resp.discounts[i].value!!
                            }
                            val getAllDiscountsListResponse = GetAllDiscountsListResponse(
                                resp.discounts[i].allowOtherDiscounts!!,
                                appliesTo,
                                discountValueType,
                                exceptDiscountItems,
                                resp.discounts[i].id!!,
                                includeDiscountItems,
                                maxDiscountAmount,
                                minDiscountAmount,
                                resp.discounts[i].name!!,
                                resp.discounts[i].orderValue!!,
                                resp.discounts[i].status!!,
                                resp.discounts[i].type!!,
                                resp.discounts[i].uniqueNumber!!,
                                value
                            )
                            discountssArray!!.add(getAllDiscountsListResponse)
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    ll_discounts_id.visibility = View.VISIBLE
                    rv_discounts_list_id.visibility = View.VISIBLE
                    val mNoOfColumns_drinks =
                        Utility.calculateNoOfColumns(this@OrderDetailsActivity, 360f)
                    val drinks_LayoutManager =
                        GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
                    rv_discounts_list_id.layoutManager = drinks_LayoutManager
                    val adapter_discounts =
                        OrderDiscountsAdapter(
                            this@OrderDetailsActivity,
                            discountssArray!!,
                            DISCOUNT_LEVEL_CHECK
                        )
                    rv_discounts_list_id.adapter = adapter_discounts
                    adapter_discounts.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun GetDiscountsItemAPI(json_obj: JsonObject) {
        discountssArray = ArrayList()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDiscountsItemApi(json_obj)
        call.enqueue(object : Callback<AddDiscountResponse> {
            override fun onFailure(call: Call<AddDiscountResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddDiscountResponse>,
                response: Response<AddDiscountResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        if (resp.discounts!!.size > 0) {
                            for (i in 0 until resp.discounts.size) {
                                var value: Double
                                val maxDiscountAmount: Double
                                val minDiscountAmount: Double
                                if (resp.discounts[i].maxDiscountAmount.toString() == "null") {
                                    maxDiscountAmount = 0.00
                                } else {
                                    maxDiscountAmount = resp.discounts[i].maxDiscountAmount!!
                                }
                                if (resp.discounts[i].minDiscountAmount.toString() == "null") {
                                    minDiscountAmount = 0.00
                                } else {
                                    minDiscountAmount = resp.discounts[i].minDiscountAmount!!
                                }
                                if (response.body()!!.discounts!![i].type!! == "open" || response.body()!!.discounts!![i].type!! == "openDiscount") {
                                    value = 0.00
                                } else {
                                    value = response.body()!!.discounts!![i].value!!
                                }
                                val getAllDiscountsListResponse = GetAllDiscountsListResponse(
                                    response.body()!!.discounts!![i].allowOtherDiscounts!!,
                                    response.body()!!.discounts!![i].appliesTo!!,
                                    response.body()!!.discounts!![i].discountValueType!!,
                                    ArrayList(),
                                    response.body()!!.discounts!![i].id!!,
                                    ArrayList(),
                                    maxDiscountAmount,
                                    minDiscountAmount,
                                    response.body()!!.discounts!![i].name!!,
                                    response.body()!!.discounts!![i].orderValue!!,
                                    response.body()!!.discounts!![i].status!!,
                                    response.body()!!.discounts!![i].type!!,
                                    response.body()!!.discounts!![i].uniqueNumber!!,
                                    value
                                )
                                discountssArray!!.add(getAllDiscountsListResponse)
                            }
                            Log.e("DISCOUNTSITEMARRAY", "" + discountssArray)
                        } else {
                            Toast.makeText(
                                this@OrderDetailsActivity,
                                resp.result.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    rv_discounts_item_id.visibility = View.VISIBLE
                    val mNoOfColumns_drinks =
                        Utility.calculateNoOfColumns(this@OrderDetailsActivity, 250f)
                    val drinks_LayoutManager =
                        GridLayoutManager(this@OrderDetailsActivity, mNoOfColumns_drinks)
                    rv_discounts_item_id.layoutManager = drinks_LayoutManager
                    val adapter_discounts =
                        OrderDiscountsAdapter(
                            this@OrderDetailsActivity,
                            discountssArray!!,
                            DISCOUNT_LEVEL_ITEM
                        )
                    rv_discounts_item_id.adapter = adapter_discounts
                    adapter_discounts.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class OrderDiscountsAdapter(
        context: Context,
        val sub_listArray: ArrayList<GetAllDiscountsListResponse>, discount_type: String
    ) :
        RecyclerView.Adapter<OrderDiscountsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1
        var discount_type = ""

        init {
            this.mContext = context
            this.discount_type = discount_type
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.sub_category_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return sub_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = sub_listArray[position].name
            holder.tv_title_id.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
            Log.e("discount_type", sub_listArray[position].type)
            Log.e("discount_id", sub_listArray[position].id)
            holder.cv_sub_list_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                discountModeType = sub_listArray[position].name
                discountModeId = sub_listArray[position].id
                /*fixed*/
                /*open*/
                /*BOGO   bogo*/
                /*combo*/
                if (discount_type == DISCOUNT_LEVEL_ITEM) {
                    if (sub_listArray[position].type == "COMBO" || sub_listArray[position].type == "combo") {
                        ll_combo_id.visibility = View.VISIBLE
                        ll_bogo_id.visibility = View.GONE
                        ll_main_course_id.visibility = View.GONE
                        ll_edit_item_qty_id.visibility = View.GONE
                        ll_all_modifiers_id.visibility = View.GONE
                        ll_modifier_list_id.visibility = View.GONE
                        ll_gift_content_id.visibility = View.GONE
                        ll_menu_item_edit_id.visibility = View.GONE
                        ll_main_course_id.visibility = View.GONE
                        ll_edit_item_qty_id.visibility = View.GONE
                        rv_modifier_list_id.visibility = View.GONE
                        tv_modifiers.visibility = View.GONE
                        rv_pre_modifier_list_id.visibility = View.GONE
                        tv_pre_modifiers.visibility = View.GONE
                        ll_all_modifiers_id.visibility = View.GONE
                        ll_gift_content_id.visibility = View.GONE
                        ll_menu_item_edit_id.visibility = View.GONE
                        val obj = JSONObject()
                        obj.put("discountId", sub_listArray[position].id)
                        obj.put("userId", mEmployeeId)
                        obj.put("restaurantId", restaurantId)
                        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                        ComboDiscounts(jObject)
                        minDiscountAmount = sub_listArray[position].minDiscountAmount
                        maxDiscountAmount = sub_listArray[position].maxDiscountAmount
                    } else if (sub_listArray[position].type == "BOGO" || sub_listArray[position].type == "bogo") {
                        ll_bogo_id.visibility = View.VISIBLE
                        ll_combo_id.visibility = View.GONE
                        ll_main_course_id.visibility = View.GONE
                        ll_edit_item_qty_id.visibility = View.GONE
                        ll_all_modifiers_id.visibility = View.GONE
                        ll_modifier_list_id.visibility = View.GONE
                        ll_gift_content_id.visibility = View.GONE
                        ll_menu_item_edit_id.visibility = View.GONE
                        ll_main_course_id.visibility = View.GONE
                        ll_edit_item_qty_id.visibility = View.GONE
                        rv_modifier_list_id.visibility = View.GONE
                        tv_modifiers.visibility = View.GONE
                        rv_pre_modifier_list_id.visibility = View.GONE
                        tv_pre_modifiers.visibility = View.GONE
                        ll_all_modifiers_id.visibility = View.GONE
                        ll_gift_content_id.visibility = View.GONE
                        ll_menu_item_edit_id.visibility = View.GONE
                        val obj = JSONObject()
                        obj.put("discountId", sub_listArray[position].id)
                        obj.put("userId", mEmployeeId)
                        obj.put("restaurantId", restaurantId)
                        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                        BogoDiscounts(
                            jObject,
                            sub_listArray[position].minDiscountAmount,
                            sub_listArray[position].maxDiscountAmount
                        )
                        minDiscountAmount = sub_listArray[position].minDiscountAmount
                        maxDiscountAmount = sub_listArray[position].maxDiscountAmount
                    } else if (sub_listArray[position].type == "open" || sub_listArray[position].type == "openDiscount") {
                        openDiscountsDialog(sub_listArray[position], DISCOUNT_LEVEL_ITEM)
                    } else if (sub_listArray[position].type == "fixed" || sub_listArray[position].type == "fixedDiscount") {
                        checkLevelApplied = false
                        // checkLevelDiscountArray.clear()
                        val discountArray = sub_listArray[position]
                        val discountsArray = ArrayList<GetAllDiscountsListResponse>()
                        discountsArray.add(discountArray)
                        if (custom_array[item_index!!].discountApplicable) {
                            if ((custom_array[item_index!!].qty.toDouble() * custom_array[item_index!!].basePrice.toDouble()) >= discountArray.minDiscountAmount) {
                                val customData = OrdersCustomData(
                                    custom_array[item_index!!].basePrice,
                                    custom_array[item_index!!].discountApplicable,
                                    custom_array[item_index!!].id,
                                    custom_array[item_index!!].name,
                                    custom_array[item_index!!].itemType,
                                    custom_array[item_index!!].qty,
                                    custom_array[item_index!!].total,
                                    custom_array[item_index!!].discount,
                                    custom_array[item_index!!].restaurantId,
                                    custom_array[item_index!!].allmodifierArray,
                                    custom_array[item_index!!].itemId,
                                    custom_array[item_index!!].menuGroupId,
                                    custom_array[item_index!!].menuId,
                                    true,
                                    DISCOUNT_LEVEL_ITEM,
                                    discountsArray,
                                    custom_array[item_index!!].itemStatus,
                                    custom_array[item_index!!].isCombo,
                                    custom_array[item_index!!].isBogo,
                                    custom_array[item_index!!].comboOrBogoOfferId,
                                    custom_array[item_index!!].comboOrBogoUniqueId,
                                    custom_array[item_index!!].voidStatus,
                                    custom_array[item_index!!].voidQuantity,
                                    custom_array[item_index!!].aplicableTaxRates,
                                    custom_array[item_index!!].taxAppliedList,
                                    custom_array[item_index!!].diningOptionTaxException,
                                    custom_array[item_index!!].diningTaxOption,
                                    custom_array[item_index!!].taxIncludeOption
                                )
                                custom_array[item_index!!] = customData
                            } else {
                                Toast.makeText(
                                    this@OrderDetailsActivity,
                                    "Discount is applicable only if total is more than $currencyType${discountArray.minDiscountAmount} !",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            fullOrderSetUp()
                        } else {
                            Toast.makeText(
                                this@OrderDetailsActivity,
                                "Discount is not applicable for this item!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else if (discount_type == DISCOUNT_LEVEL_CHECK) {
                    if (sub_listArray[position].type == "COMBO" || sub_listArray[position].type == "combo") {
                        ll_combo_id.visibility = View.VISIBLE
                        ll_bogo_id.visibility = View.GONE
                        ll_main_course_id.visibility = View.GONE
                        ll_edit_item_qty_id.visibility = View.GONE
                        rv_modifier_list_id.visibility = View.GONE
                        tv_modifiers.visibility = View.GONE
                        ll_all_modifiers_id.visibility = View.GONE
                        ll_modifier_list_id.visibility = View.GONE
                        ll_gift_content_id.visibility = View.GONE
                        ll_menu_item_edit_id.visibility = View.GONE
                        val obj = JSONObject()
                        obj.put("discountId", sub_listArray[position].id)
                        obj.put("userId", mEmployeeId)
                        obj.put("restaurantId", restaurantId)
                        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                        ComboDiscounts(jObject)
                        minDiscountAmount = sub_listArray[position].minDiscountAmount
                        maxDiscountAmount = sub_listArray[position].maxDiscountAmount
                    } else if (sub_listArray[position].type == "BOGO" || sub_listArray[position].type == "bogo") {
                        ll_bogo_id.visibility = View.VISIBLE
                        ll_combo_id.visibility = View.GONE
                        ll_main_course_id.visibility = View.GONE
                        ll_edit_item_qty_id.visibility = View.GONE
                        rv_modifier_list_id.visibility = View.GONE
                        tv_modifiers.visibility = View.GONE
                        ll_all_modifiers_id.visibility = View.GONE
                        ll_modifier_list_id.visibility = View.GONE
                        ll_gift_content_id.visibility = View.GONE
                        ll_menu_item_edit_id.visibility = View.GONE
                        val obj = JSONObject()
                        obj.put("discountId", sub_listArray[position].id)
                        obj.put("userId", mEmployeeId)
                        obj.put("restaurantId", restaurantId)
                        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                        BogoDiscounts(
                            jObject,
                            sub_listArray[position].minDiscountAmount,
                            sub_listArray[position].maxDiscountAmount
                        )
                        minDiscountAmount = sub_listArray[position].minDiscountAmount
                        maxDiscountAmount = sub_listArray[position].maxDiscountAmount
                    } else if (sub_listArray[position].type == "open" || sub_listArray[position].type == "openDiscount") {
                        openDiscountsDialog(sub_listArray[position], DISCOUNT_LEVEL_CHECK)
                    } else if (sub_listArray[position].type == "fixed" || sub_listArray[position].type == "fixedDiscount") {
                        checkLevelApplied = true
                        val discountArray = sub_listArray[position]
                        val discountsArray = ArrayList<GetAllDiscountsListResponse>()
                        discountsArray.add(discountArray)
                        checkLevelDiscountArray.clear()
                        checkLevelDiscountArray.add(discountArray)
                        handleDiscounts()
                        fullOrderSetUp()
                    }
                }
            }
            if (selectedPosition == position) {
                holder.cv_sub_list_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.graditent_bg
                )
            } else {
                holder.cv_sub_list_id.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.gray_background
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_sub_list_id = view.findViewById<CardView>(R.id.cv_sub_list_id)
        }
    }

    private fun openDiscountsDialog(
        discountObject: GetAllDiscountsListResponse,
        discountLevel: String
    ) {
        val open_discount_dialog = Dialog(this)
        open_discount_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        open_discount_dialog.setContentView(R.layout.open_discount_dialog)
        open_discount_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        open_discount_dialog.setCancelable(false)
        val tv_cancel = open_discount_dialog.findViewById(R.id.tv_cancel) as TextView
        val tv_apply = open_discount_dialog.findViewById(R.id.tv_apply) as TextView
        val tv_discount_type =
            open_discount_dialog.findViewById(R.id.tv_discount_type) as TextView
        val et_discount_amount =
            open_discount_dialog.findViewById(R.id.et_discount_amount) as EditText
        tv_cancel.setOnClickListener {
            open_discount_dialog.dismiss()
        }
        if (discountObject.discountValueType == "currency") {
            tv_discount_type.text = getString(R.string.discount_amount) + " ($currencyType)"
            et_discount_amount.filters = arrayOf<InputFilter>()     // removing input filter
        } else if (discountObject.discountValueType == "percentage") {
            tv_discount_type.text = getString(R.string.discount_percent)
            et_discount_amount.filters =
                arrayOf<InputFilter>(InputFilterMinMax(0, 100))    // setting max percentage to 100
        }
        tv_apply.setOnClickListener {
            if (et_discount_amount.text.toString() == "") {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Please enter the discount value.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val discountArray = GetAllDiscountsListResponse(
                    discountObject.allowOtherDiscounts,
                    discountObject.appliesTo,
                    discountObject.discountValueType,
                    discountObject.exceptDiscountItems,
                    discountObject.id,
                    discountObject.includeDiscountItems,
                    discountObject.maxDiscountAmount,
                    discountObject.minDiscountAmount,
                    discountObject.name,
                    et_discount_amount.text.toString(),
                    discountObject.status,
                    discountObject.type,
                    discountObject.uniqueNumber,
                    et_discount_amount.text.toString().toDouble()
                )
                val discountsArray = ArrayList<GetAllDiscountsListResponse>()
                discountsArray.add(discountArray)
                checkLevelDiscountArray.clear()
                checkLevelDiscountArray.add(discountArray)
                if (discountLevel == DISCOUNT_LEVEL_CHECK) {
                    handleDiscounts()
                } else if (discountLevel == DISCOUNT_LEVEL_ITEM) {
                    if (custom_array[item_index!!].discountApplicable) {
                        if ((custom_array[item_index!!].qty.toDouble() * custom_array[item_index!!].basePrice.toDouble()) >= discountsArray[0].minDiscountAmount) {
                            checkLevelApplied = false
                            val customData = OrdersCustomData(
                                custom_array[item_index!!].basePrice,
                                custom_array[item_index!!].discountApplicable,
                                custom_array[item_index!!].id,
                                custom_array[item_index!!].name,
                                custom_array[item_index!!].itemType,
                                custom_array[item_index!!].qty,
                                custom_array[item_index!!].total,
                                custom_array[item_index!!].discount,
                                custom_array[item_index!!].restaurantId,
                                custom_array[item_index!!].allmodifierArray,
                                custom_array[item_index!!].itemId,
                                custom_array[item_index!!].menuGroupId,
                                custom_array[item_index!!].menuId,
                                true,
                                DISCOUNT_LEVEL_ITEM,
                                discountsArray,
                                custom_array[item_index!!].itemStatus,
                                custom_array[item_index!!].isCombo,
                                custom_array[item_index!!].isBogo,
                                custom_array[item_index!!].comboOrBogoOfferId,
                                custom_array[item_index!!].comboOrBogoUniqueId,
                                custom_array[item_index!!].voidStatus,
                                custom_array[item_index!!].voidQuantity,
                                custom_array[item_index!!].aplicableTaxRates,
                                custom_array[item_index!!].taxAppliedList,
                                custom_array[item_index!!].diningOptionTaxException,
                                custom_array[item_index!!].diningTaxOption,
                                custom_array[item_index!!].taxIncludeOption
                            )
                            custom_array[item_index!!] = customData
                        } else {
                            Toast.makeText(
                                this@OrderDetailsActivity,
                                "Discount is applicable only if total is more than $currencyType${discountsArray[0].minDiscountAmount} !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            "Discount is not applicable for this item!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                fullOrderSetUp()
                open_discount_dialog.dismiss()
            }
        }
        open_discount_dialog.show()
    }

    private fun GetAllOpenItemsAPI(json_obj: JsonObject) {
        loading_dialog.show()
        val openItemArray = ArrayList<GetAllOpenItemList>()
        val name_onlyArray = ArrayList<String>()
        val dummyOpenItem = GetAllOpenItemList()
        dummyOpenItem.id = "0"
        dummyOpenItem.name = "Please select any item"
        dummyOpenItem.aplicableTaxRates = ArrayList()
        dummyOpenItem.diningOptionTaxException = true
        dummyOpenItem.diningTaxOption = true
        dummyOpenItem.taxIncludeOption = true
        openItemArray.add(dummyOpenItem)
        name_onlyArray.add("Please select any item")
        val apiInterface = ApiInterface.create()
        val call = apiInterface.allOpenItemsApi(json_obj)
        call.enqueue(object : Callback<GetAllOpenItemResponse> {
            override fun onFailure(call: Call<GetAllOpenItemResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetAllOpenItemResponse>,
                response: Response<GetAllOpenItemResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        for (k in 0 until response.body()!!.openItemsList!!.size) {
                            openItemArray.add(response.body()!!.openItemsList!![k])
                            name_onlyArray.add(response.body()!!.openItemsList!![k].name!!)
                        }
                        AddOpenItenDailog(openItemArray, name_onlyArray)
                    } else {
                        AddOpenItenDailog(openItemArray, name_onlyArray)
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun AddOpenItenDailog(
        openItemArray: ArrayList<GetAllOpenItemList>,
        openItemnameArray: ArrayList<String>
    ) {
        var open_item_id = ""
        var open_item_name = ""
        val itemType = "open"
        val modifiers = ArrayList<GetAllModifierListResponseList>()
        add_open_item_dialog = Dialog(this)
        add_open_item_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        add_open_item_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        add_open_item_dialog.setCancelable(false)
        add_open_item_dialog.setContentView(R.layout.open_item_dialog)
        val sp_list_item_id = add_open_item_dialog.findViewById(R.id.sp_list_item_id) as Spinner
        val et_item_price_id =
            add_open_item_dialog.findViewById(R.id.et_item_price_id) as EditText
        val tv_value_cancel_id =
            add_open_item_dialog.findViewById(R.id.tv_value_cancel_id) as TextView
        val tv_currency_amount =
            add_open_item_dialog.findViewById(R.id.tv_currency_amount) as TextView
        val tv_add_item_id = add_open_item_dialog.findViewById(R.id.tv_add_item_id) as TextView
        tv_currency_amount.text = currencyType
        tv_value_cancel_id.setOnClickListener {
            add_open_item_dialog.dismiss()
        }
        val adapter = ArrayAdapter<String>(
            this@OrderDetailsActivity,
            R.layout.spinner_open_list_item,
            openItemnameArray
        )
        sp_list_item_id.adapter = adapter
        sp_list_item_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                open_item_id = openItemArray[position].id.toString()
                open_item_name = openItemArray[position].name.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_add_item_id.setOnClickListener {
            when {
                open_item_id == "0" -> {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        open_item_name,
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_item_price_id.text.toString().trim() == "" -> {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Enter Amount to Proceed",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    if (checkLevelApplied) {
                        if ((1 * et_item_price_id.text.toString()
                                .toDouble()) >= checkLevelDiscountArray[0].minDiscountAmount
                        ) {
                            val orderCustomdata =
                                OrdersCustomData(
                                    et_item_price_id.text.toString(),
                                    discountApplicable,
                                    open_item_id,
                                    open_item_name,
                                    itemType,
                                    1.toString(),
                                    (1 * et_item_price_id.text.toString().toDouble()).toString(),
                                    "",
                                    restaurantId,
                                    modifiers,
                                    open_item_id,
                                    "",
                                    "",
                                    true,
                                    DISCOUNT_LEVEL_CHECK,
                                    checkLevelDiscountArray,
                                    0,
                                    isCombo = false,
                                    isBogo = false,
                                    comboOrBogoOfferId = "",
                                    comboOrBogoUniqueId = "",
                                    voidStatus = false,
                                    voidQuantity = 0,
                                    aplicableTaxRates = ArrayList(),
                                    taxAppliedList = ArrayList(),
                                    diningOptionTaxException = true,
                                    diningTaxOption = true,
                                    taxIncludeOption = true
                                )
                            custom_array.add(orderCustomdata)
                        } else {
                            val orderCustomdata =
                                OrdersCustomData(
                                    et_item_price_id.text.toString(),
                                    discountApplicable,
                                    open_item_id,
                                    open_item_name,
                                    itemType,
                                    1.toString(),
                                    (1 * et_item_price_id.text.toString().toDouble()).toString(),
                                    "",
                                    restaurantId,
                                    modifiers,
                                    open_item_id,
                                    "",
                                    "",
                                    false,
                                    "",
                                    ArrayList(),
                                    0,
                                    isCombo = false,
                                    isBogo = false,
                                    comboOrBogoOfferId = "",
                                    comboOrBogoUniqueId = "",
                                    voidStatus = false,
                                    voidQuantity = 0,
                                    aplicableTaxRates = ArrayList(),
                                    taxAppliedList = ArrayList(),
                                    diningOptionTaxException = true,
                                    diningTaxOption = true,
                                    taxIncludeOption = true
                                )
                            custom_array.add(orderCustomdata)
                        }
                    } else {
                        val orderZCustomdata =
                            OrdersCustomData(
                                et_item_price_id.text.toString(),
                                discountApplicable,
                                open_item_id,
                                open_item_name,
                                itemType,
                                1.toString(),
                                (1 * et_item_price_id.text.toString().toDouble()).toString(),
                                "",
                                restaurantId,
                                modifiers,
                                open_item_id,
                                "",
                                "",
                                false,
                                "",
                                ArrayList(),
                                0,
                                isCombo = false,
                                isBogo = false,
                                comboOrBogoOfferId = "",
                                comboOrBogoUniqueId = "",
                                voidStatus = false,
                                voidQuantity = 0,
                                aplicableTaxRates = ArrayList(),
                                taxAppliedList = ArrayList(),
                                diningOptionTaxException = true,
                                diningTaxOption = true,
                                taxIncludeOption = true
                            )
                        custom_array.add(orderZCustomdata)
                    }
                    handleDiscounts()
                    fullOrderSetUp()
                    add_open_item_dialog.dismiss()
                }
            }
        }
        add_open_item_dialog.show()
    }

    private fun fullOrderSetUp() {
        total_final_tax = 0.00
        price_total = 0.00
        var modifier_price_total = 0.00
        discount_amount = 0.00
        var totalTax = 0.00
        var checkLevel = 0 // is applicable only if discount level = "check"
        if (custom_array.size > 0) {
            for (i in 0 until custom_array.size) {
                if (isDeliverySelected || isTakeOutSelected) {
                    if (custom_array[i].diningTaxOption) {
                        val orderCustomData = OrdersCustomData(
                            custom_array[i].basePrice,
                            custom_array[i].discountApplicable,
                            custom_array[i].id,
                            custom_array[i].name,
                            custom_array[i].itemType,
                            custom_array[i].qty,
                            custom_array[i].total,
                            custom_array[i].discount,
                            custom_array[i].restaurantId,
                            custom_array[i].allmodifierArray,
                            custom_array[i].itemId,
                            custom_array[i].menuGroupId,
                            custom_array[i].menuId,
                            custom_array[i].discountApplied,
                            custom_array[i].discountAppliedType,
                            custom_array[i].discountsArray,
                            custom_array[i].itemStatus,
                            custom_array[i].isCombo,
                            custom_array[i].isBogo,
                            custom_array[i].comboOrBogoOfferId,
                            custom_array[i].comboOrBogoUniqueId,
                            custom_array[i].voidStatus,
                            custom_array[i].voidQuantity,
                            custom_array[i].aplicableTaxRates,
                            ArrayList(),
                            custom_array[i].diningOptionTaxException,
                            custom_array[i].diningTaxOption,
                            custom_array[i].taxIncludeOption
                        )
                        custom_array[i] = orderCustomData
                    }
                }
            }
        }
        for (newOrder in 0 until custom_array.size) {
            if (custom_array[newOrder].isCombo /*|| custom_array[newOrder].isBogo*/) {
                if (!custom_array[newOrder].discountApplied) {
                    price_total += (custom_array[newOrder].qty.toInt() * custom_array[newOrder].basePrice.toDouble())
                    for (m in 0 until custom_array[newOrder].allmodifierArray.size) {
                        modifier_price_total += (custom_array[newOrder].allmodifierArray[m].total)
                    }
                }
            } else {
                price_total += (custom_array[newOrder].qty.toInt() * custom_array[newOrder].basePrice.toDouble())
                for (m in 0 until custom_array[newOrder].allmodifierArray.size) {
                    modifier_price_total += (custom_array[newOrder].allmodifierArray[m].total)
                }
            }
            if (custom_array[newOrder].taxAppliedList.size > 0) {
                for (i in 0 until custom_array[newOrder].taxAppliedList.size) {
                    totalTax += custom_array[newOrder].taxAppliedList[i].taxAmount
                }
            }
        }
        for (i in 0 until custom_array.size) {
            if (custom_array[i].discountApplied && !custom_array[i].isCombo /*&& !custom_array[i].isBogo*/) {
                var discount = 0.00
                if (custom_array[i].discountAppliedType == DISCOUNT_LEVEL_CHECK) {
                    for (j in 0 until custom_array[i].discountsArray.size) {
                        if (custom_array[i].discountsArray[j].discountValueType == "percentage") {
                            discount += when {
                                custom_array[i].discountsArray[j].maxDiscountAmount == 0.00 -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble() * (custom_array[i].discountsArray[j].value / 100))
                                }
                                (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) >= custom_array[i].discountsArray[j].maxDiscountAmount -> {
                                    (custom_array[i].discountsArray[j].maxDiscountAmount) * (custom_array[i].discountsArray[j].value / 100)
                                }
                                else -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble() * (custom_array[i].discountsArray[j].value / 100))
                                }
                            }
                        } else if (custom_array[i].discountsArray[j].discountValueType == "currency") {
                            discount += when {
                                custom_array[i].discountsArray[j].maxDiscountAmount == 0.00 -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].value)
                                }
                                (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) >= custom_array[i].discountsArray[j].maxDiscountAmount -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].maxDiscountAmount)
                                }
                                else -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].value)
                                }
                            }
                        }
                        Log.d("discount...", discount.toString())
                        /*if (custom_array[i].discountsArray[j].maxDiscountAmount == 0.00) {
                            if (custom_array[i].discountsArray[j].discountValueType == "currency") {
                                discount += custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].value
                            } else if (custom_array[i].discountsArray[j].discountValueType == "percentage") {
                                discount += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) * (custom_array[i].discountsArray[j].value / 100)
                            }
                        } else {
                            if ((custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) >= custom_array[i].discountsArray[j].maxDiscountAmount) {
                                discount += (custom_array[i].discountsArray[j].maxDiscountAmount) * (custom_array[i].discountsArray[j].value / 100)
                            } else {
                                discount += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) * (custom_array[i].discountsArray[j].value / 100)
                            }
                        }*/
                    }
                    discount_amount += discount
                    checkLevel++
                } else if (custom_array[i].discountAppliedType == DISCOUNT_LEVEL_ITEM) {
                    for (j in 0 until custom_array[i].discountsArray.size) {
                        if (custom_array[i].discountsArray[j].discountValueType == "percentage") {
                            discount += when {
                                custom_array[i].discountsArray[j].maxDiscountAmount == 0.00 -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble() * (custom_array[i].discountsArray[j].value / 100))
                                }
                                (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) >= custom_array[i].discountsArray[j].maxDiscountAmount -> {
                                    (custom_array[i].discountsArray[j].maxDiscountAmount) * (custom_array[i].discountsArray[j].value / 100)
                                }
                                else -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble() * (custom_array[i].discountsArray[j].value / 100))
                                }
                            }
                        } else if (custom_array[i].discountsArray[j].discountValueType == "currency") {
                            discount += when {
                                custom_array[i].discountsArray[j].maxDiscountAmount == 0.00 -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].value)
                                }
                                (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) >= custom_array[i].discountsArray[j].maxDiscountAmount -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].maxDiscountAmount)
                                }
                                else -> {
                                    (custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].value)
                                }
                            }
                        }
                        /*if (custom_array[i].discountsArray[j].discountValueType == "currency") {
                            discount += custom_array[i].qty.toDouble() * custom_array[i].discountsArray[j].value
                        } else if (custom_array[i].discountsArray[j].discountValueType == "percentage") {
                            if (custom_array[i].isBogo) {
                                discount += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) * (custom_array[i].discountsArray[j].value / 100)
                            } else {
                                if (custom_array[i].discountsArray[j].maxDiscountAmount == 0.00) {
                                    discount += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) * (custom_array[i].discountsArray[j].value / 100)
                                } else {
                                    if ((custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) >= custom_array[i].discountsArray[j].maxDiscountAmount) {
                                        discount += (custom_array[i].discountsArray[j].maxDiscountAmount) * (custom_array[i].discountsArray[j].value / 100)
                                    } else {
                                        discount += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble()) * (custom_array[i].discountsArray[j].value / 100)
                                    }
                                }
                            }
                        }*/
                    }
                    discount_amount += discount
                }
            }
        }
        if (checkLevel == 0) {
            checkLevelApplied = checkLevelDiscountArray.size > 0
        }
        tv_total_discount_id.text = "%.2f".format(discount_amount)
        final_sub_total = (price_total + modifier_price_total)
        tv_sub_total_id.text = "%.2f".format(price_total + modifier_price_total)
        tv_final_sum_price.text =
            "%.2f".format(price_total + modifier_price_total + totalTax - discount_amount)
        tv_tax_amount_id.text = "%.2f".format(totalTax)
        val order_LayoutManager =
            LinearLayoutManager(this@OrderDetailsActivity, RecyclerView.VERTICAL, false)
        rv_order_item_id.layoutManager = order_LayoutManager
        orderAdapter = OrderAdapter(this, custom_array)
        rv_order_item_id.adapter = orderAdapter
        orderAdapter.notifyDataSetChanged()
        if (custom_array.size > 0) {
            rv_order_item_id.scrollToPosition(custom_array.size - 1)
            val gson = GsonBuilder().serializeSpecialFloatingPointValues().create()
            val orderDetails = gson.toJson(custom_array).toString()
            sessionManager.saveTempOrderList(orderDetails)
        } else {
            sessionManager.saveTempOrderList("")
        }
        GuestDisplayNotification()
    }

    private fun OrderGuestNotifications(json_obj: JsonObject) {
        val apiInterface = ApiInterface.create()
        val call = apiInterface.orderGuestNotificationApi(json_obj)
        call.enqueue(object : Callback<AddOrderResponse> {
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            "Guest Display " + response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (response.body()!!.responseStatus!! == 0) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            "Guest Display " + response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun TipAmountDailog() {
        tip_dialog = Dialog(this)
        tip_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        tip_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        tip_dialog.setCancelable(false)
        tip_dialog.setContentView(R.layout.tip_req_dailog)
        val tv_tip_cancel_button =
            tip_dialog.findViewById(R.id.tv_tip_cancel_button) as TextView
        val tv_tip_next_button = tip_dialog.findViewById(R.id.tv_tip_next_button) as TextView
        val tv_skip_pay_button = tip_dialog.findViewById(R.id.tv_skip_pay_button) as TextView
        val tv_amount_one_id = tip_dialog.findViewById(R.id.tv_amount_one_id) as TextView
        val tv_amount_two_id = tip_dialog.findViewById(R.id.tv_amount_two_id) as TextView
        val tv_amount_three_id = tip_dialog.findViewById(R.id.tv_amount_three_id) as TextView
        val tv_amount_four_id = tip_dialog.findViewById(R.id.tv_amount_four_id) as TextView
        val tv_amount_five_id = tip_dialog.findViewById(R.id.tv_amount_five_id) as TextView
        val tv_currency_custom = tip_dialog.findViewById(R.id.tv_currency_custom) as TextView
        val et_amount_custom = tip_dialog.findViewById(R.id.et_amount_custom) as EditText
        tv_currency_custom.text = currencyType
        tv_amount_one_id.text = "$currencyType 1"
        tv_amount_two_id.text = "$currencyType 2"
        tv_amount_three_id.text = "$currencyType 3"
        tv_amount_four_id.text = "$currencyType 4"
        tv_amount_five_id.text = "$currencyType 5"
        tv_amount_one_id.setOnClickListener {
            final_tip_amount = 1.00
            TipAmountNotifcation(final_tip_amount)
            tv_amount_one_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.graditent_bg
            )
            tv_amount_two_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_three_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_four_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_five_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
        }
        tv_amount_two_id.setOnClickListener {
            final_tip_amount = 2.00
            TipAmountNotifcation(final_tip_amount)
            tv_amount_two_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.graditent_bg
            )
            tv_amount_one_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_three_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_four_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_five_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
        }
        tv_amount_three_id.setOnClickListener {
            final_tip_amount = 3.00
            TipAmountNotifcation(final_tip_amount)
            tv_amount_three_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.graditent_bg
            )
            tv_amount_two_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_one_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_four_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_five_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
        }
        tv_amount_four_id.setOnClickListener {
            final_tip_amount = 4.00
            TipAmountNotifcation(final_tip_amount)
            tv_amount_four_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.graditent_bg
            )
            tv_amount_two_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_three_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_one_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_five_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
        }
        tv_amount_five_id.setOnClickListener {
            final_tip_amount = 5.00
            TipAmountNotifcation(final_tip_amount)
            tv_amount_five_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.graditent_bg
            )
            tv_amount_two_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_three_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_four_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
            tv_amount_one_id.background = ContextCompat.getDrawable(
                this,
                R.drawable.dark_gray_dispatch_background
            )
        }
        et_amount_custom.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                tv_amount_five_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.dark_gray_dispatch_background
                )
                tv_amount_two_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.dark_gray_dispatch_background
                )
                tv_amount_three_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.dark_gray_dispatch_background
                )
                tv_amount_four_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.dark_gray_dispatch_background
                )
                tv_amount_one_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.dark_gray_dispatch_background
                )
                final_tip_amount = if (s!!.isNotEmpty()) {
                    s.toString().toDouble()
                } else {
                    0.00
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })
        tv_tip_cancel_button.setOnClickListener {
            tip_dialog.dismiss()
        }
        tv_tip_next_button.setOnClickListener {
            TipAmountNotifcation(final_tip_amount)
            if (pay_type == "pay") {
                UpdatedOrder(1)
            } else if (pay_type == "prepaid")
                CreateAddOrder(2, "prepaid")
            else if (pay_type == "send_pay")
                CreateAddOrder(1, "send_pay")
            tip_dialog.dismiss()
        }
        tv_skip_pay_button.setOnClickListener {
            final_tip_amount = 0.00
            TipAmountNotifcation(final_tip_amount)
            if (pay_type == "pay")
                UpdatedOrder(1)
            else if (pay_type == "prepaid")
                CreateAddOrder(2, "prepaid")
            else if (pay_type == "send_pay")
                CreateAddOrder(1, "send_pay")
            tip_dialog.dismiss()
        }
        TipAmountNotifcation(0.00)
        tip_dialog.show()
    }

    private fun TipAmountNotifcation(tip_amount: Double) {
        val obj_add_card = JSONObject()
        obj_add_card.put("deviceToken", deviceToken)
        obj_add_card.put("tipAmount", "%.2f".format(tip_amount))
        val jObject_add_tip = JsonParser.parseString(obj_add_card.toString()).asJsonObject
        Log.e("jObject_tip_amount", jObject_add_tip.toString())
        val apiInterface = ApiInterface.create()
        val call = apiInterface.tipGuestNotificationApi(jObject_add_tip)
        call.enqueue(object : Callback<AddOrderResponse> {
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (response.body()!!.responseStatus!! == 0) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    /* search user */
    private fun searchUser() {
        var phoneNumber: String
        val customer_search_dialog = Dialog(this)
        customer_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.search_user_dialog, null)
        customer_search_dialog.setContentView(view)
        customer_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        customer_search_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        customer_search_dialog.setCanceledOnTouchOutside(false)
        val tv_search = view.findViewById(R.id.tv_search) as TextView
        val et_number = view.findViewById(R.id.et_number) as EditText
        rv_user_search = view.findViewById(R.id.rv_user_search)
        ll_user_empty = view.findViewById(R.id.ll_user_empty)
        val tv_add_new_customer = view.findViewById(R.id.tv_add_new_customer) as TextView
        tv_ok = view.findViewById(R.id.tv_ok)
        tv_ok.isEnabled = false
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_user_search.layoutManager = linearLayoutManager
        rv_user_search.hasFixedSize()
        tv_search.setOnClickListener {
            phoneNumber = et_number.text.toString().trim()
            if (phoneNumber == "") {
                Toast.makeText(this, "Enter phone number!", Toast.LENGTH_SHORT).show()
            } else {
                /*if (phoneNumber.length > MAX_CHAR) {
                    phoneNumber = phoneNumber.substring(0, MAX_CHAR)
                }*/
                val jObj = JSONObject()
                jObj.put("userId", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("phonenumber", phoneNumber)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                searchCustomerPhone(finalObject)
            }
        }
        tv_add_new_customer.setOnClickListener {
            addCustomer("search")
            if (!isDeliverySelected) {
                customer_search_dialog.dismiss()
            }
        }
        tv_cancel.setOnClickListener {
            isHavingCustomer = false
            customerId = ""
            if (isDeliverySelected && !isHavingDeliveryInfo) {
                customer_search_dialog.dismiss()
            } else if (isDeliverySelected && isHavingDeliveryDetails && !isHavingCustomer) {
                customer_search_dialog.dismiss()
            } else if (isCurbSideSelected && !isHavingCurbSideInfo) {
                customer_search_dialog.dismiss()
            } /*else if (isTakeOutSelected) {

            }*/ else {
                if (pay_type == "send") {
                    CreateAddOrder(1, "send")
                } else if (pay_type == "send_pay") {
                    TipAmountDailog()
                }
                customer_search_dialog.dismiss()
            }
        }
        tv_ok.setOnClickListener {
            isHavingCustomer = true
            tv_customer_name_id.text = "$customerFirstName $customerLastName"
            if (isDeliverySelected && !isHavingDeliveryInfo) {
                val jObj = JSONObject()
                jObj.put("employeeId", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("customerId", customerId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.d("finalObject", finalObject.toString())
                getCustomerAddress(finalObject)
            } else if (isCurbSideSelected && !isHavingCurbSideInfo) {
                addCurbsideDetails()
            } /*else if (isTakeOutSelected) {

            } */ else {
                if (pay_type == "send") {
                    CreateAddOrder(1, "send")
                } else if (pay_type == "send_pay") {
                    TipAmountDailog()
                }
                customer_search_dialog.dismiss()
            }
        }
        customer_search_dialog.show()
    }

    private fun addCustomer(type: String) {
        add_customer_dialog = Dialog(this)
        add_customer_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.add_user_dialog, null)
        add_customer_dialog.setContentView(view)
        add_customer_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        add_customer_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        add_customer_dialog.setCanceledOnTouchOutside(false)
        val tv_customer_details_header =
            view.findViewById(R.id.tv_customer_details_header) as TextView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val ll_countries = view.findViewById(R.id.ll_countries) as LinearLayout
        et_first_name = view.findViewById(R.id.et_first_name)
        et_last_name = view.findViewById(R.id.et_last_name)
        et_phone_number = view.findViewById(R.id.et_phone_number)
        et_email = view.findViewById(R.id.et_email)
        sp_country = view.findViewById(R.id.sp_country)
        sp_state = view.findViewById(R.id.sp_state)
        sp_city = view.findViewById(R.id.sp_city)
        statesList.add(dummyState)
        citiesList.add(dummyCity)
        /*val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId)
        jObj.put("restaurantId", restaurantId)
        val jsonObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getCountries(jsonObject)*/
        statesListAdapter = StatesListAdapter(
            this@OrderDetailsActivity,
            statesList
        )
        sp_state.adapter = statesListAdapter
        citiesListAdapter = CitiesListAdapter(
            this@OrderDetailsActivity,
            citiesList
        )
        sp_city.adapter = citiesListAdapter
        if (type == "search") {
            tv_customer_details_header.text = getString(R.string.add_new_customer)
            ll_countries.visibility = View.GONE
        } else if (type == "sell") {
            tv_customer_details_header.text = getString(R.string.customer_details)
            ll_countries.visibility = View.GONE
        }
        sp_country.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                countryId = countriesListAdapter.getSelectedItem(position)._id!!
                Log.d("add customer countryId", countryId)
                if (countryId == "dummy") {
                    // do nothing
                } else {
                    // resetting state & city spinners
                    statesListAdapter = StatesListAdapter(
                        this@OrderDetailsActivity,
                        statesList
                    )
                    sp_state.adapter = statesListAdapter
                    citiesListAdapter = CitiesListAdapter(
                        this@OrderDetailsActivity,
                        citiesList
                    )
                    sp_city.adapter = citiesListAdapter
                    val jsonObj = JSONObject()
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("countryId", countryId)
                    val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    getStates(finalObject, sp_state)
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        sp_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                stateId = statesListAdapter.getSelectedItem(position)._id!!
                stateName = statesListAdapter.getSelectedItem(position).name.toString()
                Log.d("add customer stateId", stateId)
                if (stateId == "dummy") {
                    // do nothing
                } else {
                    // resetting city spinner
                    citiesListAdapter = CitiesListAdapter(
                        this@OrderDetailsActivity,
                        citiesList
                    )
                    sp_city.adapter = citiesListAdapter
                    val jsonObj = JSONObject()
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("stateId", stateId)
                    val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    getCities(finalObject, sp_city)
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        sp_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                cityId = citiesListAdapter.getSelectedItem(position)._id!!
                cityName = citiesListAdapter.getSelectedItem(position).name.toString()
                Log.d("add customer cityId", cityId)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_cancel.setOnClickListener {
            add_customer_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            if (type == "search") {
                if (isValid()) {
                    val obj = JSONObject()
                    obj.put("firstName", et_first_name.text.toString())
                    obj.put("lastName", et_last_name.text.toString())
                    obj.put("phoneNumber", et_phone_number.text.toString())
                    obj.put("email", et_email.text.toString())
                    obj.put("restaurantId", restaurantId)
                    obj.put("createdBy", mEmployeeId)
                    obj.put("status", 1)
                    /*obj.put("countryId", countryId)
                    obj.put("stateId", stateId)
                    obj.put("cityId", cityId)*/
                    val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                    addCustomerApi(jObject, add_customer_dialog)
                }
            } else if (type == "sell") {
                if (isValid()) {
                    giftCardPaymentDialog()
                }
            }
        }
        add_customer_dialog.show()
    }

    private fun giftCardPaymentDialog() {
        var pay_type = 0
        gift_card_payment_dialog = Dialog(this)
        gift_card_payment_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.gift_cards_payment_dialog, null)
        gift_card_payment_dialog.setContentView(view)
        gift_card_payment_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        gift_card_payment_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        gift_card_payment_dialog.setCanceledOnTouchOutside(false)
        val tv_pay_cash = view.findViewById(R.id.tv_pay_cash) as TextView
        val tv_pay_card = view.findViewById(R.id.tv_pay_card) as TextView
        val ll_cash = view.findViewById(R.id.ll_cash) as LinearLayout
        val ll_card = view.findViewById(R.id.ll_card) as LinearLayout
        val tv_void = view.findViewById(R.id.tv_void) as TextView
        val tv_pay = view.findViewById(R.id.tv_pay) as TextView
        val tv_key_in = view.findViewById(R.id.tv_key_in) as TextView
        val et_amount_tendered = view.findViewById(R.id.et_amount_tendered) as EditText
        val tv_change_due = view.findViewById(R.id.tv_change_due) as TextView
        tv_change_due.text = currencyType + getString(R.string.default_value_0)
        tv_key_in.visibility = View.GONE
        et_amount_tendered.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    if (s != "") {
                        if (giftCardAmount < et_amount_tendered.text.toString().toDouble()) {
                            tv_change_due.text = currencyType + "%.2f".format(
                                et_amount_tendered.text.toString().toDouble() - giftCardAmount
                            )
                        } else {
                            tv_change_due.text = currencyType + getString(R.string.default_value_0)
                        }
                    } else {
                        tv_change_due.text = currencyType + getString(R.string.default_value_0)
                    }
                } else {
                    tv_change_due.text = currencyType + getString(R.string.default_value_0)
                }
            }
        })
        tv_void.setOnClickListener {
            gift_card_payment_dialog.dismiss()
        }
        tv_pay_cash.setOnClickListener {
            tv_pay_cash.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_pay_card.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_cash.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_pay_card.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            ll_cash.visibility = View.VISIBLE
            tv_pay.visibility = View.VISIBLE
            ll_card.visibility = View.GONE
            tv_key_in.visibility = View.GONE
            pay_type = 0
        }
        tv_pay_card.setOnClickListener {
            tv_pay_cash.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_card.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_pay_cash.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_card.setTextColor(ContextCompat.getColor(this, R.color.white))
            ll_cash.visibility = View.GONE
            tv_pay.visibility = View.GONE
            ll_card.visibility = View.VISIBLE
            tv_key_in.visibility = View.VISIBLE
            pay_type = 1
        }
        tv_pay.setOnClickListener {
            if (et_amount_tendered.text.toString().trim() == "") {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val obj_add_card = JSONObject()
                obj_add_card.put("paymentType", pay_type)
                obj_add_card.put("amount", giftCardAmount)
                obj_add_card.put("cardType", giftCardType)
                obj_add_card.put("email", et_email.text.toString())
                obj_add_card.put("custFirstName", et_first_name.text.toString())
                obj_add_card.put("custLastName", et_last_name.text.toString())
                obj_add_card.put("custPhone", et_phone_number.text.toString())
                obj_add_card.put("createdBy", mEmployeeId)
                obj_add_card.put("restaurantId", restaurantId)
                val jObject_add_card = JsonParser.parseString(obj_add_card.toString()).asJsonObject
                Log.e("jObject_add_card", jObject_add_card.toString())
                AddGiftCard(jObject_add_card)
            }
        }
        tv_key_in.setOnClickListener {
            val obj_add_card = JSONObject()
            obj_add_card.put("paymentType", pay_type)
            obj_add_card.put("amount", giftCardAmount)
            obj_add_card.put("cardType", giftCardType)
            obj_add_card.put("email", et_email.text.toString())
            obj_add_card.put("custFirstName", et_first_name.text.toString())
            obj_add_card.put("custLastName", et_last_name.text.toString())
            obj_add_card.put("custPhone", et_phone_number.text.toString())
            obj_add_card.put("createdBy", mEmployeeId)
            obj_add_card.put("restaurantId", restaurantId)
            // cardpointe
            cardDetailsDialog(obj_add_card, "sell_gift_card")
        }
        gift_card_payment_dialog.show()
    }

    private fun giftCardWithBankPaymentDialog(cardNumber: String, cardAmount: Double) {
        var pay_type = 0
        val bankAccountId = ""
        gift_card_with_bank_payment_dialog = Dialog(this)
        gift_card_with_bank_payment_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.gift_cards_payment_with_bank_dialog, null)
        gift_card_with_bank_payment_dialog.setContentView(view)
        gift_card_with_bank_payment_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        gift_card_with_bank_payment_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        gift_card_with_bank_payment_dialog.setCanceledOnTouchOutside(false)
        val tv_gift_amount_id = view.findViewById(R.id.tv_gift_amount_id) as TextView
        val tv_gift_card_number_id = view.findViewById(R.id.tv_gift_card_number_id) as TextView
        val tv_pay_cash = view.findViewById(R.id.tv_pay_cash) as TextView
        val tv_pay_card = view.findViewById(R.id.tv_pay_card) as TextView
        val tv_pay_bank = view.findViewById(R.id.tv_pay_bank) as TextView
        val ll_cash = view.findViewById(R.id.ll_cash) as LinearLayout
        val ll_card = view.findViewById(R.id.ll_card) as LinearLayout
        val ll_bank = view.findViewById(R.id.ll_bank) as LinearLayout
        val tv_void = view.findViewById(R.id.tv_void) as TextView
        val tv_pay = view.findViewById(R.id.tv_pay) as TextView
        val tv_key_in = view.findViewById(R.id.tv_key_in) as TextView
        val et_amount_tendered = view.findViewById(R.id.et_amount_tendered) as EditText
        val tv_change_due = view.findViewById(R.id.tv_change_due) as TextView
        val tv_currency_change_due = view.findViewById(R.id.tv_currency_change_due) as TextView
        val tv_currency_card_balance = view.findViewById(R.id.tv_currency_card_balance) as TextView
        tv_currency_change_due.text = currencyType
        tv_currency_card_balance.text = currencyType
        tv_change_due.text = "0.00"
        tv_gift_amount_id.text = "%.2f".format(cardAmount)
        tv_gift_card_number_id.text = cardNumber
        tv_key_in.visibility = View.GONE
        et_amount_tendered.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    if (s != "") {
                        if (cardAmount < et_amount_tendered.text.toString().toDouble()) {
                            tv_change_due.text = "%.2f".format(
                                et_amount_tendered.text.toString().toDouble() - cardAmount
                            )
                        } else {
                            tv_change_due.text = "0.00"
                        }
                    } else {
                        tv_change_due.text = "0.00"
                    }
                } else {
                    tv_change_due.text = "0.00"
                }
            }
        })
        tv_void.setOnClickListener {
            gift_card_with_bank_payment_dialog.dismiss()
        }
        tv_pay_cash.setOnClickListener {
            tv_pay_cash.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_pay_card.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_bank.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_cash.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_pay_card.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_bank.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            ll_cash.visibility = View.VISIBLE
            tv_pay.visibility = View.VISIBLE
            ll_card.visibility = View.GONE
            tv_key_in.visibility = View.GONE
            ll_bank.visibility = View.GONE
            pay_type = 0
        }
        tv_pay_card.setOnClickListener {
            tv_pay_cash.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_card.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_pay_bank.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_cash.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_card.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_pay_bank.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            ll_cash.visibility = View.GONE
            tv_pay.visibility = View.GONE
            ll_card.visibility = View.VISIBLE
            tv_key_in.visibility = View.VISIBLE
            ll_bank.visibility = View.GONE
            pay_type = 1
        }
        tv_pay_bank.setOnClickListener {
            tv_pay_cash.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_card.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_bank.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_pay_cash.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_card.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_bank.setTextColor(ContextCompat.getColor(this, R.color.white))
            ll_cash.visibility = View.GONE
            tv_pay.visibility = View.VISIBLE
            ll_card.visibility = View.GONE
            tv_key_in.visibility = View.GONE
            ll_bank.visibility = View.VISIBLE
            pay_type = 2
        }
        tv_pay.setOnClickListener {
            if (et_amount_tendered.text.toString().trim() == "") {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                /*Add Card API Call*/
                if (pay_type == 0) {
                    val obj_add_card = JSONObject()
                    obj_add_card.put("giftCardNo", cardNumber)
                    obj_add_card.put("paymentType", pay_type)
                    obj_add_card.put("amount", cardAmount)
                    obj_add_card.put("createdBy", mEmployeeId)
                    obj_add_card.put("restaurantId", restaurantId)
                    val jObject_add_card =
                        JsonParser.parseString(obj_add_card.toString()).asJsonObject
                    Log.e("jObject_add_value", jObject_add_card.toString())
                    AddValueGiftCard(jObject_add_card)
                } else if (pay_type == 2) {
                    val obj_add_card = JSONObject()
                    obj_add_card.put("giftCardNo", cardNumber)
                    obj_add_card.put("paymentType", pay_type)
                    obj_add_card.put("amount", cardAmount)
                    obj_add_card.put("bankAccountId", bankAccountId)
                    obj_add_card.put("createdBy", mEmployeeId)
                    obj_add_card.put("restaurantId", restaurantId)
                    val jObject_add_card =JsonParser.parseString(obj_add_card.toString()).asJsonObject
                    Log.e("jObject_add_value", jObject_add_card.toString())
                    AddValueGiftCard(jObject_add_card)
                }
            }
        }
        tv_key_in.setOnClickListener {
            /*Add Card API Call*/
            //cardpointe
            val obj_add_card = JSONObject()
            obj_add_card.put("giftCardNo", cardNumber)
            obj_add_card.put("paymentType", pay_type)
            obj_add_card.put("amount", cardAmount)
            obj_add_card.put("createdBy", mEmployeeId)
            obj_add_card.put("restaurantId", restaurantId)
            val jObject_add_card = JsonParser.parseString(obj_add_card.toString()).asJsonObject
            Log.e("jObject_add_value", jObject_add_card.toString())
            cardDetailsDialog(obj_add_card,"add_gift_card_balance")
        }
        gift_card_with_bank_payment_dialog.show()
    }

    private fun cardDetailsDialog(jObj:JSONObject,type:String) {
        val card_details_dialog = Dialog(this)
        card_details_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.card_details_dialog, null)
        card_details_dialog.setContentView(view)
        card_details_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        card_details_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val et_credit_card_number =
            view.findViewById(R.id.et_credit_card_number) as CCConsumerCreditCardNumberEditText
        val et_credit_card_expiration_date =
            view.findViewById(R.id.et_credit_card_expiration_date) as CCConsumerExpirationDateEditText
        val et_credit_card_cvv = view.findViewById(R.id.et_credit_card_cvv) as CCConsumerCvvEditText
        card_details_dialog.setCanceledOnTouchOutside(false)
        val mCCConsumerCardInfo = CCConsumerCardInfo()
//        et_credit_card_number.ccConsumerMaskFormat = CCConsumerMaskFormat.LAST_FOUR
//        et_credit_card_number.ccConsumerMaskFormat = CCConsumerMaskFormat.FIRST_LAST_FOUR
        et_credit_card_number.ccConsumerMaskFormat = CCConsumerMaskFormat.CARD_MASK_LAST_FOUR
        et_credit_card_number.setCreditCardTextChangeListener {
            if (!et_credit_card_number.isCardNumberValid && et_credit_card_number.text!!.isNotEmpty()) {
                et_credit_card_number.error = getString(R.string.card_not_valid)
            } else {
                et_credit_card_number.error = null
            }
        }
        et_credit_card_expiration_date.setExpirationDateTextChangeListener {
            if (!et_credit_card_expiration_date.isExpirationDateValid && et_credit_card_expiration_date.text!!.isNotEmpty()
            ) {
                et_credit_card_expiration_date.error = getString(R.string.date_not_valid)
            } else {
                et_credit_card_expiration_date.error = null
            }
        }
        et_credit_card_cvv.setCvvTextChangeListener {
            if (!et_credit_card_cvv.isCvvCodeValid && et_credit_card_cvv.text!!.isNotEmpty()) {
                et_credit_card_cvv.error = getString(R.string.cvv_not_valid)
            } else {
                et_credit_card_cvv.error = null
            }
        }
        tv_cancel.setOnClickListener {
            card_details_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_credit_card_number.setCardNumberOnCardInfo(mCCConsumerCardInfo)
            et_credit_card_expiration_date.setExpirationDateOnCardInfo(mCCConsumerCardInfo)
            et_credit_card_cvv.setCvvCodeOnCardInfo(mCCConsumerCardInfo)
            if (!mCCConsumerCardInfo.isCardValid) {
                Toast.makeText(this, getString(R.string.card_invalid), Toast.LENGTH_SHORT).show()
            } else {
                loading_dialog.show()
                CCConsumer.getInstance().api.setEndPoint(RestaurantId.CARD_POINTE_END_POINT)
                CCConsumer.getInstance().api.generateAccountForCard(mCCConsumerCardInfo,
                    object : CCConsumerTokenCallback {
                        override fun onCCConsumerTokenResponseError(error: CCConsumerError) {
                            if (loading_dialog.isShowing)
                                loading_dialog.dismiss()
                            Toast.makeText(
                                this@OrderDetailsActivity,
                                error.responseMessage,
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onCCConsumerTokenResponse(account: CCConsumerAccount) {
                            val accountResponse = account
                            Log.d("accountResponse", "${accountResponse.token}=====${mCCConsumerCardInfo.cardNumber}=====${mCCConsumerCardInfo.expirationDate}=====${mCCConsumerCardInfo.expirationDateWithoutSeparator}")
                            Toast.makeText(
                                this@OrderDetailsActivity,
                                accountResponse.token,
                                Toast.LENGTH_SHORT
                            ).show()
                            jObj.put("cardToken", accountResponse.token)
                            jObj.put("expiry", mCCConsumerCardInfo.expirationDateWithoutSeparator)
                            jObj.put("sourceLast4", mCCConsumerCardInfo.cardNumber)
                            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                            Log.d("finalObject",finalObject.toString())
                            card_details_dialog.dismiss()
                            if (type == "sell_gift_card") {
                                AddGiftCard(finalObject)
                            } else if (type == "add_gift_card_balance") {
                                AddValueGiftCard(finalObject)
                            }
                        }
                    })
            }
        }
        card_details_dialog.show()
    }

    private fun editGuests() {
        var guests :Int
        if (noOfGuests == "" || noOfGuests == "null") {
            guests = 0
        } else {
            guests = noOfGuests.toInt()
        }
        val edit_guests_dialog = Dialog(this)
        edit_guests_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.edit_guests_dialog, null)
        edit_guests_dialog.setContentView(view)
        edit_guests_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        edit_guests_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        edit_guests_dialog.setCanceledOnTouchOutside(false)
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_done = view.findViewById(R.id.tv_done) as TextView
        val cv_combo_plus_id = view.findViewById(R.id.cv_combo_plus_id) as CardView
        val cv_combo_minus_id = view.findViewById(R.id.cv_combo_minus_id) as CardView
        val tv_set_combo_qty_id = view.findViewById(R.id.tv_set_combo_qty_id) as TextView
        tv_set_combo_qty_id.text = guests.toString()
        edit_guests_dialog.show()
        tv_cancel.setOnClickListener {
            edit_guests_dialog.dismiss()
        }
        cv_combo_plus_id.setOnClickListener {
            guests++
            tv_set_combo_qty_id.text = guests.toString()
        }
        cv_combo_minus_id.setOnClickListener {
            if (guests == 0) {
                // minimum quantity
            } else {
                guests--
            }
            tv_set_combo_qty_id.text = guests.toString()
        }
        tv_done.setOnClickListener {
            noOfGuests = guests.toString()
            tv_edit_guests.text = noOfGuests + " " + getString(R.string.guests)
            edit_guests_dialog.dismiss()
        }
    }

    private fun editTable() {
        var table :Int
        if (tableNumber == "" || tableNumber == "null") {
            table = 0
        } else {
            table = tableNumber.toInt()
        }
        val edit_table_dialog = Dialog(this)
        edit_table_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.edit_table_dialog, null)
        edit_table_dialog.setContentView(view)
        edit_table_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        edit_table_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        edit_table_dialog.setCanceledOnTouchOutside(false)
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_done = view.findViewById(R.id.tv_done) as TextView
        val et_table_number = view.findViewById(R.id.et_table_number) as EditText
        et_table_number.setText(table.toString())
        edit_table_dialog.show()
        tv_cancel.setOnClickListener {
            edit_table_dialog.dismiss()
        }
        et_table_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.isNotEmpty()) {
                    table = s.toString().toInt()
                } else {
                    if (s.toString() == "") {
                        table = tableNumber.toInt()
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        tv_done.setOnClickListener {
            tableNumber = table.toString()
            tv_table_no.text = "Table No: $tableNumber"
            tv_check_table_dinein_option.text = "#$checkNumber, Table $tableNumber, $dinningName"
            edit_table_dialog.dismiss()
        }
    }

    private fun isValid(): Boolean {
        when {
            et_first_name.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "First name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_first_name.requestFocus()
            }
            et_last_name.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Last name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_last_name.requestFocus()
            }
            et_phone_number.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Phone number should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_phone_number.requestFocus()
            }
            et_email.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "E-mail should not be empty!", Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            !isValidEmail(et_email.text.toString()) -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "Invalid E-mail address",
                    Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun isAddValid(): Boolean {
        when {
            et_first_name.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "First name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_first_name.requestFocus()
            }
            et_last_name.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Last name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_last_name.requestFocus()
            }
            et_phone_number.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Phone number should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_phone_number.requestFocus()
            }
            et_email.text.toString() == "" -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "E-mail should not be empty!", Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            !isValidEmail(et_email.text.toString()) -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "Invalid E-mail address",
                    Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            countryId == "" || countryId == "dummy" -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "Please select valid country!",
                    Toast.LENGTH_SHORT
                ).show()
            }
            stateId == "" || stateId == "dummy" -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "Please select valid state!",
                    Toast.LENGTH_SHORT
                ).show()
            }
            cityId == "" || cityId == "dummy" -> {
                Toast.makeText(
                    this@OrderDetailsActivity, "Please select valid city!",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun searchCustomerPhone(finalObject: JsonObject) {
        loading_dialog.show()
        mSelectedIndex = -1
        val api = ApiInterface.create()
        val call = api.searchCustomerPhoneApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val customersList = respBody.customer!!
                        if (customersList.size > 0) {
                            val adapter =
                                CustomersListAdapter(this@OrderDetailsActivity, customersList)
                            rv_user_search.adapter = adapter
                            adapter.notifyDataSetChanged()
                            ll_user_empty.visibility = View.GONE
                            rv_user_search.visibility = View.VISIBLE
                        } else {
                            ll_user_empty.visibility = View.VISIBLE
                            rv_user_search.visibility = View.GONE
                        }
                    } else {
                        ll_user_empty.visibility = View.VISIBLE
                        rv_user_search.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun addCustomerApi(json_obj: JsonObject, dialog: Dialog) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.addCustomerApi(json_obj)
        call.enqueue(object : Callback<AddNewCustomerResponse> {
            override fun onFailure(call: Call<AddNewCustomerResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddNewCustomerResponse>,
                response: Response<AddNewCustomerResponse>?
            ) {
                try {
                    val resp = response!!.body()
                    if (resp!!.responseStatus == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            "Customer created successfully.",
                            Toast.LENGTH_SHORT
                        ).show()
                        if (isDeliverySelected) {
                            dialog.dismiss()
                        } else {
                            /* result is customer Id */
                            customerId = resp.result.toString()
                            customerFirstName = et_first_name.text.toString().trim()
                            customerLastName = et_last_name.text.toString().trim()
                            customerEmail = et_email.text.toString().trim()
                            customerPhone = et_phone_number.text.toString().trim()
                            dialog.dismiss()
                            isHavingCustomer = true
                            if (pay_type == "send") {
                                CreateAddOrder(1, "send")
                            } else if (pay_type == "send_pay") {
                                TipAmountDailog()
                            }
                        }
                    } else {
                        customerId = ""
                        customerFirstName = ""
                        customerLastName = ""
                        customerEmail = ""
                        customerPhone = ""
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        isHavingCustomer = false
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCountries(final_object: JsonObject, spinner: Spinner) {
        loading_dialog.show()
        countriesList.clear()
        citiesList.clear()
        statesList.clear()
        countriesList.add(dummyCountry)
        statesList.add(dummyState)
        citiesList.add(dummyCity)
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllCountriesApi(final_object)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("getAllCountriesApi", "Exception  $t")
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        if (resp.contries!!.size > 0) {
                            countriesList.addAll(resp.contries)
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    countriesListAdapter = CountriesListAdapter(
                        this@OrderDetailsActivity,
                        countriesList
                    )
                    spinner.adapter = countriesListAdapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getStates(final_object: JsonObject, spinner: Spinner) {
        loading_dialog.show()
        statesList.clear()
        citiesList.clear()
        statesList.add(dummyState)
        citiesList.add(dummyCity)
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllStatesApi(final_object)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("getAllStatesApi", "Exception  $t")
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        if (resp.states!!.size > 0) {
                            statesList.addAll(resp.states)
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    statesListAdapter = StatesListAdapter(
                        this@OrderDetailsActivity,
                        statesList
                    )
                    spinner.adapter = statesListAdapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCities(final_object: JsonObject, spinner: Spinner) {
        loading_dialog.show()
        citiesList.clear()
        citiesList.add(dummyCity)
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllCitiesApi(final_object)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("getAllCitiesApi", "Exception  $t")
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        if (resp.cities!!.size > 0) {
                            citiesList.addAll(resp.cities)
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    citiesListAdapter = CitiesListAdapter(
                        this@OrderDetailsActivity,
                        citiesList
                    )
                    spinner.adapter = citiesListAdapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class CustomersListAdapter(context: Context, list: ArrayList<CustomerDataResponse>) :
        RecyclerView.Adapter<CustomersListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<CustomerDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.customer_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): CustomerDataResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            holder.tv_name.text =
                "${mList!![position].firstName.toString()} ${mList!![position].lastName.toString()}"
            holder.tv_email.text = mList!![position].email.toString()
            holder.tv_number.text = mList!![position].phoneNumber.toString()
            holder.cv_item_header.setOnClickListener {
                if (mSelectedIndex == -1 || mSelectedIndex != position) {
                    mSelectedIndex = position
                    customerId = mList!![position].id.toString()
                    customerFirstName = mList!![position].firstName.toString()
                    customerLastName = mList!![position].lastName.toString()
                    customerEmail = mList!![position].email.toString()
                    customerPhone = mList!![position].phoneNumber.toString()
                    tv_ok.isEnabled = true
                } else if (mSelectedIndex == position) {
                    mSelectedIndex = -1
                    customerId = ""
                    customerFirstName = ""
                    customerLastName = ""
                    customerEmail = ""
                    customerPhone = ""
                    tv_ok.isEnabled = false
                }
                if (mSelectedIndex == position) {
                    holder.cv_item_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.cyan
                        )
                    )
                    holder.tv_name.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_email.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                } else {
                    holder.cv_item_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_name.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.edit_text_hint
                        )
                    )
                    holder.tv_email.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.button_black
                        )
                    )
                    holder.tv_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.button_black
                        )
                    )
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_name: TextView = view.findViewById(R.id.tv_name)
            var tv_email: TextView = view.findViewById(R.id.tv_email)
            var tv_number: TextView = view.findViewById(R.id.tv_number)
            var cv_item_header: CardView = view.findViewById(R.id.cv_item_header)
        }
    }

    //discount_detail_view
    private fun ComboDiscounts(json_obj: JsonObject) {
        loading_dialog.show()
        maincomboArrayList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getComboBOGODiscountsApi(json_obj)
        call.enqueue(object : Callback<AddComboBogoDiscountResponse> {
            override fun onFailure(call: Call<AddComboBogoDiscountResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddComboBogoDiscountResponse>,
                response: Response<AddComboBogoDiscountResponse>?
            ) {
                try {
                    val resp = response!!.body()
                    if (resp!!.responseStatus == 1) {
                        comboPrice = resp.discountValue
                        for (k in resp.combo.indices) {
                            comboArrayList = ArrayList()
                            for (element in resp.combo[k]) {
                                comboArrayList.add(element)
                            }
                            if (comboArrayList.isNotEmpty()) {
                                maincomboArrayList.add(comboArrayList)
                            }
                        }
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val drinks_LayoutManager =
                        LinearLayoutManager(
                            this@OrderDetailsActivity,
                            RecyclerView.VERTICAL,
                            false
                        )
                    rv_combo_list.layoutManager = drinks_LayoutManager
                    val adapter_combodiscounts =
                        ComboDiscountsAdapter(
                            this@OrderDetailsActivity,
                            maincomboArrayList
                        )
                    rv_combo_list.adapter = adapter_combodiscounts
                    adapter_combodiscounts.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class ComboDiscountsAdapter(
        context: Context,
        val sub_listArray: List<List<Combo>>
    ) :
        RecyclerView.Adapter<ComboDiscountsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        lateinit var combo_sub_array: ArrayList<Combo>
        var selectedPosition = -1
        private var price_provider = ""

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.combo_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return sub_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_combo_name_id.text = "Combo " + (position + 1)
            Log.e("discount_position", position.toString())
            combo_sub_array = ArrayList()
            combo_sub_array.addAll(sub_listArray[position])
            holder.tv_price_id.text = "$currencyType${"%.2f".format(comboPrice)}"
            val drinks_LayoutManager =
                LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
            holder.rv_get_combo_list.layoutManager = drinks_LayoutManager
            val adapter_combodiscounts =
                ComboListAdapter(this@OrderDetailsActivity, combo_sub_array)
            holder.rv_get_combo_list.adapter = adapter_combodiscounts
            holder.rv_get_combo_list.hasFixedSize()
            adapter_combodiscounts.notifyDataSetChanged()
            holder.ll_combo_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                comboBogoUniqueId = generateRandomId("combo")
                comboBogoId = sub_listArray[position][0].comboId
                comboItemTypeList = ArrayList()
                for (m in sub_listArray[position].indices) {
                    var comboBogoItemType = ComboBogoItemType("", "", "", 0, ArrayList())
                    if (sub_listArray[position][m].type == "Menu Group") {
                        comboBogoItemType = ComboBogoItemType(
                            sub_listArray[position][m].id,
                            sub_listArray[position][m].name,
                            sub_listArray[position][m].type,
                            sub_listArray[position][m].quantity,
                            ArrayList()
                        )
                    } else if (sub_listArray[position][m].type == "Menu Item") {
                        val comboBogoCustomList: ArrayList<ComboBogoCustomPojo> = ArrayList()
                        val timePriceList =
                            if (sub_listArray[position][m].timePriceList.toString() == "null") {
                                ArrayList()
                            } else {
                                sub_listArray[position][m].timePriceList!!
                            }
                        val sizeList =
                            if (sub_listArray[position][m].sizeList.toString() == "null") {
                                ArrayList()
                            } else {
                                sub_listArray[position][m].sizeList!!
                            }
                        val menuPriceList =
                            if (sub_listArray[position][m].menuPriceList.toString() == "null") {
                                ArrayList()
                            } else {
                                sub_listArray[position][m].menuPriceList!!
                            }
                        val specialRequestList =
                            if (sub_listArray[position][m].specialRequestList.toString() == "null") {
                                ArrayList()
                            } else {
                                sub_listArray[position][m].specialRequestList!!
                            }
//                        don't apply tax
                        val taxesList: ArrayList<TaxRatesDetailsResponse> =
//                            if (sub_listArray[position][m].taxesList.toString() == "null") {
                            ArrayList()
//                            } else {
//                                sub_listArray[position][m].taxesList!!
//                            }
                        val comboBogoCustomPojo = ComboBogoCustomPojo(
                            sub_listArray[position][m].comboId,
                            "combo",
                            sub_listArray[position][m].basePrice,
                            sub_listArray[position][m].discountAmount,
                            sub_listArray[position][m].discountId,
                            sub_listArray[position][m].discountName,
                            sub_listArray[position][m].discountType,
                            sub_listArray[position][m].discountValue,
                            sub_listArray[position][m].id,
                            sub_listArray[position][m].isBogo,
                            sub_listArray[position][m].isCombo,
                            sub_listArray[position][m].discountApplicable,
                            sub_listArray[position][m].itemId,
                            sub_listArray[position][m].itemName,
                            sub_listArray[position][m].itemType,
                            sub_listArray[position][m].menuGroupId,
                            sub_listArray[position][m].menuId,
                            sub_listArray[position][m].modifiersList,
                            sub_listArray[position][m].name,
                            sub_listArray[position][m].pricingStrategy,
                            sub_listArray[position][m].quantity,
                            specialRequestList,
                            timePriceList,
                            sizeList,
                            menuPriceList,
                            sub_listArray[position][m].totalPrice,
                            sub_listArray[position][m].type,
                            sub_listArray[position][m].uniqueNumber,
                            sub_listArray[position][m].unitPrice,
                            taxesList,
                            sub_listArray[position][m].diningOptionTaxException,
                            sub_listArray[position][m].diningTaxOption,
                            sub_listArray[position][m].taxIncludeOption
                        )
                        comboBogoCustomList.add(comboBogoCustomPojo)
                        comboBogoItemType = ComboBogoItemType(
                            sub_listArray[position][m].id,
                            sub_listArray[position][m].name,
                            sub_listArray[position][m].type,
                            sub_listArray[position][m].quantity,
                            comboBogoCustomList
                        )
                    }
                    comboItemTypeList.add(comboBogoItemType)
                }
                Log.d("comboBogoItemTypeList", comboItemTypeList.size.toString())
                showCombosDialog("Combo ${position + 1}", comboItemTypeList)
            }
            if (selectedPosition == position) {
                holder.rl_combo_id.setBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.success_text)
                )
            } else {
                holder.rl_combo_id.setBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.combo_bg_color)
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_combo_name_id = view.findViewById<TextView>(R.id.tv_combo_name_id)
            val rv_get_combo_list = view.findViewById<RecyclerView>(R.id.rv_get_combo_list)
            val tv_price_id = view.findViewById<TextView>(R.id.tv_price_id)
            val rl_combo_id = view.findViewById<LinearLayout>(R.id.rl_combo_id)
            val ll_combo_id = view.findViewById<LinearLayout>(R.id.ll_combo_id)
        }
    }

    private fun BogoDiscounts(
        json_obj: JsonObject,
        minDiscountAmount: Double,
        maxDiscountAmount: Double
    ) {
        loading_dialog.show()
        mainbogoArrayList = ArrayList()
        bogoGetArrayList = ArrayList()
        isBogoBuySelected = false
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getBOGODiscountsApi(json_obj)
        call.enqueue(object : Callback<BoGoResponse> {
            override fun onFailure(call: Call<BoGoResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<BoGoResponse>,
                response: Response<BoGoResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        //rv_combo_list
                        /*bogodiscouuntApplyType:1
                        (of first eligible item)
                        bogodiscouuntApplyType:2
                        (of least expensive eligible item)
                        bogodiscouuntApplyType:3
                        (of most expensive eligible item)*/
                        if (resp.bogoBuyItems.isNotEmpty()) {
                            for (k in resp.bogoBuyItems.indices) {
                                bogoArrayList = ArrayList()
                                for (element in resp.bogoBuyItems[k]) {
                                    bogoArrayList.add(element)
                                }
                                if (bogoArrayList.isNotEmpty()) {
                                    mainbogoArrayList.add(bogoArrayList)
                                }
                            }
                        }
                        if (resp.bogoGetItems.isNotEmpty()) {
                            for (m in resp.bogoGetItems.indices) {
                                bogoGetArrayList.add(resp.bogoGetItems[m])
                            }
                        }
                        val bogoBuyItemLM =
                            LinearLayoutManager(
                                this@OrderDetailsActivity, RecyclerView.VERTICAL, false
                            )
                        rv_buy_bogo_list.layoutManager = bogoBuyItemLM
                        val adapter_combodiscounts =
                            BoGoBuyListsAdapter(
                                this@OrderDetailsActivity,
                                mainbogoArrayList
                            )
                        rv_buy_bogo_list.adapter = adapter_combodiscounts
                        adapter_combodiscounts.notifyDataSetChanged()
                        if (resp.discountValueType == "percentage") {
                            tv_bogo_get_discount.text =
                                "Get One of the Following Items for ${resp.getDiscountValue} % OFF"
                        } else if (resp.discountValueType == "currency") {
                            tv_bogo_get_discount.text =
                                "Get One of the Following Items for $currencyType${resp.getDiscountValue} OFF"
                        }
                        bogoGetDiscountValue = resp.getDiscountValue
                        bogoGetDiscountValueType = resp.discountValueType
                        discountApplyType = resp.discountApplyType
                        // TODO apply discount based on type.
                        val bogoGetItemLM = LinearLayoutManager(
                            this@OrderDetailsActivity, RecyclerView.HORIZONTAL, false
                        )
                        rv_get_bogo_list.layoutManager = bogoGetItemLM
                        val adapter_getBOGOdiscounts =
                            BoGoGetListAdapter(
                                this@OrderDetailsActivity,
                                bogoGetArrayList,
                                bogoGetDiscountValueType,
                                bogoGetDiscountValue,
                                minDiscountAmount,
                                maxDiscountAmount
                            )
                        rv_get_bogo_list.adapter = adapter_getBOGOdiscounts
                        rv_get_bogo_list.hasFixedSize()
                        adapter_getBOGOdiscounts.notifyDataSetChanged()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class BoGoBuyListsAdapter(
        context: Context,
        val bogo_listArray: List<List<BogoBuyItem>>
    ) :
        RecyclerView.Adapter<BoGoBuyListsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        lateinit var bogo_sub_array: ArrayList<BogoBuyItem>
        lateinit var bogo_sub_group_array: ArrayList<BogoBuyItem>
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.bogo_main_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return bogo_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            bogo_sub_array = ArrayList()
            bogo_sub_group_array = ArrayList()
            for (m in bogo_listArray[position].indices) {
                if (bogo_listArray[position][m].type != "Menu Group") {
                    bogo_sub_array.add(bogo_listArray[position][m])
                    Log.e("@Array_bogo", bogo_listArray[position][m].itemName)
                } else {
                    holder.tv_group_id.text = bogo_listArray[position][m].name
                    bogo_sub_group_array.add(bogo_listArray[position][m])
                }
            }
            val drinks_LayoutManager =
                LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
            holder.rv_get_bogo_list.layoutManager = drinks_LayoutManager
            val getBoGoGroupLayoutManager =
                LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
            holder.rv_get_bogo_group_list.layoutManager = getBoGoGroupLayoutManager
            val adapter_combodiscounts =
                BoGoListAdapter(this@OrderDetailsActivity, bogo_sub_array)
            holder.rv_get_bogo_list.adapter = adapter_combodiscounts
            holder.rv_get_bogo_list.hasFixedSize()
            adapter_combodiscounts.notifyDataSetChanged()
            val orderBoGoGroupDiscountsAdapter =
                OrderBoGoGroupDiscountsAdapter(this@OrderDetailsActivity, bogo_sub_group_array)
            holder.rv_get_bogo_group_list.adapter = orderBoGoGroupDiscountsAdapter
            holder.rv_get_bogo_group_list.hasFixedSize()
            orderBoGoGroupDiscountsAdapter.notifyDataSetChanged()
            holder.cv_bogo_buy.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                bogoBuyItemTypeList = ArrayList()
                comboBogoId = bogo_listArray[position][0].bogoId
                comboBogoUniqueId = generateRandomId("bogo")
                for (m in bogo_listArray[position].indices) {
                    var bogoItemType = ComboBogoItemType("", "", "", 0, ArrayList())
                    if (bogo_listArray[position][m].type == "Menu Group") {
                        bogoItemType = ComboBogoItemType(
                            bogo_listArray[position][m].id,
                            bogo_listArray[position][m].name,
                            bogo_listArray[position][m].type,
                            bogo_listArray[position][m].quantity,
                            ArrayList()
                        )
                    } else if (bogo_listArray[position][m].type == "Menu Item") {
                        val bogoCustomList: ArrayList<ComboBogoCustomPojo> = ArrayList()
                        val timePriceList =
                            if (bogo_listArray[position][m].timePriceList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position][m].timePriceList!!
                            }
                        val sizeList =
                            if (bogo_listArray[position][m].sizeList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position][m].sizeList!!
                            }
                        val menuPriceList =
                            if (bogo_listArray[position][m].menuPriceList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position][m].menuPriceList!!
                            }
                        val taxesList: ArrayList<TaxRatesDetailsResponse> =
                            if (bogo_listArray[position][m].taxesList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position][m].taxesList!!
                            }
                        val specialRequestList =
                            if (bogo_listArray[position][m].specialRequestList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position][m].specialRequestList!!
                            }
                        val bogoCustomPojo = ComboBogoCustomPojo(
                            bogo_listArray[position][m].bogoId,
                            "bogo",
                            bogo_listArray[position][m].basePrice,
                            bogo_listArray[position][m].discountAmount,
                            bogo_listArray[position][m].discountId,
                            bogo_listArray[position][m].discountName,
                            bogo_listArray[position][m].discountType,
                            bogo_listArray[position][m].discountValue,
                            bogo_listArray[position][m].id,
                            bogo_listArray[position][m].isBogo,
                            bogo_listArray[position][m].isCombo,
                            bogo_listArray[position][m].discountApplicable,
                            bogo_listArray[position][m].itemId,
                            bogo_listArray[position][m].itemName,
                            bogo_listArray[position][m].itemType,
                            bogo_listArray[position][m].menuGroupId,
                            bogo_listArray[position][m].menuId,
                            bogo_listArray[position][m].modifiersList,
                            bogo_listArray[position][m].name,
                            bogo_listArray[position][m].pricingStrategy,
                            bogo_listArray[position][m].quantity,
                            specialRequestList,
                            timePriceList,
                            sizeList,
                            menuPriceList,
                            bogo_listArray[position][m].totalPrice,
                            bogo_listArray[position][m].type,
                            bogo_listArray[position][m].uniqueNumber,
                            bogo_listArray[position][m].unitPrice,
                            taxesList,
                            bogo_listArray[position][m].diningOptionTaxException,
                            bogo_listArray[position][m].diningTaxOption,
                            bogo_listArray[position][m].taxIncludeOption
                        )
                        bogoCustomList.add(bogoCustomPojo)
                        bogoItemType = ComboBogoItemType(
                            bogo_listArray[position][m].id,
                            bogo_listArray[position][m].name,
                            bogo_listArray[position][m].type,
                            bogo_listArray[position][m].quantity,
                            bogoCustomList
                        )
                    }
                    bogoBuyItemTypeList.add(bogoItemType)
                }
                Log.d("bogoItemTypeList", bogoBuyItemTypeList.size.toString())
                showBogoBuyDialog(bogoBuyItemTypeList)
            }
            if (selectedPosition == position) {
                holder.cv_bogo_buy.setCardBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.success_text)
                )
            } else {
                holder.cv_bogo_buy.setCardBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.combo_bg_color)
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rv_get_bogo_list = view.findViewById<RecyclerView>(R.id.rv_get_bogo_list)
            val tv_group_id = view.findViewById<TextView>(R.id.tv_group_id)
            val rv_get_bogo_group_list =
                view.findViewById<RecyclerView>(R.id.rv_get_bogo_group_list)
            val cv_bogo_buy = view.findViewById(R.id.cv_bogo_buy) as CardView
        }
    }

    inner class BoGoGetListAdapter(
        context: Context,
        val bogo_listArray: ArrayList<BogoGetItem>,
        val discountType: String,
        val discountValue: Int,
        val minDiscountAmount: Double,
        val maxDiscountAmount: Double
    ) :
        RecyclerView.Adapter<BoGoGetListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.combo_menu_item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return bogo_listArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_title_id.text = bogo_listArray[position].name
            holder.cv_bogo_id.setOnClickListener {
                for (m in 0 until bogo_listArray.size) {
                    selectedPosition = position
                    notifyDataSetChanged()
                    bogoGetItemTypeList = ArrayList()
                    var bogoItemType = ComboBogoItemType("", "", "", 0, ArrayList())
                    if (bogo_listArray[position].type == "Menu Group") {
                        bogoItemType = ComboBogoItemType(
                            bogo_listArray[position].id,
                            bogo_listArray[position].name,
                            bogo_listArray[position].type,
                            bogo_listArray[position].quantity,
                            ArrayList()
                        )
                    } else if (bogo_listArray[position].type == "Menu Item") {
                        val bogoCustomList: ArrayList<ComboBogoCustomPojo> = ArrayList()
                        val timePriceList =
                            if (bogo_listArray[position].timePriceList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position].timePriceList!!
                            }
                        val sizeList =
                            if (bogo_listArray[position].sizeList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position].sizeList!!
                            }
                        val menuPriceList =
                            if (bogo_listArray[position].menuPriceList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position].menuPriceList!!
                            }
                        val taxesList: ArrayList<TaxRatesDetailsResponse> =
                            if (bogo_listArray[position].taxesList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position].taxesList!!
                            }
                        val specialRequestList =
                            if (bogo_listArray[position].specialRequestList.toString() == "null") {
                                ArrayList()
                            } else {
                                bogo_listArray[position].specialRequestList!!
                            }
                        val bogoCustomPojo = ComboBogoCustomPojo(
                            bogo_listArray[position].bogoId,
                            "bogo",
                            bogo_listArray[position].basePrice,
                            bogo_listArray[position].discountAmount,
                            bogo_listArray[position].discountId,
                            bogo_listArray[position].discountName,
                            bogo_listArray[position].discountType,
                            bogo_listArray[position].discountValue,
                            bogo_listArray[position].id,
                            bogo_listArray[position].isBogo,
                            bogo_listArray[position].isCombo,
                            bogo_listArray[position].discountApplicable,
                            bogo_listArray[position].itemId,
                            bogo_listArray[position].itemName,
                            bogo_listArray[position].itemType,
                            bogo_listArray[position].menuGroupId,
                            bogo_listArray[position].menuId,
                            bogo_listArray[position].modifiersList,
                            bogo_listArray[position].name,
                            bogo_listArray[position].pricingStrategy,
                            bogo_listArray[position].quantity,
                            specialRequestList,
                            timePriceList,
                            sizeList,
                            menuPriceList,
                            bogo_listArray[position].totalPrice,
                            bogo_listArray[position].type,
                            bogo_listArray[position].uniqueNumber,
                            bogo_listArray[position].unitPrice,
                            taxesList,
                            bogo_listArray[position].diningOptionTaxException,
                            bogo_listArray[position].diningTaxOption,
                            bogo_listArray[position].taxIncludeOption
                        )
                        bogoCustomList.add(bogoCustomPojo)
                        bogoItemType = ComboBogoItemType(
                            bogo_listArray[position].id,
                            bogo_listArray[position].name,
                            bogo_listArray[position].type,
                            bogo_listArray[position].quantity,
                            bogoCustomList
                        )
                    }
                    bogoGetItemTypeList.add(bogoItemType)
                }
                Log.d("bogoItemTypeList", bogoGetItemTypeList.size.toString())
                showBogoGetDialog(bogoGetItemTypeList)
            }
            if (selectedPosition == position) {
                holder.cv_bogo_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.graditent_bg
                )
            } else {
                holder.cv_bogo_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.dark_gray_dispatch_background
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
            val cv_bogo_id = view.findViewById<CardView>(R.id.cv_bogo_id)
        }
    }

    private fun showEditComboDialog(ordersCustomData: OrdersCustomData) {
        var qty = ordersCustomData.qty.toInt()
        val bogoOfferId = ordersCustomData.comboOrBogoOfferId
        val bogoUniqueId = ordersCustomData.comboOrBogoUniqueId
        val edit_combo_dialog = Dialog(this)
        edit_combo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.combo_edit_dialog, null)
        edit_combo_dialog.setContentView(view)
        edit_combo_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        edit_combo_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        edit_combo_dialog.setCanceledOnTouchOutside(false)
        edit_combo_dialog.show()
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_remove = view.findViewById(R.id.tv_remove) as TextView
        val tv_done = view.findViewById(R.id.tv_done) as TextView
        val cv_combo_plus_id = view.findViewById(R.id.cv_combo_plus_id) as CardView
        val cv_combo_minus_id = view.findViewById(R.id.cv_combo_minus_id) as CardView
        val tv_set_combo_qty_id = view.findViewById(R.id.tv_set_combo_qty_id) as TextView
        tv_set_combo_qty_id.text = qty.toString()
        tv_cancel.setOnClickListener {
            edit_combo_dialog.dismiss()
        }
        tv_done.setOnClickListener {
            var checkTotalBeforeAddingToCheck = 0.00
            var minimumDiscountAmount = 0.00
            var maximumDiscountAmount = 0.00
            for (i in 0 until custom_array.size) {
                if (custom_array[i].isCombo) {
                    if (!custom_array[i].discountApplied) {
                        checkTotalBeforeAddingToCheck += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble())
                    }
                } else {
                    if (ordersCustomData.isBogo == custom_array[i].isBogo && bogoOfferId == custom_array[i].comboOrBogoOfferId && bogoUniqueId == custom_array[i].comboOrBogoUniqueId) {
                        if (custom_array[i].discountsArray.size > 0) {
                            minimumDiscountAmount =
                                custom_array[i].discountsArray[0].minDiscountAmount
                            maximumDiscountAmount =
                                custom_array[i].discountsArray[0].maxDiscountAmount
                        }
                        checkTotalBeforeAddingToCheck += (qty.toDouble() * custom_array[i].basePrice.toDouble())
                    } else {
                        checkTotalBeforeAddingToCheck += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble())
                    }
                }
            }
            if (maximumDiscountAmount != 0.00) {
                if (checkTotalBeforeAddingToCheck < minimumDiscountAmount) {
                    CenterToast().showToast(this@OrderDetailsActivity,"Sub-Total amount should be more than $currencyType$minimumDiscountAmount to apply this offer!")
                } else if (checkTotalBeforeAddingToCheck > maximumDiscountAmount) {
                    CenterToast().showToast(this@OrderDetailsActivity,"Sub-Total amount should be less than $currencyType$maximumDiscountAmount to apply this offer!")
                } else {
                    for (i in 0 until custom_array.size) {
                        var taxAppliedList: ArrayList<SendTaxRates>
                        if (custom_array[i].taxIncludeOption) {
                            taxAppliedList = taxCalculationNew(
                                custom_array[i].qty.toDouble(),
                                custom_array[i].basePrice.toDouble(),
                                custom_array[i].aplicableTaxRates
                            )
                        } else {
                            taxAppliedList = ArrayList()
                        }
                        if (ordersCustomData.isCombo == custom_array[i].isCombo && ordersCustomData.comboOrBogoOfferId == custom_array[i].comboOrBogoOfferId && ordersCustomData.comboOrBogoUniqueId == custom_array[i].comboOrBogoUniqueId) {
                            val customData = OrdersCustomData(
                                custom_array[i].basePrice,
                                custom_array[i].discountApplicable,
                                custom_array[i].id,
                                custom_array[i].name,
                                custom_array[i].itemType,
                                qty.toString(),
                                custom_array[i].total,
                                custom_array[i].discount,
                                custom_array[i].restaurantId,
                                custom_array[i].allmodifierArray,
                                custom_array[i].itemId,
                                custom_array[i].menuGroupId,
                                custom_array[i].menuId,
                                custom_array[i].discountApplied,
                                custom_array[i].discountAppliedType,
                                custom_array[i].discountsArray,
                                custom_array[i].itemStatus,
                                custom_array[i].isCombo,
                                custom_array[i].isBogo,
                                custom_array[i].comboOrBogoOfferId,
                                custom_array[i].comboOrBogoUniqueId,
                                custom_array[i].voidStatus,
                                custom_array[i].voidQuantity,
                                custom_array[i].aplicableTaxRates,
                                taxAppliedList,
                                custom_array[i].diningOptionTaxException,
                                custom_array[i].diningTaxOption,
                                custom_array[i].taxIncludeOption
                            )
                            custom_array[i] = customData
                        } else if (ordersCustomData.isBogo == custom_array[i].isBogo && ordersCustomData.comboOrBogoOfferId == custom_array[i].comboOrBogoOfferId && ordersCustomData.comboOrBogoUniqueId == custom_array[i].comboOrBogoUniqueId) {
                            val customData = OrdersCustomData(
                                custom_array[i].basePrice,
                                custom_array[i].discountApplicable,
                                custom_array[i].id,
                                custom_array[i].name,
                                custom_array[i].itemType,
                                qty.toString(),
                                custom_array[i].total,
                                custom_array[i].discount,
                                custom_array[i].restaurantId,
                                custom_array[i].allmodifierArray,
                                custom_array[i].itemId,
                                custom_array[i].menuGroupId,
                                custom_array[i].menuId,
                                custom_array[i].discountApplied,
                                custom_array[i].discountAppliedType,
                                custom_array[i].discountsArray,
                                custom_array[i].itemStatus,
                                custom_array[i].isCombo,
                                custom_array[i].isBogo,
                                custom_array[i].comboOrBogoOfferId,
                                custom_array[i].comboOrBogoUniqueId,
                                custom_array[i].voidStatus,
                                custom_array[i].voidQuantity,
                                custom_array[i].aplicableTaxRates,
                                taxAppliedList,
                                custom_array[i].diningOptionTaxException,
                                custom_array[i].diningTaxOption,
                                custom_array[i].taxIncludeOption
                            )
                            custom_array[i] = customData
                        }
                    }
                }
            } else {
                for (i in 0 until custom_array.size) {
                    var taxAppliedList: ArrayList<SendTaxRates>
                    if (custom_array[i].taxIncludeOption) {
                        taxAppliedList = taxCalculationNew(
                            custom_array[i].qty.toDouble(),
                            custom_array[i].basePrice.toDouble(),
                            custom_array[i].aplicableTaxRates
                        )
                    } else {
                        taxAppliedList = ArrayList()
                    }
                    if (ordersCustomData.isCombo == custom_array[i].isCombo && ordersCustomData.comboOrBogoOfferId == custom_array[i].comboOrBogoOfferId && ordersCustomData.comboOrBogoUniqueId == custom_array[i].comboOrBogoUniqueId) {
                        val customData = OrdersCustomData(
                            custom_array[i].basePrice,
                            custom_array[i].discountApplicable,
                            custom_array[i].id,
                            custom_array[i].name,
                            custom_array[i].itemType,
                            qty.toString(),
                            custom_array[i].total,
                            custom_array[i].discount,
                            custom_array[i].restaurantId,
                            custom_array[i].allmodifierArray,
                            custom_array[i].itemId,
                            custom_array[i].menuGroupId,
                            custom_array[i].menuId,
                            custom_array[i].discountApplied,
                            custom_array[i].discountAppliedType,
                            custom_array[i].discountsArray,
                            custom_array[i].itemStatus,
                            custom_array[i].isCombo,
                            custom_array[i].isBogo,
                            custom_array[i].comboOrBogoOfferId,
                            custom_array[i].comboOrBogoUniqueId,
                            custom_array[i].voidStatus,
                            custom_array[i].voidQuantity,
                            custom_array[i].aplicableTaxRates,
                            taxAppliedList,
                            custom_array[i].diningOptionTaxException,
                            custom_array[i].diningTaxOption,
                            custom_array[i].taxIncludeOption
                        )
                        custom_array[i] = customData
                    } else if (ordersCustomData.isBogo == custom_array[i].isBogo && ordersCustomData.comboOrBogoOfferId == custom_array[i].comboOrBogoOfferId && ordersCustomData.comboOrBogoUniqueId == custom_array[i].comboOrBogoUniqueId) {
                        val customData = OrdersCustomData(
                            custom_array[i].basePrice,
                            custom_array[i].discountApplicable,
                            custom_array[i].id,
                            custom_array[i].name,
                            custom_array[i].itemType,
                            qty.toString(),
                            custom_array[i].total,
                            custom_array[i].discount,
                            custom_array[i].restaurantId,
                            custom_array[i].allmodifierArray,
                            custom_array[i].itemId,
                            custom_array[i].menuGroupId,
                            custom_array[i].menuId,
                            custom_array[i].discountApplied,
                            custom_array[i].discountAppliedType,
                            custom_array[i].discountsArray,
                            custom_array[i].itemStatus,
                            custom_array[i].isCombo,
                            custom_array[i].isBogo,
                            custom_array[i].comboOrBogoOfferId,
                            custom_array[i].comboOrBogoUniqueId,
                            custom_array[i].voidStatus,
                            custom_array[i].voidQuantity,
                            custom_array[i].aplicableTaxRates,
                            taxAppliedList,
                            custom_array[i].diningOptionTaxException,
                            custom_array[i].diningTaxOption,
                            custom_array[i].taxIncludeOption
                        )
                        custom_array[i] = customData
                    }
                }
            }
            // check for discounts
            handleDiscounts()
            fullOrderSetUp()
            edit_combo_dialog.dismiss()
        }
        tv_remove.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                custom_array.removeIf { e: OrdersCustomData ->
                    ((e.comboOrBogoOfferId == ordersCustomData.comboOrBogoOfferId) && (e.comboOrBogoUniqueId == ordersCustomData.comboOrBogoUniqueId))
                }
            }
            // check for discounts
            handleDiscounts()
            fullOrderSetUp()
            edit_combo_dialog.dismiss()
        }
        cv_combo_plus_id.setOnClickListener {
            qty++
            tv_set_combo_qty_id.text = qty.toString()
        }
        cv_combo_minus_id.setOnClickListener {
            if (qty == 0) {
                // minimum quantity
            } else {
                qty--
            }
            tv_set_combo_qty_id.text = qty.toString()
        }
    }

    private fun getTaxRates(finalObject: JsonObject) {
        taxRatesList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getTaxRates(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Log.e("tax_rate error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val taxRates = resp.taxRates!!
                        if (taxRates.size > 0) {
                            for (i in 0 until taxRates.size) {
                                taxRatesList.add(taxRates[i])
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun taxCalculationNew(
        itemQuantity: Double,
        itemBasePrice: Double,
        taxList: ArrayList<TaxRatesDetailsResponse>
    ): ArrayList<SendTaxRates> {
        val taxesList = ArrayList<SendTaxRates>()
        var taxType = ""
        var taxTypeId = 0
        var tax: Double
        val itemTotal = (itemQuantity * itemBasePrice)
        var roundingOptions = ""
        var roundingOptionId = 0
        /*
        * taxTypes :
        * 1-Percent : calculate percent from total ,
        * 2-Fixed : add tax amount directly ,
        * 3-Table : ,
        * 0-Disable : tax = 0.00
        * */
        /*
        * roundingOptions :
        * 1- Half Even Rounding
        * 2- Half Up Rounding
        * 3- Always Round Down
        * 4- Always Round Up
        * */
        if (taxList.size > 0) {
            for (i in 0 until taxList.size) {
                tax = 0.00
                when (taxList[i].taxType) {
                    0 -> {
                        taxType = "disable"
                        taxTypeId = 0
                        tax = 0.00
                    }
                    1 -> {
                        val df = DecimalFormat("#.##")
                        val taxRate = itemTotal * (taxList[i].taxRate!!.toDouble() / 100)
                        when (taxList[i].roundingOptions) {
                            1 -> {
                                df.roundingMode = RoundingMode.HALF_EVEN
                                roundingOptions = "halfEvenRounding"
                                roundingOptionId = taxList[i].roundingOptions!!
                            }
                            2 -> {
                                df.roundingMode = RoundingMode.HALF_UP
                                roundingOptions = "halfUpRounding"
                                roundingOptionId = taxList[i].roundingOptions!!
                            }
                            3 -> {
                                df.roundingMode = RoundingMode.DOWN
                                roundingOptions = "roundDownRounding"
                                roundingOptionId = taxList[i].roundingOptions!!
                            }
                            4 -> {
                                df.roundingMode = RoundingMode.UP
                                roundingOptions = "roundUpRounding"
                                roundingOptionId = taxList[i].roundingOptions!!
                            }
                        }
                        taxType = "percent"
                        taxTypeId = 1
                        tax = df.format(taxRate).toDouble()
                    }
                    2 -> {
                        taxType = "fixed"
                        taxTypeId = 2
                        tax = itemQuantity * taxList[i].taxRate!!.toDouble()
                    }
                    3 -> {
                        taxType = "taxTable"
                        taxTypeId = 3
                        if (taxList[i].taxTable!!.size > 0) {
                            for (j in 0 until taxList[i].taxTable!!.size) {
                                if (itemTotal >= taxList[i].taxTable!![j].from!!.toDouble() && itemTotal <= taxList[i].taxTable!![j].to!!.toDouble()) {
                                    tax += taxList[i].taxTable!![j].taxApplied!!.toDouble()
                                }
                            }
                        } else {
                            tax = 0.00
                        }
                    }
                }
                if (taxList[i].taxType == 3) {
                    val sendTaxRates = SendTaxRates(
                        taxList[i].default,
                        taxList[i].importId!!,
                        taxList[i].status!!,
                        taxList[i].taxName!!,
                        0.00,
                        taxType,
                        taxTypeId,
                        taxList[i].taxTable!!,
                        roundingOptions,
                        roundingOptionId,
                        taxList[i].taxid!!,
                        taxList[i].uniqueNumber!!,
                        tax
                    )
                    taxesList.add(sendTaxRates)
                } else {
                    val sendTaxRates = SendTaxRates(
                        taxList[i].default,
                        taxList[i].importId!!,
                        taxList[i].status!!,
                        taxList[i].taxName!!,
                        taxList[i].taxRate!!,
                        taxType,
                        taxTypeId,
                        ArrayList(),
                        roundingOptions,
                        roundingOptionId,
                        taxList[i].taxid!!,
                        taxList[i].uniqueNumber!!,
                        tax
                    )
                    taxesList.add(sendTaxRates)
                }
            }
        }
        return taxesList
    }

    private fun getVoidItemsReasons(finalObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.voidItemReasonsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Log.e("void reasons error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        void_reasonsList = resp.void_reasonsList!!
                    } else {
                        void_reasonsList = ArrayList()
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun voidItem(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.voidItemApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("tax_rate error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        val qty = custom_array[item_index!!].qty.toInt() - voidQty
                        tv_set_qty_id.text = qty.toString()
                        val taxAppliedList: ArrayList<SendTaxRates>
                        if (custom_array[item_index!!].taxIncludeOption) {
                            taxAppliedList = taxCalculationNew(
                                custom_array[item_index!!].qty.toDouble(),
                                custom_array[item_index!!].basePrice.toDouble(),
                                custom_array[item_index!!].aplicableTaxRates
                            )
                        } else {
                            taxAppliedList = ArrayList()
                        }
                        if (custom_array[item_index!!].allmodifierArray.isNotEmpty()) {
                            for (i in 0 until custom_array[item_index!!].allmodifierArray.size) {
                                custom_array[item_index!!].allmodifierArray[i].total = qty * custom_array[item_index!!].allmodifierArray[i].total
                            }
                        }
                        if (qty == 0) {
                            val orderCustomData = OrdersCustomData(
                                custom_array[item_index!!].basePrice,
                                custom_array[item_index!!].discountApplicable,
                                custom_array[item_index!!].id,
                                custom_array[item_index!!].name,
                                custom_array[item_index!!].itemType,
                                qty.toString(),
                                (qty * custom_array[item_index!!].basePrice.toDouble()).toString(),
                                custom_array[item_index!!].discount,
                                custom_array[item_index!!].restaurantId,
                                custom_array[item_index!!].allmodifierArray,
                                custom_array[item_index!!].itemId,
                                custom_array[item_index!!].menuGroupId,
                                custom_array[item_index!!].menuId,
                                false,
                                "",
                                ArrayList(),
                                custom_array[item_index!!].itemStatus,
                                custom_array[item_index!!].isCombo,
                                custom_array[item_index!!].isBogo,
                                custom_array[item_index!!].comboOrBogoOfferId,
                                custom_array[item_index!!].comboOrBogoUniqueId,
                                custom_array[item_index!!].voidStatus,
                                custom_array[item_index!!].voidQuantity,
                                custom_array[item_index!!].aplicableTaxRates,
                                taxAppliedList,
                                custom_array[item_index!!].diningOptionTaxException,
                                custom_array[item_index!!].diningTaxOption,
                                custom_array[item_index!!].taxIncludeOption
                            )
                            custom_array[item_index!!] = orderCustomData
                        } else {
                            val orderCustomData = OrdersCustomData(
                                custom_array[item_index!!].basePrice,
                                custom_array[item_index!!].discountApplicable,
                                custom_array[item_index!!].id,
                                custom_array[item_index!!].name,
                                custom_array[item_index!!].itemType,
                                qty.toString(),
                                (qty * custom_array[item_index!!].basePrice.toDouble()).toString(),
                                custom_array[item_index!!].discount,
                                custom_array[item_index!!].restaurantId,
                                custom_array[item_index!!].allmodifierArray,
                                custom_array[item_index!!].itemId,
                                custom_array[item_index!!].menuGroupId,
                                custom_array[item_index!!].menuId,
                                custom_array[item_index!!].discountApplied,
                                custom_array[item_index!!].discountAppliedType,
                                custom_array[item_index!!].discountsArray,
                                custom_array[item_index!!].itemStatus,
                                custom_array[item_index!!].isCombo,
                                custom_array[item_index!!].isBogo,
                                custom_array[item_index!!].comboOrBogoOfferId,
                                custom_array[item_index!!].comboOrBogoUniqueId,
                                custom_array[item_index!!].voidStatus,
                                custom_array[item_index!!].voidQuantity,
                                custom_array[item_index!!].aplicableTaxRates,
                                taxAppliedList,
                                custom_array[item_index!!].diningOptionTaxException,
                                custom_array[item_index!!].diningTaxOption,
                                custom_array[item_index!!].taxIncludeOption
                            )
                            custom_array[item_index!!] = orderCustomData
                        }
                        if (custom_array[item_index!!].discountAppliedType == DISCOUNT_LEVEL_ITEM) {
                            if (custom_array[item_index!!].discountApplicable) {
                                if ((item_qty.toDouble() * custom_array[item_index!!].basePrice.toDouble()) >= custom_array[item_index!!].discountsArray[0].minDiscountAmount) {
                                    val orderCustomData = OrdersCustomData(
                                        custom_array[item_index!!].basePrice,
                                        custom_array[item_index!!].discountApplicable,
                                        custom_array[item_index!!].id,
                                        custom_array[item_index!!].name,
                                        custom_array[item_index!!].itemType,
                                        custom_array[item_index!!].qty,
                                        custom_array[item_index!!].total,
                                        custom_array[item_index!!].discount,
                                        custom_array[item_index!!].restaurantId,
                                        custom_array[item_index!!].allmodifierArray,
                                        custom_array[item_index!!].itemId,
                                        custom_array[item_index!!].menuGroupId,
                                        custom_array[item_index!!].menuId,
                                        custom_array[item_index!!].discountApplied,
                                        custom_array[item_index!!].discountAppliedType,
                                        custom_array[item_index!!].discountsArray,
                                        custom_array[item_index!!].itemStatus,
                                        custom_array[item_index!!].isCombo,
                                        custom_array[item_index!!].isBogo,
                                        custom_array[item_index!!].comboOrBogoOfferId,
                                        custom_array[item_index!!].comboOrBogoUniqueId,
                                        custom_array[item_index!!].voidStatus,
                                        custom_array[item_index!!].voidQuantity,
                                        custom_array[item_index!!].aplicableTaxRates,
                                        custom_array[item_index!!].taxAppliedList,
                                        custom_array[item_index!!].diningOptionTaxException,
                                        custom_array[item_index!!].diningTaxOption,
                                        custom_array[item_index!!].taxIncludeOption
                                    )
                                    custom_array[item_index!!] = orderCustomData
                                } else {
                                    val orderCustomData = OrdersCustomData(
                                        custom_array[item_index!!].basePrice,
                                        custom_array[item_index!!].discountApplicable,
                                        custom_array[item_index!!].id,
                                        custom_array[item_index!!].name,
                                        custom_array[item_index!!].itemType,
                                        custom_array[item_index!!].qty,
                                        custom_array[item_index!!].total,
                                        custom_array[item_index!!].discount,
                                        custom_array[item_index!!].restaurantId,
                                        custom_array[item_index!!].allmodifierArray,
                                        custom_array[item_index!!].itemId,
                                        custom_array[item_index!!].menuGroupId,
                                        custom_array[item_index!!].menuId,
                                        false,
                                        "",
                                        ArrayList(),
                                        custom_array[item_index!!].itemStatus,
                                        custom_array[item_index!!].isCombo,
                                        custom_array[item_index!!].isBogo,
                                        custom_array[item_index!!].comboOrBogoOfferId,
                                        custom_array[item_index!!].comboOrBogoUniqueId,
                                        custom_array[item_index!!].voidStatus,
                                        custom_array[item_index!!].voidQuantity,
                                        custom_array[item_index!!].aplicableTaxRates,
                                        custom_array[item_index!!].taxAppliedList,
                                        custom_array[item_index!!].diningOptionTaxException,
                                        custom_array[item_index!!].diningTaxOption,
                                        custom_array[item_index!!].taxIncludeOption
                                    )
                                    custom_array[item_index!!] = orderCustomData
                                }
                            } else {
                                val orderCustomData = OrdersCustomData(
                                    custom_array[item_index!!].basePrice,
                                    custom_array[item_index!!].discountApplicable,
                                    custom_array[item_index!!].id,
                                    custom_array[item_index!!].name,
                                    custom_array[item_index!!].itemType,
                                    custom_array[item_index!!].qty,
                                    custom_array[item_index!!].total,
                                    custom_array[item_index!!].discount,
                                    custom_array[item_index!!].restaurantId,
                                    custom_array[item_index!!].allmodifierArray,
                                    custom_array[item_index!!].itemId,
                                    custom_array[item_index!!].menuGroupId,
                                    custom_array[item_index!!].menuId,
                                    false,
                                    "",
                                    ArrayList(),
                                    custom_array[item_index!!].itemStatus,
                                    custom_array[item_index!!].isCombo,
                                    custom_array[item_index!!].isBogo,
                                    custom_array[item_index!!].comboOrBogoOfferId,
                                    custom_array[item_index!!].comboOrBogoUniqueId,
                                    custom_array[item_index!!].voidStatus,
                                    custom_array[item_index!!].voidQuantity,
                                    custom_array[item_index!!].aplicableTaxRates,
                                    custom_array[item_index!!].taxAppliedList,
                                    custom_array[item_index!!].diningOptionTaxException,
                                    custom_array[item_index!!].diningTaxOption,
                                    custom_array[item_index!!].taxIncludeOption
                                )
                                custom_array[item_index!!] = orderCustomData
                            }
                        } else {
                            handleDiscounts()
                        }
                        fullOrderSetUp()
                        void_reason_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun successDialog(message: String) {
        successDialog = Dialog(this)
        successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.order_success_dialog, null)
        successDialog.setContentView(view)
        successDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        successDialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        successDialog.setCanceledOnTouchOutside(false)
        val tv_order_status = view.findViewById(R.id.tv_order_status) as TextView
        val tv_done = view.findViewById(R.id.tv_done) as TextView
        tv_order_status.text = message
        tv_done.setOnClickListener {
            successDialog.dismiss()
            when {
                pay_type == "stay" -> {
                    finish()
                    startActivity(intent)
                }
                from_screen == "table_service" -> {
                    val intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }
                else -> {
                    finish()
                }
            }
        }
        successDialog.setOnCancelListener {
            when {
                pay_type == "stay" -> {
                    finish()
                    startActivity(intent)
                }
                from_screen == "table_service" -> {
                    val intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }
                else -> {
                    finish()
                }
            }
        }
        successDialog.show()
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@OrderDetailsActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@OrderDetailsActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)) { dialog, which ->
                            showStayOrders()
                            dialog!!.dismiss()
                        }
                        alert.setCancelable(false)
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateTableServiceGuests(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.updateTableServiceGuestsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun showStayOrders() {
        isStaySelectedAll = false
        stay_orders_dialog = Dialog(this)
        stay_orders_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.stay_orders_dialog, null)
        stay_orders_dialog.setContentView(view)
        stay_orders_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        stay_orders_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        stay_orders_dialog.setCanceledOnTouchOutside(true)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(stay_orders_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        stay_orders_dialog.window!!.attributes = layoutParams
        tv_select_all = view.findViewById(R.id.tv_select_all) as TextView
        val tv_remove = view.findViewById(R.id.tv_remove) as TextView
        ll_cancel = view.findViewById(R.id.ll_cancel) as LinearLayout
        tv_stay_orders_empty = view.findViewById(R.id.tv_stay_orders_empty) as TextView
        rv_stay_orders = view.findViewById(R.id.rv_stay_orders) as RecyclerView
        val layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        rv_stay_orders.layoutManager = layoutManager
        rv_stay_orders.hasFixedSize()
        ll_cancel.visibility = View.GONE
        if (stayOrders.size > 0) {
            tv_stay_orders_empty.visibility = View.GONE
            rv_stay_orders.visibility = View.VISIBLE
            tv_select_all.visibility = View.VISIBLE
            stayOrdersAdapter = StayOrdersAdapter(this@OrderDetailsActivity, stayOrders)
            rv_stay_orders.adapter = stayOrdersAdapter
            stayOrdersAdapter.notifyDataSetChanged()
        } else {
            tv_stay_orders_empty.visibility = View.VISIBLE
            rv_stay_orders.visibility = View.GONE
            tv_select_all.visibility = View.GONE
        }
        tv_select_all.setOnClickListener {
            if (isStaySelectedAll) {
                isStaySelectedAll = false
                tv_select_all.text = getString(R.string.select_all)
                stayOrdersAdapter.clearAll()
                ll_cancel.visibility = View.GONE
            } else {
                isStaySelectedAll = true
                tv_select_all.text = getString(R.string.deselect_all)
                stayOrdersAdapter.selectAll()
                ll_cancel.visibility = View.VISIBLE
            }
        }
        tv_remove.setOnClickListener {
            cancelReason("")
        }
        stay_orders_dialog.show()
    }

    private fun cancelReason(orderId: String) {
        stay_orders_comment_dialog = Dialog(this)
        stay_orders_comment_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.stay_orders_comment_dialog, null)
        stay_orders_comment_dialog.setContentView(view)
        stay_orders_comment_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        stay_orders_comment_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        stay_orders_comment_dialog.setCanceledOnTouchOutside(true)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(stay_orders_comment_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        stay_orders_comment_dialog.window!!.attributes = layoutParams
        val tv_done = view.findViewById(R.id.tv_done) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val et_reason = view.findViewById(R.id.et_reason) as EditText
        tv_cancel.setOnClickListener {
            stay_orders_comment_dialog.dismiss()
        }
        tv_done.setOnClickListener {
            if (et_reason.text.toString().trim() == "") {
                Toast.makeText(
                    this@OrderDetailsActivity,
                    "Please enter reason for cancellation",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val array = JSONArray()
                if (orderId == "") {
                    val mSelected = stayOrdersAdapter.getSelectedList()
                    if (mSelected.size > 0) {
                        for (i in 0 until mSelected.size) {
                            array.put(mSelected[i].orderId.toString())
                        }
                    }
                } else {
                    array.put(orderId)
                }
                val jObject = JSONObject()
                jObject.put("employeeId", mEmployeeId)
                jObject.put("restaurantId", restaurantId)
                jObject.put("orders_list", array)
                jObject.put("comment", et_reason.text.toString().trim())
                val finalObject = JsonParser.parseString(jObject.toString()).asJsonObject
                Log.d("cancelStayOrders", finalObject.toString())
                cancelStayOrders(finalObject = finalObject)
            }
        }
        stay_orders_comment_dialog.show()
    }

    private fun checkStayOrders(finalObject: JsonObject) {
        loading_dialog.show()
        stayOrders = ArrayList()
        val api = ApiInterface.create()
        val call = api.getStayOrdersApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        stayOrders = resp.orders!!
                        if (stayOrders.size > 0) {
                            tv_stay_orders_empty.visibility = View.GONE
                            rv_stay_orders.visibility = View.VISIBLE
                            tv_select_all.visibility = View.VISIBLE
                            if (isStaySelectedAll) {
                                ll_cancel.visibility = View.VISIBLE
                            } else {
                                ll_cancel.visibility = View.GONE
                            }
                            stayOrdersAdapter =
                                StayOrdersAdapter(this@OrderDetailsActivity, stayOrders)
                            rv_stay_orders.adapter = stayOrdersAdapter
                            stayOrdersAdapter.notifyDataSetChanged()
                        } else {
                            tv_stay_orders_empty.visibility = View.VISIBLE
                            rv_stay_orders.visibility = View.GONE
                            tv_select_all.visibility = View.GONE
                            ll_cancel.visibility = View.GONE
                        }
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun checkStayOrdersCount(finalObject: JsonObject) {
        loading_dialog.show()
        stayOrders = ArrayList()
        val api = ApiInterface.create()
        val call = api.getStayOrdersApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        stayOrders = resp.orders!!
                        tv_stay_order_count.text =
                            getString(R.string.stay_orders) + " (${stayOrders.size})"
                    } else {
                        tv_stay_order_count.text = getString(R.string.stay_orders)
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun cancelStayOrders(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.cancelStayOrdersApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val obj = JSONObject()
                        obj.put("employeeId", mEmployeeId)
                        obj.put("restaurantId", restaurantId)
                        val final_object = JsonParser.parseString(obj.toString()).asJsonObject
                        checkStayOrders(final_object)
                        checkStayOrdersCount(final_object)
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        stay_orders_comment_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class StayOrdersAdapter(context: Context, ordersList: ArrayList<StayOrderResponse>) :
        RecyclerView.Adapter<StayOrdersAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<StayOrderResponse>? = null
        private var mSelectedList: ArrayList<StayOrderResponse> = ArrayList()

        init {
            this.mContext = context
            this.mList = ordersList
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            position: Int
        ): StayOrdersAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.stay_order_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: StayOrdersAdapter.ViewHolder, @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_dine_type.isSelected = true
            holder.tv_order_number.text =
                "#" + mList!![position].checkNumber.toString() + "/" + mList!![position].tableNumber.toString()
            holder.tv_dine_type.text = mList!![position].dineInOptionName.toString()
            holder.tv_check_order_number.text = "Order #" + mList!![position].orderNumber.toString()
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd", Locale.getDefault())
                val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date1 = sdf1.format(date!!)
                val time = stf.format(date)
                holder.tv_order_date.text = "$date1 $time".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                e.printStackTrace()
                holder.tv_order_date.text = ""
            }
            holder.tv_order_amount.text =
                currencyType + "%.2f".format(mList!![position].totalAmount.toString().toDouble())
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items!!.size) {
                sbString.append(mList!![position].items!![i].name).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_order_items.text = strList
            holder.tv_cancel.setOnClickListener {
                cancelReason(mList!![position].orderId.toString())
            }
            holder.tv_view.setOnClickListener {
                try {
                    val jObject = JSONObject()
                    jObject.put("userId", mEmployeeId)
                    jObject.put("restaurantId", restaurantId)
                    jObject.put("orderId", mList!![position].orderId.toString())
                    val finalObject = JsonParser.parseString(jObject.toString()).asJsonObject
                    Log.d("finalObject",finalObject.toString())
                    getOrderDetails(finalObject = finalObject)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            holder.cv_header.setOnClickListener {
                if (mSelectedList.isNotEmpty()) {
                    if (mSelectedList.contains(mList!![position])) {
                        mSelectedList.remove(mList!![position])
                        if (mSelectedList.size == 0) {
                            isStaySelectedAll = false
                            tv_select_all.text = getString(R.string.select_all)
                            ll_cancel.visibility = View.GONE
                        }
                        holder.cv_header.setCardBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                        holder.tv_dine_type.background = ContextCompat.getDrawable(
                            mContext!!,
                            R.drawable.payment_terminal_unselected_in_out
                        )
                        holder.tv_order_number.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.current_status_close
                            )
                        )
                        holder.tv_dine_type.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.current_status_close
                            )
                        )
                        holder.tv_order_date.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.current_status_close
                            )
                        )
                        holder.tv_order_amount.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.current_status_close
                            )
                        )
                        holder.tv_order_items.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.current_status_close
                            )
                        )
                        holder.tv_check_order_number.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.current_status_close
                            )
                        )
                    } else {
                        mSelectedList.add(mList!![position])
                        isStaySelectedAll = true
                        tv_select_all.text = getString(R.string.deselect_all)
                        ll_cancel.visibility = View.VISIBLE
                        holder.cv_header.setCardBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.payment_list_item_selected
                            )
                        )
                        holder.tv_dine_type.background = ContextCompat.getDrawable(
                            mContext!!,
                            R.drawable.payment_terminal_selected_in_out
                        )
                        holder.tv_order_number.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                        holder.tv_dine_type.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                        holder.tv_order_date.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                        holder.tv_order_amount.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                        holder.tv_order_items.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                        holder.tv_check_order_number.setTextColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.white
                            )
                        )
                    }
                } else {
                    mSelectedList.add(mList!![position])
                    isStaySelectedAll = true
                    tv_select_all.text = getString(R.string.deselect_all)
                    ll_cancel.visibility = View.VISIBLE
                    holder.cv_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.payment_list_item_selected
                        )
                    )
                    holder.tv_dine_type.background = ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                    holder.tv_order_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_dine_type.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_order_date.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_order_amount.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_order_items.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_check_order_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                }
                Log.d("sizeeee", mSelectedList.size.toString())
            }
            if (mSelectedList.isNotEmpty()) {
                if (mSelectedList.contains(mList!![position])) {
                    holder.cv_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.payment_list_item_selected
                        )
                    )
                    holder.tv_dine_type.background = ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                    holder.tv_order_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_dine_type.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_order_date.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_order_amount.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_order_items.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_check_order_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                } else {
                    holder.cv_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_dine_type.background = ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )
                    holder.tv_order_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_close
                        )
                    )
                    holder.tv_dine_type.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_close
                        )
                    )
                    holder.tv_order_date.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_close
                        )
                    )
                    holder.tv_order_amount.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_close
                        )
                    )
                    holder.tv_order_items.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_close
                        )
                    )
                    holder.tv_check_order_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_close
                        )
                    )
                }
            } else {
                holder.cv_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_dine_type.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.payment_terminal_unselected_in_out
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_dine_type.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_date.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_amount.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_items.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_check_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
            }
        }

        fun selectAll() {
            mSelectedList.clear()
            mSelectedList.addAll(mList!!)
            notifyDataSetChanged()
        }

        fun clearAll() {
            mSelectedList.clear()
            notifyDataSetChanged()
        }

        fun selectedList(selectedList: ArrayList<StayOrderResponse>) {
            this.mSelectedList = selectedList
            notifyDataSetChanged()
        }

        fun getSelectedList(): ArrayList<StayOrderResponse> {
            return mSelectedList
        }

        fun getItem(position: Int): StayOrderResponse {
            return mList!![position]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val cv_header = view.findViewById(R.id.cv_header) as CardView
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_dine_type = view.findViewById(R.id.tv_dine_type) as TextView
            var tv_order_date = view.findViewById(R.id.tv_order_date) as TextView
            var tv_order_amount = view.findViewById(R.id.tv_order_amount) as TextView
            var tv_order_items = view.findViewById(R.id.tv_order_items) as TextView
            var tv_check_order_number = view.findViewById(R.id.tv_check_order_number) as TextView
            var tv_view = view.findViewById(R.id.tv_view) as TextView
            var tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        }
    }

    private fun getOrderDetails(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewOrderApi(finalObject)
        call.enqueue(object : Callback<ViewOrderDataResponse> {
            override fun onFailure(call: Call<ViewOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ViewOrderDataResponse>,
                response: Response<ViewOrderDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        orderDetails = respBody.orderDetails!!
                        try {
                            customizeOrderDetails(orderDetails, "")
                            isStayOrderSelected = true
                            stay_orders_dialog.dismiss()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        Log.d("orderDetails---", orderDetails.toString())
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun showTableOrders(checks: ArrayList<OrderDetailsResponse>) {
        table_orders_dialog = Dialog(this)
        table_orders_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.table_orders_dialog, null)
        table_orders_dialog.setContentView(view)
        table_orders_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        table_orders_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        table_orders_dialog.setCanceledOnTouchOutside(false)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(table_orders_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        table_orders_dialog.window!!.attributes = layoutParams
        val tv_table_close = table_orders_dialog.findViewById(R.id.tv_table_close) as TextView
        val rv_table_orders = table_orders_dialog.findViewById(R.id.rv_table_orders) as RecyclerView
        val tv_orders_empty = table_orders_dialog.findViewById(R.id.tv_orders_empty) as TextView
        val layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        rv_table_orders.layoutManager = layoutManager
        rv_table_orders.hasFixedSize()
        if (checks.size > 0) {
            tv_orders_empty.visibility = View.GONE
            rv_table_orders.visibility = View.VISIBLE
        } else {
            tv_orders_empty.visibility = View.VISIBLE
            rv_table_orders.visibility = View.GONE
        }
        val tableOrdersAdapter = TableOrdersAdapter(this@OrderDetailsActivity, checks)
        rv_table_orders.adapter = tableOrdersAdapter
        tableOrdersAdapter.notifyDataSetChanged()
        tv_table_close.setOnClickListener {
            table_orders_dialog.dismiss()
        }
        table_orders_dialog.show()
    }

    inner class TableOrdersAdapter(context: Context, ordersList: ArrayList<OrderDetailsResponse>) :
        RecyclerView.Adapter<TableOrdersAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<OrderDetailsResponse>? = null

        init {
            this.mContext = context
            this.mList = ordersList
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            position: Int
        ): TableOrdersAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.table_order_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder, @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_dine_type.isSelected = true
            holder.tv_order_number.text =
                "#" + mList!![position].checkNumber.toString() + "/" + mList!![position].tableNumber.toString()
            holder.tv_dine_type.text = mList!![position].dineInOptionName.toString()
            holder.tv_check_order_number.text = "Order #" + mList!![position].orderNumber.toString()
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd", Locale.getDefault())
                val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date1 = sdf1.format(date!!)
                val time = stf.format(date)
                holder.tv_order_date.text = "$date1 $time".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                e.printStackTrace()
                holder.tv_order_date.text = ""
            }
            holder.tv_order_amount.text =
                currencyType + "%.2f".format(mList!![position].totalAmount.toString().toDouble())
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].itemsDetails!!.size) {
                sbString.append(mList!![position].itemsDetails!![i].itemName).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_order_items.text = strList
            holder.cv_header.setOnClickListener {
                try {
                    orderDetails = mList!![position]
                    customizeOrderDetails(orderDetails, "table")
                    val obj = JSONObject()
                    obj.put("userId", mEmployeeId)
                    obj.put("restaurantId", restaurantId)
                    val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                    GetRightMenuResponse(jObject)
                    table_orders_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        fun getItem(position: Int): OrderDetailsResponse {
            return mList!![position]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val cv_header = view.findViewById(R.id.cv_header) as CardView
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_dine_type = view.findViewById(R.id.tv_dine_type) as TextView
            var tv_order_date = view.findViewById(R.id.tv_order_date) as TextView
            var tv_order_amount = view.findViewById(R.id.tv_order_amount) as TextView
            var tv_order_items = view.findViewById(R.id.tv_order_items) as TextView
            var tv_check_order_number = view.findViewById(R.id.tv_check_order_number) as TextView
        }
    }

    private fun addCurbsideDetails() {
        val add_curbside_dialog = Dialog(this)
        add_curbside_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.curbside_dialog, null)
        add_curbside_dialog.setContentView(view)
        add_curbside_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        add_curbside_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        add_curbside_dialog.setCanceledOnTouchOutside(false)
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val et_transport_color = view.findViewById(R.id.et_transport_color) as EditText
        val et_transport_desc = view.findViewById(R.id.et_transport_desc) as EditText
        val et_notes = view.findViewById(R.id.et_notes) as EditText
        tv_cancel.setOnClickListener {
            add_curbside_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            isHavingCurbSideInfo = true
            notes = et_notes.text.toString().trim()
            transportColor = et_transport_color.text.toString().trim()
            transportDescription = et_transport_desc.text.toString().trim()
            add_curbside_dialog.dismiss()
        }
        add_curbside_dialog.show()
    }

    private fun addDeliveryDetails(addresses: ArrayList<CustomerAddressDataResponse>) {
        var country_id = ""
        var state_id = ""
        var city_id = ""
        val add_delivery_dialog = Dialog(this)
        add_delivery_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.delivery_address_dialog, null)
        add_delivery_dialog.setContentView(view)
        add_delivery_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        add_delivery_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        add_delivery_dialog.setCanceledOnTouchOutside(false)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(add_delivery_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        add_delivery_dialog.window!!.attributes = layoutParams
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_add_new_address = view.findViewById(R.id.tv_add_new_address) as TextView
        val et_delivery_notes = view.findViewById(R.id.et_delivery_notes) as EditText
        val et_delivery_address_1 = view.findViewById(R.id.et_delivery_address_1) as EditText
        val et_delivery_address_2 = view.findViewById(R.id.et_delivery_address_2) as EditText
        val et_delivery_zip_code = view.findViewById(R.id.et_delivery_zip_code) as EditText
        val scrollView =
            view.findViewById(R.id.scrollView) as ScrollView
        val rv_delivery_addresses = view.findViewById(R.id.rv_delivery_addresses) as RecyclerView
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_delivery_addresses.layoutManager = layoutManager
        rv_delivery_addresses.hasFixedSize()
        val adapter = CustomerAddressListAdapter(this, addresses)
        rv_delivery_addresses.adapter = adapter
        adapter.notifyDataSetChanged()
        if (addresses.size > 0) {
            rv_delivery_addresses.visibility = View.VISIBLE
            tv_add_new_address.visibility = View.VISIBLE
            scrollView.visibility = View.GONE
        } else {
            rv_delivery_addresses.visibility = View.GONE
            tv_add_new_address.visibility = View.GONE
            scrollView.visibility = View.VISIBLE
        }
        tv_add_new_address.setOnClickListener {
            isHavingDeliveryDetails = false
            rv_delivery_addresses.visibility = View.GONE
            tv_add_new_address.visibility = View.GONE
            scrollView.visibility = View.VISIBLE
        }
        sp_delivery_country = view.findViewById(R.id.sp_delivery_country)
        sp_delivery_state = view.findViewById(R.id.sp_delivery_state)
        sp_delivery_city = view.findViewById(R.id.sp_delivery_city)
        val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId)
        jObj.put("restaurantId", restaurantId)
        val jsonObject = JsonParser.parseString(jObj.toString()).asJsonObject
        statesList.add(dummyState)
        citiesList.add(dummyCity)
        statesListAdapter = StatesListAdapter(
            this@OrderDetailsActivity,
            statesList
        )
        sp_delivery_state.adapter = statesListAdapter
        citiesListAdapter = CitiesListAdapter(
            this@OrderDetailsActivity,
            citiesList
        )
        sp_delivery_city.adapter = citiesListAdapter
        getCountries(jsonObject, sp_delivery_country)
        tv_cancel.setOnClickListener {
            add_delivery_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            if (isHavingDeliveryDetails) {
                isHavingDeliveryInfo = true
                customer_delivery_address_notes = et_delivery_notes.text.toString().trim()
                add_delivery_dialog.dismiss()
            } else {
                if (country_id == "" || country_id == "dummy") {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Please select country",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (state_id == "" || state_id == "dummy") {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Please select state",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (city_id == "" || city_id == "dummy") {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Please select city",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (et_delivery_address_1.text.toString().trim() == "") {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Please enter address",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (et_delivery_zip_code.text.toString().trim() == "") {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        "Please enter zipcode",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    isHavingDeliveryInfo = true
                    customer_delivery_address_line_1 = et_delivery_address_1.text.toString().trim()
                    customer_delivery_address_line_2 = et_delivery_address_2.text.toString().trim()
                    customer_delivery_address_zip_code = et_delivery_zip_code.text.toString().trim()
                    if (customer_delivery_address_line_2 == "") {
                        customer_delivery_address =
                            "$customer_delivery_address_line_1, $cityName, $stateName, $customer_delivery_address_zip_code, $countryCode"
                    } else {
                        customer_delivery_address =
                            "$customer_delivery_address_line_1, $customer_delivery_address_line_1, $cityName, $stateName, $customer_delivery_address_zip_code, $countryCode"
                    }
                    customer_delivery_address_lat = "40.730610"
                    customer_delivery_address_long = "-73.935242"
                    customer_delivery_address_notes = et_delivery_notes.text.toString().trim()
                    add_delivery_dialog.dismiss()
                }
            }
            Log.d(
                "address_details",
                "$customer_delivery_address_id, $customer_delivery_address, $customer_delivery_address_lat, $customer_delivery_address_long, $customer_delivery_address_notes"
            )
        }
        sp_delivery_country.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                country_id =
                    countriesListAdapter.getSelectedItem(position)._id!!
                countryCode = countriesListAdapter.getSelectedItem(position).countryCode.toString()
                Log.d("add customer countryId", country_id)
                if (country_id == "dummy") {
                    // do nothing
                } else {
                    // resetting state & city spinners
                    statesListAdapter = StatesListAdapter(
                        this@OrderDetailsActivity,
                        statesList
                    )
                    sp_delivery_state.adapter = statesListAdapter
                    citiesListAdapter = CitiesListAdapter(
                        this@OrderDetailsActivity,
                        citiesList
                    )
                    sp_delivery_city.adapter = citiesListAdapter
                    val jsonObj = JSONObject()
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("countryId", country_id)
                    val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    getStates(finalObject, sp_delivery_state)
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        sp_delivery_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                state_id = statesListAdapter.getSelectedItem(position)._id!!
                stateName = statesListAdapter.getSelectedItem(position).name.toString()
                Log.d("add customer stateId", state_id)
                if (state_id == "dummy") {
                    // do nothing
                } else {
                    // resetting city spinner
                    citiesListAdapter = CitiesListAdapter(
                        this@OrderDetailsActivity,
                        citiesList
                    )
                    sp_delivery_city.adapter = citiesListAdapter
                    val jsonObj = JSONObject()
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("stateId", state_id)
                    val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    getCities(finalObject, sp_delivery_city)
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        sp_delivery_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                city_id = citiesListAdapter.getSelectedItem(position)._id!!
                cityName = citiesListAdapter.getSelectedItem(position).name.toString()
                Log.d("add customer cityId", city_id)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        add_delivery_dialog.show()
    }

    private fun getCustomerAddress(finalObject: JsonObject) {
        loading_dialog.show()
        mSelectedDeliveryAddressIndex = -1
        val api = ApiInterface.create()
        val call = api.getCustomerAddressApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        customerAddresesList = respBody.customers_addressList!!
                        addDeliveryDetails(customerAddresesList)
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class CustomerAddressListAdapter(
        context: Context,
        list: ArrayList<CustomerAddressDataResponse>
    ) :
        RecyclerView.Adapter<CustomerAddressListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<CustomerAddressDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.delivery_address_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): CustomerAddressDataResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            holder.tv_address.text = mList!![position].fullAddress.toString()
            holder.cv_item_header.setOnClickListener {
                if (mSelectedDeliveryAddressIndex == -1 || mSelectedDeliveryAddressIndex != position) {
                    mSelectedDeliveryAddressIndex = position
                    isHavingDeliveryDetails = true
                    customer_delivery_address_id = mList!![position].id.toString()
                    customer_delivery_address = mList!![position].fullAddress.toString()
                    customer_delivery_address_lat = mList!![position].latitude.toString()
                    customer_delivery_address_long = mList!![position].longitude.toString()
                } else if (mSelectedDeliveryAddressIndex == position) {
                    mSelectedDeliveryAddressIndex = -1
                    isHavingDeliveryDetails = false
                    customer_delivery_address_id = ""
                    customer_delivery_address = ""
                    customer_delivery_address_lat = ""
                    customer_delivery_address_long = ""
                }
                notifyDataSetChanged()
            }
            if (mSelectedDeliveryAddressIndex == position) {
                holder.cv_item_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.cyan
                    )
                )
                holder.tv_address.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_address_title.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_item_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_address.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.edit_text_hint
                    )
                )
                holder.tv_address_title.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.edit_text_hint
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_address: TextView = view.findViewById(R.id.tv_address)
            var tv_address_title: TextView = view.findViewById(R.id.tv_address_title)
            var cv_item_header: CardView = view.findViewById(R.id.cv_item_header)
        }
    }

    private fun handleDiscounts() {
        var checkTotal = 0.00
        var includeItemsCount = 0
        var hasIncludeItems = false
        var hasExcludeItems = false
        if (checkLevelDiscountArray.size > 0) {
            for (i in 0 until checkLevelDiscountArray[0].exceptDiscountItems.size) {
                hasExcludeItems = true
            }
        }
        for (i in 0 until custom_array.size) {
            if (custom_array[i].isCombo) {
                if (!custom_array[i].discountApplied) {
                    checkTotal += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble())
                }
            } else {
                checkTotal += (custom_array[i].qty.toDouble() * custom_array[i].basePrice.toDouble())
            }
            if (checkLevelDiscountArray.size > 0) {
                if (checkLevelDiscountArray[0].includeDiscountItems.size > 0) {
                    hasIncludeItems = true
                    for (j in 0 until checkLevelDiscountArray[0].includeDiscountItems.size) {
                        if (custom_array[i].itemId == checkLevelDiscountArray[0].includeDiscountItems[j].id) {
                            includeItemsCount += 1
                        }
                    }
                }
            } else {
                hasIncludeItems = false
                includeItemsCount = 0
            }
        }
        if (checkLevelDiscountArray.size > 0) {
            if (hasIncludeItems && includeItemsCount == 0) {
                // don't apply discount
                checkLevelApplied = false
                for (i in 0 until custom_array.size) {
                    if (custom_array[i].isCombo || custom_array[i].isBogo) {
                        // don't add or remove discounts
                    } else {
                        // remove discounts
                        val customData = OrdersCustomData(
                            custom_array[i].basePrice,
                            custom_array[i].discountApplicable,
                            custom_array[i].id,
                            custom_array[i].name,
                            custom_array[i].itemType,
                            custom_array[i].qty,
                            custom_array[i].total,
                            custom_array[i].discount,
                            custom_array[i].restaurantId,
                            custom_array[i].allmodifierArray,
                            custom_array[i].itemId,
                            custom_array[i].menuGroupId,
                            custom_array[i].menuId,
                            false,
                            "",
                            ArrayList(),
                            custom_array[i].itemStatus,
                            custom_array[i].isCombo,
                            custom_array[i].isBogo,
                            custom_array[i].comboOrBogoOfferId,
                            custom_array[i].comboOrBogoUniqueId,
                            custom_array[i].voidStatus,
                            custom_array[i].voidQuantity,
                            custom_array[i].aplicableTaxRates,
                            custom_array[i].taxAppliedList,
                            custom_array[i].diningOptionTaxException,
                            custom_array[i].diningTaxOption,
                            custom_array[i].taxIncludeOption
                        )
                        custom_array[i] = customData
                    }
                }
                CenterToast().showToast(this@OrderDetailsActivity,"Must contain at least 1 Included item to avail this discount!")
            } else if ((!hasIncludeItems && includeItemsCount == 0) || (hasIncludeItems && includeItemsCount > 0)) {
                // (no included items and item count = 0) or (having included items and item count > 0), apply to all
                checkLevelApplied = true
                if (checkLevelDiscountArray[0].maxDiscountAmount != 0.00) {
                    when {
                        checkTotal > checkLevelDiscountArray[0].maxDiscountAmount -> {
                            checkLevelApplied = false
                            CenterToast().showToast(this@OrderDetailsActivity,"Sub-Total amount should be less than $currencyType${checkLevelDiscountArray[0].maxDiscountAmount}")
                            // don't apply discount
                            for (i in 0 until custom_array.size) {
                                if (custom_array[i].isCombo || custom_array[i].isBogo) {
                                    // don't add or remove discounts
                                } else {
                                    // remove discounts
                                    val customData = OrdersCustomData(
                                        custom_array[i].basePrice,
                                        custom_array[i].discountApplicable,
                                        custom_array[i].id,
                                        custom_array[i].name,
                                        custom_array[i].itemType,
                                        custom_array[i].qty,
                                        custom_array[i].total,
                                        custom_array[i].discount,
                                        custom_array[i].restaurantId,
                                        custom_array[i].allmodifierArray,
                                        custom_array[i].itemId,
                                        custom_array[i].menuGroupId,
                                        custom_array[i].menuId,
                                        false,
                                        "",
                                        ArrayList(),
                                        custom_array[i].itemStatus,
                                        custom_array[i].isCombo,
                                        custom_array[i].isBogo,
                                        custom_array[i].comboOrBogoOfferId,
                                        custom_array[i].comboOrBogoUniqueId,
                                        custom_array[i].voidStatus,
                                        custom_array[i].voidQuantity,
                                        custom_array[i].aplicableTaxRates,
                                        custom_array[i].taxAppliedList,
                                        custom_array[i].diningOptionTaxException,
                                        custom_array[i].diningTaxOption,
                                        custom_array[i].taxIncludeOption
                                    )
                                    custom_array[i] = customData
                                }
                            }
                        }
                        checkTotal < checkLevelDiscountArray[0].minDiscountAmount -> {
                            checkLevelApplied = false
                            CenterToast().showToast(this@OrderDetailsActivity,"Sub-Total amount should be more than $currencyType${checkLevelDiscountArray[0].minDiscountAmount}")
                            // don't apply discount
                            for (i in 0 until custom_array.size) {
                                if (custom_array[i].isCombo || custom_array[i].isBogo) {
                                    // don't add or remove discounts
                                } else {
                                    // remove discounts
                                    val customData = OrdersCustomData(
                                        custom_array[i].basePrice,
                                        custom_array[i].discountApplicable,
                                        custom_array[i].id,
                                        custom_array[i].name,
                                        custom_array[i].itemType,
                                        custom_array[i].qty,
                                        custom_array[i].total,
                                        custom_array[i].discount,
                                        custom_array[i].restaurantId,
                                        custom_array[i].allmodifierArray,
                                        custom_array[i].itemId,
                                        custom_array[i].menuGroupId,
                                        custom_array[i].menuId,
                                        false,
                                        "",
                                        ArrayList(),
                                        custom_array[i].itemStatus,
                                        custom_array[i].isCombo,
                                        custom_array[i].isBogo,
                                        custom_array[i].comboOrBogoOfferId,
                                        custom_array[i].comboOrBogoUniqueId,
                                        custom_array[i].voidStatus,
                                        custom_array[i].voidQuantity,
                                        custom_array[i].aplicableTaxRates,
                                        custom_array[i].taxAppliedList,
                                        custom_array[i].diningOptionTaxException,
                                        custom_array[i].diningTaxOption,
                                        custom_array[i].taxIncludeOption
                                    )
                                    custom_array[i] = customData
                                }
                            }
                        }
                        else -> {
                            //applying discount
                            for (i in 0 until custom_array.size) {
                                if (!custom_array[i].isCombo && !custom_array[i].isBogo) {
                                    if (custom_array[i].discountApplicable) {
                                        if (hasExcludeItems) {
                                            for (j in 0 until checkLevelDiscountArray[0].exceptDiscountItems.size) {
                                                if (checkLevelDiscountArray[0].exceptDiscountItems[j].id == custom_array[i].itemId) {
                                                    val customData = OrdersCustomData(
                                                        custom_array[i].basePrice,
                                                        custom_array[i].discountApplicable,
                                                        custom_array[i].id,
                                                        custom_array[i].name,
                                                        custom_array[i].itemType,
                                                        custom_array[i].qty,
                                                        custom_array[i].total,
                                                        custom_array[i].discount,
                                                        custom_array[i].restaurantId,
                                                        custom_array[i].allmodifierArray,
                                                        custom_array[i].itemId,
                                                        custom_array[i].menuGroupId,
                                                        custom_array[i].menuId,
                                                        false,
                                                        "",
                                                        ArrayList(),
                                                        custom_array[i].itemStatus,
                                                        custom_array[i].isCombo,
                                                        custom_array[i].isBogo,
                                                        custom_array[i].comboOrBogoOfferId,
                                                        custom_array[i].comboOrBogoUniqueId,
                                                        custom_array[i].voidStatus,
                                                        custom_array[i].voidQuantity,
                                                        custom_array[i].aplicableTaxRates,
                                                        custom_array[i].taxAppliedList,
                                                        custom_array[i].diningOptionTaxException,
                                                        custom_array[i].diningTaxOption,
                                                        custom_array[i].taxIncludeOption
                                                    )
                                                    custom_array[i] = customData
                                                } else {
                                                    val customData = OrdersCustomData(
                                                        custom_array[i].basePrice,
                                                        custom_array[i].discountApplicable,
                                                        custom_array[i].id,
                                                        custom_array[i].name,
                                                        custom_array[i].itemType,
                                                        custom_array[i].qty,
                                                        custom_array[i].total,
                                                        custom_array[i].discount,
                                                        custom_array[i].restaurantId,
                                                        custom_array[i].allmodifierArray,
                                                        custom_array[i].itemId,
                                                        custom_array[i].menuGroupId,
                                                        custom_array[i].menuId,
                                                        true,
                                                        DISCOUNT_LEVEL_CHECK,
                                                        checkLevelDiscountArray,
                                                        custom_array[i].itemStatus,
                                                        custom_array[i].isCombo,
                                                        custom_array[i].isBogo,
                                                        custom_array[i].comboOrBogoOfferId,
                                                        custom_array[i].comboOrBogoUniqueId,
                                                        custom_array[i].voidStatus,
                                                        custom_array[i].voidQuantity,
                                                        custom_array[i].aplicableTaxRates,
                                                        custom_array[i].taxAppliedList,
                                                        custom_array[i].diningOptionTaxException,
                                                        custom_array[i].diningTaxOption,
                                                        custom_array[i].taxIncludeOption
                                                    )
                                                    custom_array[i] = customData
                                                }
                                            }
                                        } else {
                                            val customData = OrdersCustomData(
                                                custom_array[i].basePrice,
                                                custom_array[i].discountApplicable,
                                                custom_array[i].id,
                                                custom_array[i].name,
                                                custom_array[i].itemType,
                                                custom_array[i].qty,
                                                custom_array[i].total,
                                                custom_array[i].discount,
                                                custom_array[i].restaurantId,
                                                custom_array[i].allmodifierArray,
                                                custom_array[i].itemId,
                                                custom_array[i].menuGroupId,
                                                custom_array[i].menuId,
                                                true,
                                                DISCOUNT_LEVEL_CHECK,
                                                checkLevelDiscountArray,
                                                custom_array[i].itemStatus,
                                                custom_array[i].isCombo,
                                                custom_array[i].isBogo,
                                                custom_array[i].comboOrBogoOfferId,
                                                custom_array[i].comboOrBogoUniqueId,
                                                custom_array[i].voidStatus,
                                                custom_array[i].voidQuantity,
                                                custom_array[i].aplicableTaxRates,
                                                custom_array[i].taxAppliedList,
                                                custom_array[i].diningOptionTaxException,
                                                custom_array[i].diningTaxOption,
                                                custom_array[i].taxIncludeOption
                                            )
                                            custom_array[i] = customData
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    // applying discount
                    for (i in 0 until custom_array.size) {
                        if (!custom_array[i].isCombo && !custom_array[i].isBogo) {
                            if (custom_array[i].discountApplicable) {
                                if (hasExcludeItems) {
                                    for (j in 0 until checkLevelDiscountArray[0].exceptDiscountItems.size) {
                                        if (checkLevelDiscountArray[0].exceptDiscountItems[j].id == custom_array[i].itemId) {
                                            val customData = OrdersCustomData(
                                                custom_array[i].basePrice,
                                                custom_array[i].discountApplicable,
                                                custom_array[i].id,
                                                custom_array[i].name,
                                                custom_array[i].itemType,
                                                custom_array[i].qty,
                                                custom_array[i].total,
                                                custom_array[i].discount,
                                                custom_array[i].restaurantId,
                                                custom_array[i].allmodifierArray,
                                                custom_array[i].itemId,
                                                custom_array[i].menuGroupId,
                                                custom_array[i].menuId,
                                                false,
                                                "",
                                                ArrayList(),
                                                custom_array[i].itemStatus,
                                                custom_array[i].isCombo,
                                                custom_array[i].isBogo,
                                                custom_array[i].comboOrBogoOfferId,
                                                custom_array[i].comboOrBogoUniqueId,
                                                custom_array[i].voidStatus,
                                                custom_array[i].voidQuantity,
                                                custom_array[i].aplicableTaxRates,
                                                custom_array[i].taxAppliedList,
                                                custom_array[i].diningOptionTaxException,
                                                custom_array[i].diningTaxOption,
                                                custom_array[i].taxIncludeOption
                                            )
                                            custom_array[i] = customData
                                        } else {
                                            val customData = OrdersCustomData(
                                                custom_array[i].basePrice,
                                                custom_array[i].discountApplicable,
                                                custom_array[i].id,
                                                custom_array[i].name,
                                                custom_array[i].itemType,
                                                custom_array[i].qty,
                                                custom_array[i].total,
                                                custom_array[i].discount,
                                                custom_array[i].restaurantId,
                                                custom_array[i].allmodifierArray,
                                                custom_array[i].itemId,
                                                custom_array[i].menuGroupId,
                                                custom_array[i].menuId,
                                                true,
                                                DISCOUNT_LEVEL_CHECK,
                                                checkLevelDiscountArray,
                                                custom_array[i].itemStatus,
                                                custom_array[i].isCombo,
                                                custom_array[i].isBogo,
                                                custom_array[i].comboOrBogoOfferId,
                                                custom_array[i].comboOrBogoUniqueId,
                                                custom_array[i].voidStatus,
                                                custom_array[i].voidQuantity,
                                                custom_array[i].aplicableTaxRates,
                                                custom_array[i].taxAppliedList,
                                                custom_array[i].diningOptionTaxException,
                                                custom_array[i].diningTaxOption,
                                                custom_array[i].taxIncludeOption
                                            )
                                            custom_array[i] = customData
                                        }
                                    }
                                } else {
                                    val customData = OrdersCustomData(
                                        custom_array[i].basePrice,
                                        custom_array[i].discountApplicable,
                                        custom_array[i].id,
                                        custom_array[i].name,
                                        custom_array[i].itemType,
                                        custom_array[i].qty,
                                        custom_array[i].total,
                                        custom_array[i].discount,
                                        custom_array[i].restaurantId,
                                        custom_array[i].allmodifierArray,
                                        custom_array[i].itemId,
                                        custom_array[i].menuGroupId,
                                        custom_array[i].menuId,
                                        true,
                                        DISCOUNT_LEVEL_CHECK,
                                        checkLevelDiscountArray,
                                        custom_array[i].itemStatus,
                                        custom_array[i].isCombo,
                                        custom_array[i].isBogo,
                                        custom_array[i].comboOrBogoOfferId,
                                        custom_array[i].comboOrBogoUniqueId,
                                        custom_array[i].voidStatus,
                                        custom_array[i].voidQuantity,
                                        custom_array[i].aplicableTaxRates,
                                        custom_array[i].taxAppliedList,
                                        custom_array[i].diningOptionTaxException,
                                        custom_array[i].diningTaxOption,
                                        custom_array[i].taxIncludeOption
                                    )
                                    custom_array[i] = customData
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun customizeOrderDetails(orderDetails: OrderDetailsResponse, from: String) {
        try {
            sessionManager.saveTempOrderList("")
            custom_array.clear()
            mOrderId = orderDetails.id!!
            order_number = orderDetails.orderNumber!!
            dinningId = orderDetails.dineInOption!!
            dinningName = orderDetails.dineInOptionName!!
            dinningOptionBehaviour = orderDetails.dineInBehaviour!!
            tableNumber = orderDetails.tableNumber.toString()
            checkNumber = orderDetails.checkNumber!!
            final_tip_amount = orderDetails.tipAmount!!
            isHavingCustomer = orderDetails.haveCustomer!!
            if (dinningId != "") {
                when (dinningOptionBehaviour) {
                    "delivery" -> {
                        isDeliverySelected = true
                    }
                    "dine_in" -> {
                        isDineInSelected = true
                    }
                    "take_out" -> {
                        isTakeOutSelected = true
                    }
                    "curbside" -> {
                        isCurbSideSelected = true
                    }
                    else -> {
                    }
                }
                tv_dine_in_option_id.background = ContextCompat.getDrawable(
                    this@OrderDetailsActivity,
                    R.drawable.graditent_bg
                )
            }
            if (isHavingCustomer) {
                val customer_details = orderDetails.customerDetails!!
                customerFirstName = customer_details.firstName!!
                customerLastName = customer_details.lastName!!
                customerId = customer_details.id!!
                customerEmail = customer_details.email!!
                customerPhone = customer_details.phoneNumber!!
            } else {
                customerId = ""
                customerFirstName = ""
                customerLastName = ""
                customerEmail = ""
                customerPhone = ""
            }
            val orderUpdateDetails = if (from == "table") {
                orderDetails.itemsDetails!!
            } else {
                orderDetails.orderItems!!
            }
            for (i in 0 until orderUpdateDetails.size) {
                val allmodifierArray = ArrayList<GetAllModifierListResponseList>()
                for (j in 0 until orderUpdateDetails[i].modifiersList!!.size) {
                    val modifierGroupIdListarray = ArrayList<modifierIdList>()
                    val modListIds = modifierIdList()
                    modListIds.id =
                        orderUpdateDetails[i].modifiersList!![j].modifierGroupId.toString()
                    modListIds.name = ""
                    modifierGroupIdListarray.add(modListIds)
                    val totalPrice = if (orderUpdateDetails[i].quantity!! == 0) {
                        orderUpdateDetails[i].modifiersList!![j].modifierTotalPrice!!
                    } else {
                        orderUpdateDetails[i].modifiersList!![j].modifierTotalPrice!! * orderUpdateDetails[i].quantity!!
                    }
                    val modifiersList = GetAllModifierListResponseList(
                        orderUpdateDetails[i].modifiersList!![j].modifierId.toString(),
                        orderUpdateDetails[i].modifiersList!![j].modifierName.toString(),
                        orderUpdateDetails[i].modifiersList!![j].modifierTotalPrice!!,
                        totalPrice,
                        modifierGroupIdListarray
                    )
                    allmodifierArray.add(modifiersList)
                }
                for (k in 0 until orderUpdateDetails[i].specialRequestList!!.size) {
                    val modifierGroupIdListarray = ArrayList<modifierIdList>()
                    val modListIds = modifierIdList()
                    modListIds.id = ""
                    modListIds.name = ""
                    modifierGroupIdListarray.add(modListIds)
                    val reqPrice = if (orderUpdateDetails[i].quantity!! == 0) {
                        orderUpdateDetails[i].specialRequestList!![k].requestPrice!!
                    } else {
                        orderUpdateDetails[i].specialRequestList!![k].requestPrice!! / orderUpdateDetails[i].quantity!!
                    }
                    val specialRequestList = GetAllModifierListResponseList(
                        "",
                        orderUpdateDetails[i].specialRequestList!![k].name.toString(),
                        reqPrice,
                        orderUpdateDetails[i].specialRequestList!![k].requestPrice!!,
                        modifierGroupIdListarray
                    )
                    allmodifierArray.add(specialRequestList)
                }
                var discountValue = 0.00
                if (orderUpdateDetails[i].discountType!! == "percentage") {
                    discountValue =
                        if (orderUpdateDetails[i].discountAmount!!.toDouble() == 0.00 && orderUpdateDetails[i].unitPrice!!.toDouble() == 0.00) {
                            0.00
                        } else {
                            (orderUpdateDetails[i].discountAmount!!.toDouble() / (orderUpdateDetails[i].unitPrice!!.toDouble() * orderUpdateDetails[i].quantity!!.toDouble())) * 100
                        }
                } else if (orderUpdateDetails[i].discountType!! == "currency") {
                    discountValue = orderUpdateDetails[i].discountAmount!!
                }
                val discountsArray = ArrayList<GetAllDiscountsListResponse>()
                val discountApplied: Boolean = orderUpdateDetails[i].discountType!! != ""
                discountApplicableItem = orderUpdateDetails[i].discountType!! != ""
                val discountAppliedType = orderUpdateDetails[i].discountLevel!!
                if (discountAppliedType == "") {
                    val discountObject = GetAllDiscountsListResponse(
                        true,
                        0,
                        orderUpdateDetails[i].discountValueType.toString(),
                        ArrayList(),
                        "",
                        ArrayList(),
                        0.00,
                        0.00,
                        "",
                        "0",
                        0,
                        "",
                        "",
                        0.00
                    )
                    discountsArray.add(discountObject)
                } else {
                    val exceptDiscountItemsList: ArrayList<DiscountItems> =
                        if (orderUpdateDetails[i].exceptDiscountItems.toString() == "null") {
                            ArrayList()
                        } else {
                            orderUpdateDetails[i].exceptDiscountItems!!
                        }
                    val includeDiscountItemsList: ArrayList<DiscountItems> =
                        if (orderUpdateDetails[i].includeDiscountItems.toString() == "null") {
                            ArrayList()
                        } else {
                            orderUpdateDetails[i].includeDiscountItems!!
                        }
                    val maxDiscountAmount: Double =
                        if (orderUpdateDetails[i].maxDiscountAmount.toString() == "null") {
                            0.00
                        } else {
                            orderUpdateDetails[i].maxDiscountAmount!!
                        }
                    val minDiscountAmount: Double =
                        if (orderUpdateDetails[i].minDiscountAmount.toString() == "null") {
                            0.00
                        } else {
                            orderUpdateDetails[i].minDiscountAmount!!
                        }
                    if (discountAppliedType == DISCOUNT_LEVEL_CHECK) {
                        val discountObject = GetAllDiscountsListResponse(
                            true,
                            0,
                            orderUpdateDetails[i].discountValueType.toString(),
                            exceptDiscountItemsList,
                            "",
                            includeDiscountItemsList,
                            maxDiscountAmount,
                            minDiscountAmount,
                            orderUpdateDetails[i].discountName!!,
                            orderUpdateDetails[i].discountValue!!.toString(),
                            0,
                            "",
                            "",
                            discountValue
                        )
                        discountsArray.add(discountObject)
                        if (!orderUpdateDetails[i].isCombo!! && !orderUpdateDetails[i].isBogo!!) {
                            checkLevelDiscountArray.clear()
                            checkLevelDiscountArray.add(discountObject)
                        }
                    } else if (discountAppliedType == DISCOUNT_LEVEL_ITEM) {
                        val discountObject = GetAllDiscountsListResponse(
                            true,
                            0,
                            orderUpdateDetails[i].discountValueType!!,
                            exceptDiscountItemsList,
                            "",
                            includeDiscountItemsList,
                            orderUpdateDetails[i].maxDiscountAmount!!,
                            orderUpdateDetails[i].minDiscountAmount!!,
                            orderUpdateDetails[i].discountName!!,
                            orderUpdateDetails[i].discountValue!!.toString(),
                            0,
                            "",
                            "",
                            discountValue
                        )
                        discountsArray.add(discountObject)
                    }
                }
                when {
                    orderUpdateDetails[i].isCombo!! -> {
                        comboBogoId = orderUpdateDetails[i].comboId!!
                        comboBogoUniqueId = orderUpdateDetails[i].comboUniqueId.toString()
                    }
                    orderUpdateDetails[i].isBogo!! -> {
                        comboBogoId = orderUpdateDetails[i].bogoId!!
                        comboBogoUniqueId = orderUpdateDetails[i].bogoUniqueId.toString()
                    }
                    else -> {
                        comboBogoId = ""
                        comboBogoUniqueId = ""
                    }
                }
                val aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>
                val taxAppliedList: ArrayList<SendTaxRates>
                if (orderUpdateDetails[i].taxIncludeOption!!) {
                    aplicableTaxRates =
                        if (orderUpdateDetails[i].taxesList!!.toString() == "null") {
                            ArrayList()
                        } else {
                            orderUpdateDetails[i].taxesList!!
                        }
                } else {
                    aplicableTaxRates = ArrayList()
                }
                taxAppliedList = if (aplicableTaxRates.size > 0) {
                    taxCalculationNew(
                        orderUpdateDetails[i].quantity!!.toDouble(),
                        orderUpdateDetails[i].unitPrice!!.toDouble(),
                        aplicableTaxRates
                    )
                } else {
                    ArrayList()
                }
                val ordersCustomData = OrdersCustomData(
                    orderUpdateDetails[i].unitPrice.toString(),
                    orderUpdateDetails[i].discountApplicable,
                    orderUpdateDetails[i].id.toString(),
                    orderUpdateDetails[i].itemName.toString(),
                    orderUpdateDetails[i].itemType.toString(),
                    orderUpdateDetails[i].quantity.toString(),
                    orderUpdateDetails[i].totalPrice.toString(),
                    orderUpdateDetails[i].discountValue.toString(),
                    orderDetails.restaurantId.toString(),
                    allmodifierArray,
                    orderUpdateDetails[i].itemId.toString(),
                    "",
                    "",
                    discountApplied,
                    discountAppliedType,
                    discountsArray,
                    orderUpdateDetails[i].itemStatus!!,
                    orderUpdateDetails[i].isCombo!!,
                    orderUpdateDetails[i].isBogo!!,
                    comboBogoId,
                    comboBogoUniqueId,
                    orderUpdateDetails[i].voidStatus!!,
                    orderUpdateDetails[i].voidQuantity!!,
                    aplicableTaxRates,
                    taxAppliedList,
                    orderUpdateDetails[i].diningOptionTaxException!!,
                    orderUpdateDetails[i].diningTaxOption!!,
                    orderUpdateDetails[i].taxIncludeOption!!
                )
                custom_array.add(ordersCustomData)
            }
            fullOrderSetUp()
            pay_type = "pay"
            update_order = "update"
            noOfGuests = orderDetails.guestCount.toString()
            if (noOfGuests == "" || noOfGuests == "null") {
                tv_edit_guests.text = getString(R.string.guests)
            } else {
                tv_edit_guests.text = noOfGuests + " " + getString(R.string.guests)
            }
            tv_customer_name_id.text = "$customerFirstName $customerLastName"
            tv_order_no_id.text = "Order #$order_number"
            tv_table_no.text = "Table No: $tableNumber"
            tv_check_table_dinein_option.text = "#$checkNumber, Table $tableNumber, $dinningName"
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showCombosDialog(title: String, comboItemTypeList: ArrayList<ComboBogoItemType>) {
        val combos_dialog = Dialog(this)
        combos_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.combos_dialog, null)
        combos_dialog.setContentView(view)
        combos_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        combos_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        combos_dialog.setCanceledOnTouchOutside(false)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(combos_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        combos_dialog.window!!.attributes = layoutParams
        val tv_header_title = view.findViewById(R.id.tv_header_title) as TextView
        val tv_add = view.findViewById(R.id.tv_add) as TextView
        val tv_close = view.findViewById(R.id.tv_close) as TextView
        rv_combo_items = view.findViewById(R.id.rv_combo_items) as RecyclerView
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_combo_items.layoutManager = layoutManager
        rv_combo_items.hasFixedSize()
        if (comboItemTypeList.size > 0) {
            comboMenuListAdapter =
                ComboMenuListAdapter(
                    this@OrderDetailsActivity,
                    comboItemTypeList,
                    1
                )
            rv_combo_items.adapter = comboMenuListAdapter
            comboMenuListAdapter.notifyDataSetChanged()
        }
        for (i in 0 until comboItemTypeList.size) {
            if (comboItemTypeList[i].menu_type == "Menu Group") {
                val obj = JSONObject()
                obj.put("menuGroupId", comboItemTypeList[i].id)
                obj.put("restaurantId", restaurantId)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                getComboDiscountsMenuGroup(
                    jObject,
                    comboItemTypeList[i].id,
                    comboItemTypeList[i].quantity
                )
            }
        }
        tv_header_title.text = title
        tv_close.setOnClickListener {
            combos_dialog.dismiss()
        }
        tv_add.setOnClickListener {
            Log.d("combo_size", comboMenuListAdapter.getSelected().size.toString())
            val list = comboMenuListAdapter.getSelected()
            if (list.size > 0 && list.size == comboItemTypeList.size) {
                for (i in 0 until list.size)
                    if (i == 0) {
                        val orderCustomData = OrdersCustomData(
                            comboPrice.toString(),
                            list[i].item.discountApplicable,
                            list[i].item.itemId,
                            list[i].name + " " + list[i].sizeName,
                            list[i].item.itemType,
                            list[i].item.quantity.toString(),
                            list[i].item.totalPrice.toString(),
                            "",
                            restaurantId,
                            ArrayList(),
                            list[i].item.itemId,
                            list[i].item.menuGroupId,
                            list[i].item.menuId,
                            list[i].item.discountApplicable,
                            list[i].item.discountType,
                            ArrayList(),
                            0,
                            true,
                            false,
                            comboBogoId,
                            comboBogoUniqueId,
                            false,
                            0,
                            list[i].item.taxesList,
                            ArrayList(),
                            list[i].item.diningOptionTaxException,
                            list[i].item.diningTaxOption,
                            list[i].item.taxIncludeOption
                        )
                        custom_array.add(orderCustomData)
                    } else {
                        val discountArray = GetAllDiscountsListResponse(
                            true,
                            0,
                            "percentage",
                            ArrayList(),
                            discountModeId,
                            ArrayList(),
                            0.00,
                            0.00,
                            discountModeType,
                            "100",
                            1,
                            "fixed",
                            comboBogoId,
                            100.00
                        )
                        val discountsArray = ArrayList<GetAllDiscountsListResponse>()
                        discountsArray.add(discountArray)
                        val orderCustomData = OrdersCustomData(
                            /*list[i].item.basePrice.toString()*/"0.00",
                            list[i].item.discountApplicable,
                            list[i].item.itemId,
                            list[i].name + " " + list[i].sizeName,
                            list[i].item.itemType,
                            list[i].item.quantity.toString(),
                            list[i].item.totalPrice.toString(),
                            "",
                            restaurantId,
                            ArrayList(),
                            list[i].item.itemId,
                            list[i].item.menuGroupId,
                            list[i].item.menuId,
                            true,
                            DISCOUNT_LEVEL_CHECK,
                            discountsArray,
                            0,
                            true,
                            false,
                            comboBogoId,
                            comboBogoUniqueId,
                            false,
                            0,
                            list[i].item.taxesList,
                            ArrayList(),
                            list[i].item.diningOptionTaxException,
                            list[i].item.diningTaxOption,
                            list[i].item.taxIncludeOption
                        )
                        custom_array.add(orderCustomData)
                    }
                handleDiscounts()
                fullOrderSetUp()
                combos_dialog.dismiss()
            } else {
                Toast.makeText(this, "Please select items", Toast.LENGTH_SHORT).show()
            }
        }
        combos_dialog.show()
    }

    private fun showBogoBuyDialog(bogoItemTypeList: ArrayList<ComboBogoItemType>) {
        isBogoBuySelected = false
        val bogos_dialog = Dialog(this)
        bogos_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.bogo_buy_dialog, null)
        bogos_dialog.setContentView(view)
        bogos_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        bogos_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        bogos_dialog.setCanceledOnTouchOutside(false)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(bogos_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        bogos_dialog.window!!.attributes = layoutParams
        val tv_header_title = view.findViewById(R.id.tv_header_title) as TextView
        val tv_add = view.findViewById(R.id.tv_add) as TextView
        val tv_close = view.findViewById(R.id.tv_close) as TextView
        rv_bogo_buy_items = view.findViewById(R.id.rv_bogo_buy_items) as RecyclerView
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_bogo_buy_items.layoutManager = layoutManager
        rv_bogo_buy_items.hasFixedSize()
        rv_bogo_buy_items.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                hideKeyboard(view)
            }
        })
        if (bogoItemTypeList.size > 0) {
            bogoBuyMenuListAdapter =
                BogoMenuListAdapter(
                    this@OrderDetailsActivity,
                    bogoGetItemTypeList,
                    1
                )
            rv_bogo_buy_items.adapter = bogoBuyMenuListAdapter
            bogoBuyMenuListAdapter.notifyDataSetChanged()
        }
        for (i in 0 until bogoItemTypeList.size) {
            if (bogoItemTypeList[i].menu_type == "Menu Group") {
                val obj = JSONObject()
                obj.put("menuGroupId", bogoItemTypeList[i].id)
                obj.put("restaurantId", restaurantId)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                getBogoDiscountsBuyMenuGroup(
                    jObject,
                    bogoItemTypeList[i].id,
                    bogoItemTypeList[i].quantity
                )
            }
        }
        tv_header_title.text = "Buy Items"
        tv_close.setOnClickListener {
            isBogoBuySelected = false
            bogos_dialog.dismiss()
        }
        tv_add.setOnClickListener {
            Log.d("bogo_get_size", bogoBuyMenuListAdapter.getSelected().size.toString())
            if (bogoBuyMenuListAdapter.getSelected().size > 0 && bogoBuyMenuListAdapter.getSelected().size == bogoItemTypeList.size) {
                isBogoBuySelected = true
                bogos_dialog.dismiss()
            } else {
                isBogoBuySelected = false
                Toast.makeText(this, "Please select items", Toast.LENGTH_SHORT).show()
            }
        }
        bogos_dialog.setOnCancelListener { isBogoBuySelected = false }
        bogos_dialog.show()
    }

    private fun showBogoGetDialog(bogoItemTypeList: ArrayList<ComboBogoItemType>) {
        val bogos_dialog = Dialog(this)
        bogos_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@OrderDetailsActivity)
                .inflate(R.layout.bogo_get_dialog, null)
        bogos_dialog.setContentView(view)
        bogos_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        bogos_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        bogos_dialog.setCanceledOnTouchOutside(false)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(bogos_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        bogos_dialog.window!!.attributes = layoutParams
        val tv_header_title = view.findViewById(R.id.tv_header_title) as TextView
        val tv_add = view.findViewById(R.id.tv_add) as TextView
        val tv_close = view.findViewById(R.id.tv_close) as TextView
        rv_bogo_get_items = view.findViewById(R.id.rv_bogo_get_items) as RecyclerView
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_bogo_get_items.layoutManager = layoutManager
        rv_bogo_get_items.hasFixedSize()
        rv_bogo_get_items.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                hideKeyboard(view)
            }
        })
        if (bogoItemTypeList.size > 0) {
            bogoGetMenuListAdapter =
                BogoMenuListAdapter(
                    this@OrderDetailsActivity,
                    bogoGetItemTypeList,
                    1
                )
            rv_bogo_get_items.adapter = bogoGetMenuListAdapter
            bogoGetMenuListAdapter.notifyDataSetChanged()
        }
        for (i in 0 until bogoItemTypeList.size) {
            if (bogoItemTypeList[i].menu_type == "Menu Group") {
                val obj = JSONObject()
                obj.put("menuGroupId", bogoItemTypeList[i].id)
                obj.put("restaurantId", restaurantId)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                getBogoDiscountsGetMenuGroup(
                    jObject,
                    bogoItemTypeList[i].id,
                    bogoItemTypeList[i].quantity
                )
            }
        }
        tv_header_title.text = "Get Items"
        tv_close.setOnClickListener {
            bogos_dialog.dismiss()
        }
        tv_add.setOnClickListener {
            if (isBogoBuySelected) {
                val custom_array_buy_items = ArrayList<OrdersCustomData>()
                Log.d("bogo_get_size", bogoGetMenuListAdapter.getSelected().size.toString())
                if (bogoBuyMenuListAdapter.getSelected().size > 0) {
                    val list = bogoBuyMenuListAdapter.getSelected()
                    for (i in 0 until list.size) {
                        val taxAppliedList: ArrayList<SendTaxRates>
                        val aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>
                        if (list[i].item.taxIncludeOption) {
                            aplicableTaxRates = list[i].item.taxesList
                            taxAppliedList = taxCalculationNew(
                                1.toDouble(),
                                list[i].basePrice,
                                list[i].item.taxesList
                            )
                        } else {
                            taxAppliedList = ArrayList()
                            aplicableTaxRates = ArrayList()
                        }
                        val orderCustomData = OrdersCustomData(
                            list[i].basePrice.toString(),
                            list[i].item.discountApplicable,
                            list[i].item.id,
                            list[i].item.name + " " + list[i].sizeName,
                            "normal",
                            "1",
                            (1 * list[i].basePrice).toString(),
                            "",
                            restaurantId,
                            ArrayList(),
                            list[i].item.itemId,
                            list[i].item.menuGroupId,
                            list[i].item.menuId,
                            list[i].item.discountApplicable,
                            list[i].item.discountType,
                            ArrayList(),
                            0,
                            false,
                            true,
                            comboBogoId,
                            comboBogoUniqueId,
                            false,
                            0,
                            aplicableTaxRates,
                            taxAppliedList,
                            list[i].item.diningOptionTaxException,
                            list[i].item.diningTaxOption,
                            list[i].item.taxIncludeOption
                        )
                        custom_array_buy_items.add(orderCustomData)
                    }
                }
                if (bogoGetMenuListAdapter.getSelected().size > 0) {
                    custom_array.addAll(custom_array_buy_items)
                    val list = bogoGetMenuListAdapter.getSelected()
                    for (i in 0 until list.size) {
                        val taxAppliedList: ArrayList<SendTaxRates>
                        val aplicableTaxRates: ArrayList<TaxRatesDetailsResponse>
                        if (list[i].item.taxIncludeOption) {
                            aplicableTaxRates = list[i].item.taxesList
                            taxAppliedList = taxCalculationNew(
                                1.toDouble(),
                                list[i].basePrice,
                                list[i].item.taxesList
                            )
                        } else {
                            taxAppliedList = ArrayList()
                            aplicableTaxRates = ArrayList()
                        }
                        val discountArray = GetAllDiscountsListResponse(
                            true,
                            0,
                            bogoGetDiscountValueType,
                            ArrayList(),
                            discountModeId,
                            ArrayList(),
                            maxDiscountAmount,
                            minDiscountAmount,
                            discountModeType,
                            bogoGetDiscountValue.toString(),
                            1,
                            "fixed",
                            comboBogoId,
                            bogoGetDiscountValue.toDouble()
                        )
                        val discountsArray = ArrayList<GetAllDiscountsListResponse>()
                        discountsArray.add(discountArray)
                        val orderCustomData = OrdersCustomData(
                            list[i].basePrice.toString(),
                            list[i].item.discountApplicable,
                            list[i].item.id,
                            list[i].item.name + " " + list[i].sizeName,
                            "normal",
                            "1",
                            (1 * list[i].basePrice).toString(),
                            "",
                            restaurantId,
                            list[i].item.modifiersList,
                            list[i].item.itemId,
                            list[i].item.menuGroupId,
                            list[i].item.menuId,
                            true,
                            DISCOUNT_LEVEL_ITEM,
                            discountsArray,
                            0,
                            false,
                            true,
                            comboBogoId,
                            comboBogoUniqueId,
                            false,
                            0,
                            aplicableTaxRates,
                            taxAppliedList,
                            list[i].item.diningOptionTaxException,
                            list[i].item.diningTaxOption,
                            list[i].item.taxIncludeOption
                        )
                        custom_array.add(orderCustomData)
                    }
                }
                handleDiscounts()
                fullOrderSetUp()
                bogoBuyItemTypeList.clear()
                bogoGetItemTypeList.clear()
                isBogoBuySelected = false
            } else {
                Toast.makeText(this, "Please select any buy item", Toast.LENGTH_SHORT).show()
            }
            bogos_dialog.dismiss()
        }
        bogos_dialog.show()
    }

    private fun getComboDiscountsMenuGroup(json_obj: JsonObject, id: String, quantity: Int) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDiscountMenuGroupApi(json_obj)
        call.enqueue(object : Callback<DiscountMenuItemsModel> {
            override fun onFailure(call: Call<DiscountMenuItemsModel>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<DiscountMenuItemsModel>,
                response: Response<DiscountMenuItemsModel>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        val menuItems = resp.menuItems
                        val comboCustomList: ArrayList<ComboBogoCustomPojo> = ArrayList()
                        if (menuItems.size > 0) {
                            for (i in 0 until menuItems.size) {
                                if (menuItems[i].itemId != null) {
                                    val timePriceList =
                                        if (menuItems[i].timePriceList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].timePriceList!!
                                        }
                                    val sizeList =
                                        if (menuItems[i].sizeList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].sizeList!!
                                        }
                                    val menuPriceList =
                                        if (menuItems[i].menuPriceList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].menuPriceList!!
                                        }
//                                    don't apply tax
                                    val taxesList: ArrayList<TaxRatesDetailsResponse> =
//                                        if (menuItems[i].taxesList.toString() == "null") {
                                        ArrayList()
//                                        } else {
//                                            menuItems[i].taxesList!!
//                                        }
                                    val specialRequestList =
                                        if (menuItems[i].specialRequestList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].specialRequestList!!
                                        }
                                    val comboCustomPojo = ComboBogoCustomPojo(
                                        comboBogoId,
                                        "combo",
                                        menuItems[i].basePrice,
                                        menuItems[i].discountAmount,
                                        menuItems[i].discountId,
                                        menuItems[i].discountName,
                                        menuItems[i].discountType,
                                        menuItems[i].discountValue,
                                        menuItems[i].itemId!!,
                                        menuItems[i].isBogo,
                                        menuItems[i].isCombo,
                                        menuItems[i].discountApplicable,
                                        menuItems[i].itemId!!,
                                        menuItems[i].itemName,
                                        menuItems[i].itemType,
                                        menuItems[i].menuGroupId,
                                        menuItems[i].menuId,
                                        menuItems[i].modifiersList!!,
                                        menuItems[i].itemName,
                                        menuItems[i].pricingStrategy,
                                        menuItems[i].quantity,
                                        specialRequestList,
                                        timePriceList,
                                        sizeList,
                                        menuPriceList,
                                        menuItems[i].totalPrice,
                                        menuItems[i].type,
                                        "",
                                        menuItems[i].unitPrice,
                                        taxesList,
                                        menuItems[i].diningOptionTaxException,
                                        menuItems[i].diningTaxOption,
                                        menuItems[i].taxIncludeOption
                                    )
                                    comboCustomList.add(comboCustomPojo)
                                }
                            }
                        }
                        for (j in 0 until comboItemTypeList.size) {
                            if (comboItemTypeList[j].id == id) {
                                val comboBogoItemType = ComboBogoItemType(
                                    comboItemTypeList[j].id,
                                    comboItemTypeList[j].name,
                                    comboItemTypeList[j].menu_type,
                                    comboItemTypeList[j].quantity,
                                    comboCustomList
                                )
                                comboItemTypeList[j] = comboBogoItemType
                            }
                        }
                        comboMenuListAdapter =
                            ComboMenuListAdapter(
                                this@OrderDetailsActivity,
                                comboItemTypeList,
                                quantity
                            )
                        rv_combo_items.adapter = comboMenuListAdapter
                        comboMenuListAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getBogoDiscountsBuyMenuGroup(json_obj: JsonObject, id: String, quantity: Int) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDiscountMenuGroupApi(json_obj)
        call.enqueue(object : Callback<DiscountMenuItemsModel> {
            override fun onFailure(call: Call<DiscountMenuItemsModel>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<DiscountMenuItemsModel>,
                response: Response<DiscountMenuItemsModel>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        val menuItems = resp.menuItems
                        val bogoCustomList: ArrayList<ComboBogoCustomPojo> = ArrayList()
                        if (menuItems.size > 0) {
                            for (i in 0 until menuItems.size) {
                                if (menuItems[i].itemId != null) {
                                    val timePriceList =
                                        if (menuItems[i].timePriceList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].timePriceList!!
                                        }
                                    val sizeList =
                                        if (menuItems[i].sizeList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].sizeList!!
                                        }
                                    val menuPriceList =
                                        if (menuItems[i].menuPriceList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].menuPriceList!!
                                        }
                                    val taxesList: ArrayList<TaxRatesDetailsResponse> =
                                        if (menuItems[i].taxesList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].taxesList!!
                                        }
                                    val specialRequestList =
                                        if (menuItems[i].specialRequestList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].specialRequestList!!
                                        }
                                    val bogoCustomPojo = ComboBogoCustomPojo(
                                        comboBogoId,
                                        "bogo",
                                        menuItems[i].basePrice,
                                        menuItems[i].discountAmount,
                                        menuItems[i].discountId,
                                        menuItems[i].discountName,
                                        menuItems[i].discountType,
                                        menuItems[i].discountValue,
                                        menuItems[i].itemId!!,
                                        menuItems[i].isBogo,
                                        menuItems[i].isCombo,
                                        menuItems[i].discountApplicable,
                                        menuItems[i].itemId!!,
                                        menuItems[i].itemName,
                                        menuItems[i].itemType,
                                        menuItems[i].menuGroupId,
                                        menuItems[i].menuId,
                                        menuItems[i].modifiersList!!,
                                        menuItems[i].itemName,
                                        menuItems[i].pricingStrategy,
                                        menuItems[i].quantity,
                                        specialRequestList,
                                        timePriceList,
                                        sizeList,
                                        menuPriceList,
                                        menuItems[i].totalPrice,
                                        menuItems[i].type,
                                        "",
                                        menuItems[i].unitPrice,
                                        taxesList,
                                        menuItems[i].diningOptionTaxException,
                                        menuItems[i].diningTaxOption,
                                        menuItems[i].taxIncludeOption
                                    )
                                    bogoCustomList.add(bogoCustomPojo)
                                }
                            }
                        }
                        for (j in 0 until bogoBuyItemTypeList.size) {
                            if (bogoBuyItemTypeList[j].id == id) {
                                val bogoItemType = ComboBogoItemType(
                                    bogoBuyItemTypeList[j].id,
                                    bogoBuyItemTypeList[j].name,
                                    bogoBuyItemTypeList[j].menu_type,
                                    bogoBuyItemTypeList[j].quantity,
                                    bogoCustomList
                                )
                                bogoBuyItemTypeList[j] = bogoItemType
                            }
                        }
                        bogoBuyMenuListAdapter =
                            BogoMenuListAdapter(
                                this@OrderDetailsActivity,
                                bogoBuyItemTypeList,
                                quantity
                            )
                        rv_bogo_buy_items.adapter = bogoBuyMenuListAdapter
                        bogoBuyMenuListAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getBogoDiscountsGetMenuGroup(json_obj: JsonObject, id: String, quantity: Int) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDiscountMenuGroupApi(json_obj)
        call.enqueue(object : Callback<DiscountMenuItemsModel> {
            override fun onFailure(call: Call<DiscountMenuItemsModel>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Toast.makeText(
                        this@OrderDetailsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<DiscountMenuItemsModel>,
                response: Response<DiscountMenuItemsModel>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        val menuItems = resp.menuItems
                        val bogoCustomList: ArrayList<ComboBogoCustomPojo> = ArrayList()
                        if (menuItems.size > 0) {
                            for (i in 0 until menuItems.size) {
                                if (menuItems[i].itemId != null) {
                                    val timePriceList =
                                        if (menuItems[i].timePriceList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].timePriceList!!
                                        }
                                    val sizeList =
                                        if (menuItems[i].sizeList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].sizeList!!
                                        }
                                    val menuPriceList =
                                        if (menuItems[i].menuPriceList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].menuPriceList!!
                                        }
                                    val taxesList: ArrayList<TaxRatesDetailsResponse> =
                                        if (menuItems[i].taxesList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].taxesList!!
                                        }
                                    val specialRequestList =
                                        if (menuItems[i].specialRequestList.toString() == "null") {
                                            ArrayList()
                                        } else {
                                            menuItems[i].specialRequestList!!
                                        }
                                    val bogoCustomPojo = ComboBogoCustomPojo(
                                        comboBogoId,
                                        "bogo",
                                        menuItems[i].basePrice,
                                        menuItems[i].discountAmount,
                                        menuItems[i].discountId,
                                        menuItems[i].discountName,
                                        menuItems[i].discountType,
                                        menuItems[i].discountValue,
                                        menuItems[i].itemId!!,
                                        menuItems[i].isBogo,
                                        menuItems[i].isCombo,
                                        menuItems[i].discountApplicable,
                                        menuItems[i].itemId!!,
                                        menuItems[i].itemName,
                                        menuItems[i].itemType,
                                        menuItems[i].menuGroupId,
                                        menuItems[i].menuId,
                                        menuItems[i].modifiersList!!,
                                        menuItems[i].itemName,
                                        menuItems[i].pricingStrategy,
                                        menuItems[i].quantity,
                                        specialRequestList,
                                        timePriceList,
                                        sizeList,
                                        menuPriceList,
                                        menuItems[i].totalPrice,
                                        menuItems[i].type,
                                        "",
                                        menuItems[i].unitPrice,
                                        taxesList,
                                        menuItems[i].diningOptionTaxException,
                                        menuItems[i].diningTaxOption,
                                        menuItems[i].taxIncludeOption
                                    )
                                    bogoCustomList.add(bogoCustomPojo)
                                }
                            }
                        }
                        for (j in 0 until bogoGetItemTypeList.size) {
                            if (bogoGetItemTypeList[j].id == id) {
                                val bogoItemType = ComboBogoItemType(
                                    bogoGetItemTypeList[j].id,
                                    bogoGetItemTypeList[j].name,
                                    bogoGetItemTypeList[j].menu_type,
                                    bogoGetItemTypeList[j].quantity,
                                    bogoCustomList
                                )
                                bogoGetItemTypeList[j] = bogoItemType
                            }
                        }
                        bogoGetMenuListAdapter =
                            BogoMenuListAdapter(
                                this@OrderDetailsActivity,
                                bogoGetItemTypeList,
                                quantity
                            )
                        rv_bogo_get_items.adapter = bogoGetMenuListAdapter
                        bogoGetMenuListAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(
                            this@OrderDetailsActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun generateRandomId(comboOrBogo: String): String {
        val id: String
        // creating 4 digit random number for unique id
        val randomId = Random().nextInt(10000)
        // random combo/bogo unique id with 4 digits
        id = comboOrBogo + "_" + String.format("%04d", randomId)
        return id
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onBackPressed() {
        when (from_screen) {
            "home" -> {
                if (isStayOrderSelected) {
                    sessionManager.saveTempOrderList("")
                }
                this.finish()
            }
            "payment_terminal" -> {
                sessionManager.saveTempOrderList("")
                this.finish()
            }
            "split_screen" -> {
                this.finish()
            }
            "table_service" -> {
                sessionManager.saveTempOrderList("")
                val intent = Intent(this@OrderDetailsActivity, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            }
        }
    }
}