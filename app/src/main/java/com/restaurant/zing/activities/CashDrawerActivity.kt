package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.CashTransactionsListAdapter
import com.restaurant.zing.adapters.ServerListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.abs

@SuppressLint("SetTextI18n")
class CashDrawerActivity : AppCompatActivity() {

    private lateinit var tabLayout: TabLayout
    private lateinit var activeTab: TextView
    private lateinit var openTab: TextView
    private lateinit var closedTab: TextView
    private lateinit var activeTabHeader: LinearLayout
    private lateinit var openTabHeader: LinearLayout
    private lateinit var closedTabHeader: LinearLayout
    private lateinit var ll_active_drawer: LinearLayout
    private lateinit var ll_open_drawer: LinearLayout
    private lateinit var ll_closed_drawer: LinearLayout
    private lateinit var iv_back: ImageView
    private lateinit var ll_cash_drawer_report: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var ll_cash_drawer_main: LinearLayout
    private lateinit var cv_active: CardView
    private lateinit var rv_active_cash_drawer: RecyclerView
    private lateinit var tv_active_cash_empty: TextView
    private lateinit var cv_open: CardView
    private lateinit var rv_open_cash_drawer: RecyclerView
    private lateinit var tv_open_cash_empty: TextView
    private lateinit var cv_closed: CardView
    private lateinit var rv_closed_cash_drawer: RecyclerView
    private lateinit var tv_closed_cash_empty: TextView
    private lateinit var ll_cash_drawer_detailed: LinearLayout
    private lateinit var tv_cash_drawer_title: TextView
    private lateinit var tv_starting_balance: TextView
    private lateinit var tv_balance: TextView
    private lateinit var rv_cash_drawer_details: RecyclerView
    private lateinit var tv_no_sale: TextView
    private lateinit var tv_cash_in: TextView
    private lateinit var tv_cash_out: TextView
    private lateinit var tv_cash_drop: TextView
    private lateinit var tv_close_drawer: TextView
    private lateinit var tv_open_drawer: TextView
    private lateinit var tv_adjust_starting_balance: TextView
    private lateinit var tv_currency_amount_header: TextView
    private lateinit var iv_cash_drawer_lock: ImageView
    private lateinit var tv_close_out_balance: TextView
    private lateinit var ll_close_out: LinearLayout
    private lateinit var ll_closed: LinearLayout
    private lateinit var dialog_cashin: Dialog
    private lateinit var dialog_cashout: Dialog
    private lateinit var dialog_no_sales: Dialog
    private lateinit var dialog_cash_drop: Dialog
    private lateinit var dialog_close_drawer: Dialog
    private lateinit var dialog_adjust_balance: Dialog
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""

    private var currencyType = ""
    private var cashDrawerId = ""
    private lateinit var serversList: ArrayList<ServersDataResponse>
    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private var str_dot = "."
    private var str_double_zero = "00"
    private var balance = 0.00
    private var isCashDrawerClosed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cash_drawer)

        initialize()

        tv_currency_amount_header.text = getString(R.string.amount) + " ($currencyType)"
        val obj = JSONObject()
        obj.put("restaurantId", restaurantId)
        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
        getAllCashDrawers(finalObject = jObject)
        val jObj = JSONObject()
        jObj.put("restaurantId", restaurantId)
        jObj.put("employeeId", mEmployeeId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getAllServers(finalObject = finalObject)

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_cash_drawer_main.visibility = View.VISIBLE
        ll_cash_drawer_detailed.visibility = View.GONE
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}
            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab!!.position) {
                    0 -> {
                        activeTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.white)
                        )
                        openTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
                        )
                        closedTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
                        )
                        activeTabHeader.background =
                            ContextCompat.getDrawable(
                                this@CashDrawerActivity, R.drawable.gradient_bg_1
                            )
                        openTabHeader.background = null
                        closedTabHeader.background = null
                        ll_active_drawer.visibility = View.VISIBLE
                        ll_open_drawer.visibility = View.GONE
                        ll_closed_drawer.visibility = View.GONE
                    }
                    1 -> {
                        activeTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
                        )
                        openTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.white)
                        )
                        closedTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
                        )
                        activeTabHeader.background = null
                        openTabHeader.background =
                            ContextCompat.getDrawable(
                                this@CashDrawerActivity, R.drawable.gradient_bg_1
                            )
                        closedTabHeader.background = null
                        ll_active_drawer.visibility = View.GONE
                        ll_open_drawer.visibility = View.VISIBLE
                        ll_closed_drawer.visibility = View.GONE
                    }
                    2 -> {
                        activeTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
                        )
                        openTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
                        )
                        closedTab.setTextColor(
                            ContextCompat.getColor(this@CashDrawerActivity, R.color.white)
                        )
                        activeTabHeader.background = null
                        openTabHeader.background = null
                        closedTabHeader.background =
                            ContextCompat.getDrawable(
                                this@CashDrawerActivity, R.drawable.gradient_bg_1
                            )
                        ll_active_drawer.visibility = View.GONE
                        ll_open_drawer.visibility = View.GONE
                        ll_closed_drawer.visibility = View.VISIBLE
                    }
                }
            }
        })

        tv_no_sale.setOnClickListener {
            noSalesDialog()
        }
        tv_cash_in.setOnClickListener {
            Cash_InDialog()
        }
        tv_cash_out.setOnClickListener {
            Cash_OutDialog()
        }
        tv_cash_drop.setOnClickListener {
            Cash_DropDialog()
        }
        tv_close_drawer.setOnClickListener {
            if (isCashDrawerClosed) {
                val jsonObj = JSONObject()
                jsonObj.put("restaurantId", restaurantId)
                jsonObj.put("createdBy", mEmployeeId)
                jsonObj.put("cashDrawersId", cashDrawerId)
                jsonObj.put("preAssignedTo", mEmployeeId)
                val final_Object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                openCashDrawer(finalObject = final_Object)
            } else {
                CloseDrawer_Dialog()
            }
        }
        tv_adjust_starting_balance.setOnClickListener {
            AdjustStartingBalance_Dialog()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        tabLayout = findViewById(R.id.tabLayout)
        ll_active_drawer = findViewById(R.id.ll_active_drawer)
        ll_open_drawer = findViewById(R.id.ll_open_drawer)
        ll_closed_drawer = findViewById(R.id.ll_closed_drawer)
        iv_back = findViewById(R.id.iv_back)
        ll_cash_drawer_report = findViewById(R.id.ll_cash_drawer_report)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        ll_cash_drawer_main = findViewById(R.id.ll_cash_drawer_main)
        ll_cash_drawer_detailed = findViewById(R.id.ll_cash_drawer_detailed)
        cv_active = findViewById(R.id.cv_active)
        rv_active_cash_drawer = findViewById(R.id.rv_active_cash_drawer)
        tv_active_cash_empty = findViewById(R.id.tv_active_cash_empty)
        cv_open = findViewById(R.id.cv_open)
        rv_open_cash_drawer = findViewById(R.id.rv_open_cash_drawer)
        tv_open_cash_empty = findViewById(R.id.tv_open_cash_empty)
        cv_closed = findViewById(R.id.cv_closed)
        rv_closed_cash_drawer = findViewById(R.id.rv_closed_cash_drawer)
        tv_closed_cash_empty = findViewById(R.id.tv_closed_cash_empty)
        tv_cash_drawer_title = findViewById(R.id.tv_cash_drawer_title)
        tv_starting_balance = findViewById(R.id.tv_starting_balance)
        tv_balance = findViewById(R.id.tv_balance)
        rv_cash_drawer_details = findViewById(R.id.rv_cash_drawer_details)
        tv_no_sale = findViewById(R.id.tv_no_sale)
        tv_cash_in = findViewById(R.id.tv_cash_in)
        tv_cash_out = findViewById(R.id.tv_cash_out)
        tv_cash_drop = findViewById(R.id.tv_cash_drop)
        tv_close_drawer = findViewById(R.id.tv_close_drawer)
        tv_open_drawer = findViewById(R.id.tv_open_drawer)
        tv_adjust_starting_balance = findViewById(R.id.tv_adjust_starting_balance)
        iv_cash_drawer_lock = findViewById(R.id.iv_cash_drawer_lock)
        ll_close_out = findViewById(R.id.ll_close_out)
        tv_close_out_balance = findViewById(R.id.tv_close_out_balance)
        ll_closed = findViewById(R.id.ll_closed)
        tv_currency_amount_header = findViewById(R.id.tv_currency_amount_header)

        val activeLm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val openLm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val closedLm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val drawerDetailsLm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_active_cash_drawer.hasFixedSize()
        rv_open_cash_drawer.hasFixedSize()
        rv_closed_cash_drawer.hasFixedSize()
        rv_cash_drawer_details.hasFixedSize()
        rv_active_cash_drawer.layoutManager = activeLm
        rv_open_cash_drawer.layoutManager = openLm
        rv_closed_cash_drawer.layoutManager = closedLm
        rv_cash_drawer_details.layoutManager = drawerDetailsLm
        val dividerItemDecoration =
            DividerItemDecoration(rv_cash_drawer_details.context, drawerDetailsLm.orientation)
        rv_cash_drawer_details.addItemDecoration(dividerItemDecoration)

        setUpTabIcons()
    }

    private fun setUpTabIcons() {
        val activeTabView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val openTabView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val closedTabView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        activeTab = activeTabView.findViewById(R.id.tv_tab_name)
        openTab = openTabView.findViewById(R.id.tv_tab_name)
        closedTab = closedTabView.findViewById(R.id.tv_tab_name)
        activeTabHeader = activeTabView.findViewById(R.id.ll_tab_header)
        openTabHeader = openTabView.findViewById(R.id.ll_tab_header)
        closedTabHeader = closedTabView.findViewById(R.id.ll_tab_header)
        activeTab.text = getString(R.string.active)
        openTab.text = getString(R.string.open)
        closedTab.text = getString(R.string.closed)
        activeTab.setTextColor(
            ContextCompat.getColor(this@CashDrawerActivity, R.color.white)
        )
        openTab.setTextColor(
            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
        )
        closedTab.setTextColor(
            ContextCompat.getColor(this@CashDrawerActivity, R.color.edit_text_hint)
        )
        activeTabHeader.background = ContextCompat.getDrawable(this, R.drawable.gradient_bg_1)
        openTabHeader.background = null
        closedTabHeader.background = null
        ll_active_drawer.visibility = View.VISIBLE
        ll_open_drawer.visibility = View.GONE
        ll_closed_drawer.visibility = View.GONE
        val activeLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val openLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val closedLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        activeTabHeader.layoutParams = activeLp
        openTabHeader.layoutParams = openLp
        closedTabHeader.layoutParams = closedLp
        tabLayout.addTab(tabLayout.newTab().setCustomView(activeTabView), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(openTabView))
        tabLayout.addTab(tabLayout.newTab().setCustomView(closedTabView))
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun noSalesDialog() {
        var amount = 0.00
        val serverId = "null"
        dialog_no_sales = Dialog(this)
        dialog_no_sales.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.no_sales_dialog, null)
        dialog_no_sales.setContentView(view)
        dialog_no_sales.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_no_sales.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_no_sales.setCanceledOnTouchOutside(false)
        val tv_currency_amount_header =
            dialog_no_sales.findViewById(R.id.tv_currency_amount_header) as TextView
        val et_amount = view.findViewById(R.id.et_amount) as EditText
        val et_comment = view.findViewById(R.id.et_comment) as EditText
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        tv_currency_amount_header.text = currencyType
        tv_cancel.setOnClickListener {
            et_amount.setText("%.2f".format(amount))
            et_comment.setText("")
            dialog_no_sales.dismiss()
        }

        et_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amount = if (s!!.isNotEmpty()) {
                    s.toString().toDouble()
                } else {
                    0.00
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        tv_ok.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("actionType", "no_sale")
            jObj.put("transactionType", "")
            jObj.put("action", "No Sales")
            jObj.put("amount", amount)
            jObj.put("comment", et_comment.text.toString())
            jObj.put("serverId", serverId)
            jObj.put("cashDrawersId", cashDrawerId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("createdBy", mEmployeeId)
            jObj.put("status", 1)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            addCashTransactions(finalObject = finalObject, dialog = dialog_no_sales)
        }
        dialog_no_sales.show()
    }

    private fun Cash_InDialog() {
        var amount = 0.00
        var serverId = "null"
        var actionType = "cash_in"
        var action = "Cash In"
        dialog_cashin = Dialog(this)
        dialog_cashin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this).inflate(R.layout.cash_in_dialog, null)
        dialog_cashin.setContentView(view)
        dialog_cashin.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_cashin.setCanceledOnTouchOutside(false)
        val ll_spinner = view.findViewById(R.id.ll_spinner) as LinearLayout
        val et_amount = view.findViewById(R.id.et_amount) as EditText
        val et_comment = view.findViewById(R.id.et_comment) as EditText
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cash_collected = view.findViewById(R.id.tv_cash_collected) as TextView
        val tv_currency_amount_header =
            view.findViewById(R.id.tv_currency_amount_header) as TextView
        val tv_cash_in = view.findViewById(R.id.tv_cash_in) as TextView
        val sp_server = view.findViewById(R.id.sp_server) as Spinner
        tv_currency_amount_header.text = currencyType
        val adapter = ServerListAdapter(this, serversList)
        sp_server.adapter = adapter
        sp_server.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = adapter.getSelectedItem(position)
                serverId = selectedItem.id.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_cancel.setOnClickListener {
            et_amount.setText("%.2f".format(amount))
            et_comment.setText("")
            dialog_cashin.dismiss()
        }
        tv_cash_in.setOnClickListener {
            et_comment.setText("")
            ll_spinner.visibility = View.GONE
            tv_cash_in.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_cash_collected.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.current_status_close
                )
            )
            tv_cash_collected.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_cash_in.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            serverId = "null"
            actionType = "cash_in"
            action = "Cash In"
        }
        tv_cash_collected.setOnClickListener {
            // et_amount.setText("%.2f".format(amount))
            et_comment.setText("")
            ll_spinner.visibility = View.VISIBLE
            tv_cash_in.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_cash_collected.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_cash_collected.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_cash_in.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            actionType = "cash_collected"
            action = "Cash In"
        }

        et_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amount = if (s!!.isNotEmpty()) {
                    s.toString().toDouble()
                } else {
                    0.00
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        tv_ok.setOnClickListener {
            if (serverId == "empty") {
                Toast.makeText(
                    this@CashDrawerActivity,
                    "Please select a server",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObj = JSONObject()
                jObj.put("actionType", actionType)
                jObj.put("transactionType", "credit")
                jObj.put("action", action)
                jObj.put("amount", amount)
                jObj.put("comment", et_comment.text.toString())
                jObj.put("serverId", serverId)
                jObj.put("cashDrawersId", cashDrawerId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("createdBy", mEmployeeId)
                jObj.put("status", 1)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                addCashTransactions(finalObject = finalObject, dialog = dialog_cashin)
            }
        }
        dialog_cashin.show()
    }

    private fun Cash_OutDialog() {
        var amount = 0.00
        var serverId = "null"
        var actionType = "cash_out"
        var action = "Cash Out"
        dialog_cashout = Dialog(this)
        dialog_cashout.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this).inflate(R.layout.cash_out_dialog, null)
        dialog_cashout.setContentView(view)
        dialog_cashout.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_cashout.setCanceledOnTouchOutside(false)
        val ll_spinner = view.findViewById(R.id.ll_spinner) as LinearLayout
        val et_amount = view.findViewById(R.id.et_amount) as EditText
        val et_comment = view.findViewById(R.id.et_comment) as EditText
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_pay_out = view.findViewById(R.id.tv_pay_out) as TextView
        val tv_tip_out = view.findViewById(R.id.tv_tip_out) as TextView
        val tv_cash_out = view.findViewById(R.id.tv_cash_out) as TextView
        val tv_currency_amount_header =
            view.findViewById(R.id.tv_currency_amount_header) as TextView
        val sp_server = view.findViewById(R.id.sp_server) as Spinner
        tv_currency_amount_header.text = currencyType
        val adapter = ServerListAdapter(this, serversList)
        sp_server.adapter = adapter
        sp_server.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = adapter.getSelectedItem(position)
                serverId = selectedItem.id.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_cancel.setOnClickListener {
            et_amount.setText("%.2f".format(amount))
            et_comment.setText("")
            dialog_cashout.dismiss()
        }
        tv_cash_out.setOnClickListener {
            et_comment.setText("")
            ll_spinner.visibility = View.GONE
            tv_cash_out.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_tip_out.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_out.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_cash_out.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_tip_out.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_out.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            serverId = "null"
            actionType = "cash_out"
            action = "Cash Out"
        }
        tv_tip_out.setOnClickListener {
            et_comment.setText("")
            ll_spinner.visibility = View.VISIBLE
            tv_cash_out.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_tip_out.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_pay_out.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_cash_out.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_tip_out.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            tv_pay_out.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            actionType = "tip_out"
            action = "Tip Out"
        }
        tv_pay_out.setOnClickListener {
            et_comment.setText("")
            ll_spinner.visibility = View.GONE
            tv_cash_out.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_tip_out.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pay_out.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_cash_out.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_tip_out.background = ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pay_out.background =
                ContextCompat.getDrawable(this, R.drawable.gradient_bg_1_corners)
            serverId = "null"
            actionType = "pay_out"
            action = "Pay Out"
        }

        et_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amount = if (s!!.isNotEmpty()) {
                    s.toString().toDouble()
                } else {
                    0.00
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        tv_ok.setOnClickListener {
            if (serverId == "empty") {
                Toast.makeText(
                    this@CashDrawerActivity,
                    "Please select a server",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObj = JSONObject()
                jObj.put("actionType", actionType)
                jObj.put("transactionType", "debit")
                jObj.put("action", action)
                jObj.put("amount", amount)
                jObj.put("comment", et_comment.text.toString())
                jObj.put("serverId", serverId)
                jObj.put("cashDrawersId", cashDrawerId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("createdBy", mEmployeeId)
                jObj.put("status", 1)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                addCashTransactions(finalObject = finalObject, dialog = dialog_cashout)
            }
        }
        dialog_cashout.show()
    }

    private fun Cash_DropDialog() {
        var amount = 0.00
        var drop_amount = ""
        val serverId = "null"
        dialog_cash_drop = Dialog(this)
        dialog_cash_drop.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.cashdrop_key_pad_dailog, null)
        dialog_cash_drop.setContentView(view)
        dialog_cash_drop.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_cash_drop.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_cash_drop.setCanceledOnTouchOutside(false)
        val iv_close_num_pad = view.findViewById(R.id.iv_close_num_pad) as ImageView
        val tv_current_balance = view.findViewById(R.id.tv_current_balance) as TextView
        val tv_cash_drop_amount = view.findViewById(R.id.tv_cash_drop_amount) as TextView
        val cv_one = view.findViewById(R.id.cv_one) as CardView
        val cv_two = view.findViewById(R.id.cv_two) as CardView
        val cv_three = view.findViewById(R.id.cv_three) as CardView
        val cv_four = view.findViewById(R.id.cv_four) as CardView
        val cv_five = view.findViewById(R.id.cv_five) as CardView
        val cv_six = view.findViewById(R.id.cv_six) as CardView
        val cv_seven = view.findViewById(R.id.cv_seven) as CardView
        val cv_eight = view.findViewById(R.id.cv_eight) as CardView
        val cv_nine = view.findViewById(R.id.cv_nine) as CardView
        val cv_zero = view.findViewById(R.id.cv_zero) as CardView
        val cv_double_zero = view.findViewById(R.id.cv_double_zero) as CardView
        val cv_dot = view.findViewById(R.id.cv_dot) as CardView
        val cv_clear = view.findViewById(R.id.cv_clear) as CardView
        val cv_delete = view.findViewById(R.id.cv_delete) as CardView
        val ll_done = view.findViewById(R.id.ll_done) as LinearLayout
        tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        if (balance >= 0) {
            tv_current_balance.text = currencyType + "%.2f".format(abs(balance))
        } else {
            tv_current_balance.text = "-" + currencyType + "%.2f".format(abs(balance))
        }
        iv_close_num_pad.setOnClickListener {
            dialog_cash_drop.dismiss()
        }
        cv_clear.setOnClickListener {
            drop_amount = getString(R.string.default_value_0)
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_delete.setOnClickListener {
            if (drop_amount != "") {
                drop_amount = drop_amount.dropLast(1)
                if (drop_amount == "") {
                    drop_amount = getString(R.string.default_value_0)
                }
            } else {
                drop_amount == getString(R.string.default_value_0)
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_one.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_one
            } else {
                drop_amount += str_one
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_two.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_two
            } else {
                drop_amount += str_two
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_three.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_three
            } else {
                drop_amount += str_three
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_four.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_four
            } else {
                drop_amount += str_four
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_five.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_five
            } else {
                drop_amount += str_five
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_six.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_six
            } else {
                drop_amount += str_six
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_seven.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_seven
            } else {
                drop_amount += str_seven
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_eight.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_eight
            } else {
                drop_amount += str_eight
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_nine.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_nine
            } else {
                drop_amount += str_nine
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_zero.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_zero
            } else {
                drop_amount += str_zero
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_double_zero.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_double_zero
            } else {
                drop_amount += str_double_zero
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        cv_dot.setOnClickListener {
            val drp_amount = amount.toString()
            if (drp_amount == getString(R.string.default_value_0)) {
                drop_amount = str_dot
            } else if (drp_amount.contains(str_dot)) {
                // don't add another decimal point
            } else {
                drop_amount += str_dot
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_drop_amount.text = currencyType + "%.2f".format(amount)
        }
        ll_done.setOnClickListener {
            if (amount <= balance) {
                cashDropCommentDialog(amount, serverId)
            } else {
                Toast.makeText(
                    this,
                    "Amount should be less than current balance!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        dialog_cash_drop.show()
    }

    private fun cashDropCommentDialog(amount: Double, serverId: String) {
        val dialog_cash_drop_comment = Dialog(this)
        dialog_cash_drop_comment.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.cash_drop_comment, null)
        dialog_cash_drop_comment.setContentView(view)
        dialog_cash_drop_comment.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_cash_drop_comment.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_cash_drop_comment.setCanceledOnTouchOutside(false)
        val et_cash_drop_amount = view.findViewById(R.id.et_cash_drop_amount) as EditText
        val et_comment = view.findViewById(R.id.et_comment) as EditText
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_currency_amount_header =
            view.findViewById(R.id.tv_currency_amount_header) as TextView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        tv_currency_amount_header.text = currencyType
        tv_cancel.setOnClickListener {
            dialog_cash_drop_comment.dismiss()
        }
        et_cash_drop_amount.setText("%.2f".format(amount))
        tv_ok.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("actionType", "cash_drop")
            jObj.put("transactionType", "debit")
            jObj.put("action", "Cash Drop")
            jObj.put("amount", amount)
            jObj.put("comment", et_comment.text.toString())
            jObj.put("serverId", serverId)
            jObj.put("cashDrawersId", cashDrawerId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("createdBy", mEmployeeId)
            jObj.put("status", 1)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            addCashTransactions(finalObject = finalObject, dialog = dialog_cash_drop)
            dialog_cash_drop_comment.dismiss()
        }
        dialog_cash_drop_comment.show()
    }

    private fun CloseDrawer_Dialog() {
        var amount = 0.00
        var diff_amount = 0.00
        dialog_close_drawer = Dialog(this)
        dialog_close_drawer.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.closedrawer_dialog, null)
        dialog_close_drawer.setContentView(view)
        dialog_close_drawer.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_close_drawer.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_close_drawer.setCanceledOnTouchOutside(false)
        val tv_currency_cash_expected_header =
            view.findViewById(R.id.tv_currency_cash_expected_header) as TextView
        val tv_currency_cash_actual_header =
            view.findViewById(R.id.tv_currency_cash_actual_header) as TextView
        val tv_cash_expected = view.findViewById(R.id.tv_cash_expected) as TextView
        val tv_cash_actual = view.findViewById(R.id.tv_cash_actual) as TextView
        val et_cash_actual = view.findViewById(R.id.et_cash_actual) as EditText
        val et_difference = view.findViewById(R.id.et_difference) as EditText
        val et_comment = view.findViewById(R.id.et_comment) as EditText
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_count_bills = view.findViewById(R.id.tv_count_bills) as TextView
        val tv_close_drawer = view.findViewById(R.id.tv_close_drawer) as TextView
        tv_cash_expected.text = "%.2f".format(balance)
        tv_cash_actual.text = "%.2f".format(balance)
        tv_currency_cash_actual_header.text = getString(R.string.cash_actual) + " ($currencyType)"
        tv_currency_cash_expected_header.text =
            getString(R.string.cash_expected) + " ($currencyType)"
        tv_cancel.setOnClickListener {
            dialog_close_drawer.dismiss()
        }
        tv_count_bills.setOnClickListener {
            // intent count bills
        }
        et_cash_actual.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    val diff =
                        abs(tv_cash_actual.text.toString().toDouble()) - s.toString().toDouble()
                    diff_amount = diff
                    et_difference.setText("%.2f".format(abs(diff)))
                } else {
                    diff_amount = 0.00
                    et_difference.setText(diff_amount.toString())
                }
            }
        })
        et_cash_actual.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amount = if (s!!.isNotEmpty()) {
                    s.toString().toDouble()
                } else {
                    0.00
                }
            }
        })
        tv_close_drawer.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("actionType", "closeout_overages")
            jObj.put("action", "Closeout Overages")
            jObj.put("amount", amount)
            jObj.put("comment", et_comment.text.toString())
            jObj.put("cashDrawersId", cashDrawerId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("createdBy", mEmployeeId)
            jObj.put("closeDiff", diff_amount)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            closeCashDrawer(finalObject = finalObject)
            dialog_close_drawer.dismiss()
        }
        dialog_close_drawer.show()
    }

    private fun AdjustStartingBalance_Dialog() {
        var amount = 0.00
        var drop_amount = ""
        dialog_adjust_balance = Dialog(this)
        dialog_adjust_balance.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.key_pad_cashdrawer_dailog, null)
        dialog_adjust_balance.setContentView(view)
        dialog_adjust_balance.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_adjust_balance.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_adjust_balance.setCanceledOnTouchOutside(false)
        val iv_close_num_pad = view.findViewById(R.id.iv_close_num_pad) as ImageView
        val tv_min_balance = view.findViewById(R.id.tv_min_balance) as TextView
        val tv_cash_added = view.findViewById(R.id.tv_cash_added) as TextView
        val cv_one = view.findViewById(R.id.cv_one) as CardView
        val cv_two = view.findViewById(R.id.cv_two) as CardView
        val cv_three = view.findViewById(R.id.cv_three) as CardView
        val cv_four = view.findViewById(R.id.cv_four) as CardView
        val cv_five = view.findViewById(R.id.cv_five) as CardView
        val cv_six = view.findViewById(R.id.cv_six) as CardView
        val cv_seven = view.findViewById(R.id.cv_seven) as CardView
        val cv_eight = view.findViewById(R.id.cv_eight) as CardView
        val cv_nine = view.findViewById(R.id.cv_nine) as CardView
        val cv_zero = view.findViewById(R.id.cv_zero) as CardView
        val cv_clear = view.findViewById(R.id.cv_clear) as CardView
        val cv_delete = view.findViewById(R.id.cv_delete) as CardView
        val ll_done = view.findViewById(R.id.ll_done) as LinearLayout
        tv_cash_added.text = currencyType + "%.2f".format(amount)
        tv_min_balance.text = currencyType + "%.2f".format(amount)
        iv_close_num_pad.setOnClickListener {
            dialog_adjust_balance.dismiss()
        }
        cv_clear.setOnClickListener {
            drop_amount = getString(R.string.default_value_0)
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_delete.setOnClickListener {
            if (drop_amount != "") {
                drop_amount = drop_amount.dropLast(1)
                if (drop_amount == "") {
                    drop_amount = getString(R.string.default_value_0)
                }
            } else {
                drop_amount == getString(R.string.default_value_0)
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_one.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_one
            } else {
                drop_amount += str_one
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_two.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_two
            } else {
                drop_amount += str_two
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_three.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_three
            } else {
                drop_amount += str_three
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_four.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_four
            } else {
                drop_amount += str_four
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_five.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_five
            } else {
                drop_amount += str_five
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_six.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_six
            } else {
                drop_amount += str_six
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_seven.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_seven
            } else {
                drop_amount += str_seven
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_eight.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_eight
            } else {
                drop_amount += str_eight
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_nine.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_nine
            } else {
                drop_amount += str_nine
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        cv_zero.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_zero
            } else {
                drop_amount += str_zero
            }
            amount = drop_amount.toDouble()
            amount /= 100
            tv_cash_added.text = currencyType + "%.2f".format(amount)
        }
        ll_done.setOnClickListener {
//            if (amount == 0.00) {
//            } else {
            val jObj = JSONObject()
            jObj.put("userId", mEmployeeId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("cashDrawersId", cashDrawerId)
            jObj.put("adjustStartingBalance", amount)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            adjustStartingBalance(finalObject)
//            }
        }
        dialog_adjust_balance.show()
    }

    private fun getAllServers(finalObject: JsonObject) {
        serversList = ArrayList()
        val empty = ServersDataResponse()
        empty.firstName = "Select a Server"
        empty.lastName = ""
        empty.id = "empty"
        serversList.add(empty)
        val api = ApiInterface.create()
        val call = api.getAllServersApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        if (resp.serverList!!.size > 0) {
                            serversList.addAll(resp.serverList)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllCashDrawers(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getAllCashDrawersListApi(finalObject)
        call.enqueue(object : Callback<CashDrawersListDataResponse> {
            override fun onFailure(call: Call<CashDrawersListDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashDrawersListDataResponse>,
                response: Response<CashDrawersListDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val openCashDrawersList = resp.cashdrawersOpenList!!
                        val closedCashDrawersList = resp.cashdrawersClosedList!!
                        val activeCashDrawersList = resp.cashdrawersActiveList!!
                        if (activeCashDrawersList.size > 0) {
                            cv_active.visibility = View.VISIBLE
                            tv_active_cash_empty.visibility = View.GONE
                        } else {
                            cv_active.visibility = View.GONE
                            tv_active_cash_empty.visibility = View.VISIBLE
                        }
                        if (openCashDrawersList.size > 0) {
                            cv_open.visibility = View.VISIBLE
                            tv_open_cash_empty.visibility = View.GONE
                        } else {
                            cv_open.visibility = View.GONE
                            tv_open_cash_empty.visibility = View.VISIBLE
                        }
                        if (closedCashDrawersList.size > 0) {
                            cv_closed.visibility = View.VISIBLE
                            tv_closed_cash_empty.visibility = View.GONE
                        } else {
                            cv_closed.visibility = View.GONE
                            tv_closed_cash_empty.visibility = View.VISIBLE
                        }
                        val activeListAdapter =
                            CashDrawersListAdapter(this@CashDrawerActivity, activeCashDrawersList)
                        val openListAdapter =
                            CashDrawersListAdapter(this@CashDrawerActivity, openCashDrawersList)
                        val closedListAdapter =
                            CashDrawersListAdapter(this@CashDrawerActivity, closedCashDrawersList)
                        rv_active_cash_drawer.adapter = activeListAdapter
                        rv_open_cash_drawer.adapter = openListAdapter
                        rv_closed_cash_drawer.adapter = closedListAdapter
                        activeListAdapter.notifyDataSetChanged()
                        openListAdapter.notifyDataSetChanged()
                        closedListAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(
                            this@CashDrawerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCashTransactions(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewCashTransactionsApi(finalObject)
        call.enqueue(object : Callback<CashTransactionsDataResponse> {
            override fun onFailure(call: Call<CashTransactionsDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashTransactionsDataResponse>,
                response: Response<CashTransactionsDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        if (resp.startingBalance.toString() == "null") {
                            tv_starting_balance.text =
                                currencyType + getString(R.string.default_value_0)
                        } else {
                            if (resp.startingBalance!! >= 0) {
                                tv_starting_balance.text =
                                    currencyType + "%.2f".format(abs(resp.startingBalance))
                            } else {
                                tv_starting_balance.text =
                                    "-" + currencyType + "%.2f".format(abs(resp.startingBalance))
                            }
                        }
                        /*here if close out balance is not null
                        * cash drawer is closed */
                        if (resp.closeOutBalance.toString() == "null") {
                            tv_close_out_balance.text =
                                currencyType + getString(R.string.default_value_0)
                            ll_close_out.visibility = View.GONE
                            ll_closed.visibility = View.GONE
                            tv_close_drawer.text = getString(R.string.close_drawer)
                            tv_no_sale.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.white_bg_corner
                            )
                            tv_cash_in.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.white_bg_corner
                            )
                            tv_cash_out.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.white_bg_corner
                            )
                            tv_cash_drop.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.white_bg_corner
                            )
                            isCashDrawerClosed = false
                            tv_no_sale.isEnabled = true
                            tv_cash_in.isEnabled = true
                            tv_cash_out.isEnabled = true
                            tv_cash_drop.isEnabled = true
                        } else {
                            if (resp.closeOutBalance!! >= 0) {
                                tv_close_out_balance.text =
                                    currencyType + "%.2f".format(abs(resp.closeOutBalance))
                            } else {
                                tv_close_out_balance.text =
                                    "-" + currencyType + "%.2f".format(abs(resp.closeOutBalance))
                            }
                            ll_close_out.visibility = View.VISIBLE
                            ll_closed.visibility = View.VISIBLE
                            tv_close_drawer.text = getString(R.string.re_open)
                            tv_no_sale.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.disabled_bg_corner
                            )
                            tv_cash_in.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.disabled_bg_corner
                            )
                            tv_cash_out.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.disabled_bg_corner
                            )
                            tv_cash_drop.background = ContextCompat.getDrawable(
                                this@CashDrawerActivity,
                                R.drawable.disabled_bg_corner
                            )
                            isCashDrawerClosed = true
                            tv_no_sale.isEnabled = false
                            tv_cash_in.isEnabled = false
                            tv_cash_out.isEnabled = false
                            tv_cash_drop.isEnabled = false
                        }
                        if (resp.Balance.toString() == "null") {
                            tv_balance.text = currencyType + getString(R.string.default_value_0)
                            balance = 0.00
                        } else {
                            if (resp.Balance!! >= 0) {
                                tv_balance.text = currencyType + "%.2f".format(abs(resp.Balance))
                            } else {
                                tv_balance.text =
                                    "-" + currencyType + "%.2f".format(abs(resp.Balance))
                            }
                            balance = resp.Balance
                        }
                        val cashTransactionsList = resp.cashdrawers!!
                        val adapter = CashTransactionsListAdapter(
                            this@CashDrawerActivity, cashTransactionsList
                        )
                        rv_cash_drawer_details.swapAdapter(adapter, true)
                        adapter.notifyDataSetChanged()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun addCashTransactions(finalObject: JsonObject, dialog: Dialog) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addCashTransactionsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val jObj = JSONObject()
                        jObj.put("restaurantId", restaurantId)
                        jObj.put("userId", mEmployeeId)
                        jObj.put("cashDrawersId", cashDrawerId)
                        val final_object = JsonParser.parseString(jObj.toString()).asJsonObject
                        getCashTransactions(finalObject = final_object)
                        if (dialog.isShowing)
                            dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@CashDrawerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun closeCashDrawer(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.closeCashDrawerApi(finalObject)
        call.enqueue(object : Callback<CashTransactionsCloseDataResponse> {
            override fun onFailure(call: Call<CashTransactionsCloseDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashTransactionsCloseDataResponse>,
                response: Response<CashTransactionsCloseDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        /*ll_close_out.visibility = View.VISIBLE
                        ll_closed.visibility = View.VISIBLE
                        tv_close_drawer.text = getString(R.string.re_open)
                        tv_no_sale.background = ContextCompat.getDrawable(
                            this@CashDrawerActivity,
                            R.drawable.disabled_bg_corner
                        )
                        tv_cash_in.background = ContextCompat.getDrawable(
                            this@CashDrawerActivity,
                            R.drawable.disabled_bg_corner
                        )
                        tv_cash_out.background = ContextCompat.getDrawable(
                            this@CashDrawerActivity,
                            R.drawable.disabled_bg_corner
                        )
                        tv_cash_drop.background = ContextCompat.getDrawable(
                            this@CashDrawerActivity,
                            R.drawable.disabled_bg_corner
                        )
                        tv_no_sale.isEnabled = false
                        tv_cash_in.isEnabled = false
                        tv_cash_out.isEnabled = false
                        tv_cash_drop.isEnabled = false*/
                        isCashDrawerClosed = true
                        val jObj = JSONObject()
                        jObj.put("restaurantId", restaurantId)
                        jObj.put("userId", mEmployeeId)
                        jObj.put("cashDrawersId", cashDrawerId)
                        val final_object = JsonParser.parseString(jObj.toString()).asJsonObject
                        getCashTransactions(finalObject = final_object)
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun openCashDrawer(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.openCashDrawerApi(finalObject)
        call.enqueue(object : Callback<CashTransactionsOpenDataResponse> {
            override fun onFailure(call: Call<CashTransactionsOpenDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashTransactionsOpenDataResponse>,
                response: Response<CashTransactionsOpenDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        isCashDrawerClosed = false
                        val jObj = JSONObject()
                        jObj.put("restaurantId", restaurantId)
                        jObj.put("userId", mEmployeeId)
                        jObj.put("cashDrawersId", cashDrawerId)
                        val final_object = JsonParser.parseString(jObj.toString()).asJsonObject
                        getCashTransactions(finalObject = final_object)
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun adjustStartingBalance(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.adjustStartingBalanceApi(finalObject)
        call.enqueue(object : Callback<AdjustStartingBalanceDataResponse> {
            override fun onFailure(call: Call<AdjustStartingBalanceDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AdjustStartingBalanceDataResponse>,
                response: Response<AdjustStartingBalanceDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        balance = resp.Balance!!
                        if (resp.Balance.toString() != "null") {
                            if (resp.Balance >= 0) {
                                tv_balance.text =
                                    currencyType + "%.2f".format(abs(resp.Balance))
                            } else {
                                tv_balance.text =
                                    "-" + currencyType + "%.2f".format(abs(resp.Balance))
                            }
                        } else {
                            tv_balance.text =
                                currencyType + getString(R.string.default_value_0)
                        }
                        if (resp.startingBalance.toString() != "null") {
                            if (resp.startingBalance!! >= 0) {
                                tv_starting_balance.text =
                                    currencyType + "%.2f".format(abs(resp.startingBalance))
                            } else {
                                tv_starting_balance.text =
                                    "-" + currencyType + "%.2f".format(abs(resp.startingBalance))
                            }
                        } else {
                            tv_starting_balance.text =
                                currencyType + getString(R.string.default_value_0)
                        }
                        if (isCashDrawerClosed) {
                            tv_close_out_balance.text =
                                currencyType + getString(R.string.default_value_0)
                        } else {
                            if (resp.closeOutBalance.toString() != "null") {
                                if (resp.closeOutBalance!! >= 0) {
                                    tv_close_out_balance.text =
                                        currencyType + "%.2f".format(abs(resp.closeOutBalance))
                                } else {
                                    tv_close_out_balance.text =
                                        "-" + currencyType + "%.2f".format(abs(resp.closeOutBalance))
                                }
                            } else {
                                tv_close_out_balance.text =
                                    currencyType + getString(R.string.default_value_0)
                            }
                        }
                        dialog_adjust_balance.dismiss()
                    } else {
                        Toast.makeText(
                            this@CashDrawerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@CashDrawerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@CashDrawerActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@CashDrawerActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@CashDrawerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class CashDrawersListAdapter(context: Context, list: ArrayList<CashDrawersDataResponse>) :
        RecyclerView.Adapter<CashDrawersListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<CashDrawersDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.cash_drawer_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): CashDrawersDataResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (position % 2 == 1) {
                holder.ll_item_header.setBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.payment_list_item_closed)
                )
            } else {
                holder.ll_item_header.setBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.white)
                )
            }
            holder.tv_currency_drawer_balance.text = currencyType
            holder.tv_drawer_name.text = mList!![position].cashDrawerName.toString()
            holder.tv_drawer_balance.text = "%.2f".format(mList!![position].cashDrawerOneBalnce)
            if (mList!![position].preAssigned!!) {
                holder.iv_lock.setImageDrawable(
                    ContextCompat.getDrawable(mContext!!, R.drawable.ic_unlocked)
                )
            } else {
                holder.iv_lock.setImageDrawable(
                    ContextCompat.getDrawable(mContext!!, R.drawable.ic_locked)
                )
            }
            holder.ll_item_header.setOnClickListener {
                cashDrawerId = mList!![position].id.toString()
                val jObj = JSONObject()
                jObj.put("restaurantId", restaurantId)
                jObj.put("userId", mEmployeeId)
                jObj.put("cashDrawersId", cashDrawerId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                getCashTransactions(finalObject = finalObject)
                ll_cash_drawer_main.visibility = View.GONE
                ll_cash_drawer_detailed.visibility = View.VISIBLE
                tv_cash_drawer_title.text = mList!![position].cashDrawerName.toString()
                if (mList!![position].preAssigned!!) {
                    iv_cash_drawer_lock.setImageDrawable(
                        ContextCompat.getDrawable(mContext!!, R.drawable.ic_unlocked)
                    )
                } else {
                    iv_cash_drawer_lock.setImageDrawable(
                        ContextCompat.getDrawable(mContext!!, R.drawable.ic_locked)
                    )
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var iv_lock: ImageView = view.findViewById(R.id.iv_lock)
            var tv_drawer_name: TextView = view.findViewById(R.id.tv_drawer_name)
            var tv_drawer_balance: TextView = view.findViewById(R.id.tv_drawer_balance)
            var tv_currency_drawer_balance: TextView =
                view.findViewById(R.id.tv_currency_drawer_balance)
            var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
        }
    }

    override fun onBackPressed() {
        if (ll_cash_drawer_detailed.visibility == View.VISIBLE) {
            ll_cash_drawer_main.visibility = View.VISIBLE
            ll_cash_drawer_detailed.visibility = View.GONE
            val obj = JSONObject()
            obj.put("restaurantId", restaurantId)
            val jObject = JsonParser.parseString(obj.toString()).asJsonObject
            getAllCashDrawers(finalObject = jObject)
        } else {
            super.onBackPressed()
        }
    }
}