package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.DeliveryItemListAdapter
import com.restaurant.zing.adapters.SnoozeListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.helpers.SnoozeTimeListDataModel
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

@SuppressLint("SetTextI18n")
class PendingOrdersActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var tabLayout: TabLayout
    private lateinit var ll_new_order: LinearLayout
    private lateinit var ll_throttle_onlineorders: LinearLayout
    private lateinit var ll_button_options: LinearLayout
    private lateinit var ll_approve_options: LinearLayout
    private lateinit var tv_button_approve: TextView
    private lateinit var tv_button_reject: TextView
    private lateinit var tv_button_send_kitchen: TextView
    private lateinit var tv_button_pay: TextView
    private lateinit var loading_dialog: Dialog
    private lateinit var dialog_throttleonline_orders: Dialog
    private lateinit var seekbar_deliverydelay: AppCompatSeekBar
    private lateinit var seekbar_takeoutdelay: AppCompatSeekBar
    private lateinit var sp_snooze: Spinner
    private lateinit var sp_adapter: SnoozeListAdapter
    private lateinit var ll_spinner: LinearLayout
    private lateinit var tv_pending_on: TextView
    private lateinit var tv_pending_snooze: TextView
    private lateinit var tv_pending_off: TextView

    private lateinit var needapprovalTab: TextView
    private lateinit var futurechecksTab: TextView
    private lateinit var approvedChecksTab: TextView
    private lateinit var needapprovalTabHeader: LinearLayout
    private lateinit var approvedTabHeader: LinearLayout
    private lateinit var futurechecksTabHeader: LinearLayout

    private lateinit var ll_needsapproval: LinearLayout
    private lateinit var ll_futurechecks: LinearLayout
    private lateinit var ll_approved: LinearLayout
    private lateinit var tv_needsapproval_empty: TextView
    private lateinit var tv_futurechecks_empty: TextView
    private lateinit var tv_approved_empty: TextView
    private lateinit var rv_futurechecks: RecyclerView
    private lateinit var rv_needsapproval: RecyclerView
    private lateinit var rv_approved: RecyclerView

    private lateinit var tv_pending_credits: TextView
    private lateinit var tv_pending_subtotal: TextView
    private lateinit var tv_pending_discount: TextView
    private lateinit var tv_pending_tax: TextView
    private lateinit var tv_pending_tips: TextView
    private lateinit var tv_deliverycreatedon: TextView
    private lateinit var tv_pending_balance_due: TextView
    private lateinit var tv_pending_total: TextView
    private lateinit var rv_order_items: RecyclerView
    private lateinit var tv_currency_total: TextView
    private lateinit var tv_currency_balance_due: TextView
    private lateinit var tv_currency_tax: TextView
    private lateinit var tv_currency_sub_total: TextView
    private lateinit var tv_currency_tip_amount: TextView
    private lateinit var tv_currency_discount: TextView
    private lateinit var tv_currency_credits: TextView
    private lateinit var tv_currency_quantity: TextView

    var unapprovedArrayList: ArrayList<PendingOnlineOrederDataResponse>? = null
    var futureonlineArrayList: ArrayList<FutureOnlineOrderDataResponse>? = null
    var viewOrderitemsArrayList: ArrayList<OrderItemsResponse>? = null

    private lateinit var unApprovedList: ArrayList<NeedApprovalOrdersItemsResponse>
    private lateinit var scheduledList: ArrayList<NeedApprovalOrdersItemsResponse>
    private lateinit var approvedList: ArrayList<NeedApprovalOrdersItemsResponse>
    private lateinit var snoozeTimeList: ArrayList<SnoozeTimeListDataModel>

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mSelectedItem = -1
    private var mOrderId = ""
    private var isPendingOrderSelected = false
    private var isFutureOrderSelected = false
    private var isApprovedOrderSelected = false
    private var availability = ""
    private var selected_tab = ""
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var currentTime = ""
    private var currentTimeMillis = 0L
    private var currencyType = ""
    private var from_screen = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pendingorders)
        try {
            from_screen = intent.getStringExtra("from_screen")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()

        tv_currency_total.text = currencyType
        tv_currency_balance_due.text = currencyType
        tv_currency_tax.text = currencyType
        tv_currency_sub_total.text = currencyType
        tv_currency_tip_amount.text = currencyType
        tv_currency_discount.text = currencyType
        tv_currency_credits.text = currencyType
        tv_currency_quantity.text = "($currencyType)"

        try {
            currentTimeMillis = Calendar.getInstance().timeInMillis
            val df: DateFormat = SimpleDateFormat("MM/dd,hh:mm", Locale.getDefault())
            currentTime = df.format(currentTimeMillis)
            Log.e("currentTime", currentTime)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        ll_button_options.visibility = View.GONE
        tv_deliverycreatedon.isSelected = true
        resetData()
        getAllOrders()

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_throttle_onlineorders.setOnClickListener {
            throttleOnlineOrders()
        }
        ll_new_order.setOnClickListener {
            val intent =
                Intent(this@PendingOrdersActivity, OrderDetailsActivity::class.java)
            intent.putExtra("from_screen", "home")
            startActivity(intent)
        }
        tv_button_approve.setOnClickListener {
            if (isPendingOrderSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("employeeId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("restaurantId", restaurantId)
                val final_Object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_Object", final_Object.toString())
                approvedPendingOnlineOrder(final_Object)
            }
            if (isFutureOrderSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("employeeId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("restaurantId", restaurantId)
                val final_Object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_Object", final_Object.toString())
                confirmOrder(final_Object)
            }
        }
        tv_button_reject.setOnClickListener {
            if (isPendingOrderSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("employeeId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("restaurantId", restaurantId)
                val final_Object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_Object", final_Object.toString())
                cancelOrder(final_Object)
            }
            if (isFutureOrderSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("employeeId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("restaurantId", restaurantId)
                val final_Object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_Object", final_Object.toString())
                cancelOrder(final_Object)
            }
        }
        tv_button_send_kitchen.setOnClickListener {
            if (isFutureOrderSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("employeeId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("restaurantId", restaurantId)
                val final_Object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_Object", final_Object.toString())
                approvedPendingOnlineOrder(final_Object)
            }
        }
        tv_button_pay.setOnClickListener {
            if (isApprovedOrderSelected) {
                val intent = Intent(this@PendingOrdersActivity, PaymentScreenActivity::class.java)
                intent.putExtra("from_screen", "payment_terminal")
                intent.putExtra("mOrderId", mOrderId)
                startActivity(intent)
            }
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                resetData()
                when (tab.position) {
                    0 -> {
                        ll_needsapproval.visibility = View.VISIBLE
                        ll_futurechecks.visibility = View.GONE
                        ll_approved.visibility = View.GONE
                        ll_approve_options.visibility = View.GONE
                        ll_button_options.visibility = View.GONE
                        tv_button_send_kitchen.visibility = View.GONE
                        selected_tab = "needs_approval"
                        needapprovalTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.white
                            )
                        )
                        futurechecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.edit_text_hint
                            )
                        )
                        approvedChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.edit_text_hint
                            )
                        )
                        needapprovalTabHeader.background = ContextCompat.getDrawable(
                            this@PendingOrdersActivity,
                            R.drawable.gradient_bg_1
                        )
                        futurechecksTabHeader.background = null
                        approvedTabHeader.background = null
                    }
                    1 -> {
                        selected_tab = "approved"
                        ll_needsapproval.visibility = View.GONE
                        ll_approved.visibility = View.VISIBLE
                        ll_futurechecks.visibility = View.GONE
                        ll_approve_options.visibility = View.GONE
                        ll_button_options.visibility = View.GONE
                        tv_button_send_kitchen.visibility = View.GONE
                        needapprovalTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.edit_text_hint
                            )
                        )
                        futurechecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.edit_text_hint
                            )
                        )
                        approvedChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.white
                            )
                        )
                        needapprovalTabHeader.background = null
                        futurechecksTabHeader.background = null
                        approvedTabHeader.background = ContextCompat.getDrawable(
                            this@PendingOrdersActivity,
                            R.drawable.gradient_bg_1
                        )
                    }
                    2 -> {
                        selected_tab = "future_check"
                        ll_needsapproval.visibility = View.GONE
                        ll_approved.visibility = View.GONE
                        ll_futurechecks.visibility = View.VISIBLE
                        ll_approve_options.visibility = View.GONE
                        ll_button_options.visibility = View.GONE
                        tv_button_send_kitchen.visibility = View.GONE
                        needapprovalTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.edit_text_hint
                            )
                        )
                        futurechecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.white
                            )
                        )
                        approvedChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PendingOrdersActivity,
                                R.color.edit_text_hint
                            )
                        )
                        needapprovalTabHeader.background = null
                        futurechecksTabHeader.background = ContextCompat.getDrawable(
                            this@PendingOrdersActivity,
                            R.drawable.gradient_bg_1
                        )
                        approvedTabHeader.background = null
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

        })
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        unapprovedArrayList = ArrayList()
        futureonlineArrayList = ArrayList()
        viewOrderitemsArrayList = ArrayList()
        iv_back = findViewById(R.id.iv_back)
        ll_new_order = findViewById(R.id.ll_new_order)
        ll_throttle_onlineorders = findViewById(R.id.ll_throttle_onlineorders)

        tv_pending_credits = findViewById(R.id.tv_credits)
        tv_pending_subtotal = findViewById(R.id.tv_sub_total)
        tv_pending_discount = findViewById(R.id.tv_discount)
        tv_pending_tax = findViewById(R.id.tv_tax)
        tv_pending_tips = findViewById(R.id.tv_tip_amount)
        tv_deliverycreatedon = findViewById(R.id.tv_deliverycreatedon)
        tv_pending_balance_due = findViewById(R.id.tv_balance)
        tv_pending_total = findViewById(R.id.tv_total)
        rv_order_items = findViewById(R.id.rv_order_items)
        tabLayout = findViewById(R.id.tabLayout)
        ll_needsapproval = findViewById(R.id.ll_needsapproval)
        rv_needsapproval = findViewById(R.id.rv_needsapproval)
        rv_approved = findViewById(R.id.rv_approved)
        tv_needsapproval_empty = findViewById(R.id.tv_needsapproval_empty)
        ll_futurechecks = findViewById(R.id.ll_futurechecks)
        ll_approved = findViewById(R.id.ll_approved)
        rv_futurechecks = findViewById(R.id.rv_futurechecks)
        tv_futurechecks_empty = findViewById(R.id.tv_futurechecks_empty)
        tv_approved_empty = findViewById(R.id.tv_approved_empty)
        ll_button_options = findViewById(R.id.ll_button_options)
        ll_approve_options = findViewById(R.id.ll_approve_options)
        tv_button_approve = findViewById(R.id.tv_button_approve)
        tv_button_reject = findViewById(R.id.tv_button_reject)
        tv_button_send_kitchen = findViewById(R.id.tv_button_send_kitchen)
        tv_button_pay = findViewById(R.id.tv_button_pay)
        tv_currency_total = findViewById(R.id.tv_currency_total)
        tv_currency_balance_due = findViewById(R.id.tv_currency_balance_due)
        tv_currency_tax = findViewById(R.id.tv_currency_tax)
        tv_currency_sub_total = findViewById(R.id.tv_currency_sub_total)
        tv_currency_tip_amount = findViewById(R.id.tv_currency_tip_amount)
        tv_currency_discount = findViewById(R.id.tv_currency_discount)
        tv_currency_credits = findViewById(R.id.tv_currency_credits)
        tv_currency_quantity = findViewById(R.id.tv_currency_quantity)
        unApprovedList = ArrayList()
        approvedList = ArrayList()
        scheduledList = ArrayList()
        setUpTabIcons()

        val needsapprovalLinearLayoutManager =
            GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val approvedLinearLayoutManager =
            GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val enrouteLinearLayoutManager =
            GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val orderItemsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_needsapproval.layoutManager = needsapprovalLinearLayoutManager
        rv_approved.layoutManager = approvedLinearLayoutManager
        rv_futurechecks.layoutManager = enrouteLinearLayoutManager
        rv_order_items.layoutManager = orderItemsLayoutManager
        rv_needsapproval.hasFixedSize()
        rv_approved.hasFixedSize()
        rv_futurechecks.hasFixedSize()
        rv_order_items.hasFixedSize()
        snoozeTimeList = ArrayList()
        snoozeTimeList.add(SnoozeTimeListDataModel("empty", "Select Snooze time"))
        snoozeTimeList.add(SnoozeTimeListDataModel("20", "20 Min"))
        snoozeTimeList.add(SnoozeTimeListDataModel("40", "40 Min"))
        snoozeTimeList.add(SnoozeTimeListDataModel("today", "Today"))

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun throttleOnlineOrders() {
        /* availability
        * on = "accept"
        * snooze = "snooze"
        * off = "dont_accept"*/
        var deliveryDelay = 0
        var takeoutDelay = 0
        var snoozeTimeId = ""

        dialog_throttleonline_orders = Dialog(this)
        dialog_throttleonline_orders.window
        dialog_throttleonline_orders.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_throttleonline_orders.setContentView(R.layout.dialog_pendingorders)
        dialog_throttleonline_orders.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_throttleonline_orders.setCanceledOnTouchOutside(false)

        val obj = JSONObject()
        obj.put("restaurantId", restaurantId)
        obj.put("userId", mEmployeeId)
        val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
        getThrottleOnlineOrders(finalObject = finalObject)

        tv_pending_on = dialog_throttleonline_orders.findViewById(R.id.tv_pending_on)
        tv_pending_snooze = dialog_throttleonline_orders.findViewById(R.id.tv_pending_snooze)
        tv_pending_off = dialog_throttleonline_orders.findViewById(R.id.tv_pending_off)
        val tv_ok = dialog_throttleonline_orders.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = dialog_throttleonline_orders.findViewById(R.id.tv_cancel) as TextView
        seekbar_takeoutdelay = dialog_throttleonline_orders.findViewById(R.id.seekbar_takeoutdelay)
        seekbar_deliverydelay =
            dialog_throttleonline_orders.findViewById(R.id.seekbar_deliverydelay)
        val tv_takeouttime =
            dialog_throttleonline_orders.findViewById(R.id.tv_takeouttime) as TextView
        val tv_deliverytime =
            dialog_throttleonline_orders.findViewById(R.id.tv_deliverytime) as TextView
        ll_spinner = dialog_throttleonline_orders.findViewById(R.id.ll_spinner)
        sp_snooze = dialog_throttleonline_orders.findViewById(R.id.sp_snooze)
        sp_adapter = SnoozeListAdapter(this, snoozeTimeList)
        sp_snooze.adapter = sp_adapter
        sp_snooze.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = sp_adapter.getSelectedItem(position)
                snoozeTimeId = selectedItem.id
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }

        seekbar_takeoutdelay.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                tv_takeouttime.text = "$progress Min"
                takeoutDelay = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        seekbar_deliverydelay.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                tv_deliverytime.text = "$progress Min"
                deliveryDelay = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        // setting default to "ON"
        tv_pending_on.background =
            ContextCompat.getDrawable(this, R.drawable.blue_background_with_corners)
        tv_pending_on.setTextColor(ContextCompat.getColor(this, R.color.white))
        tv_pending_snooze.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
        tv_pending_off.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
        availability = "accept"
        tv_pending_on.setOnClickListener {
            tv_pending_on.background =
                ContextCompat.getDrawable(this, R.drawable.blue_background_with_corners)
            tv_pending_snooze.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pending_off.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pending_on.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_pending_snooze.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.current_status_close
                )
            )
            tv_pending_off.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            availability = "accept"
            ll_spinner.visibility = View.GONE
        }
        tv_pending_snooze.setOnClickListener {
            tv_pending_on.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pending_snooze.background =
                ContextCompat.getDrawable(this, R.drawable.blue_background_with_corners)
            tv_pending_off.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pending_on.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pending_snooze.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_pending_off.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            availability = "snooze"
            ll_spinner.visibility = View.VISIBLE
        }
        tv_pending_off.setOnClickListener {
            tv_pending_on.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pending_snooze.background =
                ContextCompat.getDrawable(this, R.drawable.white_bg_corner)
            tv_pending_off.background =
                ContextCompat.getDrawable(this, R.drawable.blue_background_with_corners)
            tv_pending_on.setTextColor(ContextCompat.getColor(this, R.color.current_status_close))
            tv_pending_snooze.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.current_status_close
                )
            )
            tv_pending_off.setTextColor(ContextCompat.getColor(this, R.color.white))
            availability = "dont_accept"
            ll_spinner.visibility = View.GONE
        }
        tv_cancel.setOnClickListener {
            dialog_throttleonline_orders.dismiss()
        }
        tv_ok.setOnClickListener {
            when (availability) {
                "accept" -> {
                    val jObj = JSONObject()
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("createdBy", mEmployeeId)
                    jObj.put("acceptOnlineOrders", availability)
                    jObj.put("takeoutDelay", takeoutDelay)
                    jObj.put("deliveryDelay", deliveryDelay)
                    val final_object = JsonParser.parseString(jObj.toString()).asJsonObject
                    updateThrottleOnlineOrders(finalObject = final_object)
                }
                "dont_accept" -> {
                    val jObj = JSONObject()
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("createdBy", mEmployeeId)
                    jObj.put("acceptOnlineOrders", availability)
                    val final_object = JsonParser.parseString(jObj.toString()).asJsonObject
                    updateThrottleOnlineOrders(finalObject = final_object)
                }
                "snooze" -> {
                    if (snoozeTimeId == "empty") {
                        Toast.makeText(
                            this,
                            "Please select a valid snooze time!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val jObj = JSONObject()
                        jObj.put("restaurantId", restaurantId)
                        jObj.put("createdBy", mEmployeeId)
                        jObj.put("acceptOnlineOrders", availability)
                        jObj.put("snoozeTime", snoozeTimeId)
                        jObj.put("takeoutDelay", takeoutDelay)
                        jObj.put("deliveryDelay", deliveryDelay)
                        val final_object = JsonParser.parseString(jObj.toString()).asJsonObject
                        updateThrottleOnlineOrders(finalObject = final_object)
                    }
                }
            }
        }
        dialog_throttleonline_orders.show()
    }

    private fun setUpTabIcons() {
        val needsapprovalView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        val futurechecksView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        val approvedChecksView =
            LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        needapprovalTabHeader = needsapprovalView.findViewById(R.id.ll_tab_header)
        futurechecksTabHeader = futurechecksView.findViewById(R.id.ll_tab_header)
        approvedTabHeader = approvedChecksView.findViewById(R.id.ll_tab_header)
        needapprovalTab = needsapprovalView.findViewById(R.id.tv_tab_name)
        futurechecksTab = futurechecksView.findViewById(R.id.tv_tab_name)
        approvedChecksTab = approvedChecksView.findViewById(R.id.tv_tab_name)
        needapprovalTab.text = getString(R.string.needs_approval)
        futurechecksTab.text = getString(R.string.future_checks)
        approvedChecksTab.text = getString(R.string.approved_checks)
        needapprovalTab.setTextColor(ContextCompat.getColor(this, R.color.white))
        futurechecksTab.setTextColor(ContextCompat.getColor(this, R.color.edit_text_hint))
        approvedChecksTab.setTextColor(ContextCompat.getColor(this, R.color.edit_text_hint))
        needapprovalTabHeader.background = ContextCompat.getDrawable(this, R.drawable.gradient_bg_1)
        futurechecksTabHeader.background = null
        approvedTabHeader.background = null
        ll_needsapproval.visibility = View.VISIBLE
        ll_futurechecks.visibility = View.GONE
        ll_approved.visibility = View.GONE
        ll_approve_options.visibility = View.GONE
        ll_button_options.visibility = View.GONE
        tv_button_send_kitchen.visibility = View.GONE
        val openLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val paidLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val approvedLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        needsapprovalView.layoutParams = openLp
        futurechecksView.layoutParams = paidLp
        approvedChecksView.layoutParams = approvedLp
        tabLayout.addTab(tabLayout.newTab().setCustomView(needsapprovalView), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(approvedChecksView))
        tabLayout.addTab(tabLayout.newTab().setCustomView(futurechecksView))
        selected_tab = "needs_approval"
    }

    private fun getAllOrders() {
        val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId)
        jObj.put("restaurantId", restaurantId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getUnApprovedOrders(finalObject)
        getApprovedOrders(finalObject)
        getScheduledOrders(finalObject)
    }

    private fun getViewOnlineOrder(finalObject: JsonObject) {
        loading_dialog.show()
        viewOrderitemsArrayList!!.clear()
        val api = ApiInterface.create()
        val call = api.viewOrderApi(finalObject)
        call.enqueue(object : Callback<ViewOrderDataResponse> {
            override fun onFailure(call: Call<ViewOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ViewOrderDataResponse>,
                response: Response<ViewOrderDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val orderDetails = respBody.orderDetails!!
                        val orderItems = orderDetails.orderItems
                        if (orderItems!!.size > 0) {
                            for (i in 0 until orderItems.size) {
                                viewOrderitemsArrayList!!.add(orderItems[i])
                            }
                        }
                        val adapter =
                            DeliveryItemListAdapter(
                                this@PendingOrdersActivity,
                                viewOrderitemsArrayList!!
                            )
                        rv_order_items.adapter = adapter
                        adapter.notifyDataSetChanged()
                        tv_pending_credits.text = "%.2f".format(orderDetails.creditsUsed)
                        tv_pending_subtotal.text = "%.2f".format(orderDetails.subTotal)
                        tv_pending_discount.text = "%.2f".format(orderDetails.discountAmount)
                        tv_pending_tax.text = "%.2f".format(orderDetails.taxAmount)
                        tv_pending_tips.text = "%.2f".format(orderDetails.tipAmount)
                        tv_pending_balance_due.text = "%.2f".format(orderDetails.dueAmount)
                        tv_pending_total.text = "%.2f".format(orderDetails.totalAmount)
                        if (orderDetails.customerDetails.toString() != "null") {
                            tv_deliverycreatedon.text =
                                "#${orderDetails.orderNumber}, ${orderDetails.customerDetails!!.firstName} ${orderDetails.customerDetails.lastName}, ${orderDetails.deliveryDetails!!.fullAddress}, ${orderDetails.customerDetails.phoneNumber}."
                        } else {
                            tv_deliverycreatedon.text = "#${orderDetails.orderNumber}."
                        }
                         } else {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getThrottleOnlineOrders(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getThrottleOnlineOrdersApi(finalObject)
        call.enqueue(object : Callback<ThrottleOnlineOrderDataResponse> {
            override fun onFailure(call: Call<ThrottleOnlineOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ThrottleOnlineOrderDataResponse>,
                response: Response<ThrottleOnlineOrderDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        when (resp.acceptOnlineOrders) {
                            "accept" -> {
                                if (resp.deliveryDelay!!.toString() == "null") {
                                    seekbar_deliverydelay.setProgress(0, true)
                                } else {
                                    seekbar_deliverydelay.setProgress(resp.deliveryDelay, true)
                                }
                                if (resp.takeoutDelay!!.toString() == "null") {
                                    seekbar_takeoutdelay.setProgress(0, true)
                                } else {
                                    seekbar_takeoutdelay.setProgress(resp.takeoutDelay, true)
                                }
                                tv_pending_on.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.blue_background_with_corners
                                    )
                                tv_pending_snooze.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.white_bg_corner
                                    )
                                tv_pending_off.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.white_bg_corner
                                    )
                                tv_pending_on.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.white
                                    )
                                )
                                tv_pending_snooze.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.current_status_close
                                    )
                                )
                                tv_pending_off.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.current_status_close
                                    )
                                )
                                availability = "accept"
                                ll_spinner.visibility = View.GONE
                            }
                            "dont_accept" -> {
                                // do nothing
                                tv_pending_on.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.white_bg_corner
                                    )
                                tv_pending_snooze.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.white_bg_corner
                                    )
                                tv_pending_off.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.blue_background_with_corners
                                    )
                                tv_pending_on.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.current_status_close
                                    )
                                )
                                tv_pending_snooze.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.current_status_close
                                    )
                                )
                                tv_pending_off.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.white
                                    )
                                )
                                availability = "dont_accept"
                                ll_spinner.visibility = View.GONE
                            }
                            "snooze" -> {
                                if (resp.deliveryDelay!!.toString() == "null") {
                                    seekbar_deliverydelay.setProgress(0, true)
                                } else {
                                    seekbar_deliverydelay.setProgress(resp.deliveryDelay, true)
                                }
                                if (resp.takeoutDelay!!.toString() == "null") {
                                    seekbar_takeoutdelay.setProgress(0, true)
                                } else {
                                    seekbar_takeoutdelay.setProgress(resp.takeoutDelay, true)
                                }
                                if (resp.snoozeTime.toString() == "null" || resp.snoozeTime.toString() == "") {
                                    sp_snooze.setSelection(0, true)
                                } else {
                                    for (i in 0 until snoozeTimeList.size) {
                                        if (resp.snoozeTime.toString() == snoozeTimeList[i].id) {
                                            sp_snooze.setSelection(i, true)
                                        }
                                    }
                                }
                                tv_pending_on.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.white_bg_corner
                                    )
                                tv_pending_snooze.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.blue_background_with_corners
                                    )
                                tv_pending_off.background =
                                    ContextCompat.getDrawable(
                                        this@PendingOrdersActivity,
                                        R.drawable.white_bg_corner
                                    )
                                tv_pending_on.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.current_status_close
                                    )
                                )
                                tv_pending_snooze.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.white
                                    )
                                )
                                tv_pending_off.setTextColor(
                                    ContextCompat.getColor(
                                        this@PendingOrdersActivity,
                                        R.color.current_status_close
                                    )
                                )
                                availability = "snooze"
                                ll_spinner.visibility = View.VISIBLE
                            }
                        }
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateThrottleOnlineOrders(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.updateThrottleOnlineOrdersApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        dialog_throttleonline_orders.dismiss()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getUnApprovedOrders(finalObject: JsonObject) {
        loading_dialog.show()
        unApprovedList.clear()
        val api = ApiInterface.create()
        val call = api.getUnApprovalOrdersApi(finalObject)
        call.enqueue(object : Callback<NeedApprovalOrdersResponse> {
            override fun onFailure(call: Call<NeedApprovalOrdersResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("getUnApprovalOrdersApi", "error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<NeedApprovalOrdersResponse>,
                response: Response<NeedApprovalOrdersResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        unApprovedList = resp.orders!!
                        if (unApprovedList.size > 0) {
                            val unApprovedListAdapter =
                                UnApprovedListAdapter(this@PendingOrdersActivity, unApprovedList)
                            rv_needsapproval.adapter = unApprovedListAdapter
                            unApprovedListAdapter.notifyDataSetChanged()
                            rv_needsapproval.visibility = View.VISIBLE
                            tv_needsapproval_empty.visibility = View.GONE
                        } else {
                            rv_needsapproval.visibility = View.GONE
                            tv_needsapproval_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_needsapproval.visibility = View.GONE
                        tv_needsapproval_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getScheduledOrders(finalObject: JsonObject) {
        loading_dialog.show()
        scheduledList.clear()
        val api = ApiInterface.create()
        val call = api.getScheduledOrdersApi(finalObject)
        call.enqueue(object : Callback<NeedApprovalOrdersResponse> {
            override fun onFailure(call: Call<NeedApprovalOrdersResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("getScheduledOrdersApi", "error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<NeedApprovalOrdersResponse>,
                response: Response<NeedApprovalOrdersResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        scheduledList = resp.orders!!
                        if (scheduledList.size > 0) {
                            val scheduledListAdapter =
                                ScheduledListAdapter(this@PendingOrdersActivity, scheduledList)
                            rv_futurechecks.adapter = scheduledListAdapter
                            scheduledListAdapter.notifyDataSetChanged()
                            rv_futurechecks.visibility = View.VISIBLE
                            tv_futurechecks_empty.visibility = View.GONE
                        } else {
                            rv_futurechecks.visibility = View.GONE
                            tv_futurechecks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_futurechecks.visibility = View.GONE
                        tv_futurechecks_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getApprovedOrders(finalObject: JsonObject) {
        loading_dialog.show()
        approvedList.clear()
        val api = ApiInterface.create()
        val call = api.getApprovedOrdersApi(finalObject)
        call.enqueue(object : Callback<NeedApprovalOrdersResponse> {
            override fun onFailure(call: Call<NeedApprovalOrdersResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("getApprovedOrdersApi", "error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<NeedApprovalOrdersResponse>,
                response: Response<NeedApprovalOrdersResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        approvedList = resp.orders!!
                        if (approvedList.size > 0) {
                            val approvedListAdapter =
                                ApprovedListAdapter(this@PendingOrdersActivity, approvedList)
                            rv_approved.adapter = approvedListAdapter
                            approvedListAdapter.notifyDataSetChanged()
                            rv_approved.visibility = View.VISIBLE
                            tv_approved_empty.visibility = View.GONE
                        } else {
                            rv_approved.visibility = View.GONE
                            tv_approved_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_approved.visibility = View.GONE
                        tv_approved_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun approvedPendingOnlineOrder(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.approvePendingOnlineOrders(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        isPendingOrderSelected = false
                        isFutureOrderSelected = false
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun confirmOrder(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.confirmScheduledOrderApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("confirmScheduledOrder", "error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        isPendingOrderSelected = false
                        isFutureOrderSelected = false
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun cancelOrder(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.cancelScheduledOrderApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PendingOrdersActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("cancelScheduledOrderApi", "error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        isPendingOrderSelected = false
                        isFutureOrderSelected = false
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@PendingOrdersActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class UnApprovedListAdapter(
        context: Context,
        list: ArrayList<NeedApprovalOrdersItemsResponse>
    ) :
        RecyclerView.Adapter<UnApprovedListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<NeedApprovalOrdersItemsResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.GONE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd,hh:mm aa", Locale.getDefault())
                val date_str = sdf1.format(date!!).toUpperCase(Locale.ROOT)
                holder.tv_order_time.text = date_str
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items!!.size) {
                sbString.append(mList!![position].items!![i].name).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList

            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    isPendingOrderSelected = true
                    mOrderId = mList!![position].orderId.toString()
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].orderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    getViewOnlineOrder(finalObject)
                    ll_approve_options.visibility = View.VISIBLE
                    tv_button_send_kitchen.visibility = View.GONE
                    ll_button_options.visibility = View.GONE
                } else if (mSelectedItem == position) {
                    isPendingOrderSelected = false
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
        }
    }

    inner class ApprovedListAdapter(
        context: Context,
        list: ArrayList<NeedApprovalOrdersItemsResponse>
    ) :
        RecyclerView.Adapter<ApprovedListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<NeedApprovalOrdersItemsResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.GONE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd,hh:mm aa", Locale.getDefault())
                val date_str = sdf1.format(date!!).toUpperCase(Locale.ROOT)
                holder.tv_order_time.text = date_str
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items!!.size) {
                sbString.append(mList!![position].items!![i].name).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList

            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    mOrderId = mList!![position].orderId.toString()
                    isApprovedOrderSelected = true
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].orderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    getViewOnlineOrder(finalObject)
                    if (mList!![position].paymentStatus == 1) {
                        ll_approve_options.visibility = View.GONE
                        ll_button_options.visibility = View.GONE
                        tv_button_send_kitchen.visibility = View.GONE
                    } else {
                        ll_approve_options.visibility = View.GONE
                        ll_button_options.visibility = View.VISIBLE
                        tv_button_send_kitchen.visibility = View.GONE
                    }
                } else if (mSelectedItem == position) {
                    isApprovedOrderSelected = false
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
        }
    }

    inner class ScheduledListAdapter(
        context: Context,
        list: ArrayList<NeedApprovalOrdersItemsResponse>
    ) :
        RecyclerView.Adapter<ScheduledListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<NeedApprovalOrdersItemsResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.VISIBLE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            if (mList!![position].dineInOptionName.toString() == "null") {
                holder.tv_pending_dinein.text = ""
            } else {
                holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            }
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd,hh:mm aa", Locale.getDefault())
                val date_str = sdf1.format(date!!).toUpperCase(Locale.ROOT)
                holder.tv_order_time.text = date_str
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            if (mList!![position].scheduledEndTime.toString() == "null") {
                holder.tv_scheduled_time.text = ""
            } else {
                try {
                    val full_date = mList!![position].scheduledEndTime.toString()
                    val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                    val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                    val date = sdf.parse(full_date)
                    val sdf1 = SimpleDateFormat("MM/dd,hh:mm aa", Locale.getDefault())
                    val date_str = sdf1.format(date!!).toUpperCase(Locale.ROOT)
                    val diffMilliSeconds = date.time - currentTimeMillis
                    var sec = diffMilliSeconds / 1000
                    val hours = sec / 3600
                    val min = (sec % 3600) / 60
                    sec = (sec % 3600) % 60
                    if (diffMilliSeconds < 0) {
                        holder.tv_scheduled_time.text = "0h 0min 0sec"
                    } else {
                        holder.tv_scheduled_time.text =
                            "${abs(hours)}h ${abs(min)}min ${abs(sec)}sec"
                    }
                    Log.d("date_str", date_str)
                } catch (e: Exception) {
                    holder.tv_scheduled_time.text = ""
                    e.printStackTrace()
                }
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items!!.size) {
                sbString.append(mList!![position].items!![i].name).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList

            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    isFutureOrderSelected = true
                    mOrderId = mList!![position].orderId.toString()
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].orderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    getViewOnlineOrder(finalObject)
                    ll_approve_options.visibility = View.VISIBLE
                    ll_button_options.visibility = View.GONE
                    tv_button_send_kitchen.visibility = View.VISIBLE
                } else if (mSelectedItem == position) {
                    isFutureOrderSelected = false
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )

            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
        }
    }

    override fun onBackPressed() {
        if (from_screen == "notifications") {
            val intent = Intent(this@PendingOrdersActivity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        } else if (from_screen == "home") {
            super.onBackPressed()
        }
    }

    private fun resetData() {
        mSelectedItem = -1
        mOrderId = ""
        val adapter = DeliveryItemListAdapter(this@PendingOrdersActivity, ArrayList())
        rv_order_items.adapter = adapter
        adapter.notifyDataSetChanged()
        tv_pending_credits.text = getString(R.string.default_value_0)
        tv_pending_subtotal.text = getString(R.string.default_value_0)
        tv_pending_discount.text = getString(R.string.default_value_0)
        tv_pending_tax.text = getString(R.string.default_value_0)
        tv_pending_tips.text = getString(R.string.default_value_0)
        tv_deliverycreatedon.text = ""
        tv_pending_balance_due.text = getString(R.string.default_value_0)
        tv_pending_total.text = getString(R.string.default_value_0)
        ll_approve_options.visibility = View.GONE
        ll_button_options.visibility = View.GONE
        tv_button_send_kitchen.visibility = View.GONE
    }

    override fun onRestart() {
        super.onRestart()
        try {
            resetData()
            getAllOrders()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}