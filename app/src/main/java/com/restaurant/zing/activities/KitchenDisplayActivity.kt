package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.helpers.RecyclerItemClickListener
import com.restaurant.zing.adapters.AllDayListAdapter
import com.restaurant.zing.adapters.PreparationStationsListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.CenterToast
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.abs
import android.view.Gravity

import android.text.SpannableString
import android.text.style.ForegroundColorSpan


@SuppressLint("SetTextI18n")
class KitchenDisplayActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var rv_kitchen_day_view: RecyclerView
    private lateinit var rv_kitchen_all_day_view: RecyclerView
    private lateinit var ll_show_all_day_view: LinearLayout
    private lateinit var tv_show_all_day_view: TextView
    private lateinit var ll_show_recently_full_filled: LinearLayout
    private lateinit var tv_show_recently_full_filled: TextView
    private lateinit var tv_prep_station: TextView
    private lateinit var tv_empty_orders: TextView
    private lateinit var ll_recall: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var received_order_dialog: Dialog
    private lateinit var mPrepStationsDialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var getPrepStations: HashMap<String, String>
    private var mSelectedFinal: ArrayList<OrderedItemsResponse> = ArrayList()
    private var prepStationsList: ArrayList<PreparationStationsDataResponse> = ArrayList()
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mOrderId = ""
    private var isShowingAllDayView = false
    private var isShowingRecent = false
    private var mSelectedItem = -1
    private var isSelectedForRecall = false
    private var isItemSelected = false
    private var from_screen = ""
    private var prepStationId = ""
    private var prepStationName = ""
    private var mDisplayPrepType = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kitchendisplay)
        try {
            from_screen = intent.getStringExtra("from_screen")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()
        tv_prep_station.text = prepStationName
        if (prepStationId == "") {
            val obj = JSONObject()
            obj.put("restaurantId", restaurantId)
            val jObject = JsonParser.parseString(obj.toString()).asJsonObject
            Log.d("finalObject", jObject.toString())
            getAllPrepStations(jObject)
        } else {
            receivedOrders()
        }
        ll_recall.isEnabled = false
        ll_recall.alpha = 0.5f
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_show_all_day_view.setOnClickListener {
            if (isShowingAllDayView) {
                receivedOrders()
                tv_show_all_day_view.text = getString(R.string.show_all_day_view)
                tv_show_recently_full_filled.text = getString(R.string.show_recently_fulfilled)
                rv_kitchen_day_view.visibility = View.VISIBLE
                rv_kitchen_all_day_view.visibility = View.GONE
                isShowingAllDayView = false
                isShowingRecent = false
                ll_recall.isEnabled = false
                ll_recall.alpha = 0.5f
            } else {
                allDayOrderItems()
                tv_show_all_day_view.text = getString(R.string.hide_all_day_view)
                tv_show_recently_full_filled.text = getString(R.string.show_recently_fulfilled)
                rv_kitchen_day_view.visibility = View.GONE
                rv_kitchen_all_day_view.visibility = View.VISIBLE
                isShowingAllDayView = true
                isShowingRecent = false
                ll_recall.isEnabled = false
                ll_recall.alpha = 0.5f
            }
        }
        ll_show_recently_full_filled.setOnClickListener {
            rv_kitchen_day_view.visibility = View.VISIBLE
            rv_kitchen_all_day_view.visibility = View.GONE
            if (isShowingRecent) {
                receivedOrders()
                tv_show_recently_full_filled.text = getString(R.string.show_recently_fulfilled)
                tv_show_all_day_view.text = getString(R.string.show_all_day_view)
                isShowingRecent = false
                isShowingAllDayView = false
                ll_recall.isEnabled = false
                ll_recall.alpha = 0.5f
            } else {
                mSelectedItem = -1
                recentFulfilledOrders()
                tv_show_recently_full_filled.text = getString(R.string.hide_recently_fulfilled)
                tv_show_all_day_view.text = getString(R.string.show_all_day_view)
                isShowingRecent = true
                isShowingAllDayView = false
                ll_recall.isEnabled = false
                ll_recall.alpha = 0.5f
            }
        }
        ll_recall.setOnClickListener {
            if (isSelectedForRecall) {
                try {
                    val jsonObject = JSONObject()
                    jsonObject.put("restaurantId", restaurantId)
                    jsonObject.put("userId", mEmployeeId)
                    jsonObject.put("orderId", mOrderId)
                    val final_object = JsonParser.parseString(jsonObject.toString()).asJsonObject
                    Log.d("jsonObject", final_object.toString())
                    updateRecallOrder(finalObject = final_object)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(this, "Please select any check to recall", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        getPrepStations = sessionManager.getPrepStation
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        prepStationId = getPrepStations[SessionManager.DEVICE_PREP_STATION_ID]!!
        prepStationName = getPrepStations[SessionManager.DEVICE_PREP_STATION_NAME]!!
        iv_back = findViewById(R.id.iv_back)
        ll_show_all_day_view = findViewById(R.id.ll_show_all_day_view)
        tv_show_all_day_view = findViewById(R.id.tv_show_all_day_view)
        ll_show_recently_full_filled = findViewById(R.id.ll_show_recently_full_filled)
        tv_show_recently_full_filled = findViewById(R.id.tv_show_recently_full_filled)
        ll_recall = findViewById(R.id.ll_recall)
        rv_kitchen_day_view = findViewById(R.id.rv_kitchen_day_view)
        rv_kitchen_all_day_view = findViewById(R.id.rv_kitchen_all_day_view)
        tv_empty_orders = findViewById(R.id.tv_empty_orders)
        tv_prep_station = findViewById(R.id.tv_prep_station)
        val dayViewLayoutManager =
            StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL)
        rv_kitchen_day_view.layoutManager = dayViewLayoutManager
        val allDayViewLayoutManager =
            StaggeredGridLayoutManager(7, LinearLayoutManager.HORIZONTAL)
        rv_kitchen_all_day_view.layoutManager = allDayViewLayoutManager
        rv_kitchen_day_view.itemAnimator = DefaultItemAnimator()
        rv_kitchen_all_day_view.itemAnimator = DefaultItemAnimator()
        rv_kitchen_day_view.hasFixedSize()
        rv_kitchen_all_day_view.hasFixedSize()
        rv_kitchen_day_view.isNestedScrollingEnabled = false
        rv_kitchen_all_day_view.isNestedScrollingEnabled = false
    }

    private fun receivedOrders() {
        val jObj = JSONObject()
        jObj.put("restaurantId", restaurantId)
        jObj.put("prepStationId", prepStationId)
        jObj.put("userId", mEmployeeId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        Log.d("finalObject", finalObject.toString())
        getReceivedOrders(finalObject = finalObject)
    }

    private fun allDayOrderItems() {
        val jObj = JSONObject()
        jObj.put("restaurantId", restaurantId)
        jObj.put("prepStationId", prepStationId)
        jObj.put("userId", mEmployeeId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        Log.d("finalObject", finalObject.toString())
        getAllDayOrders(finalObject = finalObject)
    }

    private fun recentFulfilledOrders() {
        val jObj = JSONObject()
        jObj.put("restaurantId", restaurantId)
        jObj.put("prepStationId", prepStationId)
        jObj.put("userId", mEmployeeId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        Log.d("finalObject", finalObject.toString())
        getRecentFulFilledOrders(finalObject = finalObject)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showPrepStationsDialog() {
        mPrepStationsDialog = Dialog(this@KitchenDisplayActivity)
        mPrepStationsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@KitchenDisplayActivity)
                .inflate(R.layout.dialog_preparation_stations, null)
        mPrepStationsDialog.setContentView(view)
        mPrepStationsDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        mPrepStationsDialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mPrepStationsDialog.setCanceledOnTouchOutside(false)
        val tv_cancel: TextView = view.findViewById(R.id.tv_cancel)
        val tv_ok: TextView = view.findViewById(R.id.tv_ok)
        val sp_prep_stations: Spinner = view.findViewById(R.id.sp_prep_stations)
        tv_cancel.setOnClickListener {
            mPrepStationsDialog.dismiss()
            onBackPressed()
        }
        val adapter = PreparationStationsListAdapter(this, prepStationsList)
        sp_prep_stations.adapter = adapter
        sp_prep_stations.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = adapter.getSelectedItem(position)
                prepStationId = selectedItem.id.toString()
                prepStationName = selectedItem.stationName.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_ok.setOnClickListener {
            if (prepStationId == "" || prepStationId == "empty") {
                Toast.makeText(this, "Please select a preparation station!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                sessionManager.savePrepStation(prepStationId, prepStationName)
                tv_prep_station.text = prepStationName
                CenterToast().showToast(this@KitchenDisplayActivity, "Saved successfully.")
                receivedOrders()
                mPrepStationsDialog.dismiss()
            }
        }
        mPrepStationsDialog.setOnCancelListener {
            finish()
        }
        mPrepStationsDialog.show()
    }

    private fun showReceivedOrderDialog(response: GetOrdersDataResponse) {
        received_order_dialog = Dialog(this)
        var itemsAdapter: ItemsListClickableAdapter? = null
        var mSelected: ArrayList<OrderedItemsResponse>
        mSelectedFinal = ArrayList()
        var isSelectedAll = false
        received_order_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.received_order_dialog, null)
        received_order_dialog.setContentView(view)
        received_order_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        received_order_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        received_order_dialog.window!!.setGravity(Gravity.CENTER)
        received_order_dialog.setCanceledOnTouchOutside(false)
        val ll_header = view.findViewById(R.id.ll_header) as LinearLayout
        val tv_check_number = view.findViewById(R.id.tv_check_number) as TextView
        val tv_time = view.findViewById(R.id.tv_time) as TextView
        val tv_table_number = view.findViewById(R.id.tv_table_number) as TextView
        val tv_name = view.findViewById(R.id.tv_name) as TextView
        val rv_items = view.findViewById(R.id.rv_items) as RecyclerView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_select_all = view.findViewById(R.id.tv_select_all) as TextView
        val tv_fulfill = view.findViewById(R.id.tv_fulfill) as TextView
        val tv_un_fulfill = view.findViewById(R.id.tv_un_fulfill) as TextView
        val tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
        val tv_recalled = view.findViewById(R.id.tv_recalled) as TextView
        val tv_order_from = view.findViewById(R.id.tv_order_from) as TextView
        val itemsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_items.layoutManager = itemsLayoutManager
        rv_items.hasFixedSize()
        try {
            val full_date = response.created.toString()
            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.parse(full_date)
            val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
            val time = stf.format(date!!)
            tv_time.text = time
        } catch (e: Exception) {
            tv_time.text = ""
            e.printStackTrace()
        }
        tv_check_number.text = "#" + response.checkNumber.toString()
        tv_order_number.text = "Order#" + response.orderNumber.toString()
        tv_name.text = response.name.toString()
        if (response.tableNumber.toString() == "" || response.tableNumber.toString() == "null") {
            tv_table_number.visibility = View.GONE
        } else {
            tv_table_number.visibility = View.VISIBLE
            tv_table_number.text =
                "Table " + response.tableNumber.toString()
        }
        when (response.status) {
            0 -> {
                tv_recalled.visibility = View.GONE
                ll_header.setBackgroundColor(
                    ContextCompat.getColor(this, R.color.kitchen_display_recalled)
                )
            }
            1 -> {
                tv_recalled.visibility = View.GONE
                ll_header.setBackgroundColor(
                    ContextCompat.getColor(this, R.color.kitchen_display_recalled)
                )
            }
            2 -> {
                tv_recalled.visibility = View.GONE
                ll_header.setBackgroundColor(
                    ContextCompat.getColor(this, R.color.kitchen_display_fulfilled)
                )
            }
            3 -> {
                tv_recalled.visibility = View.GONE
                ll_header.setBackgroundColor(
                    ContextCompat.getColor(this, R.color.kitchen_display_recalled)
                )
            }
            4 -> {
                tv_recalled.visibility = View.GONE
                ll_header.setBackgroundColor(
                    ContextCompat.getColor(this, R.color.item_header_kitchendisplay)
                )
            }
        }
        if (response.isRecallOrder!!) {
            tv_recalled.visibility = View.VISIBLE
            ll_header.setBackgroundColor(
                ContextCompat.getColor(this, R.color.kitchen_display_recalled_new)
            )
        }
        when {
            response.placedFrom.equals("pos", true) -> {
                if (response.deviceName == null) {
                    tv_order_from.text = "POS"
                } else {
                    val header = SpannableString("POS - ${response.deviceName}")
                    header.setSpan(
                        ForegroundColorSpan(getColor(R.color.button_black)),
                        3,
                        header.length,
                        0
                    )
                    tv_order_from.text = header
                }
            }
            response.placedFrom.equals("waiter", true) -> {
                if (response.deviceName == null) {
                    tv_order_from.text = "Waiter"
                } else {
                    val header = SpannableString("Waiter - ${response.deviceName}")
                    header.setSpan(
                        ForegroundColorSpan(getColor(R.color.button_black)),
                        6,
                        header.length,
                        0
                    )
                    tv_order_from.text = header
                }
            }
            response.placedFrom.equals("kiosk", true) -> {
                if (response.deviceName == null) {
                    tv_order_from.text = "Kiosk"
                } else {
                    val header = SpannableString("Kiosk - ${response.deviceName}")
                    header.setSpan(
                        ForegroundColorSpan(getColor(R.color.button_black)),
                        5,
                        header.length,
                        0
                    )
                    tv_order_from.text = header
                }
            }
            response.placedFrom.equals("online", true) -> {
                tv_order_from.text = "Online"
            }
            response.placedFrom.equals("ubereats", true) -> {
                tv_order_from.text = "Uber Eats"
            }
            response.placedFrom.equals("doordash", true) -> {
                tv_order_from.text = "Doordash"
            }
            response.placedFrom.equals("qr_order", true) -> {
                tv_order_from.text = "QR Order"
            }
            else -> {
                tv_order_from.text = response.placedFrom
            }
        }
        if (response.items == null) {
            // do nothing
        } else {
            if (response.items!!.isEmpty()) {
                rv_items.visibility = View.GONE
            } else {
                rv_items.visibility = View.VISIBLE
            }
            itemsAdapter = ItemsListClickableAdapter(this, response.items!!)
            rv_items.adapter = itemsAdapter
            itemsAdapter.notifyDataSetChanged()
        }
        tv_cancel.setOnClickListener {
            received_order_dialog.dismiss()
        }
        tv_select_all.setOnClickListener {
            if (isSelectedAll) {
                isSelectedAll = false
                tv_select_all.text = getString(R.string.select_all)
                itemsAdapter!!.clearAll()
            } else {
                isSelectedAll = true
                tv_select_all.text = getString(R.string.deselect_all)
                itemsAdapter!!.selectAll()
            }
        }
        tv_fulfill.setOnClickListener {
            val array = JSONArray()
            mSelected = itemsAdapter!!.getSelectedList()
            if (mSelected.size > 0) {
                for (i in 0 until mSelected.size) {
                    array.put(mSelected[i].id.toString())
                }
                val jObj = JSONObject()
                jObj.put("restaurantId", restaurantId)
                jObj.put("userId", mEmployeeId)
                jObj.put("orderId", mOrderId)
                jObj.put("itemsList", array)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.d("finalobject", finalObject.toString())
                fulFilledOrderApi(finalObject = finalObject)
            } else {
                Toast.makeText(this, "Please select any item!", Toast.LENGTH_SHORT).show()
            }
        }
        tv_un_fulfill.setOnClickListener {
            val array = JSONArray()
            mSelected = itemsAdapter!!.getSelectedList()
            if (mSelected.size > 0) {
                for (i in 0 until mSelected.size) {
                    array.put(mSelected[i].id.toString())
                }
                val jObj = JSONObject()
                jObj.put("restaurantId", restaurantId)
                jObj.put("userId", mEmployeeId)
                jObj.put("orderId", mOrderId)
                jObj.put("itemsList", array)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.d("finalobject", finalObject.toString())
                unFulFilledOrderApi(finalObject = finalObject)
            } else {
                Toast.makeText(this, "Please select any item!", Toast.LENGTH_SHORT).show()
            }
        }
        rv_items.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val data = itemsAdapter!!.getItem(position)
                        mSelected = itemsAdapter.getSelectedList()
                        if (mSelected.size > 0) {
                            if (mSelected.contains(data)) {
                                mSelected.remove(data)
                            } else {
                                mSelected.add(data)
                            }
                        } else {
                            mSelected.add(data)
                        }
                        itemsAdapter.selectedList(mSelected)
                    }
                })
        )
        received_order_dialog.show()
    }

    private fun getReceivedOrders(finalObject: JsonObject) {
        if (from_screen == "notifications") {
            // don't show loader
        } else {
            loading_dialog.show()
        }
        val api = ApiInterface.create()
        val call = api.getReceivedOrdersApi(finalObject)
        call.enqueue(object : Callback<ReceivedOrderDataResponse> {
            override fun onFailure(call: Call<ReceivedOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ReceivedOrderDataResponse>,
                response: Response<ReceivedOrderDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    Log.d(
                        "result kitc",
                        resp.responseStatus.toString() + "--" + resp.orders.toString()
                    )
                    if (resp.responseStatus == 1) {
                        val orders = resp.orders!!
                        mDisplayPrepType = if (resp.displayPrepType == null) {
                            false
                        } else {
                            resp.displayPrepType!!
                        }
                        if (orders.size > 0) {
                            rv_kitchen_day_view.layoutManager = null
                            val dayViewLayoutManager =
                                StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL)
                            rv_kitchen_day_view.layoutManager = dayViewLayoutManager
                            val adapter =
                                ReceivedOrdersAdapter(this@KitchenDisplayActivity, orders, false)
                            rv_kitchen_day_view.adapter = adapter
                            adapter.notifyDataSetChanged()
                            tv_empty_orders.text = ""
                            tv_empty_orders.visibility = View.GONE
                            rv_kitchen_day_view.visibility = View.VISIBLE
                        } else {
                            tv_empty_orders.text = getString(R.string.no_orders_available)
                            tv_empty_orders.visibility = View.VISIBLE
                            rv_kitchen_day_view.visibility = View.GONE
                        }
                    } else {
                        tv_empty_orders.text = getString(R.string.no_orders_available)
                        tv_empty_orders.visibility = View.VISIBLE
                        rv_kitchen_day_view.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getRecentFulFilledOrders(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getRecentFulFilledOrdersApi(finalObject)
        call.enqueue(object : Callback<ReceivedOrderDataResponse> {
            override fun onFailure(call: Call<ReceivedOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ReceivedOrderDataResponse>,
                response: Response<ReceivedOrderDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val orders = resp.orders!!
                        if (orders.size > 0) {
                            rv_kitchen_day_view.layoutManager = null
                            val dayViewLayoutManager =
                                StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL)
                            rv_kitchen_day_view.layoutManager = dayViewLayoutManager
                            val adapter =
                                ReceivedOrdersAdapter(this@KitchenDisplayActivity, orders, true)
                            rv_kitchen_day_view.adapter = adapter
                            adapter.notifyDataSetChanged()
                            tv_empty_orders.text = ""
                            tv_empty_orders.visibility = View.GONE
                            rv_kitchen_day_view.visibility = View.VISIBLE
                        } else {
                            tv_empty_orders.text = getString(R.string.no_recent_orders_available)
                            tv_empty_orders.visibility = View.VISIBLE
                            rv_kitchen_day_view.visibility = View.GONE
                        }
                    } else {
                        tv_empty_orders.text = getString(R.string.no_recent_orders_available)
                        tv_empty_orders.visibility = View.VISIBLE
                        rv_kitchen_day_view.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllDayOrders(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getAllDayOrdersApi(finalObject)
        call.enqueue(object : Callback<AllDayOrderDataResponse> {
            override fun onFailure(call: Call<AllDayOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDayOrderDataResponse>,
                response: Response<AllDayOrderDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val orders = resp.orders!!
                        if (orders.size > 0) {
                            rv_kitchen_all_day_view.layoutManager = null
                            val allDayViewLayoutManager =
                                StaggeredGridLayoutManager(10, LinearLayoutManager.HORIZONTAL)
                            rv_kitchen_all_day_view.layoutManager = allDayViewLayoutManager
                            val adapter = AllDayListAdapter(this@KitchenDisplayActivity, orders)
                            rv_kitchen_all_day_view.adapter = adapter
                            adapter.notifyDataSetChanged()
                            tv_empty_orders.text = ""
                            tv_empty_orders.visibility = View.GONE
                            rv_kitchen_all_day_view.visibility = View.VISIBLE
                        } else {
                            tv_empty_orders.text = getString(R.string.no_orders_available)
                            tv_empty_orders.visibility = View.VISIBLE
                            rv_kitchen_all_day_view.visibility = View.GONE
                        }
                    } else {
                        tv_empty_orders.text = getString(R.string.no_orders_available)
                        tv_empty_orders.visibility = View.VISIBLE
                        rv_kitchen_all_day_view.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun fulFilledOrderApi(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.fulFilledOrderApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        receivedOrders()
                        tv_show_recently_full_filled.text =
                            getString(R.string.show_recently_fulfilled)
                        tv_show_all_day_view.text = getString(R.string.show_all_day_view)
                        rv_kitchen_day_view.visibility = View.VISIBLE
                        rv_kitchen_all_day_view.visibility = View.GONE
                        Toast.makeText(
                            this@KitchenDisplayActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        received_order_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@KitchenDisplayActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("error resp", resp.result.toString())
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun unFulFilledOrderApi(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.unFulFilledOrderApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        receivedOrders()
                        tv_show_recently_full_filled.text =
                            getString(R.string.show_recently_fulfilled)
                        tv_show_all_day_view.text = getString(R.string.show_all_day_view)
                        rv_kitchen_day_view.visibility = View.VISIBLE
                        rv_kitchen_all_day_view.visibility = View.GONE
                        Toast.makeText(
                            this@KitchenDisplayActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        received_order_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@KitchenDisplayActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateRecallOrder(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
//        val call = api.updateRecallOrderApi(finalObject)
        val call = api.updateRecallOrderNewApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        ll_recall.isEnabled = false
                        ll_recall.alpha = 0.5f
                        mSelectedItem = -1
                        receivedOrders()
                        tv_show_recently_full_filled.text =
                            getString(R.string.show_recently_fulfilled)
                        tv_show_all_day_view.text = getString(R.string.show_all_day_view)
                        rv_kitchen_day_view.visibility = View.VISIBLE
                        rv_kitchen_all_day_view.visibility = View.GONE
                        isShowingAllDayView = false
                        isShowingRecent = false
                    } else {
                        Log.e(
                            "Error_message",
                            respBody.responseStatus!!.toString() + "--" + respBody.result
                        )
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllPrepStations(finalObject: JsonObject) {
        loading_dialog.show()
        prepStationsList = ArrayList()
        val dummy = PreparationStationsDataResponse()
        dummy.id = "empty"
        dummy.stationName = "Select a preparation station"
        dummy.uniqueNumber = "empty"
        prepStationsList.add(dummy)
        val api = ApiInterface.create()
        val call = api.getAllPrepStationsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@KitchenDisplayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus!! == 1) {
                        val list = resp.stationsList!!
                        for (i in 0 until list.size) {
                            prepStationsList.add(list[i])
                        }
                    } else {
                        Toast.makeText(
                            this@KitchenDisplayActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    showPrepStationsDialog()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class ReceivedOrdersAdapter(
        context: Context,
        list: ArrayList<GetOrdersDataResponse>,
        isRecalling: Boolean
    ) :
        RecyclerView.Adapter<ReceivedOrdersAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mItemsList: ArrayList<GetOrdersDataResponse>? = null
        private var isRecalling: Boolean? = null

        init {
            this.mContext = context
            this.mItemsList = list
            this.isRecalling = isRecalling
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.received_order_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mItemsList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            /* status
            * 0 - stay
            * 1 - send
            * 2 - fulfilled entire check
            * 3 - payment done
            * 4 - cancelled order
            * 5 - recall */
            var date: Date? = null
            val itemsLayoutManager = LinearLayoutManager(mContext!!, RecyclerView.VERTICAL, false)
            holder.rv_items.layoutManager = itemsLayoutManager
            holder.rv_items.hasFixedSize()
            holder.tv_check_number.text = "#" + mItemsList!![position].checkNumber.toString()
            holder.tv_order_number.text = "Order#" + mItemsList!![position].orderNumber.toString()
            if (mItemsList!![position].status == 2) {
                try {
                    val full_date = mItemsList!![position].fullfilledON.toString()
                    val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                    val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                    date = sdf.parse(full_date)
                    val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                    val time = stf.format(date!!)
                    holder.tv_time.text = time
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                try {
                    val full_date = mItemsList!![position].created.toString()
                    val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                    val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                    date = sdf.parse(full_date)
                    val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                    val time = stf.format(date!!)
                    holder.tv_time.text = time
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            if (mItemsList!![position].tableNumber.toString() == "" || mItemsList!![position].tableNumber.toString() == "null") {
                holder.tv_table_number.visibility = View.GONE
            } else {
                holder.tv_table_number.visibility = View.VISIBLE
                holder.tv_table_number.text =
                    "Table " + mItemsList!![position].tableNumber.toString()
            }
            holder.tv_name.text = mItemsList!![position].name.toString()
            holder.tv_name.isSelected = true
            when (mItemsList!![position].status) {
                0 -> {
                    holder.ll_header.setBackgroundColor(
                        ContextCompat.getColor(mContext!!, R.color.kitchen_display_recalled)
                    )
                    holder.tv_recalled.visibility = View.GONE
                }
                1 -> {
                    holder.ll_header.setBackgroundColor(
                        ContextCompat.getColor(mContext!!, R.color.kitchen_display_recalled)
                    )
                    holder.tv_recalled.visibility = View.GONE
                }
                2 -> {
                    holder.ll_header.setBackgroundColor(
                        ContextCompat.getColor(mContext!!, R.color.kitchen_display_fulfilled)
                    )
                    holder.tv_recalled.visibility = View.GONE
                }
                3 -> {
                    holder.ll_header.setBackgroundColor(
                        ContextCompat.getColor(mContext!!, R.color.kitchen_display_recalled)
                    )
                    holder.tv_recalled.visibility = View.GONE
                }
                4 -> {
                    holder.ll_header.setBackgroundColor(
                        ContextCompat.getColor(mContext!!, R.color.item_header_kitchendisplay)
                    )
                    holder.tv_recalled.visibility = View.GONE
                }
            }
            if (!isRecalling!!) {
                if (mItemsList!![position].isRecallOrder!!) {
                    holder.ll_header.setBackgroundColor(
                        ContextCompat.getColor(mContext!!, R.color.kitchen_display_recalled_new)
                    )
                    holder.tv_recalled.visibility = View.VISIBLE
                }
            }
            when {
                mItemsList!![position].placedFrom.equals("pos", true) -> {
                    if (mItemsList!![position].deviceName == null) {
                        holder.tv_order_from.text = "POS"
                    } else {
                        val header = SpannableString("POS - ${mItemsList!![position].deviceName}")
//                        header.setSpan(RelativeSizeSpan(1f), 3, header.length, 0)
                        header.setSpan(
                            ForegroundColorSpan(getColor(R.color.button_black)),
                            3,
                            header.length,
                            0
                        )
                        holder.tv_order_from.text = header
                    }
                }
                mItemsList!![position].placedFrom.equals("waiter", true) -> {
                    if (mItemsList!![position].deviceName == null) {
                        holder.tv_order_from.text = "Waiter"
                    } else {
                        val header =
                            SpannableString("Waiter - ${mItemsList!![position].deviceName}")
                        header.setSpan(
                            ForegroundColorSpan(getColor(R.color.button_black)),
                            6,
                            header.length,
                            0
                        )
                        holder.tv_order_from.text = header
                    }
                }
                mItemsList!![position].placedFrom.equals("kiosk", true) -> {
                    if (mItemsList!![position].deviceName == null) {
                        holder.tv_order_from.text = "Kiosk"
                    } else {
                        val header = SpannableString("Kiosk - ${mItemsList!![position].deviceName}")
                        header.setSpan(
                            ForegroundColorSpan(getColor(R.color.button_black)),
                            5,
                            header.length,
                            0
                        )
                        holder.tv_order_from.text = header
                    }
                }
                mItemsList!![position].placedFrom.equals("online", true) -> {
                    holder.tv_order_from.text = "Online"
                }
                mItemsList!![position].placedFrom.equals("ubereats", true) -> {
                    holder.tv_order_from.text = "Uber Eats"
                }
                mItemsList!![position].placedFrom.equals("doordash", true) -> {
                    holder.tv_order_from.text = "Doordash"
                }
                mItemsList!![position].placedFrom.equals("qr_order", true) -> {
                    holder.tv_order_from.text = "QR Order"
                }
                else -> {
                    holder.tv_order_from.text = mItemsList!![position].placedFrom
                }
            }
            if (mItemsList!![position].items == null) {
                // do nothing
            } else {
                if (mItemsList!![position].items!!.isEmpty()) {
                    holder.rv_items.visibility = View.GONE
                } else {
                    holder.rv_items.visibility = View.VISIBLE
                }
                val itemsAdapter =
                    ItemsListAdapter(mContext!!, mItemsList!![position].items!!, date)
                holder.rv_items.adapter = itemsAdapter
                itemsAdapter.notifyDataSetChanged()
            }
            holder.cv_header.setOnClickListener {
                if (isRecalling!!) {
                    if (mSelectedItem == -1 || mSelectedItem != position) {
                        mOrderId = mItemsList!![position].orderId.toString()
                        mSelectedItem = position
                        isSelectedForRecall = true
                        ll_recall.alpha = 1.0f
                        ll_recall.isEnabled = true
                    } else if (mSelectedItem == position) {
                        mOrderId = ""
                        mSelectedItem = -1
                        ll_recall.isEnabled = false
                        ll_recall.alpha = 0.5f
                        isSelectedForRecall = false
                    }
                    notifyDataSetChanged()
                } else {
                    mOrderId = mItemsList!![position].orderId.toString()
                    showReceivedOrderDialog(mItemsList!![position])
                }
            }
            if (mSelectedItem == position) {
                holder.cv_header.cardElevation = 3.0f
                holder.ll_header.setBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.kitchen_display_recalled)
                )
            } else {
                holder.cv_header.cardElevation = 10.0f
                when (mItemsList!![position].status) {
                    0 -> {
                        holder.ll_header.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.kitchen_display_recalled
                            )
                        )
                    }
                    1 -> {
                        holder.ll_header.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.kitchen_display_recalled
                            )
                        )
                    }
                    2 -> {
                        holder.ll_header.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.kitchen_display_fulfilled
                            )
                        )
                    }
                    3 -> {
                        holder.ll_header.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.kitchen_display_recalled
                            )
                        )
                    }
                    4 -> {
                        holder.ll_header.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.item_header_kitchendisplay
                            )
                        )
                    }
                }
                if (!isRecalling!!) {
                    if (mItemsList!![position].isRecallOrder!!) {
                        holder.ll_header.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext!!,
                                R.color.kitchen_display_recalled_new
                            )
                        )
                        holder.tv_recalled.visibility = View.VISIBLE
                    }
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val cv_header = view.findViewById(R.id.cv_header) as CardView
            val ll_header = view.findViewById(R.id.ll_header) as LinearLayout
            val tv_order_from = view.findViewById(R.id.tv_order_from) as TextView
            val tv_check_number = view.findViewById(R.id.tv_check_number) as TextView
            val tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            val tv_time = view.findViewById(R.id.tv_time) as TextView
            val tv_table_number = view.findViewById(R.id.tv_table_number) as TextView
            val tv_name = view.findViewById(R.id.tv_name) as TextView
            val tv_recalled = view.findViewById(R.id.tv_recalled) as TextView
            val rv_items = view.findViewById(R.id.rv_items) as RecyclerView
        }
    }

    inner class ItemsListAdapter(
        context: Context,
        modifiers: ArrayList<OrderedItemsResponse>,
        date: Date?
    ) :
        RecyclerView.Adapter<ItemsListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<OrderedItemsResponse>? = null
        private var mSelectedList: ArrayList<OrderedItemsResponse> = ArrayList()
        private var mDate: Date? = null

        init {
            this.context = context
            this.mList = modifiers
            this.mDate = date
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.received_order_sub_list_item_with_timer, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder, @SuppressLint("RecyclerView") position: Int
        ) {
            /*status
            * 0 - stay
            * 1 - send
            * 2 - fulfilled
            * 3 - recalled */
            val modifiersLayoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_modifiers.layoutManager = modifiersLayoutManager
            holder.rv_modifiers.hasFixedSize()
            val splRequestLayoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_spl_requests.layoutManager = splRequestLayoutManager
            holder.rv_spl_requests.hasFixedSize()
            if (mList!![position].modifiersList!!.isEmpty()) {
                holder.rv_modifiers.visibility = View.GONE
            } else {
                holder.rv_modifiers.visibility = View.VISIBLE
            }
            if (mList!![position].specialRequestList!!.isEmpty()) {
                holder.rv_spl_requests.visibility = View.GONE
            } else {
                holder.rv_spl_requests.visibility = View.VISIBLE
            }
            holder.tv_item_name.text = mList!![position].name.toString()
            holder.tv_item_quantity.text = mList!![position].quantity.toString()
            if (mList!![position].voidStatus!! && mList!![position].quantity == 0) {
                holder.tv_item_name.text =
                    mList!![position].name.toString() + " " + context!!.getString(R.string.voided)
                holder.tv_item_name.paintFlags =
                    holder.tv_item_name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                holder.tv_item_quantity.paintFlags =
                    holder.tv_item_name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                holder.tv_delay.paintFlags =
                    holder.tv_delay.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
            if (mDisplayPrepType) {
                holder.tv_delayed.visibility = View.VISIBLE
                /* setting image layout_gravity to center */
                val params = holder.iv_fulfilled.layoutParams as LinearLayout.LayoutParams
                params.gravity = Gravity.CENTER
                holder.iv_fulfilled.layoutParams = params
            } else {
                holder.tv_delayed.visibility = View.GONE
            }
            when (mList!![position].status) {
                0 -> {
                    holder.iv_fulfilled.visibility = View.GONE
                    if (mDisplayPrepType) {
                        holder.rl_counter.visibility = View.VISIBLE
                        holder.status_layout.visibility = View.VISIBLE
                    } else {
                        holder.rl_counter.visibility = View.GONE
                        holder.status_layout.visibility = View.GONE
                    }
                }
                1 -> {
                    holder.iv_fulfilled.visibility = View.GONE
                    if (mDisplayPrepType) {
                        holder.rl_counter.visibility = View.VISIBLE
                        holder.status_layout.visibility = View.VISIBLE
                    } else {
                        holder.rl_counter.visibility = View.GONE
                        holder.status_layout.visibility = View.GONE
                    }
                }
                2 -> {
                    holder.iv_fulfilled.visibility = View.VISIBLE
                    holder.status_layout.visibility = View.VISIBLE
                    holder.rl_counter.visibility = View.GONE
                }
                3 -> {
                    holder.iv_fulfilled.visibility = View.GONE
                    if (mDisplayPrepType) {
                        holder.rl_counter.visibility = View.VISIBLE
                        holder.status_layout.visibility = View.VISIBLE
                    } else {
                        holder.rl_counter.visibility = View.GONE
                        holder.status_layout.visibility = View.GONE
                    }
                }
            }
            holder.tv_item_quantity.visibility = View.VISIBLE
            if (mDisplayPrepType) {
                val timer = Timer()
                timer.scheduleAtFixedRate(object : TimerTask() {
                    override fun run() {
                        val currentDate = Date()
                        /* Calculating time diff between ordered time and current device time */
                        /* val timeDiffInHours = ((currentDate.time - mDate!!.time) / (1000 * 60 * 60)) % 60
                           val timeDiffInMinutes = ((currentDate.time - mDate!!.time) / (1000 * 60)) % 60 */
                        val timeDiffInMinutes =
                            TimeUnit.MILLISECONDS.toMinutes((currentDate.time - mDate!!.time))
                        Log.d(
                            "timeInSeconds",
                            "${timeDiffInMinutes}====${(mList!![position].preparationTime!! % 3600) / 60}"
                        )
                        /* Converting preparation time to minutes for calculation */
                        val timeDiff =
                            ((mList!![position].preparationTime!! % 3600) / 60) - timeDiffInMinutes
                        holder.tv_delay.text = timeDiff.toString()
                        holder.progressBar.max = 60
                        holder.progressBar.setProgress(abs(timeDiff).toInt(), true)
                        if (mList!![position].status != 2) {
                            if (timeDiff < 0) {
                                holder.tv_item_name.setTextColor(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.current_status_open
                                    )
                                )
                                holder.progressBar.progressTintList = ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.progress_red
                                    )
                                )
                            } else {
                                holder.tv_item_name.setTextColor(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.edit_text_hint
                                    )
                                )
                                holder.progressBar.progressTintList = ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.progress_green
                                    )
                                )
                            }
                            if (mList!![position].voidStatus!!) {
                                holder.tv_delay.text = "0"
                                holder.tv_item_name.setTextColor(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.edit_text_hint
                                    )
                                )
                                holder.progressBar.progressTintList = ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.progress_transparent
                                    )
                                )
                            }
                        }
                    }
                }, 100, 60 * 1000)
            }
            val modifiersAdapter =
                ModifiersListAdapter(
                    context!!,
                    mList!![position].modifiersList!!,
                    false
                )
            holder.rv_modifiers.adapter = modifiersAdapter
            modifiersAdapter.notifyDataSetChanged()
            val requestsAdapter = SplRequestsAdapter(
                context!!,
                mList!![position].specialRequestList!!,
                false
            )
            holder.rv_spl_requests.adapter = requestsAdapter
            requestsAdapter.notifyDataSetChanged()
        }

        fun clearAll() {
            mSelectedList.clear()
            notifyDataSetChanged()
        }

        fun selectedList(selectedList: ArrayList<OrderedItemsResponse>) {
            this.mSelectedList = selectedList
            notifyDataSetChanged()
        }

        fun getSelectedList(): ArrayList<OrderedItemsResponse> {
            return mSelectedList
        }

        fun getItem(position: Int): OrderedItemsResponse {
            return mList!![position]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val ll_header = view.findViewById(R.id.ll_header) as LinearLayout
            val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            val tv_item_quantity = view.findViewById(R.id.tv_item_quantity) as TextView
            val iv_fulfilled = view.findViewById(R.id.iv_fulfilled) as ImageView
            val rv_spl_requests = view.findViewById(R.id.rv_spl_requests) as RecyclerView
            val rv_modifiers = view.findViewById(R.id.rv_modifiers) as RecyclerView
            val rl_counter = view.findViewById(R.id.rl_counter) as RelativeLayout
            val progressBar = view.findViewById(R.id.progressBar_determinate) as ProgressBar
            val tv_delay = view.findViewById(R.id.tv_delay) as TextView
            val status_layout = view.findViewById(R.id.status_layout) as LinearLayout
            val tv_delayed = view.findViewById(R.id.tv_delayed) as TextView
        }
    }

    inner class ItemsListClickableAdapter(
        context: Context,
        modifiers: ArrayList<OrderedItemsResponse>
    ) :
        RecyclerView.Adapter<ItemsListClickableAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<OrderedItemsResponse>? = null
        private var mSelectedList: ArrayList<OrderedItemsResponse> = ArrayList()

        init {
            this.context = context
            this.mList = modifiers
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.received_order_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder, position: Int
        ) {
            /* status
            * 0 - stay
            * 1 - send
            * 2 - fullfilled
            * 3 - recalled */
            val modifiersLayoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_modifiers.layoutManager = modifiersLayoutManager
            holder.rv_modifiers.hasFixedSize()
            val splRequestLayoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_spl_requests.layoutManager = splRequestLayoutManager
            holder.rv_spl_requests.hasFixedSize()
            if (mList!![position].modifiersList!!.isEmpty()) {
                holder.rv_modifiers.visibility = View.GONE
            } else {
                holder.rv_modifiers.visibility = View.VISIBLE
            }
            if (mList!![position].specialRequestList!!.isEmpty()) {
                holder.rv_spl_requests.visibility = View.GONE
            } else {
                holder.rv_spl_requests.visibility = View.VISIBLE
            }
            holder.tv_item_name.text = mList!![position].name.toString()
            holder.tv_item_quantity.text = mList!![position].quantity.toString()
            if (mList!![position].specialInstructions == "") {
                holder.tv_spl_instructions.visibility = View.GONE
            } else {
                holder.tv_spl_instructions.visibility = View.VISIBLE
                holder.tv_spl_instructions.text =
                    "${getString(R.string.spl_instructions)} ${mList!![position].specialInstructions}"
            }
            if (mList!![position].voidStatus!! && mList!![position].quantity == 0) {
                holder.tv_item_name.text =
                    mList!![position].name.toString() + " " + context!!.getString(R.string.voided)
                holder.tv_item_name.paintFlags =
                    holder.tv_item_name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                holder.tv_item_quantity.paintFlags =
                    holder.tv_item_name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
            when (mList!![position].status) {
                0 -> {
                    holder.iv_fulfilled.visibility = View.GONE
                }
                1 -> {
                    holder.iv_fulfilled.visibility = View.GONE
                }
                2 -> {
                    holder.iv_fulfilled.visibility = View.VISIBLE
                }
                3 -> {
                    holder.iv_fulfilled.visibility = View.GONE
                }
            }
            if (mSelectedList.isNotEmpty()) {
                if (mSelectedList.contains(mList!![position])) {
                    holder.ll_header.background =
                        ContextCompat.getDrawable(
                            context!!,
                            R.color.payment_list_item_in_out_selected
                        )
                    holder.tv_item_name.setTextColor(
                        ContextCompat.getColor(
                            context!!,
                            R.color.white
                        )
                    )
                    holder.tv_item_quantity.setTextColor(
                        ContextCompat.getColor(
                            context!!,
                            R.color.white
                        )
                    )
                    isItemSelected = true
                } else {
                    holder.ll_header.background =
                        ContextCompat.getDrawable(context!!, R.color.white)
                    holder.tv_item_name.setTextColor(
                        ContextCompat.getColor(
                            context!!,
                            R.color.edit_text_hint
                        )
                    )
                    holder.tv_item_quantity.setTextColor(
                        ContextCompat.getColor(
                            context!!,
                            R.color.edit_text_hint
                        )
                    )
                    isItemSelected = false
                }
            } else {
                holder.ll_header.background =
                    ContextCompat.getDrawable(context!!, R.color.white)
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.edit_text_hint
                    )
                )
                holder.tv_item_quantity.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.edit_text_hint
                    )
                )
                isItemSelected = false
            }
            val modifiersAdapter =
                ModifiersListAdapter(
                    context!!,
                    mList!![position].modifiersList!!,
                    isItemSelected
                )
            holder.rv_modifiers.adapter = modifiersAdapter
            modifiersAdapter.notifyDataSetChanged()
            val requestsAdapter = SplRequestsAdapter(
                context!!,
                mList!![position].specialRequestList!!,
                isItemSelected
            )
            holder.rv_spl_requests.adapter = requestsAdapter
            requestsAdapter.notifyDataSetChanged()
        }

        fun selectAll() {
            mSelectedList.clear()
            mSelectedList.addAll(mList!!)
            notifyDataSetChanged()
        }

        fun clearAll() {
            mSelectedList.clear()
            notifyDataSetChanged()
        }

        fun selectedList(selectedList: ArrayList<OrderedItemsResponse>) {
            this.mSelectedList = selectedList
            notifyDataSetChanged()
        }

        fun getSelectedList(): ArrayList<OrderedItemsResponse> {
            return mSelectedList
        }

        fun getItem(position: Int): OrderedItemsResponse {
            return mList!![position]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val ll_header = view.findViewById(R.id.ll_header) as LinearLayout
            val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            val tv_item_quantity = view.findViewById(R.id.tv_item_quantity) as TextView
            val iv_fulfilled = view.findViewById(R.id.iv_fulfilled) as ImageView
            val rv_spl_requests = view.findViewById(R.id.rv_spl_requests) as RecyclerView
            val rv_modifiers = view.findViewById(R.id.rv_modifiers) as RecyclerView
            val tv_spl_instructions = view.findViewById(R.id.tv_spl_instructions) as TextView
        }
    }

    inner class ModifiersListAdapter(
        context: Context,
        modifiers: ArrayList<ModifiersDataResponse>,
        isSelected: Boolean
    ) :
        RecyclerView.Adapter<ModifiersListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<ModifiersDataResponse>? = null
        private var isSelected = false

        init {
            this.context = context
            this.mList = modifiers
            this.isSelected = isSelected
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.modifiers_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (isSelected) {
                holder.tv_sub_item_name.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.white
                    )
                )
            } else {
                holder.tv_sub_item_name.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.modifiers_sub_list_text
                    )
                )
            }
            holder.tv_sub_item_name.text = mList!![position].modifierName.toString()
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
        }
    }

    inner class SplRequestsAdapter(
        context: Context,
        splRequests: ArrayList<SpecialRequestDataResponse>,
        isSelected: Boolean
    ) :
        RecyclerView.Adapter<SplRequestsAdapter.ViewHolder>() {
        private var context: Context? = null
        private var mList: ArrayList<SpecialRequestDataResponse>? = null
        private var isSelected = false

        init {
            this.context = context
            this.mList = splRequests
            this.isSelected = isSelected
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.modifiers_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (isSelected) {
                holder.tv_sub_item_name.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.white
                    )
                )
            } else {
                holder.tv_sub_item_name.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.modifiers_sub_list_text
                    )
                )
            }
            holder.tv_sub_item_name.text = mList!![position].name.toString()
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
        }
    }

    override fun onBackPressed() {
        when (from_screen) {
            "notifications" -> {
                val intent = Intent(this@KitchenDisplayActivity, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            }
            "home" -> {
                super.onBackPressed()
            }
        }
    }
}