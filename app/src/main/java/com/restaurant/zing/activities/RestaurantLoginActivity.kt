package com.restaurant.zing.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.apiInterface.RestaurantOtpResponse
import com.restaurant.zing.helpers.HandSetModelInfo
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class RestaurantLoginActivity : AppCompatActivity() {

    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var et_restaurant_code: EditText
    private lateinit var tv_submit: TextView
    private lateinit var restaurantDetails: HashMap<String, String>
    private var isLoggedIn = false
    private var isRestaurantLoggedIn = false
    private var restaurantId = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_login)

        initialize()
        if (isRestaurantLoggedIn && isLoggedIn) {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        } else if (isRestaurantLoggedIn) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
        tv_submit.setOnClickListener {
            if (et_restaurant_code.text.toString().trim() == "") {
                Toast.makeText(
                    this@RestaurantLoginActivity,
                    "Please enter Restaurant code!",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                val jObj = JSONObject()
                jObj.put(
                    "restaurantCode",
                    et_restaurant_code.text.toString().trim().toUpperCase(Locale.ROOT)
                )
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                restaurantLogin(finalObject)
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        restaurantDetails = sessionManager.getRestaurantDetails
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        isRestaurantLoggedIn = restaurantId != ""
        isLoggedIn = sessionManager.isLoggedIn
        handSetModelInfo = HandSetModelInfo()
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
        et_restaurant_code = findViewById(R.id.et_restaurant_code)
        tv_submit = findViewById(R.id.tv_submit)
        et_restaurant_code.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun restaurantLogin(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.generateRestaurantOtpApi(final_object)
        call.enqueue(object : Callback<RestaurantOtpResponse> {
            override fun onFailure(call: Call<RestaurantOtpResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@RestaurantLoginActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RestaurantOtpResponse>,
                response: Response<RestaurantOtpResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@RestaurantLoginActivity,
                            resp.result.toString() + resp.OTP.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("otp", resp.OTP.toString())
                        val intent =
                            Intent(this@RestaurantLoginActivity, RestaurantOtpActivity::class.java)
                        intent.putExtra("otp", resp.OTP.toString())
                        intent.putExtra("restaurantId", resp.restaurantId.toString())
                        intent.putExtra("restaurantName", resp.restaurantName.toString())
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            this@RestaurantLoginActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        et_restaurant_code.setText("")
    }
}