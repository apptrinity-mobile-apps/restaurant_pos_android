package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.apiInterface.GiftCardBalanceResponse
import com.restaurant.zing.apiInterface.RestaurantOtpResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n")
class TransferGiftCardActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var loading_dialog: Dialog
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var tv_transfer: TextView
    private lateinit var tv_old_verify: TextView
    private lateinit var tv_new_verify: TextView
    private lateinit var et_old_number: EditText
    private lateinit var et_new_number: EditText
    private lateinit var et_amount: EditText
    private lateinit var ll_old_card_details: LinearLayout
    private lateinit var ll_new_card_details: LinearLayout
    private lateinit var tv_old_name: TextView
    private lateinit var tv_new_name: TextView
    private lateinit var tv_old_email: TextView
    private lateinit var tv_new_email: TextView
    private lateinit var tv_old_card_no: TextView
    private lateinit var tv_new_card_no: TextView
    private lateinit var tv_old_balance: TextView
    private lateinit var tv_new_balance: TextView
    private lateinit var ll_otp: LinearLayout
    private lateinit var et_otp_number: EditText
    private lateinit var tv_otp_verify: TextView
    private lateinit var tv_currency_old_balance: TextView
    private lateinit var tv_currency_new_balance: TextView
    private lateinit var tv_currency_balance_to_transfer: TextView

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var restaurantId = ""
    private var new_gift_card_no = ""
    private var old_gift_card_no = ""
    private var otp = ""
    private var currenryType = ""
    private var new_gift_card_balance = 0.00
    private var old_gift_card_balance = 0.00
    private var isOldCardVerified = false
    private var isNewCardVerified = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_gift_card)

        initialize()
        tv_currency_old_balance.text = currenryType
        tv_currency_new_balance.text = currenryType
        tv_currency_balance_to_transfer.text = currenryType
        ll_otp.visibility = View.GONE
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        tv_old_verify.setOnClickListener {
            if (et_old_number.text.toString().trim() == "") {
                old_gift_card_no = ""
                Toast.makeText(
                    this@TransferGiftCardActivity,
                    "Please enter gift card number.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                old_gift_card_no = et_old_number.text.toString().trim()
                et_otp_number.setText("")
                val jObj = JSONObject()
                jObj.put("giftCardNo", old_gift_card_no)
                jObj.put("employeeId", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                sendGiftCardOtp(finalObject)
            }
        }
        tv_new_verify.setOnClickListener {
            if (et_new_number.text.toString().trim() == "") {
                new_gift_card_no = ""
                Toast.makeText(
                    this@TransferGiftCardActivity,
                    "Please enter gift card number.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                new_gift_card_no = et_new_number.text.toString().trim()
                val jObj = JSONObject()
                jObj.put("giftCardNo", new_gift_card_no)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                getGiftCardBalance(finalObject, "new")
            }
        }
        et_old_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isOldCardVerified = false
                ll_old_card_details.visibility = View.GONE
            }
        })
        et_new_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isNewCardVerified = false
                ll_new_card_details.visibility = View.GONE
            }
        })
        tv_otp_verify.setOnClickListener {
            if (et_otp_number.text.toString().trim() == "") {
                Toast.makeText(
                    this@TransferGiftCardActivity,
                    "Invalid OTP!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (otp != et_otp_number.text.toString().trim()) {
                Toast.makeText(
                    this@TransferGiftCardActivity,
                    "Invalid OTP!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObj = JSONObject()
                jObj.put("giftCardNo", old_gift_card_no)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                getGiftCardBalance(finalObject, "old")
            }
        }
        tv_transfer.setOnClickListener {
            when {
                !isOldCardVerified -> {
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        "Old card not verified.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                !isNewCardVerified -> {
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        "New card not verified.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_amount.text.toString().trim() == "" -> {
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        "Enter amount to be transferred.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_amount.text.toString().toDouble() > old_gift_card_balance -> {
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        "Amount is greater than card balance.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val jObj = JSONObject()
                    jObj.put("employeeId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("fromCard", old_gift_card_no)
                    jObj.put("toCard", new_gift_card_no)
                    jObj.put("amount", et_amount.text.toString().toDouble())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    transferBalance(finalObject)
                }
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currenryType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tv_transfer = findViewById(R.id.tv_transfer)
        tv_new_verify = findViewById(R.id.tv_new_verify)
        tv_old_verify = findViewById(R.id.tv_old_verify)
        et_old_number = findViewById(R.id.et_old_number)
        et_new_number = findViewById(R.id.et_new_number)
        et_amount = findViewById(R.id.et_amount)
        ll_old_card_details = findViewById(R.id.ll_old_card_details)
        ll_new_card_details = findViewById(R.id.ll_new_card_details)
        tv_old_name = findViewById(R.id.tv_old_name)
        tv_new_name = findViewById(R.id.tv_new_name)
        tv_old_email = findViewById(R.id.tv_old_email)
        tv_new_email = findViewById(R.id.tv_new_email)
        tv_old_card_no = findViewById(R.id.tv_old_card_no)
        tv_new_card_no = findViewById(R.id.tv_new_card_no)
        tv_old_balance = findViewById(R.id.tv_old_balance)
        tv_new_balance = findViewById(R.id.tv_new_balance)
        ll_otp = findViewById(R.id.ll_otp)
        et_otp_number = findViewById(R.id.et_otp_number)
        tv_otp_verify = findViewById(R.id.tv_otp_verify)
        tv_currency_old_balance = findViewById(R.id.tv_currency_old_balance)
        tv_currency_new_balance = findViewById(R.id.tv_currency_new_balance)
        tv_currency_balance_to_transfer = findViewById(R.id.tv_currency_balance_to_transfer)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent =
                            Intent(this@TransferGiftCardActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@TransferGiftCardActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@TransferGiftCardActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun sendGiftCardOtp(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.sendGiftCardOtpApi(final_object)
        call.enqueue(object : Callback<RestaurantOtpResponse> {
            override fun onFailure(call: Call<RestaurantOtpResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RestaurantOtpResponse>,
                response: Response<RestaurantOtpResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        ll_otp.visibility = View.VISIBLE
                        Toast.makeText(
                            this@TransferGiftCardActivity,
                            resp.result.toString() + resp.otp.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        otp = resp.otp.toString()
                        Log.d("otp", resp.otp.toString())
                    } else {
                        ll_otp.visibility = View.GONE
                        Toast.makeText(
                            this@TransferGiftCardActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getGiftCardBalance(json_obj: JsonObject, card: String) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.giftCardBalanceEnqApi(json_obj)
        call.enqueue(object : Callback<GiftCardBalanceResponse> {
            override fun onFailure(call: Call<GiftCardBalanceResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GiftCardBalanceResponse>,
                response: Response<GiftCardBalanceResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus == 1) {
                        val giftCardDetails = resp.giftCard!!
                        if (card == "new") {
                            isNewCardVerified = true
                            ll_new_card_details.visibility = View.VISIBLE
                            if (giftCardDetails.name.toString() == "null") {
                                tv_new_name.text = ""
                            } else {
                                tv_new_name.text = giftCardDetails.name.toString()
                            }
                            if (giftCardDetails.email.toString() == "null") {
                                tv_new_email.text = ""
                            } else {
                                tv_new_email.text = giftCardDetails.email.toString()
                            }
                            tv_new_card_no.text = new_gift_card_no
                            new_gift_card_balance = giftCardDetails.amount!!
                            tv_new_balance.text = "%.2f".format(new_gift_card_balance)
                        } else if (card == "old") {
                            isOldCardVerified = true
                            ll_otp.visibility = View.GONE
                            ll_old_card_details.visibility = View.VISIBLE
                            if (giftCardDetails.name.toString() == "null") {
                                tv_old_name.text = ""
                            } else {
                                tv_old_name.text = giftCardDetails.name.toString()
                            }
                            if (giftCardDetails.email.toString() == "null") {
                                tv_old_email.text = ""
                            } else {
                                tv_old_email.text = giftCardDetails.email.toString()
                            }
                            tv_old_card_no.text = old_gift_card_no
                            old_gift_card_balance = giftCardDetails.amount!!
                            tv_old_balance.text = "%.2f".format(old_gift_card_balance)
                        }
                    } else {
                        Toast.makeText(
                            this@TransferGiftCardActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                        if (card == "new") {
                            isNewCardVerified = false
                            ll_new_card_details.visibility = View.GONE
                        } else if (card == "old") {
                            isOldCardVerified = false
                            ll_old_card_details.visibility = View.GONE
                        }
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun transferBalance(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.giftCardBalanceTransferApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TransferGiftCardActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@TransferGiftCardActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                        // refresh data
                        val jsonObj = JSONObject()
                        jsonObj.put("giftCardNo", old_gift_card_no)
                        jsonObj.put("restaurantId", restaurantId)
                        val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                        getGiftCardBalance(final_object, "old")
                        val jObj = JSONObject()
                        jObj.put("giftCardNo", new_gift_card_no)
                        jObj.put("restaurantId", restaurantId)
                        val final_Object = JsonParser.parseString(jObj.toString()).asJsonObject
                        getGiftCardBalance(final_Object, "new")
                    } else {
                        Toast.makeText(
                            this@TransferGiftCardActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}