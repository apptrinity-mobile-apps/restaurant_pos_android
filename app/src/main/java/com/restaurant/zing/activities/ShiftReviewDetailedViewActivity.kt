package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.apiInterface.EmployeeShiftReviewDetailedResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

@SuppressLint("SetTextI18n")
class ShiftReviewDetailedViewActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_filter: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var tv_employee_header: TextView
    private lateinit var iv_unpaid_check: ImageView
    private lateinit var tv_closed_check: TextView
    private lateinit var tv_closed_check_details: TextView
    private lateinit var iv_unclosed_check: ImageView
    private lateinit var tv_unclosed_check: TextView
    private lateinit var tv_unclosed_check_details: TextView
    private lateinit var iv_closed_check: ImageView
    private lateinit var tv_paid_check: TextView
    private lateinit var tv_paid_check_details: TextView
    private lateinit var iv_cash_tips: ImageView
    private lateinit var tv_cash_tips: TextView
    private lateinit var tv_declare_cash_tips: TextView
    private lateinit var tv_employee_name: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_cash_on_hand: TextView
    private lateinit var tv_total_gratuity: TextView
    private lateinit var tv_credit_non_cash_tips: TextView
    private lateinit var tv_credit_non_cash_tips_percent: TextView
    private lateinit var tv_employee_owes: TextView
    private lateinit var tv_owes_total: TextView
    private lateinit var tv_tips_gratuity: TextView
    private lateinit var tv_close_shift: TextView
    private lateinit var tv_currency_tips_gratuity: TextView
    private lateinit var tv_currency_owes_total: TextView
    private lateinit var tv_currency_credit_non_cash_tips: TextView
    private lateinit var tv_currency_total_gratuity: TextView
    private lateinit var tv_currency_cash_on_hand: TextView
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var restaurantId = ""
    private var empId = ""
    private var currencyType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shift_review_detailed_view)

        initialize()
        try {
            empId = intent.getStringExtra("empId")!!
            val pattern = "EEEE M/dd/yyyy hh:mm aa"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.format(Date())
            tv_date.text = date.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val jObj = JSONObject()
        if (empId == "") {
            jObj.put("employeeId", mEmployeeId)
        } else {
            jObj.put("employeeId", empId)
        }
        jObj.put("restaurantId", restaurantId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getEmployeeDetails(finalObject)
        tv_currency_cash_on_hand.text = currencyType
        tv_currency_credit_non_cash_tips.text = currencyType
        tv_currency_owes_total.text = currencyType
        tv_currency_tips_gratuity.text = currencyType
        tv_currency_total_gratuity.text = currencyType

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        tv_closed_check_details.setOnClickListener {
            val intent =
                Intent(this@ShiftReviewDetailedViewActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("from_screen", "shift_review")
            if (empId == "") {
                intent.putExtra("employeeId", mEmployeeId)
            } else {
                intent.putExtra("employeeId", empId)
            }
            intent.putExtra("show_tab", "closed")
            startActivity(intent)
        }
        tv_unclosed_check_details.setOnClickListener {
            val intent =
                Intent(this@ShiftReviewDetailedViewActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("from_screen", "shift_review")
            if (empId == "") {
                intent.putExtra("employeeId", mEmployeeId)
            } else {
                intent.putExtra("employeeId", empId)
            }
            intent.putExtra("show_tab", "open")
            startActivity(intent)
        }
        tv_paid_check_details.setOnClickListener {
            val intent =
                Intent(this@ShiftReviewDetailedViewActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("from_screen", "shift_review")
            if (empId == "") {
                intent.putExtra("employeeId", mEmployeeId)
            } else {
                intent.putExtra("employeeId", empId)
            }
            intent.putExtra("show_tab", "paid")
            startActivity(intent)
        }
        tv_declare_cash_tips.setOnClickListener { }
        tv_close_shift.setOnClickListener { }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        ll_filter = findViewById(R.id.ll_filter)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tv_employee_header = findViewById(R.id.tv_employee_header)
        iv_unpaid_check = findViewById(R.id.iv_unpaid_check)
        tv_closed_check = findViewById(R.id.tv_closed_check)
        tv_closed_check_details = findViewById(R.id.tv_closed_check_details)
        iv_unclosed_check = findViewById(R.id.iv_unclosed_check)
        tv_unclosed_check = findViewById(R.id.tv_unclosed_check)
        tv_unclosed_check_details = findViewById(R.id.tv_unclosed_check_details)
        iv_closed_check = findViewById(R.id.iv_closed_check)
        tv_paid_check = findViewById(R.id.tv_paid_check)
        tv_paid_check_details = findViewById(R.id.tv_paid_check_details)
        iv_cash_tips = findViewById(R.id.iv_cash_tips)
        tv_cash_tips = findViewById(R.id.tv_cash_tips)
        tv_declare_cash_tips = findViewById(R.id.tv_declare_cash_tips)
        tv_employee_name = findViewById(R.id.tv_employee_name)
        tv_date = findViewById(R.id.tv_date)
        tv_cash_on_hand = findViewById(R.id.tv_cash_on_hand)
        tv_total_gratuity = findViewById(R.id.tv_total_gratuity)
        tv_credit_non_cash_tips = findViewById(R.id.tv_credit_non_cash_tips)
        tv_credit_non_cash_tips_percent = findViewById(R.id.tv_credit_non_cash_tips_percent)
        tv_employee_owes = findViewById(R.id.tv_employee_owes)
        tv_owes_total = findViewById(R.id.tv_owes_total)
        tv_tips_gratuity = findViewById(R.id.tv_tips_gratuity)
        tv_close_shift = findViewById(R.id.tv_close_shift)
        tv_currency_cash_on_hand = findViewById(R.id.tv_currency_cash_on_hand)
        tv_currency_credit_non_cash_tips = findViewById(R.id.tv_currency_credit_non_cash_tips)
        tv_currency_owes_total = findViewById(R.id.tv_currency_owes_total)
        tv_currency_tips_gratuity = findViewById(R.id.tv_currency_tips_gratuity)
        tv_currency_total_gratuity = findViewById(R.id.tv_currency_total_gratuity)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getEmployeeDetails(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewEmployeeShiftReviewDetailedApi(finalObject)
        call.enqueue(object : Callback<EmployeeShiftReviewDetailedResponse> {
            override fun onFailure(call: Call<EmployeeShiftReviewDetailedResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@ShiftReviewDetailedViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<EmployeeShiftReviewDetailedResponse>,
                response: Response<EmployeeShiftReviewDetailedResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val currentStatus = resp.currentStatus!!
                        tv_employee_header.text =
                            resp.employeeDetails!!.employeeName.toString() + " " + getString(R.string.header_shift_review)
                        tv_closed_check.text =
                            currentStatus.closedChecks.toString() + " " + getString(R.string.closed_checks)
                        tv_unclosed_check.text =
                            currentStatus.unclosedChecks.toString() + " " + getString(R.string.current_unclosed_checks)
                        tv_paid_check.text =
                            currentStatus.paidChecks.toString() + " " + getString(R.string.paid_checks)
                        /*if (currentStatus.cashTipsNotDeclared.toString() != "null") {
                            if (currentStatus.cashTipsNotDeclared!!) {
                                tv_cash_tips.text = getString(R.string.cash_tips_declared)
                                tv_declare_cash_tips.isEnabled = false
                            } else {
                                tv_cash_tips.text = getString(R.string.cash_tips_not_declared)
                                tv_declare_cash_tips.isEnabled = true
                            }
                        } else {
                            tv_cash_tips.text = getString(R.string.cash_tips_not_declared)
                            tv_declare_cash_tips.isEnabled = true
                        }*/
                        tv_employee_name.text = resp.employeeDetails.employeeName.toString()
                        tv_employee_owes.text =
                            resp.employeeDetails.employeeName.toString() + " " + "owes house"
                        if (resp.salesAndTaxesSummary!!.equals("null")) {
                            tv_cash_on_hand.text = getString(R.string.default_value_0)
                            tv_total_gratuity.text = getString(R.string.default_value_0)
                            tv_credit_non_cash_tips_percent.text = "(" + "0%" + ")"
                            tv_owes_total.text = getString(R.string.default_value_0)
                        } else {
                            tv_cash_on_hand.text =
                                "%.2f".format(resp.salesAndTaxesSummary.cashOnHand)
                            tv_total_gratuity.text =
                                "%.2f".format(resp.salesAndTaxesSummary.gratuity)
                            tv_credit_non_cash_tips_percent.text = "(" + "0%" + ")"
                            tv_owes_total.text =
                                "%.2f".format(abs(resp.salesAndTaxesSummary.janeOwesHouse!!))
                        }
                        if (resp.tipSummary!!.equals("null")) {
                            tv_credit_non_cash_tips.text = "0.00"
                            tv_tips_gratuity.text = "0.00"
                        } else {
                            tv_credit_non_cash_tips.text =
                                "%.2f".format(resp.tipSummary.creditNonCashTips)
                            tv_tips_gratuity.text =
                                "%.2f".format(resp.tipSummary.cashGratuity)
                        }
                    } else {
                        Toast.makeText(
                            this@ShiftReviewDetailedViewActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ShiftReviewDetailedViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent =
                            Intent(this@ShiftReviewDetailedViewActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@ShiftReviewDetailedViewActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@ShiftReviewDetailedViewActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        try {
            val jObj = JSONObject()
            if (empId == "") {
                jObj.put("employeeId", mEmployeeId) //employee id
            } else {
                jObj.put("employeeId", empId) //employee id
            }
            jObj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            getEmployeeDetails(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}