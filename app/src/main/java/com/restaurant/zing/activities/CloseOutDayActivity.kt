package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.CardBreakdownListAdapter
import com.restaurant.zing.adapters.DeliveryCloseOutAdapter
import com.restaurant.zing.adapters.DiscountsListAdapter
import com.restaurant.zing.adapters.SalesCategoryListAdapter
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.apiInterface.CloseOutDayDataResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

@SuppressLint("SetTextI18n")
class CloseOutDayActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_full_sales_report: LinearLayout
    private lateinit var ll_print: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var iv_unpaid_check: ImageView
    private lateinit var tv_paid_check: TextView
    private lateinit var tv_paid_check_details: TextView
    private lateinit var iv_unclosed_check: ImageView
    private lateinit var tv_unclosed_check: TextView
    private lateinit var tv_unclosed_check_details: TextView
    private lateinit var iv_clocked_in_employee: ImageView
    private lateinit var tv_clocked_in_employee: TextView
    private lateinit var tv_clocked_in_employee_details: TextView
    private lateinit var iv_payments_capture: ImageView
    private lateinit var tv_payments_capture: TextView
    private lateinit var tv_payments_capture_details: TextView
    private lateinit var iv_unclosed_drawers: ImageView
    private lateinit var tv_unclosed_drawers: TextView
    private lateinit var tv_unclosed_drawers_details: TextView
    private lateinit var iv_actual_deposits: ImageView
    private lateinit var tv_actual_deposits: TextView
    private lateinit var tv_actual_deposits_details: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_sales_taxes_net_sales: TextView
    private lateinit var tv_sales_taxes_tax: TextView
    private lateinit var tv_sales_taxes_total_sales: TextView
    private lateinit var tv_sales_category_total_sales: TextView
    private lateinit var rv_sales_category: RecyclerView
    private lateinit var tv_payment_details_cash: TextView
    private lateinit var tv_payment_details_credit: TextView
    private lateinit var tv_payment_details_total_sales: TextView
    private lateinit var tv_payment_details_total_balance: TextView
    private lateinit var tv_tipouts_total: TextView
    private lateinit var tv_tipouts_non_cash_tips: TextView
    private lateinit var tv_tipouts_non_cash_gratuity: TextView
    private lateinit var tv_tipouts_cash_gratuity: TextView
    private lateinit var tv_tipouts_cash_before_tipouts: TextView
    private lateinit var tv_tipouts_total_cash: TextView
    private lateinit var tv_void_amount: TextView
    private lateinit var tv_void_order_count: TextView
    private lateinit var tv_void_item_count: TextView
    private lateinit var tv_void_percent: TextView
    private lateinit var tv_removal_amount: TextView
    private lateinit var tv_removal_count: TextView
    private lateinit var rv_total_discounts: RecyclerView
    private lateinit var rv_card_breakdown: RecyclerView
    private lateinit var tv_labor_net_sales: TextView
    private lateinit var tv_labor_hours: TextView
    private lateinit var tv_labor_cost: TextView
    private lateinit var tv_labor_sales: TextView
    private lateinit var tv_deposit_expected: TextView
    private lateinit var tv_deposit_actual: TextView
    private lateinit var tv_deposit_total: TextView
    private lateinit var rv_delivery: RecyclerView
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var currencyType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_close_out_day)
        initialize()
        try {
            val pattern = "EEEE MM/dd/yyyy"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.format(Date())
            tv_date.text = date.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val jObj = JSONObject()
        jObj.put("restaurantId", restaurantId)
        jObj.put("employeeId", mEmployeeId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        Log.d("finalObject", finalObject.toString())
        getCloseOutSummary(finalObject)
        iv_back.setOnClickListener {
            onBackPressed()
        }

        tv_paid_check_details.setOnClickListener {
            val intent = Intent(this@CloseOutDayActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("from_screen", "close_out")
            intent.putExtra("employeeId", "")
            intent.putExtra("show_tab", "paid")
            startActivity(intent)
        }
        tv_unclosed_check_details.setOnClickListener {
            val intent = Intent(this@CloseOutDayActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("from_screen", "close_out")
            intent.putExtra("employeeId", "")
            intent.putExtra("show_tab", "open")
            startActivity(intent)
        }
        tv_clocked_in_employee_details.setOnClickListener {
            val intent = Intent(this@CloseOutDayActivity, TimeCardsActivity::class.java)
            startActivity(intent)
        }
        tv_actual_deposits_details.setOnClickListener {
            val intent = Intent(this@CloseOutDayActivity, DepositActivity::class.java)
            startActivity(intent)
        }
        tv_unclosed_drawers_details.setOnClickListener {
            val intent = Intent(this@CloseOutDayActivity, CashDrawerActivity::class.java)
            startActivity(intent)
        }
        tv_payments_capture_details.setOnClickListener {
            /*val intent = Intent(this@CloseOutDayActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("from_screen", "close_out")
            startActivity(intent)*/
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        ll_full_sales_report = findViewById(R.id.ll_full_sales_report)
        ll_print = findViewById(R.id.ll_print)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        iv_unpaid_check = findViewById(R.id.iv_unpaid_check)
        tv_paid_check = findViewById(R.id.tv_paid_check)
        tv_paid_check_details = findViewById(R.id.tv_paid_check_details)
        iv_unclosed_check = findViewById(R.id.iv_unclosed_check)
        tv_unclosed_check = findViewById(R.id.tv_unclosed_check)
        tv_unclosed_check_details = findViewById(R.id.tv_unclosed_check_details)
        iv_clocked_in_employee = findViewById(R.id.iv_clocked_in_employee)
        tv_clocked_in_employee = findViewById(R.id.tv_clocked_in_employee)
        tv_clocked_in_employee_details = findViewById(R.id.tv_clocked_in_employee_details)
        iv_payments_capture = findViewById(R.id.iv_payments_capture)
        tv_payments_capture = findViewById(R.id.tv_payments_capture)
        tv_payments_capture_details = findViewById(R.id.tv_payments_capture_details)
        iv_unclosed_drawers = findViewById(R.id.iv_unclosed_drawers)
        tv_unclosed_drawers = findViewById(R.id.tv_unclosed_drawers)
        tv_unclosed_drawers_details = findViewById(R.id.tv_unclosed_drawers_details)
        iv_actual_deposits = findViewById(R.id.iv_actual_deposits)
        tv_actual_deposits = findViewById(R.id.tv_actual_deposits)
        tv_actual_deposits_details = findViewById(R.id.tv_actual_deposits_details)
        tv_date = findViewById(R.id.tv_date)
        tv_sales_taxes_net_sales = findViewById(R.id.tv_sales_taxes_net_sales)
        tv_sales_taxes_tax = findViewById(R.id.tv_sales_taxes_tax)
        tv_sales_taxes_total_sales = findViewById(R.id.tv_sales_taxes_total_sales)
        tv_sales_category_total_sales = findViewById(R.id.tv_sales_category_total_sales)
        rv_sales_category = findViewById(R.id.rv_sales_category)
        tv_payment_details_cash = findViewById(R.id.tv_payment_details_cash)
        tv_payment_details_credit = findViewById(R.id.tv_payment_details_credit)
        tv_payment_details_total_sales = findViewById(R.id.tv_payment_details_total_sales)
        tv_payment_details_total_balance = findViewById(R.id.tv_payment_details_total_balance)
        tv_tipouts_total = findViewById(R.id.tv_tipouts_total)
        tv_tipouts_non_cash_tips = findViewById(R.id.tv_tipouts_non_cash_tips)
        tv_tipouts_non_cash_gratuity = findViewById(R.id.tv_tipouts_non_cash_gratuity)
        tv_tipouts_cash_gratuity = findViewById(R.id.tv_tipouts_cash_gratuity)
        tv_tipouts_cash_before_tipouts = findViewById(R.id.tv_tipouts_cash_before_tipouts)
        tv_tipouts_total_cash = findViewById(R.id.tv_tipouts_total_cash)
        tv_void_amount = findViewById(R.id.tv_void_amount)
        tv_void_order_count = findViewById(R.id.tv_void_order_count)
        tv_void_item_count = findViewById(R.id.tv_void_item_count)
        tv_void_percent = findViewById(R.id.tv_void_percent)
        tv_removal_amount = findViewById(R.id.tv_removal_amount)
        tv_removal_count = findViewById(R.id.tv_removal_count)
        rv_total_discounts = findViewById(R.id.rv_total_discounts)
        rv_card_breakdown = findViewById(R.id.rv_card_breakdown)
        tv_labor_net_sales = findViewById(R.id.tv_labor_net_sales)
        tv_labor_hours = findViewById(R.id.tv_labor_hours)
        tv_labor_cost = findViewById(R.id.tv_labor_cost)
        tv_labor_sales = findViewById(R.id.tv_labor_sales)
        tv_deposit_expected = findViewById(R.id.tv_deposit_expected)
        tv_deposit_actual = findViewById(R.id.tv_deposit_actual)
        tv_deposit_total = findViewById(R.id.tv_deposit_total)
        rv_delivery = findViewById(R.id.rv_delivery)

        val salesCategoryLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val totalDiscountsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val cardBreakDownLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val deliveryLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_sales_category.layoutManager = salesCategoryLayoutManager
        rv_total_discounts.layoutManager = totalDiscountsLayoutManager
        rv_card_breakdown.layoutManager = cardBreakDownLayoutManager
        rv_delivery.layoutManager = deliveryLayoutManager

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getCloseOutSummary(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.closeOutDaySummaryApi(final_object)
        call.enqueue(object : Callback<CloseOutDayDataResponse> {
            override fun onFailure(call: Call<CloseOutDayDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CloseOutDayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error---", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CloseOutDayDataResponse>,
                response: Response<CloseOutDayDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val cardsList = resp.creditCardBreakdown!!
                        val currentStatus = resp.currentStatus!!
                        val deposits = resp.deposits!!
                        val delivery = resp.delivery!!
                        val labour = resp.labour!!
                        val paymentDetails = resp.paymentDetails!!
                        val salesAndTaxesSummary = resp.salesAndTaxesSummary!!
                        val salesCategories = resp.salesCategories!!
                        val salesCategoriesList = salesCategories.salesCategories!!
                        val serverTips = resp.serverTipouts!!
                        val discountsList = resp.totalDiscounts!!
                        val totalRemovals = resp.totalRemovals!!
                        val totalVoids = resp.totalVoids!!
                        tv_date.text = resp.dateFormat.toString()
                        tv_paid_check.text =
                            currentStatus.paidChecks.toString() + " " + getString(R.string.paid_checks)
                        tv_unclosed_check.text =
                            currentStatus.unclosedChecks.toString() + " " + getString(R.string.current_unclosed_checks)
                        tv_clocked_in_employee.text =
                            currentStatus.clockedInEmployee.toString() + " " + getString(R.string.current_clocked_in_employees)
                        tv_payments_capture.text =
                            currentStatus.paymentsCapture.toString() + " " + getString(R.string.current_payments_needing_capture)
                        tv_unclosed_drawers.text =
                            currentStatus.unclosedCashDrawers.toString() + " " + getString(R.string.current_unclosed_drawer)
                        tv_actual_deposits.text =
                            currentStatus.actualDeposits.toString() + " " + getString(R.string.current_actual_deposits)
                        tv_deposit_expected.text =
                            currencyType + "%.2f".format(deposits.expectedDeposit)
                        tv_deposit_actual.text =
                            currencyType + "%.2f".format(deposits.actualDeposit)
                        tv_deposit_total.text =
                            currencyType + "%.2f".format(deposits.overage_shortage)
                        tv_labor_net_sales.text =
                            currencyType + "%.2f".format(labour.totalNetSales!!)
                        tv_labor_hours.text = labour.totalHours.toString()
                        if (labour.totalLabourCost.toString() == "null") {
                            tv_labor_cost.text = currencyType + getString(R.string.default_value_0)
                        } else {
                            tv_labor_cost.text =
                                currencyType + "%.2f".format(labour.totalLabourCost!!.toDouble())
                        }
                        tv_labor_sales.text = labour.costs_sales.toString() + "%"
                        tv_payment_details_cash.text =
                            currencyType + "%.2f".format(paymentDetails.cash)
                        tv_payment_details_credit.text =
                            currencyType + "%.2f".format(paymentDetails.credits)
                        tv_payment_details_total_sales.text =
                            currencyType + "%.2f".format(paymentDetails.totalSales)
                        val total = if (paymentDetails.totalPayments.toString() != "null") {
                            paymentDetails.totalPayments!! - paymentDetails.totalSales!!
                        } else {
                            0.00 - paymentDetails.totalSales!!
                        }
                        if (total >= 0) {
                            tv_payment_details_total_balance.text =
                                currencyType + "%.2f".format(total)
                        } else {
                            tv_payment_details_total_balance.text =
                                "-" + currencyType + "%.2f".format(abs(total))
                        }
                        tv_sales_taxes_net_sales.text =
                            currencyType + "%.2f".format(salesAndTaxesSummary.totalNetSales)
                        tv_sales_taxes_tax.text =
                            currencyType + "%.2f".format(salesAndTaxesSummary.tax)
                        tv_sales_taxes_total_sales.text =
                            currencyType + "%.2f".format(salesAndTaxesSummary.totalSales)
                        tv_sales_category_total_sales.text =
                            currencyType + "%.2f".format(salesCategories.salesCategoriesTotalNetSales)
                        tv_tipouts_cash_before_tipouts.text =
                            currencyType + "%.2f".format(serverTips.cashBeforeTips)
                        tv_tipouts_cash_gratuity.text =
                            "-" + currencyType + "%.2f".format(serverTips.cashGratuity)
                        tv_tipouts_non_cash_gratuity.text =
                            "-" + currencyType + "%.2f".format(serverTips.creditNonCashGratuity)
                        tv_tipouts_non_cash_tips.text =
                            "-" + currencyType + "%.2f".format(serverTips.creditNonCashTips)
                        val tipouts =
                            if (serverTips.totalTipsAndFeesTippedOut.toString() != "null") {
                                serverTips.totalTipsAndFeesTippedOut!!
                            } else {
                                0.00
                            }
                        if (tipouts >= 0) {
                            tv_tipouts_total.text = "-" + currencyType + "%.2f".format(tipouts)
                        } else {
                            tv_tipouts_total.text = "-" + currencyType + "%.2f".format(abs(tipouts))
                        }
                        if (serverTips.totalCash!! >= 0) {
                            tv_tipouts_total_cash.text =
                                currencyType + "%.2f".format(abs(serverTips.totalCash))
                        } else {
                            tv_tipouts_total_cash.text =
                                "-" + currencyType + "%.2f".format(abs(serverTips.totalCash))
                        }
                        tv_removal_amount.text =
                            currencyType + "%.2f".format(totalRemovals.removalAmount)
                        tv_removal_count.text = totalRemovals.removedItemCount.toString()
                        tv_void_amount.text = currencyType + "%.2f".format(totalVoids.voidAmount)
                        tv_void_order_count.text = totalVoids.voidItemCount.toString()
                        tv_void_item_count.text = totalVoids.voidOrderCount.toString()
                        tv_void_percent.text = totalVoids.voidPercent.toString() + "%"
                        val cardsListAdapter =
                            CardBreakdownListAdapter(this@CloseOutDayActivity, cardsList)
                        val deliveryAdapter =
                            DeliveryCloseOutAdapter(this@CloseOutDayActivity, delivery)
                        val salesCategoryAdapter =
                            SalesCategoryListAdapter(this@CloseOutDayActivity, salesCategoriesList)
                        val discountsListAdapter =
                            DiscountsListAdapter(this@CloseOutDayActivity, discountsList)
                        rv_card_breakdown.adapter = cardsListAdapter
                        rv_delivery.adapter = deliveryAdapter
                        rv_sales_category.adapter = salesCategoryAdapter
                        rv_total_discounts.adapter = discountsListAdapter
                        salesCategoryAdapter.notifyDataSetChanged()
                        discountsListAdapter.notifyDataSetChanged()
                        cardsListAdapter.notifyDataSetChanged()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@CloseOutDayActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@CloseOutDayActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@CloseOutDayActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@CloseOutDayActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        try {
            val jObj = JSONObject()
            jObj.put("restaurantId", restaurantId)
            jObj.put("employeeId", mEmployeeId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            getCloseOutSummary(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
