package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.ChecksBreakdownListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class PaymentTerminalActivity : AppCompatActivity() {

    private lateinit var tv_sort: TextView
    private lateinit var iv_back: ImageView
    private lateinit var iv_search: ImageView
    private lateinit var ll_new_order: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var tabLayout: TabLayout
    private lateinit var closedChecksTab: TextView
    private lateinit var paidChecksTab: TextView
    private lateinit var openChecksTab: TextView
    private lateinit var closedChecksTabHeader: LinearLayout
    private lateinit var paidChecksTabHeader: LinearLayout
    private lateinit var openChecksTabHeader: LinearLayout
    private lateinit var ll_open_checks: LinearLayout
    private lateinit var rv_open_checks: RecyclerView
    private lateinit var tv_open_checks_empty: TextView
    private lateinit var ll_paid_checks: LinearLayout
    private lateinit var rv_paid_checks: RecyclerView
    private lateinit var tv_paid_checks_empty: TextView
    private lateinit var ll_closed_checks: LinearLayout
    private lateinit var rv_closed_checks: RecyclerView
    private lateinit var tv_closed_checks_empty: TextView
    private lateinit var tv_closed_checks_load_more: TextView
    private lateinit var tv_check_table_no: TextView
    private lateinit var tv_check_status: TextView
    private lateinit var tv_check_employee: TextView
    private lateinit var rv_order_items: RecyclerView
    private lateinit var tv_check_items_status: TextView
    private lateinit var tv_credits: TextView
    private lateinit var tv_sub_total: TextView
    private lateinit var tv_discount: TextView
    private lateinit var tv_tax: TextView
    private lateinit var tv_tip_percent: TextView
    private lateinit var tv_tip_amount: TextView
    private lateinit var tv_balance: TextView
    private lateinit var tv_total: TextView
    private lateinit var ll_button_options: LinearLayout
    private lateinit var tv_button_hide: TextView
    private lateinit var tv_button_update: TextView
    private lateinit var tv_button_print: TextView
    private lateinit var tv_button_pay: TextView
    private lateinit var ll_check_items_list: LinearLayout
    private lateinit var ll_credits_used: LinearLayout
    private lateinit var ll_tips: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var tv_currency_total: TextView
    private lateinit var tv_currency_balance_due: TextView
    private lateinit var tv_currency_tax: TextView
    private lateinit var tv_currency_sub_total: TextView
    private lateinit var tv_currency_tip_amount: TextView
    private lateinit var tv_currency_discount: TextView
    private lateinit var tv_currency_credits: TextView
    private lateinit var tv_currency_quantity: TextView
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mPosUserId = ""
    private var restaurantId = ""
    private var employeeId = ""
    private var mEmpJobId = ""

    private var mSelectedIndex = -1
    private var from_screen = ""
    private var selected_tab = ""
    private var orderDetails = OrderDetailsResponse()
    private var orderItemsList = ArrayList<OrderDetailsResponse>()
    private var mOrderId = ""
    private var mTableNumber = 0
    private var checkNumber = 0
    private var show_tab = ""
    private lateinit var openChecksList: ArrayList<ChecksDataResponse>
    private lateinit var closedChecksList: ArrayList<ChecksDataResponse>
    private lateinit var paidChecksList: ArrayList<ChecksDataResponse>
    private var currencyType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_terminal)

        try {
            employeeId = intent.getStringExtra("employeeId")!!
            from_screen = intent.getStringExtra("from_screen")!!
            show_tab = intent.getStringExtra("show_tab")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()
        tv_currency_total.text = currencyType
        tv_currency_balance_due.text = currencyType
        tv_currency_tax.text = currencyType
        tv_currency_sub_total.text = currencyType
        tv_currency_tip_amount.text = currencyType
        tv_currency_discount.text = currencyType
        tv_currency_credits.text = currencyType
        tv_currency_quantity.text = "($currencyType)"
        tv_total.text = getString(R.string.default_value_0)
        tv_tax.text = getString(R.string.default_value_0)
        tv_sub_total.text = getString(R.string.default_value_0)
        tv_tip_amount.text = getString(R.string.default_value_0)
        tv_discount.text = getString(R.string.default_value_0)
        tv_credits.text = getString(R.string.default_value_0)
        tv_balance.text = getString(R.string.default_value_0)
        if (from_screen == "home") {
            tv_check_items_status.visibility = View.GONE
        } else if (from_screen == "close_out" || from_screen == "shift_review") {
            sessionManager.saveTempEmpId(employeeId)
            tv_check_items_status.visibility = View.GONE
        }
        val jObj = JSONObject()
        employeeId = sessionManager.getTempEmpId
        if (employeeId == "") {
            jObj.put("userId", mPosUserId)  // pos user id
            jObj.put("restaurantId", restaurantId)
        } else {
            jObj.put("userId", mPosUserId)  // pos user id
            jObj.put("restaurantId", restaurantId)
            jObj.put("employeeId", employeeId)
        }
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        Log.d("finalObject", finalObject.toString())
        getAllOpenChecks(finalObject)
        getAllClosedChecks(finalObject)
        getAllPaidChecks(finalObject)

        ll_check_items_list.visibility = View.GONE
        tv_check_status.visibility = View.GONE
        ll_credits_used.visibility = View.GONE
        ll_tips.visibility = View.GONE
        tv_check_table_no.isSelected = true

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mPosUserId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        tv_button_update.setOnClickListener {
            if (mSelectedIndex != -1) {
                val intent = Intent(this@PaymentTerminalActivity, OrderDetailsActivity::class.java)
                intent.putExtra("from_screen", "payment_terminal")
                intent.putExtra("orderDetails", orderDetails)
                intent.putExtra("orderId", mOrderId)
                startActivity(intent)
            }
        }
        tv_button_hide.setOnClickListener {

        }
        ll_new_order.setOnClickListener {
            val intent =
                Intent(this@PaymentTerminalActivity, OrderDetailsActivity::class.java)
            intent.putExtra("from_screen", "home")
            startActivity(intent)
        }
        tv_button_print.setOnClickListener {
            if (mSelectedIndex != -1) {
                val intent = Intent(this, ViewItemsActivity::class.java)
                intent.putExtra("payment_status", "empty")
                intent.putExtra("orderItemsList", orderItemsList)
                intent.putExtra("checkNumber", checkNumber)
                startActivity(intent)
            }
        }
        tv_button_pay.setOnClickListener {
            if (mSelectedIndex != -1) {
                val intent = Intent(this@PaymentTerminalActivity, PaymentScreenActivity::class.java)
                intent.putExtra("from_screen", "payment_terminal")
                intent.putExtra("mOrderId", mOrderId)
                startActivity(intent)
            }
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                // setting order items as empty
                tv_total.text = getString(R.string.default_value_0)
                tv_tax.text = getString(R.string.default_value_0)
                tv_sub_total.text = getString(R.string.default_value_0)
                tv_tip_amount.text = getString(R.string.default_value_0)
                tv_discount.text = getString(R.string.default_value_0)
                tv_credits.text = getString(R.string.default_value_0)
                tv_balance.text = getString(R.string.default_value_0)
                tv_check_employee.text = ""
                tv_check_table_no.text = ""
                tv_check_status.text = ""
                val items = ArrayList<OrderItemsResponse>()
                val adapter =
                    ChecksBreakdownListAdapter(this@PaymentTerminalActivity, items, selected_tab)
                rv_order_items.adapter = adapter
                when (tab!!.position) {
                    0 -> {
                        mSelectedIndex = -1
                        selected_tab = "open"
                        ll_open_checks.visibility = View.VISIBLE
                        ll_paid_checks.visibility = View.GONE
                        ll_closed_checks.visibility = View.GONE
                        ll_button_options.visibility = View.VISIBLE
                        tv_button_print.visibility = View.VISIBLE
                        tv_button_pay.visibility = View.VISIBLE
                        tv_button_update.visibility = View.VISIBLE
                        ll_credits_used.visibility = View.GONE
                        ll_tips.visibility = View.GONE
                        openChecksTab.setTextColor(
                            ContextCompat.getColor(this@PaymentTerminalActivity, R.color.white)
                        )
                        paidChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PaymentTerminalActivity, R.color.edit_text_hint
                            )
                        )
                        closedChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PaymentTerminalActivity, R.color.edit_text_hint
                            )
                        )
                        openChecksTabHeader.background = ContextCompat.getDrawable(
                            this@PaymentTerminalActivity, R.drawable.gradient_bg_1
                        )
                        paidChecksTabHeader.background = null
                        closedChecksTabHeader.background = null
                    }
                    1 -> {
                        mSelectedIndex = -1
                        selected_tab = "paid"
                        ll_open_checks.visibility = View.GONE
                        ll_paid_checks.visibility = View.VISIBLE
                        ll_closed_checks.visibility = View.GONE
                        ll_button_options.visibility = View.VISIBLE
                        tv_button_print.visibility = View.VISIBLE
                        tv_button_pay.visibility = View.GONE
                        tv_button_update.visibility = View.GONE
                        ll_credits_used.visibility = View.VISIBLE
                        ll_tips.visibility = View.VISIBLE
                        openChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PaymentTerminalActivity, R.color.edit_text_hint
                            )
                        )
                        paidChecksTab.setTextColor(
                            ContextCompat.getColor(this@PaymentTerminalActivity, R.color.white)
                        )
                        closedChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PaymentTerminalActivity, R.color.edit_text_hint
                            )
                        )
                        openChecksTabHeader.background = null
                        paidChecksTabHeader.background = ContextCompat.getDrawable(
                            this@PaymentTerminalActivity, R.drawable.gradient_bg_1
                        )
                        closedChecksTabHeader.background = null
                    }
                    2 -> {
                        mSelectedIndex = -1
                        selected_tab = "closed"
                        ll_open_checks.visibility = View.GONE
                        ll_paid_checks.visibility = View.GONE
                        ll_closed_checks.visibility = View.VISIBLE
                        ll_button_options.visibility = View.VISIBLE
                        tv_button_print.visibility = View.GONE
                        tv_button_pay.visibility = View.VISIBLE
                        tv_button_update.visibility = View.VISIBLE
                        ll_credits_used.visibility = View.GONE
                        ll_tips.visibility = View.GONE
                        openChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PaymentTerminalActivity, R.color.edit_text_hint
                            )
                        )
                        paidChecksTab.setTextColor(
                            ContextCompat.getColor(
                                this@PaymentTerminalActivity, R.color.edit_text_hint
                            )
                        )
                        closedChecksTab.setTextColor(
                            ContextCompat.getColor(this@PaymentTerminalActivity, R.color.white)
                        )
                        openChecksTabHeader.background = null
                        paidChecksTabHeader.background = null
                        closedChecksTabHeader.background = ContextCompat.getDrawable(
                            this@PaymentTerminalActivity, R.drawable.gradient_bg_1
                        )
                    }
                }
            }
        })

    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mPosUserId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        tv_sort = findViewById(R.id.tv_sort)
        iv_back = findViewById(R.id.iv_back)
        iv_search = findViewById(R.id.iv_search)
        ll_new_order = findViewById(R.id.ll_new_order)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tabLayout = findViewById(R.id.tabLayout)

        ll_open_checks = findViewById(R.id.ll_open_checks)
        rv_open_checks = findViewById(R.id.rv_open_checks)
        tv_open_checks_empty = findViewById(R.id.tv_open_checks_empty)
        ll_paid_checks = findViewById(R.id.ll_paid_checks)
        rv_paid_checks = findViewById(R.id.rv_paid_checks)
        tv_paid_checks_empty = findViewById(R.id.tv_paid_checks_empty)
        ll_closed_checks = findViewById(R.id.ll_closed_checks)
        rv_closed_checks = findViewById(R.id.rv_closed_checks)
        tv_closed_checks_empty = findViewById(R.id.tv_closed_checks_empty)
        tv_closed_checks_load_more = findViewById(R.id.tv_closed_checks_load_more)
        tv_check_table_no = findViewById(R.id.tv_check_table_no)
        tv_check_status = findViewById(R.id.tv_check_status)
        tv_check_employee = findViewById(R.id.tv_check_employee)
        rv_order_items = findViewById(R.id.rv_order_items)
        tv_check_items_status = findViewById(R.id.tv_check_items_status)
        tv_credits = findViewById(R.id.tv_credits)
        tv_sub_total = findViewById(R.id.tv_sub_total)
        tv_discount = findViewById(R.id.tv_discount)
        tv_tax = findViewById(R.id.tv_tax)
        tv_tip_percent = findViewById(R.id.tv_tip_percent)
        tv_tip_amount = findViewById(R.id.tv_tip_amount)
        tv_balance = findViewById(R.id.tv_balance)
        tv_total = findViewById(R.id.tv_total)
        ll_button_options = findViewById(R.id.ll_button_options)
        tv_button_hide = findViewById(R.id.tv_button_hide)
        tv_button_update = findViewById(R.id.tv_button_update)
        tv_button_print = findViewById(R.id.tv_button_print)
        tv_button_pay = findViewById(R.id.tv_button_pay)
        ll_check_items_list = findViewById(R.id.ll_check_items_list)
        ll_credits_used = findViewById(R.id.ll_credits_used)
        ll_tips = findViewById(R.id.ll_tips)
        tv_currency_total = findViewById(R.id.tv_currency_total)
        tv_currency_balance_due = findViewById(R.id.tv_currency_balance_due)
        tv_currency_tax = findViewById(R.id.tv_currency_tax)
        tv_currency_sub_total = findViewById(R.id.tv_currency_sub_total)
        tv_currency_tip_amount = findViewById(R.id.tv_currency_tip_amount)
        tv_currency_discount = findViewById(R.id.tv_currency_discount)
        tv_currency_credits = findViewById(R.id.tv_currency_credits)
        tv_currency_quantity = findViewById(R.id.tv_currency_quantity)

        setUpTabIcons()
        val openLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val paidLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val closedLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val orderItemsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_open_checks.layoutManager = openLinearLayoutManager
        rv_paid_checks.layoutManager = paidLinearLayoutManager
        rv_closed_checks.layoutManager = closedLinearLayoutManager
        rv_order_items.layoutManager = orderItemsLayoutManager
        rv_open_checks.hasFixedSize()
        rv_paid_checks.hasFixedSize()
        rv_closed_checks.hasFixedSize()
        rv_order_items.hasFixedSize()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun setUpTabIcons() {
        val openChecksView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val paidChecksView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        val closedChecksView = LayoutInflater.from(this).inflate(R.layout.tab_header_payment, null)
        openChecksTabHeader = openChecksView.findViewById(R.id.ll_tab_header)
        paidChecksTabHeader = paidChecksView.findViewById(R.id.ll_tab_header)
        closedChecksTabHeader = closedChecksView.findViewById(R.id.ll_tab_header)
        openChecksTab = openChecksView.findViewById(R.id.tv_tab_name)
        paidChecksTab = paidChecksView.findViewById(R.id.tv_tab_name)
        closedChecksTab = closedChecksView.findViewById(R.id.tv_tab_name)
        openChecksTab.text = getString(R.string.open_checks)
        paidChecksTab.text = getString(R.string.paid_checks)
        closedChecksTab.text = getString(R.string.closed_checks)
        val openLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val paidLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val closedLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        openChecksView.layoutParams = openLp
        paidChecksView.layoutParams = paidLp
        closedChecksView.layoutParams = closedLp
        selected_tab = show_tab
        if (show_tab == "open") {
            mSelectedIndex = -1
            selected_tab = "open"
            ll_open_checks.visibility = View.VISIBLE
            ll_paid_checks.visibility = View.GONE
            ll_closed_checks.visibility = View.GONE
            ll_button_options.visibility = View.VISIBLE
            tv_button_pay.visibility = View.VISIBLE
            tv_button_update.visibility = View.VISIBLE
            tv_button_print.visibility = View.VISIBLE
            ll_credits_used.visibility = View.GONE
            ll_tips.visibility = View.GONE
            openChecksTab.setTextColor(
                ContextCompat.getColor(this@PaymentTerminalActivity, R.color.white)
            )
            paidChecksTab.setTextColor(
                ContextCompat.getColor(
                    this@PaymentTerminalActivity, R.color.edit_text_hint
                )
            )
            closedChecksTab.setTextColor(
                ContextCompat.getColor(
                    this@PaymentTerminalActivity, R.color.edit_text_hint
                )
            )
            openChecksTabHeader.background = ContextCompat.getDrawable(
                this@PaymentTerminalActivity, R.drawable.gradient_bg_1
            )
            paidChecksTabHeader.background = null
            closedChecksTabHeader.background = null
            tabLayout.addTab(tabLayout.newTab().setCustomView(openChecksView), true)
            tabLayout.addTab(tabLayout.newTab().setCustomView(paidChecksView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(closedChecksView))
        } else if (show_tab == "paid") {
            mSelectedIndex = -1
            selected_tab = "paid"
            ll_open_checks.visibility = View.GONE
            ll_paid_checks.visibility = View.VISIBLE
            ll_closed_checks.visibility = View.GONE
            ll_button_options.visibility = View.VISIBLE
            tv_button_print.visibility = View.VISIBLE
            tv_button_pay.visibility = View.GONE
            tv_button_update.visibility = View.GONE
            ll_credits_used.visibility = View.VISIBLE
            ll_tips.visibility = View.VISIBLE
            openChecksTab.setTextColor(
                ContextCompat.getColor(
                    this@PaymentTerminalActivity, R.color.edit_text_hint
                )
            )
            paidChecksTab.setTextColor(
                ContextCompat.getColor(this@PaymentTerminalActivity, R.color.white)
            )
            closedChecksTab.setTextColor(
                ContextCompat.getColor(
                    this@PaymentTerminalActivity, R.color.edit_text_hint
                )
            )
            openChecksTabHeader.background = null
            paidChecksTabHeader.background = ContextCompat.getDrawable(
                this@PaymentTerminalActivity, R.drawable.gradient_bg_1
            )
            closedChecksTabHeader.background = null
            tabLayout.addTab(tabLayout.newTab().setCustomView(openChecksView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(paidChecksView), true)
            tabLayout.addTab(tabLayout.newTab().setCustomView(closedChecksView))
        } else if (show_tab == "closed") {
            mSelectedIndex = -1
            selected_tab = "closed"
            ll_open_checks.visibility = View.GONE
            ll_paid_checks.visibility = View.GONE
            ll_closed_checks.visibility = View.VISIBLE
            ll_button_options.visibility = View.VISIBLE
            tv_button_print.visibility = View.GONE
            tv_button_pay.visibility = View.VISIBLE
            tv_button_update.visibility = View.VISIBLE
            ll_credits_used.visibility = View.GONE
            ll_tips.visibility = View.GONE
            openChecksTab.setTextColor(
                ContextCompat.getColor(
                    this@PaymentTerminalActivity, R.color.edit_text_hint
                )
            )
            paidChecksTab.setTextColor(
                ContextCompat.getColor(
                    this@PaymentTerminalActivity, R.color.edit_text_hint
                )
            )
            closedChecksTab.setTextColor(
                ContextCompat.getColor(this@PaymentTerminalActivity, R.color.white)
            )
            openChecksTabHeader.background = null
            paidChecksTabHeader.background = null
            closedChecksTabHeader.background = ContextCompat.getDrawable(
                this@PaymentTerminalActivity, R.drawable.gradient_bg_1
            )
            tabLayout.addTab(tabLayout.newTab().setCustomView(openChecksView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(paidChecksView))
            tabLayout.addTab(tabLayout.newTab().setCustomView(closedChecksView), true)
        }
    }

    private fun getAllOpenChecks(finalObject: JsonObject) {
        openChecksList = ArrayList()
        val api = ApiInterface.create()
        val call = api.openChecksApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentTerminalActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        openChecksList = respBody.openChecks!!
                        if (openChecksList.size > 0) {
                            val adapter =
                                ChecksListAdapter(
                                    this@PaymentTerminalActivity,
                                    openChecksList,
                                    "open"
                                )
                            rv_open_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_open_checks.visibility = View.VISIBLE
                            tv_open_checks_empty.visibility = View.GONE
                        } else {
                            rv_open_checks.visibility = View.GONE
                            tv_open_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_open_checks.visibility = View.GONE
                        tv_open_checks_empty.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllClosedChecks(finalObject: JsonObject) {
        closedChecksList = ArrayList()
        val api = ApiInterface.create()
        val call = api.closedChecksApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentTerminalActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        closedChecksList = respBody.closedChecks!!
                        if (closedChecksList.size > 0) {
                            val adapter =
                                ChecksListAdapter(
                                    this@PaymentTerminalActivity,
                                    closedChecksList,
                                    "closed"
                                )
                            rv_closed_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_closed_checks.visibility = View.VISIBLE
                            tv_closed_checks_empty.visibility = View.GONE
                        } else {
                            rv_closed_checks.visibility = View.GONE
                            tv_closed_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_closed_checks.visibility = View.GONE
                        tv_closed_checks_empty.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllPaidChecks(finalObject: JsonObject) {
        paidChecksList = ArrayList()
        val api = ApiInterface.create()
        val call = api.paidChecksApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentTerminalActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        paidChecksList = respBody.paidChecks!!
                        if (paidChecksList.size > 0) {
                            val adapter =
                                ChecksListAdapter(
                                    this@PaymentTerminalActivity,
                                    paidChecksList,
                                    "paid"
                                )
                            rv_paid_checks.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_paid_checks.visibility = View.VISIBLE
                            tv_paid_checks_empty.visibility = View.GONE
                        } else {
                            rv_paid_checks.visibility = View.GONE
                            tv_paid_checks_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_paid_checks.visibility = View.GONE
                        tv_paid_checks_empty.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getOrderDetails(finalObject: JsonObject) {
        loading_dialog.show()
        orderItemsList = ArrayList()
        val api = ApiInterface.create()
        val call = api.viewOrderApi(finalObject)
        call.enqueue(object : Callback<ViewOrderDataResponse> {
            override fun onFailure(call: Call<ViewOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentTerminalActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ViewOrderDataResponse>,
                response: Response<ViewOrderDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        orderDetails = respBody.orderDetails!!
                        orderItemsList.add(orderDetails)
                        checkNumber = orderDetails.checkNumber!!
                        tv_total.text = "%.2f".format(orderDetails.totalAmount!!)
                        tv_tax.text = "%.2f".format(orderDetails.taxAmount!!)
                        tv_sub_total.text = "%.2f".format(orderDetails.subTotal!!)
                        tv_tip_amount.text = "%.2f".format(orderDetails.tipAmount!!)
                        tv_discount.text = "%.2f".format(orderDetails.discountAmount!!)
                        tv_balance.text = "%.2f".format(orderDetails.dueAmount!!)
                        tv_credits.text = "%.2f".format(orderDetails.creditsUsed!!)
                        tv_check_employee.text = orderDetails.createdByName.toString()
                        tv_check_table_no.text =
                            "#" + orderDetails.checkNumber.toString() + ", Order #" + orderDetails.orderNumber.toString() + ", " + orderDetails.dineInOptionName.toString()
                        when (selected_tab) {
                            "open" -> {
                                tv_check_status.visibility = View.GONE
                            }
                            "closed" -> {
                                if (orderDetails.closedOn.toString() == "null") {
                                    tv_check_status.visibility = View.GONE
                                } else {
                                    tv_check_status.visibility = View.VISIBLE
                                }
                            }
                            "paid" -> {
                                if (orderDetails.closedOn.toString() == "null") {
                                    tv_check_status.visibility = View.GONE
                                } else {
                                    tv_check_status.visibility = View.VISIBLE
                                }
                            }
                        }
                        if (orderDetails.closedOn.toString() == "null") {
                            // do nothing
                        } else {
                            try {
                                val full_date = orderDetails.closedOn.toString()
                                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                                val date = sdf.parse(full_date)
                                val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                                val time = stf.format(date!!)
                                tv_check_status.text =
                                    getString(R.string.closed_at) + " " + time.toUpperCase(Locale.ROOT)
                            } catch (e: Exception) {
                                tv_check_status.text = getString(R.string.closed_at)
                                e.printStackTrace()
                            }
                        }
                        if (selected_tab == "open" || selected_tab == "closed") {
                            if ("%.2f".format(orderDetails.tipAmount!!) == "0.00") {
                                ll_tips.visibility = View.GONE
                            } else {
                                ll_tips.visibility = View.VISIBLE
                            }
                        }
                        val items = orderDetails.orderItems!!
                        val adapter =
                            ChecksBreakdownListAdapter(
                                this@PaymentTerminalActivity, items, selected_tab
                            )
                        rv_order_items.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@PaymentTerminalActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@PaymentTerminalActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@PaymentTerminalActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)) { dialog, _ -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@PaymentTerminalActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class ChecksListAdapter(
        context: Context,
        list: ArrayList<ChecksDataResponse>,
        check_type: String
    ) :
        RecyclerView.Adapter<ChecksListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mCheckType: String? = null
        private var mList: ArrayList<ChecksDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
            this.mCheckType = check_type
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.payment_terminal_check_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): ChecksDataResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_dine_type.isSelected = true
            if (mList!![position].tableNumber.toString() == "" || mList!![position].tableNumber.toString() == "null") {
                holder.tv_order_number.text = "#" + mList!![position].checkNumber.toString()
            } else {
                holder.tv_order_number.text =
                    "#" + mList!![position].checkNumber.toString() + "/" + mList!![position].tableNumber.toString()
            }
            holder.tv_dine_type.text = mList!![position].dineInOptionName.toString()
            holder.tv_check_order_number.text = "Order #" + mList!![position].orderNumber.toString()
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd", Locale.getDefault())
                val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date1 = sdf1.format(date!!)
                val time = stf.format(date)
                holder.tv_order_date.text = "$date1 $time".toUpperCase(Locale.ROOT)
                tv_check_items_status.text = getString(R.string.sent) + " " + time
            } catch (e: Exception) {
                e.printStackTrace()
            }
            holder.tv_order_amount.text = currencyType + "%.2f".format(mList!![position].amount)
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].itemNames!!.size) {
                sbString.append(mList!![position].itemNames!![i].itemName).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_order_items.text = strList
            if (mList!![position].isRecallOrder!!) {
                holder.cv_header.setCardBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.kitchen_display_recalled_new)
                )
            } else {
                holder.cv_header.setCardBackgroundColor(
                    ContextCompat.getColor(mContext!!, R.color.white)
                )
            }
            holder.ll_check.setOnClickListener {
                if (mSelectedIndex == -1 || mSelectedIndex != position) {
                    mSelectedIndex = position
                    ll_check_items_list.visibility = View.VISIBLE
                    if (selected_tab == "closed") {
                        tv_check_status.visibility = View.VISIBLE
                    } else {
                        tv_check_status.visibility = View.GONE
                    }
                    mOrderId = mList!![position].orderId.toString()
                    mTableNumber = if (mList!![position].tableNumber.toString() == "null") {
                        0
                    } else {
                        mList!![position].tableNumber!!
                    }
                    val jObject = JSONObject()
                    jObject.put("userId", mPosUserId)
                    jObject.put("restaurantId", restaurantId)
                    jObject.put("orderId", mOrderId)
                    val finalObject = JsonParser.parseString(jObject.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    getOrderDetails(finalObject = finalObject)
                } else if (mSelectedIndex == position) {
                    mSelectedIndex = -1
                    ll_check_items_list.visibility = View.GONE
                    tv_check_status.visibility = View.GONE
                    tv_total.text = getString(R.string.default_value_0)
                    tv_tax.text = getString(R.string.default_value_0)
                    tv_sub_total.text = getString(R.string.default_value_0)
                    tv_tip_amount.text = getString(R.string.default_value_0)
                    tv_discount.text = getString(R.string.default_value_0)
                    tv_credits.text = getString(R.string.default_value_0)
                    tv_balance.text = getString(R.string.default_value_0)
                    tv_check_employee.text = ""
                    tv_check_table_no.text = ""
                    mOrderId = ""
                    mTableNumber = 0
                    tv_check_status.text = ""
                }
                notifyDataSetChanged()
            }
            if (mSelectedIndex == position) {
                holder.ll_check.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_dine_type.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_dine_type.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_order_date.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_order_amount.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_items.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_check_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
            } else {
                holder.ll_check.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_dine_type.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_dine_type.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_date.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_amount.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_items.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_check_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var ll_check: LinearLayout = view.findViewById(R.id.ll_check)
            var tv_order_number: TextView = view.findViewById(R.id.tv_order_number)
            var tv_dine_type: TextView = view.findViewById(R.id.tv_dine_type)
            var tv_order_date: TextView = view.findViewById(R.id.tv_order_date)
            var tv_order_amount: TextView = view.findViewById(R.id.tv_order_amount)
            var tv_order_items: TextView = view.findViewById(R.id.tv_order_items)
            var tv_check_order_number: TextView = view.findViewById(R.id.tv_check_order_number)
            var cv_header: CardView = view.findViewById(R.id.cv_header)
        }
    }

    override fun onRestart() {
        super.onRestart()
        try {
            // update open,closed,paid checks list
            mSelectedIndex = -1
            // setting order items as empty
            tv_total.text = getString(R.string.default_value_0)
            tv_tax.text = getString(R.string.default_value_0)
            tv_sub_total.text = getString(R.string.default_value_0)
            tv_tip_amount.text = getString(R.string.default_value_0)
            tv_discount.text = getString(R.string.default_value_0)
            tv_credits.text = getString(R.string.default_value_0)
            tv_balance.text = getString(R.string.default_value_0)
            tv_check_employee.text = ""
            tv_check_table_no.text = ""
            tv_check_status.text = ""
            val items = ArrayList<OrderItemsResponse>()
            val adapter =
                ChecksBreakdownListAdapter(this@PaymentTerminalActivity, items, selected_tab)
            rv_order_items.adapter = adapter
            val jObj = JSONObject()
            if (employeeId == "") {
                jObj.put("userId", mPosUserId)  // pos user id
                jObj.put("restaurantId", restaurantId)
            } else {
                jObj.put("userId", mPosUserId)  // pos user id
                jObj.put("restaurantId", restaurantId)
                jObj.put("employeeId", employeeId) // employee id
            }
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            getAllOpenChecks(finalObject)
            getAllClosedChecks(finalObject)
            getAllPaidChecks(finalObject)
            // updating previously selected check
            /*val jObject = JSONObject()
            jObject.put("userId", mPosUserId)
            jObject.put("restaurantId", restaurantId)
            jObject.put("orderId", mOrderId)
            val final_Object = JsonParser.parseString(jObject.toString()).asJsonObject
            getOrderDetails(finalObject = final_Object)*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        sessionManager.saveTempEmpId("")
        super.onBackPressed()
    }

}