package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.DeliveryItemListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@SuppressLint("SetTextI18n")
class DeliveryActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var tabLayout: TabLayout
    private lateinit var tv_credits: TextView
    private lateinit var tv_sub_total: TextView
    private lateinit var tv_discount: TextView
    private lateinit var tv_tax: TextView
    private lateinit var tv_balance: TextView
    private lateinit var tv_total: TextView
    private lateinit var tv_tip_amount: TextView
    private lateinit var tv_delivery_to_address: TextView
    private lateinit var tv_delivery_time: TextView
    private lateinit var rv_order_items: RecyclerView

    private lateinit var ll_unassigned: LinearLayout
    private lateinit var ll_assigned: LinearLayout
    private lateinit var rv_unassigned: RecyclerView
    private lateinit var rv_assigned: RecyclerView
    private lateinit var tv_unassigned_empty: TextView
    private lateinit var tv_assigned_empty: TextView
    private lateinit var ll_enroute: LinearLayout
    private lateinit var rv_enroute: RecyclerView
    private lateinit var rv_driverslist: RecyclerView
    private lateinit var tv_no_drivers: TextView
    private lateinit var tv_enroute_empty: TextView
    private lateinit var ll_delivered: LinearLayout
    private lateinit var rv_delivered: RecyclerView
    private lateinit var tv_delivered_empty: TextView
    private lateinit var tv_items_status: TextView
    private lateinit var tv_ready_time: TextView
    private lateinit var ll_ready_time: LinearLayout
    private lateinit var ll_new_order: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var ll_cancel_dispatch: LinearLayout
    private lateinit var ll_complete_delivery: LinearLayout
    private lateinit var ll_dispatch_driver: LinearLayout
    private lateinit var ll_update_driver: LinearLayout
    private lateinit var ll_pickup_order: LinearLayout
    private lateinit var ll_approve_options: LinearLayout
    private lateinit var ll_button_options: LinearLayout
    private lateinit var unassignedTab: TextView
    private lateinit var assignedTab: TextView
    private lateinit var enrouteTab: TextView
    private lateinit var deliveredTab: TextView
    private lateinit var unassignedTabHeader: LinearLayout
    private lateinit var assignedTabHeader: LinearLayout
    private lateinit var enrouteTabHeader: LinearLayout
    private lateinit var deliveredTabHeader: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var dialog_dispatchdriver: Dialog
    private lateinit var tv_button_pay: TextView
    private lateinit var cancel_dispatch_comment_dialog: Dialog
    private lateinit var tv_currency_total: TextView
    private lateinit var tv_currency_balance_due: TextView
    private lateinit var tv_currency_tax: TextView
    private lateinit var tv_currency_sub_total: TextView
    private lateinit var tv_currency_tip_amount: TextView
    private lateinit var tv_currency_discount: TextView
    private lateinit var tv_currency_credits: TextView
    private lateinit var tv_currency_quantity: TextView

    private lateinit var viewOrderItemsArrayList: ArrayList<OrderItemsResponse>
    private lateinit var unassignedArrayList: ArrayList<DeliveryOnlineDataResponse>
    private lateinit var assignedArrayList: ArrayList<DeliveryOnlineDataResponse>
    private lateinit var enRouteArrayList: ArrayList<DeliveryOnlineDataResponse>
    private lateinit var deliveredArrayList: ArrayList<DeliveryOnlineDataResponse>
    private lateinit var driversListArrayList: ArrayList<DriversListResponse>

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var selected_tab = ""
    private var mOrderId = ""
    private var mSelectedItem = -1
    private var mDriverSelectedItem = -1
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var driver_id = ""
    private var isUnAssignedSelected = false
    private var isAssignedSelected = false
    private var isEnRouteSelected = false
    private var isDeliveredSelected = false
    private var currencyType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery)
        initialize()
        tv_currency_total.text = currencyType
        tv_currency_balance_due.text = currencyType
        tv_currency_tax.text = currencyType
        tv_currency_sub_total.text = currencyType
        tv_currency_tip_amount.text = currencyType
        tv_currency_discount.text = currencyType
        tv_currency_credits.text = currencyType
        tv_currency_quantity.text = "($currencyType)"
        ll_approve_options.visibility = View.GONE
        ll_button_options.visibility = View.GONE
        iv_back.setOnClickListener {
            onBackPressed()
        }
        getAllOrders()
        resetData()
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                resetData()
                when (tab.position) {
                    0 -> {
                        selected_tab = "unassigned"
                        ll_unassigned.visibility = View.VISIBLE
                        ll_assigned.visibility = View.GONE
                        ll_enroute.visibility = View.GONE
                        ll_delivered.visibility = View.GONE
                        ll_dispatch_driver.visibility = View.VISIBLE
                        ll_complete_delivery.visibility = View.GONE
                        ll_cancel_dispatch.visibility = View.GONE
                        ll_update_driver.visibility = View.GONE
                        ll_pickup_order.visibility = View.GONE
                        unassignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.white
                            )
                        )
                        assignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        enrouteTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        deliveredTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        unassignedTabHeader.background = ContextCompat.getDrawable(
                            this@DeliveryActivity,
                            R.drawable.gradient_bg_1
                        )
                        assignedTabHeader.background = null
                        enrouteTabHeader.background = null
                        deliveredTabHeader.background = null
                    }
                    1 -> {
                        selected_tab = "assigned"
                        ll_unassigned.visibility = View.GONE
                        ll_assigned.visibility = View.VISIBLE
                        ll_enroute.visibility = View.GONE
                        ll_delivered.visibility = View.GONE
                        ll_dispatch_driver.visibility = View.GONE
                        ll_complete_delivery.visibility = View.GONE
                        ll_cancel_dispatch.visibility = View.GONE
                        ll_update_driver.visibility = View.VISIBLE
                        ll_pickup_order.visibility = View.VISIBLE
                        unassignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        assignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.white
                            )
                        )
                        enrouteTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        deliveredTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        unassignedTabHeader.background = null
                        assignedTabHeader.background = ContextCompat.getDrawable(
                            this@DeliveryActivity,
                            R.drawable.gradient_bg_1
                        )
                        enrouteTabHeader.background = null
                        deliveredTabHeader.background = null
                    }
                    2 -> {
                        selected_tab = "enroute"
                        ll_unassigned.visibility = View.GONE
                        ll_assigned.visibility = View.GONE
                        ll_enroute.visibility = View.VISIBLE
                        ll_delivered.visibility = View.GONE
                        ll_dispatch_driver.visibility = View.GONE
                        ll_complete_delivery.visibility = View.VISIBLE
                        ll_cancel_dispatch.visibility = View.VISIBLE
                        ll_update_driver.visibility = View.VISIBLE
                        ll_pickup_order.visibility = View.GONE
                        unassignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        assignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        enrouteTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.white
                            )
                        )
                        deliveredTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        unassignedTabHeader.background = null
                        assignedTabHeader.background = null
                        enrouteTabHeader.background = ContextCompat.getDrawable(
                            this@DeliveryActivity,
                            R.drawable.gradient_bg_1
                        )
                        deliveredTabHeader.background = null
                    }
                    3 -> {
                        selected_tab = "delivered"
                        ll_unassigned.visibility = View.GONE
                        ll_assigned.visibility = View.GONE
                        ll_enroute.visibility = View.GONE
                        ll_delivered.visibility = View.VISIBLE
                        ll_dispatch_driver.visibility = View.GONE
                        ll_complete_delivery.visibility = View.GONE
                        ll_cancel_dispatch.visibility = View.GONE
                        ll_update_driver.visibility = View.GONE
                        ll_pickup_order.visibility = View.GONE
                        unassignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        assignedTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        enrouteTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.edit_text_hint
                            )
                        )
                        deliveredTab.setTextColor(
                            ContextCompat.getColor(
                                this@DeliveryActivity,
                                R.color.white
                            )
                        )
                        unassignedTabHeader.background = null
                        enrouteTabHeader.background = null
                        assignedTabHeader.background = null
                        deliveredTabHeader.background = ContextCompat.getDrawable(
                            this@DeliveryActivity,
                            R.drawable.gradient_bg_1
                        )
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

        })

        ll_dispatch_driver.setOnClickListener {
            dialog_dispatchdriver.show()
        }
        ll_cancel_dispatch.setOnClickListener {
            if (isEnRouteSelected) {
                cancelReason()
            }
        }
        ll_pickup_order.setOnClickListener {
            if (isAssignedSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("restaurantId", restaurantId)
                jsonObj.put("userId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("driverId", driver_id)
                val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_object", final_object.toString())
                pickupOrder(final_object)
            }
        }
        ll_complete_delivery.setOnClickListener {
            if (isEnRouteSelected) {
                val jsonObj = JSONObject()
                jsonObj.put("restaurantId", restaurantId)
                jsonObj.put("userId", mEmployeeId)
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("driverId", driver_id)
                val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("final_object", final_object.toString())
                completeDelivery(final_object)
            }
        }
        ll_update_driver.setOnClickListener {
            dialog_dispatchdriver.show()
        }
        ll_new_order.setOnClickListener {
            val intent =
                Intent(this@DeliveryActivity, OrderDetailsActivity::class.java)
            intent.putExtra("from_screen", "home")
            startActivity(intent)
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        tv_button_pay.setOnClickListener {
            val intent = Intent(this@DeliveryActivity, PaymentScreenActivity::class.java)
            intent.putExtra("from_screen", "payment_terminal")
            intent.putExtra("mOrderId", mOrderId)
            startActivity(intent)
        }
    }

    private fun initialize() {
        loadingDialog()
        dispatchDriverListDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        ll_new_order = findViewById(R.id.ll_new_order)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        ll_cancel_dispatch = findViewById(R.id.ll_cancel_dispatch)
        ll_complete_delivery = findViewById(R.id.ll_complete_delivery)
        ll_dispatch_driver = findViewById(R.id.ll_dispatch_driver)
        ll_update_driver = findViewById(R.id.ll_update_driver)
        ll_pickup_order = findViewById(R.id.ll_pickup_order)
        tv_credits = findViewById(R.id.tv_credits)
        tv_sub_total = findViewById(R.id.tv_sub_total)
        tv_discount = findViewById(R.id.tv_discount)
        tv_tax = findViewById(R.id.tv_tax)
        tv_tip_amount = findViewById(R.id.tv_tip_amount)
        tv_delivery_to_address = findViewById(R.id.tv_delivery_to_address)
        tv_delivery_time = findViewById(R.id.tv_delivery_time)
        tv_balance = findViewById(R.id.tv_balance)
        tv_total = findViewById(R.id.tv_total)
        rv_order_items = findViewById(R.id.rv_order_items)
        tabLayout = findViewById(R.id.tabLayout)
        ll_unassigned = findViewById(R.id.ll_unassigned)
        ll_assigned = findViewById(R.id.ll_assigned)
        rv_unassigned = findViewById(R.id.rv_unassigned)
        rv_assigned = findViewById(R.id.rv_assigned)
        tv_unassigned_empty = findViewById(R.id.tv_unassigned_empty)
        tv_assigned_empty = findViewById(R.id.tv_assigned_empty)
        ll_enroute = findViewById(R.id.ll_enroute)
        rv_enroute = findViewById(R.id.rv_enroute)
        tv_enroute_empty = findViewById(R.id.tv_enroute_empty)
        ll_delivered = findViewById(R.id.ll_delivered)
        rv_delivered = findViewById(R.id.rv_delivered)
        tv_delivered_empty = findViewById(R.id.tv_delivered_empty)
        ll_approve_options = findViewById(R.id.ll_approve_options)
        ll_button_options = findViewById(R.id.ll_button_options)
        tv_items_status = findViewById(R.id.tv_items_status)
        tv_ready_time = findViewById(R.id.tv_ready_time)
        ll_ready_time = findViewById(R.id.ll_ready_time)
        tv_button_pay = findViewById(R.id.tv_button_pay)
        tv_currency_total = findViewById(R.id.tv_currency_total)
        tv_currency_balance_due = findViewById(R.id.tv_currency_balance_due)
        tv_currency_tax = findViewById(R.id.tv_currency_tax)
        tv_currency_sub_total = findViewById(R.id.tv_currency_sub_total)
        tv_currency_tip_amount = findViewById(R.id.tv_currency_tip_amount)
        tv_currency_discount = findViewById(R.id.tv_currency_discount)
        tv_currency_credits = findViewById(R.id.tv_currency_credits)
        tv_currency_quantity = findViewById(R.id.tv_currency_quantity)

        setUpTabIcons()
        val unassignedLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val assignedLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val enrouteLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val deliveredLinearLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val orderItemsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_unassigned.layoutManager = unassignedLinearLayoutManager
        rv_assigned.layoutManager = assignedLinearLayoutManager
        rv_enroute.layoutManager = enrouteLinearLayoutManager
        rv_delivered.layoutManager = deliveredLinearLayoutManager
        rv_order_items.layoutManager = orderItemsLayoutManager
        rv_unassigned.hasFixedSize()
        rv_assigned.hasFixedSize()
        rv_enroute.hasFixedSize()
        rv_delivered.hasFixedSize()
        rv_order_items.hasFixedSize()

        unassignedArrayList = ArrayList()
        assignedArrayList = ArrayList()
        enRouteArrayList = ArrayList()
        deliveredArrayList = ArrayList()
        viewOrderItemsArrayList = ArrayList()
        driversListArrayList = ArrayList()

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun dispatchDriverListDialog() {
        dialog_dispatchdriver = Dialog(this)
        dialog_dispatchdriver.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_dispatchdriver.setContentView(R.layout.dialog_dispatch_driver)
        dialog_dispatchdriver.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_dispatchdriver.setCanceledOnTouchOutside(false)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog_dispatchdriver.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        dialog_dispatchdriver.window!!.attributes = layoutParams
        rv_driverslist = dialog_dispatchdriver.findViewById(R.id.rv_driverslist) as RecyclerView
        tv_no_drivers = dialog_dispatchdriver.findViewById(R.id.tv_no_drivers) as TextView
        val tv_ok = dialog_dispatchdriver.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = dialog_dispatchdriver.findViewById(R.id.tv_cancel) as TextView
        val driverslistLinearLayoutManager =
            LinearLayoutManager(this@DeliveryActivity, RecyclerView.VERTICAL, false)
        rv_driverslist.layoutManager = driverslistLinearLayoutManager
        rv_driverslist.hasFixedSize()
        tv_cancel.setOnClickListener {
            dialog_dispatchdriver.dismiss()
        }
        tv_ok.setOnClickListener {
            if (driver_id == "") {
                Toast.makeText(
                    this@DeliveryActivity,
                    "Please select driver for dispatch.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObjDriver = JSONObject()
                jObjDriver.put("driverId", driver_id)
                jObjDriver.put("restaurantId", restaurantId)
                jObjDriver.put("orderId", mOrderId)
                jObjDriver.put("userId", mEmployeeId)
                val finalObjectDriver = JsonParser.parseString(jObjDriver.toString()).asJsonObject
                Log.d("final_object", finalObjectDriver.toString())
                dispatchDeliveryToDriver(finalObjectDriver)
            }
        }
    }

    private fun cancelReason() {
        cancel_dispatch_comment_dialog = Dialog(this)
        cancel_dispatch_comment_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@DeliveryActivity)
                .inflate(R.layout.cancel_dispatch_comment_dialog, null)
        cancel_dispatch_comment_dialog.setContentView(view)
        cancel_dispatch_comment_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        cancel_dispatch_comment_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        cancel_dispatch_comment_dialog.setCanceledOnTouchOutside(true)
        val displayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = this.display
            display!!.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(cancel_dispatch_comment_dialog.window!!.attributes)
        val dialogWindowHeight = (displayHeight * 0.85f).toInt()
        layoutParams.height = dialogWindowHeight
        cancel_dispatch_comment_dialog.window!!.attributes = layoutParams
        val tv_done = view.findViewById(R.id.tv_done) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val et_reason = view.findViewById(R.id.et_reason) as EditText
        tv_cancel.setOnClickListener {
            cancel_dispatch_comment_dialog.dismiss()
        }
        tv_done.setOnClickListener {
            if (et_reason.text.toString().trim() == "") {
                Toast.makeText(
                    this@DeliveryActivity,
                    "Please enter reason for cancellation",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jsonObj = JSONObject()
                jsonObj.put("orderId", mOrderId)
                jsonObj.put("cancelledBy", mEmployeeId)
                jsonObj.put("cancelReason", et_reason.text.toString().trim())
                val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                cancelDispatch(final_object)
            }
        }
        cancel_dispatch_comment_dialog.show()
    }

    private fun setUpTabIcons() {
        val unassignedView = LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        val assignedView = LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        val enrouteView = LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        val deliveredView = LayoutInflater.from(this).inflate(R.layout.tab_header_delivered, null)
        unassignedTabHeader = unassignedView.findViewById(R.id.ll_tab_header)
        assignedTabHeader = assignedView.findViewById(R.id.ll_tab_header)
        enrouteTabHeader = enrouteView.findViewById(R.id.ll_tab_header)
        deliveredTabHeader = deliveredView.findViewById(R.id.ll_tab_header)
        unassignedTab = unassignedView.findViewById(R.id.tv_tab_name)
        assignedTab = assignedView.findViewById(R.id.tv_tab_name)
        enrouteTab = enrouteView.findViewById(R.id.tv_tab_name)
        deliveredTab = deliveredView.findViewById(R.id.tv_tab_name)
        unassignedTab.text = getString(R.string.unassigned)
        assignedTab.text = getString(R.string.assigned)
        enrouteTab.text = getString(R.string.enroute)
        deliveredTab.text = getString(R.string.delivered)
        unassignedTab.setTextColor(
            ContextCompat.getColor(this, R.color.white)
        )
        assignedTab.setTextColor(
            ContextCompat.getColor(this, R.color.edit_text_hint)
        )
        enrouteTab.setTextColor(
            ContextCompat.getColor(this, R.color.edit_text_hint)
        )
        deliveredTab.setTextColor(
            ContextCompat.getColor(this, R.color.edit_text_hint)
        )
        unassignedTabHeader.background = ContextCompat.getDrawable(this, R.drawable.gradient_bg_1)
        assignedTabHeader.background = null
        enrouteTabHeader.background = null
        deliveredTabHeader.background = null
        ll_unassigned.visibility = View.VISIBLE
        ll_assigned.visibility = View.GONE
        ll_enroute.visibility = View.GONE
        ll_delivered.visibility = View.GONE
        ll_dispatch_driver.visibility = View.VISIBLE
        ll_complete_delivery.visibility = View.GONE
        ll_cancel_dispatch.visibility = View.GONE
        ll_update_driver.visibility = View.GONE
        ll_pickup_order.visibility = View.GONE
        ll_button_options.visibility = View.GONE
        val unAssignedLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val assignedLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val enrouteLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val deliveredLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        unassignedView.layoutParams = unAssignedLp
        assignedView.layoutParams = assignedLp
        enrouteView.layoutParams = enrouteLp
        deliveredView.layoutParams = deliveredLp
        tabLayout.addTab(tabLayout.newTab().setCustomView(unassignedView), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(assignedView))
        tabLayout.addTab(tabLayout.newTab().setCustomView(enrouteView))
        tabLayout.addTab(tabLayout.newTab().setCustomView(deliveredView))
        selected_tab = "unassigned"
    }

    private fun getAllOrders() {
        val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId) // remove after testing
        jObj.put("restaurantId", restaurantId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        Log.d("final_object", finalObject.toString())
        getUnassignedOrder(finalObject)
        getAssignedOrders(finalObject)
        getEnrouteOrder(finalObject)
        getDeliveredOrder(finalObject)
        getAllDriversList(finalObject)
    }

    private fun resetData() {
        val adapter = DeliveryItemListAdapter(this@DeliveryActivity, ArrayList())
        rv_order_items.adapter = adapter
        adapter.notifyDataSetChanged()
        tv_credits.text = getString(R.string.default_value_0)
        tv_sub_total.text = getString(R.string.default_value_0)
        tv_discount.text = getString(R.string.default_value_0)
        tv_tax.text = getString(R.string.default_value_0)
        tv_tip_amount.text = getString(R.string.default_value_0)
        tv_delivery_to_address.text = ""
        tv_delivery_time.text = ""
        tv_balance.text = getString(R.string.default_value_0)
        tv_total.text = getString(R.string.default_value_0)
        ll_ready_time.visibility = View.GONE
        tv_items_status.visibility = View.GONE
        tv_ready_time.text = ""
        mSelectedItem = -1
        mDriverSelectedItem = -1
        ll_button_options.visibility = View.GONE
        ll_dispatch_driver.alpha = 0.5f
        ll_complete_delivery.alpha = 0.5f
        ll_cancel_dispatch.alpha = 0.5f
        ll_update_driver.alpha = 0.5f
        ll_pickup_order.alpha = 0.5f
        ll_dispatch_driver.isEnabled = false
        ll_complete_delivery.isEnabled = false
        ll_cancel_dispatch.isEnabled = false
        ll_update_driver.isEnabled = false
        ll_pickup_order.isEnabled = false
    }

    private fun getUnassignedOrder(finalObject: JsonObject) {
        loading_dialog.show()
        unassignedArrayList.clear()
        val api = ApiInterface.create()
        val call = api.getDeliveryOnlineOrder(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val deliveryList = respBody.ordersData!!
                        if (deliveryList.size > 0) {
                            for (i in 0 until deliveryList.size) {
                                unassignedArrayList.add(deliveryList[i])
                            }
                            val adapter =
                                UnAssignedListAdapter(
                                    this@DeliveryActivity,
                                    unassignedArrayList
                                )
                            rv_unassigned.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_unassigned.visibility = View.VISIBLE
                            tv_unassigned_empty.visibility = View.GONE
                        } else {
                            rv_unassigned.visibility = View.GONE
                            tv_unassigned_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_unassigned.visibility = View.GONE
                        tv_unassigned_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAssignedOrders(finalObject: JsonObject) {
        loading_dialog.show()
        assignedArrayList.clear()
        val api = ApiInterface.create()
        val call = api.getDeliveryAssignedOrdersApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val deliveryList = respBody.ordersData!!
                        if (deliveryList.size > 0) {
                            for (i in 0 until deliveryList.size) {
                                assignedArrayList.add(deliveryList[i])
                            }
                            val adapter =
                                AssignedListAdapter(
                                    this@DeliveryActivity,
                                    assignedArrayList
                                )
                            rv_assigned.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_assigned.visibility = View.VISIBLE
                            tv_assigned_empty.visibility = View.GONE
                        } else {
                            rv_assigned.visibility = View.GONE
                            tv_assigned_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_assigned.visibility = View.GONE
                        tv_assigned_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getEnrouteOrder(finalObject: JsonObject) {
        loading_dialog.show()
        enRouteArrayList.clear()
        val api = ApiInterface.create()
        val call = api.getEnrouteDelivery(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val deliveryList = respBody.ordersData!!
                        if (deliveryList.size > 0) {
                            for (i in 0 until deliveryList.size) {
                                enRouteArrayList.add(deliveryList[i])
                            }
                            val adapter =
                                EnRouteListAdapter(
                                    this@DeliveryActivity,
                                    enRouteArrayList
                                )
                            rv_enroute.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_enroute.visibility = View.VISIBLE
                            tv_enroute_empty.visibility = View.GONE
                        } else {
                            rv_enroute.visibility = View.GONE
                            tv_enroute_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_enroute.visibility = View.GONE
                        tv_enroute_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getDeliveredOrder(finalObject: JsonObject) {
        loading_dialog.show()
        deliveredArrayList.clear()
        val api = ApiInterface.create()
        val call = api.getDeliveredOrders(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val deliveryList = respBody.ordersData!!
                        if (deliveryList.size > 0) {
                            for (i in 0 until deliveryList.size) {
                                deliveredArrayList.add(deliveryList[i])
                            }
                            val adapter =
                                DeliveredListAdapter(
                                    this@DeliveryActivity,
                                    deliveredArrayList
                                )
                            rv_delivered.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_delivered.visibility = View.VISIBLE
                            tv_delivered_empty.visibility = View.GONE
                        } else {
                            rv_delivered.visibility = View.GONE
                            tv_delivered_empty.visibility = View.VISIBLE
                        }
                    } else {
                        rv_delivered.visibility = View.GONE
                        tv_delivered_empty.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getViewOnlineOrder(finalObject: JsonObject) {
        loading_dialog.show()
        viewOrderItemsArrayList.clear()
        val api = ApiInterface.create()
        val call = api.viewOrderApi(finalObject)
        call.enqueue(object : Callback<ViewOrderDataResponse> {
            override fun onFailure(call: Call<ViewOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ViewOrderDataResponse>,
                response: Response<ViewOrderDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val orderDetails = respBody.orderDetails!!
                        val orderItems = orderDetails.orderItems
                        if (orderItems!!.size > 0) {
                            for (i in 0 until orderItems.size) {
                                viewOrderItemsArrayList.add(orderItems[i])
                            }
                        }
                        val adapter = DeliveryItemListAdapter(
                            this@DeliveryActivity, viewOrderItemsArrayList
                        )
                        rv_order_items.adapter = adapter
                        adapter.notifyDataSetChanged()
                        tv_credits.text = "%.2f".format(orderDetails.creditsUsed)
                        tv_sub_total.text = "%.2f".format(orderDetails.subTotal)
                        tv_discount.text = "%.2f".format(orderDetails.discountAmount)
                        tv_tax.text = "%.2f".format(orderDetails.taxAmount)
                        tv_tip_amount.text = "%.2f".format(orderDetails.tipAmount)
                        tv_balance.text = "%.2f".format(orderDetails.dueAmount)
                        tv_total.text = "%.2f".format(orderDetails.totalAmount)
                        tv_delivery_to_address.text =
                            "#${orderDetails.orderNumber}, Delivery for ${orderDetails.customerDetails!!.firstName} ${orderDetails.customerDetails.lastName} to ${orderDetails.deliveryDetails!!.fullAddress}, ${orderDetails.customerDetails.phoneNumber}"
                        try {
                            val full_date = orderDetails.toBeDeliveredBy.toString()
                            if (full_date != "") {
                                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                                val date = sdf.parse(full_date)
                                val sdf1 = SimpleDateFormat("EEEE, MMM dd", Locale.getDefault())
                                val date_str = sdf1.format(date!!)
                                val sdf2 = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                                val time_str = sdf2.format(date).toUpperCase(Locale.ROOT)
                                if (orderDetails.status == 3) {
                                    tv_delivery_time.text = "Delivered by $date_str at $time_str"
                                } else {
                                    tv_delivery_time.text = "Deliver by $date_str at $time_str"
                                }
                            } else {
                                if (orderDetails.status == 3) {
                                    tv_delivery_time.text = ""
                                } else {
                                    tv_delivery_time.text = ""
                                }
                            }
                        } catch (e: Exception) {
                            if (orderDetails.status == 3) {
                                tv_delivery_time.text = "Delivered"
                            } else {
                                tv_delivery_time.text = "Deliver by"
                            }
                            e.printStackTrace()
                        }
                        try {
                            val full_date = orderDetails.sentToKitchenOn.toString()
                            if (full_date != "") {
                                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                                val date = sdf.parse(full_date)
                                val sdf1 = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                                val time_str = sdf1.format(date!!).toUpperCase(Locale.ROOT)
                                tv_items_status.text = "Sent $time_str"
                            } else {
                                tv_items_status.text = ""
                            }
                        } catch (e: Exception) {
                            tv_items_status.text = "Sent "
                            e.printStackTrace()
                        }
                        tv_ready_time.text = "${orderDetails.preparationTime.toString()} Hour(s)"
                        ll_ready_time.visibility = View.VISIBLE
                        tv_items_status.visibility = View.VISIBLE
                        /*if (orderDetails.status == 3) {
                            ll_button_options.visibility = View.GONE
                            ll_ready_time.visibility = View.GONE
                            tv_items_status.visibility = View.GONE
                        } else {
                            ll_button_options.visibility = View.VISIBLE
                            ll_ready_time.visibility = View.VISIBLE
                            tv_items_status.visibility = View.VISIBLE
                        }*/
                    } else {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@DeliveryActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@DeliveryActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@DeliveryActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@DeliveryActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllDriversList(finalObject: JsonObject) {
        loading_dialog.show()
        driversListArrayList.clear()
        val api = ApiInterface.create()
        val call = api.getAllDriversList(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val driversList = respBody.diversList
                        if (driversList!!.size > 0) {
                            for (i in 0 until driversList.size) {
                                driversListArrayList.add(driversList[i])
                            }
                            val adapter =
                                DriverListAdapter(
                                    this@DeliveryActivity,
                                    driversListArrayList
                                )
                            rv_driverslist.adapter = adapter
                            adapter.notifyDataSetChanged()
                            rv_driverslist.visibility = View.VISIBLE
                            tv_no_drivers.visibility = View.GONE
                        } else {
                            rv_driverslist.visibility = View.GONE
                            tv_no_drivers.visibility = View.VISIBLE
                        }
                    } else {
                        rv_driverslist.visibility = View.GONE
                        tv_no_drivers.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun dispatchDeliveryToDriver(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.dispatchDeliveryToDriver(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        dialog_dispatchdriver.dismiss()
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun completeDelivery(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.completeDeliveryApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun pickupOrder(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.pickupApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun cancelDispatch(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.cancelDispatchApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeliveryActivity,
                        "Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        cancel_dispatch_comment_dialog.dismiss()
                        resetData()
                        getAllOrders()
                    } else {
                        Toast.makeText(
                            this@DeliveryActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class UnAssignedListAdapter(
        context: Context,
        list: ArrayList<DeliveryOnlineDataResponse>
    ) :
        RecyclerView.Adapter<UnAssignedListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<DeliveryOnlineDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.GONE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            try {
                val orderDate = mList!![position].orderDate.toString()
                val orderTime = mList!![position].orderTime.toString()
                val date_pattern = "MM-dd-yyyy"
                val time_pattern = "HH:mm:ss"
                val sdf_date = SimpleDateFormat(date_pattern, Locale.getDefault())
                val sdf_time = SimpleDateFormat(time_pattern, Locale.getDefault())
                val date = sdf_date.parse(orderDate)
                val time = sdf_time.parse(orderTime)
                val date_sdf = SimpleDateFormat("MM/dd", Locale.getDefault())
                val time_sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date_str = date_sdf.format(date!!)
                val time_str = time_sdf.format(time!!)
                holder.tv_order_time.text = "$date_str, $time_str".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items_list!!.size) {
                sbString.append(mList!![position].items_list!![i].itemName).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList
            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    mOrderId = mList!![position].posOrderId.toString()
                    isUnAssignedSelected = true
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].posOrderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    getViewOnlineOrder(finalObject)
                    ll_dispatch_driver.alpha = 1f
                    ll_complete_delivery.alpha = 0.5f
                    ll_cancel_dispatch.alpha = 0.5f
                    ll_update_driver.alpha = 0.5f
                    ll_pickup_order.alpha = 0.5f
                    ll_dispatch_driver.isEnabled = true
                    ll_complete_delivery.isEnabled = false
                    ll_cancel_dispatch.isEnabled = false
                    ll_update_driver.isEnabled = false
                    ll_update_driver.isEnabled = false
                    if (mList!![position].paymentStatus == 1) {
                        ll_button_options.visibility = View.GONE
                        ll_ready_time.visibility = View.GONE
                        tv_items_status.visibility = View.GONE
                    } else {
                        ll_button_options.visibility = View.VISIBLE
                        ll_ready_time.visibility = View.VISIBLE
                        tv_items_status.visibility = View.VISIBLE
                    }
                } else if (mSelectedItem == position) {
                    isUnAssignedSelected = false
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
        }
    }

    inner class AssignedListAdapter(
        context: Context,
        list: ArrayList<DeliveryOnlineDataResponse>
    ) :
        RecyclerView.Adapter<AssignedListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<DeliveryOnlineDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.GONE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            try {
                val orderDate = mList!![position].orderDate.toString()
                val orderTime = mList!![position].orderTime.toString()
                val date_pattern = "MM-dd-yyyy"
                val time_pattern = "HH:mm:ss"
                val sdf_date = SimpleDateFormat(date_pattern, Locale.getDefault())
                val sdf_time = SimpleDateFormat(time_pattern, Locale.getDefault())
                val date = sdf_date.parse(orderDate)
                val time = sdf_time.parse(orderTime)
                val date_sdf = SimpleDateFormat("MM/dd", Locale.getDefault())
                val time_sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date_str = date_sdf.format(date!!)
                val time_str = time_sdf.format(time!!)
                holder.tv_order_time.text = "$date_str, $time_str".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items_list!!.size) {
                sbString.append(mList!![position].items_list!![i].itemName).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList
            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    mOrderId = mList!![position].posOrderId.toString()
                    driver_id = mList!![position].driverId.toString()
                    isAssignedSelected = true
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].posOrderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    getViewOnlineOrder(finalObject)
                    ll_dispatch_driver.alpha = 0.5f
                    ll_complete_delivery.alpha = 0.5f
                    ll_cancel_dispatch.alpha = 0.5f
                    ll_update_driver.alpha = 1f
                    ll_pickup_order.alpha = 1f
                    ll_dispatch_driver.isEnabled = false
                    ll_complete_delivery.isEnabled = false
                    ll_cancel_dispatch.isEnabled = false
                    ll_update_driver.isEnabled = true
                    ll_pickup_order.isEnabled = true
                    if (mList!![position].paymentStatus == 1) {
                        ll_button_options.visibility = View.GONE
                        ll_ready_time.visibility = View.GONE
                        tv_items_status.visibility = View.GONE
                    } else {
                        ll_button_options.visibility = View.VISIBLE
                        ll_ready_time.visibility = View.VISIBLE
                        tv_items_status.visibility = View.VISIBLE
                    }
                } else if (mSelectedItem == position) {
                    isAssignedSelected = false
                    driver_id = ""
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
        }
    }

    inner class EnRouteListAdapter(
        context: Context,
        list: ArrayList<DeliveryOnlineDataResponse>
    ) :
        RecyclerView.Adapter<EnRouteListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<DeliveryOnlineDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.GONE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            try {
                val orderDate = mList!![position].orderDate.toString()
                val orderTime = mList!![position].orderTime.toString()
                val date_pattern = "MM-dd-yyyy"
                val time_pattern = "HH:mm:ss"
                val sdf_date = SimpleDateFormat(date_pattern, Locale.getDefault())
                val sdf_time = SimpleDateFormat(time_pattern, Locale.getDefault())
                val date = sdf_date.parse(orderDate)
                val time = sdf_time.parse(orderTime)
                val date_sdf = SimpleDateFormat("MM/dd", Locale.getDefault())
                val time_sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date_str = date_sdf.format(date!!)
                val time_str = time_sdf.format(time!!)
                holder.tv_order_time.text = "$date_str, $time_str".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items_list!!.size) {
                sbString.append(mList!![position].items_list!![i].itemName).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList
            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    mOrderId = mList!![position].posOrderId.toString()
                    driver_id = mList!![position].driverId.toString()
                    isEnRouteSelected = true
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].posOrderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    getViewOnlineOrder(finalObject)
                    ll_dispatch_driver.alpha = 0.5f
                    ll_complete_delivery.alpha = 1f
                    ll_cancel_dispatch.alpha = 1f
                    ll_update_driver.alpha = 1f
                    ll_pickup_order.alpha = 0.5f
                    ll_dispatch_driver.isEnabled = false
                    ll_complete_delivery.isEnabled = true
                    ll_cancel_dispatch.isEnabled = true
                    ll_update_driver.isEnabled = true
                    ll_pickup_order.isEnabled = false
                    if (mList!![position].paymentStatus == 1) {
                        ll_button_options.visibility = View.GONE
                        ll_ready_time.visibility = View.GONE
                        tv_items_status.visibility = View.GONE
                    } else {
                        ll_button_options.visibility = View.VISIBLE
                        ll_ready_time.visibility = View.VISIBLE
                        tv_items_status.visibility = View.VISIBLE
                    }
                } else if (mSelectedItem == position) {
                    isEnRouteSelected = false
                    driver_id = ""
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
        }
    }

    inner class DeliveredListAdapter(
        context: Context,
        list: ArrayList<DeliveryOnlineDataResponse>
    ) :
        RecyclerView.Adapter<DeliveredListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<DeliveryOnlineDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pending_orders_unapproved_item_new, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.ll_schedule_time.visibility = View.GONE
            holder.tv_currency_sub_total.text = currencyType
            holder.tv_order_number.text = "#" + mList!![position].orderNumber.toString()
            if (mList!![position].totalAmount.toString() == "null") {
                holder.tv_sub_total.text = "0.00"
            } else {
                holder.tv_sub_total.text = "%.2f".format(mList!![position].totalAmount!!)
            }
            holder.tv_pending_dinein.text = mList!![position].dineInOptionName.toString()
            try {
                val orderDate = mList!![position].orderDate.toString()
                val orderTime = mList!![position].orderTime.toString()
                val date_pattern = "MM-dd-yyyy"
                val time_pattern = "HH:mm:ss"
                val sdf_date = SimpleDateFormat(date_pattern, Locale.getDefault())
                val sdf_time = SimpleDateFormat(time_pattern, Locale.getDefault())
                val date = sdf_date.parse(orderDate)
                val time = sdf_time.parse(orderTime)
                val date_sdf = SimpleDateFormat("MM/dd", Locale.getDefault())
                val time_sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date_str = date_sdf.format(date!!)
                val time_str = time_sdf.format(time!!)
                holder.tv_order_time.text = "$date_str, $time_str".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                holder.tv_order_time.text = ""
                e.printStackTrace()
            }
            val sbString = StringBuilder("")
            for (i in 0 until mList!![position].items_list!!.size) {
                sbString.append(mList!![position].items_list!![i].itemName).append(",")
            }
            var strList = sbString.toString()
            if (strList.isNotEmpty()) {
                strList = strList.substring(0, strList.length - 1)
            }
            holder.tv_item_name.text = strList
            holder.ll_item_header.setOnClickListener {
                if (mSelectedItem == -1 || mSelectedItem != position) {
                    mSelectedItem = position
                    mOrderId = mList!![position].posOrderId.toString()
                    isDeliveredSelected = true
                    val jObj = JSONObject()
                    jObj.put("userId", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("orderId", mList!![position].posOrderId.toString())
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    getViewOnlineOrder(finalObject)
                    ll_dispatch_driver.alpha = 0.5f
                    ll_complete_delivery.alpha = 0.5f
                    ll_cancel_dispatch.alpha = 0.5f
                    ll_update_driver.alpha = 0.5f
                    ll_pickup_order.alpha = 0.5f
                    ll_dispatch_driver.isEnabled = false
                    ll_complete_delivery.isEnabled = false
                    ll_cancel_dispatch.isEnabled = false
                    ll_update_driver.isEnabled = false
                    ll_pickup_order.isEnabled = false
                    if (mList!![position].paymentStatus == 1) {
                        ll_button_options.visibility = View.GONE
                        ll_ready_time.visibility = View.GONE
                        tv_items_status.visibility = View.GONE
                    } else {
                        ll_button_options.visibility = View.VISIBLE
                        ll_ready_time.visibility = View.VISIBLE
                        tv_items_status.visibility = View.VISIBLE
                    }
                } else if (mSelectedItem == position) {
                    isDeliveredSelected = false
                    resetData()
                }
                notifyDataSetChanged()
            }
            if (mSelectedItem == position) {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_selected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_selected_in_out
                    )
                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )
                holder.tv_order_time.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_sub_total.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_item_name.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_dispatch_carditem.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.payment_terminal_unselected)
                holder.tv_pending_dinein.background =
                    ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.payment_terminal_unselected_in_out
                    )

                holder.tv_pending_dinein.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_currency_sub_total.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.current_status_close
                    )
                )
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_order_number.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
                holder.tv_scheduled_time.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.current_status_close
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_order_number = view.findViewById(R.id.tv_order_number) as TextView
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_sub_total = view.findViewById(R.id.tv_sub_total) as TextView
            var tv_currency_sub_total = view.findViewById(R.id.tv_currency_sub_total) as TextView
            var tv_order_time = view.findViewById(R.id.tv_order_time) as TextView
            var tv_pending_dinein = view.findViewById(R.id.tv_pending_dinein) as TextView
            var tv_scheduled_time = view.findViewById(R.id.tv_scheduled_time) as TextView
            var ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            var cv_dispatch_carditem = view.findViewById(R.id.cv_pendingorders_carditem) as CardView
            var ll_schedule_time = view.findViewById(R.id.ll_schedule_time) as LinearLayout
        }
    }

    inner class DriverListAdapter(
        context: Context,
        list: ArrayList<DriversListResponse>
    ) :
        RecyclerView.Adapter<DriverListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<DriversListResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.driverlist_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_drivername.text =
                mList!![position].firstName.toString() + " " + mList!![position].lastName.toString()
            holder.tv_driveremail.text = mList!![position].empEmail.toString()
            if (mList!![position].phoneNumber.toString() == "null") {
                holder.tv_driverphonenumber.text = ""
            } else {
                holder.tv_driverphonenumber.text = mList!![position].phoneNumber.toString()
            }
            holder.cv_driver_item_header.setOnClickListener {
                if (mDriverSelectedItem == -1 || mDriverSelectedItem != position) {
                    mDriverSelectedItem = position
                    driver_id = mList!![position].id.toString()
                } else if (mDriverSelectedItem == position) {
                    mDriverSelectedItem = -1
                    driver_id = ""
                }
                notifyDataSetChanged()
            }
            if (mDriverSelectedItem == position) {
                holder.cv_driver_item_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.cyan
                    )
                )
                holder.tv_drivername.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_driveremail.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_driverphonenumber.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
            } else {
                holder.cv_driver_item_header.setCardBackgroundColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.white
                    )
                )
                holder.tv_drivername.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.edit_text_hint
                    )
                )
                holder.tv_driveremail.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.button_black
                    )
                )
                holder.tv_driverphonenumber.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.button_black
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_drivername = view.findViewById(R.id.tv_drivername) as TextView
            var tv_driveremail = view.findViewById(R.id.tv_driveremail) as TextView
            var tv_driverphonenumber = view.findViewById(R.id.tv_driverphonenumber) as TextView
            var cv_driver_item_header = view.findViewById(R.id.cv_driver_item_header) as CardView
        }
    }

    override fun onRestart() {
        super.onRestart()
        try {
            resetData()
            getAllOrders()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}