package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.helpers.RecyclerItemClickListener
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.apiInterface.CashDepositsDataResponse
import com.restaurant.zing.helpers.BillsListDataModel
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class DepositActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var dialog_adddeposit: Dialog
    private lateinit var dialog_withdraw: Dialog
    private lateinit var tv_add_deposit: TextView
    private lateinit var cv_header_deposit: CardView
    private lateinit var cv_header_countbills: CardView
    private lateinit var rv_deposits: RecyclerView
    private lateinit var tv_depositbalance: TextView
    private lateinit var loading_dialog: Dialog
    private lateinit var rv_bills: RecyclerView
    private lateinit var et_bills_value: EditText
    private lateinit var et_total_amount: EditText
    private lateinit var sessionManager: SessionManager
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var getCashDrawer: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var currencyType = ""

    private lateinit var cv_one: CardView
    private lateinit var cv_two: CardView
    private lateinit var cv_three: CardView
    private lateinit var cv_four: CardView
    private lateinit var cv_five: CardView
    private lateinit var cv_six: CardView
    private lateinit var cv_seven: CardView
    private lateinit var cv_eight: CardView
    private lateinit var cv_nine: CardView
    private lateinit var cv_zero: CardView
    private lateinit var cv_double_zero: CardView
    private lateinit var ll_six_dollar: LinearLayout
    private lateinit var ll_eight_dollar: LinearLayout
    private lateinit var ll_done: LinearLayout
    private lateinit var cv_delete: CardView
    private lateinit var cv_clear: CardView
    private lateinit var deposit_dialog: Dialog
    private lateinit var billsList: ArrayList<BillsListDataModel>
    private lateinit var mSelected: ArrayList<BillsListDataModel>
    private lateinit var billsAdapter: BillsAdapter
    private lateinit var tv_withdraw: TextView
    private lateinit var tv_currency_depositbalance: TextView
    private lateinit var tv_currency_amount_header: TextView

    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private var str_double_zero = "00"
    private var amount = 0.00
    private var drop_amount = ""
    private val TYPE_CREDIT = "credit"
    private val TYPE_DEBIT = "debit"
    private var cashDrawerId = ""
    private var totalDeposit = 0.00

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit)
        initialize()
        tv_currency_depositbalance.text = currencyType
        tv_currency_amount_header.text = "($currencyType)"
        loadBills()
        // load default values
        et_bills_value.setText(currencyType + "%.2f".format(getString(R.string.default_value_0).toDouble()))
        et_total_amount.setText(currencyType + "%.2f".format(getString(R.string.default_value_0).toDouble()))
        getDeposits()

        iv_back.setOnClickListener {
            onBackPressed()
        }
        tv_add_deposit.setOnClickListener {
            addDepositDialog()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        ll_done.setOnClickListener {
            cv_header_deposit.visibility = View.VISIBLE
            cv_header_countbills.visibility = View.GONE
            dialog_adddeposit.show()
        }
        tv_withdraw.setOnClickListener {
            withdrawalCashDepositDialog()
        }
        rv_bills.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        var total = 0.00
                        val data = billsAdapter.getItem(position)
                        if (mSelected.size > 0) {
                            if (mSelected.contains(data)) {
                                mSelected.remove(data)
                            } else {
                                mSelected.add(data)
                            }
                        } else {
                            mSelected.add(data)
                        }
                        billsAdapter.selectedList(mSelected)
                        if (mSelected.size > 0) {
                            for (i in 0 until mSelected.size) {
                                total += mSelected[i].value
                            }
                            et_bills_value.setText(currencyType + "%.2f".format(total))
                        } else {
                            et_bills_value.setText(currencyType + getString(R.string.default_value_0))
                        }
                    }
                })
        )
        cv_clear.setOnClickListener {
            drop_amount = getString(R.string.default_value_0)
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_delete.setOnClickListener {
            if (drop_amount != "") {
                drop_amount = drop_amount.dropLast(1)
                if (drop_amount == "") {
                    drop_amount = getString(R.string.default_value_0)
                }
            } else {
                drop_amount = getString(R.string.default_value_0)
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_one.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_one
            } else {
                drop_amount += str_one
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_two.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_two
            } else {
                drop_amount += str_two
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_three.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_three
            } else {
                drop_amount += str_three
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_four.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_four
            } else {
                drop_amount += str_four
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_five.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_five
            } else {
                drop_amount += str_five
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_six.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_six
            } else {
                drop_amount += str_six
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_seven.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_seven
            } else {
                drop_amount += str_seven
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_eight.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_eight
            } else {
                drop_amount += str_eight
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_nine.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_nine
            } else {
                drop_amount += str_nine
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_zero.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_zero
            } else {
                drop_amount += str_zero
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
        cv_double_zero.setOnClickListener {
            if (drop_amount == getString(R.string.default_value_0)) {
                drop_amount = str_double_zero
            } else {
                drop_amount += str_double_zero
            }
            amount = drop_amount.toDouble()
            amount /= 100
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        getCashDrawer = sessionManager.getCashDrawer
        cashDrawerId = getCashDrawer[SessionManager.CASH_DRAWER_ID_KEY]!!
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        tv_add_deposit = findViewById(R.id.tv_add_deposit)
        cv_header_deposit = findViewById(R.id.cv_header_deposit)
        cv_header_countbills = findViewById(R.id.cv_header_countbills)
        rv_deposits = findViewById(R.id.rv_deposits)
        tv_depositbalance = findViewById(R.id.tv_depositbalance)
        rv_bills = findViewById(R.id.rv_bills)
        tv_withdraw = findViewById(R.id.tv_withdraw)
        tv_currency_depositbalance = findViewById(R.id.tv_currency_depositbalance)
        tv_currency_amount_header = findViewById(R.id.tv_currency_amount_header)
        val layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        rv_bills.layoutManager = layoutManager
        rv_bills.hasFixedSize()
        val lManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_deposits.layoutManager = lManager
        rv_deposits.hasFixedSize()
        val dividerItemDecoration = DividerItemDecoration(rv_deposits.context, lManager.orientation)
        rv_deposits.addItemDecoration(dividerItemDecoration)

//        calculator UI
        cv_one = findViewById(R.id.cv_one)
        cv_two = findViewById(R.id.cv_two)
        cv_three = findViewById(R.id.cv_three)
        cv_four = findViewById(R.id.cv_four)
        cv_five = findViewById(R.id.cv_five)
        cv_six = findViewById(R.id.cv_six)
        cv_seven = findViewById(R.id.cv_seven)
        cv_eight = findViewById(R.id.cv_eight)
        cv_nine = findViewById(R.id.cv_nine)
        cv_zero = findViewById(R.id.cv_zero)
        cv_double_zero = findViewById(R.id.cv_double_zero)
        cv_delete = findViewById(R.id.cv_delete)
        cv_clear = findViewById(R.id.cv_clear)
        ll_six_dollar = findViewById(R.id.ll_six_dollar)
        ll_eight_dollar = findViewById(R.id.ll_eight_dollar)
        ll_done = findViewById(R.id.ll_done)
        et_bills_value = findViewById(R.id.et_bills_value)
        et_total_amount = findViewById(R.id.et_total_amount)
        ll_switch_user = findViewById(R.id.ll_switch_user)

        billsList = ArrayList()
        mSelected = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun loadBills() {
        billsList = ArrayList()
        billsList.add(BillsListDataModel("1", 0.01, "cents", R.drawable.ic_cent, "0"))
        billsList.add(BillsListDataModel("1", 1.00, "bills", R.drawable.ic_dollar_bill, "0"))
        billsList.add(BillsListDataModel("5", 0.05, "cents", R.drawable.ic_cent, "0"))
        billsList.add(BillsListDataModel("5", 5.00, "bills", R.drawable.ic_dollar_bill, "0"))
        billsList.add(BillsListDataModel("10", 0.10, "cents", R.drawable.ic_cent, "0"))
        billsList.add(BillsListDataModel("10", 10.00, "bills", R.drawable.ic_dollar_bill, "0"))
        billsList.add(BillsListDataModel("25", 0.25, "cents", R.drawable.ic_cent, "0"))
        billsList.add(BillsListDataModel("25", 25.00, "bills", R.drawable.ic_dollar_bill, "0"))
        billsList.add(BillsListDataModel("50", 0.50, "cents", R.drawable.ic_cent, "0"))
        billsList.add(BillsListDataModel("50", 50.00, "bills", R.drawable.ic_dollar_bill, "0"))
        billsList.add(BillsListDataModel("1", 1.00, "coins", R.drawable.ic_dollar_coin, "0"))
        billsList.add(BillsListDataModel("100", 100.00, "bills", R.drawable.ic_dollar_bill, "0"))
        billsAdapter = BillsAdapter(this, billsList)
        rv_bills.adapter = billsAdapter
        billsAdapter.notifyDataSetChanged()
        et_bills_value.setText(currencyType + getString(R.string.default_value_0))
        mSelected.clear()
    }

    private fun getDeposits() {
        val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId)
        jObj.put("restaurantId", restaurantId)
        jObj.put("cashDrawersId", cashDrawerId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getAllDeposits(finalObject)
    }

    private fun addDepositDialog() {
        dialog_adddeposit = Dialog(this)
        dialog_adddeposit.window
        dialog_adddeposit.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_adddeposit.setContentView(R.layout.dialog_add_deposit)
        dialog_adddeposit.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_adddeposit.setCanceledOnTouchOutside(false)
        val tv_currency_amount_header =
            dialog_adddeposit.findViewById(R.id.tv_currency_amount_header) as TextView
        val et_amount = dialog_adddeposit.findViewById(R.id.et_amount) as EditText
        val et_comment = dialog_adddeposit.findViewById(R.id.et_comment) as EditText
        val ll_adddeposit = dialog_adddeposit.findViewById(R.id.ll_adddeposit) as LinearLayout
        val ll_countbills = dialog_adddeposit.findViewById(R.id.ll_countbills) as LinearLayout
        val ll_cancel_deposit =
            dialog_adddeposit.findViewById(R.id.ll_cancel_deposit) as LinearLayout
        tv_currency_amount_header.text = currencyType
        ll_cancel_deposit.setOnClickListener {
            dialog_adddeposit.dismiss()
        }
        ll_adddeposit.setOnClickListener {
            val et_amount_stg = et_amount.text.toString().trim()
            val et_comment_stg = et_comment.text.toString().trim()
            when {
                et_amount_stg == "" -> {
                    Toast.makeText(
                        this, "Please Enter Deposit Amount",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_comment_stg == "" -> {
                    Toast.makeText(
                        this, "Please Enter Comment",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val jObj = JSONObject()
                    val countbillsarray = JSONArray()
                    jObj.put("actionType", "add_deposit")
                    jObj.put("action", "Add Deposit")
                    jObj.put("amount", et_amount_stg)
                    jObj.put("comment", et_comment_stg)
                    jObj.put("transactionType", TYPE_CREDIT)
                    jObj.put("countBillsList", countbillsarray)
                    jObj.put("cashDrawersId", cashDrawerId)
                    jObj.put("createdBy", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("status", 1)
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    addCashDepositApi(finalObject, dialog_adddeposit)
                }
            }
        }
        ll_countbills.setOnClickListener {
            cv_header_deposit.visibility = View.GONE
            cv_header_countbills.visibility = View.VISIBLE
            loadBills()
            dialog_adddeposit.dismiss()
        }
        dialog_adddeposit.show()
    }

    private fun depositsDialog(response: CashDepositsDataResponse) {
        deposit_dialog = Dialog(this)
        deposit_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@DepositActivity)
                .inflate(R.layout.deposits_dialog, null)
        deposit_dialog.setContentView(view)
        deposit_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        deposit_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        deposit_dialog.setCanceledOnTouchOutside(true)
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_undo = view.findViewById(R.id.tv_undo) as TextView
        val tv_date = view.findViewById(R.id.tv_date) as TextView
        val tv_action_type = view.findViewById(R.id.tv_action_type) as TextView
        val tv_amount = view.findViewById(R.id.tv_amount) as TextView
        val tv_user = view.findViewById(R.id.tv_user) as TextView
        val et_reason = view.findViewById(R.id.et_reason) as EditText
        val tv_char_count = view.findViewById(R.id.tv_char_count) as TextView
        val tv_currency_amount_header =
            view.findViewById(R.id.tv_currency_amount_header) as TextView
        val ll_comment = view.findViewById(R.id.ll_comment) as LinearLayout
        tv_currency_amount_header.text = currencyType
        if (response.transactionType.toString() == TYPE_DEBIT) {
            tv_undo.visibility = View.GONE
            ll_comment.visibility = View.GONE
        } else if (response.transactionType.toString() == TYPE_CREDIT) {
            tv_undo.visibility = View.VISIBLE
            ll_comment.visibility = View.VISIBLE
        }
        tv_cancel.setOnClickListener {
            deposit_dialog.dismiss()
        }
        tv_undo.setOnClickListener {
            if (et_reason.text.toString().trim() == "") {
                Toast.makeText(
                    this@DepositActivity,
                    "Please enter reason",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObj = JSONObject()
                val countbillsarray = JSONArray()
                jObj.put("actionType", "add_deposit")
                jObj.put("action", "Undo Entry")
                jObj.put("amount", response.amount)
                jObj.put("comment", et_reason.text.toString().trim())
                jObj.put("transactionType", TYPE_DEBIT)
                jObj.put("countBillsList", countbillsarray)
                jObj.put("cashDrawersId", cashDrawerId)
                jObj.put("createdBy", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("status", 1)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.d("finalObject", finalObject.toString())
                undoCashDepositApi(finalObject)
            }
        }
        try {
            val full_date = response.createdOn.toString()
            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val date = sdf.parse(full_date)
            val sdf1 = SimpleDateFormat("MM/dd/yyyy, hh:mm aa", Locale.getDefault())
            val date1 = sdf1.format(date!!)
            tv_date.text = date1.toUpperCase(Locale.ROOT)
        } catch (e: Exception) {
            e.printStackTrace()
            tv_date.text = ""
        }
        tv_action_type.text = response.action
        tv_amount.text = "%.2f".format(response.amount)
        tv_user.text = response.employeeName
        et_reason.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    tv_char_count.text = s.length.toString()
                } else {
                    tv_char_count.text = "0"
                }
            }
        })
        deposit_dialog.show()
    }

    private fun addCashDepositApi(finalObject: JsonObject, dialog: Dialog) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addCashDeposit(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DepositActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@DepositActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        dialog.dismiss()
                        getDeposits()
                    } else {
                        Toast.makeText(
                            this@DepositActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun undoCashDepositApi(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addCashDeposit(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DepositActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        Toast.makeText(
                            this@DepositActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        deposit_dialog.dismiss()
                        getDeposits()
                    } else {
                        Toast.makeText(
                            this@DepositActivity,
                            respBody.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun withdrawalCashDepositDialog() {
        dialog_withdraw = Dialog(this)
        dialog_withdraw.window
        dialog_withdraw.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_withdraw.setContentView(R.layout.dialog_withdraw)
        dialog_withdraw.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_withdraw.setCanceledOnTouchOutside(false)
        val tv_currency_amount_header =
            dialog_withdraw.findViewById(R.id.tv_currency_amount_header) as TextView
        val et_amount = dialog_withdraw.findViewById(R.id.et_amount) as EditText
        val et_comment = dialog_withdraw.findViewById(R.id.et_comment) as EditText
        val ll_withdraw = dialog_withdraw.findViewById(R.id.ll_withdraw) as LinearLayout
        val ll_cancel = dialog_withdraw.findViewById(R.id.ll_cancel) as LinearLayout
        tv_currency_amount_header.text = currencyType
        ll_cancel.setOnClickListener {
            dialog_withdraw.dismiss()
        }
        ll_withdraw.setOnClickListener {
            val et_amount_stg = et_amount.text.toString().trim()
            val et_comment_stg = et_comment.text.toString().trim()
            when {
                et_amount_stg == "" -> {
                    Toast.makeText(
                        this, "Please Enter Withdrawal Amount",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_comment_stg == "" -> {
                    Toast.makeText(
                        this, "Please Enter Comment",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_amount_stg.toDouble() > totalDeposit ->{
                    Toast.makeText(
                        this, "Cannot withdraw amount more than total balance.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val jObj = JSONObject()
                    val countbillsarray = JSONArray()
                    jObj.put("actionType", "add_deposit")
                    jObj.put("action", "Withdrawal")
                    jObj.put("amount", et_amount_stg)
                    jObj.put("comment", et_comment_stg)
                    jObj.put("transactionType", TYPE_DEBIT)
                    jObj.put("countBillsList", countbillsarray)
                    jObj.put("cashDrawersId", cashDrawerId)
                    jObj.put("createdBy", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("status", 1)
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("finalObject", finalObject.toString())
                    addCashDepositApi(finalObject, dialog_withdraw)
                }
            }
        }
        dialog_withdraw.show()
    }

    private fun getAllDeposits(finalObject: JsonObject) {
        loading_dialog.show()
        var total = 0.00
        val api = ApiInterface.create()
        val call = api.getAllCashDepositsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DepositActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val cashDepositsList = respBody.cashDepositsList!!
                        if (cashDepositsList.size > 0) {
                            for (i in 0 until cashDepositsList.size) {
                                when {
                                    cashDepositsList[i].transactionType.toString() == "null" -> {
                                        // do not add
                                    }
                                    cashDepositsList[i].transactionType.toString() == TYPE_CREDIT -> {
                                        total += cashDepositsList[i].amount!!
                                    }
                                    cashDepositsList[i].transactionType.toString() == TYPE_DEBIT -> {
                                        total -= cashDepositsList[i].amount!!
                                    }
                                }
                            }
                            tv_depositbalance.text = "%.2f".format(total)
                            totalDeposit = total
                            val adapter =
                                DepositsListAdapter(this@DepositActivity, cashDepositsList)
                            rv_deposits.adapter = adapter
                            adapter.notifyDataSetChanged()
                        } else {
                            tv_depositbalance.text = getString(R.string.default_value_0)
                            totalDeposit = total
                        }
                    } else {
                        totalDeposit = total
                        tv_depositbalance.text = getString(R.string.default_value_0)
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@DepositActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@DepositActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@DepositActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@DepositActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class BillsAdapter(context: Context, bills: ArrayList<BillsListDataModel>) :
        RecyclerView.Adapter<BillsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<BillsListDataModel>? = null
        private var mSelectedList: ArrayList<BillsListDataModel> = ArrayList()

        init {
            this.mContext = context
            this.mList = bills
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BillsAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.count_bills_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: BillsAdapter.ViewHolder, position: Int) {
            holder.tv_bill_value.text = mList!![position].displayValue
            when (mList!![position].type) {
                "cents" -> {
                    holder.iv_bill_type.setImageResource(R.drawable.ic_cent)
                }
                "coins" -> {
                    holder.iv_bill_type.setImageResource(R.drawable.ic_dollar_coin)
                }
                "bills" -> {
                    holder.iv_bill_type.setImageResource(R.drawable.ic_dollar_bill)
                }
            }
            holder.tv_bill_count.text = mList!![position].count
            if (mSelectedList.isNotEmpty()) {
                if (mSelectedList.contains(mList!![position])) {
                    holder.ll_item_header.background =
                        ContextCompat.getDrawable(
                            mContext!!,
                            R.drawable.payment_terminal_selected
                        )
                    holder.tv_bill_count.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_bill_value.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                } else {
                    holder.ll_item_header.background = ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.deposit_bg_corners
                    )
                    holder.tv_bill_count.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.button_black
                        )
                    )
                    holder.tv_bill_value.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.button_black
                        )
                    )
                }
            } else {
                holder.ll_item_header.background = ContextCompat.getDrawable(
                    mContext!!,
                    R.drawable.deposit_bg_corners
                )
                holder.tv_bill_count.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.button_black
                    )
                )
                holder.tv_bill_value.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.button_black
                    )
                )
            }
        }

        fun selectedList(selectedList: ArrayList<BillsListDataModel>) {
            this.mSelectedList = selectedList
            notifyDataSetChanged()
        }

        fun getItem(position: Int): BillsListDataModel {
            return mList!![position]
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val ll_item_header = view.findViewById(R.id.ll_item_header) as LinearLayout
            val tv_bill_value = view.findViewById(R.id.tv_bill_value) as TextView
            val tv_bill_count = view.findViewById(R.id.tv_bill_count) as TextView
            val iv_bill_type = view.findViewById(R.id.iv_bill_type) as ImageView
        }
    }

    inner class DepositsListAdapter(context: Context, list: ArrayList<CashDepositsDataResponse>) :
        RecyclerView.Adapter<DepositsListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<CashDepositsDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.deposits_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): CashDepositsDataResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd/yyyy, hh:mm aa", Locale.getDefault())
                val date1 = sdf1.format(date!!)
                holder.tv_date.text = date1.toUpperCase(Locale.ROOT)
                holder.tv_action_type.text = mList!![position].action.toString()
                if (mList!![position].transactionType == TYPE_CREDIT) {
                    holder.tv_amount.text = "%.2f".format(mList!![position].amount)
                    holder.tv_amount.setTextColor(ContextCompat.getColor(mContext!!, R.color.black))
                } else if (mList!![position].transactionType == TYPE_DEBIT) {
                    holder.tv_amount.text = "-" + "%.2f".format(mList!![position].amount)
                    holder.tv_amount.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.current_status_open
                        )
                    )
                }
                holder.tv_user.text = mList!![position].employeeName.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            holder.ll_item_header.setOnClickListener {
//                if (mList!![position].transactionType == TYPE_CREDIT) {
                depositsDialog(mList!![position])
//                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_user: TextView = view.findViewById(R.id.tv_user)
            val tv_amount: TextView = view.findViewById(R.id.tv_amount)
            val tv_action_type: TextView = view.findViewById(R.id.tv_action_type)
            val tv_date: TextView = view.findViewById(R.id.tv_date)
            val ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
        }

    }

    override fun onBackPressed() {
        if (cv_header_countbills.visibility == View.VISIBLE) {
            cv_header_deposit.visibility = View.VISIBLE
            cv_header_countbills.visibility = View.GONE
            dialog_adddeposit.show()
        } else {
            super.onBackPressed()
        }
    }
}