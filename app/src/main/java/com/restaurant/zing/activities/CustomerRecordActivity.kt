package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.CustomerCreditsHistoryListAdapter
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.apiInterface.CustomerCreditsHistoryResponse
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n")
class CustomerRecordActivity : AppCompatActivity() {

    private lateinit var tv_customer_name: TextView
    private lateinit var tv_customer_phone: TextView
    private lateinit var tv_customer_email: TextView
    private lateinit var tv_customer_credits: TextView
    private lateinit var ll_customer_credits: LinearLayout
    private lateinit var tv_add_credit: TextView
    private lateinit var ll_add_credits: RelativeLayout
    private lateinit var tv_expiry_date: TextView
    private lateinit var et_credits_to_be_added: EditText
    private lateinit var ll_clear: LinearLayout
    private lateinit var ll_delete: LinearLayout
    private lateinit var ll_one: LinearLayout
    private lateinit var ll_four: LinearLayout
    private lateinit var ll_seven: LinearLayout
    private lateinit var ll_two: LinearLayout
    private lateinit var ll_five: LinearLayout
    private lateinit var ll_eight: LinearLayout
    private lateinit var ll_zero: LinearLayout
    private lateinit var ll_three: LinearLayout
    private lateinit var ll_six: LinearLayout
    private lateinit var ll_nine: LinearLayout
    private lateinit var ll_double_zero: LinearLayout
    private lateinit var ll_save_credits: LinearLayout
    private lateinit var ll_cancel: LinearLayout
    private lateinit var ll_next: LinearLayout
    private lateinit var tv_added_credits: TextView
    private lateinit var tv_date_expiry: TextView
    private lateinit var ll_add_credit_reason: LinearLayout
    private lateinit var tv_char_count: TextView
    private lateinit var ll_reason_cancel: LinearLayout
    private lateinit var ll_edit_amount: LinearLayout
    private lateinit var ll_save: LinearLayout
    private lateinit var et_credit_reason: EditText
    private lateinit var ll_credits_1: LinearLayout
    private lateinit var ll_credits_5: LinearLayout
    private lateinit var ll_credits_10: LinearLayout
    private lateinit var iv_back: ImageView
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var rv_credit_history: RecyclerView
    private lateinit var customerCreditsList: ArrayList<CustomerCreditsHistoryResponse>
    private lateinit var tv_currency_customer_credits: TextView
    private lateinit var tv_currency_credits_to_be_added: TextView
    private lateinit var tv_currency_added_credits: TextView
    private lateinit var tv_credit_1: TextView
    private lateinit var tv_credit_2: TextView
    private lateinit var tv_credit_3: TextView

    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var customerId = ""
    private var currencyType = ""
    private var firstName = ""
    private var lastName = ""
    private var email = ""
    private var phoneNumber = ""
    private var credits = ""
    private var credits_to_be_added_str = ""
    private var credits_to_be_added = 0.00
    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private var str_ten = "10"
    private var str_double_zero = "00"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_record)

        try {
            customerId = intent.getStringExtra("customerId")!!
            firstName = intent.getStringExtra("firstName")!!
            lastName = intent.getStringExtra("lastName")!!
            email = intent.getStringExtra("email")!!
            phoneNumber = intent.getStringExtra("phoneNumber")!!
            credits = intent.getStringExtra("credits")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()
        tv_currency_customer_credits.text = currencyType
        tv_currency_credits_to_be_added.text = currencyType
        tv_currency_added_credits.text = currencyType
        tv_credit_1.text = "${currencyType}1 ${getString(R.string.credit)}"
        tv_credit_2.text = "${currencyType}5 ${getString(R.string.credit)}"
        tv_credit_3.text = "${currencyType}10 ${getString(R.string.credit)}"
        tv_customer_name.text = "$firstName $lastName"
        tv_customer_phone.text = phoneNumber
        tv_customer_email.text = email
        tv_customer_credits.text = "%.2f".format(credits.toDouble())

        customerDetails()

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        tv_add_credit.setOnClickListener {
            ll_customer_credits.visibility = View.GONE
            ll_add_credits.visibility = View.VISIBLE
            ll_add_credit_reason.visibility = View.GONE
        }

        ll_next.setOnClickListener {
            if (et_credits_to_be_added.text.toString() == getString(R.string.default_value_0)) {
                Toast.makeText(this, "Enter a valid amount!", Toast.LENGTH_SHORT).show()
            } else {
                ll_customer_credits.visibility = View.GONE
                ll_add_credits.visibility = View.GONE
                ll_add_credit_reason.visibility = View.VISIBLE
            }
        }

        ll_clear.setOnClickListener {
            credits_to_be_added_str = getString(R.string.default_value_0)
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }

        ll_delete.setOnClickListener {
            if (credits_to_be_added_str != "") {
                // removes 'n' chars in string
                credits_to_be_added_str = credits_to_be_added_str.dropLast(1)
                if (credits_to_be_added_str == "") {
                    credits_to_be_added_str = getString(R.string.default_value_0)
                }
            } else {
                credits_to_be_added_str = getString(R.string.default_value_0)
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }

        ll_cancel.setOnClickListener {
            credits_to_be_added_str = getString(R.string.default_value_0)
            credits_to_be_added = 0.00
            et_credits_to_be_added.setText(getString(R.string.default_value_0))
            tv_added_credits.text = getString(R.string.default_value_0)
            ll_customer_credits.visibility = View.VISIBLE
            ll_add_credits.visibility = View.GONE
            ll_add_credit_reason.visibility = View.GONE
        }

        ll_edit_amount.setOnClickListener {
            ll_customer_credits.visibility = View.GONE
            ll_add_credits.visibility = View.VISIBLE
            ll_add_credit_reason.visibility = View.GONE
        }

        ll_reason_cancel.setOnClickListener {
            credits_to_be_added_str = getString(R.string.default_value_0)
            credits_to_be_added = 0.00
            et_credits_to_be_added.setText(getString(R.string.default_value_0))
            tv_added_credits.text = getString(R.string.default_value_0)
            ll_customer_credits.visibility = View.VISIBLE
            ll_add_credits.visibility = View.GONE
            ll_add_credit_reason.visibility = View.GONE
        }

        ll_save.setOnClickListener {
            if (et_credit_reason.text.toString() == "") {
                Toast.makeText(this, "Enter a valid reason!", Toast.LENGTH_SHORT).show()
            } else {
                val jsonObj = JSONObject()
                jsonObj.put("orderId", "0")
                jsonObj.put("credits", credits_to_be_added)
                jsonObj.put("creditsReason", et_credit_reason.text.toString())
                jsonObj.put("customerId", customerId)
                jsonObj.put("transactionType", "credit")
                jsonObj.put("createdBy", mEmployeeId)
                jsonObj.put("restaurantId", restaurantId)
                jsonObj.put("status", 1)
                val fObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                addCredits(fObject)
            }
        }

        ll_one.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_one
            } else {
                credits_to_be_added_str += str_one
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_two.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_two
            } else {
                credits_to_be_added_str += str_two
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_three.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_three
            } else {
                credits_to_be_added_str += str_three
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_four.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_four
            } else {
                credits_to_be_added_str += str_four
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_five.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_five
            } else {
                credits_to_be_added_str += str_five
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_six.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_six
            } else {
                credits_to_be_added_str += str_six
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_seven.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_seven
            } else {
                credits_to_be_added_str += str_seven
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_eight.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_eight
            } else {
                credits_to_be_added_str += str_eight
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_nine.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_nine
            } else {
                credits_to_be_added_str += str_nine
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_zero.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_zero
            } else {
                credits_to_be_added_str += str_zero
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_double_zero.setOnClickListener {
            if (credits_to_be_added_str == getString(R.string.default_value_0)) {
                credits_to_be_added_str = str_double_zero
            } else {
                credits_to_be_added_str += str_double_zero
            }
            credits_to_be_added = credits_to_be_added_str.toDouble()
            credits_to_be_added /= 100
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_credits_1.setOnClickListener {
            credits_to_be_added_str = str_one
            credits_to_be_added = credits_to_be_added_str.toDouble()
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_credits_5.setOnClickListener {
            credits_to_be_added_str = str_five
            credits_to_be_added = credits_to_be_added_str.toDouble()
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        ll_credits_10.setOnClickListener {
            credits_to_be_added_str = str_ten
            credits_to_be_added = credits_to_be_added_str.toDouble()
            et_credits_to_be_added.setText("%.2f".format(credits_to_be_added))
            tv_added_credits.text = "%.2f".format(credits_to_be_added)
        }
        et_credits_to_be_added.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.isNotEmpty()) {
                    if (s.toString() == getString(R.string.default_value_0)) {
                        ll_save_credits.visibility = View.INVISIBLE
                    } else {
                        ll_save_credits.visibility = View.VISIBLE
                    }
                } else {
                    ll_save_credits.visibility = View.INVISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    Log.d("size", s.toString())
                    if (s == getString(R.string.default_value_0)) {
                        ll_save_credits.visibility = View.INVISIBLE
                    } else {
                        ll_save_credits.visibility = View.VISIBLE
                    }
                } else {
                    ll_save_credits.visibility = View.INVISIBLE
                }
            }
        })

        et_credit_reason.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    tv_char_count.text = s.length.toString()
                } else {
                    tv_char_count.text = "0"
                }
            }
        })
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        tv_customer_name = findViewById(R.id.tv_customer_name)
        tv_customer_phone = findViewById(R.id.tv_customer_phone)
        tv_customer_email = findViewById(R.id.tv_customer_email)
        tv_customer_credits = findViewById(R.id.tv_customer_credits)
        ll_customer_credits = findViewById(R.id.ll_customer_credits)
        tv_add_credit = findViewById(R.id.tv_add_credit)
        ll_add_credits = findViewById(R.id.ll_add_credits)
        tv_expiry_date = findViewById(R.id.tv_expiry_date)
        et_credits_to_be_added = findViewById(R.id.et_credits_to_be_added)
        ll_clear = findViewById(R.id.ll_clear)
        ll_delete = findViewById(R.id.ll_delete)
        ll_one = findViewById(R.id.ll_one)
        ll_four = findViewById(R.id.ll_four)
        ll_seven = findViewById(R.id.ll_seven)
        ll_two = findViewById(R.id.ll_two)
        ll_five = findViewById(R.id.ll_five)
        ll_eight = findViewById(R.id.ll_eight)
        ll_zero = findViewById(R.id.ll_zero)
        ll_three = findViewById(R.id.ll_three)
        ll_six = findViewById(R.id.ll_six)
        ll_nine = findViewById(R.id.ll_nine)
        ll_double_zero = findViewById(R.id.ll_double_zero)
        ll_save_credits = findViewById(R.id.ll_save_credits)
        ll_cancel = findViewById(R.id.ll_cancel)
        ll_next = findViewById(R.id.ll_next)
        tv_added_credits = findViewById(R.id.tv_added_credits)
        tv_date_expiry = findViewById(R.id.tv_date_expiry)
        ll_add_credit_reason = findViewById(R.id.ll_add_credit_reason)
        tv_char_count = findViewById(R.id.tv_char_count)
        ll_reason_cancel = findViewById(R.id.ll_reason_cancel)
        ll_edit_amount = findViewById(R.id.ll_edit_amount)
        ll_save = findViewById(R.id.ll_save)
        et_credit_reason = findViewById(R.id.et_credit_reason)
        ll_credits_1 = findViewById(R.id.ll_credits_1)
        ll_credits_10 = findViewById(R.id.ll_credits_10)
        ll_credits_5 = findViewById(R.id.ll_credits_5)
        iv_back = findViewById(R.id.iv_back)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tv_currency_customer_credits = findViewById(R.id.tv_currency_customer_credits)
        tv_currency_credits_to_be_added = findViewById(R.id.tv_currency_credits_to_be_added)
        tv_currency_added_credits = findViewById(R.id.tv_currency_added_credits)
        tv_credit_1 = findViewById(R.id.tv_credit_1)
        tv_credit_2 = findViewById(R.id.tv_credit_2)
        tv_credit_3 = findViewById(R.id.tv_credit_3)
        rv_credit_history = findViewById(R.id.rv_credit_history)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_credit_history.layoutManager = layoutManager
        rv_credit_history.hasFixedSize()

        customerCreditsList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun customerDetails() {
        val jsonObj = JSONObject()
        jsonObj.put("userId", mEmployeeId)
        jsonObj.put("restaurantId", restaurantId)
        jsonObj.put("cid", customerId)
        val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
        getCustomerDetails(final_object)

        val jObj = JSONObject()
        jObj.put("customerId", customerId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getCustomerCreditsHistory(finalObject)
    }

    private fun getCustomerDetails(customerObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewCustomerApi(customerObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CustomerRecordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()
                    if (respBody!!.responseStatus == 1) {
                        val customerList = respBody.customer
                        if (customerList!!.size > 0) {
                            for (i in 0 until customerList.size) {
                                tv_customer_name.text =
                                    "${customerList[i].firstName.toString()} ${customerList[i].lastName.toString()}"
                                tv_customer_phone.text = customerList[i].phoneNumber.toString()
                                tv_customer_email.text = customerList[i].email.toString()
                                if (customerList[i].credits.toString() == "null") {
                                    tv_customer_credits.text = getString(R.string.default_value_0)
                                } else {
                                    tv_customer_credits.text =
                                        "%.2f".format(customerList[i].credits!!)
                                }
                            }
                        }
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCustomerCreditsHistory(customerObject: JsonObject) {
        loading_dialog.show()
        customerCreditsList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getCustomerCreditsHistoryApi(customerObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CustomerRecordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()
                    if (respBody!!.responseStatus == 1) {
                        customerCreditsList = respBody.customerCreditsList!!
                    } else {
                        Toast.makeText(
                            this@CustomerRecordActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val adapter = CustomerCreditsHistoryListAdapter(
                        this@CustomerRecordActivity,
                        customerCreditsList
                    )
                    rv_credit_history.adapter = adapter
                    adapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun addCredits(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addCreditsToCustomer(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@CustomerRecordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                Log.d("reponse", response.toString())
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        ll_customer_credits.visibility = View.VISIBLE
                        ll_add_credits.visibility = View.GONE
                        ll_add_credit_reason.visibility = View.GONE
                        et_credits_to_be_added.setText(getString(R.string.default_value_0))
                        tv_added_credits.text = getString(R.string.default_value_0)
                        et_credit_reason.setText("")
                        customerDetails()
                        Toast.makeText(
                            this@CustomerRecordActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this@CustomerRecordActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@CustomerRecordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@CustomerRecordActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@CustomerRecordActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@CustomerRecordActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onBackPressed() {
        when {
            ll_add_credit_reason.visibility == View.VISIBLE -> {
                ll_customer_credits.visibility = View.GONE
                ll_add_credits.visibility = View.VISIBLE
                ll_add_credit_reason.visibility = View.GONE
            }
            ll_add_credits.visibility == View.VISIBLE -> {
                ll_customer_credits.visibility = View.VISIBLE
                ll_add_credits.visibility = View.GONE
                ll_add_credit_reason.visibility = View.GONE
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

}