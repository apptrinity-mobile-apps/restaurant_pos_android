package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.ConstantURLs
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import kotlinx.android.synthetic.main.activity_table_setup.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TableSetupActivity : AppCompatActivity() {

    private lateinit var loading_dialog: Dialog
    private lateinit var ll_previous_checks: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var img_back_id: ImageView
    private lateinit var tv_test_mode: TextView
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var getServiceAreaOptions: HashMap<String, Any>
    private var serviceAreaId = ""
    private var revenueCenterId = ""
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var deviceToken = ""
    private var isTestModeEnabled = false

    @SuppressLint("AddJavascriptInterface", "SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table_setup)
        init()
        loadingDialog()
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            deviceToken = it!!.toString()
            Log.d("token", deviceToken)
        }
        if (isTestModeEnabled) {
            tv_test_mode.visibility = View.VISIBLE
        } else {
            tv_test_mode.visibility = View.GONE
        }
        val obj_dine = JSONObject()
        obj_dine.put("restaurantId", restaurantId)
        val jObject_dine = JsonParser.parseString(obj_dine.toString()).asJsonObject
        getServiceAreaOptionsAPI(jObject_dine)

        img_back_id.setOnClickListener {
            super.onBackPressed()
        }
        ll_previous_checks.setOnClickListener {
            val intent = Intent(this@TableSetupActivity, PaymentTerminalActivity::class.java)
            intent.putExtra("employeeId", "")
            intent.putExtra("from_screen", "home")
            intent.putExtra("show_tab", "paid")
            startActivity(intent)
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        table_web_view.settings.javaScriptEnabled = true
        table_web_view.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                loading_dialog.show()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                loading_dialog.dismiss()
            }
        }
    }

    private fun init() {
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        getServiceAreaOptions = sessionManager.getServiceAreaOptions
        serviceAreaId = getServiceAreaOptions[SessionManager.SERVICE_AREA_ID_KEY].toString()
        revenueCenterId = getServiceAreaOptions[SessionManager.REVENUE_CENTER_ID_KEY].toString()
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        isTestModeEnabled = sessionManager.getRestaurantMode
        ll_previous_checks = findViewById(R.id.ll_previous_checks)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        img_back_id = findViewById(R.id.img_back_id)
        tv_test_mode = findViewById(R.id.tv_test_mode)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getServiceAreaOptionsAPI(json_obj: JsonObject) {
        val serviceAreaArray = ArrayList<ServiceAreasListResponse>()
        val serviceAreaOptionNameArray = ArrayList<String>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getServiceAreasApi(json_obj)
        call.enqueue(object : Callback<ServiceAreasResponse> {
            override fun onFailure(call: Call<ServiceAreasResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ServiceAreasResponse>,
                response: Response<ServiceAreasResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()!!
                    if (resp.responseStatus!! == 1) {
                        val list = resp.service_areasList!!
                        for (i in 0 until list.size) {
                            serviceAreaArray.add(list[i])
                            serviceAreaOptionNameArray.add(list[i].serviceName!!)
                        }
                    } else {
                        Toast.makeText(
                            this@TableSetupActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    val adapter = ArrayAdapter<String>(
                        this@TableSetupActivity,
                        R.layout.spinner_dinning_option_item,
                        serviceAreaOptionNameArray
                    )
                    sp_dinning_option_id.adapter = adapter
                    if (serviceAreaArray.size > 0) {
                        var selected = -1
                        for (i in 0 until serviceAreaArray.size) {
                            if (serviceAreaId == serviceAreaArray[i].id.toString()) {
                                selected = i
                            }
                        }
                        sp_dinning_option_id.setSelection(selected)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

        sp_dinning_option_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                (arg0.getChildAt(0) as TextView).setTextColor(Color.WHITE)
                Log.e(
                    "open_item_id",
                    serviceAreaArray[position].id.toString() + "====" + serviceAreaArray[position].serviceName.toString() + "====" + serviceAreaArray[position].revenueCenter.toString()
                )
                serviceAreaId = serviceAreaArray[position].id.toString()
                revenueCenterId = serviceAreaArray[position].revenueCenter.toString()
                val url =
                    "${ConstantURLs.TABLE_SERVICE}?userId=$mEmployeeId&restaurantId=$restaurantId&serviceArea=$serviceAreaId&deviceToken=$deviceToken"
                println(url)
                table_web_view.loadUrl(url)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}

        }
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TableSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@TableSetupActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@TableSetupActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@TableSetupActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (table_web_view.canGoBack()) {
                        table_web_view.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy() {
        table_web_view.removeJavascriptInterface("HtmlViewer")
        super.onDestroy()
    }

}