package com.restaurant.zing.activities

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.ShiftReviewListAdapter
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShiftReviewActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_filter: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var rv_employee_shift: RecyclerView
    private lateinit var ll_view_status: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shift_review)

        initialize()

        val jObj = JSONObject()
        jObj.put("createdBy", mEmployeeId) //employee id
        jObj.put("restaurantId", restaurantId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        viewEmployeeShiftReviewManager(finalObject)

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        ll_filter = findViewById(R.id.ll_filter)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        rv_employee_shift = findViewById(R.id.rv_employee_shift)
        ll_view_status = findViewById(R.id.ll_view_status)

        val layoutManager =
            LinearLayoutManager(this@ShiftReviewActivity, RecyclerView.VERTICAL, false)
        rv_employee_shift.layoutManager = layoutManager
        rv_employee_shift.hasFixedSize()
        val dividerItemDecoration =
            DividerItemDecoration(rv_employee_shift.context, layoutManager.orientation)
        rv_employee_shift.addItemDecoration(dividerItemDecoration)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun viewEmployeeShiftReviewManager(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewEmployeeShiftReviewManagerApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@ShiftReviewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val customersList = respBody.managerLevelShiftReviewList!!
                        if (customersList.size > 0) {
                            val adapter =
                                ShiftReviewListAdapter(this@ShiftReviewActivity, customersList)
                            rv_employee_shift.adapter = adapter
                            adapter.notifyDataSetChanged()
                        } else {
                            Toast.makeText(
                                this@ShiftReviewActivity,
                                respBody.result.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ShiftReviewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@ShiftReviewActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@ShiftReviewActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@ShiftReviewActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        try {
            val jObj = JSONObject()
            jObj.put("createdBy", mEmployeeId) //employee id
            jObj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            viewEmployeeShiftReviewManager(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}