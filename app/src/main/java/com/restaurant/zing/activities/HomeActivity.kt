package com.restaurant.zing.activities

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.helpers.RecyclerItemClickListener
import com.restaurant.zing.adapters.AllCashDrawersListAdapter
import com.restaurant.zing.adapters.HomeListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.ConstantURLs
import com.restaurant.zing.helpers.HomeListDataModel
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : AppCompatActivity() {

    private lateinit var mModeList: ArrayList<HomeListDataModel>
    private lateinit var mManagerActivityList: ArrayList<HomeListDataModel>
    private lateinit var mCashManagementList: ArrayList<HomeListDataModel>
    private lateinit var mReportsList: ArrayList<HomeListDataModel>
    private lateinit var mMyAccountList: ArrayList<HomeListDataModel>
    private lateinit var mSupportList: ArrayList<HomeListDataModel>
    private lateinit var mSetupList: ArrayList<HomeListDataModel>

    private lateinit var rv_mode: RecyclerView
    private lateinit var rv_manager_activities: RecyclerView
    private lateinit var rv_cash_management: RecyclerView
    private lateinit var rv_reports: RecyclerView
    private lateinit var rv_my_account: RecyclerView
    private lateinit var rv_support: RecyclerView
    private lateinit var rv_setup: RecyclerView
    private lateinit var tv_switch_user: TextView
    private lateinit var tv_restaurant_name: TextView
    private lateinit var tv_user_name: TextView
    private lateinit var mPayOutDialog: Dialog
    private lateinit var loading_dialog: Dialog
    private lateinit var tv_test_mode: TextView

    private lateinit var modesAdapter: HomeListAdapter
    private lateinit var managerActAdapter: HomeListAdapter
    private lateinit var cashManagerAdapter: HomeListAdapter
    private lateinit var reportsAdapter: HomeListAdapter
    private lateinit var myAccountAdapter: HomeListAdapter
    private lateinit var supportAdapter: HomeListAdapter
    private lateinit var setupAdapter: HomeListAdapter

    private lateinit var allCashDrawersList: ArrayList<CashDrawersDataResponse>

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var posAccessPermissions: HashMap<String, Boolean>
    private lateinit var webSetupAccessPermissions: HashMap<String, Boolean>
    private lateinit var getDeviceDetails: HashMap<String, String>
    private var isTestModeEnabled = false
    private var cashDrawerId = ""
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var mEmployeeName = ""
    private var restaurantName = ""
    private var restaurantId = ""
    private var currencyType = ""
    private val mTAG = "HomeActivity"
    private var deviceName = ""
    private var deviceNameId = ""

    // pos access permission
    private var hasAddOrUpdateServiceChargesPermission = false
    private var hasApplyCashPaymentsPermission = false
    private var hasCashDrawerAccessPermission = false
    private var hasChangeServerPermission = false
    private var hasChangeTablePermission = false
    private var hasEditOtherEmployeesOrdersPermission = false
    private var hasKeyInCreditCardsPermission = false
    private var hasKitchenDisplayScreenModePermission = false
    private var hasMyReportsPermission = false
    private var hasNoSalePermission = false
    private var hasOfflineOrBackgroundCreditCardProcessingPermission = false
    private var hasPendingOrdersModePermission = false
    private var hasPaymentTerminalModePermission = false
    private var hasQuickOrderModePermission = false
    private var hasShiftReviewSalesDataPermission = false
    private var hasTableServiceModePermission = false
    private var hasViewOtherEmployeesOrdersPermission = false

    //web setup permissions
    private var dataExportConfig = false
    private var discountsSetup = false
    private var financialAccounts = false
    private var kitchenOrDiningRoomSetup = false
    private var manageInstructions = false
    private var paymentsSetup = false
    private var publishing = false
    private var restaurantGroupsSetup = false
    private var restaurantOperationsSetup = false
    private var taxRatesSetup = false
    private var userPermissions = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initialize()
        tv_restaurant_name.text = restaurantName
        tv_user_name.text = mEmployeeName
        getDefaults()
        setupHomeList()
        /*// to change app icon programmatically, add new alias launcher activity in manifest with app icon-new
        val manager = packageManager
        // disable current or old launcher activity
        manager.setComponentEnabledSetting(ComponentName(this,RestaurantLoginActivity::class.java),PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP)
        // enable new alias activity with new app icon
        manager.setComponentEnabledSetting(ComponentName(this,"com.restaurant.zing.Activities.RestaurantLoginActivityAlias"),PackageManager.COMPONENT_ENABLED_STATE_ENABLED,PackageManager.DONT_KILL_APP)*/
        tv_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        rv_mode.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "mode position clicked - $position")
                        if (modesAdapter.getItem(position).name == getString(R.string.header_table_service)) {
                            if (hasTableServiceModePermission) {
                                val intent =
                                    Intent(this@HomeActivity, TableSetupActivity::class.java)
                                startActivity(intent)
                            } else {
                                Toast.makeText(
                                    this@HomeActivity,
                                    getString(R.string.no_access),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else if (modesAdapter.getItem(position).name == getString(R.string.header_quick_order)) {
                            if (hasQuickOrderModePermission) {
                                val intent =
                                    Intent(this@HomeActivity, OrderDetailsActivity::class.java)
                                intent.putExtra("from_screen", "home")
                                startActivity(intent)
                            } else {
                                Toast.makeText(
                                    this@HomeActivity,
                                    getString(R.string.no_access),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else if (modesAdapter.getItem(position).name == getString(R.string.header_delivery)) {
                            val intent = Intent(this@HomeActivity, DeliveryActivity::class.java)
                            startActivity(intent)
                        } else if (modesAdapter.getItem(position).name.contains(getString(R.string.header_pending_orders))) {
                            if (hasPendingOrdersModePermission) {
                                val intent =
                                    Intent(this@HomeActivity, PendingOrdersActivity::class.java)
                                intent.putExtra("from_screen", "home")
                                startActivity(intent)
                            } else {
                                Toast.makeText(
                                    this@HomeActivity,
                                    getString(R.string.no_access),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else if (modesAdapter.getItem(position).name == getString(R.string.header_payment_terminal)) {
                            if (hasPaymentTerminalModePermission) {
                                val intent =
                                    Intent(
                                        this@HomeActivity,
                                        PaymentTerminalActivity::class.java
                                    )
                                intent.putExtra("employeeId", "")
                                intent.putExtra("from_screen", "home")
                                intent.putExtra("show_tab", "open")
                                startActivity(intent)
                            } else {
                                Toast.makeText(
                                    this@HomeActivity,
                                    getString(R.string.no_access),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else if (modesAdapter.getItem(position).name == getString(R.string.header_kitchen_display_screen)) {
                            if (hasKitchenDisplayScreenModePermission) {
                                val intent =
                                    Intent(
                                        this@HomeActivity,
                                        KitchenDisplayActivity::class.java
                                    )
                                intent.putExtra("from_screen", "home")
                                startActivity(intent)
                            } else {
                                Toast.makeText(
                                    this@HomeActivity,
                                    getString(R.string.no_access),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                })
        )
        rv_manager_activities.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "manager position clicked - $position")
                        when (managerActAdapter.getItem(position).name) {
                            getString(R.string.header_shift_review) -> {
                                val intent =
                                    Intent(this@HomeActivity, ShiftReviewActivity::class.java)
                                startActivity(intent)
                            }
                            getString(R.string.header_close_out_day) -> {
                                val intent =
                                    Intent(this@HomeActivity, CloseOutDayActivity::class.java)
                                startActivity(intent)
                            }
                            getString(R.string.header_time_cards) -> {
                                val intent =
                                    Intent(this@HomeActivity, TimeCardsActivity::class.java)
                                startActivity(intent)
                            }
                            getString(R.string.header_transfer_gift_card) -> {
                                val intent =
                                    Intent(this@HomeActivity, TransferGiftCardActivity::class.java)
                                startActivity(intent)
                            }
                            getString(R.string.header_register_swipe_card) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, RegisterSwipeCardActivity::class.java)
                                startActivity(intent)*/
                            }
                            getString(R.string.header_lookup_customer) -> {
                                val intent =
                                    Intent(this@HomeActivity, LookupCustomerActivity::class.java)
                                startActivity(intent)
                            }
                            getString(R.string.header_find_checks) -> {
                                val intent =
                                    Intent(this@HomeActivity, FindChecksActivity::class.java)
                                intent.putExtra("selected_tab", "check_no")
                                startActivity(intent)
                            }
                        }
                    }
                })
        )
        rv_cash_management.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "cash position clicked - $position")
                        when (cashManagerAdapter.getItem(position).name) {
                            getString(R.string.header_cash_drawers) -> {
                                if (hasCashDrawerAccessPermission) {
                                    val intent =
                                        Intent(this@HomeActivity, CashDrawerActivity::class.java)
                                    startActivity(intent)
                                } else {
                                    Toast.makeText(
                                        this@HomeActivity,
                                        getString(R.string.no_access),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                            getString(R.string.header_pay_out) -> {
                                showPayOutDialog()
                            }
                            getString(R.string.header_deposits) -> {
                                val intent = Intent(this@HomeActivity, DepositActivity::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                })
        )
        rv_reports.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "reports position clicked - $position")
                        when (reportsAdapter.getItem(position).name) {
                            getString(R.string.header_sales_reports) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra("title", getString(R.string.header_sales_reports))
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_menu_reports) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra("title", getString(R.string.header_menu_reports))
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_labor_reports) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra("title", getString(R.string.header_labor_reports))
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                })
        )
        rv_my_account.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "myAccount position clicked - $position")
                        when (myAccountAdapter.getItem(position).name) {
                            getString(R.string.header_shift_review) -> {
                                val intent =
                                    Intent(
                                        this@HomeActivity,
                                        ShiftReviewDetailedViewActivity::class.java
                                    )
                                intent.putExtra("empId", "")
                                startActivity(intent)
                            }
                            getString(R.string.header_time_clock) -> {
                                val intent =
                                    Intent(this@HomeActivity, EmployeeTimeClockActivity::class.java)
                                intent.putExtra("empId", mEmployeeId)
                                startActivity(intent)
                            }
                            getString(R.string.header_sales_report) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra("title", getString(R.string.header_sales_report))
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                })
        )
        rv_support.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "support position clicked - $position")
                        when (supportAdapter.getItem(position).name) {
                            getString(R.string.header_support_site) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra("title", getString(R.string.header_support_site))
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_status_page) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra("title", getString(R.string.header_status_page))
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_start_screen_share) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra(
                                    "title",
                                    getString(R.string.header_start_screen_share)
                                )
                                startActivity(intent)*/
                                /*var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }*/
                            }
                        }
                    }
                })
        )
        rv_setup.addOnItemTouchListener(
            RecyclerItemClickListener(this@HomeActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.d(mTAG, "setup position clicked - $position")
                        when (setupAdapter.getItem(position).name) {
                            getString(R.string.header_menu) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra(
                                    "title",
                                    getString(R.string.header_menu)
                                )
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_tables) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra(
                                    "title",
                                    getString(R.string.header_tables)
                                )
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_labor) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra(
                                    "title",
                                    getString(R.string.header_labor)
                                )
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_other_setup) -> {
                                /*val intent =
                                    Intent(this@HomeActivity, DefaultWebViewActivity::class.java)
                                intent.putExtra(
                                    "title",
                                    getString(R.string.header_other_setup)
                                )
                                startActivity(intent)*/
                                var url = ConstantURLs.HOME
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://$url"
                                try {
                                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    startActivity(Intent.createChooser(myIntent, "Open with"))
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                            getString(R.string.header_device_setup) -> {
                                val intent =
                                    Intent(this@HomeActivity, DeviceSetupActivity::class.java)
                                startActivity(intent)
                            }
                            getString(R.string.header_enable_test_mode) -> {
                                val message: String = if (isTestModeEnabled) {
                                    "Do you wish to Exit Test mode."
                                } else {
                                    "Do you wish to Enter Test mode."
                                }
                                val alert = AlertDialog.Builder(this@HomeActivity)
                                alert.setTitle("Alert!")
                                alert.setMessage(message)
                                alert.setPositiveButton(
                                    getString(R.string.yes)
                                ) { dialog, _ ->
                                    val jsonObj = JSONObject()
                                    jsonObj.put("restaurantId", restaurantId)
                                    jsonObj.put("employeeId", mEmployeeId)
                                    val final_object =
                                        JsonParser.parseString(jsonObj.toString()).asJsonObject
                                    enableTestMode(final_object)
                                    dialog!!.dismiss()
                                }
                                alert.setNegativeButton(
                                    getString(R.string.no)
                                ) { dialog, _ -> dialog!!.dismiss() }
                                alert.setCancelable(false)
                                alert.show()
                            }
                        }
                    }
                })
        )
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        restaurantName = restaurantDetails[SessionManager.RESTAURANT_NAME_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        posAccessPermissions = sessionManager.getPosAccessPermissions
        webSetupAccessPermissions = sessionManager.getWebSetupAccessPermissions
        getDeviceDetails = sessionManager.getDeviceDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        mEmployeeName =
            employeeDetails[SessionManager.EMPLOYEE_FIRST_NAME_KEY]!! + " " + employeeDetails[SessionManager.EMPLOYEE_LAST_NAME_KEY]!!
        isTestModeEnabled = sessionManager.getRestaurantMode
        deviceName = getDeviceDetails[SessionManager.DEVICE_NAME]!!
        deviceNameId = getDeviceDetails[SessionManager.DEVICE_NAME_ID]!!
        mModeList = ArrayList()
        mManagerActivityList = ArrayList()
        mCashManagementList = ArrayList()
        mReportsList = ArrayList()
        mMyAccountList = ArrayList()
        mSupportList = ArrayList()
        mSetupList = ArrayList()

        // pos access permissions
        hasAddOrUpdateServiceChargesPermission =
            posAccessPermissions[SessionManager.ADD_OR_UPDATE_SERVICE_CHARGES_POS_ACCESS_KEY]!!
        hasApplyCashPaymentsPermission =
            posAccessPermissions[SessionManager.APPLY_CASH_PAYMENTS_POS_ACCESS_KEY]!!
        hasCashDrawerAccessPermission =
            posAccessPermissions[SessionManager.CASH_DRAWER_ACCESS_POS_ACCESS_KEY]!!
        hasChangeServerPermission =
            posAccessPermissions[SessionManager.CHANGE_SERVER_POS_ACCESS_KEY]!!
        hasChangeTablePermission =
            posAccessPermissions[SessionManager.CHANGE_TABLE_POS_ACCESS_KEY]!!
        hasEditOtherEmployeesOrdersPermission =
            posAccessPermissions[SessionManager.EDIT_OTHER_EMPLOYEE_ORDERS_POS_ACCESS_KEY]!!
        hasKeyInCreditCardsPermission =
            posAccessPermissions[SessionManager.KEY_IN_CREDIT_CARDS_POS_ACCESS_KEY]!!
        hasKitchenDisplayScreenModePermission =
            posAccessPermissions[SessionManager.KITCHEN_DISPLAY_SCREEN_MODE_POS_ACCESS_KEY]!!
        hasMyReportsPermission = posAccessPermissions[SessionManager.MY_REPORTS_POS_ACCESS_KEY]!!
        hasNoSalePermission = posAccessPermissions[SessionManager.NO_SALE_POS_ACCESS_KEY]!!
        hasOfflineOrBackgroundCreditCardProcessingPermission =
            posAccessPermissions[SessionManager.OFFLINE_OR_BACKGROUND_CREDIT_CARD_PROCESSING_POS_ACCESS_KEY]!!
        hasPendingOrdersModePermission =
            posAccessPermissions[SessionManager.PENDING_ORDERS_MODE_POS_ACCESS_KEY]!!
        hasPaymentTerminalModePermission =
            posAccessPermissions[SessionManager.PAYMENT_TERMINAL_MODE_POS_ACCESS_KEY]!!
        hasQuickOrderModePermission =
            posAccessPermissions[SessionManager.QUICK_ORDER_MODE_POS_ACCESS_KEY]!!
        hasShiftReviewSalesDataPermission =
            posAccessPermissions[SessionManager.SHIFT_REVIEW_SALES_DATA_POS_ACCESS_KEY]!!
        hasTableServiceModePermission =
            posAccessPermissions[SessionManager.TABLE_SERVICE_MODE_POS_ACCESS_KEY]!!
        hasViewOtherEmployeesOrdersPermission =
            posAccessPermissions[SessionManager.VIEW_OTHER_EMPLOYEES_ORDERS_POS_ACCESS_KEY]!!

        //web setup access permissions
        dataExportConfig =
            webSetupAccessPermissions[SessionManager.DATA_EXPORT_CONFIG_WEB_SETUP_ACCESS_KEY]!!
        discountsSetup =
            webSetupAccessPermissions[SessionManager.DISCOUNTS_SETUP_WEB_SETUP_ACCESS_KEY]!!
        financialAccounts =
            webSetupAccessPermissions[SessionManager.FINANCIAL_ACCOUNTS_WEB_WEB_SETUP_ACCESS_KEY]!!
        kitchenOrDiningRoomSetup =
            webSetupAccessPermissions[SessionManager.KITCHEN_OR_DINING_ROOM_SETUP_WEB_SETUP_ACCESS_KEY]!!
        manageInstructions =
            webSetupAccessPermissions[SessionManager.MANAGE_INSTRUCTIONS_WEB_SETUP_ACCESS_KEY]!!
        paymentsSetup =
            webSetupAccessPermissions[SessionManager.PAYMENTS_SETUP_WEB_SETUP_ACCESS_KEY]!!
        publishing = webSetupAccessPermissions[SessionManager.PUBLISHING_WEB_SETUP_ACCESS_KEY]!!
        restaurantGroupsSetup =
            webSetupAccessPermissions[SessionManager.RESTAURANT_GROUPS_SETUP_WEB_SETUP_ACCESS_KEY]!!
        restaurantOperationsSetup =
            webSetupAccessPermissions[SessionManager.RESTAURANT_OPERATIONS_SETUP_WEB_SETUP_ACCESS_KEY]!!
        taxRatesSetup =
            webSetupAccessPermissions[SessionManager.TAX_RATES_SETUP_WEB_SETUP_ACCESS_KEY]!!
        userPermissions =
            webSetupAccessPermissions[SessionManager.USER_PERMISSIONS_WEB_WEB_SETUP_ACCESS_KEY]!!

        rv_mode = findViewById(R.id.rv_mode)
        rv_manager_activities = findViewById(R.id.rv_manager_activities)
        rv_cash_management = findViewById(R.id.rv_cash_management)
        rv_reports = findViewById(R.id.rv_reports)
        rv_my_account = findViewById(R.id.rv_my_account)
        rv_support = findViewById(R.id.rv_support)
        rv_setup = findViewById(R.id.rv_setup)
        tv_switch_user = findViewById(R.id.tv_switch_user)
        tv_restaurant_name = findViewById(R.id.tv_restaurant_name)
        tv_user_name = findViewById(R.id.tv_user_name)
        tv_test_mode = findViewById(R.id.tv_test_mode)

        val modeLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_mode.layoutManager = modeLinearLayoutManager
        val managerActLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_manager_activities.layoutManager = managerActLinearLayoutManager
        val cashManagerLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_cash_management.layoutManager = cashManagerLinearLayoutManager
        val reportsLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_reports.layoutManager = reportsLinearLayoutManager
        val myAccLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_my_account.layoutManager = myAccLinearLayoutManager
        val supportLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_support.layoutManager = supportLinearLayoutManager
        val setupLinearLayoutManager =
            LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        rv_setup.layoutManager = setupLinearLayoutManager
        rv_mode.hasFixedSize()
        rv_manager_activities.hasFixedSize()
        rv_cash_management.hasFixedSize()
        rv_reports.hasFixedSize()
        rv_my_account.hasFixedSize()
        rv_support.hasFixedSize()
        rv_setup.hasFixedSize()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getDefaults() {
        val jObj = JSONObject()
        jObj.put("restaurantId", restaurantId)
        jObj.put("employeeId", mEmployeeId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getUnapprovedOrders(finalObject)
        getRestaurantMode(finalObject)
        val obj = JSONObject()
        obj.put("restaurantId", restaurantId)
        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
        getAllCashDrawers(finalObject = jObject)
    }

    private fun setupHomeList() {
        // mode
        mModeList.add(
            HomeListDataModel(
                getString(R.string.header_table_service),
                R.drawable.ic_home_table_service
            )
        )
        mModeList.add(
            HomeListDataModel(
                getString(R.string.header_quick_order),
                R.drawable.ic_home_quick_order
            )
        )
        mModeList.add(
            HomeListDataModel(
                getString(R.string.header_kitchen_display_screen),
                R.drawable.ic_home_kitchen_display
            )
        )
        mModeList.add(
            HomeListDataModel(
                getString(R.string.header_payment_terminal),
                R.drawable.ic_home_payment_terminal
            )
        )
        mModeList.add(
            HomeListDataModel(
                getString(R.string.header_delivery),
                R.drawable.ic_home_delivery
            )
        )
        mModeList.add(
            HomeListDataModel(
                getString(R.string.header_pending_orders),
                R.drawable.ic_home_pending_orders
            )
        )

        modesAdapter = HomeListAdapter(this@HomeActivity, mModeList)
        rv_mode.adapter = modesAdapter
        modesAdapter.notifyDataSetChanged()

        // manager activities
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_shift_review),
                R.drawable.ic_home_shift_review
            )
        )
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_time_cards),
                R.drawable.ic_home_time_card
            )
        )
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_close_out_day),
                R.drawable.ic_home_close_out_day
            )
        )
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_find_checks),
                R.drawable.ic_home_find_checks
            )
        )
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_transfer_gift_card),
                R.drawable.ic_home_transfer_gift_card
            )
        )
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_register_swipe_card),
                R.drawable.ic_home_register_swipe_card
            )
        )
        mManagerActivityList.add(
            HomeListDataModel(
                getString(R.string.header_lookup_customer),
                R.drawable.ic_home_lookup_customer
            )
        )
        managerActAdapter = HomeListAdapter(this@HomeActivity, mManagerActivityList)
        rv_manager_activities.adapter = managerActAdapter
        managerActAdapter.notifyDataSetChanged()

        // cash management
        mCashManagementList.add(
            HomeListDataModel(
                getString(R.string.header_cash_drawers),
                R.drawable.ic_home_cash_drawer
            )
        )
        mCashManagementList.add(
            HomeListDataModel(
                getString(R.string.header_pay_out),
                R.drawable.ic_home_pay_out
            )
        )
        mCashManagementList.add(
            HomeListDataModel(
                getString(R.string.header_deposits),
                R.drawable.ic_home_deposit
            )
        )
        cashManagerAdapter = HomeListAdapter(this@HomeActivity, mCashManagementList)
        rv_cash_management.adapter = cashManagerAdapter
        cashManagerAdapter.notifyDataSetChanged()

        // reports
        mReportsList.add(
            HomeListDataModel(
                getString(R.string.header_sales_reports),
                R.drawable.ic_home_sales_report
            )
        )
        mReportsList.add(
            HomeListDataModel(
                getString(R.string.header_menu_reports),
                R.drawable.ic_home_menu_report
            )
        )
        mReportsList.add(
            HomeListDataModel(
                getString(R.string.header_labor_reports),
                R.drawable.ic_home_labor_report
            )
        )
        reportsAdapter = HomeListAdapter(this@HomeActivity, mReportsList)
        rv_reports.adapter = reportsAdapter
        reportsAdapter.notifyDataSetChanged()

        // my account
        mMyAccountList.add(
            HomeListDataModel(
                getString(R.string.header_shift_review),
                R.drawable.ic_home_shift_review
            )
        )
        mMyAccountList.add(
            HomeListDataModel(
                getString(R.string.header_sales_report),
                R.drawable.ic_home_sales_report
            )
        )
        mMyAccountList.add(
            HomeListDataModel(
                getString(R.string.header_time_clock),
                R.drawable.ic_home_time_clock
            )
        )
        myAccountAdapter = HomeListAdapter(this@HomeActivity, mMyAccountList)
        rv_my_account.adapter = myAccountAdapter
        myAccountAdapter.notifyDataSetChanged()

        // support
        mSupportList.add(
            HomeListDataModel(
                getString(R.string.header_support_site),
                R.drawable.ic_home_support_site
            )
        )
        mSupportList.add(
            HomeListDataModel(
                getString(R.string.header_status_page),
                R.drawable.ic_home_status_page
            )
        )
        mSupportList.add(
            HomeListDataModel(
                getString(R.string.header_start_screen_share),
                R.drawable.ic_home_share_screen
            )
        )
        supportAdapter = HomeListAdapter(this@HomeActivity, mSupportList)
        rv_support.adapter = supportAdapter
        supportAdapter.notifyDataSetChanged()

        // setup
        mSetupList.add(HomeListDataModel(getString(R.string.header_menu), R.drawable.ic_home_menu))
        mSetupList.add(
            HomeListDataModel(
                getString(R.string.header_tables),
                R.drawable.ic_home_table
            )
        )
        mSetupList.add(
            HomeListDataModel(
                getString(R.string.header_labor),
                R.drawable.ic_home_labor
            )
        )
        mSetupList.add(
            HomeListDataModel(
                getString(R.string.header_other_setup),
                R.drawable.ic_home_other_setup
            )
        )
        mSetupList.add(
            HomeListDataModel(
                getString(R.string.header_device_setup),
                R.drawable.ic_home_device_setup
            )
        )
        mSetupList.add(
            HomeListDataModel(
                getString(R.string.header_enable_test_mode),
                R.drawable.ic_home_device_test_mode
            )
        )
        setupAdapter = HomeListAdapter(this@HomeActivity, mSetupList)
        rv_setup.adapter = setupAdapter
        setupAdapter.notifyDataSetChanged()

    }

    private fun showPayOutDialog() {
        var amount = 0.00
        mPayOutDialog = Dialog(this@HomeActivity)
        mPayOutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@HomeActivity).inflate(R.layout.pay_out_dialog, null)
        mPayOutDialog.setContentView(view)
        mPayOutDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        mPayOutDialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mPayOutDialog.setCanceledOnTouchOutside(false)
        val tv_cancel: TextView = view.findViewById(R.id.tv_cancel)
        val tv_currency_amount_header: TextView = view.findViewById(R.id.tv_currency_amount_header)
        val tv_ok: TextView = view.findViewById(R.id.tv_ok)
        val et_amount: EditText = view.findViewById(R.id.et_amount)
        val et_comment: EditText = view.findViewById(R.id.et_comment)
        val sp_cash_drawer: Spinner = view.findViewById(R.id.sp_cash_drawer)
        tv_currency_amount_header.text = currencyType
        tv_cancel.setOnClickListener {
            mPayOutDialog.dismiss()
        }
        val adapter = AllCashDrawersListAdapter(this, allCashDrawersList)
        sp_cash_drawer.adapter = adapter
        sp_cash_drawer.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = adapter.getSelectedItem(position)
                cashDrawerId = selectedItem.id.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        et_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amount = if (s!!.isNotEmpty()) {
                    s.toString().toDouble()
                } else {
                    0.00
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })
        tv_ok.setOnClickListener {
            if (cashDrawerId == "" || cashDrawerId == "empty") {
                Toast.makeText(this, "Please select a cash drawer!", Toast.LENGTH_SHORT).show()
            } else {
                val jObj = JSONObject()
                jObj.put("actionType", "pay_out")
                jObj.put("transactionType", "debit")
                jObj.put("action", "Pay Out")
                jObj.put("amount", amount)
                jObj.put("comment", et_comment.text.toString())
                jObj.put("cashDrawersId", cashDrawerId)
                jObj.put("createdBy", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("status", 1)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                addPayOut(finalObject = finalObject)
                mPayOutDialog.dismiss()
            }
        }
        mPayOutDialog.show()
    }

    private fun getAllCashDrawers(finalObject: JsonObject) {
        allCashDrawersList = ArrayList()
        val emptyCashDrawer = CashDrawersDataResponse()
        emptyCashDrawer.cashDrawerName = "Select a Cash Drawer"
        emptyCashDrawer.id = "empty"
        allCashDrawersList.add(emptyCashDrawer)
        val api = ApiInterface.create()
        val call = api.getAllCashDrawersListApi(finalObject)
        call.enqueue(object : Callback<CashDrawersListDataResponse> {
            override fun onFailure(call: Call<CashDrawersListDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashDrawersListDataResponse>,
                response: Response<CashDrawersListDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val openCashDrawersList = resp.cashdrawersOpenList!!
                        val closedCashDrawersList = resp.cashdrawersClosedList!!
                        val activeCashDrawersList = resp.cashdrawersActiveList!!
                        if (openCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(openCashDrawersList)
                        }
                        if (closedCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(closedCashDrawersList)
                        }
                        if (activeCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(activeCashDrawersList)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun addPayOut(finalObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.addPayOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@HomeActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getUnapprovedOrders(finalObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.getUnApprovalOrdersApi(finalObject)
        call.enqueue(object : Callback<NeedApprovalOrdersResponse> {
            override fun onFailure(call: Call<NeedApprovalOrdersResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<NeedApprovalOrdersResponse>,
                response: Response<NeedApprovalOrdersResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val pendingorderlist = respBody.orders!!
                        if (pendingorderlist.size > 0) {
                            if (mModeList.size > 0) {
                                for (i in 0 until mModeList.size) {
                                    if (mModeList[i].name.contains(getString(R.string.header_pending_orders))) {
                                        val dataModel = HomeListDataModel(
                                            getString(R.string.header_pending_orders) + " (${pendingorderlist.size})",
                                            R.drawable.ic_home_pending_orders
                                        )
                                        mModeList[i] = dataModel
                                    }
                                }
                            }
                            modesAdapter = HomeListAdapter(this@HomeActivity, mModeList)
                            rv_mode.adapter = modesAdapter
                            modesAdapter.notifyDataSetChanged()
                        } else {
                            if (mModeList.size > 0) {
                                for (i in 0 until mModeList.size) {
                                    if (mModeList[i].name.contains(getString(R.string.header_pending_orders))) {
                                        val dataModel = HomeListDataModel(
                                            getString(R.string.header_pending_orders),
                                            R.drawable.ic_home_pending_orders
                                        )
                                        mModeList[i] = dataModel
                                    }
                                }
                            }
                            modesAdapter = HomeListAdapter(this@HomeActivity, mModeList)
                            rv_mode.adapter = modesAdapter
                            modesAdapter.notifyDataSetChanged()
                        }
                    } else {
                        if (mModeList.size > 0) {
                            for (i in 0 until mModeList.size) {
                                if (mModeList[i].name.contains(getString(R.string.header_pending_orders))) {
                                    val dataModel = HomeListDataModel(
                                        getString(R.string.header_pending_orders),
                                        R.drawable.ic_home_pending_orders
                                    )
                                    mModeList[i] = dataModel
                                }
                            }
                        }
                        modesAdapter = HomeListAdapter(this@HomeActivity, mModeList)
                        rv_mode.adapter = modesAdapter
                        modesAdapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@HomeActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(
                            getString(R.string.ok)
                        ) { dialog, _ -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@HomeActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getRestaurantMode(finalObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.getRestaurantModeApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.updateRestaurantMode(resp.testModeStatus!!)
                    } else {
                        // default setting as false
                        sessionManager.updateRestaurantMode(false)
                    }
                    isTestModeEnabled = sessionManager.getRestaurantMode
                    if (isTestModeEnabled) {
                        tv_test_mode.visibility = View.VISIBLE
                    } else {
                        tv_test_mode.visibility = View.GONE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun enableTestMode(finalObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.enableTestModeApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.updateRestaurantMode(resp.testModeStatus!!)
                        Toast.makeText(
                            this@HomeActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        // default setting as false
                        Toast.makeText(
                            this@HomeActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        sessionManager.updateRestaurantMode(false)
                    }
                    isTestModeEnabled = sessionManager.getRestaurantMode
                    if (isTestModeEnabled) {
                        tv_test_mode.visibility = View.VISIBLE
                    } else {
                        tv_test_mode.visibility = View.GONE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        try {
            getDefaults()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        this.finishAffinity()
    }
}