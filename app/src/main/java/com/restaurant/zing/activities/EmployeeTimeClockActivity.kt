package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.EmployeeTimeCardDetailsListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class EmployeeTimeClockActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_shift_review: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var tv_time: TextView
    private lateinit var tv_employee_name: TextView
    private lateinit var tv_restaurant_name: TextView
    private lateinit var tv_employee_type: TextView
    private lateinit var rv_employee_time: RecyclerView
    private lateinit var tv_check_in_out: TextView
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var isCheckedIn = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_clock)

        initialize()
        try {
            val pattern = "hh:mm aa"
            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
            val time = sdf.format(Date())
            tv_time.text = time.toString()
            mEmployeeId = intent.getStringExtra("empId")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId)
        jObj.put("restaurantId", restaurantId)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getEmployeeTimeCards(finalObject = finalObject)

        iv_back.setOnClickListener {
            onBackPressed()
        }
        tv_check_in_out.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("employeeId", mEmployeeId)
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            Log.d("final_object", final_object.toString())
            if (isCheckedIn) {
                checkOutEmployee(finalObject = final_object)
            } else {
                checkInEmployee(finalObject = final_object)
            }
        }
        tv_employee_type.setOnClickListener {
            // TODO can update job title - future change
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        ll_shift_review = findViewById(R.id.ll_shift_review)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        tv_time = findViewById(R.id.tv_time)
        tv_employee_name = findViewById(R.id.tv_employee_name)
        tv_restaurant_name = findViewById(R.id.tv_restaurant_name)
        tv_employee_type = findViewById(R.id.tv_employee_type)
        rv_employee_time = findViewById(R.id.rv_employee_time)
        tv_check_in_out = findViewById(R.id.tv_check_in_out)

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_employee_time.layoutManager = layoutManager
        rv_employee_time.hasFixedSize()
        val dividerItemDecorationOut =
            DividerItemDecoration(rv_employee_time.context, layoutManager.orientation)
        rv_employee_time.addItemDecoration(dividerItemDecorationOut)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showDetails(employeeInOutHoursList: ArrayList<EmployeeInOutHoursDataResponse>) {
        val details_dialog = Dialog(this)
        details_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.clock_in_out_view_dialog, null)
        details_dialog.setContentView(view)
        details_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        details_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        details_dialog.setCanceledOnTouchOutside(true)
        val rv_employee_time_detail =
            view.findViewById(R.id.rv_employee_time_detail) as RecyclerView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_employee_time_detail.layoutManager = layoutManager
        rv_employee_time_detail.hasFixedSize()
        val dividerItemDecorationOut =
            DividerItemDecoration(rv_employee_time_detail.context, layoutManager.orientation)
        rv_employee_time_detail.addItemDecoration(dividerItemDecorationOut)
        val adapter =
            EmployeeTimeCardDetailsListAdapter(
                this@EmployeeTimeClockActivity,
                employeeInOutHoursList
            )
        rv_employee_time_detail.adapter = adapter
        adapter.notifyDataSetChanged()
        tv_cancel.setOnClickListener {
            details_dialog.dismiss()
        }
        details_dialog.show()
    }

    private fun getEmployeeTimeCards(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getEmployeeTimeCardsApi(finalObject)
        call.enqueue(object : Callback<EmployeeTimeCardsListDataResponse> {
            override fun onFailure(call: Call<EmployeeTimeCardsListDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@EmployeeTimeClockActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<EmployeeTimeCardsListDataResponse>,
                response: Response<EmployeeTimeCardsListDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        tv_employee_name.text = resp.employeeName.toString()
                        tv_restaurant_name.text = resp.restaurantName.toString()
                        tv_employee_type.text = resp.jobTitle.toString()
                        mEmpJobId = resp.jobId.toString()
                        isCheckedIn = resp.checkInStatus!!
                        if (isCheckedIn) {
                            tv_check_in_out.text = getString(R.string.check_out)
                        } else {
                            tv_check_in_out.text = getString(R.string.check_in)
                        }
                        val timeList = resp.employeeData
                        if (timeList!!.size > 0) {
                            val adapter =
                                EmployeeTimeCardListAdapter(
                                    this@EmployeeTimeClockActivity,
                                    timeList
                                )
                            rv_employee_time.adapter = adapter
                            adapter.notifyDataSetChanged()
                        } else {
                            val adapter =
                                EmployeeTimeCardListAdapter(
                                    this@EmployeeTimeClockActivity,
                                    timeList
                                )
                            rv_employee_time.adapter = adapter
                            adapter.notifyDataSetChanged()
                        }
                    } else {
                        val adapter =
                            EmployeeTimeCardListAdapter(
                                this@EmployeeTimeClockActivity,
                                ArrayList()
                            )
                        rv_employee_time.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun checkInEmployee(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.employeeCheckInApi(finalObject)
        call.enqueue(object : Callback<EmployeeCheckInResponse> {
            override fun onFailure(call: Call<EmployeeCheckInResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@EmployeeTimeClockActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<EmployeeCheckInResponse>,
                response: Response<EmployeeCheckInResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val jObj = JSONObject()
                        jObj.put("employeeId", mEmployeeId)
                        jObj.put("restaurantId", restaurantId)
                        val final_bject = JsonParser.parseString(jObj.toString()).asJsonObject
                        getEmployeeTimeCards(finalObject = final_bject)
                    } else {
                        Toast.makeText(
                            this@EmployeeTimeClockActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun checkOutEmployee(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.employeeCheckOutApi(finalObject)
        call.enqueue(object : Callback<EmployeeCheckOutResponse> {
            override fun onFailure(call: Call<EmployeeCheckOutResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@EmployeeTimeClockActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<EmployeeCheckOutResponse>,
                response: Response<EmployeeCheckOutResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val jObj = JSONObject()
                        jObj.put("employeeId", mEmployeeId)
                        jObj.put("restaurantId", restaurantId)
                        val final_bject = JsonParser.parseString(jObj.toString()).asJsonObject
                        getEmployeeTimeCards(finalObject = final_bject)
                    } else {
                        Toast.makeText(
                            this@EmployeeTimeClockActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@EmployeeTimeClockActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent =
                            Intent(this@EmployeeTimeClockActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@EmployeeTimeClockActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@EmployeeTimeClockActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class EmployeeTimeCardListAdapter(
        context: Context,
        list: ArrayList<EmployeeInOutHoursResponse>
    ) :
        RecyclerView.Adapter<EmployeeTimeCardListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<EmployeeInOutHoursResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.employee_clock_in_out_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): EmployeeInOutHoursResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            try {
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val str_intime = mList!![position].checkInTime.toString()
                val str_outtime = mList!![position].checkOutTime.toString()
                val str_date = mList!![position].day_for_sorting.toString()
                val intime_str = sdf.parse(str_intime)
                val date_str = sdf.parse(str_date)
                val time_sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date_sdf = SimpleDateFormat("MM/dd/yy", Locale.getDefault())
                val inTime = time_sdf.format(intime_str!!)
                val outTime: String
                if (str_outtime == "null") {
                    outTime = "---"
                } else {
                    val outtime_str = sdf.parse(str_outtime)
                    outTime = time_sdf.format(outtime_str!!)
                }
                val date = date_sdf.format(date_str!!)
                holder.tv_item_date.text = date
                holder.tv_item_job_title.text = mList!![position].jobTitle.toString()
                holder.tv_item_time_in.text = inTime
                holder.tv_item_time_out.text = outTime
                holder.tv_item_hours.text = mList!![position].hours.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            holder.ll_header.setOnClickListener {
                showDetails(mList!![position].emp_checkinout_data!!)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_item_date: TextView = view.findViewById(R.id.tv_item_date)
            var tv_item_job_title: TextView = view.findViewById(R.id.tv_item_job_title)
            var tv_item_time_in: TextView = view.findViewById(R.id.tv_item_time_in)
            var tv_item_time_out: TextView = view.findViewById(R.id.tv_item_time_out)
            var tv_item_hours: TextView = view.findViewById(R.id.tv_item_hours)
            var ll_header: LinearLayout = view.findViewById(R.id.ll_header)
        }
    }

}