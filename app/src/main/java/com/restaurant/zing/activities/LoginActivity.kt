package com.restaurant.zing.activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.helpers.*
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    private lateinit var ll_passcode: LinearLayout
    private lateinit var ll_password: LinearLayout
    private lateinit var iv_passcode: ImageView
    private lateinit var iv_password: ImageView
    private lateinit var tv_passcode: TextView
    private lateinit var tv_password: TextView
    private lateinit var et_passcode: EditText
    private lateinit var ll_passcode_details: LinearLayout
    private lateinit var ll_password_details: LinearLayout
    private lateinit var et_username: EditText
    private lateinit var et_password: EditText
    private lateinit var cv_submit: CardView
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var pinView: PinView

    private lateinit var ll_one: LinearLayout
    private lateinit var ll_two: LinearLayout
    private lateinit var ll_three: LinearLayout
    private lateinit var ll_four: LinearLayout
    private lateinit var ll_five: LinearLayout
    private lateinit var ll_six: LinearLayout
    private lateinit var ll_seven: LinearLayout
    private lateinit var ll_eight: LinearLayout
    private lateinit var ll_nine: LinearLayout
    private lateinit var ll_zero: LinearLayout
    private lateinit var ll_delete: LinearLayout

    private var passcode = ""
    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private val MAX_CHAR = 6
    private var isLoggedIn = false

    private val device_type_stg = "Android"
    private var deviceToken = ""
    private lateinit var handSetModelInfo: HandSetModelInfo
    private var deviceName = ""
    private var restaurantId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initialize()
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            deviceToken = it!!.toString()
            Log.d("token", deviceToken)
        }
        isLoggedIn = sessionManager.isLoggedIn
        if (isLoggedIn) {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
        pinView.setPasswordHidden(true)
        pinView.setAnimationEnable(true)
        // default set as pincode
        et_passcode.setText("")
        pinView.setText("")
        et_username.setText("")
        et_password.setText("")
        ll_passcode.background = ContextCompat.getDrawable(this, R.drawable.gradient_bg_1)
        iv_passcode.setImageResource(R.drawable.ic_passcode_selected)
        tv_passcode.setTextColor(ContextCompat.getColor(this, R.color.white))
        ll_password.background = ContextCompat.getDrawable(this, R.drawable.white_background)
        iv_password.setImageResource(R.drawable.ic_password_default)
        tv_password.setTextColor(ContextCompat.getColor(this, R.color.login_text_default))
        ll_passcode_details.visibility = View.VISIBLE
        ll_password_details.visibility = View.GONE
        ll_passcode.setOnClickListener {
            passcode = ""
            hideKeyboard()
            et_passcode.setText("")
            pinView.setText("")
            et_username.setText("")
            et_password.setText("")
            ll_passcode.background = ContextCompat.getDrawable(this, R.drawable.gradient_bg_1)
            iv_passcode.setImageResource(R.drawable.ic_passcode_selected)
            tv_passcode.setTextColor(ContextCompat.getColor(this, R.color.white))
            ll_password.background = ContextCompat.getDrawable(this, R.drawable.white_background)
            iv_password.setImageResource(R.drawable.ic_password_default)
            tv_password.setTextColor(ContextCompat.getColor(this, R.color.login_text_default))
            ll_passcode_details.visibility = View.VISIBLE
            ll_password_details.visibility = View.GONE
        }
        ll_password.setOnClickListener {
            passcode = ""
            hideKeyboard()
            et_passcode.setText("")
            pinView.setText("")
            et_username.setText("")
            et_password.setText("")
            ll_passcode.background = ContextCompat.getDrawable(this, R.drawable.white_background)
            iv_passcode.setImageResource(R.drawable.ic_passcode_default)
            tv_passcode.setTextColor(ContextCompat.getColor(this, R.color.login_text_default))
            ll_password.background = ContextCompat.getDrawable(this, R.drawable.gradient_bg_1)
            iv_password.setImageResource(R.drawable.ic_password_selected)
            tv_password.setTextColor(ContextCompat.getColor(this, R.color.white))
            ll_passcode_details.visibility = View.GONE
            ll_password_details.visibility = View.VISIBLE
        }
        pinView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    if (passcode.length > MAX_CHAR) {
                        passcode = passcode.substring(0, MAX_CHAR)
                    } else if (s.length == MAX_CHAR) {
                        val jObj = JSONObject()
                        jObj.put("restaurantId", restaurantId)
                        jObj.put("passcode", passcode)
                        jObj.put("deviceType", device_type_stg)
                        jObj.put("deviceName", deviceName)
                        jObj.put("deviceToken", deviceToken)
                        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                        Log.d("final_object", finalObject.toString())
                        loginUsingPasscode(finalObject = finalObject)
                    }
                }
            }
        })
        cv_submit.setOnClickListener {
            when {
                et_username.text.toString() == "" -> {
                    Toast.makeText(this, "User Name is required!", Toast.LENGTH_SHORT).show()
                }
                et_password.text.toString() == "" -> {
                    Toast.makeText(this, "Password is required!", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    val jObj = JSONObject()
                    jObj.put("email", et_username.text.toString().trim())
                    jObj.put("password", et_password.text.toString().trim())
                    jObj.put("restaurantId", restaurantId)
                    jObj.put("deviceType", device_type_stg)
                    jObj.put("deviceName", deviceName)
                    jObj.put("deviceToken", deviceToken)
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.d("final_object", finalObject.toString())
                    loginUsingPassword(finalObject = finalObject)
                }
            }

        }
        ll_one.setOnClickListener {
            if (passcode == "") {
                passcode = str_one
            } else {
                passcode += str_one
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_two.setOnClickListener {
            if (passcode == "") {
                passcode = str_two
            } else {
                passcode += str_two
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_three.setOnClickListener {
            if (passcode == "") {
                passcode = str_three
            } else {
                passcode += str_three
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_four.setOnClickListener {
            if (passcode == "") {
                passcode = str_four
            } else {
                passcode += str_four
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_five.setOnClickListener {
            if (passcode == "") {
                passcode = str_five
            } else {
                passcode += str_five
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_six.setOnClickListener {
            if (passcode == "") {
                passcode = str_six
            } else {
                passcode += str_six
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_seven.setOnClickListener {
            if (passcode == "") {
                passcode = str_seven
            } else {
                passcode += str_seven
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_eight.setOnClickListener {
            if (passcode == "") {
                passcode = str_eight
            } else {
                passcode += str_eight
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_nine.setOnClickListener {
            if (passcode == "") {
                passcode = str_nine
            } else {
                passcode += str_nine
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_zero.setOnClickListener {
            if (passcode == "") {
                passcode = str_zero
            } else {
                passcode += str_zero
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
        ll_delete.setOnClickListener {
            if (passcode != "") {
                passcode = passcode.dropLast(1)
                if (passcode == "") {
                    passcode = ""
                }
            } else {
                passcode = ""
            }
            et_passcode.setText(passcode)
            pinView.setText(passcode)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        restaurantDetails = sessionManager.getRestaurantDetails
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        ll_passcode = findViewById(R.id.ll_passcode)
        ll_password = findViewById(R.id.ll_password)
        tv_passcode = findViewById(R.id.tv_passcode)
        tv_password = findViewById(R.id.tv_password)
        iv_passcode = findViewById(R.id.iv_passcode)
        iv_password = findViewById(R.id.iv_password)
        et_passcode = findViewById(R.id.et_passcode)
        ll_passcode_details = findViewById(R.id.ll_passcode_details)
        ll_password_details = findViewById(R.id.ll_password_details)
        et_username = findViewById(R.id.et_username)
        et_password = findViewById(R.id.et_password)
        cv_submit = findViewById(R.id.cv_submit)
        pinView = findViewById(R.id.pinView)

        ll_one = findViewById(R.id.ll_one)
        ll_two = findViewById(R.id.ll_two)
        ll_three = findViewById(R.id.ll_three)
        ll_four = findViewById(R.id.ll_four)
        ll_five = findViewById(R.id.ll_five)
        ll_six = findViewById(R.id.ll_six)
        ll_seven = findViewById(R.id.ll_seven)
        ll_eight = findViewById(R.id.ll_eight)
        ll_nine = findViewById(R.id.ll_nine)
        ll_zero = findViewById(R.id.ll_zero)
        ll_delete = findViewById(R.id.ll_delete)

        et_passcode.transformationMethod = AsteriskPasswordTransformationMethod()
        handSetModelInfo = HandSetModelInfo()
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun loginUsingPasscode(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.loginWithPasscodeApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@LoginActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val userDetails = resp.userDetails!!
                        val userPermissions = userDetails.permissionList!!
                        val accountAdminAccess = userPermissions.accountAdminAccess!!
                        val deliveryAccess = userPermissions.deliveryAccess!!
                        val deviceSetupAccess = userPermissions.deviceSetupAccess!!
                        val managerAccess = userPermissions.managerAccess!!
                        val posAccess = userPermissions.posAccess!!
                        val quickEditAccess = userPermissions.quickEditAccess!!
                        val restaurantAdminAccess = userPermissions.restaurantAdminAccess!!
                        val webSetupAccess = userPermissions.webSetupAccess!!
                        sessionManager.loginDetails(
                            userDetails.id.toString(),
                            userDetails.empId.toString(),
                            userDetails.firstName.toString(),
                            userDetails.lastName.toString(),
                            userDetails.jobId.toString(),
                            userDetails.profilePic.toString()
                        )
                        sessionManager.accountAdminPermissions(
                            accountAdminAccess.dataExportConfig!!,
                            accountAdminAccess.financialAccounts!!,
                            accountAdminAccess.manageIntegrations!!,
                            accountAdminAccess.userPermissions!!
                        )
                        sessionManager.deliveryAccessPermissions(
                            deliveryAccess.cancelDispatch!!,
                            deliveryAccess.completeDelivery!!,
                            deliveryAccess.deliveryMode!!,
                            deliveryAccess.dispatchDriver!!,
                            deliveryAccess.updateAllDeliveryOrders!!,
                            deliveryAccess.updateDriver!!
                        )
                        sessionManager.deviceSetupAccessPermissions(
                            deviceSetupAccess.advancedTerminalSetup!!,
                            deviceSetupAccess.kdsAndOrderScreenSetup!!,
                            deviceSetupAccess.terminalSetup!!
                        )
                        sessionManager.managerAccessPermissions(
                            managerAccess.adjustCashDrawerStartBalance!!,
                            managerAccess.bulkClosePaidChecks!!,
                            managerAccess.bulkTransferCheck!!,
                            managerAccess.bulkVoidOpenChecks!!,
                            managerAccess.cashDrawerLockdown!!,
                            managerAccess.cashDrawersBlind!!,
                            managerAccess.cashDrawersFull!!,
                            managerAccess.closeOutDay!!,
                            managerAccess.discounts!!,
                            managerAccess.editSentItems!!,
                            managerAccess.editTimeEntries!!,
                            managerAccess.endBreaksEarly!!,
                            managerAccess.findChecks!!,
                            managerAccess.giftCardAdjustment!!,
                            managerAccess.largeCashOverOrUnder!!,
                            managerAccess.logBook!!,
                            managerAccess.negativeDeclaredTips!!,
                            managerAccess.openItems!!,
                            managerAccess.otherPaymentTypes!!,
                            managerAccess.payout!!,
                            managerAccess.registerSwipeCards!!,
                            managerAccess.sendNotifications!!,
                            managerAccess.shiftReview!!,
                            managerAccess.taxExempt!!,
                            managerAccess.throttleOnlineOrders!!,
                            managerAccess.transferOrRewardsAdjustment!!,
                            managerAccess.unlinkedRefunds!!,
                            managerAccess.voidItemOrOrders!!,
                            managerAccess.voidOrRefundPayments!!
                        )
                        sessionManager.posAccessPermissions(
                            posAccess.addOrUpdateServiceCharges!!,
                            posAccess.applyCashPayments!!,
                            posAccess.cashDrawerAccess!!,
                            posAccess.changeServer!!,
                            posAccess.changeTable!!,
                            posAccess.editOtherEmployeesOrders!!,
                            posAccess.keyInCreditCards!!,
                            posAccess.kitchenDisplayScreenMode!!,
                            posAccess.myReports!!,
                            posAccess.noSale!!,
                            posAccess.offlineOrBackgroundCreditCardProcessing!!,
                            posAccess.pandingOrdersMode!!,
                            posAccess.paymentTerminalMode!!,
                            posAccess.quickOrderMode!!,
                            posAccess.shiftReviewSalesData!!,
                            posAccess.tableServiceMode!!,
                            posAccess.viewOtherEmployeesOrders!!
                        )
                        sessionManager.quickEditAccessPermissions(
                            quickEditAccess.addExistingItemsOrMods!!,
                            quickEditAccess.addNewItemsMods!!,
                            quickEditAccess.buttonColor!!,
                            quickEditAccess.fullQuickEdit!!,
                            quickEditAccess.inventoryAndQuantity!!,
                            quickEditAccess.name!!,
                            quickEditAccess.posName!!,
                            quickEditAccess.price!!,
                            quickEditAccess.rearrangingItemsMods!!,
                            quickEditAccess.removeItemsOrMods!!,
                            quickEditAccess.sku!!
                        )
                        sessionManager.restaurantAdminAccessPermissions(
                            restaurantAdminAccess.customersCreditsAndReports!!,
                            restaurantAdminAccess.editFullMenu!!,
                            restaurantAdminAccess.editHistoricalData!!,
                            restaurantAdminAccess.employeeInfo!!,
                            restaurantAdminAccess.employeeJobsAndWages!!,
                            restaurantAdminAccess.giftOrRewardsCardReport!!,
                            restaurantAdminAccess.houseAccounts!!,
                            restaurantAdminAccess.laborReports!!,
                            restaurantAdminAccess.localMenuEdit!!,
                            restaurantAdminAccess.marketingInfo!!,
                            restaurantAdminAccess.menuReports!!,
                            restaurantAdminAccess.salesReports!!,
                            restaurantAdminAccess.tables!!
                        )
                        sessionManager.webSetupAccessPermissions(
                            webSetupAccess.dataExportConfig!!,
                            webSetupAccess.discountsSetup!!,
                            webSetupAccess.financialAccounts!!,
                            webSetupAccess.kitchenOrDiningRoomSetup!!,
                            webSetupAccess.manageInstructions!!,
                            webSetupAccess.paymentsSetup!!,
                            webSetupAccess.publishing!!,
                            webSetupAccess.restaurantGroupsSetup!!,
                            webSetupAccess.restaurantOperationsSetup!!,
                            webSetupAccess.taxRatesSetup!!,
                            webSetupAccess.userPermissions!!
                        )
                        if (userDetails.currencySymbol.toString() == "null" || userDetails.currencySymbol.toString() == "") {
                            sessionManager.saveCurrencyType("$")
                        } else {
                            sessionManager.saveCurrencyType(userDetails.currencySymbol.toString())
                        }
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loginUsingPassword(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.loginWithPasswordApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@LoginActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val userDetails = resp.userDetails!!
                        val userPermissions = userDetails.permissionList!!
                        val accountAdminAccess = userPermissions.accountAdminAccess!!
                        val deliveryAccess = userPermissions.deliveryAccess!!
                        val deviceSetupAccess = userPermissions.deviceSetupAccess!!
                        val managerAccess = userPermissions.managerAccess!!
                        val posAccess = userPermissions.posAccess!!
                        val quickEditAccess = userPermissions.quickEditAccess!!
                        val restaurantAdminAccess = userPermissions.restaurantAdminAccess!!
                        val webSetupAccess = userPermissions.webSetupAccess!!
                        sessionManager.loginDetails(
                            userDetails.id.toString(),
                            userDetails.empId.toString(),
                            userDetails.firstName.toString(),
                            userDetails.lastName.toString(),
                            userDetails.jobId.toString(),
                            userDetails.profilePic.toString()
                        )
                        sessionManager.accountAdminPermissions(
                            accountAdminAccess.dataExportConfig!!,
                            accountAdminAccess.financialAccounts!!,
                            accountAdminAccess.manageIntegrations!!,
                            accountAdminAccess.userPermissions!!
                        )
                        sessionManager.deliveryAccessPermissions(
                            deliveryAccess.cancelDispatch!!,
                            deliveryAccess.completeDelivery!!,
                            deliveryAccess.deliveryMode!!,
                            deliveryAccess.dispatchDriver!!,
                            deliveryAccess.updateAllDeliveryOrders!!,
                            deliveryAccess.updateDriver!!
                        )
                        sessionManager.deviceSetupAccessPermissions(
                            deviceSetupAccess.advancedTerminalSetup!!,
                            deviceSetupAccess.kdsAndOrderScreenSetup!!,
                            deviceSetupAccess.terminalSetup!!
                        )
                        sessionManager.managerAccessPermissions(
                            managerAccess.adjustCashDrawerStartBalance!!,
                            managerAccess.bulkClosePaidChecks!!,
                            managerAccess.bulkTransferCheck!!,
                            managerAccess.bulkVoidOpenChecks!!,
                            managerAccess.cashDrawerLockdown!!,
                            managerAccess.cashDrawersBlind!!,
                            managerAccess.cashDrawersFull!!,
                            managerAccess.closeOutDay!!,
                            managerAccess.discounts!!,
                            managerAccess.editSentItems!!,
                            managerAccess.editTimeEntries!!,
                            managerAccess.endBreaksEarly!!,
                            managerAccess.findChecks!!,
                            managerAccess.giftCardAdjustment!!,
                            managerAccess.largeCashOverOrUnder!!,
                            managerAccess.logBook!!,
                            managerAccess.negativeDeclaredTips!!,
                            managerAccess.openItems!!,
                            managerAccess.otherPaymentTypes!!,
                            managerAccess.payout!!,
                            managerAccess.registerSwipeCards!!,
                            managerAccess.sendNotifications!!,
                            managerAccess.shiftReview!!,
                            managerAccess.taxExempt!!,
                            managerAccess.throttleOnlineOrders!!,
                            managerAccess.transferOrRewardsAdjustment!!,
                            managerAccess.unlinkedRefunds!!,
                            managerAccess.voidItemOrOrders!!,
                            managerAccess.voidOrRefundPayments!!
                        )
                        sessionManager.posAccessPermissions(
                            posAccess.addOrUpdateServiceCharges!!,
                            posAccess.applyCashPayments!!,
                            posAccess.cashDrawerAccess!!,
                            posAccess.changeServer!!,
                            posAccess.changeTable!!,
                            posAccess.editOtherEmployeesOrders!!,
                            posAccess.keyInCreditCards!!,
                            posAccess.kitchenDisplayScreenMode!!,
                            posAccess.myReports!!,
                            posAccess.noSale!!,
                            posAccess.offlineOrBackgroundCreditCardProcessing!!,
                            posAccess.pandingOrdersMode!!,
                            posAccess.paymentTerminalMode!!,
                            posAccess.quickOrderMode!!,
                            posAccess.shiftReviewSalesData!!,
                            posAccess.tableServiceMode!!,
                            posAccess.viewOtherEmployeesOrders!!
                        )
                        sessionManager.quickEditAccessPermissions(
                            quickEditAccess.addExistingItemsOrMods!!,
                            quickEditAccess.addNewItemsMods!!,
                            quickEditAccess.buttonColor!!,
                            quickEditAccess.fullQuickEdit!!,
                            quickEditAccess.inventoryAndQuantity!!,
                            quickEditAccess.name!!,
                            quickEditAccess.posName!!,
                            quickEditAccess.price!!,
                            quickEditAccess.rearrangingItemsMods!!,
                            quickEditAccess.removeItemsOrMods!!,
                            quickEditAccess.sku!!
                        )
                        sessionManager.restaurantAdminAccessPermissions(
                            restaurantAdminAccess.customersCreditsAndReports!!,
                            restaurantAdminAccess.editFullMenu!!,
                            restaurantAdminAccess.editHistoricalData!!,
                            restaurantAdminAccess.employeeInfo!!,
                            restaurantAdminAccess.employeeJobsAndWages!!,
                            restaurantAdminAccess.giftOrRewardsCardReport!!,
                            restaurantAdminAccess.houseAccounts!!,
                            restaurantAdminAccess.laborReports!!,
                            restaurantAdminAccess.localMenuEdit!!,
                            restaurantAdminAccess.marketingInfo!!,
                            restaurantAdminAccess.menuReports!!,
                            restaurantAdminAccess.salesReports!!,
                            restaurantAdminAccess.tables!!
                        )
                        sessionManager.webSetupAccessPermissions(
                            webSetupAccess.dataExportConfig!!,
                            webSetupAccess.discountsSetup!!,
                            webSetupAccess.financialAccounts!!,
                            webSetupAccess.kitchenOrDiningRoomSetup!!,
                            webSetupAccess.manageInstructions!!,
                            webSetupAccess.paymentsSetup!!,
                            webSetupAccess.publishing!!,
                            webSetupAccess.restaurantGroupsSetup!!,
                            webSetupAccess.restaurantOperationsSetup!!,
                            webSetupAccess.taxRatesSetup!!,
                            webSetupAccess.userPermissions!!
                        )
                        if (userDetails.currencySymbol.toString() == "null" || userDetails.currencySymbol.toString() == "") {
                            sessionManager.saveCurrencyType("$")
                        } else {
                            sessionManager.saveCurrencyType(userDetails.currencySymbol.toString())
                        }
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = this.currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}