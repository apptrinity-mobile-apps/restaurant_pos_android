package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.FindChecksOrderItemsListAdapter
import com.restaurant.zing.adapters.FindChecksPaymentsListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class FindChecksDetailedViewActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var orderDetails: OrderDetailsResponse
    private lateinit var itemsList: ArrayList<OrderItemsResponse>
    private lateinit var paymentsList: ArrayList<TransactionsReceiptResponse>
    private lateinit var rv_order_items: RecyclerView
    private lateinit var rv_order_payments: RecyclerView
    private lateinit var tv_check_no_main: TextView
    private lateinit var tv_send_to_devices: TextView
    private lateinit var tv_re_open_check: TextView
    private lateinit var tv_revenue_edit: TextView
    private lateinit var tv_created_by: TextView
    private lateinit var tv_check_id_main: TextView
    private lateinit var tv_guests_count: TextView
    private lateinit var tv_checks_count: TextView
    private lateinit var tv_revenue_name: TextView
    private lateinit var tv_source: TextView
    private lateinit var tv_origin_device: TextView
    private lateinit var tv_recent_device: TextView
    private lateinit var tv_order_no: TextView
    private lateinit var tv_check_id: TextView
    private lateinit var tv_open_time: TextView
    private lateinit var tv_server_name: TextView
    private lateinit var tv_opened_server_name: TextView
    private lateinit var tv_table_no: TextView
    private lateinit var tv_discount: TextView
    private lateinit var tv_credits: TextView
    private lateinit var tv_sub_total: TextView
    private lateinit var tv_tax: TextView
    private lateinit var tv_total: TextView
    private lateinit var tv_balance_due: TextView
    private lateinit var tv_tip_amount: TextView
    private lateinit var tv_currency_total: TextView
    private lateinit var tv_currency_balance_due: TextView
    private lateinit var tv_currency_sub_total: TextView
    private lateinit var tv_currency_tax: TextView
    private lateinit var tv_currency_tip_amount: TextView
    private lateinit var tv_currency_credits: TextView
    private lateinit var tv_currency_discount: TextView

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var mOrderId = ""
    private var currenryType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_checks_detailed_view)

        try {
            mOrderId = intent.getStringExtra("mOrderId")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()
        tv_currency_total.text = currenryType
        tv_currency_balance_due.text = currenryType
        tv_currency_sub_total.text = currenryType
        tv_currency_tax.text = currenryType
        tv_currency_tip_amount.text = currenryType
        tv_currency_credits.text = currenryType
        tv_currency_discount.text = currenryType
        try {
            val jObject = JSONObject()
            jObject.put("userId", mEmployeeId)
            jObject.put("restaurantId", restaurantId)
            jObject.put("orderId", mOrderId)
            val finalObject = JsonParser.parseString(jObject.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            getOrderDetails(finalObject = finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("restaurantId", restaurantId)
            jObj.put("userId", mEmployeeId)
            jObj.put("jobId", mEmpJobId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            employeeLogout(finalObject)
        }
        tv_send_to_devices.setOnClickListener { }
        tv_re_open_check.setOnClickListener { }
        tv_revenue_edit.setOnClickListener { }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currenryType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        rv_order_payments = findViewById(R.id.rv_order_payments)
        rv_order_items = findViewById(R.id.rv_order_items)
        tv_check_no_main = findViewById(R.id.tv_check_no_main)
        tv_send_to_devices = findViewById(R.id.tv_send_to_devices)
        tv_re_open_check = findViewById(R.id.tv_re_open_check)
        tv_revenue_edit = findViewById(R.id.tv_revenue_edit)
        tv_created_by = findViewById(R.id.tv_created_by)
        tv_check_id_main = findViewById(R.id.tv_check_id_main)
        tv_guests_count = findViewById(R.id.tv_guests_count)
        tv_checks_count = findViewById(R.id.tv_checks_count)
        tv_revenue_name = findViewById(R.id.tv_revenue_name)
        tv_source = findViewById(R.id.tv_source)
        tv_origin_device = findViewById(R.id.tv_origin_device)
        tv_recent_device = findViewById(R.id.tv_recent_device)
        tv_order_no = findViewById(R.id.tv_order_no)
        tv_check_id = findViewById(R.id.tv_check_id)
        tv_open_time = findViewById(R.id.tv_open_time)
        tv_server_name = findViewById(R.id.tv_server_name)
        tv_opened_server_name = findViewById(R.id.tv_opened_server_name)
        tv_table_no = findViewById(R.id.tv_table_no)
        tv_discount = findViewById(R.id.tv_discount)
        tv_credits = findViewById(R.id.tv_credits)
        tv_sub_total = findViewById(R.id.tv_sub_total)
        tv_tax = findViewById(R.id.tv_tax)
        tv_total = findViewById(R.id.tv_total)
        tv_balance_due = findViewById(R.id.tv_balance_due)
        tv_tip_amount = findViewById(R.id.tv_tip_amount)
        tv_currency_total = findViewById(R.id.tv_currency_total)
        tv_currency_balance_due = findViewById(R.id.tv_currency_balance_due)
        tv_currency_sub_total = findViewById(R.id.tv_currency_sub_total)
        tv_currency_tax = findViewById(R.id.tv_currency_tax)
        tv_currency_tip_amount = findViewById(R.id.tv_currency_tip_amount)
        tv_currency_credits = findViewById(R.id.tv_currency_credits)
        tv_currency_discount = findViewById(R.id.tv_currency_discount)
        val itemsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val paymentsLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_order_payments.layoutManager = paymentsLayoutManager
        rv_order_items.layoutManager = itemsLayoutManager
        rv_order_items.hasFixedSize()
        rv_order_payments.hasFixedSize()
        itemsList = ArrayList()
        paymentsList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@FindChecksDetailedViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent =
                            Intent(this@FindChecksDetailedViewActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@FindChecksDetailedViewActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getOrderDetails(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewOrderApi(finalObject)
        call.enqueue(object : Callback<ViewOrderDataResponse> {
            override fun onFailure(call: Call<ViewOrderDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@FindChecksDetailedViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ViewOrderDataResponse>,
                response: Response<ViewOrderDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        orderDetails = respBody.orderDetails!!
                        itemsList.addAll(orderDetails.orderItems!!)
                        paymentsList.addAll(orderDetails.transactionsList!!)
                        if (orderDetails.status == 3) {
                            tv_check_no_main.text =
                                "${getString(R.string.order)}#${orderDetails.orderNumber} (${getString(
                                    R.string.closed
                                )})"
                            tv_order_no.text =
                                "${getString(R.string.order)}#${orderDetails.orderNumber} (${getString(
                                    R.string.closed
                                )})"
                        } else {
                            tv_check_no_main.text =
                                "${getString(R.string.order)}#${orderDetails.orderNumber}"
                            tv_order_no.text =
                                "${getString(R.string.order)}#${orderDetails.orderNumber}"
                        }
                        tv_created_by.text = orderDetails.createdByName.toString()
                        tv_check_id_main.text = orderDetails.checkUniqueId.toString()
                        if (orderDetails.guestCount.toString() == "null") {
                            tv_guests_count.text = ""
                        } else {
                            tv_guests_count.text = orderDetails.guestCount.toString()
                        }
                        tv_checks_count.text = orderDetails.noOfChecks.toString()
                        tv_revenue_name.text = orderDetails.revenueCenterName.toString()
                        tv_source.text = orderDetails.source.toString()
                        tv_origin_device.text = orderDetails.originDevice.toString()
                        tv_recent_device.text = orderDetails.recentDevice.toString()
                        tv_check_id.text = orderDetails.checkUniqueId.toString()
                        tv_server_name.text = orderDetails.createdByName.toString()
                        tv_opened_server_name.text = orderDetails.createdByName.toString()
                        tv_table_no.text = orderDetails.tableNumber.toString()
                        tv_discount.text = "%.2f".format(orderDetails.discountAmount)
                        tv_credits.text = "%.2f".format(orderDetails.creditsUsed)
                        tv_sub_total.text = "%.2f".format(orderDetails.subTotal)
                        tv_tax.text = "%.2f".format(orderDetails.taxAmount)
                        tv_total.text = "%.2f".format(orderDetails.totalAmount)
                        tv_balance_due.text = "%.2f".format(orderDetails.dueAmount)
                        tv_tip_amount.text = "%.2f".format(orderDetails.tipAmount)
                        try {
                            val full_date = orderDetails.createdOn.toString()
                            val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                            val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                            val date = sdf.parse(full_date)
                            val sdf1 = SimpleDateFormat("MM-dd-yyyy hh:mm:ss", Locale.getDefault())
                            val date_str = sdf1.format(date!!)
                            tv_open_time.text = date_str
                        } catch (e: Exception) {
                            tv_open_time.text = ""
                            e.printStackTrace()
                        }
                    } else {
                        tv_check_no_main.text = "${getString(R.string.order)}#"
                        tv_created_by.text = ""
                        tv_check_id_main.text = ""
                        tv_guests_count.text = ""
                        tv_checks_count.text = ""
                        tv_revenue_name.text = ""
                        tv_source.text = ""
                        tv_origin_device.text = ""
                        tv_recent_device.text = ""
                        tv_order_no.text = "${getString(R.string.order)}#"
                        tv_check_id.text = ""
                        tv_open_time.text = ""
                        tv_server_name.text = ""
                        tv_opened_server_name.text = ""
                        tv_table_no.text = ""
                        tv_discount.text = ""
                        tv_credits.text = ""
                        tv_sub_total.text = ""
                        tv_tax.text = ""
                        tv_total.text = ""
                        tv_balance_due.text = ""
                        tv_tip_amount.text = ""
                        Toast.makeText(
                            this@FindChecksDetailedViewActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val itemsAdapter =
                        FindChecksOrderItemsListAdapter(
                            this@FindChecksDetailedViewActivity,
                            itemsList
                        )
                    val paymentsListAdapter =
                        FindChecksPaymentsListAdapter(
                            this@FindChecksDetailedViewActivity,
                            paymentsList
                        )
                    rv_order_items.adapter = itemsAdapter
                    rv_order_payments.adapter = paymentsListAdapter
                    itemsAdapter.notifyDataSetChanged()
                    paymentsListAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}