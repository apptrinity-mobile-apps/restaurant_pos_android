package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.CenterToast
import com.restaurant.zing.helpers.HandSetModelInfo
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestaurantOtpActivity : AppCompatActivity() {

    private lateinit var loading_dialog: Dialog
    private lateinit var et_otp: EditText
    private lateinit var ll_otp: LinearLayout
    private lateinit var et_device_name: EditText
    private lateinit var ll_device_name: LinearLayout
    private lateinit var ll_dinning_options: LinearLayout
    private lateinit var ll_service_options: LinearLayout
    private lateinit var ll_prep_stations_options: LinearLayout
    private lateinit var ll_cash_drawers: LinearLayout
    private lateinit var tv_submit: TextView
    private lateinit var tv_header: TextView
    private lateinit var tv_device_name_status: TextView
    private lateinit var sessionManager: SessionManager
    private lateinit var rv_dining_options: RecyclerView
    private lateinit var rv_service_area_options: RecyclerView
    private lateinit var rv_prep_stations_options: RecyclerView
    private lateinit var rv_cash_drawers: RecyclerView
    private lateinit var allCashDrawersList: ArrayList<CashDrawersDataResponse>
    private var restaurantId = ""
    private var restaurantName = ""
    private var dinningOptionBehaviour = ""
    private var dinningName = ""
    private var dinningId = ""
    private var otp = ""
    private var isOtpMatched = false
    private var isDeviceNameUnique = false
    private var serviceID = ""
    private var serviceName = ""
    private var serviceChargeId = ""
    private var revenueCenter = ""
    private var revenueCenterName = ""
    private var serviceAreaPrimary = false
    private var diningSelected = false
    private var serviceSelected = false
    private var prepStationSelected = false
    private var cashDrawerSelected = false
    private var cashDrawerId = ""
    private var cashDrawerName = ""
    private var cashDrawerNumber = ""
    private lateinit var handSetModelInfo: HandSetModelInfo
    private var deviceName = ""
    private var deviceToken = ""
    private var prepStationId = ""
    private var prepStationName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_otp)
        try {
            restaurantId = intent.getStringExtra("restaurantId")!!
            restaurantName = intent.getStringExtra("restaurantName")!!
            otp = intent.getStringExtra("otp")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            deviceToken = it!!.toString()
            Log.d("token", deviceToken)
        }
        initialize()
        ll_otp.visibility = View.VISIBLE
        ll_dinning_options.visibility = View.GONE
        ll_service_options.visibility = View.GONE
        ll_prep_stations_options.visibility = View.GONE
        ll_cash_drawers.visibility = View.GONE
        val obj = JSONObject()
        obj.put("restaurantId", restaurantId)
        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
        Log.d("finalObject", jObject.toString())
        getDinningOptions(jObject)
        getServiceAreaOptions(jObject)
        getAllCashDrawers(jObject)
        getAllPrepStations(jObject)
        tv_submit.setOnClickListener {
            hideKeyboard()
            if (!isOtpMatched) {
                if (et_otp.text.toString().trim() == "") {
                    Toast.makeText(
                        this@RestaurantOtpActivity,
                        "Enter OTP sent to your e-mail!",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    if (otp != et_otp.text.toString().trim()) {
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            "Invalid OTP!",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        isOtpMatched = true
                        ll_otp.visibility = View.GONE
                        ll_device_name.visibility = View.VISIBLE
                        ll_service_options.visibility = View.GONE
                        ll_dinning_options.visibility = View.GONE
                    }
                }
            } else if (!isDeviceNameUnique) {
                tv_device_name_status.visibility = View.VISIBLE
                if (et_device_name.text.trim().toString() == "") {
                    tv_device_name_status.text = getString(R.string.name_empty)
                } else {
                    deviceName = et_device_name.text.trim().toString()
                    try {
                        val jObj = JSONObject()
                        jObj.put("restaurantId", restaurantId)
                        jObj.put("deviceName", deviceName)
                        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                        Log.d("finalObject", finalObject.toString())
                        checkDeviceName(finalObject)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.VISIBLE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.GONE
            } else if (!serviceSelected) {
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.VISIBLE
                ll_dinning_options.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.GONE
            } else if (!diningSelected) {
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.VISIBLE
                ll_prep_stations_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.GONE
            } else if (!prepStationSelected) {
                prepStationSelected = true // for optional
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.GONE
                ll_prep_stations_options.visibility = View.VISIBLE
                ll_cash_drawers.visibility = View.GONE
            } else if (!cashDrawerSelected) {
                cashDrawerSelected = true   // for optional
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.VISIBLE
            } else {
                val jObj = JSONObject()
                jObj.put("createdBy", "")
                jObj.put("restaurantId", restaurantId)
                jObj.put("dineInOption", dinningId)
                jObj.put("serviceAreaId", serviceID)
                jObj.put("cashDrawersId", cashDrawerId)
                jObj.put("deviceName", deviceName)
                jObj.put("deviceToken", deviceToken)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.d("finalObject", finalObject.toString());
                saveDeviceSetup(finalObject)
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        handSetModelInfo = HandSetModelInfo()
//        deviceName = handSetModelInfo.getDeviceName
        et_otp = findViewById(R.id.et_otp)
        ll_otp = findViewById(R.id.ll_otp)
        ll_device_name = findViewById(R.id.ll_device_name)
        et_device_name = findViewById(R.id.et_device_name)
        tv_submit = findViewById(R.id.tv_submit)
        tv_header = findViewById(R.id.tv_header)
        tv_device_name_status = findViewById(R.id.tv_device_name_status)
        ll_dinning_options = findViewById(R.id.ll_dinning_options)
        ll_service_options = findViewById(R.id.ll_service_options)
        ll_prep_stations_options = findViewById(R.id.ll_prep_stations_options)
        ll_cash_drawers = findViewById(R.id.ll_cash_drawers)
        rv_dining_options = findViewById(R.id.rv_dining_options)
        rv_service_area_options = findViewById(R.id.rv_service_area_options)
        rv_prep_stations_options = findViewById(R.id.rv_prep_stations_options)
        rv_cash_drawers = findViewById(R.id.rv_cash_drawers)
        val layoutManager =
            GridLayoutManager(this@RestaurantOtpActivity, 3, LinearLayoutManager.VERTICAL, false)
        val serviceLayoutManager =
            GridLayoutManager(this@RestaurantOtpActivity, 3, LinearLayoutManager.VERTICAL, false)
        val stationsLayoutManager =
            GridLayoutManager(this@RestaurantOtpActivity, 3, LinearLayoutManager.VERTICAL, false)
        val cashDrawerLayoutManager =
            GridLayoutManager(this@RestaurantOtpActivity, 3, LinearLayoutManager.VERTICAL, false)
        rv_dining_options.layoutManager = layoutManager
        rv_service_area_options.layoutManager = serviceLayoutManager
        rv_prep_stations_options.layoutManager = stationsLayoutManager
        rv_cash_drawers.layoutManager = cashDrawerLayoutManager
        rv_dining_options.hasFixedSize()
        rv_service_area_options.hasFixedSize()
        rv_prep_stations_options.hasFixedSize()
        rv_cash_drawers.hasFixedSize()
        allCashDrawersList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getDinningOptions(json_obj: JsonObject) {
        val dinningOptionsArray = ArrayList<GetDinningOptionsListResponseList>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDinningOptionsListApi(json_obj)
        call.enqueue(object : Callback<GetDinningOptionsListResponse> {
            override fun onFailure(call: Call<GetDinningOptionsListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<GetDinningOptionsListResponse>,
                response: Response<GetDinningOptionsListResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus!! == 1) {
                        for (i in 0 until response.body()!!.dinningOptionsList!!.size) {
                            dinningOptionsArray.add(response.body()!!.dinningOptionsList!![i])
                        }
                    } else {
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val dinningOptionAdapter =
                        DinningOptionAdapter(this@RestaurantOtpActivity, dinningOptionsArray)
                    rv_dining_options.adapter = dinningOptionAdapter
                    dinningOptionAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getServiceAreaOptions(json_obj: JsonObject) {
        val serviceAreaArray = ArrayList<ServiceAreasListResponse>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getServiceAreasApi(json_obj)
        call.enqueue(object : Callback<ServiceAreasResponse> {
            override fun onFailure(call: Call<ServiceAreasResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ServiceAreasResponse>,
                response: Response<ServiceAreasResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus!! == 1) {
                        val list = resp.service_areasList!!
                        for (i in 0 until list.size) {
                            serviceAreaArray.add(list[i])
                        }
                    } else {
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val serviceAreaOptionAdapter =
                        ServiceAreaOptionsAdapter(this@RestaurantOtpActivity, serviceAreaArray)
                    rv_service_area_options.adapter = serviceAreaOptionAdapter
                    serviceAreaOptionAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllCashDrawers(finalObject: JsonObject) {
        loading_dialog.show()
        allCashDrawersList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getAllCashDrawersListApi(finalObject)
        call.enqueue(object : Callback<CashDrawersListDataResponse> {
            override fun onFailure(call: Call<CashDrawersListDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@RestaurantOtpActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashDrawersListDataResponse>,
                response: Response<CashDrawersListDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val openCashDrawersList = resp.cashdrawersOpenList!!
                        val closedCashDrawersList = resp.cashdrawersClosedList!!
                        val activeCashDrawersList = resp.cashdrawersActiveList!!
                        if (openCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(openCashDrawersList)
                        }
                        if (closedCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(closedCashDrawersList)
                        }
                        if (activeCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(activeCashDrawersList)
                        }
                    } else {
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val cashDrawerAdapter =
                        CashDrawerAdapter(this@RestaurantOtpActivity, allCashDrawersList)
                    rv_cash_drawers.adapter = cashDrawerAdapter
                    cashDrawerAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun saveDeviceSetup(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addActiveCashDrawerApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@RestaurantOtpActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.saveRestaurantId(restaurantId, restaurantName)
                        sessionManager.saveDiningOptions(
                            dinningId,
                            dinningName,
                            dinningOptionBehaviour
                        )
                        sessionManager.saveServiceAreaOptions(
                            serviceID,
                            serviceAreaPrimary,
                            serviceName,
                            serviceChargeId,
                            revenueCenter,
                            revenueCenterName
                        )
                        sessionManager.saveCashDrawer(
                            cashDrawerId,
                            cashDrawerName,
                            cashDrawerNumber
                        )
                        sessionManager.savePrepStation(prepStationId, prepStationName)
                        sessionManager.saveDeviceDetails(deviceName, resp.deviceId!!)
                        CenterToast().showToast(this@RestaurantOtpActivity, "Saved successfully.")
                        val intent = Intent(this@RestaurantOtpActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllPrepStations(finalObject: JsonObject) {
        loading_dialog.show()
        val stationsArray = ArrayList<PreparationStationsDataResponse>()
        val api = ApiInterface.create()
        val call = api.getAllPrepStationsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@RestaurantOtpActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus!! == 1) {
                        val list = resp.stationsList!!
                        for (i in 0 until list.size) {
                            stationsArray.add(list[i])
                        }
                    } else {
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val prepStationOptionAdapter =
                        PrepStationOptionAdapter(this@RestaurantOtpActivity, stationsArray)
                    rv_prep_stations_options.adapter = prepStationOptionAdapter
                    prepStationOptionAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun checkDeviceName(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.checkDeviceNameApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@RestaurantOtpActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus!! == 1) {
                        isDeviceNameUnique = true
                    } else {
                        isDeviceNameUnique = false
                        Toast.makeText(
                            this@RestaurantOtpActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    deviceNameStatus()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class ServiceAreaOptionsAdapter(
        context: Context,
        dinningOptionArray: ArrayList<ServiceAreasListResponse>
    ) :
        RecyclerView.Adapter<ServiceAreaOptionsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<ServiceAreasListResponse>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = dinningOptionArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.dinning_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].serviceName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                serviceSelected = true
                serviceID = mList!![position].id.toString()
                serviceAreaPrimary = mList!![position].primary!!
                serviceName = mList!![position].serviceName.toString()
                serviceChargeId = mList!![position].serviceChargeId.toString()
                revenueCenter = mList!![position].revenueCenter.toString()
                revenueCenterName = mList!![position].revenueCenterName.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    inner class DinningOptionAdapter(
        context: Context,
        dinningOptionArray: ArrayList<GetDinningOptionsListResponseList>
    ) :
        RecyclerView.Adapter<DinningOptionAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<GetDinningOptionsListResponseList>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = dinningOptionArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.dinning_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].optionName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                diningSelected = true
                dinningId = mList!![position].id.toString()
                dinningName = mList!![position].optionName.toString()
                dinningOptionBehaviour = mList!![position].behavior.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    inner class PrepStationOptionAdapter(
        context: Context,
        dinningOptionArray: ArrayList<PreparationStationsDataResponse>
    ) :
        RecyclerView.Adapter<PrepStationOptionAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<PreparationStationsDataResponse>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = dinningOptionArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.dinning_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].stationName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                prepStationSelected = true
                prepStationName = mList!![position].stationName.toString()
                prepStationId = mList!![position].id.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    inner class CashDrawerAdapter(
        context: Context,
        dinningOptionArray: ArrayList<CashDrawersDataResponse>
    ) :
        RecyclerView.Adapter<CashDrawerAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<CashDrawersDataResponse>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = dinningOptionArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.dinning_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].cashDrawerName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                cashDrawerSelected = true
                cashDrawerId = mList!![position].id.toString()
                cashDrawerName = mList!![position].cashDrawerName.toString()
                cashDrawerNumber = mList!![position].cashDrawer.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = this.currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun deviceNameStatus() {
        if (isDeviceNameUnique) {
            tv_device_name_status.text = getString(R.string.name_available)
        } else {
            tv_device_name_status.text = getString(R.string.name_already_in_use)
        }
    }

    override fun onBackPressed() {
        when {
            ll_prep_stations_options.visibility == View.VISIBLE -> {
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.VISIBLE
                ll_prep_stations_options.visibility = View.GONE
            }
            ll_cash_drawers.visibility == View.VISIBLE -> {
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.VISIBLE
                ll_cash_drawers.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
            }
            ll_dinning_options.visibility == View.VISIBLE -> {
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.VISIBLE
                ll_dinning_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
            }
            ll_service_options.visibility == View.VISIBLE -> {
                ll_otp.visibility = View.GONE
                ll_device_name.visibility = View.VISIBLE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
            }
            ll_device_name.visibility == View.VISIBLE -> {
                ll_otp.visibility = View.VISIBLE
                ll_device_name.visibility = View.GONE
                ll_service_options.visibility = View.GONE
                ll_dinning_options.visibility = View.GONE
                ll_cash_drawers.visibility = View.GONE
                ll_prep_stations_options.visibility = View.GONE
            }
            else -> {
                super.onBackPressed()
            }
        }
    }
}