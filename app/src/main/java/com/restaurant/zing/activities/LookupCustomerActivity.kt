package com.restaurant.zing.activities

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.CustomersListAdapter
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LookupCustomerActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var tabLayout: TabLayout
    private lateinit var tabPhone: TextView
    private lateinit var tabEmail: TextView
    private lateinit var ll_name_search: LinearLayout
    private lateinit var ll_phone_search: RelativeLayout
    private lateinit var et_phone: EditText
    private lateinit var et_name: EditText
    private lateinit var iv_phone_clear: ImageView
    private lateinit var iv_name_clear: ImageView
    private lateinit var ll_name_empty: LinearLayout
    private lateinit var tv_name_add_new_customer: TextView
    private lateinit var ll_phone_empty: LinearLayout
    private lateinit var tv_phone_add_new_customer: TextView
    private lateinit var ll_minus: LinearLayout
    private lateinit var ll_keypad: LinearLayout
    private lateinit var ll_add: LinearLayout
    private lateinit var ll_delete: LinearLayout
    private lateinit var ll_search: LinearLayout
    private lateinit var ll_one: LinearLayout
    private lateinit var ll_two: LinearLayout
    private lateinit var ll_three: LinearLayout
    private lateinit var ll_four: LinearLayout
    private lateinit var ll_five: LinearLayout
    private lateinit var ll_six: LinearLayout
    private lateinit var ll_seven: LinearLayout
    private lateinit var ll_eight: LinearLayout
    private lateinit var ll_nine: LinearLayout
    private lateinit var ll_star: LinearLayout
    private lateinit var ll_zero: LinearLayout
    private lateinit var ll_hash: LinearLayout
    private lateinit var rv_phone_search: RecyclerView
    private lateinit var rv_email_search: RecyclerView
    private lateinit var ll_customer_details_phone: LinearLayout
    private lateinit var tv_email_search: TextView
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var loading_dialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""

    private var phoneNumber = ""
    private var name_or_email = ""
    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private val MAX_CHAR = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lookup_customer)

        initialize()

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }

        iv_name_clear.setOnClickListener {
            et_name.setText("")
        }

        iv_phone_clear.setOnClickListener {
            phoneNumber = ""
            et_phone.setText(phoneNumber)
            ll_keypad.visibility = View.VISIBLE
            ll_customer_details_phone.visibility = View.GONE
        }

        ll_search.setOnClickListener {
            if (phoneNumber.length > MAX_CHAR) {
                phoneNumber = phoneNumber.substring(0, MAX_CHAR)
            }
            val jObj = JSONObject()
            jObj.put("userId", mEmployeeId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("phonenumber", phoneNumber)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            searchCustomerPhone(finalObject)
        }

        tv_email_search.setOnClickListener {
            name_or_email = et_name.text.toString().trim()
            val jObj = JSONObject()
            jObj.put("userId", mEmployeeId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("name_or_email", name_or_email)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            searchCustomerEmail(finalObject)
        }

        ll_delete.setOnClickListener {
            if (phoneNumber != "") {
                // removes 'n' chars in string
                phoneNumber = phoneNumber.dropLast(1)
                et_phone.setText(phoneNumber)
            } else {
                phoneNumber = ""
                et_phone.setText(phoneNumber)
            }
        }

        tv_name_add_new_customer.setOnClickListener {
            val intent = Intent(this@LookupCustomerActivity, AddNewCustomerActivity::class.java)
            startActivity(intent)
        }

        tv_phone_add_new_customer.setOnClickListener {
            val intent = Intent(this@LookupCustomerActivity, AddNewCustomerActivity::class.java)
            startActivity(intent)
        }

        et_phone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    ll_search.background = ContextCompat.getDrawable(
                        this@LookupCustomerActivity,
                        R.drawable.number_pad_search_background_gradient
                    )
                } else {
                    ll_search.background = ContextCompat.getDrawable(
                        this@LookupCustomerActivity,
                        R.drawable.number_pad_search_background_blue
                    )
                }
            }
        })

        et_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    tv_email_search.visibility = View.VISIBLE
                } else {
                    tv_email_search.visibility = View.GONE
                }
            }
        })

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                hideKeyboard()
                if (tab!!.position == 0) {
                    ll_phone_search.visibility = View.VISIBLE
                    ll_name_search.visibility = View.GONE
                    tabPhone.setTextColor(
                        ContextCompat.getColor(
                            this@LookupCustomerActivity,
                            R.color.tab_indicator
                        )
                    )
                    tabEmail.setTextColor(
                        ContextCompat.getColor(
                            this@LookupCustomerActivity,
                            R.color.edit_text_hint
                        )
                    )
                } else if (tab.position == 1) {
                    ll_phone_search.visibility = View.GONE
                    ll_name_search.visibility = View.VISIBLE
                    tabEmail.setTextColor(
                        ContextCompat.getColor(
                            this@LookupCustomerActivity,
                            R.color.tab_indicator
                        )
                    )
                    tabPhone.setTextColor(
                        ContextCompat.getColor(
                            this@LookupCustomerActivity,
                            R.color.edit_text_hint
                        )
                    )
                }
            }
        })

        ll_one.setOnClickListener {
            phoneNumber += str_one
            et_phone.setText(phoneNumber)
        }
        ll_two.setOnClickListener {
            phoneNumber += str_two
            et_phone.setText(phoneNumber)
        }
        ll_three.setOnClickListener {
            phoneNumber += str_three
            et_phone.setText(phoneNumber)
        }
        ll_four.setOnClickListener {
            phoneNumber += str_four
            et_phone.setText(phoneNumber)
        }
        ll_five.setOnClickListener {
            phoneNumber += str_five
            et_phone.setText(phoneNumber)
        }
        ll_six.setOnClickListener {
            phoneNumber += str_six
            et_phone.setText(phoneNumber)
        }
        ll_seven.setOnClickListener {
            phoneNumber += str_seven
            et_phone.setText(phoneNumber)
        }
        ll_eight.setOnClickListener {
            phoneNumber += str_eight
            et_phone.setText(phoneNumber)
        }
        ll_nine.setOnClickListener {
            phoneNumber += str_nine
            et_phone.setText(phoneNumber)
        }
        ll_zero.setOnClickListener {
            phoneNumber += str_zero
            et_phone.setText(phoneNumber)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        tabLayout = findViewById(R.id.tabLayout)
        setUpTabIcons()
        ll_name_search = findViewById(R.id.ll_name_search)
        ll_phone_search = findViewById(R.id.ll_phone_search)
        et_phone = findViewById(R.id.et_phone)
        et_name = findViewById(R.id.et_name)
        iv_phone_clear = findViewById(R.id.iv_phone_clear)
        iv_name_clear = findViewById(R.id.iv_name_clear)
        tv_name_add_new_customer = findViewById(R.id.tv_name_add_new_customer)
        tv_phone_add_new_customer = findViewById(R.id.tv_phone_add_new_customer)
        ll_phone_empty = findViewById(R.id.ll_phone_empty)
        ll_name_empty = findViewById(R.id.ll_name_empty)
        rv_phone_search = findViewById(R.id.rv_phone_search)
        ll_customer_details_phone = findViewById(R.id.ll_customer_details_phone)
        tv_email_search = findViewById(R.id.tv_email_search)
        rv_email_search = findViewById(R.id.rv_email_search)
        ll_switch_user = findViewById(R.id.ll_switch_user)

        val phoneLayoutManager =
            LinearLayoutManager(this@LookupCustomerActivity, RecyclerView.VERTICAL, false)
        rv_phone_search.layoutManager = phoneLayoutManager
        val emailLayoutManager =
            LinearLayoutManager(this@LookupCustomerActivity, RecyclerView.VERTICAL, false)
        rv_email_search.layoutManager = emailLayoutManager
        ll_keypad = findViewById(R.id.ll_keypad)
        ll_minus = findViewById(R.id.ll_minus)
        ll_add = findViewById(R.id.ll_add)
        ll_delete = findViewById(R.id.ll_delete)
        ll_search = findViewById(R.id.ll_search)
        ll_one = findViewById(R.id.ll_one)
        ll_two = findViewById(R.id.ll_two)
        ll_three = findViewById(R.id.ll_three)
        ll_four = findViewById(R.id.ll_four)
        ll_five = findViewById(R.id.ll_five)
        ll_six = findViewById(R.id.ll_six)
        ll_seven = findViewById(R.id.ll_seven)
        ll_eight = findViewById(R.id.ll_eight)
        ll_nine = findViewById(R.id.ll_nine)
        ll_zero = findViewById(R.id.ll_zero)
        ll_hash = findViewById(R.id.ll_hash)
        ll_star = findViewById(R.id.ll_star)
    }

    private fun setUpTabIcons() {
        val phoneView = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        val emailView = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        tabPhone = phoneView.findViewById(R.id.tv_tab_name)
        tabEmail = emailView.findViewById(R.id.tv_tab_name)
        tabPhone.text = getString(R.string.phone_number)
        tabEmail.text = getString(R.string.name_email)
        tabPhone.setTextColor(
            ContextCompat.getColor(this@LookupCustomerActivity, R.color.tab_indicator)
        )
        tabEmail.setTextColor(
            ContextCompat.getColor(this@LookupCustomerActivity, R.color.edit_text_hint)
        )
        tabLayout.addTab(tabLayout.newTab().setCustomView(phoneView), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(emailView))
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun searchCustomerPhone(finalObject: JsonObject) {
        loading_dialog.show()
        ll_keypad.visibility = View.GONE
        ll_customer_details_phone.visibility = View.VISIBLE
        val api = ApiInterface.create()
        val call = api.searchCustomerPhoneApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@LookupCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val customersList = respBody.customer!!
                        if (customersList.size > 0) {
                            val adapter =
                                CustomersListAdapter(this@LookupCustomerActivity, customersList)
                            rv_phone_search.adapter = adapter
                            adapter.notifyDataSetChanged()
                            ll_phone_empty.visibility = View.GONE
                            rv_phone_search.visibility = View.VISIBLE
                        } else {
                            ll_phone_empty.visibility = View.VISIBLE
                            rv_phone_search.visibility = View.GONE
                        }
                    } else {
                        ll_phone_empty.visibility = View.VISIBLE
                        rv_phone_search.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun searchCustomerEmail(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.searchCustomerNameAndEmailApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@LookupCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val customersList = respBody.customer!!
                        if (customersList.size > 0) {
                            val adapter =
                                CustomersListAdapter(this@LookupCustomerActivity, customersList)
                            rv_email_search.adapter = adapter
                            adapter.notifyDataSetChanged()
                            ll_name_empty.visibility = View.GONE
                            rv_email_search.visibility = View.VISIBLE
                        } else {
                            ll_name_empty.visibility = View.VISIBLE
                            rv_email_search.visibility = View.GONE
                        }
                    } else {
                        ll_name_empty.visibility = View.VISIBLE
                        rv_email_search.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@LookupCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@LookupCustomerActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@LookupCustomerActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@LookupCustomerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = this.currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}