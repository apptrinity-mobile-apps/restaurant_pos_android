package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import kotlinx.android.synthetic.main.activity_order_split.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

@SuppressLint("SetTextI18n")
class OrderSplitNewActivity : AppCompatActivity() {

    lateinit var loading_dialog: Dialog
    lateinit var img_back_id: ImageView
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantId = ""
    lateinit var taxRatesList: ArrayList<TaxRatesDetailsResponse>

    lateinit var splitArrayFromOrders: ArrayList<SplitOrderSetupModelNew1>
    lateinit var splitArrayOne: ArrayList<OrderItemsModel>
    lateinit var splitArrayTwo: ArrayList<OrderItemsModel>
    lateinit var splitArrayThree: ArrayList<OrderItemsModel>
    lateinit var splitArrayFour: ArrayList<OrderItemsModel>
    lateinit var splitAdapterNew: SplitDataAdapterNew
    lateinit var splitTwoAdapterNew: SplitDataTwoAdapterNew
    lateinit var splitThreeAdapterNew: SplitDataThreeAdapterNew
    lateinit var splitFourAdapterNew: SplitDataFourAdapterNew
    lateinit var finalSplitOneArrayListNew: SplitOrderSetupModelNew1
    lateinit var finalSplitTwoArrayListNew: SplitOrderSetupModelNew1
    lateinit var finalSplitThreeArrayListNew: SplitOrderSetupModelNew1
    lateinit var finalSplitFourArrayListNew: SplitOrderSetupModelNew1
    lateinit var finalAllOrderSplitData: ArrayList<SplitOrderSetupModelNew1>

    lateinit var tv_check_number_one: TextView
    lateinit var tv_check_number_two: TextView
    lateinit var tv_check_number_three: TextView
    lateinit var tv_check_number_four: TextView
    private var check_id_1 = ""
    private var check_id_2 = ""
    private var check_id_3 = ""
    private var check_id_4 = ""
    private var check_pay_status_1 = 0
    private var check_pay_status_2 = 0
    private var check_pay_status_3 = 0
    private var check_pay_status_4 = 0
    private var check_1 = false
    private var check_2 = false
    private var check_3 = false
    private var check_4 = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_split)

        initialize()
        img_back_id.setOnClickListener {
            onBackPressed()
        }
        val obj = JSONObject()
        obj.put("userId", mEmployeeId)
        obj.put("restaurantId", restaurantId)
        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
        getTaxRates(jObject)

        try {
            val splitArrayNew =
                intent.getSerializableExtra("splitArrayNew")!! as ArrayList<SplitOrderSetupModelNew1>
            splitArrayFromOrders.addAll(splitArrayNew)
            println("splitArrayFromOrders===" + splitArrayFromOrders.size)
            for (i in 0 until splitArrayFromOrders.size) {
                when (splitArrayFromOrders[i].checkNumber) {
                    1 -> {
                        splitArrayOne.addAll(splitArrayFromOrders[i].orderDetails)
                        check_id_1 = splitArrayFromOrders[i].id
                        check_pay_status_1 = splitArrayFromOrders[i].status
                        check_1 = true
                    }
                    2 -> {
                        splitArrayTwo.addAll(splitArrayFromOrders[i].orderDetails)
                        check_id_2 = splitArrayFromOrders[i].id
                        check_pay_status_2 = splitArrayFromOrders[i].status
                        check_2 = true
                    }
                    3 -> {
                        splitArrayThree.addAll(splitArrayFromOrders[i].orderDetails)
                        check_id_3 = splitArrayFromOrders[i].id
                        check_pay_status_3 = splitArrayFromOrders[i].status
                        check_3 = true
                    }
                    4 -> {
                        splitArrayFour.addAll(splitArrayFromOrders[i].orderDetails)
                        check_id_4 = splitArrayFromOrders[i].id
                        check_pay_status_4 = splitArrayFromOrders[i].status
                        check_4 = true
                    }
                }
            }
            tv_check_number_one.text =
                "#1, " + " Table " + splitArrayFromOrders[0].tableNumber + ", " + splitArrayFromOrders[0].dineInOptionName
            tv_check_number_two.text =
                "#2, " + " Table " + splitArrayFromOrders[0].tableNumber + ", " + splitArrayFromOrders[0].dineInOptionName
            tv_check_number_three.text =
                "#3, " + " Table " + splitArrayFromOrders[0].tableNumber + ", " + splitArrayFromOrders[0].dineInOptionName
            tv_check_number_four.text =
                "#4, " + " Table " + splitArrayFromOrders[0].tableNumber + ", " + splitArrayFromOrders[0].dineInOptionName
            val order_LayoutManager =
                LinearLayoutManager(this@OrderSplitNewActivity, RecyclerView.VERTICAL, false)
            rv_split_one_id.layoutManager = order_LayoutManager
            val order_LayoutManager1 =
                LinearLayoutManager(this@OrderSplitNewActivity, RecyclerView.VERTICAL, false)
            rv_split_two_id.layoutManager = order_LayoutManager1
            val order_LayoutManager2 =
                LinearLayoutManager(this@OrderSplitNewActivity, RecyclerView.VERTICAL, false)
            rv_split_three_id.layoutManager = order_LayoutManager2
            val order_LayoutManager3 =
                LinearLayoutManager(this@OrderSplitNewActivity, RecyclerView.VERTICAL, false)
            rv_split_four_id.layoutManager = order_LayoutManager3
            splitAdapterNew = SplitDataAdapterNew(this, splitArrayOne)
            splitTwoAdapterNew = SplitDataTwoAdapterNew(this, splitArrayTwo)
            splitThreeAdapterNew = SplitDataThreeAdapterNew(this, splitArrayThree)
            splitFourAdapterNew = SplitDataFourAdapterNew(this, splitArrayFour)
            rv_split_one_id.adapter = splitAdapterNew
            splitAdapterNew.notifyDataSetChanged()
            rv_split_two_id.adapter = splitTwoAdapterNew
            splitTwoAdapterNew.notifyDataSetChanged()
            rv_split_three_id.adapter = splitThreeAdapterNew
            splitThreeAdapterNew.notifyDataSetChanged()
            rv_split_four_id.adapter = splitFourAdapterNew
            splitFourAdapterNew.notifyDataSetChanged()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        ll_split_id.setOnClickListener {
            CollectionOfKeys()
        }

        cv_split_card_one.setOnClickListener {
            if (check_pay_status_1 == 3) {
                cv_split_card_one.isEnabled = false
            } else {
                if (splitArrayTwo.size > 0 || splitArrayThree.size > 0 || splitArrayFour.size > 0) {
                    Log.d("spli", splitArrayOne.size.toString())
                    val splitArray = ArrayList<OrderItemsModel>()
                    val mList: List<OrderItemsModel> = splitTwoAdapterNew.ourSplitArray
                    splitArrayTwo = ArrayList()
                    if (mList.isNotEmpty())
                        for (i in mList.indices) {
                            val splitOrderModelNew: OrderItemsModel = mList[i]
                            if (splitOrderModelNew.isSelected) {
                                splitOrderModelNew.isSelected = false
                                splitArray.add(splitOrderModelNew)
                            } else {
                                splitArrayTwo.add(splitOrderModelNew)
                            }
                        }
                    val mList1: List<OrderItemsModel> = splitThreeAdapterNew.ourSplitArray
                    splitArrayThree = ArrayList()
                    if (mList1.isNotEmpty())
                        for (i in mList1.indices) {
                            val splitOrderModelNew: OrderItemsModel = mList1[i]
                            if (splitOrderModelNew.isSelected) {
                                splitOrderModelNew.isSelected = false
                                splitArray.add(splitOrderModelNew)
                            } else {
                                splitArrayThree.add(splitOrderModelNew)
                            }
                        }
                    val mList2: List<OrderItemsModel> = splitFourAdapterNew.ourSplitArray
                    splitArrayFour = ArrayList()
                    if (mList2.isNotEmpty())
                        for (i in mList2.indices) {
                            val splitOrderModelNew: OrderItemsModel = mList2[i]
                            if (splitOrderModelNew.isSelected) {
                                splitOrderModelNew.isSelected = false
                                splitArray.add(splitOrderModelNew)
                            } else {
                                splitArrayFour.add(splitOrderModelNew)
                            }
                        }
                    splitArrayOne.addAll(splitArray)
                    val order_LayoutManager =
                        LinearLayoutManager(
                            this@OrderSplitNewActivity,
                            RecyclerView.VERTICAL,
                            false
                        )
                    rv_split_one_id.layoutManager = order_LayoutManager
                    splitAdapterNew = SplitDataAdapterNew(this, splitArrayOne)
                    rv_split_one_id.adapter = splitAdapterNew
                    splitAdapterNew.notifyDataSetChanged()
                    val order_LayoutManager1 =
                        LinearLayoutManager(
                            this@OrderSplitNewActivity,
                            RecyclerView.VERTICAL,
                            false
                        )
                    rv_split_two_id.layoutManager = order_LayoutManager1
                    splitTwoAdapterNew = SplitDataTwoAdapterNew(this, splitArrayTwo)
                    rv_split_two_id.adapter = splitTwoAdapterNew
                    splitTwoAdapterNew.notifyDataSetChanged()
                    val order_LayoutManager2 =
                        LinearLayoutManager(
                            this@OrderSplitNewActivity,
                            RecyclerView.VERTICAL,
                            false
                        )
                    rv_split_three_id.layoutManager = order_LayoutManager2
                    splitThreeAdapterNew = SplitDataThreeAdapterNew(this, splitArrayThree)
                    rv_split_three_id.adapter = splitThreeAdapterNew
                    splitThreeAdapterNew.notifyDataSetChanged()
                    val order_LayoutManager3 =
                        LinearLayoutManager(
                            this@OrderSplitNewActivity,
                            RecyclerView.VERTICAL,
                            false
                        )
                    rv_split_four_id.layoutManager = order_LayoutManager3
                    splitFourAdapterNew = SplitDataFourAdapterNew(this, splitArrayFour)
                    rv_split_four_id.adapter = splitFourAdapterNew
                    splitFourAdapterNew.notifyDataSetChanged()
                }
            }
        }
        cv_split_card_two.setOnClickListener {
            if (check_pay_status_2 == 3) {
                cv_split_card_two.isEnabled = false
            } else {
                val splitArray = ArrayList<OrderItemsModel>()
                val mList: List<OrderItemsModel> = splitAdapterNew.ourSplitArray
                splitArrayOne = ArrayList()
                if (mList.isNotEmpty())
                    for (i in mList.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayOne.add(splitOrderModelNew)
                        }
                    }
                val mList1: List<OrderItemsModel> = splitThreeAdapterNew.ourSplitArray
                splitArrayThree = ArrayList()
                if (mList1.isNotEmpty())
                    for (i in mList1.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList1[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayThree.add(splitOrderModelNew)
                        }
                    }
                val mList2: List<OrderItemsModel> = splitFourAdapterNew.ourSplitArray
                splitArrayFour = ArrayList()
                if (mList2.isNotEmpty())
                    for (i in mList2.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList2[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayFour.add(splitOrderModelNew)
                        }
                    }
                val order_LayoutManager =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_one_id.layoutManager = order_LayoutManager
                splitAdapterNew = SplitDataAdapterNew(this, splitArrayOne)
                rv_split_one_id.adapter = splitAdapterNew
                splitAdapterNew.notifyDataSetChanged()
                splitArrayTwo.addAll(splitArray)
                val order_LayoutManager1 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_two_id.layoutManager = order_LayoutManager1
                splitTwoAdapterNew = SplitDataTwoAdapterNew(this, splitArrayTwo)
                rv_split_two_id.adapter = splitTwoAdapterNew
                splitTwoAdapterNew.notifyDataSetChanged()
                val order_LayoutManager2 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_three_id.layoutManager = order_LayoutManager2
                splitThreeAdapterNew = SplitDataThreeAdapterNew(this, splitArrayThree)
                rv_split_three_id.adapter = splitThreeAdapterNew
                splitThreeAdapterNew.notifyDataSetChanged()
                val order_LayoutManager3 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_four_id.layoutManager = order_LayoutManager3
                splitFourAdapterNew = SplitDataFourAdapterNew(this, splitArrayFour)
                rv_split_four_id.adapter = splitFourAdapterNew
                splitFourAdapterNew.notifyDataSetChanged()
            }
        }
        cv_split_card_three.setOnClickListener {
            if (check_pay_status_3 == 3) {
                cv_split_card_three.isEnabled = false

            } else {
                val splitArray = ArrayList<OrderItemsModel>()
                val mList: List<OrderItemsModel> = splitAdapterNew.ourSplitArray
                splitArrayOne = ArrayList()
                if (mList.isNotEmpty())
                    for (i in mList.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayOne.add(splitOrderModelNew)
                        }
                    }
                val mList1: List<OrderItemsModel> = splitTwoAdapterNew.ourSplitArray
                splitArrayTwo = ArrayList()
                if (mList1.isNotEmpty())
                    for (i in mList1.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList1[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayTwo.add(splitOrderModelNew)
                        }
                    }
                val mList2: List<OrderItemsModel> = splitFourAdapterNew.ourSplitArray
                splitArrayFour = ArrayList()
                if (mList2.isNotEmpty())
                    for (i in mList2.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList2[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayFour.add(splitOrderModelNew)
                        }
                    }
                val order_LayoutManager =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_one_id.layoutManager = order_LayoutManager
                splitAdapterNew = SplitDataAdapterNew(this, splitArrayOne)
                rv_split_one_id.adapter = splitAdapterNew
                splitAdapterNew.notifyDataSetChanged()
                val order_LayoutManager1 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_two_id.layoutManager = order_LayoutManager1
                splitTwoAdapterNew = SplitDataTwoAdapterNew(this, splitArrayTwo)
                rv_split_two_id.adapter = splitTwoAdapterNew
                splitTwoAdapterNew.notifyDataSetChanged()
                splitArrayThree.addAll(splitArray)
                val order_LayoutManager2 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_three_id.layoutManager = order_LayoutManager2
                splitThreeAdapterNew = SplitDataThreeAdapterNew(this, splitArrayThree)
                rv_split_three_id.adapter = splitThreeAdapterNew
                splitThreeAdapterNew.notifyDataSetChanged()
                val order_LayoutManager3 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_four_id.layoutManager = order_LayoutManager3
                splitFourAdapterNew = SplitDataFourAdapterNew(this, splitArrayFour)
                rv_split_four_id.adapter = splitFourAdapterNew
                splitFourAdapterNew.notifyDataSetChanged()
            }
        }
        cv_split_card_four.setOnClickListener {
            if (check_pay_status_4 == 3) {
                cv_split_card_four.isEnabled = false
            } else {
                val splitArray = ArrayList<OrderItemsModel>()
                val mList: List<OrderItemsModel> = splitAdapterNew.ourSplitArray
                splitArrayOne = ArrayList()
                if (mList.isNotEmpty())
                    for (i in mList.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayOne.add(splitOrderModelNew)
                        }
                    }
                val mList1: List<OrderItemsModel> = splitTwoAdapterNew.ourSplitArray
                splitArrayTwo = ArrayList()
                if (mList1.isNotEmpty())
                    for (i in mList1.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList1[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayTwo.add(splitOrderModelNew)
                        }
                    }
                val mList2: List<OrderItemsModel> = splitThreeAdapterNew.ourSplitArray
                splitArrayThree = ArrayList()
                if (mList2.isNotEmpty())
                    for (i in mList2.indices) {
                        val splitOrderModelNew: OrderItemsModel = mList2[i]
                        if (splitOrderModelNew.isSelected) {
                            splitOrderModelNew.isSelected = false
                            splitArray.add(splitOrderModelNew)
                        } else {
                            splitArrayThree.add(splitOrderModelNew)
                        }
                    }
                val order_LayoutManager =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_one_id.layoutManager = order_LayoutManager
                splitAdapterNew = SplitDataAdapterNew(this, splitArrayOne)
                rv_split_one_id.adapter = splitAdapterNew
                splitAdapterNew.notifyDataSetChanged()
                val order_LayoutManager1 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_two_id.layoutManager = order_LayoutManager1
                splitTwoAdapterNew = SplitDataTwoAdapterNew(this, splitArrayTwo)
                rv_split_two_id.adapter = splitTwoAdapterNew
                splitTwoAdapterNew.notifyDataSetChanged()
                val order_LayoutManager2 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_three_id.layoutManager = order_LayoutManager2
                splitThreeAdapterNew = SplitDataThreeAdapterNew(this, splitArrayThree)
                rv_split_three_id.adapter = splitThreeAdapterNew
                splitThreeAdapterNew.notifyDataSetChanged()
                splitArrayFour.addAll(splitArray)
                val order_LayoutManager3 =
                    LinearLayoutManager(
                        this@OrderSplitNewActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                rv_split_four_id.layoutManager = order_LayoutManager3
                splitFourAdapterNew = SplitDataFourAdapterNew(this, splitArrayFour)
                rv_split_four_id.adapter = splitFourAdapterNew
                splitFourAdapterNew.notifyDataSetChanged()
            }
        }

        ll_done_split_id.setOnClickListener {
            CollectionOfKeys()
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        splitArrayFromOrders = ArrayList()
        splitArrayOne = ArrayList()
        splitArrayTwo = ArrayList()
        splitArrayThree = ArrayList()
        splitArrayFour = ArrayList()
        img_back_id = findViewById(R.id.img_back_id)
        tv_check_number_one = findViewById(R.id.tv_check_number_one)
        tv_check_number_two = findViewById(R.id.tv_check_number_two)
        tv_check_number_three = findViewById(R.id.tv_check_number_three)
        tv_check_number_four = findViewById(R.id.tv_check_number_four)
    }

    inner class SplitDataAdapterNew(
        val context: Context,
        val ourSplitArray: ArrayList<OrderItemsModel>
    ) : RecyclerView.Adapter<SplitDataAdapterNew.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.split_list_items, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourSplitArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.check_box_split.text = ourSplitArray[position].itemName
            holder.tv_qty_id.text = ourSplitArray[position].quantity.toString()
            holder.check_box_split.isChecked = ourSplitArray[position].isSelected
            holder.check_box_split.tag = ourSplitArray[position]
            if (check_pay_status_1 == 3 || ourSplitArray[position].voidStatus) {
                holder.check_box_split.isEnabled = false
            }
            holder.check_box_split.setOnClickListener { v ->
                val cb = v as CheckBox
                val splitOrderModelNew = cb.tag as OrderItemsModel
                splitOrderModelNew.isSelected = cb.isChecked
                ourSplitArray[position].isSelected = cb.isChecked
                for (i in 0 until ourSplitArray.size) {
                    if (ourSplitArray[position].isCombo) {
                        if ((ourSplitArray[position].comboId == ourSplitArray[i].comboId) && (ourSplitArray[position].comboUniqueId == ourSplitArray[i].comboUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                    if (ourSplitArray[position].isBogo) {
                        if ((ourSplitArray[position].bogoId == ourSplitArray[i].bogoId) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[i].bogoUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                }
                notifyDataSetChanged()
            }

            // combo/bogo grouping
            val type: String
            when {
                ourSplitArray[position].isBogo -> {
                    type = "BoGo"
                }
                ourSplitArray[position].isCombo -> {
                    type = "Combo"
                }
                else -> {
                    type = ""
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(0, 0, 0, 0)
                }
            }
            holder.tv_item_type.text = type
            if (ourSplitArray[position].isCombo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (ourSplitArray[position].isBogo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (position - 1 != -1) {
                if ((ourSplitArray[position].isCombo && ourSplitArray[position - 1].isCombo) && (ourSplitArray[position].comboUniqueId == ourSplitArray[position - 1].comboUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
                if ((ourSplitArray[position].isBogo && ourSplitArray[position - 1].isBogo) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[position - 1].bogoUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val check_box_split = view.findViewById<CheckBox>(R.id.check_box_split)
            val tv_qty_id = view.findViewById<TextView>(R.id.tv_qty_id)
            val tv_item_type = view.findViewById<TextView>(R.id.tv_item_type)
            val ll_item = view.findViewById<LinearLayout>(R.id.ll_item)
        }
    }

    inner class SplitDataTwoAdapterNew(
        val context: Context,
        val ourSplitArray: ArrayList<OrderItemsModel>
    ) : RecyclerView.Adapter<SplitDataTwoAdapterNew.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.split_list_items, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourSplitArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.check_box_split.text = ourSplitArray[position].itemName
            holder.tv_qty_id.text = ourSplitArray[position].quantity.toString()
            holder.check_box_split.isChecked = ourSplitArray[position].isSelected
            holder.check_box_split.tag = ourSplitArray[position]
            if (check_pay_status_2 == 3 || ourSplitArray[position].voidStatus) {
                holder.check_box_split.isEnabled = false
            }
            holder.check_box_split.setOnClickListener { v ->
                val cb = v as CheckBox
                val splitOrderModelNew = cb.tag as OrderItemsModel
                splitOrderModelNew.isSelected = cb.isChecked
                ourSplitArray[position].isSelected = cb.isChecked
                for (i in 0 until ourSplitArray.size) {
                    if (ourSplitArray[position].isCombo) {
                        if ((ourSplitArray[position].comboId == ourSplitArray[i].comboId) && (ourSplitArray[position].comboUniqueId == ourSplitArray[i].comboUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                    if (ourSplitArray[position].isBogo) {
                        if ((ourSplitArray[position].bogoId == ourSplitArray[i].bogoId) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[i].bogoUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                }
                notifyDataSetChanged()
            }

            // combo/bogo grouping
            val type: String
            when {
                ourSplitArray[position].isBogo -> {
                    type = "BoGo"
                }
                ourSplitArray[position].isCombo -> {
                    type = "Combo"
                }
                else -> {
                    type = ""
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(0, 0, 0, 0)
                }
            }
            holder.tv_item_type.text = type
            if (ourSplitArray[position].isCombo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (ourSplitArray[position].isBogo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (position - 1 != -1) {
                if ((ourSplitArray[position].isCombo && ourSplitArray[position - 1].isCombo) && (ourSplitArray[position].comboUniqueId == ourSplitArray[position - 1].comboUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
                if ((ourSplitArray[position].isBogo && ourSplitArray[position - 1].isBogo) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[position - 1].bogoUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val check_box_split = view.findViewById<CheckBox>(R.id.check_box_split)
            val tv_qty_id = view.findViewById<TextView>(R.id.tv_qty_id)
            val tv_item_type = view.findViewById<TextView>(R.id.tv_item_type)
            val ll_item = view.findViewById<LinearLayout>(R.id.ll_item)
        }
    }

    inner class SplitDataThreeAdapterNew(
        val context: Context,
        val ourSplitArray: ArrayList<OrderItemsModel>
    ) : RecyclerView.Adapter<SplitDataThreeAdapterNew.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.split_list_items, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourSplitArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.check_box_split.text = ourSplitArray[position].itemName
            holder.tv_qty_id.text = ourSplitArray[position].quantity.toString()
            holder.check_box_split.isChecked = ourSplitArray[position].isSelected
            holder.check_box_split.tag = ourSplitArray[position]
            if (check_pay_status_3 == 3 || ourSplitArray[position].voidStatus) {
                holder.check_box_split.isEnabled = false
            }
            holder.check_box_split.setOnClickListener { v ->
                val cb = v as CheckBox
                val splitOrderModelNew = cb.tag as OrderItemsModel
                splitOrderModelNew.isSelected = cb.isChecked
                ourSplitArray[position].isSelected = cb.isChecked
                for (i in 0 until ourSplitArray.size) {
                    if (ourSplitArray[position].isCombo) {
                        if ((ourSplitArray[position].comboId == ourSplitArray[i].comboId) && (ourSplitArray[position].comboUniqueId == ourSplitArray[i].comboUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                    if (ourSplitArray[position].isBogo) {
                        if ((ourSplitArray[position].bogoId == ourSplitArray[i].bogoId) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[i].bogoUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                }
                notifyDataSetChanged()
            }

            // combo/bogo grouping
            val type: String
            when {
                ourSplitArray[position].isBogo -> {
                    type = "BoGo"
                }
                ourSplitArray[position].isCombo -> {
                    type = "Combo"
                }
                else -> {
                    type = ""
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(0, 0, 0, 0)
                }
            }
            holder.tv_item_type.text = type
            if (ourSplitArray[position].isCombo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (ourSplitArray[position].isBogo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (position - 1 != -1) {
                if ((ourSplitArray[position].isCombo && ourSplitArray[position - 1].isCombo) && (ourSplitArray[position].comboUniqueId == ourSplitArray[position - 1].comboUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
                if ((ourSplitArray[position].isBogo && ourSplitArray[position - 1].isBogo) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[position - 1].bogoUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val check_box_split = view.findViewById<CheckBox>(R.id.check_box_split)
            val tv_qty_id = view.findViewById<TextView>(R.id.tv_qty_id)
            val tv_item_type = view.findViewById<TextView>(R.id.tv_item_type)
            val ll_item = view.findViewById<LinearLayout>(R.id.ll_item)
        }
    }

    inner class SplitDataFourAdapterNew(
        val context: Context,
        val ourSplitArray: ArrayList<OrderItemsModel>
    ) : RecyclerView.Adapter<SplitDataFourAdapterNew.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.split_list_items, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return ourSplitArray.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.check_box_split.text = ourSplitArray[position].itemName
            holder.tv_qty_id.text = ourSplitArray[position].quantity.toString()
            holder.check_box_split.isChecked = ourSplitArray[position].isSelected
            holder.check_box_split.tag = ourSplitArray[position]
            if (check_pay_status_4 == 3 || ourSplitArray[position].voidStatus) {
                holder.check_box_split.isEnabled = false
            }
            holder.check_box_split.setOnClickListener { v ->
                val cb = v as CheckBox
                val splitOrderModelNew = cb.tag as OrderItemsModel
                splitOrderModelNew.isSelected = cb.isChecked
                ourSplitArray[position].isSelected = cb.isChecked
                for (i in 0 until ourSplitArray.size) {
                    if (ourSplitArray[position].isCombo) {
                        if ((ourSplitArray[position].comboId == ourSplitArray[i].comboId) && (ourSplitArray[position].comboUniqueId == ourSplitArray[i].comboUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                    if (ourSplitArray[position].isBogo) {
                        if ((ourSplitArray[position].bogoId == ourSplitArray[i].bogoId) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[i].bogoUniqueId)) {
                            ourSplitArray[i].isSelected = cb.isChecked
                        }
                    }
                }
                notifyDataSetChanged()
            }

            // combo/bogo grouping
            val type: String
            when {
                ourSplitArray[position].isBogo -> {
                    type = "BoGo"
                }
                ourSplitArray[position].isCombo -> {
                    type = "Combo"
                }
                else -> {
                    type = ""
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(0, 0, 0, 0)
                }
            }
            holder.tv_item_type.text = type
            if (ourSplitArray[position].isCombo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (ourSplitArray[position].isBogo) {
                holder.tv_item_type.visibility = View.VISIBLE
                holder.ll_item.setPadding(20, 0, 0, 0)
            }
            if (position - 1 != -1) {
                if ((ourSplitArray[position].isCombo && ourSplitArray[position - 1].isCombo) && (ourSplitArray[position].comboUniqueId == ourSplitArray[position - 1].comboUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
                if ((ourSplitArray[position].isBogo && ourSplitArray[position - 1].isBogo) && (ourSplitArray[position].bogoUniqueId == ourSplitArray[position - 1].bogoUniqueId)) {
                    holder.tv_item_type.visibility = View.GONE
                    holder.ll_item.setPadding(20, 0, 0, 0)
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val check_box_split = view.findViewById<CheckBox>(R.id.check_box_split)
            val tv_qty_id = view.findViewById<TextView>(R.id.tv_qty_id)
            val tv_item_type = view.findViewById<TextView>(R.id.tv_item_type)
            val ll_item = view.findViewById<LinearLayout>(R.id.ll_item)
        }
    }

    fun CollectionOfKeys() {

        finalAllOrderSplitData = ArrayList()

        if (splitArrayOne.size > 0) {
            finalSplitOneArrayListNew = SplitOrderSetupModelNew1(
                1,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_1,
                check_pay_status_1,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                splitArrayOne
            )
            finalAllOrderSplitData.add(finalSplitOneArrayListNew)
        } else {
            finalSplitOneArrayListNew = SplitOrderSetupModelNew1(
                1,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_1,
                check_pay_status_1,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                ArrayList()
            )
            finalAllOrderSplitData.add(finalSplitOneArrayListNew)
        }

        if (splitArrayTwo.size > 0) {
            finalSplitTwoArrayListNew = SplitOrderSetupModelNew1(
                2,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_2,
                check_pay_status_2,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                splitArrayTwo
            )
            finalAllOrderSplitData.add(finalSplitTwoArrayListNew)
        } else {
            finalSplitTwoArrayListNew = SplitOrderSetupModelNew1(
                2,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_2,
                check_pay_status_2,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                ArrayList()
            )
            finalAllOrderSplitData.add(finalSplitTwoArrayListNew)
        }

        if (splitArrayThree.size > 0) {
            finalSplitThreeArrayListNew = SplitOrderSetupModelNew1(
                3,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_3,
                check_pay_status_3,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                splitArrayThree
            )
            finalAllOrderSplitData.add(finalSplitThreeArrayListNew)
        } else {
            finalSplitThreeArrayListNew = SplitOrderSetupModelNew1(
                3,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_3,
                check_pay_status_3,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                ArrayList()
            )
            finalAllOrderSplitData.add(finalSplitThreeArrayListNew)
        }

        if (splitArrayFour.size > 0) {
            finalSplitFourArrayListNew = SplitOrderSetupModelNew1(
                4,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_4,
                check_pay_status_4,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                splitArrayFour
            )
            finalAllOrderSplitData.add(finalSplitFourArrayListNew)
        } else {
            finalSplitFourArrayListNew = SplitOrderSetupModelNew1(
                4,
                splitArrayFromOrders[0].closedOn,
                splitArrayFromOrders[0].createdBy,
                splitArrayFromOrders[0].createdByName,
                splitArrayFromOrders[0].createdOn,
                splitArrayFromOrders[0].creditsUsed,
                splitArrayFromOrders[0].dineInBehaviour,
                splitArrayFromOrders[0].dueAmount,
                splitArrayFromOrders[0].dineInOption,
                splitArrayFromOrders[0].dineInOptionName,
                splitArrayFromOrders[0].discountAmount,
                splitArrayFromOrders[0].haveCustomer,
                check_id_4,
                check_pay_status_4,
                splitArrayFromOrders[0].restaurantId,
                splitArrayFromOrders[0].revenueCenterId,
                splitArrayFromOrders[0].discountLevel,
                splitArrayFromOrders[0].discountId,
                splitArrayFromOrders[0].discountName,
                splitArrayFromOrders[0].discountType,
                splitArrayFromOrders[0].discountValue,
                splitArrayFromOrders[0].serviceAreaId,
                splitArrayFromOrders[0].orderNumber,
                splitArrayFromOrders[0].subTotal,
                splitArrayFromOrders[0].tableNumber,
                splitArrayFromOrders[0].taxAmount,
                splitArrayFromOrders[0].tipAmount,
                splitArrayFromOrders[0].totalAmount,
                splitArrayFromOrders[0].customerDetails,
                ArrayList()
            )
            finalAllOrderSplitData.add(finalSplitFourArrayListNew)
        }

        finalJsonObject()
    }

    private fun finalJsonObject() {
        if (finalAllOrderSplitData.isNotEmpty()) {
            val mMainJsonObject = JSONObject()
            val mMainChecksJsonArray = JSONArray()
            for (i in 0 until finalAllOrderSplitData.size) {
                if (finalAllOrderSplitData[i].status != 3) {
                    if ((finalAllOrderSplitData[i].id == "" && finalAllOrderSplitData[i].orderDetails.size > 0) ||
                        (finalAllOrderSplitData[i].id != "" && finalAllOrderSplitData[i].orderDetails.size > 0) ||
                        (finalAllOrderSplitData[i].id != "" && finalAllOrderSplitData[i].orderDetails.isEmpty())
                    ) {
                        var total_tax = 0.00
                        val mChecksObject = JSONObject()
                        mChecksObject.put("checkNumber", finalAllOrderSplitData[i].checkNumber)
                        mChecksObject.put("dineInOptionId", finalAllOrderSplitData[i].dineInOption)
                        mChecksObject.put(
                            "dineInOptionName",
                            finalAllOrderSplitData[i].dineInOptionName
                        )
                        mChecksObject.put(
                            "dineInBehaviour",
                            finalAllOrderSplitData[i].dineInBehaviour
                        )
                        mChecksObject.put("serviceAreaId", finalAllOrderSplitData[i].serviceAreaId)
                        mChecksObject.put(
                            "revenueCenterId",
                            finalAllOrderSplitData[i].revenueCenterId
                        )
                        mChecksObject.put("creditsUsed", 0)
                        if (i == 0) {
                            if (finalAllOrderSplitData[i].id == "") {
                                mChecksObject.put("id", "0")
                            } else {
                                mChecksObject.put("id", finalAllOrderSplitData[i].id)
                            }
                        } else {
                            if (finalAllOrderSplitData[i].id == "") {
                                mChecksObject.put("id", "0")
                            } else {
                                mChecksObject.put("id", finalAllOrderSplitData[i].id)
                            }
                        }
                        val mOrderItemsJsonArray = JSONArray()
                        var discountAmount = 0.00
                        var subTotal: Double
                        var price_total = 0.00
                        var modifier_price_total = 0.00
                        for (newOrder in 0 until finalAllOrderSplitData[i].orderDetails.size) {
                            if (finalAllOrderSplitData[i].orderDetails[newOrder].isCombo || finalAllOrderSplitData[i].orderDetails[newOrder].isBogo) {
                                val discountApplied: Boolean =
                                    finalAllOrderSplitData[i].orderDetails[newOrder].discountType != ""
                                if (!discountApplied) {
                                    price_total += (finalAllOrderSplitData[i].orderDetails[newOrder].quantity * finalAllOrderSplitData[i].orderDetails[newOrder].unitPrice)
                                    for (m in 0 until finalAllOrderSplitData[i].orderDetails[newOrder].modifiersList.size) {
                                        modifier_price_total += (finalAllOrderSplitData[i].orderDetails[newOrder].modifiersList[m].modifierTotalPrice!!)
                                    }
                                    for (m in 0 until finalAllOrderSplitData[i].orderDetails[newOrder].specialRequestList.size) {
                                        modifier_price_total += (finalAllOrderSplitData[i].orderDetails[newOrder].specialRequestList[m].requestPrice!!)
                                    }
                                }
                            } else {
                                price_total += (finalAllOrderSplitData[i].orderDetails[newOrder].quantity * finalAllOrderSplitData[i].orderDetails[newOrder].unitPrice)
                                for (m in 0 until finalAllOrderSplitData[i].orderDetails[newOrder].modifiersList.size) {
                                    modifier_price_total += (finalAllOrderSplitData[i].orderDetails[newOrder].modifiersList[m].modifierTotalPrice!!)
                                }
                                for (m in 0 until finalAllOrderSplitData[i].orderDetails[newOrder].specialRequestList.size) {
                                    modifier_price_total += (finalAllOrderSplitData[i].orderDetails[newOrder].specialRequestList[m].requestPrice!!)
                                }
                            }
                        }
                        subTotal = price_total + modifier_price_total
                        for (m in 0 until finalAllOrderSplitData[i].orderDetails.size) {
                            if (finalAllOrderSplitData[i].orderDetails[m].discountType == "") {
                                discountAmount += 0.00
                            } else {
                                discountAmount += finalAllOrderSplitData[i].orderDetails[m].discountAmount
                            }
                            val mItemsObject = JSONObject()
                            mItemsObject.put(
                                "discountAmount",
                                finalAllOrderSplitData[i].orderDetails[m].discountAmount
                            )
                            mItemsObject.put(
                                "discountApplicable",
                                finalAllOrderSplitData[i].orderDetails[m].discountApplicable
                            )
                            mItemsObject.put(
                                "discountName",
                                finalAllOrderSplitData[i].orderDetails[m].discountName
                            )
                            mItemsObject.put(
                                "discountType",
                                finalAllOrderSplitData[i].orderDetails[m].discountType
                            )
                            mItemsObject.put(
                                "discountValue",
                                finalAllOrderSplitData[i].orderDetails[m].discountValue
                            )
                            mItemsObject.put("id", finalAllOrderSplitData[i].orderDetails[m].id)
                            mItemsObject.put(
                                "isBogo",
                                finalAllOrderSplitData[i].orderDetails[m].isBogo
                            )
                            mItemsObject.put(
                                "isCombo",
                                finalAllOrderSplitData[i].orderDetails[m].isCombo
                            )
                            mItemsObject.put(
                                "itemId",
                                finalAllOrderSplitData[i].orderDetails[m].itemId
                            )
                            mItemsObject.put(
                                "itemName",
                                finalAllOrderSplitData[i].orderDetails[m].itemName
                            )
                            mItemsObject.put(
                                "itemStatus",
                                finalAllOrderSplitData[i].orderDetails[m].itemStatus
                            )
                            mItemsObject.put(
                                "itemType",
                                finalAllOrderSplitData[i].orderDetails[m].itemType
                            )
                            val mModifierJson = JSONArray()
                            if (finalAllOrderSplitData[i].orderDetails[m].modifiersList.size > 0) {
                                for (k in 0 until finalAllOrderSplitData[i].orderDetails[m].modifiersList.size) {
                                    val mModifierObj = JSONObject()
                                    mModifierObj.put(
                                        "modifierId",
                                        finalAllOrderSplitData[i].orderDetails[m].modifiersList[k].modifierId
                                    )
                                    mModifierObj.put(
                                        "modifierName",
                                        finalAllOrderSplitData[i].orderDetails[m].modifiersList[k].modifierName
                                    )
                                    mModifierObj.put("modifierQty", 1)
                                    mModifierObj.put(
                                        "modifierUnitPrice",
                                        finalAllOrderSplitData[i].orderDetails[m].modifiersList[k].modifierUnitPrice
                                    )
                                    mModifierObj.put(
                                        "modifierTotalPrice",
                                        finalAllOrderSplitData[i].orderDetails[m].modifiersList[k].modifierTotalPrice
                                    )
                                    mModifierObj.put(
                                        "modifierGroupId",
                                        finalAllOrderSplitData[i].orderDetails[m].modifiersList[k].modifierGroupId
                                    )
                                    mModifierJson.put(mModifierObj)
                                }
                            }
                            mItemsObject.put("modifiersList", mModifierJson)
                            mItemsObject.put(
                                "quantity",
                                finalAllOrderSplitData[i].orderDetails[m].quantity
                            )
                            mItemsObject.put(
                                "removeQuantity",
                                finalAllOrderSplitData[i].orderDetails[m].removeQuantity
                            )
                            mItemsObject.put(
                                "removeStatus",
                                finalAllOrderSplitData[i].orderDetails[m].removeStatus
                            )
                            val mSpecialReqJson = JSONArray()
                            if (finalAllOrderSplitData[i].orderDetails[m].specialRequestList.size > 0) {
                                for (k in 0 until finalAllOrderSplitData[i].orderDetails[m].specialRequestList.size) {
                                    val mSplRequestsObj = JSONObject()
                                    mSplRequestsObj.put(
                                        "name",
                                        finalAllOrderSplitData[i].orderDetails[m].specialRequestList[k].name
                                    )
                                    mSplRequestsObj.put(
                                        "requestPrice",
                                        finalAllOrderSplitData[i].orderDetails[m].specialRequestList[k].requestPrice
                                    )
                                    mSpecialReqJson.put(mSplRequestsObj)
                                }
                            }
                            mItemsObject.put("specialRequestList", mSpecialReqJson)
                            mItemsObject.put(
                                "totalPrice",
                                finalAllOrderSplitData[i].orderDetails[m].totalPrice
                            )
                            mItemsObject.put(
                                "unitPrice",
                                finalAllOrderSplitData[i].orderDetails[m].unitPrice
                            )
                            mItemsObject.put(
                                "voidQuantity",
                                finalAllOrderSplitData[i].orderDetails[m].voidQuantity
                            )
                            mItemsObject.put(
                                "voidStatus",
                                finalAllOrderSplitData[i].orderDetails[m].voidStatus
                            )
                            mOrderItemsJsonArray.put(mItemsObject)
                            total_tax += finalAllOrderSplitData[i].orderDetails[m].itemTaxAmount
                        }
                        mChecksObject.put("orderItems", mOrderItemsJsonArray)
                        mChecksObject.put("discountAmount", discountAmount)
                        mChecksObject.put("orderNumber", finalAllOrderSplitData[i].orderNumber)
                        mChecksObject.put("status", 1)
                        mChecksObject.put("subTotal", subTotal)
                        mChecksObject.put("tableNumber", finalAllOrderSplitData[i].tableNumber)
                        mChecksObject.put("taxAmount", total_tax)
                        mChecksObject.put("tipAmount", finalAllOrderSplitData[i].tipAmount)
                        mChecksObject.put(
                            "totalAmount",
                            subTotal + total_tax + finalAllOrderSplitData[i].tipAmount - discountAmount
                        )
                        mChecksObject.put("haveCustomer", finalAllOrderSplitData[i].haveCustomer)
                        if (finalAllOrderSplitData[i].haveCustomer) {
                            val customerDetailsObj = JSONObject()
                            customerDetailsObj.put(
                                "id",
                                finalAllOrderSplitData[i].customerDetails.id
                            )
                            mChecksObject.put("customerDetails", customerDetailsObj)
                        }
                        mMainChecksJsonArray.put(mChecksObject)
                    }
                }
            }

            mMainJsonObject.put("checks", mMainChecksJsonArray)
            mMainJsonObject.put("userId", mEmployeeId)
            mMainJsonObject.put("restaurantId", restaurantId)

            val jObject = JsonParser.parseString(mMainJsonObject.toString()).asJsonObject
            Log.d("mSplitJsonObject", jObject.toString())
            splitOrderAPI(jObject)
        }
    }

    private fun splitOrderAPI(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.splitOrderSetUpApi(json_obj)
        call.enqueue(object : Callback<GiftCardBalanceResponse> {
            override fun onFailure(call: Call<GiftCardBalanceResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("SplitOrderAPI error", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<GiftCardBalanceResponse>,
                response: Response<GiftCardBalanceResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e(
                        "SplitOrderAPI success",
                        response!!.body()!!.responseStatus!!.toString()
                    )
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@OrderSplitNewActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                        val mainOrderId = response.body()!!.mainOrderId!!
                        val intent =
                            Intent(
                                this@OrderSplitNewActivity,
                                PaymentScreenActivity::class.java
                            )
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.putExtra("from_screen", "split_order")
                        intent.putExtra("mOrderId", mainOrderId)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@OrderSplitNewActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getTaxRates(finalObject: JsonObject) {
        taxRatesList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getTaxRates(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Log.e("tax_rate error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val taxRates = resp.taxRates!!
                        if (taxRates.size > 0) {
                            for (i in 0 until taxRates.size) {
                                taxRatesList.add(taxRates[i])
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }
}