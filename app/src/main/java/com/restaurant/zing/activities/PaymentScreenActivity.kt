package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bolt.consumersdk.CCConsumer
import com.bolt.consumersdk.CCConsumerTokenCallback
import com.bolt.consumersdk.domain.CCConsumerAccount
import com.bolt.consumersdk.domain.CCConsumerCardInfo
import com.bolt.consumersdk.domain.CCConsumerError
import com.bolt.consumersdk.enums.CCConsumerMaskFormat
import com.bolt.consumersdk.views.CCConsumerCreditCardNumberEditText
import com.bolt.consumersdk.views.CCConsumerCvvEditText
import com.bolt.consumersdk.views.CCConsumerExpirationDateEditText
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.ChecksBreakdownPaymentListAdapter
import com.restaurant.zing.adapters.VoidItemsPaymentListAdapter
import com.restaurant.zing.adapters.VoidReasonsListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.R
import com.restaurant.zing.helpers.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.abs
import kotlin.math.ceil
import kotlin.math.round

@SuppressLint("SetTextI18n", "ClickableViewAccessibility")
class PaymentScreenActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var tv_order_number: TextView
    private lateinit var tv_table_number: TextView
    private lateinit var cv_home: CardView
    private lateinit var cv_dinning: CardView
    private lateinit var ll_view_items: LinearLayout
    private lateinit var ll_svc_charge: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var rv_bills: RecyclerView
    private lateinit var lmBills: LinearLayoutManager
    private lateinit var orderedListAdapter: ListAdapter
    private lateinit var snapHelper: PagerSnapHelper
    private lateinit var et_balance_due: EditText
    private lateinit var et_amount_tendered: EditText
    private lateinit var et_tip_amount: EditText
    private lateinit var tv_payment_cash: TextView
    private lateinit var tv_payment_credit: TextView
    private lateinit var tv_payment_gift_card: TextView
    private lateinit var tv_payment_other: TextView
    private lateinit var tv_payment_split: TextView
    private lateinit var cv_one: CardView
    private lateinit var cv_two: CardView
    private lateinit var cv_three: CardView
    private lateinit var cv_four: CardView
    private lateinit var cv_five: CardView
    private lateinit var cv_six: CardView
    private lateinit var cv_seven: CardView
    private lateinit var cv_eight: CardView
    private lateinit var cv_nine: CardView
    private lateinit var cv_zero: CardView
    private lateinit var cv_double_zero: CardView
    private lateinit var ll_no_round_off: LinearLayout
    private lateinit var ll_round_off_one: LinearLayout
    private lateinit var ll_round_off_two: LinearLayout
    private lateinit var tv_no_round_off: TextView
    private lateinit var tv_round_off_one: TextView
    private lateinit var tv_round_off_two: TextView
    private lateinit var cv_delete: CardView
    private lateinit var cv_clear: CardView
    private lateinit var tv_customer_name: TextView
    private lateinit var tv_customer_credits: TextView
    private lateinit var ll_insufficient_credits: LinearLayout
    private lateinit var ll_sufficient_credits: LinearLayout
    private lateinit var tv_balance_after_payment: TextView
    private lateinit var tv_add_new_customer: TextView
    private lateinit var rv_user_search: RecyclerView
    private lateinit var ll_user_empty: LinearLayout
    private lateinit var tv_ok: TextView
    private lateinit var customer_search_dialog: Dialog
    private lateinit var loyalty_points_dialog: Dialog
    private lateinit var add_customer_dialog: Dialog
    private lateinit var et_first_name: EditText
    private lateinit var et_last_name: EditText
    private lateinit var et_phone_number: EditText
    private lateinit var et_email: EditText
    private lateinit var loading_dialog: Dialog
    private lateinit var dialog_cash: Dialog
    private lateinit var dialog_credit_card: Dialog
    private lateinit var dialog_gift_card: Dialog
    private lateinit var dialog_others: Dialog
    private lateinit var dialog_customer_details: Dialog
    private lateinit var dialog_success: Dialog
    private lateinit var ll_numpad: LinearLayout
    private lateinit var tv_gift_card_balance: TextView
    private lateinit var ll_gift_card_balance_details: LinearLayout
    private lateinit var ll_customer_details: LinearLayout
    private lateinit var cb_use_loyalty_points: CheckBox
    private var orderItemsList = ArrayList<OrderItemsResponse>()
    private var orderedList = ArrayList<OrderDetailsResponse>()
    private lateinit var splitArrayList: ArrayList<SplitOrderSetupModelNew1>

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var getCashDrawer: java.util.HashMap<String, String>

    private var mEmployeeId = ""
    private var restaurantId = ""
    private var mEmpJobId = ""
    private var from_screen = ""
    private var cashDrawerId = ""
    private var isTenderAmountSelected = true
    private var isPhoneFeildSelected = true
    private var tenderedAmountStr = ""
    private var tipAmountStr = ""
    private var tenderedAmount = 0.00
    private var tipAmount = 0.00
    private var tipAmountCurrentCheck = 0.00
    private var paidAmount = 0.00
    private var due = 0.00
    private var mOrderId = ""
    private var str_one = "1"
    private var str_two = "2"
    private var str_three = "3"
    private var str_four = "4"
    private var str_five = "5"
    private var str_six = "6"
    private var str_seven = "7"
    private var str_eight = "8"
    private var str_nine = "9"
    private var str_zero = "0"
    private var str_double_zero = "00"
    private var dueAmount = 0.00
    private var totalAmount = 0.00
    private var posOrderId = ""
    private val TYPE_CASH = 0
    private val TYPE_CARD = 1
    private val TYPE_GIFT_CARD = 2
    private val TYPE_PREPAID_CARD = 3
    private var checkNumber = 0
    private var isRequestedReceipt = false
    private var isCashSelected = false
    private var isPaymentStatus = false
    private var isGiftcardSelected = false
    private var paymentStatus = 0
    private var checkPos = -1
    private var mSelectedIndex = -1
    private var customerId = ""
    private var customerFirstName = ""
    private var customerLastName = ""
    private var customerEmail = ""
    private var customerPhone = ""
    private var no_round_off = 0.00
    private var round_off_one = 0.00
    private var round_off_two = 0.00
    private var deviceToken = RestaurantId.DEVICE_TOKEN
    private var preCardNo = 0L
    private var lastPaidPosition = -1
    private var gift_card_balance = 0.00
    private var customer_credits = 0.00
    private var currencyType = ""
    private var availableLoyaltyPoints = 0.00
    private var useLoyaltyPoints = false

    private lateinit var tv_currency_balance_due: TextView
    private lateinit var tv_currency_amount_header: TextView
    private lateinit var tv_currency_tip_amount: TextView

    private lateinit var void_reasonsList: ArrayList<VoidReasonsResponse>
    private lateinit var mVoidDialog: Dialog

    /* "isTenderAmountSelected"
    * true = amount entered will be in tendered amount field (et_amount_tendered)
    * false = amount entered will be in tips field (et_tip_amount)
    * boolean value changes as edit text focus changes*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_screen)

        try {
            mOrderId = intent.getStringExtra("mOrderId")!!
            from_screen = intent.getStringExtra("from_screen")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()
        tv_currency_balance_due.text = "($currencyType)"
        tv_currency_amount_header.text = "($currencyType)"
        tv_currency_tip_amount.text = "($currencyType)"
        orderFromTable()
        getVoidReasons()
        et_amount_tendered.showSoftInputOnFocus = false
        et_tip_amount.showSoftInputOnFocus = false
        et_amount_tendered.onFocusChangeListener =
            View.OnFocusChangeListener { _, _ -> isTenderAmountSelected = true }
        et_tip_amount.onFocusChangeListener =
            View.OnFocusChangeListener { _, _ -> isTenderAmountSelected = false }
        et_amount_tendered.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    et_amount_tendered.setSelection(s.length)
                }
            }
        })
        et_tip_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    et_tip_amount.setSelection(s.length)
                    val tip = et_tip_amount.text.toString().trim()
                    et_balance_due.setText("%.2f".format(dueAmount - tipAmountCurrentCheck + tip.toDouble()))
                    //dynamicUpdateValues(dueAmount - tipAmountCurrentCheck + tip.toDouble())
                    /*val amt = dueAmount - tipAmountCurrentCheck + tip.toDouble()
                    no_round_off = amt
                    round_off_one = ceil(amt)
                    round_off_two = round((amt + 5) / 10) * 10.0
                    tv_no_round_off.text = "%.2f".format(no_round_off)
                    tv_round_off_one.text = "%.2f".format(round_off_one)
                    tv_round_off_two.text = "%.2f".format(round_off_two)*/
                }
            }
        })
        et_amount_tendered.setOnClickListener {
            et_amount_tendered.setSelection(et_amount_tendered.text.trim().length)
        }
        et_tip_amount.setOnClickListener {
            et_tip_amount.setSelection(et_tip_amount.text.trim().length)
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        cv_home.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
        cv_dinning.setOnClickListener {
            Toast.makeText(this, "Moving to Dinning room", Toast.LENGTH_SHORT).show()
        }
        ll_view_items.setOnClickListener {
            val intent = Intent(this, ViewItemsActivity::class.java)
            intent.putExtra("payment_status", "empty")
            intent.putExtra("orderItemsList", orderedList)
            intent.putExtra("checkNumber", checkNumber)
            startActivity(intent)
        }
        tv_payment_cash.setOnClickListener {
            val finalTotalAmount: Double
            paidAmount = tenderedAmount
            val tips = et_tip_amount.text.toString().trim()
            finalTotalAmount = if (useLoyaltyPoints) {
                if (tips.toDouble() == tipAmountCurrentCheck) {
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck)
                } else {
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck + tips.toDouble())
                }
            } else {
                if (tips.toDouble() == tipAmountCurrentCheck) {
                    (dueAmount - tipAmountCurrentCheck)
                } else {
                    (dueAmount - tipAmountCurrentCheck + tips.toDouble())
                }
            }
            if (et_amount_tendered.text.toString()
                    .trim() == "" || et_amount_tendered.text.toString().trim() == "0.00"
            ) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (paidAmount < finalTotalAmount) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Amount should be more than Balance due!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (paidAmount >= finalTotalAmount) {
                isCashSelected = true
                isGiftcardSelected = false
                cashDialog()
            }
        }
        tv_payment_credit.setOnClickListener {
            val finalTotalAmount: Double
            paidAmount = tenderedAmount
            val tips = et_tip_amount.text.toString().trim()
            finalTotalAmount = if (useLoyaltyPoints) {
                if (tips.toDouble() == tipAmountCurrentCheck) {
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck)
                } else {
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck + tips.toDouble())
                }
            } else {
                if (tips.toDouble() == tipAmountCurrentCheck) {
                    (dueAmount - tipAmountCurrentCheck)
                } else {
                    (dueAmount - tipAmountCurrentCheck + tips.toDouble())
                }
            }
            if (et_amount_tendered.text.toString()
                    .trim() == "" || et_amount_tendered.text.toString().trim() == "0.00"
            ) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (paidAmount < finalTotalAmount) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Amount should be more than Balance due!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (paidAmount >= finalTotalAmount) {
                creditCardDialog()
            }
        }
        tv_payment_gift_card.setOnClickListener {
            val finalTotalAmount: Double
            paidAmount = tenderedAmount
            val tips = et_tip_amount.text.toString().trim()
            finalTotalAmount = if (useLoyaltyPoints) {
                if (tips.toDouble() == tipAmountCurrentCheck) {
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck)
                } else {
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck + tips.toDouble())
                }
            } else {
                if (tips.toDouble() == tipAmountCurrentCheck) {
                    (dueAmount - tipAmountCurrentCheck)
                } else {
                    (dueAmount - tipAmountCurrentCheck + tips.toDouble())
                }
            }
            if (et_amount_tendered.text.toString()
                    .trim() == "" || et_amount_tendered.text.toString().trim() == "0.00"
            ) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (paidAmount < finalTotalAmount) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Amount should be more than Balance due!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (paidAmount >= finalTotalAmount) {
                isGiftcardSelected = true
                isCashSelected = false
                giftCardDialog()
            }
        }
        tv_payment_other.setOnClickListener {
            if (et_amount_tendered.text.toString()
                    .trim() == "" || et_amount_tendered.text.toString().trim() == "0.00"
            ) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                otherPaymentsDialog()
            }
        }
        tv_payment_split.setOnClickListener {
            splitArrayList.clear()
            try {
                for (i in 0 until orderedList.size) {
                    var orderItemsModel: OrderItemsModel
                    val orderItemsModelList = ArrayList<OrderItemsModel>()
                    for (j in 0 until orderedList[i].orderItems!!.size) {
                        val discountValue =
                            if (orderedList[i].orderItems!![j].discountValue.toString() == "null") {
                                0.00
                            } else {
                                orderedList[i].orderItems!![j].discountValue!!
                            }
                        orderItemsModel = OrderItemsModel(
                            orderedList[i].orderItems!![j].discountAmount!!,
                            orderedList[i].orderItems!![j].discountType!!,
                            orderedList[i].orderItems!![j].discountId.toString(),
                            orderedList[i].orderItems!![j].discountLevel.toString(),
                            orderedList[i].orderItems!![j].discountName.toString(),
                            discountValue,
                            orderedList[i].orderItems!![j].itemId!!,
                            orderedList[i].orderItems!![j].id!!,
                            orderedList[i].orderItems!![j].menuGroupId.toString(),
                            orderedList[i].orderItems!![j].menuId.toString(),
                            orderedList[i].orderItems!![j].itemName!!,
                            orderedList[i].orderItems!![j].itemType!!,
                            orderedList[i].orderItems!![j].itemStatus!!,
                            orderedList[i].orderItems!![j].quantity!!,
                            orderedList[i].orderItems!![j].totalPrice!!,
                            orderedList[i].orderItems!![j].unitPrice!!,
                            orderedList[i].orderItems!![j].isBogo!!,
                            orderedList[i].orderItems!![j].discountApplicable,
                            orderedList[i].orderItems!![j].isCombo!!,
                            orderedList[i].orderItems!![j].removeStatus!!,
                            orderedList[i].orderItems!![j].removeQuantity!!,
                            orderedList[i].orderItems!![j].voidStatus!!,
                            orderedList[i].orderItems!![j].voidQuantity!!,
                            orderedList[i].orderItems!![j].comboId.toString(),
                            orderedList[i].orderItems!![j].bogoId.toString(),
                            orderedList[i].orderItems!![j].comboUniqueId.toString(),
                            orderedList[i].orderItems!![j].bogoUniqueId.toString(),
                            orderedList[i].orderItems!![j].specialRequestList!!,
                            orderedList[i].orderItems!![j].modifiersList!!,
                            orderedList[i].orderItems!![j].itemTaxAmount!!,
                            orderedList[i].orderItems!![j].diningOptionTaxException!!,
                            orderedList[i].orderItems!![j].diningTaxOption!!,
                            orderedList[i].orderItems!![j].taxIncludeOption!!,
                            orderedList[i].orderItems!![j].taxesList!!,
                            isSelected = false
                        )
                        orderItemsModelList.add(orderItemsModel)
                    }
                    var customerDetails: CustomerDataResponse
                    val discountValue = if (orderedList[i].discountValue.toString() == "null") {
                        0.00
                    } else {
                        orderedList[i].discountValue!!
                    }
                    if (orderedList[i].haveCustomer!!) {
                        customerDetails = orderedList[i].customerDetails!!
                    } else {
                        customerDetails = CustomerDataResponse()
                    }
                    val splitOrderSetupModelNew1 = SplitOrderSetupModelNew1(
                        orderedList[i].checkNumber!!,
                        orderedList[i].closedOn.toString(),
                        orderedList[i].createdBy!!,
                        orderedList[i].createdByName!!,
                        orderedList[i].createdOn!!,
                        orderedList[i].creditsUsed!!,
                        orderedList[i].dineInBehaviour!!,
                        orderedList[i].dueAmount!!,
                        orderedList[i].dineInOption!!,
                        orderedList[i].dineInOptionName!!,
                        orderedList[i].discountAmount!!,
                        orderedList[i].haveCustomer!!,
                        orderedList[i].id!!,
                        orderedList[i].status!!,
                        orderedList[i].restaurantId!!,
                        orderedList[i].revenueCenterId!!,
                        orderedList[i].discountLevel.toString(),
                        orderedList[i].discountId.toString(),
                        orderedList[i].discountName.toString(),
                        orderedList[i].discountType.toString(),
                        discountValue,
                        orderedList[i].serviceAreaId!!,
                        orderedList[i].orderNumber!!,
                        orderedList[i].subTotal!!,
                        orderedList[i].tableNumber!!,
                        orderedList[i].taxAmount!!,
                        orderedList[i].tipAmount!!,
                        orderedList[i].totalAmount!!,
                        customerDetails,
                        orderItemsModelList
                    )
                    splitArrayList.add(splitOrderSetupModelNew1)
                }
                val intent = Intent(this@PaymentScreenActivity, OrderSplitNewActivity::class.java)
                intent.putExtra("splitArrayNew", splitArrayList)
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        cv_one.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_one
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_one
                    } else {
                        tenderedAmountStr += str_one
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_one
                } else {
                    tipAmountStr += str_one
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_two.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_two
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_two
                    } else {
                        tenderedAmountStr += str_two
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_two
                } else {
                    tipAmountStr += str_two
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_three.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_three
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_three
                    } else {
                        tenderedAmountStr += str_three
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_three
                } else {
                    tipAmountStr += str_three
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_four.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_four
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_four
                    } else {
                        tenderedAmountStr += str_four
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_four
                } else {
                    tipAmountStr += str_four
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_five.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_five
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_five
                    } else {
                        tenderedAmountStr += str_five
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_five
                } else {
                    tipAmountStr += str_five
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_six.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_six
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_six
                    } else {
                        tenderedAmountStr += str_six
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_six
                } else {
                    tipAmountStr += str_six
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_seven.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_seven
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_seven
                    } else {
                        tenderedAmountStr += str_seven
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_seven
                } else {
                    tipAmountStr += str_seven
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_eight.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_eight
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_eight
                    } else {
                        tenderedAmountStr += str_eight
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_eight
                } else {
                    tipAmountStr += str_eight
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_nine.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_nine
                } else {
                    if (tenderedAmountStr == no_round_off.toString() || tenderedAmountStr == round_off_one.toString() || tenderedAmountStr == round_off_two.toString()) {
                        tenderedAmountStr = str_nine
                    } else {
                        tenderedAmountStr += str_nine
                    }
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_nine
                } else {
                    tipAmountStr += str_nine
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_zero.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_zero
                } else {
                    tenderedAmountStr += str_zero
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_zero
                } else {
                    tipAmountStr += str_zero
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_double_zero.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr == getString(R.string.default_value_0)) {
                    tenderedAmountStr = str_double_zero
                } else {
                    tenderedAmountStr += str_double_zero
                }
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr == getString(R.string.default_value_0)) {
                    tipAmountStr = str_double_zero
                } else {
                    tipAmountStr += str_double_zero
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_delete.setOnClickListener {
            if (isTenderAmountSelected) {
                if (tenderedAmountStr != getString(R.string.default_value_0)) {
                    if (tenderedAmountStr != "") {
                        tenderedAmountStr = tenderedAmountStr.dropLast(1)
                        if (tenderedAmountStr == "") {
                            tenderedAmountStr = getString(R.string.default_value_0)
                        }
                    } else {
                        tenderedAmountStr = getString(R.string.default_value_0)
                    }
                }
                Log.d("tenderedAmountStr", tenderedAmountStr);
                tenderedAmount = tenderedAmountStr.toDouble()
                tenderedAmount /= 100
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                if (tipAmountStr != getString(R.string.default_value_0)) {
                    if (tipAmountStr != "") {
                        tipAmountStr = tipAmountStr.dropLast(1)
                        if (tipAmountStr == "") {
                            tipAmountStr = getString(R.string.default_value_0)
                        }
                    } else {
                        tipAmountStr = getString(R.string.default_value_0)
                    }
                }
                tipAmount = tipAmountStr.toDouble()
                tipAmount /= 100
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        cv_clear.setOnClickListener {
            if (isTenderAmountSelected) {
                tenderedAmountStr = getString(R.string.default_value_0)
                tenderedAmount = tenderedAmountStr.toDouble()
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            } else {
                tipAmountStr = getString(R.string.default_value_0)
                tipAmount = tipAmountStr.toDouble()
                et_tip_amount.setText("%.2f".format(tipAmount))
            }
        }
        ll_no_round_off.setOnClickListener {
            if (isTenderAmountSelected) {
                tenderedAmountStr = no_round_off.toString()
                tenderedAmount = tenderedAmountStr.toDouble()
                et_amount_tendered.setText("%.2f".format(no_round_off))
            }
        }
        ll_round_off_one.setOnClickListener {
            if (isTenderAmountSelected) {
                tenderedAmountStr = round_off_one.toString()
                tenderedAmount = tenderedAmountStr.toDouble()
                et_amount_tendered.setText("%.2f".format(round_off_one))
            }
        }
        ll_round_off_two.setOnClickListener {
            if (isTenderAmountSelected) {
                tenderedAmountStr = round_off_two.toString()
                tenderedAmount = tenderedAmountStr.toDouble()
                et_amount_tendered.setText("%.2f".format(round_off_two))
            }
        }
        tv_add_new_customer.setOnClickListener {
            searchUserDialog()
        }
        cb_use_loyalty_points.setOnCheckedChangeListener { buttonView, isChecked ->
            useLoyaltyPoints = isChecked
            if (useLoyaltyPoints) {
                if (dueAmount < availableLoyaltyPoints) {
                    dynamicUpdateValues(0.00)
                    tenderedAmountStr = getString(R.string.default_value_0)
                    tenderedAmount = tenderedAmountStr.toDouble()
                    et_amount_tendered.setText("%.2f".format(tenderedAmount))
                    showRedeemPointsConfirmDialog()
                } else {
                    dynamicUpdateValues(dueAmount - availableLoyaltyPoints)
                    tenderedAmountStr = getString(R.string.default_value_0)
                    tenderedAmount = tenderedAmountStr.toDouble()
                    et_amount_tendered.setText("%.2f".format(tenderedAmount))
                }
            } else {
                dynamicUpdateValues(dueAmount)
                tenderedAmountStr = getString(R.string.default_value_0)
                tenderedAmount = tenderedAmountStr.toDouble()
                et_amount_tendered.setText("%.2f".format(tenderedAmount))
            }
        }
        rv_bills.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val centerView = snapHelper.findSnapView(lmBills)
                    val pos = lmBills.getPosition(centerView!!)
                    checkPos = pos
                    val data = orderedListAdapter.getItemAtPosition(pos)
                    orderItemsList = data.orderItems!!
                    dueAmount = data.dueAmount!!
                    dynamicUpdateValues(dueAmount)
                    /*no_round_off = dueAmount
                    round_off_one = ceil(dueAmount)
                    round_off_two = round((dueAmount + 5) / 10) * 10.0
                    tv_no_round_off.text = "%.2f".format(no_round_off)
                    tv_round_off_one.text = "%.2f".format(round_off_one)
                    tv_round_off_two.text = "%.2f".format(round_off_two)*/
                    tipAmount = data.tipAmount!!
                    totalAmount = data.totalAmount!!
                    tipAmountCurrentCheck = data.tipAmount
                    tv_order_number.text = getString(R.string.order) + " #${data.orderNumber!!}"
                    tv_table_number.text =
                        getString(R.string.table) + " ${data.tableNumber.toString()}"
                    val tip = et_tip_amount.text.toString().trim()
                    et_balance_due.setText("%.2f".format(dueAmount - tipAmountCurrentCheck + tip.toDouble()))
//                    et_balance_due.setText("%.2f".format(dueAmount))
                    et_amount_tendered.setText("")
                    et_tip_amount.setText("%.2f".format(tipAmount))
                    tipAmountStr = ("%.2f".format(tipAmount).toDouble() * 100).toInt().toString()
                    tenderedAmount = 0.00
                    tenderedAmountStr = getString(R.string.default_value_0)
                    posOrderId = data.id!!
                    println("data===" + data.status)
                    cb_use_loyalty_points.isChecked = false
                    if (data.haveCustomer!!) {
                        getUserLoyaltyPoints(data.customerDetails!!.id!!)
                        tv_add_new_customer.visibility = View.GONE
                        cb_use_loyalty_points.visibility = View.VISIBLE
                    } else {
                        tv_add_new_customer.visibility = View.VISIBLE
                        cb_use_loyalty_points.visibility = View.GONE
                    }
                    paymentStatus = data.status!!
                    if (paymentStatus == 3) {
                        tv_payment_cash.isEnabled = false
                        tv_payment_credit.isEnabled = false
                        tv_payment_gift_card.isEnabled = false
                        tv_payment_other.isEnabled = false
                        tv_payment_split.isEnabled = false
                        tv_add_new_customer.isEnabled = false
                        cb_use_loyalty_points.isEnabled = false
                    } else {
                        tv_payment_cash.isEnabled = true
                        tv_payment_credit.isEnabled = true
                        tv_payment_gift_card.isEnabled = true
                        tv_payment_other.isEnabled = true
                        tv_add_new_customer.isEnabled = true
                        if (orderedList.size == 1) {
                            if (data.orderItems.size == 1) {
                                tv_payment_split.isEnabled = false
                            }
                        } else {
                            tv_payment_split.isEnabled = true
                        }
                    }
                    Log.d("checkPos ScrollChanged", "$checkPos")
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (lmBills.findFirstVisibleItemPosition() == 0) {
                    val centerView = snapHelper.findSnapView(lmBills)
                    val pos = lmBills.getPosition(centerView!!)
                    val data = orderedListAdapter.getItemAtPosition(pos)
                    if (checkPos == -1) {
                        checkPos = pos
                    }
                    orderItemsList = data.orderItems!!
                    dueAmount = data.dueAmount!!
                    dynamicUpdateValues(dueAmount)
                    /*no_round_off = dueAmount
                    round_off_one = ceil(dueAmount)
                    round_off_two = round((dueAmount + 5) / 10) * 10.0
                    tv_no_round_off.text = "%.2f".format(no_round_off)
                    tv_round_off_one.text = "%.2f".format(round_off_one)
                    tv_round_off_two.text = "%.2f".format(round_off_two)*/
                    tipAmount = data.tipAmount!!
                    totalAmount = data.totalAmount!!
                    tipAmountCurrentCheck = data.tipAmount
                    tv_order_number.text = getString(R.string.order) + " #${data.orderNumber!!}"
                    tv_table_number.text =
                        getString(R.string.table) + " ${data.tableNumber.toString()}"
//                    et_balance_due.setText("%.2f".format(dueAmount))
                    val tip = et_tip_amount.text.toString().trim()
                    et_balance_due.setText("%.2f".format(dueAmount - tipAmountCurrentCheck + tip.toDouble()))
                    et_amount_tendered.setText("")
                    et_tip_amount.setText("%.2f".format(tipAmount))
                    tipAmountStr = ("%.2f".format(tipAmount).toDouble() * 100).toInt().toString()
                    tenderedAmount = 0.00
                    tenderedAmountStr = getString(R.string.default_value_0)
                    posOrderId = data.id!!
                    paymentStatus = data.status!!
                    cb_use_loyalty_points.isChecked = false
                    if (data.haveCustomer!!) {
                        getUserLoyaltyPoints(data.customerDetails!!.id!!)
                        tv_add_new_customer.visibility = View.GONE
                        cb_use_loyalty_points.visibility = View.VISIBLE
                    } else {
                        tv_add_new_customer.visibility = View.VISIBLE
                        cb_use_loyalty_points.visibility = View.GONE
                    }
                    if (paymentStatus == 3) {
                        tv_payment_cash.isEnabled = false
                        tv_payment_credit.isEnabled = false
                        tv_payment_gift_card.isEnabled = false
                        tv_payment_other.isEnabled = false
                        tv_payment_split.isEnabled = false
                        tv_add_new_customer.isEnabled = false
                        cb_use_loyalty_points.isEnabled = false
                    } else {
                        tv_payment_cash.isEnabled = true
                        tv_payment_credit.isEnabled = true
                        tv_payment_gift_card.isEnabled = true
                        tv_payment_other.isEnabled = true
                        tv_add_new_customer.isEnabled = true
                        if (orderedList.size == 1) {
                            if (data.orderItems.size == 1) {
                                tv_payment_split.isEnabled = false
                            }
                        } else {
                            tv_payment_split.isEnabled = true
                        }
                    }
                    Log.d("checkPos onScrolled", "$checkPos")
                }
            }
        })
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        getCashDrawer = sessionManager.getCashDrawer
        cashDrawerId = getCashDrawer[SessionManager.CASH_DRAWER_ID_KEY]!!
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        currencyType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        tv_order_number = findViewById(R.id.tv_order_number)
        tv_table_number = findViewById(R.id.tv_table_number)
        cv_home = findViewById(R.id.cv_home)
        cv_dinning = findViewById(R.id.cv_dinning)
        ll_view_items = findViewById(R.id.ll_view_items)
        ll_svc_charge = findViewById(R.id.ll_svc_charge)
        ll_switch_user = findViewById(R.id.ll_switch_user)

        et_balance_due = findViewById(R.id.et_balance_due)
        et_amount_tendered = findViewById(R.id.et_amount_tendered)
        et_tip_amount = findViewById(R.id.et_tip_amount)
        tv_payment_cash = findViewById(R.id.tv_payment_cash)
        tv_payment_credit = findViewById(R.id.tv_payment_credit)
        tv_payment_gift_card = findViewById(R.id.tv_payment_gift_card)
        tv_payment_other = findViewById(R.id.tv_payment_other)
        tv_payment_split = findViewById(R.id.tv_payment_split)
        cv_one = findViewById(R.id.cv_one)
        cv_two = findViewById(R.id.cv_two)
        cv_three = findViewById(R.id.cv_three)
        cv_four = findViewById(R.id.cv_four)
        cv_five = findViewById(R.id.cv_five)
        cv_six = findViewById(R.id.cv_six)
        cv_seven = findViewById(R.id.cv_seven)
        cv_eight = findViewById(R.id.cv_eight)
        cv_nine = findViewById(R.id.cv_nine)
        cv_zero = findViewById(R.id.cv_zero)
        cv_double_zero = findViewById(R.id.cv_double_zero)
        cv_delete = findViewById(R.id.cv_delete)
        cv_clear = findViewById(R.id.cv_clear)
        ll_no_round_off = findViewById(R.id.ll_no_round_off)
        ll_round_off_one = findViewById(R.id.ll_round_off_one)
        ll_round_off_two = findViewById(R.id.ll_round_off_two)
        tv_no_round_off = findViewById(R.id.tv_no_round_off)
        tv_round_off_one = findViewById(R.id.tv_round_off_one)
        tv_round_off_two = findViewById(R.id.tv_round_off_two)
        tv_add_new_customer = findViewById(R.id.tv_add_new_customer)
        tv_currency_balance_due = findViewById(R.id.tv_currency_balance_due)
        tv_currency_amount_header = findViewById(R.id.tv_currency_amount_header)
        tv_currency_tip_amount = findViewById(R.id.tv_currency_tip_amount)
        cb_use_loyalty_points = findViewById(R.id.cb_use_loyalty_points)

        rv_bills = findViewById(R.id.rv_bills)
        lmBills = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_bills.layoutManager = lmBills
        snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rv_bills)
        orderedList = ArrayList()
        splitArrayList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun orderFromTable() {
        try {
            val jObject = JSONObject()
            jObject.put("userId", mEmployeeId)
            jObject.put("restaurantId", restaurantId)
            jObject.put("orderId", mOrderId)
            val finalObject = JsonParser.parseString(jObject.toString()).asJsonObject
            Log.e("GETORDERFROMTABLE", finalObject.toString());
            getOrderFromTable(finalObject = finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getVoidReasons() {
        try {
            val obj = JSONObject()
            obj.put("userId", mEmployeeId)
            obj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(obj.toString()).asJsonObject
            getVoidItemsReasons(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun dynamicUpdateValues(amount: Double) {
        no_round_off = amount
        round_off_one = ceil(amount)
        round_off_two = round((amount + 5) / 10) * 10.0
        tv_no_round_off.text = "%.2f".format(no_round_off)
        tv_round_off_one.text = "%.2f".format(round_off_one)
        tv_round_off_two.text = "%.2f".format(round_off_two)
    }

    private fun showSuccessDialog() {
        dialog_success = Dialog(this)
        dialog_success.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.success_dialog, null)
        dialog_success.setContentView(view)
        dialog_success.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_success.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_success.setCanceledOnTouchOutside(false)
        if (lastPaidPosition != -1) {
            if (orderedList[lastPaidPosition].status == 3) {
                if (lastPaidPosition + 1 < orderedList.size) {
                    lastPaidPosition += 1
                    rv_bills.scrollToPosition(lastPaidPosition)
                    Log.d("checkPos after++", "$lastPaidPosition")
                    dueAmount = orderedList[lastPaidPosition].dueAmount!!
                    dynamicUpdateValues(dueAmount)
                    tipAmount = orderedList[lastPaidPosition].tipAmount!!
                    totalAmount = orderedList[lastPaidPosition].totalAmount!!
                    tipAmountCurrentCheck = orderedList[lastPaidPosition].tipAmount!!
                    posOrderId = orderedList[lastPaidPosition].id!!
                    et_amount_tendered.setText("")
                    et_tip_amount.setText("%.2f".format(tipAmount))
                    tipAmountStr = ("%.2f".format(tipAmount).toDouble() * 100).toInt().toString()
                    tenderedAmount = 0.00
                    tenderedAmountStr = getString(R.string.default_value_0)
                }
            }
        }
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        tv_ok.setOnClickListener {
            isCashSelected = false
            isPaymentStatus = false
            isGiftcardSelected = false
            dialog_success.dismiss()
        }
        dialog_success.show()
    }

    private fun cashDialog() {
        dialog_cash = Dialog(this)
        dialog_cash.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.cash_payment_dialog, null)
        dialog_cash.setContentView(view)
        dialog_cash.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_cash.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_cash.setCanceledOnTouchOutside(true)
        val tv_amount = view.findViewById(R.id.tv_amount) as TextView
        val tv_change_due = view.findViewById(R.id.tv_change_due) as TextView
        val tv_void = view.findViewById(R.id.tv_void) as TextView
        val tv_receipt = view.findViewById(R.id.tv_receipt) as TextView
        val tv_no_receipt = view.findViewById(R.id.tv_no_receipt) as TextView
        val tv_currency_amount_tendered =
            view.findViewById(R.id.tv_currency_amount_tendered) as TextView
        val tv_currency_change_due = view.findViewById(R.id.tv_currency_change_due) as TextView
        tv_currency_amount_tendered.text = currencyType
        tv_currency_change_due.text = currencyType
        tv_amount.text = "%.2f".format(tenderedAmount)
        val tips = et_tip_amount.text.toString().trim()
        if (tips.toDouble() == tipAmountCurrentCheck) {
            paidAmount = tenderedAmount
//            due = dueAmount - paidAmount
            if (useLoyaltyPoints) {
                due = dueAmount - availableLoyaltyPoints - paidAmount
            } else {
                due = dueAmount - paidAmount
            }
            tv_amount.text = "%.2f".format(paidAmount)
            when {
                tenderedAmount == 0.00 -> {
                    tv_change_due.text = "0.00"
                }
                useLoyaltyPoints -> {
                    if (paidAmount > (dueAmount - availableLoyaltyPoints)) {
                        tv_change_due.text = "%.2f".format(abs(due))
                    } else {
                        tv_change_due.text = "0.00"
                    }
                }
                paidAmount > dueAmount -> {
                    tv_change_due.text = "%.2f".format(abs(due))
                }
                else -> {
                    tv_change_due.text = "0.00"
                }
            }
            Log.d("change_due if ", due.toString())
        } else {
            paidAmount = tenderedAmount
            if (useLoyaltyPoints) {
                due =
                    (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck + tips.toDouble()) - paidAmount
            } else {
                due = (dueAmount - tipAmountCurrentCheck + tips.toDouble()) - paidAmount
            }

            tv_amount.text = "%.2f".format(paidAmount)
            when {
                tenderedAmount == 0.00 -> {
                    tv_change_due.text = "0.00"
                }
                useLoyaltyPoints -> {
                    if (paidAmount > (dueAmount - availableLoyaltyPoints)) {
                        tv_change_due.text = "%.2f".format(abs(due))
                    } else {
                        tv_change_due.text = "0.00"
                    }
                }
                paidAmount > (dueAmount - tipAmountCurrentCheck + tips.toDouble()) -> {
                    tv_change_due.text = "%.2f".format(abs(due))
                }
                else -> {
                    tv_change_due.text = "0.00"
                }
            }
            Log.d("change_due else ", due.toString())
        }

        tv_void.setOnClickListener {
            isCashSelected = false
            dialog_cash.dismiss()
        }
        tv_receipt.setOnClickListener {
            if (et_amount_tendered.text.toString().trim() == ""
                || et_amount_tendered.text.toString().trim() == "0.00"
            ) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                isRequestedReceipt = true
                val jObj = JSONObject()
                jObj.put("posOrderId", posOrderId)
                jObj.put("cashDrawerId", cashDrawerId)
                jObj.put("amount", "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble())
                jObj.put("amountTendered", tenderedAmount)
                jObj.put("tipAmount", tips.toDouble())
                jObj.put("changeAmount", "%.2f".format(abs(due)).toDouble())
                jObj.put("paymentType", TYPE_CASH)
                if (useLoyaltyPoints) {
                    jObj.put("useCredits", useLoyaltyPoints)
                    jObj.put("creditsAmount", availableLoyaltyPoints)
                }
                jObj.put("createdBy", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.e("PAYORDERREQUEST", "" + finalObject)
                payOrder(finalObject = finalObject, dialog = dialog_cash)
                dialog_cash.dismiss()
            }
        }
        tv_no_receipt.setOnClickListener {
            if (et_amount_tendered.text.toString().trim() == ""
                || et_amount_tendered.text.toString().trim() == "0.00"
            ) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter amount!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                isRequestedReceipt = false
                val jObj = JSONObject()
                jObj.put("posOrderId", posOrderId)
                jObj.put("cashDrawerId", cashDrawerId)
                jObj.put("amount", "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble())
                jObj.put("amountTendered", tenderedAmount)
                jObj.put("tipAmount", tips.toDouble())
                jObj.put("changeAmount", "%.2f".format(abs(due)).toDouble())
                jObj.put("paymentType", TYPE_CASH)
                if (useLoyaltyPoints) {
                    jObj.put("useCredits", useLoyaltyPoints)
                    jObj.put("creditsAmount", availableLoyaltyPoints)
                }
                jObj.put("createdBy", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.e("PAYORDERREQUEST", "" + finalObject)
                payOrder(finalObject = finalObject, dialog = dialog_cash)
                dialog_cash.dismiss()
            }
        }
        dialog_cash.show()
    }

    private fun creditCardDialog() {
        dialog_credit_card = Dialog(this)
        dialog_credit_card.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.credit_card_payment_dialog, null)
        dialog_credit_card.setContentView(view)
        dialog_credit_card.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_credit_card.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_credit_card.setCanceledOnTouchOutside(false)
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_key_in = view.findViewById(R.id.tv_key_in) as TextView
        val tips = et_tip_amount.text.toString().trim()
        tv_cancel.setOnClickListener {
            dialog_credit_card.dismiss()
        }
        tv_key_in.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("posOrderId", posOrderId)
            jObj.put("amount", "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble())
            jObj.put("amountTendered", tenderedAmount)
            jObj.put("tipAmount", tips.toDouble())
            jObj.put("changeAmount", "%.2f".format(abs(due)).toDouble())
            jObj.put("paymentType", TYPE_CARD)
            if (useLoyaltyPoints) {
                jObj.put("useCredits", useLoyaltyPoints)
                jObj.put("creditsAmount", availableLoyaltyPoints)
            }
            jObj.put("createdBy", mEmployeeId)
            jObj.put("restaurantId", restaurantId)
            cardDetailsDialog(jObj)
        }
        dialog_credit_card.show()
    }

    private fun cardDetailsDialog(jObj: JSONObject) {
        val card_details_dialog = Dialog(this)
        card_details_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.card_details_dialog, null)
        card_details_dialog.setContentView(view)
        card_details_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        card_details_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val et_credit_card_number =
            view.findViewById(R.id.et_credit_card_number) as CCConsumerCreditCardNumberEditText
        val et_credit_card_expiration_date =
            view.findViewById(R.id.et_credit_card_expiration_date) as CCConsumerExpirationDateEditText
        val et_credit_card_cvv = view.findViewById(R.id.et_credit_card_cvv) as CCConsumerCvvEditText
        card_details_dialog.setCanceledOnTouchOutside(false)
        val mCCConsumerCardInfo = CCConsumerCardInfo()
//        et_credit_card_number.ccConsumerMaskFormat = CCConsumerMaskFormat.LAST_FOUR
//        et_credit_card_number.ccConsumerMaskFormat = CCConsumerMaskFormat.FIRST_LAST_FOUR
        et_credit_card_number.ccConsumerMaskFormat = CCConsumerMaskFormat.CARD_MASK_LAST_FOUR
        et_credit_card_number.setCreditCardTextChangeListener {
            if (!et_credit_card_number.isCardNumberValid && et_credit_card_number.text!!.isNotEmpty()) {
                et_credit_card_number.error = getString(R.string.card_not_valid)
            } else {
                et_credit_card_number.error = null
            }
        }
        et_credit_card_expiration_date.setExpirationDateTextChangeListener {
            if (!et_credit_card_expiration_date.isExpirationDateValid && et_credit_card_expiration_date.text!!.isNotEmpty()
            ) {
                et_credit_card_expiration_date.error = getString(R.string.date_not_valid)
            } else {
                et_credit_card_expiration_date.error = null
            }
        }
        et_credit_card_cvv.setCvvTextChangeListener {
            if (!et_credit_card_cvv.isCvvCodeValid && et_credit_card_cvv.text!!.isNotEmpty()) {
                et_credit_card_cvv.error = getString(R.string.cvv_not_valid)
            } else {
                et_credit_card_cvv.error = null
            }
        }
        tv_cancel.setOnClickListener {
            card_details_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_credit_card_number.setCardNumberOnCardInfo(mCCConsumerCardInfo)
            et_credit_card_expiration_date.setExpirationDateOnCardInfo(mCCConsumerCardInfo)
            et_credit_card_cvv.setCvvCodeOnCardInfo(mCCConsumerCardInfo)
            if (!mCCConsumerCardInfo.isCardValid) {
                Toast.makeText(this, getString(R.string.card_invalid), Toast.LENGTH_SHORT).show()
            } else {
                loading_dialog.show()
                CCConsumer.getInstance().api.setEndPoint(RestaurantId.CARD_POINTE_END_POINT)
                CCConsumer.getInstance().api.generateAccountForCard(mCCConsumerCardInfo,
                    object : CCConsumerTokenCallback {
                        override fun onCCConsumerTokenResponseError(error: CCConsumerError) {
                            if (loading_dialog.isShowing)
                                loading_dialog.dismiss()
                            Toast.makeText(
                                this@PaymentScreenActivity,
                                error.responseMessage,
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onCCConsumerTokenResponse(account: CCConsumerAccount) {
                            val accountResponse = account
                            Log.d(
                                "accountResponse",
                                "${accountResponse.token}=====${mCCConsumerCardInfo.cardNumber}=====${mCCConsumerCardInfo.expirationDate}=====${mCCConsumerCardInfo.expirationDateWithoutSeparator}"
                            )
                            Toast.makeText(
                                this@PaymentScreenActivity,
                                accountResponse.token,
                                Toast.LENGTH_SHORT
                            ).show()
                            jObj.put("cardToken", accountResponse.token)
                            jObj.put("expiry", mCCConsumerCardInfo.expirationDateWithoutSeparator)
                            jObj.put("sourceLast4", mCCConsumerCardInfo.cardNumber)
                            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                            Log.d("PAYORDERREQUEST", finalObject.toString())
                            payOrder(finalObject = finalObject, dialog = card_details_dialog)
                            dialog_credit_card.dismiss()
                        }
                    })
            }
        }
        card_details_dialog.show()
    }

    private fun giftCardDialog() {
        var cardNumber = ""
        dialog_gift_card = Dialog(this)
        dialog_gift_card.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.gift_card_payment_dailog, null)
        dialog_gift_card.setContentView(view)
        dialog_gift_card.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_gift_card.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_gift_card.setCanceledOnTouchOutside(false)
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val cv_one = view.findViewById(R.id.cv_one) as CardView
        val cv_two = view.findViewById(R.id.cv_two) as CardView
        val cv_three = view.findViewById(R.id.cv_three) as CardView
        val cv_four = view.findViewById(R.id.cv_four) as CardView
        val cv_five = view.findViewById(R.id.cv_five) as CardView
        val cv_six = view.findViewById(R.id.cv_six) as CardView
        val cv_seven = view.findViewById(R.id.cv_seven) as CardView
        val cv_eight = view.findViewById(R.id.cv_eight) as CardView
        val cv_nine = view.findViewById(R.id.cv_nine) as CardView
        val cv_zero = view.findViewById(R.id.cv_zero) as CardView
        val cv_look_up = view.findViewById(R.id.cv_look_up) as CardView
        val cv_scan = view.findViewById(R.id.cv_scan) as CardView
        val cv_delete = view.findViewById(R.id.cv_delete) as CardView
        tv_gift_card_balance = view.findViewById(R.id.tv_gift_card_balance) as TextView
        val ll_gift_card_done = view.findViewById(R.id.ll_gift_card_done) as LinearLayout
        ll_gift_card_balance_details =
            view.findViewById(R.id.ll_gift_card_balance_details) as LinearLayout
        val tv_cash_drop_amount = view.findViewById(R.id.tv_cash_drop_amount) as TextView
        val tv_verify_gift_card = view.findViewById(R.id.tv_verify_gift_card) as TextView
        val tv_currency_amount = view.findViewById(R.id.tv_currency_amount) as TextView
        tv_currency_amount.text = currencyType
        ll_gift_card_balance_details.visibility = View.INVISIBLE
        val tips = et_tip_amount.text.toString().trim()
        iv_close.setOnClickListener {
            dialog_gift_card.dismiss()
        }
        tv_verify_gift_card.setOnClickListener {
            if (cardNumber == "") {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter card number!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val obj = JSONObject()
                obj.put("giftCardNo", cardNumber)
                obj.put("restaurantId", restaurantId)
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                getGiftCardBalance(jObject)
            }
        }
        tv_cash_drop_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ll_gift_card_balance_details.visibility = View.INVISIBLE
            }
        })
        cv_delete.setOnClickListener {
            if (cardNumber != "") {
                cardNumber = cardNumber.dropLast(1)
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_one.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_one
            } else {
                cardNumber += str_one
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_two.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_two
            } else {
                cardNumber += str_two
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_three.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_three
            } else {
                cardNumber += str_three
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_four.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_four
            } else {
                cardNumber += str_four
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_five.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_five
            } else {
                cardNumber += str_five
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_six.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_six
            } else {
                cardNumber += str_six
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_seven.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_seven
            } else {
                cardNumber += str_seven
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_eight.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_eight
            } else {
                cardNumber += str_eight
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_nine.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_nine
            } else {
                cardNumber += str_nine
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_zero.setOnClickListener {
            if (cardNumber == str_zero) {
                cardNumber = str_zero
            } else {
                cardNumber += str_zero
            }
            tv_cash_drop_amount.text = cardNumber
        }
        cv_look_up.setOnClickListener {
            //Toast.makeText(this, "Looking for $cardNumber", Toast.LENGTH_SHORT).show()
        }
        cv_scan.setOnClickListener {
            //Toast.makeText(this, "Scan gift card", Toast.LENGTH_SHORT).show()
        }
        ll_gift_card_done.setOnClickListener {
            when {
                cardNumber == "" -> {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        "Please enter card number!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                tenderedAmount > gift_card_balance -> {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        "Card balance is insufficient!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val jObj = JSONObject()
                    jObj.put("posOrderId", posOrderId)
                    jObj.put("amount", "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble())
                    jObj.put("amountTendered", tenderedAmount)
                    jObj.put("tipAmount", tips.toDouble())
                    jObj.put("changeAmount", "%.2f".format(abs(due)).toDouble())
                    jObj.put("paymentType", TYPE_GIFT_CARD)
                    jObj.put("giftCardNo", cardNumber)
                    if (useLoyaltyPoints) {
                        jObj.put("useCredits", useLoyaltyPoints)
                        jObj.put("creditsAmount", availableLoyaltyPoints)
                    }
                    jObj.put("createdBy", mEmployeeId)
                    jObj.put("restaurantId", restaurantId)
                    val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                    Log.e("PAYORDERREQUEST", "" + finalObject)
                    payOrder(finalObject = finalObject, dialog = dialog_gift_card)
                }
            }
        }
        dialog_gift_card.show()
    }

    private fun otherPaymentsDialog() {
        var finalTotalAmount = 0.00
        val tips = et_tip_amount.text.toString().trim()
        finalTotalAmount = if (useLoyaltyPoints) {
            if (tips.toDouble() == tipAmountCurrentCheck) {
                (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck)
            } else {
                (dueAmount - availableLoyaltyPoints - tipAmountCurrentCheck + tips.toDouble())
            }
        } else {
            if (tips.toDouble() == tipAmountCurrentCheck) {
                (dueAmount - tipAmountCurrentCheck)
            } else {
                (dueAmount - tipAmountCurrentCheck + tips.toDouble())
            }
        }
        dialog_others = Dialog(this)
        dialog_others.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.other_payment_dialog, null)
        dialog_others.setContentView(view)
        dialog_others.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_others.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_others.setCanceledOnTouchOutside(true)
        val ll_customer_details = view.findViewById(R.id.ll_customer_details) as LinearLayout
        ll_customer_details.setOnClickListener {
            if (tenderedAmount > /*dueAmount*/finalTotalAmount) {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Amount tendered should not be more than Balance Due!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                customerDetailsDialog()
                dialog_others.dismiss()
            }
        }
        dialog_others.show()
    }

    private fun customerDetailsDialog() {
        var phoneNumber = ""
        dialog_customer_details = Dialog(this)
        dialog_customer_details.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.customer_details_dialog, null)
        dialog_customer_details.setContentView(view)
        dialog_customer_details.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog_customer_details.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog_customer_details.setCanceledOnTouchOutside(true)
        val tv_order_amount = view.findViewById(R.id.tv_order_amount) as TextView
        val et_phone_number = view.findViewById(R.id.et_phone_number) as EditText
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_currency_order_total = view.findViewById(R.id.tv_currency_order_total) as TextView
        val tv_currency_amount_tendered =
            view.findViewById(R.id.tv_currency_amount_tendered) as TextView
        ll_numpad = view.findViewById(R.id.ll_numpad)
        val ll_one = view.findViewById(R.id.ll_one) as LinearLayout
        val ll_two = view.findViewById(R.id.ll_two) as LinearLayout
        val ll_three = view.findViewById(R.id.ll_three) as LinearLayout
        val ll_four = view.findViewById(R.id.ll_four) as LinearLayout
        val ll_five = view.findViewById(R.id.ll_five) as LinearLayout
        val ll_six = view.findViewById(R.id.ll_six) as LinearLayout
        val ll_seven = view.findViewById(R.id.ll_seven) as LinearLayout
        val ll_eight = view.findViewById(R.id.ll_eight) as LinearLayout
        val ll_nine = view.findViewById(R.id.ll_nine) as LinearLayout
        val ll_zero = view.findViewById(R.id.ll_zero) as LinearLayout
        val ll_delete = view.findViewById(R.id.ll_delete) as LinearLayout
        ll_customer_details = view.findViewById(R.id.ll_customer_details) as LinearLayout
        tv_customer_name = view.findViewById(R.id.tv_customer_name)
        tv_customer_credits = view.findViewById(R.id.tv_customer_credits)
        ll_insufficient_credits = view.findViewById(R.id.ll_insufficient_credits)
        ll_sufficient_credits = view.findViewById(R.id.ll_sufficient_credits)
        tv_balance_after_payment = view.findViewById(R.id.tv_balance_after_payment)
        val tv_make_payment = view.findViewById(R.id.tv_make_payment) as TextView
        val tv_tendered_amount = view.findViewById(R.id.tv_tendered_amount) as TextView
        val tv_currency_credits = view.findViewById(R.id.tv_currency_credits) as TextView
        val tv_currency_balance_credits =
            view.findViewById(R.id.tv_currency_balance_credits) as TextView
        ll_numpad.visibility = View.GONE
        ll_customer_details.visibility = View.GONE
        tv_currency_order_total.text = currencyType
        tv_currency_amount_tendered.text = currencyType
        tv_currency_credits.text = currencyType
        tv_currency_balance_credits.text = currencyType
        tv_order_amount.text = "%.2f".format(dueAmount)
        tv_tendered_amount.text = "%.2f".format(tenderedAmount)
        val tips = et_tip_amount.text.toString().trim()
        et_phone_number.onFocusChangeListener = object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                isPhoneFeildSelected = hasFocus
            }
        }
        et_phone_number.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                ll_numpad.visibility = View.VISIBLE
                ll_customer_details.visibility = View.GONE
                return false
            }
        })
        et_phone_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    ll_numpad.visibility = View.VISIBLE
                }
            }
        })
        tv_submit.setOnClickListener {
            if (phoneNumber == "") {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Please enter phone number",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObj = JSONObject()
                jObj.put("userId", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("phonenumber", phoneNumber)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                searchCustomerPhone(finalObject)
            }
        }
        tv_make_payment.setOnClickListener {
            if (tenderedAmount > customer_credits) {
                ll_insufficient_credits.visibility = View.VISIBLE
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Insufficient credits!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jObj = JSONObject()
                jObj.put("posOrderId", posOrderId)
                jObj.put("amount", "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble())
                jObj.put("amountTendered", tenderedAmount)
                jObj.put("tipAmount", tips.toDouble())
                jObj.put("changeAmount", "%.2f".format(abs(due)).toDouble())
                jObj.put("paymentType", TYPE_PREPAID_CARD)
                jObj.put("preCardNo", preCardNo)
                if (useLoyaltyPoints) {
                    jObj.put("useCredits", useLoyaltyPoints)
                    jObj.put("creditsAmount", availableLoyaltyPoints)
                }
                jObj.put("createdBy", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.e("PAYORDERREQUEST", "" + finalObject)
                payOrder(finalObject = finalObject, dialog = dialog_customer_details)
            }
        }
        ll_one.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_one
                } else {
                    phoneNumber += str_one
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_two.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_two
                } else {
                    phoneNumber += str_two
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_three.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_three
                } else {
                    phoneNumber += str_three
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_four.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_four
                } else {
                    phoneNumber += str_four
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_five.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_five
                } else {
                    phoneNumber += str_five
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_six.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_six
                } else {
                    phoneNumber += str_six
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_seven.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_seven
                } else {
                    phoneNumber += str_seven
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_eight.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_eight
                } else {
                    phoneNumber += str_eight
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_nine.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_nine
                } else {
                    phoneNumber += str_nine
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_zero.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber == "") {
                    phoneNumber = str_zero
                } else {
                    phoneNumber += str_zero
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        ll_delete.setOnClickListener {
            if (isPhoneFeildSelected) {
                if (phoneNumber != "") {
                    phoneNumber = phoneNumber.dropLast(1)
                    if (phoneNumber == "") {
                        phoneNumber = ""
                    }
                } else {
                    phoneNumber = ""
                }
                et_phone_number.setText(phoneNumber)
            }
        }
        dialog_customer_details.show()
    }

    private fun voidItemsDialog(itemsList: ArrayList<OrderItemsResponse>) {
        var selectedItemId = ""
        var selectedItemReason = ""
        mVoidDialog = Dialog(this)
        mVoidDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.dialog_void_items_payment, null)
        mVoidDialog.setContentView(view)
        mVoidDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        mVoidDialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mVoidDialog.window!!.setGravity(Gravity.CENTER)
        mVoidDialog.setCanceledOnTouchOutside(false)
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val sp_void_reason = view.findViewById(R.id.sp_void_reason) as Spinner
        val rv_items = view.findViewById(R.id.rv_items) as RecyclerView
        val lm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_items.layoutManager = lm
        rv_items.hasFixedSize()
        val voidItemsList: ArrayList<VoidItemsDataModelNew> = ArrayList()
        for (item in 0 until itemsList.size) {
            val voidItemsDataModelNew = VoidItemsDataModelNew(
                itemsList[item].id.toString(),
                itemsList[item].itemName.toString(),
                itemsList[item].quantity!!,
                itemsList[item].comboId.toString(),
                itemsList[item].bogoId.toString(),
                itemsList[item].comboUniqueId.toString(),
                itemsList[item].bogoUniqueId.toString(),
                itemsList[item].isCombo!!,
                itemsList[item].isBogo!!,
                itemsList[item].voidStatus!!,
                false
            )
            voidItemsList.add(voidItemsDataModelNew)
        }
        val adapter = VoidItemsPaymentListAdapter(this, voidItemsList)
        rv_items.adapter = adapter
        adapter.notifyDataSetChanged()

        val sp_adapter = VoidReasonsListAdapter(this, void_reasonsList)
        sp_void_reason.adapter = sp_adapter
        sp_void_reason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val selectedItem = sp_adapter.getSelectedItem(position)
                selectedItemId = selectedItem.id.toString()
                selectedItemReason = selectedItem.name.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_cancel.setOnClickListener {
            mVoidDialog.dismiss()
        }
        tv_ok.setOnClickListener {
            //mVoidDialog.dismiss()
            Log.d("sizeee", adapter.getVoidedItems().size.toString())
            when {
                adapter.getVoidedItems().isEmpty() -> {
                    Toast.makeText(
                        this,
                        "Please select any item for voiding.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                selectedItemId == "" -> {
                    Toast.makeText(
                        this,
                        "Please select a valid reason for voiding items.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val mainJson = JSONObject()
                    val jArray = JSONArray()
                    for (i in 0 until adapter.getVoidedItems().size) {
                        var qty = adapter.getVoidedItems()[i].qty
                        // checking if more quantity is added
                        for (j in 0 until itemsList.size) {
                            if (itemsList[j].id == adapter.getVoidedItems()[i].id) {
                                if (itemsList[j].quantity!! < adapter.getVoidedItems()[i].qty) {
                                    qty = itemsList[j].quantity!!
                                }
                            }
                        }
                        val jObj = JSONObject()
                        jObj.put("id", adapter.getVoidedItems()[i].id)
                        jObj.put("voidId", selectedItemId)
                        jObj.put("voidReason", selectedItemReason)
                        jObj.put("voidQuantity", qty)
                        jArray.put(jObj)
                    }
                    mainJson.put("itemsList", jArray)
                    mainJson.put("restaurantId", restaurantId)
                    mainJson.put("employeeId", mEmployeeId)
                    Log.d("mainJson", mainJson.toString())
                    val finalObj = JsonParser.parseString(mainJson.toString()).asJsonObject
                    voidMultipleItems(finalObj)
                }
            }
        }
        mVoidDialog.show()
    }

    /* search user */
    private fun searchUserDialog() {
        var phoneNumber: String
        customer_search_dialog = Dialog(this)
        customer_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@PaymentScreenActivity)
                .inflate(R.layout.search_user_dialog, null)
        customer_search_dialog.setContentView(view)
        customer_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        customer_search_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        customer_search_dialog.setCanceledOnTouchOutside(false)
        val tv_search = view.findViewById(R.id.tv_search) as TextView
        val et_number = view.findViewById(R.id.et_number) as EditText
        rv_user_search = view.findViewById(R.id.rv_user_search)
        ll_user_empty = view.findViewById(R.id.ll_user_empty)
        val tv_add_new_customer = view.findViewById(R.id.tv_add_new_customer) as TextView
        tv_ok = view.findViewById(R.id.tv_ok)
        tv_ok.isEnabled = false
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_user_search.layoutManager = linearLayoutManager
        rv_user_search.hasFixedSize()
        tv_cancel.setOnClickListener {
            customer_search_dialog.dismiss()
        }
        tv_search.setOnClickListener {
            phoneNumber = et_number.text.toString().trim()
            if (phoneNumber == "") {
                Toast.makeText(this, "Enter phone number!", Toast.LENGTH_SHORT).show()
            } else {
                /*if (phoneNumber.length > MAX_CHAR) {
                    phoneNumber = phoneNumber.substring(0, MAX_CHAR)
                }*/
                val jObj = JSONObject()
                jObj.put("userId", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                jObj.put("phonenumber", phoneNumber)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                searchCustomer(finalObject)
            }
        }
        tv_add_new_customer.setOnClickListener {
            addCustomerDialog()
        }
        tv_ok.setOnClickListener {
            try {
                val jsonObj = JSONObject()
                jsonObj.put("restaurantId", restaurantId)
                jsonObj.put("employeeId", mEmployeeId)
                jsonObj.put("customerId", customerId)
                jsonObj.put("orderId", posOrderId)
                val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                Log.d("updateCustomerInfo", final_object.toString())
                updateCustomerInfoForCheck(final_object, customer_search_dialog)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        customer_search_dialog.show()
    }

    private fun addCustomerDialog() {
        add_customer_dialog = Dialog(this)
        add_customer_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@PaymentScreenActivity)
                .inflate(R.layout.add_user_dialog, null)
        add_customer_dialog.setContentView(view)
        add_customer_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        add_customer_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        add_customer_dialog.setCanceledOnTouchOutside(false)
        val tv_customer_details_header =
            view.findViewById(R.id.tv_customer_details_header) as TextView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val ll_countries = view.findViewById(R.id.ll_countries) as LinearLayout
        et_first_name = view.findViewById(R.id.et_first_name)
        et_last_name = view.findViewById(R.id.et_last_name)
        et_phone_number = view.findViewById(R.id.et_phone_number)
        et_email = view.findViewById(R.id.et_email)
        tv_customer_details_header.text = getString(R.string.add_new_customer)
        ll_countries.visibility = View.GONE
        tv_cancel.setOnClickListener {
            add_customer_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            if (isValid()) {
                customerId = "0"
                customerFirstName = et_first_name.text.toString().trim()
                customerLastName = et_last_name.text.toString().trim()
                customerEmail = et_email.text.toString().trim()
                customerPhone = et_phone_number.text.toString().trim()
                try {
                    val jsonObj = JSONObject()
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("customerId", customerId)
                    jsonObj.put("firstName", customerFirstName)
                    jsonObj.put("lastName", customerLastName)
                    jsonObj.put("phoneNumber", customerPhone)
                    jsonObj.put("email", customerEmail)
                    jsonObj.put("orderId", posOrderId)
                    val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    Log.d("updateCustomerInfo", final_object.toString())
                    updateCustomerInfoForCheck(final_object, add_customer_dialog)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                customer_search_dialog.dismiss()
            }
        }
        add_customer_dialog.show()
    }

    private fun showRedeemPointsConfirmDialog() {
        val tips = et_tip_amount.text.toString().trim()
        loyalty_points_dialog = Dialog(this)
        loyalty_points_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@PaymentScreenActivity)
                .inflate(R.layout.loyalty_points_confirm_dialog, null)
        loyalty_points_dialog.setContentView(view)
        loyalty_points_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loyalty_points_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loyalty_points_dialog.setCanceledOnTouchOutside(false)
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_redeemed_points = view.findViewById(R.id.tv_redeemed_points) as TextView
        tv_redeemed_points.text = "${
            "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble()
        } ${getString(R.string.bonus_points_will_be_redeemed)}"
        tv_cancel.setOnClickListener {
            cb_use_loyalty_points.isChecked = false
            loyalty_points_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            try {
                val jObj = JSONObject()
                jObj.put("posOrderId", posOrderId)
                jObj.put("cashDrawerId", cashDrawerId)
                jObj.put("amount", "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble())
                jObj.put("amountTendered", tenderedAmount)
                jObj.put("tipAmount", tips.toDouble())
                jObj.put("changeAmount", "%.2f".format(abs(due)).toDouble())
                jObj.put("useCredits", useLoyaltyPoints)
                jObj.put(
                    "creditsAmount",
                    "%.2f".format(dueAmount - tipAmountCurrentCheck).toDouble()
                )
                jObj.put("createdBy", mEmployeeId)
                jObj.put("restaurantId", restaurantId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.d("PAYORDERREQUEST", finalObject.toString())
                payOrder(finalObject = finalObject, dialog = loyalty_points_dialog)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        loyalty_points_dialog.show()
    }

    private fun getOrderFromTable(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.viewOrderPaymentApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        orderedList = respBody.orderDetails!!
                        if (orderedList.isNotEmpty()) {
                            orderedListAdapter =
                                ListAdapter(this@PaymentScreenActivity, orderedList)
                            rv_bills.adapter = orderedListAdapter
                            val decoator = LinePagerIndicatorDecoration()
                            rv_bills.addItemDecoration(decoator)
                            orderedListAdapter.notifyDataSetChanged()
                            Log.d("payment_status", isPaymentStatus.toString())
                            if (isCashSelected) {
                                if (!isRequestedReceipt) {
                                    showSuccessDialog()
                                } else if (isRequestedReceipt) {
                                    val intent =
                                        Intent(
                                            this@PaymentScreenActivity,
                                            ViewItemsActivity::class.java
                                        )
                                    intent.putExtra("payment_status", isPaymentStatus.toString())
                                    intent.putExtra("orderItemsList", orderedList)
                                    intent.putExtra("checkNumber", checkNumber)
                                    startActivity(intent)
                                }
                            }
                            if (isGiftcardSelected) {
                                if (isPaymentStatus) {
                                    if (orderedList[checkPos].dueAmount == 0.00) {
                                        val intent =
                                            Intent(
                                                this@PaymentScreenActivity,
                                                ViewItemsActivity::class.java
                                            )
                                        intent.putExtra(
                                            "payment_status",
                                            isPaymentStatus.toString()
                                        )
                                        intent.putExtra("orderItemsList", orderedList)
                                        intent.putExtra("checkNumber", checkNumber)
                                        startActivity(intent)
                                    } else {
                                        Log.d("dueAmount", "dueAmount else $dueAmount")
                                    }
                                }
                            }
                        }
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun payOrder(finalObject: JsonObject, dialog: Dialog) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.payOrderApi(finalObject)
        call.enqueue(object : Callback<AddOrderResponse> {
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("error", t.toString())
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        if (dialog.isShowing)
                            dialog.dismiss()
                        isPaymentStatus = true
                        lastPaidPosition = checkPos
                        orderFromTable()
                        successNotifyGuestDisplay(mOrderId)
                    } else {
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun searchCustomer(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.searchCustomerPhoneApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val customersList = respBody.customer!!
                        if (customersList.size > 0) {
                            val adapter =
                                CustomersListAdapter(this@PaymentScreenActivity, customersList)
                            rv_user_search.adapter = adapter
                            adapter.notifyDataSetChanged()
                            ll_user_empty.visibility = View.GONE
                            rv_user_search.visibility = View.VISIBLE
                        } else {
                            ll_user_empty.visibility = View.VISIBLE
                            rv_user_search.visibility = View.GONE
                        }
                    } else {
                        ll_user_empty.visibility = View.VISIBLE
                        rv_user_search.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateCustomerInfoForCheck(finalObject: JsonObject, dialog: Dialog) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.updateCustomerInfoForCheckApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    if (dialog.isShowing)
                        dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        orderFromTable()
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        if (dialog.isShowing)
                            dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun searchCustomerPhone(finalObject: JsonObject) {
        var balance: Double
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.searchCustomerPhoneApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val respBody = response.body()!!
                    if (respBody.responseStatus == 1) {
                        val customersList = respBody.customer!!
                        if (customersList.size > 0) {
                            tv_customer_name.text =
                                customersList[0].firstName.toString() + " " + customersList[0].lastName.toString()
                            if (customersList[0].credits.toString() == "null") {
                                tv_customer_credits.text = getString(R.string.default_value_0)
                                customer_credits = 0.00
                                balance = 0.00 - tenderedAmount
                            } else {
                                tv_customer_credits.text = "%.2f".format(customersList[0].credits)
                                customer_credits = customersList[0].credits!!
                                balance = customersList[0].credits!! - tenderedAmount
                            }
                            if (tenderedAmount > customer_credits) {
                                ll_insufficient_credits.visibility = View.VISIBLE
                                ll_sufficient_credits.visibility = View.GONE
                                Toast.makeText(
                                    this@PaymentScreenActivity,
                                    "Insufficient credits!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                ll_insufficient_credits.visibility = View.GONE
                                ll_sufficient_credits.visibility = View.VISIBLE
                            }
                            if (balance >= 0) {
                                tv_balance_after_payment.text = "%.2f".format(balance)
                            } else {
                                tv_balance_after_payment.text = getString(
                                    R.string.default_value_0
                                )
                            }
                            preCardNo = customersList[0].preCardNo!!
                            ll_customer_details.visibility = View.VISIBLE
                            ll_numpad.visibility = View.GONE
                        } else {
                            ll_customer_details.visibility = View.GONE
                            ll_numpad.visibility = View.VISIBLE
                        }
                    } else {
                        ll_customer_details.visibility = View.GONE
                        ll_numpad.visibility = View.VISIBLE
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            respBody.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun successNotifyGuestDisplay(order_id: String) {
        val jObj = JSONObject()
        jObj.put("orderId", order_id)
        jObj.put("deviceToken", deviceToken)
        val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
        val api = ApiInterface.create()
        val call = api.paymentNotificationApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Log.d("error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            "Guest Display " + resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@PaymentScreenActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@PaymentScreenActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getGiftCardBalance(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.giftCardBalanceEnqApi(json_obj)
        call.enqueue(object : Callback<GiftCardBalanceResponse> {
            override fun onFailure(call: Call<GiftCardBalanceResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GiftCardBalanceResponse>,
                response: Response<GiftCardBalanceResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus!! == 1) {
                        gift_card_balance = resp.giftCard!!.amount!!
                        tv_gift_card_balance.text = "%.2f".format(gift_card_balance)
                        ll_gift_card_balance_details.visibility = View.VISIBLE
                        if (tenderedAmount > gift_card_balance) {
                            Toast.makeText(
                                this@PaymentScreenActivity,
                                "Card balance is insufficient!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        tv_gift_card_balance.text = getString(R.string.default_value_0)
                        ll_gift_card_balance_details.visibility = View.INVISIBLE
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getVoidItemsReasons(finalObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.voidItemReasonsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Log.e("void reasons error", t.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        void_reasonsList = resp.void_reasonsList!!
                    } else {
                        void_reasonsList = ArrayList()
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun voidMultipleItems(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.voidMultipleItemsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        orderFromTable()
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        mVoidDialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@PaymentScreenActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getLoyaltyPoints(finalObject: JsonObject) {
        val apiInterface = ApiInterface.create()
        val call = apiInterface.customerBonusPointsApi(finalObject)
        call.enqueue(object : Callback<CustomerBonusResponse> {
            override fun onFailure(call: Call<CustomerBonusResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@PaymentScreenActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CustomerBonusResponse>,
                response: Response<CustomerBonusResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    availableLoyaltyPoints = if (resp.responseStatus!! == 1) {
                        val credits = resp.creditsData
                        if (credits != null) {
                            credits.restarant_available_points!!
                        } else {
                            0.00
                        }
                    } else {
                        0.00
                    }
                    cb_use_loyalty_points.text =
                        "${getString(R.string.use_bonus_points_for_bill)} ($currencyType${
                            "%.2f".format(availableLoyaltyPoints)
                        })"
                } catch (e: Exception) {
                    e.printStackTrace()
                    availableLoyaltyPoints = 0.00
                    cb_use_loyalty_points.text =
                        "${getString(R.string.use_bonus_points_for_bill)} ($currencyType${
                            "%.2f".format(availableLoyaltyPoints)
                        })"
                }
            }
        })
    }

    private fun getUserLoyaltyPoints(customer_id: String) {
        try {
            val jObj = JSONObject()
            jObj.put("customerId", customer_id)
            jObj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            Log.d("finalObject", finalObject.toString())
            getLoyaltyPoints(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun isValid(): Boolean {
        when {
            et_first_name.text.toString() == "" -> {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "First name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_first_name.requestFocus()
            }
            et_last_name.text.toString() == "" -> {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Last name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_last_name.requestFocus()
            }
            et_phone_number.text.toString() == "" -> {
                Toast.makeText(
                    this@PaymentScreenActivity,
                    "Phone number should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_phone_number.requestFocus()
            }
            et_email.text.toString() == "" -> {
                Toast.makeText(
                    this@PaymentScreenActivity, "E-mail should not be empty!", Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            !isValidEmail(et_email.text.toString()) -> {
                Toast.makeText(
                    this@PaymentScreenActivity, "Invalid E-mail address",
                    Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            else -> {
                return true
            }
        }
        return false
    }

    inner class ListAdapter(
        context: Context,
        modifiers: ArrayList<OrderDetailsResponse>
    ) :
        RecyclerView.Adapter<ListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<OrderDetailsResponse>? = null

        init {
            this.context = context
            this.mList = modifiers
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): ListAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.bill_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItemAtPosition(position: Int): OrderDetailsResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(
            holder: ListAdapter.ViewHolder, position: Int
        ) {
            holder.tv_check_table_no.isSelected = true
            holder.tv_currency_quantity.text = "($currencyType)"
            holder.tv_total.text = currencyType + "%.2f".format(mList!![position].totalAmount)
            holder.tv_tax.text = currencyType + "%.2f".format(mList!![position].taxAmount)
            holder.tv_sub_total.text = currencyType + "%.2f".format(mList!![position].subTotal)
            holder.tv_tip_amount.text = currencyType + "%.2f".format(mList!![position].tipAmount)
            holder.tv_discount.text = currencyType + "%.2f".format(mList!![position].discountAmount)
            holder.tv_balance.text = currencyType + "%.2f".format(mList!![position].dueAmount)
            holder.tv_credits.text = currencyType + "%.2f".format(mList!![position].creditsUsed)
            holder.tv_check_employee.text = mList!![position].createdByName
            holder.tv_check_table_no.text =
                "#${mList!![position].checkNumber}, ${mList!![position].dineInOptionName}"
            et_tip_amount.setText("%.2f".format(mList!![position].tipAmount))
            val tip = et_tip_amount.text.toString().trim()
            et_balance_due.setText("%.2f".format(dueAmount - tipAmountCurrentCheck + tip.toDouble()))
            tv_order_number.text =
                getString(R.string.order) + " #${mList!![position].orderNumber!!}"
            tv_table_number.text =
                getString(R.string.table) + " ${mList!![position].tableNumber.toString()}"
            orderItemsList = mList!![position].orderItems!!
            val adapter =
                ChecksBreakdownPaymentListAdapter(this@PaymentScreenActivity, orderItemsList)
            holder.rv_order_items.adapter = adapter
            adapter.notifyDataSetChanged()
            if (mList!![position].status == 3) {
                holder.iv_payment_success.visibility = View.VISIBLE
                holder.tv_button_edit.visibility = View.GONE
            } else {
                holder.iv_payment_success.visibility = View.GONE
                holder.tv_button_edit.visibility = View.VISIBLE
            }
            if (mList!![position].transactionsList!!.size > 0) {
                var change_due = 0.00
                var payment_type = ""
                val sbString = StringBuilder("")
                val list = ArrayList<String>()
                for (i in 0 until mList!![position].transactionsList!!.size) {
                    change_due += mList!![position].transactionsList!![i].changeAmount!!
                    //amount_tendered += mList!![position].transactionsList!![i].amountTendered!!
                    when (mList!![position].transactionsList!![i].paymentType) {
                        TYPE_CASH -> {
                            payment_type = "Cash"
                        }
                        TYPE_CARD -> {
                            payment_type = "Card"
                        }
                        TYPE_GIFT_CARD -> {
                            payment_type = "Gift Card"
                        }
                        TYPE_PREPAID_CARD -> {
                            payment_type = "Prepaid Card"
                        }
                    }
                    list.add(payment_type)
                    // to show names multiple times, uncomment below
                    /*if (i == mList!![position].transactionsList!!.size - 1) {
                        sbString.append(payment_type).append(" ")
                    } else {
                        sbString.append(payment_type).append(" & ")
                    }*/
                }
                // to show multiple names as single time
                val singleList = list.distinct()
                for (j in singleList.indices) {
                    if (j == singleList.size - 1) {
                        sbString.append(singleList[j]).append(" ")
                    } else {
                        sbString.append(singleList[j]).append(" & ")
                    }
                }
                var strList = sbString.toString()
                if (strList.isNotEmpty()) {
                    strList = strList.substring(0, strList.length - 1)
                }
                holder.tv_payment_type.text = strList
            } else {
                holder.tv_payment_type.text = ""
            }
            if (position - 1 != -1) {
                if (mList!![position].orderNumber == mList!![position - 1].orderNumber) {
                    holder.tv_split_no.visibility = View.VISIBLE
                    holder.tv_split_no.text = getString(R.string.split) + " ${position + 1}"
                } else {
                    holder.tv_split_no.visibility = View.GONE
                }
            } else if (position == 0 && mList!!.size > 1) {
                holder.tv_split_no.visibility = View.VISIBLE
                holder.tv_split_no.text = getString(R.string.split) + " ${position + 1}"
            }

            holder.tv_button_refund.isEnabled = false
            holder.tv_button_refund.background =
                ContextCompat.getDrawable(context!!, R.drawable.disabled_bg_corner)
            holder.tv_button_receipt.isEnabled = false
            holder.tv_button_receipt.background =
                ContextCompat.getDrawable(context!!, R.drawable.disabled_bg_corner)
            holder.tv_button_adjust.isEnabled = false
            holder.tv_button_adjust.background =
                ContextCompat.getDrawable(context!!, R.drawable.disabled_bg_corner)
            holder.tv_button_close.isEnabled = true
            holder.tv_button_close.background =
                ContextCompat.getDrawable(context!!, R.drawable.gradient_bg_1_corners)
            holder.tv_button_refund.setOnClickListener {
                // do nothing
            }
            holder.tv_button_receipt.setOnClickListener {
                // do nothing
            }
            holder.tv_button_adjust.setOnClickListener {
                // do nothing
            }
            holder.tv_button_close.setOnClickListener {
                onBackPressed()
            }
            holder.tv_button_done.setOnClickListener {
                val intent = Intent(this@PaymentScreenActivity, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }
            holder.tv_button_edit.setOnClickListener {
                voidItemsDialog(mList!![position].orderItems!!)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rv_order_items: RecyclerView = view.findViewById(R.id.rv_order_items)
            val tv_credits: TextView = view.findViewById(R.id.tv_credits)
            val tv_sub_total: TextView = view.findViewById(R.id.tv_sub_total)
            val tv_discount: TextView = view.findViewById(R.id.tv_discount)
            val tv_tax: TextView = view.findViewById(R.id.tv_tax)

            //val tv_tip_percent: TextView = view.findViewById(R.id.tv_tip_percent)
            val tv_tip_amount: TextView = view.findViewById(R.id.tv_tip_amount)
            val tv_balance: TextView = view.findViewById(R.id.tv_balance)
            val tv_total: TextView = view.findViewById(R.id.tv_total)
            val tv_check_employee: TextView = view.findViewById(R.id.tv_check_employee)
            val tv_check_table_no: TextView = view.findViewById(R.id.tv_check_table_no)
            val tv_button_refund: TextView = view.findViewById(R.id.tv_button_refund)
            val tv_button_receipt: TextView = view.findViewById(R.id.tv_button_receipt)
            val tv_button_adjust: TextView = view.findViewById(R.id.tv_button_adjust)
            val tv_button_close: TextView = view.findViewById(R.id.tv_button_close)
            val tv_button_done: TextView = view.findViewById(R.id.tv_button_done)
            val tv_split_no: TextView = view.findViewById(R.id.tv_split_no)
            val tv_payment_type: TextView = view.findViewById(R.id.tv_payment_type)
            val tv_currency_quantity: TextView = view.findViewById(R.id.tv_currency_quantity)
            val iv_payment_success: ImageView = view.findViewById(R.id.iv_payment_success)
            val tv_button_edit: TextView = view.findViewById(R.id.tv_button_edit)
        }
    }

    inner class CustomersListAdapter(context: Context, list: ArrayList<CustomerDataResponse>) :
        RecyclerView.Adapter<CustomersListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<CustomerDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.customer_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): CustomerDataResponse {
            return mList!![position]
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_name.text =
                "${mList!![position].firstName.toString()} ${mList!![position].lastName.toString()}"
            holder.tv_email.text = mList!![position].email.toString()
            holder.tv_number.text = mList!![position].phoneNumber.toString()
            holder.cv_item_header.setOnClickListener {
                if (mSelectedIndex == -1 || mSelectedIndex != position) {
                    mSelectedIndex = position
                    customerId = mList!![position].id.toString()
                    customerFirstName = mList!![position].firstName.toString()
                    customerLastName = mList!![position].lastName.toString()
                    customerEmail = mList!![position].email.toString()
                    customerPhone = mList!![position].phoneNumber.toString()
                    tv_ok.isEnabled = true
                } else if (mSelectedIndex == position) {
                    mSelectedIndex = -1
                    customerId = ""
                    customerFirstName = ""
                    customerLastName = ""
                    customerEmail = ""
                    customerPhone = ""
                    tv_ok.isEnabled = false
                }
                if (mSelectedIndex == position) {
                    holder.cv_item_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.cyan
                        )
                    )
                    holder.tv_name.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_email.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                } else {
                    holder.cv_item_header.setCardBackgroundColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    holder.tv_name.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.edit_text_hint
                        )
                    )
                    holder.tv_email.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.button_black
                        )
                    )
                    holder.tv_number.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.button_black
                        )
                    )
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_name: TextView = view.findViewById(R.id.tv_name)
            var tv_email: TextView = view.findViewById(R.id.tv_email)
            var tv_number: TextView = view.findViewById(R.id.tv_number)
            var cv_item_header: CardView = view.findViewById(R.id.cv_item_header)
        }
    }

    override fun onBackPressed() {
        when (from_screen) {
            "payment_terminal" -> {
                super.onBackPressed()
            }
            "quick_order" -> {
                val intent = Intent(this@PaymentScreenActivity, PaymentTerminalActivity::class.java)
                intent.putExtra("from_screen", "home")
                intent.putExtra("employeeId", "")
                intent.putExtra("show_tab", "open")
                startActivity(intent)
                finish()
            }
            "split_order" -> {
                super.onBackPressed()
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        isCashSelected = false
        isPaymentStatus = false
        isGiftcardSelected = false
        orderFromTable()
    }
}