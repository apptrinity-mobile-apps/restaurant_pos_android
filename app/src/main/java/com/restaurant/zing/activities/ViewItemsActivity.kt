package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.LinePagerIndicatorDecoration
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import com.restaurant.zing.helpers.isValidEmail
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@SuppressLint("SetTextI18n")
class ViewItemsActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var tv_check_no: TextView
    private lateinit var ll_done: LinearLayout
    private lateinit var ll_print: LinearLayout
    private lateinit var ll_email: LinearLayout
    private lateinit var rv_ordered_items: RecyclerView
    private lateinit var orderedItemsLinearLayout: LinearLayoutManager
    private lateinit var snapHelper: PagerSnapHelper
    private lateinit var itemsAdapter: ItemsListAdapter
    private lateinit var sessionManager: SessionManager
    private lateinit var loading_dialog: Dialog
    private lateinit var emailsent_dialog: Dialog
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var restaurantName = ""
    private var uniqueNumber = ""
    private var tipWithGrauities = ""
    private var tabName = ""
    private var surveyCode = ""
    private var status = ""
    private var specialRequests = ""
    private var showHouseAcccounts = ""
    private var serviceChargesFont = ""
    private var restaurantId = ""
    private var receiptHeader = ""
    private var receiptFooter = ""
    private var printPayments = ""
    private var largeOrderNum = ""
    private var itemizeTableService = ""
    private var itemizeQuickOrder = ""
    private var itemModifiers = ""
    private var image = ""
    private var id = ""
    private var guestDetailsDelivery = ""
    private var closedcheckRecipet = ""
    private var combineItems = ""
    private var createdBy = ""
    private var createdOn = ""
    private var creditCardRecipets = ""
    private var displayCharityTip = ""
    private var displayDiscounts = ""
    private var displayItemQTY = ""
    private var displayTipLine = ""
    private var displayTipPercent = ""
    private var freeItems = ""
    private var freeModifiers = ""
    private var guestCount = ""
    private var orderItemsList = ArrayList<OrderDetailsResponse>()
    private var receiptSetUpDetails = ReceiptSetUpDataResponse()
    private var checkNumber = 0
    private var pageCount = 0
    private var itemsTotalCount = 0
    private var payment_status = ""

    private val TYPE_CASH = 0
    private val TYPE_CARD = 1
    private val TYPE_GIFT_CARD = 2
    private val TYPE_PREPAID_CARD = 3
    private lateinit var et_email: EditText
    private var order_id = ""
    private var customer_email = ""
    private var haveCustomer = false
    private var currencyType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_items)

        initialize()

        try {
            orderItemsList =
                intent.getSerializableExtra("orderItemsList") as ArrayList<OrderDetailsResponse>
            checkNumber = intent.getIntExtra("checkNumber", 0)
            payment_status = intent.getStringExtra("payment_status")!!
            val jObj = JSONObject()
            jObj.put("userId", mEmployeeId)
            jObj.put("restaurantId", restaurantId)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            ReceiptSetUpView(finalObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        EmailSentDialog()
        Log.e("HAVECUSTOMER", "" + haveCustomer)

        itemsTotalCount = orderItemsList.size
        tv_check_no.text =
            getString(R.string.check) + " #$checkNumber ( ${pageCount + 1} of $itemsTotalCount )"

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_done.setOnClickListener {
            onBackPressed()
        }
        ll_print.setOnClickListener {
            Toast.makeText(this, "Printing", Toast.LENGTH_SHORT).show()
        }
        ll_email.setOnClickListener {
            if (haveCustomer) {
                val jObj = JSONObject()
                jObj.put("email", customer_email)
                jObj.put("orderId", order_id)
                jObj.put("restaurantId", restaurantId)
                jObj.put("employeeId", mEmployeeId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.e("EMAILSENTORDER", "" + finalObject)
                OrderDetailsEmailSent(finalObject)
            } else {
                emailsent_dialog.show()
            }
        }
        rv_ordered_items.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val centerView = snapHelper.findSnapView(orderedItemsLinearLayout)
                    val pos = orderedItemsLinearLayout.getPosition(centerView!!)
                    pageCount = pos + 1
                    tv_check_no.text =
                        getString(R.string.check) + " #$checkNumber ( $pageCount of $itemsTotalCount )"
                    order_id = orderItemsList[pos].id.toString()
                    haveCustomer = orderItemsList[pos].haveCustomer!!
                    if (haveCustomer) {
                        customer_email = orderItemsList[pos].customerDetails!!.email!!
                    }
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (orderedItemsLinearLayout.findFirstVisibleItemPosition() == 0) {
                    val centerView = snapHelper.findSnapView(orderedItemsLinearLayout)
                    val pos = orderedItemsLinearLayout.getPosition(centerView!!)
                    pageCount = 1
                    tv_check_no.text =
                        getString(R.string.check) + " #$checkNumber ( $pageCount of $itemsTotalCount )"
                    order_id = orderItemsList[pos].id.toString()
                    haveCustomer = orderItemsList[pos].haveCustomer!!
                    if (haveCustomer) {
                        customer_email = orderItemsList[pos].customerDetails!!.email!!
                    }
                }
            }
        })
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        restaurantDetails = sessionManager.getRestaurantDetails
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        restaurantName = restaurantDetails[SessionManager.RESTAURANT_NAME_KEY]!!
        currencyType = sessionManager.getCurrencyType
        iv_back = findViewById(R.id.iv_back)
        tv_check_no = findViewById(R.id.tv_check_no)
        ll_done = findViewById(R.id.ll_done)
        ll_print = findViewById(R.id.ll_print)
        ll_email = findViewById(R.id.ll_email)
        rv_ordered_items = findViewById(R.id.rv_ordered_items)
        orderedItemsLinearLayout = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_ordered_items.layoutManager = orderedItemsLinearLayout
        snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rv_ordered_items)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    inner class ItemsListAdapter(
        context: Context,
        list: ArrayList<OrderDetailsResponse>,
        receiptsetupdata: ReceiptSetUpDataResponse
    ) :
        RecyclerView.Adapter<ItemsListAdapter.ViewHolder>() {
        private var context: Context? = null
        private var mList: ArrayList<OrderDetailsResponse>? = null
        private var mreceiptSetUpData: ReceiptSetUpDataResponse? = null

        init {
            this.context = context
            this.mList = list
            this.mreceiptSetUpData = receiptsetupdata
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ItemsListAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_items_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ItemsListAdapter.ViewHolder, position: Int) {
            holder.tv_payment_type.isSelected = true
            holder.tv_receipt_restaurant_name.text = restaurantName
            val layoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_items.layoutManager = layoutManager
            holder.rv_items.hasFixedSize()
            val adapter =
                ViewItemsAdapter(context!!, mList!![position].orderItems!!, mreceiptSetUpData!!)
            holder.rv_items.adapter = adapter
            adapter.notifyDataSetChanged()
            holder.tv_item_sub_total.text = currencyType + "%.2f".format(mList!![position].subTotal)
            Log.d("status", mList!![position].status!!.toString())
            if (mList!![position].status!! == 3) {
                holder.tv_item_total_header.text = "Total Paid"
            } else {
                holder.tv_item_total_header.text = "Total Due"
            }
            holder.tv_item_total.text =
                currencyType + "%.2f".format(mList!![position].totalAmount!!)
            if (mreceiptSetUpData!!.receiptLogo.toString() == "null") {
                Picasso.get().load(R.mipmap.ic_launcher)
                    .into(holder.iv_receipt_logo)
            } else {
                if (mreceiptSetUpData!!.receiptLogo == ""){
                    Picasso.get().load(R.mipmap.ic_launcher)
                        .into(holder.iv_receipt_logo)
                } else {
                    Picasso.get().load(mreceiptSetUpData!!.receiptLogo)
                        .into(holder.iv_receipt_logo)
                }
            }
            holder.tv_receipt_header_name.text = mreceiptSetUpData!!.receiptHeader
            holder.tv_receipt_footer.text = mreceiptSetUpData!!.receiptFooter
            if (mreceiptSetUpData!!.largeOrderNum.equals("1")) {
                holder.tv_receipt_order_number.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.custom_font)
                )
            }
            if (mreceiptSetUpData!!.displayDiscounts.toString() == "null") {
                holder.ll_receipt_discount.visibility = View.VISIBLE
                if (mList!![position].discountValue.toString() == "null") {
                    holder.tv_item_total_discount.text =
                        currencyType + getString(R.string.default_value_0)
                } else {
                    holder.tv_item_total_discount.text =
                        currencyType + "%.2f".format(mList!![position].discountValue)
                }
            } else {
                if (mreceiptSetUpData!!.displayDiscounts.equals("1")) {
                    holder.ll_receipt_discount.visibility = View.VISIBLE
                    if (mList!![position].discountValue.toString() == "null") {
                        holder.tv_item_total_discount.text =
                            currencyType + getString(R.string.default_value_0)
                    } else {
                        holder.tv_item_total_discount.text =
                            currencyType + "%.2f".format(mList!![position].discountValue)
                    }
                } else {
                    holder.ll_receipt_discount.visibility = View.GONE
                }
            }
            holder.tv_receipt_order_number.text = mList!![position].orderNumber.toString()
            holder.tv_tax_amount.text = currencyType + "%.2f".format(mList!![position].taxAmount!!)
            holder.tv_tip_amount.text = currencyType + "%.2f".format(mList!![position].tipAmount!!)
            holder.tv_grand_total.text =
                currencyType + "%.2f".format(mList!![position].totalAmount!!)
            if (mList!![position].receiptNumber.toString() == "null") {
                holder.tv_receipt_no.visibility = View.GONE
                holder.tv_receipt_no.text = getString(R.string.receipt_no) + " "
            } else {
                holder.tv_receipt_no.visibility = View.VISIBLE
                holder.tv_receipt_no.text =
                    getString(R.string.receipt_no) + " " + mList!![position].receiptNumber!!
            }
            if (mList!![position].transactionsList!!.size > 0) {
                var change_due = 0.00
                var amount_tendered = 0.00
                var payment_type = ""
                val sbString = StringBuilder("")
                val list = ArrayList<String>()
                for (i in 0 until mList!![position].transactionsList!!.size) {
                    change_due += mList!![position].transactionsList!![i].changeAmount!!
                    amount_tendered += mList!![position].transactionsList!![i].amountTendered!!
                    when (mList!![position].transactionsList!![i].paymentType) {
                        TYPE_CASH -> {
                            payment_type = "Cash"
                        }
                        TYPE_CARD -> {
                            payment_type = "Card"
                        }
                        TYPE_GIFT_CARD -> {
                            payment_type = "Gift Card"
                        }
                        TYPE_PREPAID_CARD -> {
                            payment_type = "Prepaid Card"
                        }
                    }
                    list.add(payment_type)
                    // to show names multiple times, uncomment below
                    /*if (i == mList!![position].transactionsList!!.size - 1) {
                        sbString.append(payment_type).append(" ")
                    } else {
                        sbString.append(payment_type).append(" & ")
                    }*/
                }
                // to show multiple names as single time
                val singleList = list.distinct()
                for (j in singleList.indices) {
                    if (j == singleList.size - 1) {
                        sbString.append(singleList[j]).append(" ")
                    } else {
                        sbString.append(singleList[j]).append(" & ")
                    }
                }
                var strList = sbString.toString()
                if (strList.isNotEmpty()) {
                    strList = strList.substring(0, strList.length - 1)
                }
                holder.tv_payment_type.text = strList
                holder.tv_change_due.text = currencyType + "%.2f".format(change_due)
            } else {
                holder.tv_payment_type.text = ""
                holder.tv_change_due.text = currencyType + "0.00"
            }
            try {
                val full_date = mList!![position].createdOn.toString()
                val pattern = "EEE, dd MMM yyyy HH:mm:ss zzz"
                val sdf = SimpleDateFormat(pattern, Locale.getDefault())
                val date = sdf.parse(full_date)
                val sdf1 = SimpleDateFormat("MM/dd", Locale.getDefault())
                val stf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                val date1 = sdf1.format(date!!)
                val time = stf.format(date)
                holder.tv_order_date.text = "$date1 $time".toUpperCase(Locale.ROOT)
            } catch (e: Exception) {
                e.printStackTrace()
                holder.tv_order_date.text = ""
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_receipt_restaurant_name =
                view.findViewById(R.id.tv_receipt_restaurant_name) as TextView
            val tv_receipt_order_number =
                view.findViewById(R.id.tv_receipt_order_number) as TextView
            val tv_receipt_header_name = view.findViewById(R.id.tv_receipt_header_name) as TextView
            val tv_item_total_header = view.findViewById(R.id.tv_item_total_header) as TextView
            val tv_receipt_footer = view.findViewById(R.id.tv_receipt_footer) as TextView
            val rv_items = view.findViewById(R.id.rv_items) as RecyclerView
            val tv_item_sub_total = view.findViewById(R.id.tv_item_sub_total) as TextView
            val tv_item_total = view.findViewById(R.id.tv_item_total) as TextView
            val tv_item_total_discount = view.findViewById(R.id.tv_item_total_discount) as TextView
            val ll_receipt_discount = view.findViewById(R.id.ll_receipt_discount) as LinearLayout
            val iv_receipt_logo = view.findViewById(R.id.iv_receipt_logo) as ImageView
            val tv_tax_amount = view.findViewById(R.id.tv_tax_amount) as TextView
            val tv_tip_amount = view.findViewById(R.id.tv_tip_amount) as TextView
            val tv_grand_total = view.findViewById(R.id.tv_grand_total) as TextView
            val tv_order_date = view.findViewById(R.id.tv_order_date) as TextView
            val tv_payment_type = view.findViewById(R.id.tv_payment_type) as TextView
            val tv_change_due = view.findViewById(R.id.tv_change_due) as TextView
            val tv_receipt_no = view.findViewById(R.id.tv_receipt_no) as TextView
        }
    }

    inner class ViewItemsAdapter(
        context: Context,
        itemsList: ArrayList<OrderItemsResponse>, receiptsetupitemdata: ReceiptSetUpDataResponse
    ) :
        RecyclerView.Adapter<ViewItemsAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<OrderItemsResponse>? = null
        private var mReceiptSetupItemData: ReceiptSetUpDataResponse? = null

        init {
            this.context = context
            this.mList = itemsList
            this.mReceiptSetupItemData = receiptsetupitemdata
        }

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): ViewItemsAdapter.ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.print_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewItemsAdapter.ViewHolder, position: Int
        ) {
            val modifiersLayoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_modifiers.layoutManager = modifiersLayoutManager
            holder.rv_modifiers.hasFixedSize()
            val splRequestLayoutManager =
                LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            holder.rv_spl_requests.layoutManager = splRequestLayoutManager
            holder.rv_spl_requests.hasFixedSize()
            if (mList!![position].modifiersList!!.isEmpty()) {
                holder.rv_modifiers.visibility = View.GONE
            } else {
                holder.rv_modifiers.visibility = View.VISIBLE
            }
            if (mList!![position].specialRequestList!!.isEmpty()) {
                holder.rv_spl_requests.visibility = View.GONE
            } else {
                holder.rv_spl_requests.visibility = View.VISIBLE
            }
            val modifiersAdapter =
                ModifiersListAdapter(
                    context!!,
                    mList!![position].modifiersList!!,
                    mReceiptSetupItemData!!
                )
            holder.rv_modifiers.adapter = modifiersAdapter
            modifiersAdapter.notifyDataSetChanged()
            val requestsAdapter =
                SplRequestsAdapter(context!!, mList!![position].specialRequestList!!)
            holder.rv_spl_requests.adapter = requestsAdapter
            requestsAdapter.notifyDataSetChanged()
            holder.tv_currency_total.text = currencyType
            holder.tv_item_quantity.text = mList!![position].quantity.toString()
            holder.tv_item_name.text = mList!![position].itemName.toString()
            holder.tv_item_price.text = "%.2f".format(mList!![position].unitPrice)
           if (mList!![position].voidStatus!! && mList!![position].quantity==0) {
               holder.tv_item_name.paintFlags =
                   holder.tv_item_name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
               holder.tv_item_price.paintFlags =
                   holder.tv_item_price.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
           }
            if (mReceiptSetupItemData!!.displayItemQTY.toString() == "null") {
                holder.tv_item_quantity.visibility = View.VISIBLE
            } else {
                if (mReceiptSetupItemData!!.displayItemQTY.equals("1")) {
                    holder.tv_item_quantity.visibility = View.VISIBLE
                } else {
                    holder.tv_item_quantity.visibility = View.GONE
                }
            }
            if (mReceiptSetupItemData!!.itemModifiers.toString() == "null") {
                holder.rv_modifiers.visibility = View.VISIBLE
            } else {
                if (mReceiptSetupItemData!!.itemModifiers.equals("1")) {
                    holder.rv_modifiers.visibility = View.VISIBLE
                } else {
                    holder.rv_modifiers.visibility = View.GONE
                }
            }
            if (mReceiptSetupItemData!!.specialRequests.toString() == "null") {
                holder.rv_spl_requests.visibility = View.VISIBLE
            } else {
                if (mReceiptSetupItemData!!.specialRequests.equals("1")) {
                    holder.rv_spl_requests.visibility = View.VISIBLE
                } else {
                    holder.rv_spl_requests.visibility = View.GONE
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            val tv_item_quantity = view.findViewById(R.id.tv_item_quantity) as TextView
            val tv_item_price = view.findViewById(R.id.tv_item_price) as TextView
            val tv_currency_total = view.findViewById(R.id.tv_currency_total) as TextView
            val rv_modifiers = view.findViewById(R.id.rv_modifiers) as RecyclerView
            val rv_spl_requests = view.findViewById(R.id.rv_spl_requests) as RecyclerView
        }
    }

    inner class ModifiersListAdapter(
        context: Context,
        modifiers: ArrayList<ModifiersDataResponse>,
        receiptsetupmodifierdata: ReceiptSetUpDataResponse
    ) :
        RecyclerView.Adapter<ModifiersListAdapter.ViewHolder>() {

        private var context: Context? = null
        private var mList: ArrayList<ModifiersDataResponse>? = null
        private var mreceiptsetupmodifierdata: ReceiptSetUpDataResponse? = null

        init {
            this.context = context
            this.mList = modifiers
            this.mreceiptsetupmodifierdata = receiptsetupmodifierdata
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.print_modifiers_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_sub_item_name.isAllCaps = false
            holder.tv_currency_total.text = currencyType
            holder.tv_sub_item_name.text = mList!![position].modifierName.toString()
            holder.tv_sub_item_prices.text = "%.2f".format(mList!![position].modifierTotalPrice!!)
            if (mreceiptsetupmodifierdata!!.hideModifierPrices.toString() == "null") {
                holder.tv_sub_item_prices.visibility = View.VISIBLE
            } else {
                if (mreceiptsetupmodifierdata!!.hideModifierPrices.equals("1")) {
                    holder.tv_sub_item_prices.visibility = View.VISIBLE
                } else {
                    holder.tv_sub_item_prices.visibility = View.GONE
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
            var tv_sub_item_prices: TextView = view.findViewById(R.id.tv_sub_item_prices)
            var tv_currency_total: TextView = view.findViewById(R.id.tv_currency_total)
        }
    }

    inner class SplRequestsAdapter(
        context: Context,
        splRequests: ArrayList<SpecialRequestDataResponse>
    ) :
        RecyclerView.Adapter<SplRequestsAdapter.ViewHolder>() {
        private var context: Context? = null
        private var mList: ArrayList<SpecialRequestDataResponse>? = null

        init {
            this.context = context
            this.mList = splRequests
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.print_modifiers_sub_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_sub_item_name.isAllCaps = false
            holder.tv_currency_total.text = currencyType
            holder.tv_sub_item_name.text = mList!![position].name.toString()
            holder.tv_sub_item_prices.text = "%.2f".format(mList!![position].requestPrice!!)
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_sub_item_name: TextView = view.findViewById(R.id.tv_sub_item_name)
            var tv_sub_item_prices: TextView = view.findViewById(R.id.tv_sub_item_prices)
            var tv_currency_total: TextView = view.findViewById(R.id.tv_currency_total)
        }
    }

    private fun ReceiptSetUpView(finalObject: JsonObject) {
        Log.e("RECEIPTREQUEST", "" + finalObject)
        val api = ApiInterface.create()
        val call = api.getAllReceiptData(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@ViewItemsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        receiptSetUpDetails = resp.ReceiptSetUp!!
                        closedcheckRecipet = receiptSetUpDetails.closedcheckRecipet!!
                        combineItems = receiptSetUpDetails.combineItems!!
                        createdBy = receiptSetUpDetails.createdBy!!
                        createdOn = receiptSetUpDetails.createdOn!!
                        creditCardRecipets = receiptSetUpDetails.creditCardRecipets!!
                        displayCharityTip = receiptSetUpDetails.displayCharityTip!!
                        displayDiscounts = receiptSetUpDetails.displayDiscounts!!
                        displayItemQTY = receiptSetUpDetails.displayItemQTY!!
                        displayTipLine = receiptSetUpDetails.displayTipLine!!
                        displayTipPercent = receiptSetUpDetails.displayTipPercent!!
                        freeItems = receiptSetUpDetails.freeItems!!
                        freeModifiers = receiptSetUpDetails.freeModifiers!!
                        guestCount = receiptSetUpDetails.guestCount!!
                        guestDetailsDelivery = receiptSetUpDetails.guestDetailsDelivery!!
                        id = receiptSetUpDetails.id!!
                        image = receiptSetUpDetails.receiptLogo!!
                        itemModifiers = receiptSetUpDetails.itemModifiers!!
                        itemizeQuickOrder = receiptSetUpDetails.itemizeQuickOrder!!
                        itemizeTableService = receiptSetUpDetails.itemizeTableService!!
                        largeOrderNum = receiptSetUpDetails.largeOrderNum!!
                        printPayments = receiptSetUpDetails.printPayments!!
                        receiptFooter = receiptSetUpDetails.receiptFooter!!
                        receiptHeader = receiptSetUpDetails.receiptHeader!!
                        restaurantName = receiptSetUpDetails.restaurantId!!
                        serviceChargesFont = receiptSetUpDetails.serviceChargesFont!!
                        showHouseAcccounts = receiptSetUpDetails.showHouseAcccounts!!
                        specialRequests = receiptSetUpDetails.specialRequests!!
                        status = receiptSetUpDetails.status!!
                        surveyCode = receiptSetUpDetails.surveyCode!!
                        tabName = receiptSetUpDetails.tabName!!
                        tipWithGrauities = receiptSetUpDetails.tipWithGrauities!!
                        uniqueNumber = receiptSetUpDetails.uniqueNumber!!
                        itemsAdapter = ItemsListAdapter(
                            this@ViewItemsActivity,
                            orderItemsList,
                            receiptSetUpDetails
                        )
                        rv_ordered_items.adapter = itemsAdapter
                        rv_ordered_items.addItemDecoration(LinePagerIndicatorDecoration())
                        itemsAdapter.notifyDataSetChanged()
                        Log.e("RECEPTDISPLAYQUANTITY", displayItemQTY)
                    } else {
                        receiptSetUpDetails = ReceiptSetUpDataResponse()
                        itemsAdapter = ItemsListAdapter(
                            this@ViewItemsActivity,
                            orderItemsList,
                            receiptSetUpDetails
                        )
                        rv_ordered_items.adapter = itemsAdapter
                        rv_ordered_items.addItemDecoration(LinePagerIndicatorDecoration())
                        itemsAdapter.notifyDataSetChanged()
                        Toast.makeText(
                            this@ViewItemsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun OrderDetailsEmailSent(finalObject: JsonObject) {
        loading_dialog.show()
        Log.e("RECEIPTREQUEST", "" + finalObject)
        val api = ApiInterface.create()
        val call = api.orderDetailsSentToEmailApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@ViewItemsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val result = resp.result!!
                        Toast.makeText(
                            this@ViewItemsActivity,
                            result,
                            Toast.LENGTH_SHORT
                        ).show()
                        emailsent_dialog.dismiss()
                    } else {
                        Toast.makeText(
                            this@ViewItemsActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun EmailSentDialog() {
        emailsent_dialog = Dialog(this)
        emailsent_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this)
                .inflate(R.layout.email_sent_dialog, null)
        emailsent_dialog.setContentView(view)
        emailsent_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        emailsent_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        emailsent_dialog.setCanceledOnTouchOutside(false)
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        et_email = view.findViewById(R.id.et_email)
        tv_cancel.setOnClickListener {
            emailsent_dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            if (isValid()) {
                val jObj = JSONObject()
                jObj.put("email", et_email.text.toString())
                jObj.put("orderId", order_id)
                jObj.put("restaurantId", restaurantId)
                jObj.put("employeeId", mEmployeeId)
                val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
                Log.e("EMAILSENTORDER", "" + finalObject)
                OrderDetailsEmailSent(finalObject)
            }
        }
//        emailsent_dialog.show()
    }

    private fun isValid(): Boolean {
        when {
            et_email.text.toString() == "" -> {
                Toast.makeText(
                    this, "E-mail should not be empty!", Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            !isValidEmail(et_email.text.toString()) -> {
                Toast.makeText(
                    this, "Invalid E-mail address",
                    Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            else -> {
                return true
            }
        }
        return false
    }

    override fun onBackPressed() {
        if (orderItemsList.size > 0) {
            if (orderItemsList.size == 1) {
                if (orderItemsList[0].status == 3) {
                    val intent = Intent(this@ViewItemsActivity, PaymentTerminalActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    intent.putExtra("from_screen", "home")
                    intent.putExtra("employeeId", "")
                    intent.putExtra("show_tab", "open")
                    startActivity(intent)
                    finish()
                } else {
                    super.onBackPressed()
                }
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}