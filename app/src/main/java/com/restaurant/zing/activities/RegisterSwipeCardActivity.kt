package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.helpers.RecyclerItemClickListener
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.helpers.EmployeesListDataModel
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterSwipeCardActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var ll_add: LinearLayout
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var rv_employees: RecyclerView
    private lateinit var employeesList: ArrayList<EmployeesListDataModel>
    private lateinit var mSwipeDialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var loading_dialog: Dialog
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var restaurantId = ""

    private var mSelectedIndex = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_swipe_card)
        initialize()
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        // delete after service call
        dummyList()
        val adapter = EmployeeListAdapter(this, employeesList)
        rv_employees.adapter = adapter
        adapter.notifyDataSetChanged()
        rv_employees.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {}
                })
        )

        ll_add.setOnClickListener {
            mSwipeDialog.show()
        }
    }

    private fun showSwipeDialog() {
        mSwipeDialog = Dialog(this@RegisterSwipeCardActivity)
        mSwipeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@RegisterSwipeCardActivity)
                .inflate(R.layout.swipe_card_dialog, null)
        mSwipeDialog.setContentView(view)
        mSwipeDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        mSwipeDialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mSwipeDialog.setCanceledOnTouchOutside(false)
        val tv_swipe_cancel: TextView = view.findViewById(R.id.tv_swipe_cancel)
        tv_swipe_cancel.setOnClickListener {
            mSwipeDialog.dismiss()
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        ll_add = findViewById(R.id.ll_add)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        rv_employees = findViewById(R.id.rv_employees)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_employees.layoutManager = layoutManager
        rv_employees.hasFixedSize()
        val dividerItemDecoration =
            DividerItemDecoration(rv_employees.context, layoutManager.orientation)
        rv_employees.addItemDecoration(dividerItemDecoration)
        showSwipeDialog()

        employeesList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    fun dummyList() {
        employeesList.add(EmployeesListDataModel("Nagaraj K", "1"))
        employeesList.add(EmployeesListDataModel("Azam Khan", "2"))
        employeesList.add(EmployeesListDataModel("Shafaz Rafdeen", "3"))
        employeesList.add(EmployeesListDataModel("V S", "4"))
        employeesList.add(EmployeesListDataModel("Chaitanya Srikrishna", "5"))
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@RegisterSwipeCardActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent =
                            Intent(this@RegisterSwipeCardActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@RegisterSwipeCardActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@RegisterSwipeCardActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class EmployeeListAdapter(context: Context, list: ArrayList<EmployeesListDataModel>) :
        RecyclerView.Adapter<EmployeeListAdapter.ViewHolder>() {
        private var mContext: Context? = null
        private var mList: ArrayList<EmployeesListDataModel>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.employee_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        fun getItem(position: Int): EmployeesListDataModel {
            return mList!![position]
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_item_name.text = mList!![position].name
            holder.ll_item_header.setOnClickListener {
                if (mSelectedIndex == -1 || mSelectedIndex != position) {
                    mSelectedIndex = position
                    ll_add.visibility = View.VISIBLE
                    ll_switch_user.visibility = View.GONE
                } else if (mSelectedIndex == position) {
                    mSelectedIndex = -1
                    ll_add.visibility = View.GONE
                    ll_switch_user.visibility = View.VISIBLE
                }
                notifyDataSetChanged()
            }
            if (mSelectedIndex == position) {
                holder.ll_item_header.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.gradient_selected_bg)
                holder.tv_item_name.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
            } else {
                holder.ll_item_header.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.white_background)
                holder.tv_item_name.setTextColor(
                    ContextCompat.getColor(
                        mContext!!,
                        R.color.edit_text_hint
                    )
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_item_name: TextView = view.findViewById(R.id.tv_item_name)
            var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
        }
    }
}