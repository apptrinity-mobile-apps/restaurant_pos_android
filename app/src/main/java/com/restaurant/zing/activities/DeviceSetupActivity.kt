package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.CenterToast
import com.restaurant.zing.helpers.HandSetModelInfo
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n")
class DeviceSetupActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var loading_dialog: Dialog
    private lateinit var tv_update: TextView
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var rv_dining_options: RecyclerView
    private lateinit var rv_service_area_options: RecyclerView
    private lateinit var rv_cash_drawers: RecyclerView
    private lateinit var rv_prep_stations_options: RecyclerView
    private lateinit var et_device_name: EditText

    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var getDiningOptions: HashMap<String, String>
    private lateinit var getServiceAreaOptions: HashMap<String, Any>
    private lateinit var getCashDrawer: HashMap<String, String>
    private lateinit var getPrepStations: HashMap<String, String>
    private lateinit var getDeviceDetails: HashMap<String, String>
    private lateinit var allCashDrawersList: ArrayList<CashDrawersDataResponse>
    private lateinit var handSetModelInfo: HandSetModelInfo
    private var serviceAreaPrimary = false
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var restaurantId = ""
    private var dinningOptionBehaviour = ""
    private var dinningName = ""
    private var dinningId = ""
    private var serviceID = ""
    private var serviceName = ""
    private var serviceChargeId = ""
    private var revenueCenter = ""
    private var revenueCenterName = ""
    private var cashDrawerId = ""
    private var cashDrawerName = ""
    private var cashDrawerNumber = ""
    private var deviceName = ""
    private var deviceNameId = ""
    private var deviceToken = ""
    private var prepStationId = ""
    private var prepStationName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_setup)
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            deviceToken = it!!.toString()
            Log.d("token", deviceToken)
        }
        initialize()

        val obj = JSONObject()
        obj.put("restaurantId", restaurantId)
        val jObject = JsonParser.parseString(obj.toString()).asJsonObject
        Log.d("finalObject", jObject.toString())
        et_device_name.isEnabled = false
        et_device_name.setText(deviceName)
        getDinningOptions(jObject)
        getServiceAreaOptions(jObject)
        getAllCashDrawers(jObject)
        getAllPrepStations(jObject)
        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        tv_update.setOnClickListener {
            val jObj = JSONObject()
            jObj.put("createdBy", mEmployeeId)
            jObj.put("restaurantId", restaurantId)
            jObj.put("dineInOption", dinningId)
            jObj.put("serviceAreaId", serviceID)
            jObj.put("cashDrawersId", cashDrawerId)
            jObj.put("deviceName", deviceName)
            jObj.put("deviceToken", deviceToken)
            val finalObject = JsonParser.parseString(jObj.toString()).asJsonObject
            saveDeviceSetup(finalObject)
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        getServiceAreaOptions = sessionManager.getServiceAreaOptions
        getDiningOptions = sessionManager.getDiningOptions
        getCashDrawer = sessionManager.getCashDrawer
        getPrepStations = sessionManager.getPrepStation
        getDeviceDetails = sessionManager.getDeviceDetails
        handSetModelInfo = HandSetModelInfo()
//        deviceName = handSetModelInfo.getDeviceName
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        dinningId = getDiningOptions[SessionManager.DINING_OPTION_ID_KEY]!!
        dinningName = getDiningOptions[SessionManager.DINING_OPTION_NAME_KEY]!!
        dinningOptionBehaviour = getDiningOptions[SessionManager.DINING_OPTION_BEHAVIOUR_KEY]!!
        serviceID = getServiceAreaOptions[SessionManager.SERVICE_AREA_ID_KEY]!! as String
        serviceAreaPrimary =
            getServiceAreaOptions[SessionManager.SERVICE_AREA_PRIMARY_KEY]!! as Boolean
        serviceName = getServiceAreaOptions[SessionManager.SERVICE_AREA_NAME_KEY]!! as String
        serviceChargeId =
            getServiceAreaOptions[SessionManager.SERVICE_AREA_CHARGE_ID_KEY]!! as String
        revenueCenter = getServiceAreaOptions[SessionManager.REVENUE_CENTER_ID_KEY]!! as String
        revenueCenterName =
            getServiceAreaOptions[SessionManager.REVENUE_CENTER_NAME_KEY]!! as String
        cashDrawerId = getCashDrawer[SessionManager.CASH_DRAWER_ID_KEY]!!
        cashDrawerName = getCashDrawer[SessionManager.CASH_DRAWER_NAME_KEY]!!
        cashDrawerNumber = getCashDrawer[SessionManager.CASH_DRAWER_NUMBER_KEY]!!
        prepStationId = getPrepStations[SessionManager.DEVICE_PREP_STATION_ID]!!
        prepStationName = getPrepStations[SessionManager.DEVICE_PREP_STATION_NAME]!!
        deviceName = getDeviceDetails[SessionManager.DEVICE_NAME]!!
        deviceNameId = getDeviceDetails[SessionManager.DEVICE_NAME_ID]!!
        iv_back = findViewById(R.id.iv_back)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        rv_dining_options = findViewById(R.id.rv_dining_options)
        rv_service_area_options = findViewById(R.id.rv_service_area_options)
        rv_cash_drawers = findViewById(R.id.rv_cash_drawers)
        tv_update = findViewById(R.id.tv_update)
        et_device_name = findViewById(R.id.et_device_name)
        rv_prep_stations_options = findViewById(R.id.rv_prep_stations_options)
        val layoutManager =
            GridLayoutManager(this@DeviceSetupActivity, 5, LinearLayoutManager.VERTICAL, false)
        val serviceLayoutManager =
            GridLayoutManager(this@DeviceSetupActivity, 5, LinearLayoutManager.VERTICAL, false)
        val stationsLayoutManager =
            GridLayoutManager(this@DeviceSetupActivity, 5, LinearLayoutManager.VERTICAL, false)
        val cashDrawerLayoutManager =
            GridLayoutManager(this@DeviceSetupActivity, 5, LinearLayoutManager.VERTICAL, false)
        rv_dining_options.layoutManager = layoutManager
        rv_service_area_options.layoutManager = serviceLayoutManager
        rv_cash_drawers.layoutManager = cashDrawerLayoutManager
        rv_prep_stations_options.layoutManager = stationsLayoutManager
        rv_dining_options.hasFixedSize()
        rv_service_area_options.hasFixedSize()
        rv_prep_stations_options.hasFixedSize()
        rv_cash_drawers.hasFixedSize()
        allCashDrawersList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@DeviceSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent =
                            Intent(this@DeviceSetupActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@DeviceSetupActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(
                            getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@DeviceSetupActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getDinningOptions(json_obj: JsonObject) {
        val dinningOptionsArray = ArrayList<GetDinningOptionsListResponseList>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getDinningOptionsListApi(json_obj)
        call.enqueue(object : Callback<GetDinningOptionsListResponse> {
            override fun onFailure(call: Call<GetDinningOptionsListResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@DeviceSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetDinningOptionsListResponse>,
                response: Response<GetDinningOptionsListResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus!! == 1) {
                        for (i in 0 until response.body()!!.dinningOptionsList!!.size) {
                            dinningOptionsArray.add(response.body()!!.dinningOptionsList!![i])
                        }
                    } else {
                        Toast.makeText(
                            this@DeviceSetupActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val dinningOptionAdapter =
                        DinningOptionAdapter(this@DeviceSetupActivity, dinningOptionsArray)
                    rv_dining_options.adapter = dinningOptionAdapter
                    dinningOptionAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getServiceAreaOptions(json_obj: JsonObject) {
        val serviceAreaArray = ArrayList<ServiceAreasListResponse>()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getServiceAreasApi(json_obj)
        call.enqueue(object : Callback<ServiceAreasResponse> {
            override fun onFailure(call: Call<ServiceAreasResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@DeviceSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ServiceAreasResponse>,
                response: Response<ServiceAreasResponse>?
            ) {
                try {
                    val resp = response!!.body()!!
                    if (resp.responseStatus!! == 1) {
                        val list = resp.service_areasList!!
                        for (i in 0 until list.size) {
                            serviceAreaArray.add(list[i])
                        }
                    } else {
                        Toast.makeText(
                            this@DeviceSetupActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val serviceAreaOptionAdapter =
                        ServiceAreaOptionsAdapter(this@DeviceSetupActivity, serviceAreaArray)
                    rv_service_area_options.adapter = serviceAreaOptionAdapter
                    serviceAreaOptionAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllCashDrawers(finalObject: JsonObject) {
        loading_dialog.show()
        allCashDrawersList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getAllCashDrawersListApi(finalObject)
        call.enqueue(object : Callback<CashDrawersListDataResponse> {
            override fun onFailure(call: Call<CashDrawersListDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeviceSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CashDrawersListDataResponse>,
                response: Response<CashDrawersListDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        val openCashDrawersList = resp.cashdrawersOpenList!!
                        val closedCashDrawersList = resp.cashdrawersClosedList!!
                        val activeCashDrawersList = resp.cashdrawersActiveList!!
                        if (openCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(openCashDrawersList)
                        }
                        if (closedCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(closedCashDrawersList)
                        }
                        if (activeCashDrawersList.size > 0) {
                            allCashDrawersList.addAll(activeCashDrawersList)
                        }
                    } else {
                        Toast.makeText(
                            this@DeviceSetupActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val cashDrawerAdapter =
                        CashDrawerAdapter(this@DeviceSetupActivity, allCashDrawersList)
                    rv_cash_drawers.adapter = cashDrawerAdapter
                    cashDrawerAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun saveDeviceSetup(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addActiveCashDrawerApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeviceSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.saveDiningOptions(
                            dinningId,
                            dinningName,
                            dinningOptionBehaviour
                        )
                        sessionManager.saveServiceAreaOptions(
                            serviceID,
                            serviceAreaPrimary,
                            serviceName,
                            serviceChargeId,
                            revenueCenter,
                            revenueCenterName
                        )
                        sessionManager.saveCashDrawer(
                            cashDrawerId,
                            cashDrawerName,
                            cashDrawerNumber
                        )
                        sessionManager.savePrepStation(prepStationId, prepStationName)
                        sessionManager.saveDeviceDetails(deviceName, resp.deviceId!!)
                        CenterToast().showToast(this@DeviceSetupActivity, "Updated successfully.")
                    } else {
                        Toast.makeText(
                            this@DeviceSetupActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllPrepStations(finalObject: JsonObject) {
        loading_dialog.show()
        val stationsArray = ArrayList<PreparationStationsDataResponse>()
        val api = ApiInterface.create()
        val call = api.getAllPrepStationsApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DeviceSetupActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus!! == 1) {
                        val list = resp.stationsList!!
                        for (i in 0 until list.size) {
                            stationsArray.add(list[i])
                        }
                    } else {
                        Toast.makeText(
                            this@DeviceSetupActivity,
                            response.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    val prepStationOptionAdapter =
                        PrepStationOptionAdapter(this@DeviceSetupActivity, stationsArray)
                    rv_prep_stations_options.adapter = prepStationOptionAdapter
                    prepStationOptionAdapter.notifyDataSetChanged()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class ServiceAreaOptionsAdapter(
        context: Context,
        serviceAreaOptionArray: ArrayList<ServiceAreasListResponse>
    ) :
        RecyclerView.Adapter<ServiceAreaOptionsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<ServiceAreasListResponse>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = serviceAreaOptionArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.service_area_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].serviceName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            if (serviceID == mList!![position].id.toString()) {
                holder.rb_name.isChecked = true
            }
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                serviceID = mList!![position].id.toString()
                serviceAreaPrimary = mList!![position].primary!!
                serviceName = mList!![position].serviceName.toString()
                serviceChargeId = mList!![position].serviceChargeId.toString()
                revenueCenter = mList!![position].revenueCenter.toString()
                revenueCenterName = mList!![position].revenueCenterName.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    inner class DinningOptionAdapter(
        context: Context,
        dinningOptionArray: ArrayList<GetDinningOptionsListResponseList>
    ) :
        RecyclerView.Adapter<DinningOptionAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<GetDinningOptionsListResponseList>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = dinningOptionArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.service_area_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].optionName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            if (dinningId == mList!![position].id.toString()) {
                holder.rb_name.isChecked = true
            }
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                dinningId = mList!![position].id.toString()
                dinningName = mList!![position].optionName.toString()
                dinningOptionBehaviour = mList!![position].behavior.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    inner class PrepStationOptionAdapter(
        context: Context,
        prepStationArray: ArrayList<PreparationStationsDataResponse>
    ) :
        RecyclerView.Adapter<PrepStationOptionAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<PreparationStationsDataResponse>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = prepStationArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.service_area_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].stationName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            if (prepStationId == mList!![position].id.toString()) {
                holder.rb_name.isChecked = true
            }
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                prepStationName = mList!![position].stationName.toString()
                prepStationId = mList!![position].id.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }

    inner class CashDrawerAdapter(
        context: Context,
        cashDrawerArray: ArrayList<CashDrawersDataResponse>
    ) :
        RecyclerView.Adapter<CashDrawerAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<CashDrawersDataResponse>? = null
        var selectedPosition = -1

        init {
            this.mContext = context
            this.mList = cashDrawerArray
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.service_area_options_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.rb_name.text = mList!![position].cashDrawerName.toString()
            holder.rb_name.isChecked = (position == selectedPosition)
            if (cashDrawerId == mList!![position].id.toString()) {
                holder.rb_name.isChecked = true
            }
            holder.rb_name.setOnClickListener {
                selectedPosition = position
                cashDrawerId = mList!![position].id.toString()
                cashDrawerName = mList!![position].cashDrawerName.toString()
                cashDrawerNumber = mList!![position].cashDrawer.toString()
                notifyDataSetChanged()
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val rb_name = view.findViewById(R.id.rb_name) as RadioButton
        }
    }
}