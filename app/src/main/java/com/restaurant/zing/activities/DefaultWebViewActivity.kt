package com.restaurant.zing.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.apiInterface.AllDataResponse
import com.restaurant.zing.apiInterface.ApiInterface
import com.restaurant.zing.helpers.ConstantURLs
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DefaultWebViewActivity : AppCompatActivity() {

    lateinit var loading_dialog: Dialog
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var restaurantId = ""
    private var deviceToken = ""
    private var title = ""
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private lateinit var web_view: WebView
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var iv_back: ImageView
    private lateinit var tv_activity_name: TextView

    @SuppressLint("AddJavascriptInterface", "SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_default_web_view)
        try {
            title = intent.getStringExtra("title")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        init()
        loadingDialog()
        tv_activity_name.text = title
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            deviceToken = it!!.toString()
            Log.d("token", deviceToken)
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }
        web_view.settings.javaScriptEnabled = true
        web_view.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                loading_dialog.show()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                loading_dialog.dismiss()
            }
        }
        web_view.loadUrl(ConstantURLs.HOME)

    }

    private fun init() {
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        web_view = findViewById(R.id.web_view)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        iv_back = findViewById(R.id.iv_back)
        tv_activity_name = findViewById(R.id.tv_activity_name)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@DefaultWebViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@DefaultWebViewActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@DefaultWebViewActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@DefaultWebViewActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (web_view.canGoBack()) {
                        web_view.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy() {
        web_view.removeJavascriptInterface("HtmlViewer")
        super.onDestroy()
    }
}