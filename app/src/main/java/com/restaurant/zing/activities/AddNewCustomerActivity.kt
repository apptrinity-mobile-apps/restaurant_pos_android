package com.restaurant.zing.activities

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.restaurant.zing.adapters.CitiesListAdapter
import com.restaurant.zing.adapters.CountriesListAdapter
import com.restaurant.zing.adapters.StatesListAdapter
import com.restaurant.zing.apiInterface.*
import com.restaurant.zing.helpers.SessionManager
import com.restaurant.zing.R
import com.restaurant.zing.helpers.isValidEmail
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddNewCustomerActivity : AppCompatActivity() {

    private lateinit var iv_back: ImageView
    private lateinit var tv_add_new_customer: TextView
    private lateinit var et_first_name: EditText
    private lateinit var et_last_name: EditText
    private lateinit var et_phone_number: EditText
    private lateinit var et_email: EditText
    private lateinit var loading_dialog: Dialog
    private lateinit var ll_switch_user: LinearLayout
    private lateinit var sessionManager: SessionManager
    private lateinit var employeeDetails: HashMap<String, String>
    private lateinit var restaurantDetails: HashMap<String, String>
    private var mEmployeeId = ""
    private var mEmpJobId = ""
    private var restaurantId = ""
    private var countryId = ""
    private var stateId = ""
    private var cityId = ""
    private lateinit var countriesList: ArrayList<CountriesResponse>
    private lateinit var statesList: ArrayList<StatesResponse>
    private lateinit var citiesList: ArrayList<CitiesResponse>
    private lateinit var countriesListAdapter: CountriesListAdapter
    private lateinit var statesListAdapter: StatesListAdapter
    private lateinit var citiesListAdapter: CitiesListAdapter
    private lateinit var sp_country: Spinner
    private lateinit var sp_state: Spinner
    private lateinit var sp_city: Spinner
    private lateinit var dummyCountry: CountriesResponse
    private lateinit var dummyState: StatesResponse
    private lateinit var dummyCity: CitiesResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_customer)

        initialize()

        /*val jObj = JSONObject()
        jObj.put("employeeId", mEmployeeId)
        jObj.put("restaurantId", restaurantId)
        val jsonObject = JsonParser.parseString(jObj.toString()).asJsonObject
        getCountries(jsonObject)*/

        iv_back.setOnClickListener {
            onBackPressed()
        }
        ll_switch_user.setOnClickListener {
            val jsonObj = JSONObject()
            jsonObj.put("restaurantId", restaurantId)
            jsonObj.put("userId", mEmployeeId)
            jsonObj.put("jobId", mEmpJobId)
            val final_object = JsonParser.parseString(jsonObj.toString()).asJsonObject
            employeeLogout(final_object)
        }

        sp_country.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                countryId = countriesListAdapter.getSelectedItem(position)._id!!
                Log.d("add customer countryId", countryId)
                if (countryId == "dummy") {
                    // do nothing
                } else {
                    val jsonObj = JSONObject()
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("countryId", countryId)
                    val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    getStates(finalObject)
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        sp_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                stateId = statesListAdapter.getSelectedItem(position)._id!!
                Log.d("add customer stateId", stateId)
                if (stateId == "dummy") {
                    // do nothing
                } else {
                    val jsonObj = JSONObject()
                    jsonObj.put("employeeId", mEmployeeId)
                    jsonObj.put("restaurantId", restaurantId)
                    jsonObj.put("stateId", stateId)
                    val finalObject = JsonParser.parseString(jsonObj.toString()).asJsonObject
                    getCities(finalObject)
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        sp_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                cityId = citiesListAdapter.getSelectedItem(position)._id!!
                Log.d("add customer cityId", cityId)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_add_new_customer.setOnClickListener {
            if (isValid()) {
                val obj = JSONObject()
                obj.put("firstName", et_first_name.text.toString())
                obj.put("lastName", et_last_name.text.toString())
                obj.put("phoneNumber", et_phone_number.text.toString())
                obj.put("email", et_email.text.toString())
                obj.put("restaurantId", restaurantId)
                obj.put("createdBy", mEmployeeId)
                obj.put("status", 1)
                /*obj.put("countryId", countryId)
                obj.put("stateId", stateId)
                obj.put("cityId", cityId)*/
                val jObject = JsonParser.parseString(obj.toString()).asJsonObject
                Log.e("login_input_data", jObject.toString())
                AddCustomerResponse(jObject)
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        employeeDetails = sessionManager.getUserDetails
        restaurantDetails = sessionManager.getRestaurantDetails
        mEmployeeId = employeeDetails[SessionManager.ID_KEY]!!
        mEmpJobId = employeeDetails[SessionManager.JOB_ID_KEY]!!
        restaurantId = restaurantDetails[SessionManager.RESTAURANT_ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        tv_add_new_customer = findViewById(R.id.tv_add_new_customer)
        et_first_name = findViewById(R.id.et_first_name)
        et_last_name = findViewById(R.id.et_last_name)
        et_phone_number = findViewById(R.id.et_phone_number)
        et_email = findViewById(R.id.et_email)
        ll_switch_user = findViewById(R.id.ll_switch_user)
        sp_country = findViewById(R.id.sp_country)
        sp_state = findViewById(R.id.sp_state)
        sp_city = findViewById(R.id.sp_city)
        countriesList = ArrayList()
        statesList = ArrayList()
        citiesList = ArrayList()

        dummyCountry = CountriesResponse()
        dummyCountry._id = "dummy"
        dummyCountry.capital = ""
        dummyCountry.countryCode = ""
        dummyCountry.countryId = 0
        dummyCountry.currency = ""
        dummyCountry.iso3 = ""
        dummyCountry.name = "Please select country."
        dummyCountry.phoneCode = ""
        dummyCountry.status = 0

        dummyState = StatesResponse()
        dummyState._id = "dummy"
        dummyState.countryCode = ""
        dummyState.countryId = 0
        dummyState.name = "Please select state."
        dummyState.stateCode = ""
        dummyState.stateId = 0
        dummyState.status = 0

        dummyCity = CitiesResponse()
        dummyCity._id = "dummy"
        dummyCity.cityId = 0
        dummyCity.countryCode = ""
        dummyCity.countryId = 0
        dummyCity.latitude = ""
        dummyCity.longitude = ""
        dummyCity.name = "Please select city."
        dummyCity.stateCode = ""
        dummyCity.stateId = 0
        dummyCity.status = 0
    }

    private fun isValid(): Boolean {
        when {
            et_first_name.text.toString() == "" -> {
                Toast.makeText(
                    this@AddNewCustomerActivity, "First name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_first_name.requestFocus()
            }
            et_last_name.text.toString() == "" -> {
                Toast.makeText(
                    this@AddNewCustomerActivity, "Last name should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_last_name.requestFocus()
            }
            et_phone_number.text.toString() == "" -> {
                Toast.makeText(
                    this@AddNewCustomerActivity, "Phone number should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_phone_number.requestFocus()
            }
            et_email.text.toString() == "" -> {
                Toast.makeText(
                    this@AddNewCustomerActivity, "E-mail should not be empty!",
                    Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            !isValidEmail(et_email.text.toString()) -> {
                Toast.makeText(
                    this@AddNewCustomerActivity, "Invalid E-mail address",
                    Toast.LENGTH_SHORT
                ).show()
                et_email.requestFocus()
            }
            /* countryId == "" || countryId == "dummy" -> {
                 Toast.makeText(
                     this@AddNewCustomerActivity, "Please select valid country!",
                     Toast.LENGTH_SHORT
                 ).show()
             }
             stateId == "" || stateId == "dummy" -> {
                 Toast.makeText(
                     this@AddNewCustomerActivity, "Please select valid state!",
                     Toast.LENGTH_SHORT
                 ).show()
             }
             cityId == "" || cityId == "dummy" -> {
                 Toast.makeText(
                     this@AddNewCustomerActivity, "Please select valid city!",
                     Toast.LENGTH_SHORT
                 ).show()
             }*/
            else -> {
                return true
            }
        }
        return false
    }

    private fun AddCustomerResponse(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.addCustomerApi(json_obj)
        call.enqueue(object : Callback<AddNewCustomerResponse> {
            override fun onFailure(call: Call<AddNewCustomerResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<AddNewCustomerResponse>,
                response: Response<AddNewCustomerResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        /* result is customer Id */
                        val intent =
                            Intent(this@AddNewCustomerActivity, CustomerRecordActivity::class.java)
                        intent.putExtra("customerId", resp.result.toString())
                        intent.putExtra("firstName", et_first_name.text.toString())
                        intent.putExtra("lastName", et_last_name.text.toString())
                        intent.putExtra("email", et_email.text.toString())
                        intent.putExtra("phoneNumber", et_phone_number.text.toString())
                        intent.putExtra("credits", getString(R.string.default_value_0))
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            this@AddNewCustomerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCountries(final_object: JsonObject) {
        loading_dialog.show()
        countriesList.clear()
        countriesList.add(dummyCountry)
        statesList.add(dummyState)
        citiesList.add(dummyCity)
        // resetting state & city spinners
        statesListAdapter = StatesListAdapter(
            this@AddNewCustomerActivity,
            statesList
        )
        sp_state.adapter = statesListAdapter
        citiesListAdapter = CitiesListAdapter(
            this@AddNewCustomerActivity,
            citiesList
        )
        sp_city.adapter = citiesListAdapter
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllCountriesApi(final_object)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("getAllCountriesApi", "Exception  $t")
                    Toast.makeText(
                        this@AddNewCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        if (resp.contries!!.size > 0) {
                            countriesList.addAll(resp.contries)
                        }
                    } else {
                        Toast.makeText(
                            this@AddNewCustomerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    countriesListAdapter = CountriesListAdapter(
                        this@AddNewCustomerActivity,
                        countriesList
                    )
                    sp_country.adapter = countriesListAdapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getStates(final_object: JsonObject) {
        loading_dialog.show()
        statesList.clear()
        statesList.add(dummyState)
        citiesList.add(dummyCity)
        // resetting city spinner
        citiesListAdapter = CitiesListAdapter(
            this@AddNewCustomerActivity,
            citiesList
        )
        sp_city.adapter = citiesListAdapter
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllStatesApi(final_object)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("getAllStatesApi", "Exception  $t")
                    Toast.makeText(
                        this@AddNewCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        if (resp.states!!.size > 0) {
                            statesList.addAll(resp.states)
                        }
                    } else {
                        Toast.makeText(
                            this@AddNewCustomerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    statesListAdapter = StatesListAdapter(
                        this@AddNewCustomerActivity,
                        statesList
                    )
                    sp_state.adapter = statesListAdapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCities(final_object: JsonObject) {
        loading_dialog.show()
        citiesList.clear()
        citiesList.add(dummyCity)
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getAllCitiesApi(final_object)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("getAllCitiesApi", "Exception  $t")
                    Toast.makeText(
                        this@AddNewCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == 1) {
                        if (resp.cities!!.size > 0) {
                            citiesList.addAll(resp.cities)
                        }
                    } else {
                        Toast.makeText(
                            this@AddNewCustomerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    citiesListAdapter = CitiesListAdapter(
                        this@AddNewCustomerActivity,
                        citiesList
                    )
                    sp_city.adapter = citiesListAdapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun employeeLogout(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.logOutApi(finalObject)
        call.enqueue(object : Callback<AllDataResponse> {
            override fun onFailure(call: Call<AllDataResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@AddNewCustomerActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllDataResponse>,
                response: Response<AllDataResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == 1) {
                        sessionManager.clearSession()
                        val intent = Intent(this@AddNewCustomerActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else if (resp.responseStatus == 2) {
                        val alert = AlertDialog.Builder(this@AddNewCustomerActivity)
                        alert.setTitle("Alert!")
                        alert.setMessage(resp.result.toString() + " Please close all Stay orders.")
                        alert.setPositiveButton(
                            getString(R.string.ok)
                        ) { dialog, which -> dialog!!.dismiss() }
                        alert.show()
                    } else {
                        Toast.makeText(
                            this@AddNewCustomerActivity,
                            resp.result.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }
}