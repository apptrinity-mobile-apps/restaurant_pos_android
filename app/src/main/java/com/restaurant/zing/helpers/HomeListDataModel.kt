package com.restaurant.zing.helpers

data class HomeListDataModel(val name: String, val icon: Int)