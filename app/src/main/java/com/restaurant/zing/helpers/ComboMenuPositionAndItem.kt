package com.restaurant.zing.helpers

import java.io.Serializable

class ComboMenuPositionAndItem(
    var menu_position: Int,
    var name: String,
    var sizeName: String,
    var menu_type: String,
    var item: ComboBogoCustomPojo
) : Serializable