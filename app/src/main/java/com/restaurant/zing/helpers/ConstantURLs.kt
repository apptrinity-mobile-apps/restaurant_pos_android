package com.restaurant.zing.helpers

object ConstantURLs {
    // all restaurant URLs goes here
    val BASE_URL = "http://3.14.187.24"
//    val BASE_URL = "http://18.190.55.150"
    val TABLE_SERVICE = "${BASE_URL}/zingangular/mobile-tables"
    val HOME = "${BASE_URL}/zingangular/login"
}