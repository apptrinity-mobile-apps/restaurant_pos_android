package com.restaurant.zing.helpers

import com.restaurant.zing.apiInterface.*
import java.io.Serializable

class ComboBogoCustomPojo(
    var comboOrBogoId: String,
    var comboOrBogo: String,
    var basePrice: Double,
    var discountAmount: Int,
    var discountId: String,
    var discountName: String,
    var discountType: String,
    var discountValue: Int,
    var id: String,
    var isBogo: Boolean,
    var isCombo: Boolean,
    var discountApplicable: Boolean,
    var itemId: String,
    var itemName: String,
    var itemType: String,
    var menuGroupId: String,
    var menuId: String,
    var modifiersList: ArrayList<GetAllModifierListResponseList>,
    var name: String,
    var pricingStrategy: Int,
    var quantity: Int,
    var specialRequestList: ArrayList<SpecialRequestDataResponse>,
    var timePriceList: ArrayList<TimePrice>,
    var sizeList: ArrayList<getSizeList>,
    var menuPriceList: ArrayList<getSizeList>,
    var totalPrice: Int,
    var type: String,
    var uniqueNumber: String,
    var unitPrice: Int,
// taxes
    var taxesList: ArrayList<TaxRatesDetailsResponse>,
    var diningOptionTaxException: Boolean,
    var diningTaxOption: Boolean,
    var taxIncludeOption: Boolean
) : Serializable