package com.restaurant.zing.helpers

data class VoidItemsDataModelNew(
    var itemId: String,
    var itemName: String,
    var itemQty: Int,
    var comboId: String,
    var bogoId: String,
    var comboUniqueId: String,
    var bogoUniqueId: String,
    var isCombo: Boolean,
    var isBogo: Boolean,
    var voidStatus: Boolean,
    var isSelected: Boolean
)