package com.restaurant.zing.helpers

data class VoidItemsDataModel(
    var id: String,
    var qty: Int
)