package com.restaurant.zing.helpers

data class EmployeesListDataModel(val name: String, val id: String)