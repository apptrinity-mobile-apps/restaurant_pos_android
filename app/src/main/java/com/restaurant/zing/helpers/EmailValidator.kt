package com.restaurant.zing.helpers

import java.util.regex.Pattern

fun isValidEmail(email: String): Boolean {
    val emailPattern =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    val pattern = Pattern.compile(emailPattern)
    val matcher = pattern.matcher(email)
    return matcher.matches()
}