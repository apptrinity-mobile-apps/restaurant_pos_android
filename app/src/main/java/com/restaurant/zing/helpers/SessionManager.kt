package com.restaurant.zing.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class SessionManager(context: Context) {
    private var prefs: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        prefs = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        editor = prefs.edit()
    }

    val isLoggedIn: Boolean
        get() = prefs.getBoolean(IS_LOGGED_IN, false)

    fun clearSession() {
        editor.putBoolean(IS_LOGGED_IN, false)
        editor.putString(TEMP_ORDER_LIST, "")
        editor.commit()
    }

    fun logout() {
        editor.clear()
        editor.commit()
    }

    fun loginDetails(
        id: String,
        empId: String,
        firstName: String,
        lastName: String,
        jobId: String,
        profilePic: String
    ) {
        editor.putBoolean(IS_LOGGED_IN, true)
        editor.putString(ID_KEY, id)
        editor.putString(RESTAURANT_EMPLOYEE_ID_KEY, empId)
        editor.putString(EMPLOYEE_FIRST_NAME_KEY, firstName)
        editor.putString(EMPLOYEE_LAST_NAME_KEY, lastName)
        editor.putString(JOB_ID_KEY, jobId)
        editor.putString(EMPLOYEE_PROFILE_PIC_KEY, profilePic)
        editor.commit()
    }

    fun tableServiceData(
        noofGuests: String,
        tableNumber: String
    ) {
        editor.putString(NO_OF_GUESTS, noofGuests)
        editor.putString(TABLE_NUMBER, tableNumber)
        editor.commit()
    }

    val getTableServiceData: HashMap<String, String>
        get() {
            val tableServiceData = HashMap<String, String>()
            tableServiceData[NO_OF_GUESTS] = prefs.getString(NO_OF_GUESTS, "")!!
            tableServiceData[TABLE_NUMBER] = prefs.getString(TABLE_NUMBER, "")!!
            return tableServiceData
        }

    val getUserDetails: HashMap<String, String>
        get() {
            val userDetails = HashMap<String, String>()
            userDetails[ID_KEY] = prefs.getString(ID_KEY, "")!!
            userDetails[RESTAURANT_EMPLOYEE_ID_KEY] =
                prefs.getString(RESTAURANT_EMPLOYEE_ID_KEY, "")!!
            userDetails[EMPLOYEE_FIRST_NAME_KEY] = prefs.getString(EMPLOYEE_FIRST_NAME_KEY, "")!!
            userDetails[EMPLOYEE_LAST_NAME_KEY] = prefs.getString(EMPLOYEE_LAST_NAME_KEY, "")!!
            userDetails[JOB_ID_KEY] = prefs.getString(JOB_ID_KEY, "")!!
            userDetails[EMPLOYEE_PROFILE_PIC_KEY] = prefs.getString(EMPLOYEE_PROFILE_PIC_KEY, "")!!
            return userDetails
        }

    fun accountAdminPermissions(
        dataExportConfig: Boolean,
        financialAccounts: Boolean,
        manageIntegrations: Boolean,
        userPermissions: Boolean
    ) {
        editor.putBoolean(DATA_EXPORT_CONFIG_ADMIN_ACCESS_KEY, dataExportConfig)
        editor.putBoolean(FINANCIAL_ACCOUNTS_ADMIN_ACCESS_KEY, financialAccounts)
        editor.putBoolean(MANAGE_INTEGRATIONS_ADMIN_ACCESS_KEY, manageIntegrations)
        editor.putBoolean(USER_PERMISSIONS_ADMIN_ACCESS_KEY, userPermissions)
        editor.commit()
    }

    val getAccountAdminAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[DATA_EXPORT_CONFIG_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(DATA_EXPORT_CONFIG_ADMIN_ACCESS_KEY, false)
            userPermissions[FINANCIAL_ACCOUNTS_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(FINANCIAL_ACCOUNTS_ADMIN_ACCESS_KEY, false)
            userPermissions[MANAGE_INTEGRATIONS_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(MANAGE_INTEGRATIONS_ADMIN_ACCESS_KEY, false)
            userPermissions[USER_PERMISSIONS_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(USER_PERMISSIONS_ADMIN_ACCESS_KEY, false)
            return userPermissions
        }

    fun deliveryAccessPermissions(
        cancelDispatch: Boolean,
        completeDelivery: Boolean,
        deliveryMode: Boolean,
        dispatchDriver: Boolean,
        updateAllDeliveryOrders: Boolean,
        updateDriver: Boolean
    ) {
        editor.putBoolean(CANCEL_DISPATCH_DELIVERY_ACCESS_KEY, cancelDispatch)
        editor.putBoolean(COMPLETE_DELIVERY_DELIVERY_ACCESS_KEY, completeDelivery)
        editor.putBoolean(DELIVERY_MODE_DELIVERY_ACCESS_KEY, deliveryMode)
        editor.putBoolean(DISPATCH_DRIVER_DELIVERY_ACCESS_KEY, dispatchDriver)
        editor.putBoolean(UPDATE_ALL_DELIVERY_ORDERS_DELIVERY_ACCESS_KEY, updateAllDeliveryOrders)
        editor.putBoolean(UPDATE_DRIVER_DELIVERY_ACCESS_KEY, updateDriver)
        editor.commit()
    }

    val getDeliveryAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[CANCEL_DISPATCH_DELIVERY_ACCESS_KEY] =
                prefs.getBoolean(CANCEL_DISPATCH_DELIVERY_ACCESS_KEY, false)
            userPermissions[COMPLETE_DELIVERY_DELIVERY_ACCESS_KEY] =
                prefs.getBoolean(COMPLETE_DELIVERY_DELIVERY_ACCESS_KEY, false)
            userPermissions[DELIVERY_MODE_DELIVERY_ACCESS_KEY] =
                prefs.getBoolean(DELIVERY_MODE_DELIVERY_ACCESS_KEY, false)
            userPermissions[DISPATCH_DRIVER_DELIVERY_ACCESS_KEY] =
                prefs.getBoolean(DISPATCH_DRIVER_DELIVERY_ACCESS_KEY, false)
            userPermissions[UPDATE_ALL_DELIVERY_ORDERS_DELIVERY_ACCESS_KEY] =
                prefs.getBoolean(UPDATE_ALL_DELIVERY_ORDERS_DELIVERY_ACCESS_KEY, false)
            userPermissions[UPDATE_DRIVER_DELIVERY_ACCESS_KEY] =
                prefs.getBoolean(UPDATE_DRIVER_DELIVERY_ACCESS_KEY, false)
            return userPermissions
        }

    fun deviceSetupAccessPermissions(
        advancedTerminalSetup: Boolean,
        kdsAndOrderScreenSetup: Boolean,
        terminalSetup: Boolean
    ) {
        editor.putBoolean(ADVANCED_TERMINAL_DEFAULT_SETUP_ACCESS_KEY, advancedTerminalSetup)
        editor.putBoolean(KD_AND_ORDER_SETUP_DEFAULT_SETUP_ACCESS_KEY, kdsAndOrderScreenSetup)
        editor.putBoolean(TERMINAL_SETUP_DEFAULT_SETUP_ACCESS_KEY, terminalSetup)
        editor.commit()
    }

    val getDeviceSetupAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[ADVANCED_TERMINAL_DEFAULT_SETUP_ACCESS_KEY] =
                prefs.getBoolean(ADVANCED_TERMINAL_DEFAULT_SETUP_ACCESS_KEY, false)
            userPermissions[KD_AND_ORDER_SETUP_DEFAULT_SETUP_ACCESS_KEY] =
                prefs.getBoolean(KD_AND_ORDER_SETUP_DEFAULT_SETUP_ACCESS_KEY, false)
            userPermissions[TERMINAL_SETUP_DEFAULT_SETUP_ACCESS_KEY] =
                prefs.getBoolean(TERMINAL_SETUP_DEFAULT_SETUP_ACCESS_KEY, false)
            return userPermissions
        }

    fun managerAccessPermissions(
        adjustCashDrawerStartBalance: Boolean,
        bulkClosePaidChecks: Boolean,
        bulkTransferCheck: Boolean,
        bulkVoidOpenChecks: Boolean,
        cashDrawerLockdown: Boolean,
        cashDrawersBlind: Boolean,
        cashDrawersFull: Boolean,
        closeOutDay: Boolean,
        discounts: Boolean,
        editSentItems: Boolean,
        editTimeEntries: Boolean,
        endBreaksEarly: Boolean,
        findChecks: Boolean,
        giftCardAdjustment: Boolean,
        largeCashOverOrUnder: Boolean,
        logBook: Boolean,
        negativeDeclaredTips: Boolean,
        openItems: Boolean,
        otherPaymentTypes: Boolean,
        payout: Boolean,
        registerSwipeCards: Boolean,
        sendNotifications: Boolean,
        shiftReview: Boolean,
        taxExempt: Boolean,
        throttleOnlineOrders: Boolean,
        transferOrRewardsAdjustment: Boolean,
        unlinkedRefunds: Boolean,
        voidItemOrOrders: Boolean,
        voidOrRefundPayments: Boolean
    ) {
        editor.putBoolean(
            ADJUST_CASH_DRAWER_START_BALANCE_MANAGER_ACCESS_KEY,
            adjustCashDrawerStartBalance
        )
        editor.putBoolean(BULK_CLOSE_PAID_CHECKS_MANAGER_ACCESS_KEY, bulkClosePaidChecks)
        editor.putBoolean(BULK_TRANSFER_CHECK_MANAGER_ACCESS_KEY, bulkTransferCheck)
        editor.putBoolean(BULK_VOID_OPEN_CHECKS_MANAGER_ACCESS_KEY, bulkVoidOpenChecks)
        editor.putBoolean(CASH_DRAWER_LOCK_DOWN_MANAGER_ACCESS_KEY, cashDrawerLockdown)
        editor.putBoolean(CASH_DRAWERS_BLIND_MANAGER_ACCESS_KEY, cashDrawersBlind)
        editor.putBoolean(CASH_DRAWERS_FULL_MANAGER_ACCESS_KEY, cashDrawersFull)
        editor.putBoolean(CLOSE_OUT_DAY_MANAGER_ACCESS_KEY, closeOutDay)
        editor.putBoolean(DISCOUNTS_MANAGER_ACCESS_KEY, discounts)
        editor.putBoolean(EDIT_SENT_ITEMS_MANAGER_ACCESS_KEY, editSentItems)
        editor.putBoolean(EDIT_TIME_ENTRIES_MANAGER_ACCESS_KEY, editTimeEntries)
        editor.putBoolean(END_BREAKS_EARLY_MANAGER_ACCESS_KEY, endBreaksEarly)
        editor.putBoolean(FIND_CHECKS_MANAGER_ACCESS_KEY, findChecks)
        editor.putBoolean(GIFT_CARD_ADJUSTMENT_MANAGER_ACCESS_KEY, giftCardAdjustment)
        editor.putBoolean(LARGE_CASH_OVER_UNDER_MANAGER_ACCESS_KEY, largeCashOverOrUnder)
        editor.putBoolean(LOGBOOK_MANAGER_ACCESS_KEY, logBook)
        editor.putBoolean(NEGATIVE_DECLARED_TIPS_MANAGER_ACCESS_KEY, negativeDeclaredTips)
        editor.putBoolean(OPEN_ITEMS_MANAGER_ACCESS_KEY, openItems)
        editor.putBoolean(OTHER_PAYMENT_TYPES_MANAGER_ACCESS_KEY, otherPaymentTypes)
        editor.putBoolean(PAYOUT_MANAGER_ACCESS_KEY, payout)
        editor.putBoolean(REGISTER_SWIPE_CARDS_MANAGER_ACCESS_KEY, registerSwipeCards)
        editor.putBoolean(SEND_NOTIFICATIONS_MANAGER_ACCESS_KEY, sendNotifications)
        editor.putBoolean(SHIFT_REVIEW_MANAGER_ACCESS_KEY, shiftReview)
        editor.putBoolean(TAX_EXEMPT_MANAGER_ACCESS_KEY, taxExempt)
        editor.putBoolean(THROTTLE_ONLINE_ORDERS_MANAGER_ACCESS_KEY, throttleOnlineOrders)
        editor.putBoolean(
            TRANSFER_OR_REWARDS_ADJUSTMENT_MANAGER_ACCESS_KEY,
            transferOrRewardsAdjustment
        )
        editor.putBoolean(UNLINK_REFUNDS_MANAGER_ACCESS_KEY, unlinkedRefunds)
        editor.putBoolean(VOID_ITEMS_ORDERS_MANAGER_ACCESS_KEY, voidItemOrOrders)
        editor.putBoolean(VOID_OR_REFUND_PAYMENTS_MANAGER_ACCESS_KEY, voidOrRefundPayments)
        editor.commit()
    }

    val getManagerAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[ADJUST_CASH_DRAWER_START_BALANCE_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(ADJUST_CASH_DRAWER_START_BALANCE_MANAGER_ACCESS_KEY, false)
            userPermissions[BULK_CLOSE_PAID_CHECKS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(BULK_CLOSE_PAID_CHECKS_MANAGER_ACCESS_KEY, false)
            userPermissions[BULK_TRANSFER_CHECK_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(BULK_TRANSFER_CHECK_MANAGER_ACCESS_KEY, false)
            userPermissions[BULK_VOID_OPEN_CHECKS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(BULK_VOID_OPEN_CHECKS_MANAGER_ACCESS_KEY, false)
            userPermissions[CASH_DRAWER_LOCK_DOWN_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(CASH_DRAWER_LOCK_DOWN_MANAGER_ACCESS_KEY, false)
            userPermissions[CASH_DRAWERS_BLIND_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(CASH_DRAWERS_BLIND_MANAGER_ACCESS_KEY, false)
            userPermissions[CASH_DRAWERS_FULL_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(CASH_DRAWERS_FULL_MANAGER_ACCESS_KEY, false)
            userPermissions[CLOSE_OUT_DAY_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(CLOSE_OUT_DAY_MANAGER_ACCESS_KEY, false)
            userPermissions[DISCOUNTS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(DISCOUNTS_MANAGER_ACCESS_KEY, false)
            userPermissions[EDIT_SENT_ITEMS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(EDIT_SENT_ITEMS_MANAGER_ACCESS_KEY, false)
            userPermissions[EDIT_TIME_ENTRIES_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(EDIT_TIME_ENTRIES_MANAGER_ACCESS_KEY, false)
            userPermissions[END_BREAKS_EARLY_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(END_BREAKS_EARLY_MANAGER_ACCESS_KEY, false)
            userPermissions[FIND_CHECKS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(FIND_CHECKS_MANAGER_ACCESS_KEY, false)
            userPermissions[GIFT_CARD_ADJUSTMENT_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(GIFT_CARD_ADJUSTMENT_MANAGER_ACCESS_KEY, false)
            userPermissions[LARGE_CASH_OVER_UNDER_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(LARGE_CASH_OVER_UNDER_MANAGER_ACCESS_KEY, false)
            userPermissions[LOGBOOK_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(LOGBOOK_MANAGER_ACCESS_KEY, false)
            userPermissions[NEGATIVE_DECLARED_TIPS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(NEGATIVE_DECLARED_TIPS_MANAGER_ACCESS_KEY, false)
            userPermissions[OPEN_ITEMS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(OPEN_ITEMS_MANAGER_ACCESS_KEY, false)
            userPermissions[OTHER_PAYMENT_TYPES_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(OTHER_PAYMENT_TYPES_MANAGER_ACCESS_KEY, false)
            userPermissions[PAYOUT_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(PAYOUT_MANAGER_ACCESS_KEY, false)
            userPermissions[REGISTER_SWIPE_CARDS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(REGISTER_SWIPE_CARDS_MANAGER_ACCESS_KEY, false)
            userPermissions[SEND_NOTIFICATIONS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(SEND_NOTIFICATIONS_MANAGER_ACCESS_KEY, false)
            userPermissions[SHIFT_REVIEW_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(SHIFT_REVIEW_MANAGER_ACCESS_KEY, false)
            userPermissions[TAX_EXEMPT_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(TAX_EXEMPT_MANAGER_ACCESS_KEY, false)
            userPermissions[THROTTLE_ONLINE_ORDERS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(THROTTLE_ONLINE_ORDERS_MANAGER_ACCESS_KEY, false)
            userPermissions[TRANSFER_OR_REWARDS_ADJUSTMENT_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(TRANSFER_OR_REWARDS_ADJUSTMENT_MANAGER_ACCESS_KEY, false)
            userPermissions[UNLINK_REFUNDS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(UNLINK_REFUNDS_MANAGER_ACCESS_KEY, false)
            userPermissions[VOID_ITEMS_ORDERS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(VOID_ITEMS_ORDERS_MANAGER_ACCESS_KEY, false)
            userPermissions[VOID_OR_REFUND_PAYMENTS_MANAGER_ACCESS_KEY] =
                prefs.getBoolean(VOID_OR_REFUND_PAYMENTS_MANAGER_ACCESS_KEY, false)
            return userPermissions
        }

    fun posAccessPermissions(
        addOrUpdateServiceCharges: Boolean,
        applyCashPayments: Boolean,
        cashDrawerAccess: Boolean,
        changeServer: Boolean,
        changeTable: Boolean,
        editOtherEmployeesOrders: Boolean,
        keyInCreditCards: Boolean,
        kitchenDisplayScreenMode: Boolean,
        myReports: Boolean,
        noSale: Boolean,
        offlineOrBackgroundCreditCardProcessing: Boolean,
        pandingOrdersMode: Boolean,
        paymentTerminalMode: Boolean,
        quickOrderMode: Boolean,
        shiftReviewSalesData: Boolean,
        tableServiceMode: Boolean,
        viewOtherEmployeesOrders: Boolean
    ) {
        editor.putBoolean(ADD_OR_UPDATE_SERVICE_CHARGES_POS_ACCESS_KEY, addOrUpdateServiceCharges)
        editor.putBoolean(APPLY_CASH_PAYMENTS_POS_ACCESS_KEY, applyCashPayments)
        editor.putBoolean(CASH_DRAWER_ACCESS_POS_ACCESS_KEY, cashDrawerAccess)
        editor.putBoolean(CHANGE_SERVER_POS_ACCESS_KEY, changeServer)
        editor.putBoolean(CHANGE_TABLE_POS_ACCESS_KEY, changeTable)
        editor.putBoolean(EDIT_OTHER_EMPLOYEE_ORDERS_POS_ACCESS_KEY, editOtherEmployeesOrders)
        editor.putBoolean(KEY_IN_CREDIT_CARDS_POS_ACCESS_KEY, keyInCreditCards)
        editor.putBoolean(KITCHEN_DISPLAY_SCREEN_MODE_POS_ACCESS_KEY, kitchenDisplayScreenMode)
        editor.putBoolean(MY_REPORTS_POS_ACCESS_KEY, myReports)
        editor.putBoolean(NO_SALE_POS_ACCESS_KEY, noSale)
        editor.putBoolean(
            OFFLINE_OR_BACKGROUND_CREDIT_CARD_PROCESSING_POS_ACCESS_KEY,
            offlineOrBackgroundCreditCardProcessing
        )
        editor.putBoolean(PENDING_ORDERS_MODE_POS_ACCESS_KEY, pandingOrdersMode)
        editor.putBoolean(PAYMENT_TERMINAL_MODE_POS_ACCESS_KEY, paymentTerminalMode)
        editor.putBoolean(QUICK_ORDER_MODE_POS_ACCESS_KEY, quickOrderMode)
        editor.putBoolean(SHIFT_REVIEW_SALES_DATA_POS_ACCESS_KEY, shiftReviewSalesData)
        editor.putBoolean(TABLE_SERVICE_MODE_POS_ACCESS_KEY, tableServiceMode)
        editor.putBoolean(VIEW_OTHER_EMPLOYEES_ORDERS_POS_ACCESS_KEY, viewOtherEmployeesOrders)
        editor.commit()
    }

    val getPosAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[ADD_OR_UPDATE_SERVICE_CHARGES_POS_ACCESS_KEY] =
                prefs.getBoolean(ADD_OR_UPDATE_SERVICE_CHARGES_POS_ACCESS_KEY, false)
            userPermissions[APPLY_CASH_PAYMENTS_POS_ACCESS_KEY] =
                prefs.getBoolean(APPLY_CASH_PAYMENTS_POS_ACCESS_KEY, false)
            userPermissions[CASH_DRAWER_ACCESS_POS_ACCESS_KEY] =
                prefs.getBoolean(CASH_DRAWER_ACCESS_POS_ACCESS_KEY, false)
            userPermissions[CHANGE_SERVER_POS_ACCESS_KEY] =
                prefs.getBoolean(CHANGE_SERVER_POS_ACCESS_KEY, false)
            userPermissions[CHANGE_TABLE_POS_ACCESS_KEY] =
                prefs.getBoolean(CHANGE_TABLE_POS_ACCESS_KEY, false)
            userPermissions[EDIT_OTHER_EMPLOYEE_ORDERS_POS_ACCESS_KEY] =
                prefs.getBoolean(EDIT_OTHER_EMPLOYEE_ORDERS_POS_ACCESS_KEY, false)
            userPermissions[KEY_IN_CREDIT_CARDS_POS_ACCESS_KEY] =
                prefs.getBoolean(KEY_IN_CREDIT_CARDS_POS_ACCESS_KEY, false)
            userPermissions[KITCHEN_DISPLAY_SCREEN_MODE_POS_ACCESS_KEY] =
                prefs.getBoolean(KITCHEN_DISPLAY_SCREEN_MODE_POS_ACCESS_KEY, false)
            userPermissions[MY_REPORTS_POS_ACCESS_KEY] =
                prefs.getBoolean(MY_REPORTS_POS_ACCESS_KEY, false)
            userPermissions[NO_SALE_POS_ACCESS_KEY] =
                prefs.getBoolean(NO_SALE_POS_ACCESS_KEY, false)
            userPermissions[OFFLINE_OR_BACKGROUND_CREDIT_CARD_PROCESSING_POS_ACCESS_KEY] =
                prefs.getBoolean(OFFLINE_OR_BACKGROUND_CREDIT_CARD_PROCESSING_POS_ACCESS_KEY, false)
            userPermissions[PENDING_ORDERS_MODE_POS_ACCESS_KEY] =
                prefs.getBoolean(PENDING_ORDERS_MODE_POS_ACCESS_KEY, false)
            userPermissions[PAYMENT_TERMINAL_MODE_POS_ACCESS_KEY] =
                prefs.getBoolean(PAYMENT_TERMINAL_MODE_POS_ACCESS_KEY, false)
            userPermissions[QUICK_ORDER_MODE_POS_ACCESS_KEY] =
                prefs.getBoolean(QUICK_ORDER_MODE_POS_ACCESS_KEY, false)
            userPermissions[SHIFT_REVIEW_SALES_DATA_POS_ACCESS_KEY] =
                prefs.getBoolean(SHIFT_REVIEW_SALES_DATA_POS_ACCESS_KEY, false)
            userPermissions[TABLE_SERVICE_MODE_POS_ACCESS_KEY] =
                prefs.getBoolean(TABLE_SERVICE_MODE_POS_ACCESS_KEY, false)
            userPermissions[VIEW_OTHER_EMPLOYEES_ORDERS_POS_ACCESS_KEY] =
                prefs.getBoolean(VIEW_OTHER_EMPLOYEES_ORDERS_POS_ACCESS_KEY, false)
            return userPermissions
        }

    fun quickEditAccessPermissions(
        addExistingItemsOrMods: Boolean,
        addNewItemsMods: Boolean,
        buttonColor: Boolean,
        fullQuickEdit: Boolean,
        inventoryAndQuantity: Boolean,
        name: Boolean,
        posName: Boolean,
        price: Boolean,
        rearrangingItemsMods: Boolean,
        removeItemsOrMods: Boolean,
        sku: Boolean
    ) {
        editor.putBoolean(ADD_EXISTING_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY, addExistingItemsOrMods)
        editor.putBoolean(ADD_NEW_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY, addNewItemsMods)
        editor.putBoolean(BUTTON_COLOR_QUICK_EDIT_ACCESS_KEY, buttonColor)
        editor.putBoolean(FULL_QUICK_EDIT_ACCESS_KEY, fullQuickEdit)
        editor.putBoolean(INVENTORY_AND_QUANTITY_QUICK_EDIT_ACCESS_KEY, inventoryAndQuantity)
        editor.putBoolean(NAME_QUICK_EDIT_ACCESS_KEY, name)
        editor.putBoolean(POS_NAME_QUICK_EDIT_ACCESS_KEY, posName)
        editor.putBoolean(PRICE_QUICK_EDIT_ACCESS_KEY, price)
        editor.putBoolean(REARRANGING_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY, rearrangingItemsMods)
        editor.putBoolean(REMOVE_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY, removeItemsOrMods)
        editor.putBoolean(SKU_QUICK_EDIT_ACCESS_KEY, sku)
        editor.commit()
    }

    val getQuickEditAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[ADD_EXISTING_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(ADD_EXISTING_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[ADD_NEW_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(ADD_NEW_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[BUTTON_COLOR_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(BUTTON_COLOR_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[FULL_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(FULL_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[INVENTORY_AND_QUANTITY_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(INVENTORY_AND_QUANTITY_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[NAME_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(NAME_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[POS_NAME_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(POS_NAME_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[PRICE_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(PRICE_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[REARRANGING_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(REARRANGING_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[REMOVE_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(REMOVE_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY, false)
            userPermissions[SKU_QUICK_EDIT_ACCESS_KEY] =
                prefs.getBoolean(SKU_QUICK_EDIT_ACCESS_KEY, false)
            return userPermissions
        }

    fun restaurantAdminAccessPermissions(
        customersCreditsAndReports: Boolean,
        editFullMenu: Boolean,
        editHistoricalData: Boolean,
        employeeInfo: Boolean,
        employeeJobsAndWages: Boolean,
        giftOrRewardsCardReport: Boolean,
        houseAccounts: Boolean,
        laborReports: Boolean,
        localMenuEdit: Boolean,
        marketingInfo: Boolean,
        menuReports: Boolean,
        salesReports: Boolean,
        tables: Boolean
    ) {
        editor.putBoolean(CUSTOMER_CREDIT_RESTAURANT_ADMIN_ACCESS_KEY, customersCreditsAndReports)
        editor.putBoolean(EDIT_FULL_MENU_RESTAURANT_ADMIN_ACCESS_KEY, editFullMenu)
        editor.putBoolean(EDIT_HISTORICAL_DATA_RESTAURANT_ADMIN_ACCESS_KEY, editHistoricalData)
        editor.putBoolean(EMPLOYEE_INFO_RESTAURANT_ADMIN_ACCESS_KEY, employeeInfo)
        editor.putBoolean(EMPLOYEE_JOBS_WAGES_RESTAURANT_ADMIN_ACCESS_KEY, employeeJobsAndWages)
        editor.putBoolean(
            GIFT_OR_REWARD_CARD_REPORT_RESTAURANT_ADMIN_ACCESS_KEY,
            giftOrRewardsCardReport
        )
        editor.putBoolean(HOUSE_ACCOUNTS_RESTAURANT_ADMIN_ACCESS_KEY, houseAccounts)
        editor.putBoolean(LABOR_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY, laborReports)
        editor.putBoolean(LOCAL_MENU_EDIT_RESTAURANT_ADMIN_ACCESS_KEY, localMenuEdit)
        editor.putBoolean(MARKETING_INFO_RESTAURANT_ADMIN_ACCESS_KEY, marketingInfo)
        editor.putBoolean(MENU_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY, menuReports)
        editor.putBoolean(SALES_REPORT_RESTAURANT_ADMIN_ACCESS_KEY, salesReports)
        editor.putBoolean(TABLES_RESTAURANT_ADMIN_ACCESS_KEY, tables)
        editor.commit()
    }

    val getRestaurantAdminAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[CUSTOMER_CREDIT_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(CUSTOMER_CREDIT_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[EDIT_FULL_MENU_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(EDIT_FULL_MENU_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[EDIT_HISTORICAL_DATA_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(EDIT_HISTORICAL_DATA_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[EMPLOYEE_INFO_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(EMPLOYEE_INFO_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[EMPLOYEE_JOBS_WAGES_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(EMPLOYEE_JOBS_WAGES_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[GIFT_OR_REWARD_CARD_REPORT_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(GIFT_OR_REWARD_CARD_REPORT_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[HOUSE_ACCOUNTS_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(HOUSE_ACCOUNTS_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[LABOR_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(LABOR_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[LOCAL_MENU_EDIT_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(LOCAL_MENU_EDIT_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[MARKETING_INFO_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(MARKETING_INFO_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[MENU_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(MENU_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[SALES_REPORT_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(SALES_REPORT_RESTAURANT_ADMIN_ACCESS_KEY, false)
            userPermissions[TABLES_RESTAURANT_ADMIN_ACCESS_KEY] =
                prefs.getBoolean(TABLES_RESTAURANT_ADMIN_ACCESS_KEY, false)
            return userPermissions
        }

    fun webSetupAccessPermissions(
        dataExportConfig: Boolean,
        discountsSetup: Boolean,
        financialAccounts: Boolean,
        kitchenOrDiningRoomSetup: Boolean,
        manageInstructions: Boolean,
        paymentsSetup: Boolean,
        publishing: Boolean,
        restaurantGroupsSetup: Boolean,
        restaurantOperationsSetup: Boolean,
        taxRatesSetup: Boolean,
        userPermissions: Boolean
    ) {
        editor.putBoolean(DATA_EXPORT_CONFIG_WEB_SETUP_ACCESS_KEY, dataExportConfig)
        editor.putBoolean(DISCOUNTS_SETUP_WEB_SETUP_ACCESS_KEY, discountsSetup)
        editor.putBoolean(FINANCIAL_ACCOUNTS_WEB_WEB_SETUP_ACCESS_KEY, financialAccounts)
        editor.putBoolean(
            KITCHEN_OR_DINING_ROOM_SETUP_WEB_SETUP_ACCESS_KEY,
            kitchenOrDiningRoomSetup
        )
        editor.putBoolean(MANAGE_INSTRUCTIONS_WEB_SETUP_ACCESS_KEY, manageInstructions)
        editor.putBoolean(PAYMENTS_SETUP_WEB_SETUP_ACCESS_KEY, paymentsSetup)
        editor.putBoolean(PUBLISHING_WEB_SETUP_ACCESS_KEY, publishing)
        editor.putBoolean(RESTAURANT_GROUPS_SETUP_WEB_SETUP_ACCESS_KEY, restaurantGroupsSetup)
        editor.putBoolean(
            RESTAURANT_OPERATIONS_SETUP_WEB_SETUP_ACCESS_KEY,
            restaurantOperationsSetup
        )
        editor.putBoolean(TAX_RATES_SETUP_WEB_SETUP_ACCESS_KEY, taxRatesSetup)
        editor.putBoolean(USER_PERMISSIONS_WEB_WEB_SETUP_ACCESS_KEY, userPermissions)
        editor.commit()
    }

    val getWebSetupAccessPermissions: HashMap<String, Boolean>
        get() {
            val userPermissions = HashMap<String, Boolean>()
            userPermissions[DATA_EXPORT_CONFIG_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(DATA_EXPORT_CONFIG_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[DISCOUNTS_SETUP_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(DISCOUNTS_SETUP_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[FINANCIAL_ACCOUNTS_WEB_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(FINANCIAL_ACCOUNTS_WEB_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[KITCHEN_OR_DINING_ROOM_SETUP_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(KITCHEN_OR_DINING_ROOM_SETUP_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[MANAGE_INSTRUCTIONS_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(MANAGE_INSTRUCTIONS_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[PAYMENTS_SETUP_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(PAYMENTS_SETUP_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[PUBLISHING_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(PUBLISHING_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[RESTAURANT_GROUPS_SETUP_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(RESTAURANT_GROUPS_SETUP_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[RESTAURANT_OPERATIONS_SETUP_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(RESTAURANT_OPERATIONS_SETUP_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[TAX_RATES_SETUP_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(TAX_RATES_SETUP_WEB_SETUP_ACCESS_KEY, false)
            userPermissions[USER_PERMISSIONS_WEB_WEB_SETUP_ACCESS_KEY] =
                prefs.getBoolean(USER_PERMISSIONS_WEB_WEB_SETUP_ACCESS_KEY, false)
            return userPermissions
        }

    fun saveTempOrderList(orderDetails: String) {
        editor.putString(TEMP_ORDER_LIST, orderDetails)
        editor.commit()
    }

    val getTempOrderList: HashMap<String, String>
        get() {
            val tempList = HashMap<String, String>()
            tempList[TEMP_ORDER_LIST] = prefs.getString(TEMP_ORDER_LIST, "")!!
            return tempList
        }

    fun saveRestaurantId(restaurant_id: String, restaurant_name: String) {
        editor.putString(RESTAURANT_ID_KEY, restaurant_id)
        editor.putString(RESTAURANT_NAME_KEY, restaurant_name)
        editor.commit()
    }

    val getRestaurantDetails: HashMap<String, String>
        get() {
            val restaurantDetails = HashMap<String, String>()
            restaurantDetails[RESTAURANT_ID_KEY] = prefs.getString(RESTAURANT_ID_KEY, "")!!
            restaurantDetails[RESTAURANT_NAME_KEY] = prefs.getString(RESTAURANT_NAME_KEY, "")!!
            return restaurantDetails
        }

    fun saveDiningOptions(
        diningOptionId: String,
        diningOptionName: String,
        diningOptionBehaviour: String
    ) {
        editor.putString(DINING_OPTION_ID_KEY, diningOptionId)
        editor.putString(DINING_OPTION_BEHAVIOUR_KEY, diningOptionBehaviour)
        editor.putString(DINING_OPTION_NAME_KEY, diningOptionName)
        editor.commit()
    }

    val getDiningOptions: HashMap<String, String>
        get() {
            val diningDetails = HashMap<String, String>()
            diningDetails[DINING_OPTION_NAME_KEY] = prefs.getString(DINING_OPTION_NAME_KEY, "")!!
            diningDetails[DINING_OPTION_ID_KEY] = prefs.getString(DINING_OPTION_ID_KEY, "")!!
            diningDetails[DINING_OPTION_BEHAVIOUR_KEY] =
                prefs.getString(DINING_OPTION_BEHAVIOUR_KEY, "")!!
            return diningDetails
        }

    fun saveServiceAreaOptions(
        id: String,
        primary: Boolean,
        serviceName: String,
        serviceChargeId: String,
        revenueCenter: String,
        revenueCenterName: String
    ) {
        editor.putString(SERVICE_AREA_ID_KEY, id)
        editor.putBoolean(SERVICE_AREA_PRIMARY_KEY, primary)
        editor.putString(SERVICE_AREA_NAME_KEY, serviceName)
        editor.putString(SERVICE_AREA_CHARGE_ID_KEY, serviceChargeId)
        editor.putString(REVENUE_CENTER_ID_KEY, revenueCenter)
        editor.putString(REVENUE_CENTER_NAME_KEY, revenueCenterName)
        editor.commit()
    }

    val getServiceAreaOptions: HashMap<String, Any>
        get() {
            val diningDetails = HashMap<String, Any>()
            diningDetails[SERVICE_AREA_ID_KEY] = prefs.getString(SERVICE_AREA_ID_KEY, "")!!
            diningDetails[SERVICE_AREA_PRIMARY_KEY] =
                prefs.getBoolean(SERVICE_AREA_PRIMARY_KEY, false)
            diningDetails[SERVICE_AREA_NAME_KEY] = prefs.getString(SERVICE_AREA_NAME_KEY, "")!!
            diningDetails[SERVICE_AREA_CHARGE_ID_KEY] =
                prefs.getString(SERVICE_AREA_CHARGE_ID_KEY, "")!!
            diningDetails[REVENUE_CENTER_ID_KEY] = prefs.getString(REVENUE_CENTER_ID_KEY, "")!!
            diningDetails[REVENUE_CENTER_NAME_KEY] = prefs.getString(REVENUE_CENTER_NAME_KEY, "")!!
            return diningDetails
        }

    fun saveCashDrawer(cash_drawer_id: String, cash_drawer_name: String, cash_drawer_no: String) {
        editor.putString(CASH_DRAWER_ID_KEY, cash_drawer_id)
        editor.putString(CASH_DRAWER_NAME_KEY, cash_drawer_name)
        editor.putString(CASH_DRAWER_NUMBER_KEY, cash_drawer_no)
        editor.commit()
    }

    val getCashDrawer: HashMap<String, String>
        get() {
            val cashDrawerDetails = HashMap<String, String>()
            cashDrawerDetails[CASH_DRAWER_ID_KEY] = prefs.getString(CASH_DRAWER_ID_KEY, "")!!
            cashDrawerDetails[CASH_DRAWER_NAME_KEY] = prefs.getString(CASH_DRAWER_NAME_KEY, "")!!
            cashDrawerDetails[CASH_DRAWER_NUMBER_KEY] =
                prefs.getString(CASH_DRAWER_NUMBER_KEY, "")!!
            return cashDrawerDetails
        }

    fun savePrepStation(prepStationId: String, prepStationName: String) {
        editor.putString(DEVICE_PREP_STATION_ID, prepStationId)
        editor.putString(DEVICE_PREP_STATION_NAME, prepStationName)
        editor.commit()
    }

    val getPrepStation: HashMap<String, String>
        get() {
            val prepStation = HashMap<String, String>()
            prepStation[DEVICE_PREP_STATION_ID] = prefs.getString(DEVICE_PREP_STATION_ID, "")!!
            prepStation[DEVICE_PREP_STATION_NAME] = prefs.getString(DEVICE_PREP_STATION_NAME, "")!!
            return prepStation
        }

    fun updateRestaurantMode(mode: Boolean) {
        editor.putBoolean(RESTAURANT_MODE_KEY, mode)
        editor.commit()
    }

    val getRestaurantMode: Boolean
        get() = prefs.getBoolean(RESTAURANT_MODE_KEY, false)

    fun saveCurrencyType(type: String) {
        editor.putString(CURRENCY_TYPE, type)
        editor.commit()
    }

    val getCurrencyType: String
        get() = prefs.getString(CURRENCY_TYPE, "")!!

    fun saveTempEmpId(id: String) {
        editor.putString(TEMP_EMP_ID, id)
        editor.commit()
    }

    val getTempEmpId: String
        get() = prefs.getString(TEMP_EMP_ID, "")!!

    fun saveDeviceDetails(deviceName: String, deviceNameId: String) {
        editor.putString(DEVICE_NAME, deviceName)
        editor.putString(DEVICE_NAME_ID, deviceNameId)
        editor.commit()
    }

    val getDeviceDetails: HashMap<String, String>
        get() {
            val prepStation = HashMap<String, String>()
            prepStation[DEVICE_NAME] = prefs.getString(DEVICE_NAME, "")!!
            prepStation[DEVICE_NAME_ID] = prefs.getString(DEVICE_NAME_ID, "")!!
            return prepStation
        }

    companion object {
        const val IS_LOGGED_IN = "isLoggedIn"
        const val ID_KEY = "id" // this is employee id, use this for APIs
        const val PREFERENCE_NAME = "ZingRestaurant"
        const val TABLE_NUMBER = "TableNumber"
        const val NO_OF_GUESTS = "NoofGuests"
        const val no = "TableNumber"
        const val JOB_ID_KEY = "job_id"
        const val RESTAURANT_ID_KEY = "restaurant_id"
        const val RESTAURANT_NAME_KEY = "restaurant_name"
        const val RESTAURANT_EMPLOYEE_ID_KEY =
            "employee_id"   // this restaurant specific employee id, not to use in APIs
        const val EMPLOYEE_FIRST_NAME_KEY = "employee_first_name"
        const val EMPLOYEE_LAST_NAME_KEY = "employee_last_name"
        const val EMPLOYEE_PROFILE_PIC_KEY = "employee_profile_pic"
        const val DINING_OPTION_NAME_KEY = "dining_name"
        const val DINING_OPTION_ID_KEY = "dining_id"
        const val DINING_OPTION_BEHAVIOUR_KEY = "dining_behaviour"
        const val SERVICE_AREA_ID_KEY = "service_area_id"
        const val SERVICE_AREA_PRIMARY_KEY = "service_area_primary"
        const val SERVICE_AREA_NAME_KEY = "service_area_name"
        const val SERVICE_AREA_CHARGE_ID_KEY = "service_area_charge_id"
        const val REVENUE_CENTER_ID_KEY = "revenue_center_id"
        const val REVENUE_CENTER_NAME_KEY = "revenue_center_name"
        const val CASH_DRAWER_NAME_KEY = "cash_drawer_name"
        const val CASH_DRAWER_ID_KEY = "cash_drawer_id"
        const val CASH_DRAWER_NUMBER_KEY = "cash_drawer_number"
        const val RESTAURANT_MODE_KEY = "restaurant_mode"
        const val DEVICE_PREP_STATION_NAME = "prep_station_name"
        const val DEVICE_PREP_STATION_ID = "prep_station_id"
        const val DEVICE_NAME_ID = "device_name_id"
        const val DEVICE_NAME = "device_name"

        /*employee permissions*/
        //accountAdminAccess
        const val DATA_EXPORT_CONFIG_ADMIN_ACCESS_KEY = "dataExportConfig"
        const val FINANCIAL_ACCOUNTS_ADMIN_ACCESS_KEY = "financialAccounts"
        const val MANAGE_INTEGRATIONS_ADMIN_ACCESS_KEY = "manageIntegrations"
        const val USER_PERMISSIONS_ADMIN_ACCESS_KEY = "userPermissions"

        //deliveryAccess
        const val CANCEL_DISPATCH_DELIVERY_ACCESS_KEY = "cancelDispatch"
        const val COMPLETE_DELIVERY_DELIVERY_ACCESS_KEY = "completeDelivery"
        const val DELIVERY_MODE_DELIVERY_ACCESS_KEY = "deliveryMode"
        const val DISPATCH_DRIVER_DELIVERY_ACCESS_KEY = "dispatchDriver"
        const val UPDATE_ALL_DELIVERY_ORDERS_DELIVERY_ACCESS_KEY = "updateAllDeliveryOrders"
        const val UPDATE_DRIVER_DELIVERY_ACCESS_KEY = "updateDriver"

        //deviceSetupAccess
        const val ADVANCED_TERMINAL_DEFAULT_SETUP_ACCESS_KEY = "advancedTerminalSetup"
        const val KD_AND_ORDER_SETUP_DEFAULT_SETUP_ACCESS_KEY = "kdsAndOrderScreenSetup"
        const val TERMINAL_SETUP_DEFAULT_SETUP_ACCESS_KEY = "terminalSetup"

        //managerAccess
        const val ADJUST_CASH_DRAWER_START_BALANCE_MANAGER_ACCESS_KEY =
            "adjustCashDrawerStartBalance"
        const val BULK_CLOSE_PAID_CHECKS_MANAGER_ACCESS_KEY = "bulkClosePaidChecks"
        const val BULK_TRANSFER_CHECK_MANAGER_ACCESS_KEY = "bulkTransferCheck"
        const val BULK_VOID_OPEN_CHECKS_MANAGER_ACCESS_KEY = "bulkVoidOpenChecks"
        const val CASH_DRAWER_LOCK_DOWN_MANAGER_ACCESS_KEY = "cashDrawerLockdown"
        const val CASH_DRAWERS_BLIND_MANAGER_ACCESS_KEY = "cashDrawersBlind"
        const val CASH_DRAWERS_FULL_MANAGER_ACCESS_KEY = "cashDrawersFull"
        const val CLOSE_OUT_DAY_MANAGER_ACCESS_KEY = "closeOutDay"
        const val DISCOUNTS_MANAGER_ACCESS_KEY = "discounts"
        const val EDIT_SENT_ITEMS_MANAGER_ACCESS_KEY = "editSentItems"
        const val EDIT_TIME_ENTRIES_MANAGER_ACCESS_KEY = "editTimeEntries"
        const val END_BREAKS_EARLY_MANAGER_ACCESS_KEY = "endBreaksEarly"
        const val FIND_CHECKS_MANAGER_ACCESS_KEY = "findChecks"
        const val GIFT_CARD_ADJUSTMENT_MANAGER_ACCESS_KEY = "giftCardAdjustment"
        const val LARGE_CASH_OVER_UNDER_MANAGER_ACCESS_KEY = "largeCashOverOrUnder"
        const val LOGBOOK_MANAGER_ACCESS_KEY = "logBook"
        const val NEGATIVE_DECLARED_TIPS_MANAGER_ACCESS_KEY = "negativeDeclaredTips"
        const val OPEN_ITEMS_MANAGER_ACCESS_KEY = "openItems"
        const val OTHER_PAYMENT_TYPES_MANAGER_ACCESS_KEY = "otherPaymentTypes"
        const val PAYOUT_MANAGER_ACCESS_KEY = "payout"
        const val REGISTER_SWIPE_CARDS_MANAGER_ACCESS_KEY = "registerSwipeCards"
        const val SEND_NOTIFICATIONS_MANAGER_ACCESS_KEY = "sendNotifications"
        const val SHIFT_REVIEW_MANAGER_ACCESS_KEY = "shiftReview"
        const val TAX_EXEMPT_MANAGER_ACCESS_KEY = "taxExempt"
        const val THROTTLE_ONLINE_ORDERS_MANAGER_ACCESS_KEY = "throttleOnlineOrders"
        const val TRANSFER_OR_REWARDS_ADJUSTMENT_MANAGER_ACCESS_KEY = "transferOrRewardsAdjustment"
        const val UNLINK_REFUNDS_MANAGER_ACCESS_KEY = "unlinkedRefunds"
        const val VOID_ITEMS_ORDERS_MANAGER_ACCESS_KEY = "voidItemOrOrders"
        const val VOID_OR_REFUND_PAYMENTS_MANAGER_ACCESS_KEY = "voidOrRefundPayments"

        //posAccess
        const val ADD_OR_UPDATE_SERVICE_CHARGES_POS_ACCESS_KEY = "addOrUpdateServiceCharges"
        const val APPLY_CASH_PAYMENTS_POS_ACCESS_KEY = "applyCashPayments"
        const val CASH_DRAWER_ACCESS_POS_ACCESS_KEY = "cashDrawerAccess"
        const val CHANGE_SERVER_POS_ACCESS_KEY = "changeServer"
        const val CHANGE_TABLE_POS_ACCESS_KEY = "changeTable"
        const val EDIT_OTHER_EMPLOYEE_ORDERS_POS_ACCESS_KEY = "editOtherEmployeesOrders"
        const val KEY_IN_CREDIT_CARDS_POS_ACCESS_KEY = "keyInCreditCards"
        const val KITCHEN_DISPLAY_SCREEN_MODE_POS_ACCESS_KEY = "kitchenDisplayScreenMode"
        const val MY_REPORTS_POS_ACCESS_KEY = "myReports"
        const val NO_SALE_POS_ACCESS_KEY = "noSale"
        const val OFFLINE_OR_BACKGROUND_CREDIT_CARD_PROCESSING_POS_ACCESS_KEY =
            "offlineOrBackgroundCreditCardProcessing"
        const val PENDING_ORDERS_MODE_POS_ACCESS_KEY = "pandingOrdersMode"
        const val PAYMENT_TERMINAL_MODE_POS_ACCESS_KEY = "paymentTerminalMode"
        const val QUICK_ORDER_MODE_POS_ACCESS_KEY = "quickOrderMode"
        const val SHIFT_REVIEW_SALES_DATA_POS_ACCESS_KEY = "shiftReviewSalesData"
        const val TABLE_SERVICE_MODE_POS_ACCESS_KEY = "tableServiceMode"
        const val VIEW_OTHER_EMPLOYEES_ORDERS_POS_ACCESS_KEY = "viewOtherEmployeesOrders"

        //quickEditAccess
        const val ADD_EXISTING_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY = "addExistingItemsOrMods"
        const val ADD_NEW_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY = "addNewItemsMods"
        const val BUTTON_COLOR_QUICK_EDIT_ACCESS_KEY = "buttonColor"
        const val FULL_QUICK_EDIT_ACCESS_KEY = "fullQuickEdit"
        const val INVENTORY_AND_QUANTITY_QUICK_EDIT_ACCESS_KEY = "inventoryAndQuantity"
        const val NAME_QUICK_EDIT_ACCESS_KEY = "name"
        const val POS_NAME_QUICK_EDIT_ACCESS_KEY = "posName"
        const val PRICE_QUICK_EDIT_ACCESS_KEY = "price"
        const val REARRANGING_ITEMS_MODS_QUICK_EDIT_ACCESS_KEY = "rearrangingItemsMods"
        const val REMOVE_ITEMS_OR_MODS_QUICK_EDIT_ACCESS_KEY = "removeItemsOrMods"
        const val SKU_QUICK_EDIT_ACCESS_KEY = "sku"

        //restaurantAdminAccess
        const val CUSTOMER_CREDIT_RESTAURANT_ADMIN_ACCESS_KEY = "customersCreditsAndReports"
        const val EDIT_FULL_MENU_RESTAURANT_ADMIN_ACCESS_KEY = "editFullMenu"
        const val EDIT_HISTORICAL_DATA_RESTAURANT_ADMIN_ACCESS_KEY = "editHistoricalData"
        const val EMPLOYEE_INFO_RESTAURANT_ADMIN_ACCESS_KEY = "employeeInfo"
        const val EMPLOYEE_JOBS_WAGES_RESTAURANT_ADMIN_ACCESS_KEY = "employeeJobsAndWages"
        const val GIFT_OR_REWARD_CARD_REPORT_RESTAURANT_ADMIN_ACCESS_KEY = "giftOrRewardsCardReport"
        const val HOUSE_ACCOUNTS_RESTAURANT_ADMIN_ACCESS_KEY = "houseAccounts"
        const val LABOR_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY = "laborReports"
        const val LOCAL_MENU_EDIT_RESTAURANT_ADMIN_ACCESS_KEY = "localMenuEdit"
        const val MARKETING_INFO_RESTAURANT_ADMIN_ACCESS_KEY = "marketingInfo"
        const val MENU_REPORTS_RESTAURANT_ADMIN_ACCESS_KEY = "menuReports"
        const val SALES_REPORT_RESTAURANT_ADMIN_ACCESS_KEY = "salesReports"
        const val TABLES_RESTAURANT_ADMIN_ACCESS_KEY = "tables"

        //webSetupAccess
        const val DATA_EXPORT_CONFIG_WEB_SETUP_ACCESS_KEY = "dataExportConfig"
        const val DISCOUNTS_SETUP_WEB_SETUP_ACCESS_KEY = "discountsSetup"
        const val FINANCIAL_ACCOUNTS_WEB_WEB_SETUP_ACCESS_KEY = "financialAccounts"
        const val KITCHEN_OR_DINING_ROOM_SETUP_WEB_SETUP_ACCESS_KEY = "kitchenOrDiningRoomSetup"
        const val MANAGE_INSTRUCTIONS_WEB_SETUP_ACCESS_KEY = "manageInstructions"
        const val PAYMENTS_SETUP_WEB_SETUP_ACCESS_KEY = "paymentsSetup"
        const val PUBLISHING_WEB_SETUP_ACCESS_KEY = "publishing"
        const val RESTAURANT_GROUPS_SETUP_WEB_SETUP_ACCESS_KEY = "restaurantGroupsSetup"
        const val RESTAURANT_OPERATIONS_SETUP_WEB_SETUP_ACCESS_KEY = "restaurantOperationsSetup"
        const val TAX_RATES_SETUP_WEB_SETUP_ACCESS_KEY = "taxRatesSetup"
        const val USER_PERMISSIONS_WEB_WEB_SETUP_ACCESS_KEY = "userPermissions"

        //order details
        const val TEMP_ORDER_LIST = "order_details"

        const val CURRENCY_TYPE = "currency_type"
        const val TEMP_EMP_ID = "temp_emp_id"
    }
}