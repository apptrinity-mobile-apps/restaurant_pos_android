package com.restaurant.zing.helpers

data class BillsListDataModel(
    val displayValue: String,
    val value: Double,
    val type: String,
    val icon: Int,
    val count: String
)