package com.restaurant.zing.helpers

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.widget.EditText
import java.text.SimpleDateFormat
import java.util.*

class CustomDatePickerDialog {
    var mDay = 0
    var mMonth = 0
    var mYear = 0
    lateinit var mDate: Calendar
    lateinit var datePickerDialog: DatePickerDialog

    fun showDatePicker(
        context: Context,
        date: String,
        editText: EditText, time: String
    ) {
        val calendar = Calendar.getInstance()
        if (date != "") {
            var date1 = date
            if (date1.contains("Scheduled time: ")) {
                date1 = date1.replaceFirst("Scheduled time: ", "")
            }
            val sdf = SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.getDefault())
            val newDate = sdf.parse(date1)!!
            calendar.time = newDate
        }
        mDay = calendar[Calendar.DAY_OF_MONTH]
        mMonth = calendar[Calendar.MONTH]
        mYear = calendar[Calendar.YEAR]
        mDate = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(
            context,
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                mDate.set(year, monthOfYear, dayOfMonth)
                val mFormat = "MM-dd-yyyy"
                val sdf = SimpleDateFormat(mFormat, Locale.getDefault())
                val date = sdf.format(mDate.time)
                editText.setText("Scheduled time: $date $time")
                //editText.setText(date +"---"+ time)
            },
            mYear, mMonth, mDay
        )
        val cal = Calendar.getInstance()
        datePickerDialog.datePicker.minDate = cal.timeInMillis
        datePickerDialog.show()
    }

}