package com.restaurant.zing.helpers

import android.app.ActivityOptions
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.restaurant.zing.activities.KitchenDisplayActivity
import com.restaurant.zing.activities.OrderDetailsActivity
import com.restaurant.zing.activities.PendingOrdersActivity
import com.restaurant.zing.R
import org.json.JSONObject

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("TAG", "From: ${remoteMessage.from}")

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d("TAG", "Message Notification Body: ${it.body}")
            Log.d("TAG", "Message Notification Body: ${it.title}")
            sendNotification(remoteMessage, it.title!!, it.body!!)
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    fun sendNotification(remoteMessage: RemoteMessage?, title: String, message: String) {
        Log.e("remoteMessage", remoteMessage.toString())
        var dataType = ""
        var noOfGuests = ""
        var tableNumber = ""
        var revenueCenterId = ""
        var serviceAreaId = ""
        if (remoteMessage!!.data.isNotEmpty()) {
            try {
                val params = remoteMessage.data
                val object1 = JSONObject(params as Map<*, *>)
                Log.e("JSON_OBJECT", object1.toString())
                if (object1.has("dataType")) {
                    dataType = object1.getString("dataType")
                }
                if (dataType == "order") {
                    // read only data type if from waiter order
                } else if (dataType == "table_service") {
                    noOfGuests = object1.getString("noOfGuests")
                    tableNumber = object1.getString("tableNumber")
                    revenueCenterId = object1.getString("revenueCenterId")
                    serviceAreaId = object1.getString("serviceAreaId")
                    Log.e("TYPEANDID", noOfGuests + "-----" + tableNumber)
                } else if (dataType == "table_service_occupied") {
                    noOfGuests = object1.getString("noOfGuests")
                    tableNumber = object1.getString("tableNumber")
                    revenueCenterId = object1.getString("revenueCenterId")
                    serviceAreaId = object1.getString("serviceAreaId")
                    Log.e(
                        "TYPEANDIDelse",
                        noOfGuests + "-----" + tableNumber + "===" + revenueCenterId
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 101
        val channelId = "zing-channel-01"
        val channelName = "Zing-POS"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
        val sound: Uri =
            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + RingtoneManager.TYPE_NOTIFICATION)
        val mBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle(title)
            .setContentText(message)
            .setSound(sound)
            .setAutoCancel(true)
            .setSmallIcon(R.mipmap.ic_launcher)

        when (dataType) {
            "order" -> {
                val resultIntent = Intent(this, KitchenDisplayActivity::class.java)
                resultIntent.putExtra("from_screen", "notifications")
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                mBuilder.build().contentIntent.send()
                resultIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                val options =
                    ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
                this.startActivity(resultIntent, options.toBundle())
            }
            "table_service" -> {
                val resultIntent = Intent(this, OrderDetailsActivity::class.java)
                resultIntent.putExtra("from_screen", "table_service")
                resultIntent.putExtra("noOfGuests", noOfGuests)
                resultIntent.putExtra("tableNumber", tableNumber)
                resultIntent.putExtra("revenueCenterId", revenueCenterId)
                resultIntent.putExtra("serviceAreaId", serviceAreaId)
                resultIntent.putExtra("dataType", dataType)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                mBuilder.build().contentIntent.send()
                resultIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                val options =
                    ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
                this.startActivity(resultIntent, options.toBundle())
            }
            "table_service_occupied" -> {
                val resultIntent = Intent(this, OrderDetailsActivity::class.java)
                resultIntent.putExtra("from_screen", "table_service")
                resultIntent.putExtra("noOfGuests", noOfGuests)
                resultIntent.putExtra("tableNumber", tableNumber)
                resultIntent.putExtra("revenueCenterId", revenueCenterId)
                resultIntent.putExtra("serviceAreaId", serviceAreaId)
                resultIntent.putExtra("dataType", dataType)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                mBuilder.build().contentIntent.send()
                resultIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                val options =
                    ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
                this.startActivity(resultIntent, options.toBundle())
            }
            "online_order" -> {
                val resultIntent = Intent(this, PendingOrdersActivity::class.java)
                resultIntent.putExtra("from_screen", "notifications")
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
        }
    }

}