package com.restaurant.zing.helpers

import java.io.Serializable

class ComboBogoItemType(
    var id: String,
    var name: String,
    var menu_type: String,
    var quantity: Int,
    var items: ArrayList<ComboBogoCustomPojo>
) : Serializable