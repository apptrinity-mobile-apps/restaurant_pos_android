package com.restaurant.zing.helpers

import java.io.Serializable

class BogoMenuPositionAndItem(
    var menu_position: Int,
    var name: String,
    var sizeName: String,
    var basePrice: Double,
    var menu_type: String,
    var item: ComboBogoCustomPojo
) : Serializable