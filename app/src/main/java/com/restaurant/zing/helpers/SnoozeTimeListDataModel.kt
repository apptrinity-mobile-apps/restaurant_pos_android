package com.restaurant.zing.helpers

data class SnoozeTimeListDataModel(val id: String, val value: String)